<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Profile extends My_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('users/profile_model');
	}
	public function index(){
		$data	= array(
			'user'				=> $this->profile_model->getUserDetails(),
			'globalConfig'		=> $this->profile_model->getGlobalConfig(),
			'automationConfig'	=> $this->profile_model->getAutomationConfig(),
		);
		$data['postingDates']	= json_decode($data['globalConfig']['dateLockSettings'], true);
		$this->template->load_template("users/profile", array('data' => $data), $this->session_data);
	}
	public function saveBasic(){
		$data	= array(
			'firstname'	=> $this->input->post('firstname'),
			'lastname'	=> $this->input->post('lastname'),
			'phone'		=> $this->input->post('phone'),
			'email'		=> $this->input->post('email'),
		);
		$this->profile_model->saveBasic($data);
		$return	= array(
			'status'	=> true,
			'message'	=> 'Data saved successfully',
		);
		echo json_encode($return);
		die();
	}
	public function saveDatesDatas(){
		$data	= array(
			'cogs'							=> $this->input->post('cogs'),
			'sales'							=> $this->input->post('sales'),
			'stocks'						=> $this->input->post('stocks'),
			'purchase'						=> $this->input->post('purchase'),
			'consolcogs'					=> $this->input->post('consolcogs'),
			'salescredit'					=> $this->input->post('salescredit'),
			'consolsales'					=> $this->input->post('consolsales'),
			'cogsCondition'					=> $this->input->post('cogsCondition'),
			'purchaseCredit'				=> $this->input->post('purchaseCredit'),
			'salesCondition'				=> $this->input->post('salesCondition'),
			'stocksCondition'				=> $this->input->post('stocksCondition'),
			'consolsalescredit'				=> $this->input->post('consolsalescredit'),
			'purchaseCondition'				=> $this->input->post('purchaseCondition'),
			'consolcogsCondition'			=> $this->input->post('consolcogsCondition'),
			'salescreditCondition'			=> $this->input->post('salescreditCondition'),
			'consolsalesCondition'			=> $this->input->post('consolsalesCondition'),
			'purchaseCreditCondition'		=> $this->input->post('purchaseCreditCondition'),
			'consolsalescreditCondition'	=> $this->input->post('consolsalescreditCondition'),
		);
		
		$this->profile_model->saveDatesDatas($data);
		$return	= array(
			'status'	=> true,
			'message'	=> 'Data saved successfully',
		);
		echo json_encode($return);
		die();
	}
	public function updatePassword(){
		$message	= 'Password changed successfully';
		$status		= '1';
		$data		= array(
			'password'		=> $this->input->post('password'),
			'newpassword'	=> $this->input->post('newpassword'),
			'newpassword2'	=> $this->input->post('newpassword2'),
		);
		
		$uppercase		= preg_match('@[A-Z]@', $data['newpassword']);
		$lowercase		= preg_match('@[a-z]@', $data['newpassword']);
		$number			= preg_match('@[0-9]@', $data['newpassword']);
		$specialChars	= preg_match('@[^\w]@', $data['newpassword']);
		
		if(trim($data['password']) == ''){
			$message	= 'Please enter current password';
			$status		= 0;
		}
		elseif(trim($data['newpassword']) == ''){
			$message	= 'Please enter new password';
			$status		= 0;
		}
		elseif(trim($data['newpassword']) != trim($data['newpassword2'])){
			$message	= 'New password and confirm new password not matched';
			$status		= 0;
		}
		else{
			$res	= $this->profile_model->updatePassword($data);
			if($res){
				$message	= 'Password updated successfully';
				$status		= 1;
			}
			else{
				$message	= 'Problem in updating password';
				$status		= 0;
			}
		}
		$return	= array(
			'status'	=> $status,
			'message'	=> $message,
		);
		echo json_encode($return);
		die();
	}
	public function updateProfilePic(){
		$img		= $this->input->post('file');
		$return		= array(
			'status'	=> '0',
			'message'	=> "Please select file",
		);
		if($img){
			$uploadFileName	= uniqid() . '.png';
			$img			= str_replace('data:image/png;base64,', '', $img);
			if(substr_count($img, 'image/jpg')){
				$uploadFileName	= uniqid() . '.jpg';
				$img			= str_replace('data:image/jpg;base64,', '', $img);
			}
			$img		= str_replace(' ', '+', $img);
			$data		= base64_decode($img);
			$file		= FCPATH . '/bsitc/metronics/assets/layouts/layout/img/profile/' . $uploadFileName;
			$success	= file_put_contents($file, $data);
			$return		= array(
				'status'	=> '1',
				'message'	=> "Profile image successfully uploaded",
			);
			$saveData	= array('profileimage' => '/bsitc/metronics/assets/layouts/layout/img/profile/' . $uploadFileName);
			$this->profile_model->uploadedProfiePic($saveData);
		}
		echo json_encode($return);
		die();
	}
	public function saveGlobalConfig(){
		$mappings	= $this->config->item('mapping');
		$data		= array(
			/* 'fetchProduct'				=> $this->input->post('fetchProduct'), */
			/* 'postProduct'				=> $this->input->post('postProduct'), */
			/* 'enablePrebook'				=> $this->input->post('enablePrebook'), */
			/* 'fetchCustomer'				=> $this->input->post('fetchCustomer'), */
			/* 'postCustomer'				=> $this->input->post('postCustomer'), */
			/* 'fetchSalesOrder'			=> $this->input->post('fetchSalesOrder'), */
			/* 'postSalesOrder'				=> $this->input->post('postSalesOrder'), */
			/* 'fetchReceipt'				=> $this->input->post('fetchReceipt'), */
			/* 'postReceipt'				=> $this->input->post('postReceipt'), */
			/* 'fetchDispatchConfirmation'	=> $this->input->post('fetchDispatchConfirmation'), */
			/* 'postDispatchConfirmation'	=> $this->input->post('postDispatchConfirmation'), */
			/* 'fetchPurchaseOrder'			=> $this->input->post('fetchPurchaseOrder'), */
			/* 'postPurchaseOrder'			=> $this->input->post('postPurchaseOrder'), */
			/* 'fetchStockAdjustment'		=> $this->input->post('fetchStockAdjustment'), */
			/* 'postStockAdjustment'		=> $this->input->post('postStockAdjustment'), */
			/* 'enableStockSync'			=> $this->input->post('enableStockSync'), */
			/* 'fetchStockSync'				=> $this->input->post('fetchStockSync'), */
			/* 'enblePreorder'				=> $this->input->post('enblePreorder'), */
			/* 'postStockSync'				=> $this->input->post('postStockSync'), */
			/* 'enableInventoryadvice'		=> $this->input->post('enableInventoryadvice'),   */
			/* 'fetchInventoradvice'		=> $this->input->post('fetchInventoradvice'),   */
			/* 'postInventoradvice'			=> $this->input->post('postInventoradvice'),   */
			/* 'fetchSalesCredit'			=> $this->input->post('fetchSalesCredit'),   */
			/* 'postSalesCredit'			=> $this->input->post('postSalesCredit'),   */
			/* 'fetchPurchaseCredit'		=> $this->input->post('fetchPurchaseCredit'),   */
			/* 'postPurchaseCredit'			=> $this->input->post('postPurchaseCredit'), */
			/* 'enableAvalaraTax'			=> $this->input->post('enableAvalaraTax'), */
			/* 'enableReports'				=> $this->input->post('enableReports'), */
			/* 'fetchStockJournal'			=> $this->input->post('fetchStockJournal'), */
			/* 'postStockJournal'			=> $this->input->post('postStockJournal'), */
			/* 'enableMapping'				=> $this->input->post('enableMapping'),		*/
			
			
			'app_name'									=> $this->input->post('app_name'),
			'account1Name'								=> $this->input->post('account1Name'),
			'account1Liberary'							=> $this->input->post('account1Liberary'),
			'account2Name'								=> $this->input->post('account2Name'),
			'account2Liberary'							=> $this->input->post('account2Liberary'),
			'enableProduct'								=> $this->input->post('enableProduct'),
			'enableCustomer'							=> $this->input->post('enableCustomer'),
			'enableSalesOrder'							=> $this->input->post('enableSalesOrder'),
			'enableReceipt'								=> $this->input->post('enableReceipt'),
			'enableDispatchConfirmation'				=> $this->input->post('enableDispatchConfirmation'),
			'enablePurchaseOrder'						=> $this->input->post('enablePurchaseOrder'),
			'enableStockAdjustment'						=> $this->input->post('enableStockAdjustment'),
			'enableSalesCredit'							=> $this->input->post('enableSalesCredit'),  
			'enablePurchaseCredit'						=> $this->input->post('enablePurchaseCredit'),  
			'enableInventoryTransfer'					=> $this->input->post('enableInventoryTransfer'),
			'enableInventoryManagement'					=> $this->input->post('enableInventoryManagement'),
			'enableAmazonFees'							=> $this->input->post('enableAmazonFees'),
			'enableAggregation'							=> $this->input->post('enableAggregation'),
			'enableSAConsol'							=> $this->input->post('enableSAConsol'),
			'enableCOGSJournals'						=> $this->input->post('enableCOGSJournals'),
			
			'enableCOGSWOInvoice'						=> $this->input->post('enableCOGSWOInvoice'),
			
			'enableAggregationAdvance'					=> $this->input->post('enableAggregationAdvance'),
			'enableGenericCustomerMappingCustomazation'	=> $this->input->post('enableGenericCustomerMappingCustomazation'),
			'enableConsolidationMappingCustomazation'	=> $this->input->post('enableConsolidationMappingCustomazation'),
			'enableConsolStockAdjustment'				=> $this->input->post('enableConsolStockAdjustment'),
			'enableAdvanceTaxMapping'					=> $this->input->post('enableAdvanceTaxMapping'),
			'enableAggregationOnAPIfields'				=> $this->input->post('enableAggregationOnAPIfields'),
			'enableNetOffConsol'						=> $this->input->post('enableNetOffConsol'),
			'enableAmazonFeeOther'						=> $this->input->post('enableAmazonFeeOther'),
			'enableGRNIjournal'							=> $this->input->post('enableGRNIjournal'),
			'enablePrepayments'							=> $this->input->post('enablePrepayments'),
			
			'emailReceipents'							=> $this->input->post('emailReceipents'),
			'smtpUsername'								=> $this->input->post('smtpUsername'),
			'smtpPassword'								=> $this->input->post('smtpPassword'),
			'disableSOpaymentbptoqbo'					=> $this->input->post('disableSOpaymentbptoqbo'),
			'disableSOpaymentqbotobp'					=> $this->input->post('disableSOpaymentqbotobp'),
			'disableSCpaymentbptoqbo'					=> $this->input->post('disableSCpaymentbptoqbo'),
			'disableSCpaymentqbotobp'					=> $this->input->post('disableSCpaymentqbotobp'),
			'disablePOpaymentqbotobp'					=> $this->input->post('disablePOpaymentqbotobp'),
			'disablePCpaymentqbotobp'					=> $this->input->post('disablePCpaymentqbotobp'),
			'disableConsolSOpayments'					=> $this->input->post('disableConsolSOpayments'),
			'disableConsolSCpayments'					=> $this->input->post('disableConsolSCpayments'),
			'autoArchivePeriod'							=> $this->input->post('autoArchivePeriod'),
			'archiveType'								=> $this->input->post('archiveType'),
		);
		foreach($mappings as $key => $mapping){
			$valKey			= 'enable'.ucwords($key).'Mapping';
			$data[$valKey]	= @(int)$this->input->post($valKey);
		}
		$this->profile_model->saveGlobalConfig($data);
		$return	= array(
			'status'	=> '1',
			'message'	=> "Connector configuration saved successfully",
		);
		echo json_encode($return);
		die();
	}
	public function updateMetadata(){
		$orderTypeArray		= array("sales_order","sales_credit_order","purchase_order","purchase_credit_order");
		$updateTypeArray	= array("customers","products");
		$data				= array(
			'Ids'				=> trim($this->input->post('Ids')),
			'datatype'			=> trim($this->input->post('datatype')),
			'actionRequired'	=> trim($this->input->post('actionRequired')),
			'account2Name'		=> trim($this->input->post('account2Name')),
		);
		$message	= 'Data Changed successfully';
		$status		= '1';
		if(trim($data['Ids']) == ''){
			$message	= 'Please enter IDs';
			$status		= 0;
		}
		elseif($data['datatype'] == ''){
			$message	= 'Please Select DataType';
			$status		= 0;
		}
		elseif($data['actionRequired'] == ''){
			$message	= 'Please Select Action';
			$status		= 0;
		}
		elseif($data['actionRequired'] == ''){
			$message	= 'Please Select Action';
			$status		= 0;
		}
		elseif((($data['actionRequired'] == '111') OR ($data['actionRequired'] == '3')) AND (!in_array($data['datatype'], $orderTypeArray))){
			$message	= 'Action Not Supported for selected DataType';
			$status		= 0;
		}
		elseif((($data['actionRequired'] == '222') OR ($data['actionRequired'] == '2')) AND (!in_array($data['datatype'], $updateTypeArray))){
			$message	= 'Action Not Supported for selected DataType';
			$status		= 0;
		}
		elseif(($data['actionRequired'] == '222') AND (!$data['account2Name'])){
			$message	= 'Select Account 2 For this Action';
			$status		= 0;
		}
		else{
			$res	= $this->profile_model->updateMetadata($data);
			if($res){
				$message	= 'Data Changed successfully';
				$status		= 1;
			}
			else{
				$message	= 'Problem in updating data';
				$status		= 0;
			}
		}
		$return		= array(
			'status'	=> $status,
			'message'	=> $message,
		);
		echo json_encode($return);
		die();
	}
	public function updateDataIds(){
		$data			= array(
			'brightpearlId'	=> trim($this->input->post('brightpearlId')),
			'newupdateId'	=> trim($this->input->post('newupdateId')),
			'datatype'		=> trim($this->input->post('datatype')),
			'account2Name'	=> trim($this->input->post('account2Name')),
		);
		$message	= 'Data Changed successfully';
		$status		= '1';
		if(trim($data['brightpearlId']) == ''){
			$message	= 'Please enter Brightpearl ID';
			$status		= 0;
		}
		elseif(trim($data['newupdateId']) == ''){
			$message	= 'Please enter New Updated ID';
			$status		= 0;
		}
		elseif(trim($data['datatype']) == ''){
			$message	= 'Please Select DataType';
			$status		= 0;
		}
		elseif(trim($data['account2Name']) == ''){
			$message	= 'Please Select Account 2';
			$status		= 0;
		}
		else{
			$res	= $this->profile_model->updateDataIds($data);
			if($res){
				$message	= 'Data Changed successfully';
				$status		= 1;
			}
			else{
				$message	= 'Problem in updating data';
				$status		= 0;
			}
		}
		$return		= array(
			'status'	=> $status,
			'message'	=> $message,
		);
		echo json_encode($return);
		die();
	}
	public function saveAutomationsetup(){
		$data	= array(
			'productSync'			=> $this->input->post('productSync'),
			'customerSync'			=> $this->input->post('customerSync'),
			'salesOrderSync'		=> $this->input->post('salesOrderSync'),
			'salesCreditSync'		=> $this->input->post('salesCreditSync'),
			'purchaseOrderSync'		=> $this->input->post('purchaseOrderSync'),
			'purchaseCreditSync'	=> $this->input->post('purchaseCreditSync'),
			'stockAdjustmentSync'	=> $this->input->post('stockAdjustmentSync'),
			'amazonFeesSync'		=> $this->input->post('amazonFeesSync'),
			'cogsJournalSync'		=> $this->input->post('cogsJournalSync'),
			'salesOrderConsolSync'	=> $this->input->post('salesOrderConsolSync'),
			'salesCreditConsolSync'	=> $this->input->post('salesCreditConsolSync'),
			'cogsJournalConsolSync'	=> $this->input->post('cogsJournalConsolSync'),
			'disableAutomation'		=> $this->input->post('disableAutomation'),
		);
		$this->profile_model->saveAutomationsetup($data);
		$return	= array(
			'status'		=> true,
			'message'		=> 'Data saved successfully',
		);
		echo json_encode($return);
		die();
	}
}