<?php
//Xero dashboraddatasync
if(!defined('BASEPATH')){
	exit('No direct script access allowed');
}
#[\AllowDynamicProperties]
class Dashboraddatasync extends CI_Controller {
	public $ci;
	public function __construct(){
		parent::__construct();
		$this->ci				= &get_instance();
		$this->appName			= $this->globalConfig['app_name'];
	}
	public function processdashboraddatasync(){
		$xeroApiCallInfo		= $this->getxeroapiinfo();
		$allSyncedOrdersInfo	= $this->getallordersInfo();
	}
	public function getallordersInfo(){
		$allSO		= $this->db->select('count("id") as countsales')->get_where('sales_order',array('status <=' => 3, 'status<>' => 0))->row_array()['countsales'];
		$allPO		= $this->db->select('count("id") as countsales')->get_where('purchase_order',array('status <=' => 3, 'status<>' => 0))->row_array()['countsales'];
		$allSC		= $this->db->select('count("id") as countsales')->get_where('sales_credit_order',array('status <=' => 3, 'status<>' => 0))->row_array()['countsales'];
		$allPC		= $this->db->select('count("id") as countsales')->get_where('purchase_credit_order',array('status <=' => 3, 'status<>' => 0))->row_array()['countsales'];
		$allSOArc	= $this->db->select('count("id") as countsales')->get_where('sales_order_archived',array('status <=' => 3, 'status<>' => 0))->row_array()['countsales'];
		$allPOArc	= $this->db->select('count("id") as countsales')->get_where('purchase_order_archived',array('status <=' => 3, 'status<>' => 0))->row_array()['countsales'];
		$allCOGS	= $this->db->select('count("id") as countsales')->get_where('cogs_journal',array('status' => 1))->row_array()['countsales'];
		$allSA		= $this->db->select('count("id") as countsales')->get_where('stock_adjustment',array('status' => 1))->row_array()['countsales'];
		$allSyncOrdersData	= array(
			'sales'				=> ($allSO + $allSOArc),
			'salesCredit'		=> ($allSC),
			'purchase'			=> ($allPO + $allPOArc),
			'purchaseCredit'	=> ($allPC),
			'cogs'				=> ($allCOGS),
			'stockAdjustment'	=> ($allSA),
		);
		if($allSyncOrdersData){
			return $allSyncOrdersData;
		}
	}
	public function getxeroapiinfo(){
		$allxeroAccounts	= $this->db->get_where('account_xero_account',array('username <>' => ''))->result_array();
		if($allxeroAccounts){
			$apiInfoXero	= array();
			$XeroAccountIds	= array();
			foreach($allxeroAccounts as $allxeroAccount){
				$XeroAccountIds[$allxeroAccount['id']]['name']		= $allxeroAccount['name'];
				$XeroAccountIds[$allxeroAccount['id']]['companyId']	= $allxeroAccount['companyId'];
			}
			if($XeroAccountIds){
				foreach($XeroAccountIds as $XeroAccountId){
					$lastLimit		= 0;
					$headerInfo		= array();
					$CallDatas		= $this->ci->db->order_by('id', 'desc')->select('headerData')->like('headerData', $XeroAccountId['companyId'])->get_where('api_call_log',array('headerData <>' => ''))->row_array();
					$headerDatas	= explode("\n",$CallDatas['headerData']);
					if($headerDatas){
						foreach($headerDatas as $headerData){
							if((substr_count($headerData,"X-DayLimit-Remaining"))){
								$headerInfo	= explode(":",$headerData);
								break;
							}
						}
					}
					if($headerInfo){
						$lastLimit	= trim($headerInfo[1]);
					}
					if($lastLimit){
						$apiInfoXero[$XeroAccountId['name']]	= $lastLimit;
					}
				}
			}
			if($apiInfoXero){
				return $apiInfoXero;
			}
		}
	}
}