<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Contactlinking extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/contactlinking_model');
	}
	public function index(){
		$data	= array();
		$data	= $this->contactlinking_model->get();
		$data['account1FieldValue']	= array();
		$data['account2ContactId']	= array();
		$this->template->load_template("mapping/contactlinking",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->contactlinking_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->contactlinking_model->delete($id);
		}
	}
}
?>