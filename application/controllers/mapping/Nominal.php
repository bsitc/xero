<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Nominal extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/nominal_model');
	}
	public function index(){
		$data						= array();
		$data						= $this->nominal_model->get();
		$data['account1NominalId']	= $this->{$this->globalConfig['account1Liberary']}->nominalCode();
		$data['account1ChannelId']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account2NominalId']	= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
		$this->template->load_template("mapping/nominal",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->nominal_model->save($data);
		echo json_encode($res);
		die();
	}
	public function uploadNominalMapping(){
		error_reporting('0');
		$columnRow	= 9;
		$fileCsv	= array();
		if($_FILES['uploadprefile']['tmp_name']){
			$file	= fopen($_FILES['uploadprefile']['tmp_name'], "r");
			$count	= 0;
			while(!feof($file)){
				$row	= fgetcsv($file);
				if($count++ == 0){
					$columnRow	= count($row);
					continue;
				}
				if($columnRow != 6){
					continue;
				}
				if($row){
					$fileCsv[]	= array(
						'account1NominalId'			=> $row['0'],
						'account1ChannelId'			=> $row['1'],
						'account1CustomFieldValue'	=> $row['2'],
						'account2NominalId'			=> $row['3'],
						'account1Id'				=> $row['4'],
						'account2Id'				=> $row['5'],
					);
				}
			}
			fclose($file);
		}
		foreach($fileCsv as $row){
			$this->db->replace('mapping_nominal',$row);
		}
		redirect($_SERVER['HTTP_REFERER'] , 'refresh');
	}
	public function delete($id){
		if($id){
			echo $this->nominal_model->delete($id);
		}
	}
}
?>