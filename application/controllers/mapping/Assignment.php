<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Assignment extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/assignment_model');
	}
	public function index(){
		$data							= array();
		$data							= $this->assignment_model->get();
		$data['account1StaffId']		= $this->{$this->globalConfig['account1Liberary']}->getAllSalesrep();
		$data['account1ProjectId']		= $this->{$this->globalConfig['account1Liberary']}->getAllProjects();
		$data['account1ChannelId']		= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account1LeadSourceId']	= $this->{$this->globalConfig['account1Liberary']}->getAllLeadsource();
		$data['account1TeamId']			= $this->{$this->globalConfig['account1Liberary']}->getAllTeam();
		$data['account2ChannelId']		= $this->{$this->globalConfig['account2Liberary']}->getAllChannelMethod();
		$this->template->load_template("mapping/assignment",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->assignment_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->assignment_model->delete($id);
		}
	}
}
?>