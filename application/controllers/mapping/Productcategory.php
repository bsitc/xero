<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Productcategory extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/productcategory_model');
	}
	public function index(){
		$data							= array();
		$data							= $this->productcategory_model->get();
		$data['account1CategoryId']		= $this->{$this->globalConfig['account1Liberary']}->getAllCategoryMethod();
		$data['account2ChannelId']		= $this->{$this->globalConfig['account2Liberary']}->getAllChannelMethod();
		$this->template->load_template("mapping/productcategory",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->productcategory_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->productcategory_model->delete($id);
		}
	}
}
?>