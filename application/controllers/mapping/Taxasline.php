<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Taxasline extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/taxasline_model');
	}
	public function index(){
		$data						= array();
		$data						= $this->taxasline_model->get();
		$data['account1ChannelId']	= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account2TaxAccountIds']	= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
		$data['account1ApiFieldId']	= $this->{$this->globalConfig['account1Liberary']}->getAllSalesCustomFieldForTax();
		$this->template->load_template("mapping/taxasline",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->taxasline_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->taxasline_model->delete($id);
		}
	}
}
?>