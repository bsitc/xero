<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Reference extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('mapping/reference_model');
	}
	public function index(){
		$data							= array();
		$data							= $this->reference_model->get();
		$data['account1ChannelId']		= $this->{$this->globalConfig['account1Liberary']}->getAllChannelMethod();
		$data['account1CustomFieldId']	= $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();
		$this->template->load_template("mapping/reference",array("data"=>$data));
	}
	public function save(){
		$data	= $this->input->post('data');
		$res	= $this->reference_model->save($data);
		echo json_encode($res);
		die();
	}
	public function delete($id){
		if($id){
			echo $this->reference_model->delete($id);
		}
	}
}
?>