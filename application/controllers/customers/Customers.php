<?php
//Xero Customer Controller
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Customers extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('customers/customers_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("customers/customers",$data,$this->session_data);
	}
	public function getCustomers(){
		$records	= $this->customers_model->getCustomers();
		echo json_encode($records);
	}
	public function fetchCustomers($customerId = ''){
		$this->customers_model->fetchCustomers($customerId);
	}
	public function customerInfo($customerId = ''){
		$data['customerInfo']	= $this->db->get_where('customers', array('id' => $customerId))->row_array();
		$this->template->load_template("customers/customerInfo",$data);
	}
	public function postCustomers($customerId = '',$postedAccount2Id = ''){
		$this->customers_model->postCustomers($customerId,$postedAccount2Id);
	}
	/* public function updatecontactid(){
		$bpid	= '';
		$xeroid	= '';
		if(trim($this->input->get('bpid'))){
			$bpid	= trim($this->input->get('bpid'));
		}
		if(trim($this->input->get('xeroid'))){
			$xeroid	= trim($this->input->get('xeroid'));
		}
		if($bpid AND $xeroid){
			$this->db->where(array('customerId' => $bpid))->update('customers',array('createdCustomerId' => $xeroid, 'status' => '1'));
		}
	} */
}