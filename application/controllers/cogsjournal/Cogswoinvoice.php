<?php
if(!defined('BASEPATH')){
	exit('No direct script access allowed');
}
#[\AllowDynamicProperties]
class Cogswoinvoice extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('cogsjournal/cogswoinvoice_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("cogsjournal/cogswoinvoice",$data,$this->session_data);
	}
	public function getCogswoInvoicejournal(){
		$records	= $this->cogswoinvoice_model->getCogswoInvoicejournal(); 
		echo json_encode($records);
	}
	public function fetchCogswoinvoice($orderId = ''){
		$this->cogswoinvoice_model->fetchCogswoinvoice($orderId);
	}
	public function postCogswoInvoice($orderId = ''){
		$this->cogswoinvoice_model->postCogswoInvoice($orderId);
	}
	public function postConsolidatedCogswoInvoice($orderId = ''){
		$this->cogswoinvoice_model->postConsolidatedCogswoInvoice($orderId);
	}
	public function cogswoInvoicejournalInfo($orderId = ''){
		$data['cogsjournalInfo']	= $this->db->get_where('cogs_woinvoice_journal',array('journalsId' => $orderId))->row_array();
		$this->template->load_template("cogsjournal/cogswoInvoicejournalInfo",$data,$this->session_data);
	}
	public function exportCogs(){
		error_reporting('0');
		$this->db->reset_query();
		
		$where	= array();
        $query	= $this->db;
		
		if (trim($this->input->get('account1Id'))) {
			$where['account1Id']		= trim($this->input->get('account1Id'));
		}
		if (trim($this->input->get('account2Id'))) {
			$where['account2Id']		= trim($this->input->get('account2Id'));
		}
		if (trim($this->input->get('journalsId'))) {
			$where['journalsId']		= trim($this->input->get('journalsId'));
		}
		if (trim($this->input->get('createdJournalsId'))) {
			$where['createdJournalsId']	= trim($this->input->get('createdJournalsId'));
		}
		if (trim($this->input->get('invoiceReference'))) {
			$where['invoiceReference']	= trim($this->input->get('invoiceReference'));
		}
		if (trim($this->input->get('orderId'))) {
			$where['orderId']			= trim($this->input->get('orderId'));
		}
		if (trim($this->input->get('journalTypeCode'))) {
			$where['journalTypeCode']	= trim($this->input->get('journalTypeCode'));
		}
		if(trim($this->input->get('channelName'))){
			$where['channelName']		= trim($this->input->get('channelName'));
		}
		if(trim($this->input->get('status')) >= '0'){
			$where['status']			= trim($this->input->get('status'));
		}
		if(trim($this->input->get('isConsolidated')) >= '0'){
			$where['isConsolidated']	= trim($this->input->get('isConsolidated'));
		}
		if (trim($this->input->get('taxDate_from'))) {
			$query->where('date(taxDate) >= ', "date('" . $this->input->get('taxDate_from') . "')", false);
        }
        if (trim($this->input->get('taxDate_to'))) {
			$query->where('date(taxDate) <= ', "date('" . $this->input->get('taxDate_to') . "')", false);
		}
		if (trim($this->input->get('created_from'))) {
			$query->where('date(created) >= ', "date('" . $this->input->get('created_from') . "')", false);
        }
        if (trim($this->input->get('created_to'))) {
			$query->where('date(created) <= ', "date('" . $this->input->get('created_to') . "')", false);
		}
		if ($where) {
            $query->like($where);
        }
		$datas	= $query->select('id,account1Id,account2Id,journalsId,createdJournalsId,invoiceReference,orderId,journalTypeCode,channelName,status,isConsolidated,taxDate,created,creditAmount,OrderType')->get_where('cogs_woinvoice_journal')->result_array();	
		
		$account1Mappings		= array();
		$account2Mappings		= array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$allBPWarehouse			= $this->brightpearl->getAllLocation();
		if($allBPWarehouse){
			foreach($allBPWarehouse[1] as $allBPWarehouses){
				$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
			}
		}
		
		$file	= date('Ymd').'_CogsReport.csv';
		$fp		= fopen('php://output', 'w');
		$header	= array('BPAccount','XeroAccount','JournalID','OrderID','InvoiceRef','XeroJournalID','JournalType','Channel','TaxDate','Amount','OrderType','IsConsol','Status');
		
		
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$file);
		fputcsv($fp, $header);
		$Status	= array('0' => 'Pending','1' => 'Sent','3' => 'Error','4'=> 'Archive');
		foreach($datas as $data){
			$row	= array(
				$account1Mappings[$data['account1Id']]['name'],
				$account2Mappings[$data['account2Id']]['name'],
				$data['journalsId'],
				$data['orderId'],
                $data['invoiceReference'],
                $data['createdJournalsId'],
                $data['journalTypeCode'],
				$data['channelName'],
                date('Y-m-d',strtotime($data['taxDate'])),
                $data['creditAmount'],
                $data['OrderType'],
                $data['IsConsol'],
                $Status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}