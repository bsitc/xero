<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class EmailAlerts extends CI_Controller {
	public $ci;
	public function __construct(){
		parent::__construct();
		$this->load->library('mailer');
		$this->ci			= &get_instance();
		$this->appName		= trim($this->globalConfig['app_name']);
		$this->from			= ['info@bsitc-apps.com' => $this->appName];
		$this->Receipents	= trim($this->globalConfig['emailReceipents']);
		$this->smtpUsername	= trim($this->globalConfig['smtpUsername']);
		$this->smtpPassword	= trim($this->globalConfig['smtpPassword']);
		
		if($this->Receipents === ''){
			$this->Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com,harsh@businesssolutionsinthecloud.com';
		}
		
		$this->defaultPhp	= '8.2';
		$getVersion			= phpversion();
		if(strlen($getVersion) > 0){
			$this->defaultPhp	= substr($getVersion,0,3);
		}
	}
	public function runTaskXero($taskName = 'processEmailAlerts'){
		if($taskName){
			$isRunning	= $this->getRunningTask($taskName);
			if(!$isRunning){
				$syncFilePath	= FCPATH . '/jobsoutput/' . date('Y/m/d') . '/synclogs/';
				if(!is_dir(($syncFilePath))){
					mkdir(($syncFilePath),0777,true);
					chmod(($syncFilePath), 0777);
				}
				$syncFileName	= $taskName.'_'.date('H-i-s').'.logs';
				
				if($this->defaultPhp){
					$isSellRUn	= shell_exec('/opt/plesk/php/'.$this->defaultPhp.'/bin/php '.FCPATH.'index.php emailAlerts '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				else{
					$isSellRUn	= shell_exec('php '.FCPATH.'index.php emailAlerts '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				
				if(is_file($syncFilePath.$syncFileName)){
					$filecontent	= file_get_contents($syncFilePath.$syncFileName);
					if((strlen($filecontent) > 0) AND ((substr_count($filecontent,"Type:        TypeError")) OR (substr_count($filecontent,"Fatal error")) OR (substr_count($filecontent,"Database error")) OR (substr_count($filecontent,"Type:        ValueError")) OR (substr_count($filecontent,"Type:        Error")))){
						$appName		= trim($this->globalConfig['app_name']);
						$Receipents		= trim($this->globalConfig['emailReceipents']);
						if($Receipents == ''){
							$Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com,harsh@businesssolutionsinthecloud.com';
						}
						$subject		= 'Alert '.$appName.' - '.$taskName.' Run time error';
						$from			= ['info@bsitc-apps.com' => $this->appName];
						$mailBody		= 'Hi,<br><br><p>Automation not completed successfully, please check the attached cron output file.</p><br><br><br><br><br>Thanks & Regards<br>BSITC Team';
						$this->mailer->send($Receipents, $subject, $mailBody, $from, $syncFilePath.$syncFileName);
					}
				}
			}
		}
		die();
	}
	public function getRunningTask($checkTask = 'processEmailAlerts'){
		$return		= false;
		$checkTask	= trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			$fcpath	= strtolower(FCPATH. 'index.php emailAlerts '.$checkTask);
			foreach($outputs as $output){
				$output	= strtolower($output);
				if((strpos($output, $fcpath) !== false) AND (strpos($output, $checkTask) !== false) AND (strpos($output, 'runtask') === false)){
					$return	= true;
					break;
				}
			}
		}
		return $return;
	}
	public function processEmailAlerts(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$noFetchOrderData		= '';
		$noPostOrdersData		= '';
		$duplicateJournals		= '';
		$duplicatePayments		= '';
		$pendingOrdersData		= '';
		$pendingPaymentsData	= '';
		$noAmazonfeeFetchData	= '';
		$pendingCogsJournal		= '';
		$pendingAdvance			= '';
		$FilePath				= '';
		$FilePathArray			= array();
		
		
		$noFetchOrderData		= $this->noDataFetch();
		$noPostOrdersData		= $this->noDataPost();
		$duplicateJournals		= $this->duplicateJournals();
		$duplicatePayments		= $this->duplicatePayments();
		$pendingOrdersData		= $this->pendingOrders();
		$pendingPaymentsData	= $this->pendingPaymentAlert();
		if($this->globalConfig['enableAmazonFees']){
			$noAmazonfeeFetchData	= $this->noAmazonfeeFetch();
		}
		if($this->globalConfig['enableCOGSJournals']){
			$pendingCogsJournal		= $this->pendingCogsAlert();
		}
		if($this->globalConfig['enableCOGSWOInvoice']){
			$pendingCogsJournal		= $this->pendingCogsWOINvoiceAlert();
		}
		if($this->globalConfig['enablePrepayments']){
			$pendingAdvance			= $this->pendingAdvanceAlert();
		}
		
		
		if($duplicateJournals OR $noFetchOrderData OR $noPostOrdersData OR $duplicatePayments OR $pendingOrdersData OR $pendingPaymentsData OR $noAmazonfeeFetchData OR $pendingCogsJournal OR $pendingAdvance){
			$subject	= '"'.$this->appName.'" Process Report';
			$mainBody	= '';
			$mainBody	= 'Hi,<br>Here are the process Report for '.$this->appName.'<br>';
			if($noFetchOrderData){
				$mainBody	.= $noFetchOrderData;
			}
			if($noPostOrdersData){
				$mainBody	.= $noPostOrdersData;
			}
			if($noAmazonfeeFetchData){
				$mainBody	.= $noAmazonfeeFetchData;
			}
			if($duplicateJournals){
				$mainBody	.= $duplicateJournals;
			}
			if($duplicatePayments){
				$mainBody	.= $duplicatePayments;
			}
			if($pendingCogsJournal){
				$mainBody	.= $pendingCogsJournal;
			}
			if($pendingAdvance){
				$mainBody	.= $pendingAdvance;
			}
			if(isset($pendingPaymentsData['OrderIds'])){
				$mainBody	.= $pendingPaymentsData['OrderIds'];
			}
			if(isset($pendingOrdersData['mailBody'])){
				$mainBody	.= $pendingOrdersData['mailBody'];
			}
			if(isset($pendingPaymentsData['PaymentsErrorLogs'])){
				$ErrorLogFile		= str_replace(' ', '', (strtolower($this->appName))).'_payments_error_log_'.date('Ymd').'.csv';
				$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR ;
				if(!is_dir($FilePath)){
					mkdir($FilePath,0777,true);
					chmod(dirname($FilePath), 0777);
				}
				$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR .$ErrorLogFile;
				$FilePathArray[]	= $FilePath;
				$fp					= fopen($FilePath, 'w');
				$header				= array('OrderType', 'OrderId', 'Message');
				fputcsv($fp, $header);
				foreach($pendingPaymentsData['PaymentsErrorLogs'] as $orderType => $Logs){
					foreach($Logs as $orderIds => $Messagess){
						$AllError	= array();
						$row		= array();
						foreach($Messagess as $Message){
							if(is_array($Message)){
								if($Message['Message']){
									$AllError[]	= $Message['Message'];
								}
								elseif($Message['message']){
									$AllError[]	= $Message['message'];
								}
							}
							else{
								$AllError[]	= $Message;
							}
						}
						if($AllError){
							$row	= array($orderType,$orderIds,implode("~~",$AllError));
						}
						fputcsv($fp, $row);
					}
				}
				fclose($fp);
			}
			if(isset($pendingOrdersData['ErrorLogs'])){
				$ErrorLogFile		= str_replace(' ', '', (strtolower($this->appName))).'_error_log_'.date('Ymd').'.csv';
				$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR ;
				if(!is_dir($FilePath)){
					mkdir($FilePath,0777,true);
					chmod(dirname($FilePath), 0777);
				}
				$FilePath			= FCPATH .'ErrorLogs'. DIRECTORY_SEPARATOR .$ErrorLogFile;
				$FilePathArray[]	= $FilePath;
				$fp					= fopen($FilePath, 'w');
				$header				= array('OrderType', 'OrderId', 'Message');
				fputcsv($fp, $header);
				foreach($pendingOrdersData['ErrorLogs'] as $orderType => $Logs){
					foreach($Logs as $orderIds => $Messagess){
						$AllError	= array();
						$row		= array();
						foreach($Messagess as $Message){
							if(is_array($Message)){
								$AllError[]	= $Message['Message'];
							}
							else{
								$AllError[]	= $Message;
							}
						}
						if($AllError){
							$row	= array($orderType,$orderIds,implode("~~",$AllError));
						}
						fputcsv($fp, $row);
					}
				}
				fclose($fp);
			}
			$mainBody	.= '<br>Thanks & Regards<br>BSITC Team';
			if($FilePathArray){
				$this->mailer->send($this->Receipents, $subject, $mainBody, $this->from, $FilePathArray, $this->smtpUsername, $this->smtpPassword);
			}
			else{
				$this->mailer->send($this->Receipents, $subject, $mainBody, $this->from, '', $this->smtpUsername, $this->smtpPassword);
			}
		}
		$this->db->truncate('ci_sessions');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	public function noAmazonfeeFetch(){
		$amzFeeBody		= '';
		$feeFetchInfo	= $this->db->select_max('fetchDate')->get_where('amazon_ledger', array('journalsId <>' => ''))->row_array();
		if((is_array($feeFetchInfo)) AND (isset($feeFetchInfo['fetchDate']))){
			if(strtotime($feeFetchInfo['fetchDate']) < strtotime('-16 days')){
				$amzFeeBody	.= '<br><b># No Amazon Fees have been fetched in the last 15 days.</b><br>';
			}
		}
		return $amzFeeBody;
	}
	public function noDataFetch(){
		$orderBody	= '';
		$orderTypes	= array('sales_order' => 'SalesOrder', 'sales_credit_order' => 'SalesCredit', 'purchase_order' => 'PurchaseOrder', 'purchase_credit_order' => 'PurchaseCredit');
		foreach($orderTypes as $tableName => $orderType){
			$orderFetchData	= $this->db->select_max('InvoicedTime')->get_where($tableName,array('orderId <>' => ''))->row_array();
			if((is_array($orderFetchData)) AND (!empty($orderFetchData)) AND (isset($orderFetchData['InvoicedTime']))){
				$lastOrderFetchedAt	= date('Ymd',strtotime($orderFetchData['InvoicedTime']));
				if($lastOrderFetchedAt < date('Ymd',strtotime('-1 day'))){
					$orderBody	.= '<br><b># No '.$orderType.' has been fetched in last 1 day.</b><br>';
				}
			}
		}
		return $fetchBody;
	}
	public function noDataPost(){
		$orderBody		= '';
		$orderPostData	= $this->db->select_max('PostedTime')->get_where('sales_order',array('orderId <>' => ''))->row_array();
		if((is_array($orderPostData)) AND (!empty($orderPostData)) AND (isset($orderPostData['PostedTime']))){
			$lastOrderPostedAt	= date('Ymd',strtotime($orderPostData['PostedTime']));
			if($lastOrderPostedAt < date('Ymd',strtotime('-2 day'))){
				$orderBody	.= '<br><b># No SalesOrder has been posted in last 2 days.</b><br>';
			}
		}
		return $orderBody;
	}
	public function duplicateJournals(){
		$journalsBody	= '';
		$journalTypes	= array();
		if($this->globalConfig['enableAmazonFees']){
			$journalTypes['amazon_ledger']			= 'Aamazon Journals';
		}
		if($this->globalConfig['enableCOGSJournals']){
			$journalTypes['cogs_journal']			= 'COGS Journals';
		}
		if($this->globalConfig['enableCOGSWOInvoice']){
			$journalTypes['cogs_woinvoice_journal']	= 'COGS Journals';
		}
		
		if(!empty($journalTypes)){
			foreach($journalTypes as $tableName => $journalType){
				$journalIds			= $this->ci->db->query('SELECT COUNT(journalsId), journalsId FROM `'.$tableName.'` GROUP BY journalsId HAVING COUNT(journalsId) > 1')->result_array();
				if((is_array($journalIds)) AND (!empty($journalIds))){
					$allDuplicateIds	= array();
					$allDuplicateIds	= array_column($journalIds, 'journalsId');
					$allDuplicateIds	= array_filter($allDuplicateIds);
					$allDuplicateIds	= array_unique($allDuplicateIds);
					
					$journalsBody		.= '<br><br><h4><b>Duplicate '.$journalType.' Fetched - Journal IDs ('.count($allDuplicateIds).')</b></h4>';
					$journalsBody		.= '<h>'.implode(", ",$allDuplicateIds).'</h4>';
				}
			}
		}
		return $journalsBody;
	}
	public function duplicatePayments(){
		$this->brightpearl->reInitialize();
		$orderTypes				= array();
		$saveOrdersDatas		= array();
		$paymentEndPoints		= array();
		$paymentAlertBody		= '';
		$paymentDataMapping		= array();
		$duplicatePaymentOrders	= array();
		
		if(!$this->globalConfig['disableSOpaymentqbotobp']){
			$orderTypes['sales_order']				= 'SO';
		}
		if(!$this->globalConfig['disableSCpaymentqbotobp']){
			$orderTypes['sales_credit_order']		= 'SC';
		}
		if(!$this->globalConfig['disablePOpaymentqbotobp']){
			$orderTypes['purchase_order']			= 'PO';
		}
		if(!$this->globalConfig['disablePCpaymentqbotobp']){
			$orderTypes['purchase_credit_order']	= 'PC';
		}
		
		if(!empty($orderTypes)){
			foreach($orderTypes as $tableName => $orderType){
				$tempOrderDatas	= $this->ci->db->select('orderId,totalAmount')->get_where($tableName,array('paymentDetails <>' => '', 'status <>' => '0'))->result_array();
				if((is_array($tempOrderDatas)) AND (!empty($tempOrderDatas))){
					foreach($tempOrderDatas as $tempOrderDatas){
						$saveOrdersDatas[$orderType][$tempOrderDatas['orderId']]	= $tempOrderDatas;
					}
				}
			}
		}
		
		if((isset($saveOrdersDatas['SO'])) OR (isset($saveOrdersDatas['SC']))){
			$paymentEndPoints[]	= '/accounting-service/customer-payment-search?createdOn='.date('Y-m-d',strtotime('-120 days')).'/';
		}
		if((isset($saveOrdersDatas['PO'])) OR (isset($saveOrdersDatas['PC']))){
			$paymentEndPoints[]	= '/accounting-service/supplier-payment-search?createdOn='.date('Y-m-d',strtotime('-120 days')).'/';
		}
		
		if((is_array($paymentEndPoints)) AND (!empty($paymentEndPoints))){
			foreach($paymentEndPoints as $paymentEndPoint){
				foreach($this->brightpearl->accountDetails as  $account1Id => $accountDetails){
					$paymentResults	= $this->brightpearl->getCurl($paymentEndPoint, "GET", '', 'json', $account1Id)[$account1Id];
					if((is_array($paymentResults)) AND (!empty($paymentResults)) AND (isset($paymentResults['results']))){
						$headers	= array_column($paymentResults['metaData']['columns'],'name');
						foreach($paymentResults['results'] as $paymentResult){
							$row	= array_combine($headers,$paymentResult);
							$paymentDataMapping[$row['orderId']][$row['paymentType']][$row['amountPaid']][]	= $row;
						}
						while($paymentResults['metaData']['morePagesAvailable']){
							$paymentEndPoint1	= $paymentEndPoint . '&firstResult=' . $paymentResults['metaData']['lastResult'];
							$paymentResults		= $this->brightpearl->getCurl($paymentEndPoint1, "GET", '', 'json', $account1Id)[$account1Id];
							if((is_array($paymentResults)) AND (!empty($paymentResults)) AND (isset($paymentResults['results']))){
								$headers	= array_column($paymentResults['metaData']['columns'],'name');
								foreach($paymentResults['results'] as $paymentResult){
									$row	= array_combine($headers,$paymentResult);
									$paymentDataMapping[$row['orderId']][$row['paymentType']][$row['amountPaid']][]	= $row;
								}
							}
						}
					}
				}
			}
		}
		
		if((is_array($paymentDataMapping)) AND (!empty($paymentDataMapping))){
			foreach($paymentDataMapping as $orderId => $paymentDataMapping1){
				foreach($paymentDataMapping1 as $paymentType => $paymentDataMapping11){
					foreach($paymentDataMapping11 as $amountPaid => $paymentData){
						$tempOrderType	= '';
						if(isset($saveOrdersDatas['SO'][$orderId])){$tempOrderType	= 'SO';}
						if(isset($saveOrdersDatas['SC'][$orderId])){$tempOrderType	= 'SC';}
						if(isset($saveOrdersDatas['PO'][$orderId])){$tempOrderType	= 'PO';}
						if(isset($saveOrdersDatas['PC'][$orderId])){$tempOrderType	= 'PC';}
						
						if($tempOrderType){
							$totalAmountPaid		= array_sum(array_column($paymentData, 'amountPaid'));
							$paymentTypeOne			= ($paymentType == 'RECEIPT') ? ('RECEIPT') : 'PAYMENT';
							$paymentTypeTwo			= ($paymentType == 'RECEIPT') ? ('PAYMENT') : 'RECEIPT';
							$paymentTypeOneCount	= count($paymentData);
							
							if($paymentTypeOneCount > 1){
								if(isset($paymentDataMapping[$orderId][$paymentTypeTwo][$amountPaid])){
									$paymentTypeTwoCount	= count($paymentDataMapping[$orderId][$paymentTypeTwo][$amountPaid]);
									if($paymentTypeTwoCount > 0){
										$duplicatePaymentOrders[$tempOrderType][]	= $orderId;
									}
								}
								if($saveOrdersDatas[$tempOrderType][$orderId]['totalAmount'] < $totalAmountPaid){
									$duplicatePaymentOrders[$tempOrderType][]	= $orderId;
								}
							}
						}
					}
				}
			}
		}
		
		if((is_array($duplicatePaymentOrders)) AND (!empty($duplicatePaymentOrders))){
			$paymentAlertBody	= '<br><b># Following Orders have duplicate payments in Brightpearl -:</b><br>';
			$paymentAlertBody	.= '<table border=1><thead><tr><th>OrderType</th><th>OrderIds</th></tr></thead><tbody>';
			
			$nameMapping		= array('SO' => 'SalesOrder', 'SC' => 'SalesCredit', 'PO' => 'PurchaseOrder', 'PC' => 'PurchaseCredit');
			foreach($duplicatePaymentOrders as $orderType => $allIds){
				$allOrderIds	= $allIds;
				$allOrderIds	= array_unique($allOrderIds);
				$allOrderIds	= array_unique($allOrderIds);
				$paymentAlertBody	.= '<tr>
											<td>'.$nameMapping[$orderType].' ('.count($allOrderIds).')</td><td>'.implode(", ",$allOrderIds).'</td>
										</tr>';
			}
			$paymentAlertBody	.= '</tbody></table>';
		}
		return	$paymentAlertBody;
	}
	public function pendingCogsAlert(){
		$cogsBody			= '';
		$fetchCheckDate		= date('Y-m-d',strtotime('-2 days'));
		$journalSavedDatas	= $this->ci->db->order_by('orderId','desc')->select('journalsId,journalTypeCode,orderId')->group_start()->where("date(`fetchdate`) < ", "date('".$fetchCheckDate."')",false)->or_group_start()->where('fetchdate', NULL)->group_end()->group_end()->get_where('cogs_journal',array('status' => '0'))->result_array();
		if((is_array($alljournalData)) AND (!empty($alljournalData))){
			$cogsBody		.= '<br><b># Pending COGS Journals -:</b><br>';
			$cogsBody		.= '<table  width="100%" border=1 style="border-collapse:collapse"><thead><tr><th>COGS Type</th><th>OrderIds</th></tr></thead><tbody>';
			$allPendingCogs	= array();
			foreach($alljournalData as $alljournalDataTemp){
				$allPendingCogs[$alljournalDataTemp['journalTypeCode']][]	= $alljournalDataTemp['orderId'];
			}
			if((is_array($allPendingCogs)) AND (!empty($allPendingCogs))){
				foreach($allPendingCogs as $cogsType => $allPendingCogsTemp){
					$cogsBody	.= '<tr><td>'.$cogsType.' ('.count($allPendingCogsTemp).')</td><td>'.implode(", ",$allPendingCogsTemp).'</td></tr>';
				}
				$cogsBody	.= '</tbody></table>';
			}
		}
		return $cogsBody;
	}
	public function pendingCogsWOINvoiceAlert(){
		$cogsBody			= '';
		$fetchCheckDate		= date('Y-m-d',strtotime('-2 days'));
		$journalSavedDatas		= $this->ci->db->order_by('orderId','desc')->select('journalsId,journalTypeCode,orderId')->group_start()->where("date(`fetchdate`) < ", "date('".$fetchCheckDate."')",false)->or_group_start()->where('fetchdate', NULL)->group_end()->group_end()->get_where('cogs_woinvoice_journal',array('status' => '0'))->result_array();
		if((is_array($alljournalData)) AND (!empty($alljournalData))){
			$cogsBody		.= '<br><b># Pending COGS Journals -:</b><br>';
			$cogsBody		.= '<table  width="100%" border=1 style="border-collapse:collapse"><thead><tr><th>COGS Type</th><th>OrderIds</th></tr></thead><tbody>';
			$allPendingCogs	= array();
			foreach($journalSavedDatas as $alljournalDataTemp){
				$allPendingCogs[$alljournalDataTemp['journalTypeCode']][]	= $alljournalDataTemp['orderId'];
			}
			if((is_array($allPendingCogs)) AND (!empty($allPendingCogs))){
				foreach($allPendingCogs as $cogsType => $allPendingCogsTemp){
					$cogsBody	.= '<tr><td>'.$cogsType.'('.count($allPendingCogsTemp).')</td><td>'.implode(", ",$allPendingCogsTemp).'</td></tr>';
				}
				$cogsBody	.= '</tbody></table>';
			}
		}
		return $cogsBody;
	}
	public function pendingAdvanceAlert(){
		$pendingbody		= '';
		$fetchCheckDate		= date('Y-m-d',strtotime('-1 days'));
		$advancedSavedDatas	= $this->ci->db->order_by('orderId','desc')->select('paymentId,paymentType,orderId')->group_start()->where("date(`fetchdate`) < ", "date('".$fetchCheckDate."')",false)->or_group_start()->where('fetchdate', NULL)->group_end()->group_end()->get_where('arAdvance',array('status' => '0'))->result_array();
		if((is_array($advancedSavedDatas)) AND (!empty($advancedSavedDatas))){
			$pendingbody		.= '<br><b># Pending AR Advances from more than 1 Day -:</b><br>';
			$pendingbody		.= '<table  width="100%" border=1 style="border-collapse:collapse"><thead><tr><th>Payment Type</th><th>OrderIds</th></tr></thead><tbody>';
			$allPendingAdvance	= array();
			foreach($advancedSavedDatas as $allAdvanceDatass){
				$allPendingAdvance[$allAdvanceDatass['paymentType']][]	= $allAdvanceDatass['orderId'];
			}
			if((is_array($allPendingAdvance)) AND (!empty($allPendingAdvance))){
				foreach($allPendingAdvance as $paymentType => $allPendingAdvancesss){
					$pendingbody	.= '<tr><td>'.$paymentType.' ('.count($allPendingAdvancesss).')</td><td>'.implode(", ",$allPendingAdvancesss).'</td></tr>';
				}
				$pendingbody	.= '</tbody></table>';
			}
		}
		return $pendingbody;
	}
	public function pendingOrders(){
		$consolidationMapping	= $this->ci->db->get_where('mapping_aggregation',array('account2ChannelId <>' => ''))->result_array();
		$consolMappingTempData	= array();
		if((is_array($consolidationMapping)) AND (!empty($consolidationMapping))){
			foreach($consolidationMapping as $consolidationMappingTemp){
				$mappingAcc1		= $consolidationMappingTemp['account1Id'];
				$mappingAcc2		= $consolidationMappingTemp['account2Id'];
				$mappingChannel		= $consolidationMappingTemp['account1ChannelId'];
				$mappingCurrency	= strtolower($consolidationMappingTemp['account1CurrencyId']);
				$consolMappingTempData[$mappingAcc1][$mappingAcc2][$mappingChannel][$mappingCurrency]	= $consolidationMappingTemp;
			}
		}
		
		$fetchCheckDate		= date('Y-m-d',strtotime('-2 days'));
		$PendingBodyData	= array();
		
		$stuckedSO			= $this->db->order_by('orderId','desc')->select('account1Id,account2Id,taxDate,channelId,currency,orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$fetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('sales_order',array('status' => 0))->result_array();
		
		$stuckedSC			= $this->db->order_by('orderId','desc')->select('account1Id,account2Id,taxDate,channelId,currency,orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$fetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('sales_credit_order',array('status' => 0))->result_array();
		
		$stuckedPO			= $this->db->order_by('orderId','desc')->select('orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$fetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('purchase_order',array('status' => 0))->result_array();
		
		$stuckedPC			= $this->db->order_by('orderId','desc')->select('orderId,createdRowData,message,uninvoiced')->group_start()->where("date(`InvoicedTime`) < ","date('".$fetchCheckDate."')",false)->or_group_start()->where('InvoicedTime', NULL)->group_end()->group_end()->get_where('purchase_credit_order',array('status' => 0))->result_array();
		$isPending		= 0;
		if($stuckedSO OR $stuckedSC OR $stuckedPO OR $stuckedPC){
			$AllErroLogs	= array();
			$isPending		= 1;
			$Pendingbody	= '';
			$Pendingbody	.= '<br><b># Pending Order from more than 48 hours -:</b><br>';
			
			$Pendingbody	.= '<table  width="100%" border=1 style="border-collapse:collapse"><thead><tr><th>OrderType</th><th>Message</th><th>OrderIds</th></tr></thead><tbody>';
			
			if($stuckedSO){
				$pendingSO		= array();
				foreach($stuckedSO as $stuckedSOs){
					if((strlen($stuckedSOs['message']) > 0) or ($stuckedSOs['uninvoiced'])){
						if(substr_count(strtolower($stuckedSOs['message']),'customer not sent')){
							$pendingSO['Customer Not Sent'][$stuckedSOs['orderId']]						= $stuckedSOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSOs['message']),'customer company not found')){
							$pendingSO['Customer Company Not Found'][$stuckedSOs['orderId']]			= $stuckedSOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSOs['message']),'missing sku')){
							$pendingSO['Missing SKU'][$stuckedSOs['orderId']]							= $stuckedSOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSOs['message']),'dropshippo')){
							$pendingSO['Dropship PO not sent yet'][$stuckedSOs['orderId']]				= $stuckedSOs['orderId'];
						}
						elseif((substr_count(strtolower($stuckedSOs['message']),'duplicate invoicenumber')) OR (substr_count(strtolower($stuckedSOs['message']),'already exists'))){
							$pendingSO['Duplicate InvoiceNumber Exist In Xero'][$stuckedSOs['orderId']]	= $stuckedSOs['orderId'];
						}
						elseif($stuckedSOs['uninvoiced']){
							$pendingSO['Order Un-invoiced in Brightpearl'][$stuckedSOs['orderId']]		= $stuckedSOs['orderId'];
						}
						else{
							$pendingSO['Others'][$stuckedSOs['orderId']]	= $stuckedSOs['orderId'];
						}
					}
					else{
						$pendingSO['Others'][$stuckedSOs['orderId']]	= $stuckedSOs['orderId'];
					}
					if($stuckedSOs['createdRowData']){
						$createdRowData	= json_decode($stuckedSOs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError']){
							$AllErroLogs['SO'][$stuckedSOs['orderId']]	= $createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError'];
						}
					}
					else{
						$soAccount1Id	= $stuckedSOs['account1Id'];
						$soAccount2Id	= $stuckedSOs['account2Id'];
						$soChannelId	= $stuckedSOs['channelId'];
						$soCurrency		= strtolower($stuckedSOs['currency']);
						$soTaxDate		= date('Ym',strtotime($stuckedSOs['taxDate']));
						if($consolMappingTempData[$soAccount1Id][$soAccount2Id][$soChannelId][$soCurrency]){
							if($consolMappingTempData[$soAccount1Id][$soAccount2Id][$soChannelId][$soCurrency]['consolFrequency'] == 2){
								if(date('Ym') == $soTaxDate){
									$pendingSO['MonthlyConsol'][$stuckedSOs['orderId']]	= $stuckedSOs['orderId'];
									unset($pendingSO['Others'][$stuckedSOs['orderId']]);
								}
							}
						}
					}
				}
				if($AllErroLogs['SO']){
					foreach($AllErroLogs['SO'] as $BpOrderIds => $AllErroLogsData){
						$messageFound	= 0;
						foreach($AllErroLogsData as $ErrMessage){
							if(is_array($ErrMessage)){
								if(substr_count(strtolower($ErrMessage['Message']),'available to sell')){
									$pendingSO['No items available to sell'][]	= $BpOrderIds;
									$messageFound	= 1;
								}
								if(substr_count(strtolower($ErrMessage['Message']),'lock date')){
									$pendingSO['Lock Date Issue'][]				= $BpOrderIds;
									$messageFound	= 1;
								}
							}
							else{
								if(substr_count(strtolower($ErrMessage),'available to sell')){
									$pendingSO['No items available to sell'][]	= $BpOrderIds;
									$messageFound	= 1;
								}
								if(substr_count(strtolower($ErrMessage),'lock date')){
									$pendingSO['Lock Date Issue'][]				= $BpOrderIds;
									$messageFound	= 1;
								}
							}
						}
						if($messageFound){
							unset($AllErroLogs['SO'][$BpOrderIds]);
							unset($pendingSO['Others'][$BpOrderIds]);
						}
					}
				}
				if(isset($AllErroLogs['SO'])){
					if(count($AllErroLogs['SO']) == 0){
						unset($AllErroLogs['SO']);
					}
				}
				if($pendingSO){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>SalesOrder</td></td>';
					if(isset($pendingSO['Customer Not Sent'])){
						$pendingSO['Customer Not Sent']			= array_filter($pendingSO['Customer Not Sent']);
						$pendingSO['Customer Not Sent']			= array_unique($pendingSO['Customer Not Sent']);
						if(count($pendingSO['Customer Not Sent']) > 0){
							$notSet	= 0;
							$Pendingbody	.= '<td>Customer Not Sent('.count($pendingSO['Customer Not Sent']).')</td><td>'.implode(", ",$pendingSO['Customer Not Sent']).'</td></tr>';
						}
					}
					if(isset($pendingSO['Customer Company Not Found'])){
						$pendingSO['Customer Company Not Found']			= array_filter($pendingSO['Customer Company Not Found']);
						$pendingSO['Customer Company Not Found']			= array_unique($pendingSO['Customer Company Not Found']);
						if(count($pendingSO['Customer Company Not Found']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Customer Company Not Found('.count($pendingSO['Customer Company Not Found']).')</td><td>'.implode(", ",$pendingSO['Customer Company Not Found']).'</td></tr>';
						}
					}
					if(isset($pendingSO['Missing SKU'])){
						$pendingSO['Missing SKU']				= array_filter($pendingSO['Missing SKU']);
						$pendingSO['Missing SKU']				= array_unique($pendingSO['Missing SKU']);
						if(count($pendingSO['Missing SKU']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Missing SKU('.count($pendingSO['Missing SKU']).')</td><td>'.implode(", ",$pendingSO['Missing SKU']).'</td></tr>';
						}
					}
					if(isset($pendingSO['Dropship PO not sent yet'])){
						$pendingSO['Dropship PO not sent yet']	= array_filter($pendingSO['Dropship PO not sent yet']);
						$pendingSO['Dropship PO not sent yet']	= array_unique($pendingSO['Dropship PO not sent yet']);
						if(count($pendingSO['Dropship PO not sent yet']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Dropship PO not sent yet('.count($pendingSO['Dropship PO not sent yet']).')</td><td>'.implode(", ",$pendingSO['Dropship PO not sent yet']).'</td></tr>';
						}
					}
					if(isset($pendingSO['Duplicate InvoiceNumber Exist In Xero'])){
						$pendingSO['Duplicate InvoiceNumber Exist In Xero']	= array_filter($pendingSO['Duplicate InvoiceNumber Exist In Xero']);
						$pendingSO['Duplicate InvoiceNumber Exist In Xero']	= array_unique($pendingSO['Duplicate InvoiceNumber Exist In Xero']);
						if(count($pendingSO['Duplicate InvoiceNumber Exist In Xero']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Duplicate InvoiceNumber Exist In Xero('.count($pendingSO['Duplicate InvoiceNumber Exist In Xero']).')</td><td>'.implode(", ",$pendingSO['Duplicate InvoiceNumber Exist In Xero']).'</td></tr>';
						}
					}
					if(isset($pendingSO['Order Un-invoiced in Brightpearl'])){
						$pendingSO['Order Un-invoiced in Brightpearl']	= array_filter($pendingSO['Order Un-invoiced in Brightpearl']);
						$pendingSO['Order Un-invoiced in Brightpearl']	= array_unique($pendingSO['Order Un-invoiced in Brightpearl']);
						if(count($pendingSO['Order Un-invoiced in Brightpearl']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingSO['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingSO['Order Un-invoiced in Brightpearl']).'</td></tr>';
						}
					}
					if(isset($pendingSO['No items available to sell'])){
						$pendingSO['No items available to sell']	= array_filter($pendingSO['No items available to sell']);
						$pendingSO['No items available to sell']	= array_unique($pendingSO['No items available to sell']);
						if(count($pendingSO['No items available to sell']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>No items available to sell('.count($pendingSO['No items available to sell']).')</td><td>'.implode(", ",$pendingSO['No items available to sell']).'</td></tr>';
						}
					}
					if(isset($pendingSO['Lock Date Issue'])){
						$pendingSO['Lock Date Issue']	= array_filter($pendingSO['Lock Date Issue']);
						$pendingSO['Lock Date Issue']	= array_unique($pendingSO['Lock Date Issue']);
						if(count($pendingSO['Lock Date Issue']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Lock Date Issue('.count($pendingSO['Lock Date Issue']).')</td><td>'.implode(", ",$pendingSO['Lock Date Issue']).'</td></tr>';
						}
					}
					if(isset($pendingSO['MonthlyConsol'])){
						$pendingSO['MonthlyConsol']	= array_filter($pendingSO['MonthlyConsol']);
						$pendingSO['MonthlyConsol']	= array_unique($pendingSO['MonthlyConsol']);
						if(count($pendingSO['MonthlyConsol']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Waiting For Monthly Consol('.count($pendingSO['MonthlyConsol']).')</td><td>'.implode(", ",$pendingSO['MonthlyConsol']).'</td></tr>';
						}
					}
					if(isset($pendingSO['Others'])){
						$pendingSO['Others']	= array_filter($pendingSO['Others']);
						$pendingSO['Others']	= array_unique($pendingSO['Others']);
						if(count($pendingSO['Others']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Others('.count($pendingSO['Others']).')</td><td>'.implode(", ",$pendingSO['Others']).'</td></tr>';
						}
					}
				}
			}
			if($stuckedSC){
				$pendingSC	= array();
				foreach($stuckedSC as $stuckedSCs){
					if((strlen($stuckedSCs['message']) > 0) or ($stuckedSCs['uninvoiced'])){
						if(substr_count(strtolower($stuckedSCs['message']),'customer not sent')){
							$pendingSC['Customer Not Sent'][$stuckedSCs['orderId']]							= $stuckedSCs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSCs['message']),'customer company not found')){
							$pendingSC['Customer Company Not Found'][$stuckedSCs['orderId']]				= $stuckedSCs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedSCs['message']),'missing sku')){
							$pendingSC['Missing SKU'][$stuckedSCs['orderId']]								= $stuckedSCs['orderId'];
						}
						elseif((substr_count(strtolower($stuckedSCs['message']),'duplicate creditnotenumber')) OR (substr_count(strtolower($stuckedSCs['message']),'already exists'))){
							$pendingSC['Duplicate CreditNoteNumber Exist In Xero'][$stuckedSCs['orderId']]	= $stuckedSCs['orderId'];
						}
						elseif($stuckedSCs['uninvoiced']){
							$pendingSC['Order Un-invoiced in Brightpearl'][$stuckedSCs['orderId']]			= $stuckedSCs['orderId'];
						}
						else{
							$pendingSC['Others'][$stuckedSCs['orderId']]	= $stuckedSCs['orderId'];	
						}
					}
					else{
						$pendingSC['Others'][$stuckedSCs['orderId']]	= $stuckedSCs['orderId'];	
					}
					if($stuckedSCs['createdRowData']){
						$createdRowData	= json_decode($stuckedSCs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError']){
							$AllErroLogs['SC'][$stuckedSCs['orderId']]	= $createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError'];
						}
					}
					else{
						$scAccount1Id	= $stuckedSCs['account1Id'];
						$scAccount2Id	= $stuckedSCs['account2Id'];
						$scChannelId	= $stuckedSCs['channelId'];
						$scCurrency		= strtolower($stuckedSCs['currency']);
						$scTaxDate		= date('Ym',strtotime($stuckedSCs['taxDate']));
						if($consolMappingTempData[$scAccount1Id][$scAccount2Id][$scChannelId][$scCurrency]){
							if($consolMappingTempData[$scAccount1Id][$scAccount2Id][$scChannelId][$scCurrency]['consolFrequency'] == 2){
								if(date('Ym') == $scTaxDate){
									$pendingSC['MonthlyConsol'][$stuckedSCs['orderId']]	= $stuckedSCs['orderId'];
									unset($pendingSC['Others'][$stuckedSCs['orderId']]);
								}
							}
						}
					}
				}
				if($AllErroLogs['SC']){
					foreach($AllErroLogs['SC'] as $BpOrderIds => $AllErroLogsData){
						$messageFound	= 0;
						foreach($AllErroLogsData as $ErrMessage){
							if(is_array($ErrMessage)){
								if(substr_count(strtolower($ErrMessage['Message']),'lock date')){
									$pendingSC['Lock Date Issue'][]				= $BpOrderIds;
									$messageFound	= 1;
								}
							}
							else{
								if(substr_count(strtolower($ErrMessage),'lock date')){
									$pendingSC['Lock Date Issue'][]				= $BpOrderIds;
									$messageFound	= 1;
								}
							}
						}
						if($messageFound){
							unset($AllErroLogs['SC'][$BpOrderIds]);
							unset($pendingSC['Others'][$BpOrderIds]);
						}
					}
				}
				if(isset($AllErroLogs['SC'])){
					if(count($AllErroLogs['SC']) == 0){
						unset($AllErroLogs['SC']);
					}
				}
				if($pendingSC){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>SalesCredit</td></td>';
					if(isset($pendingSC['Customer Not Sent'])){
						$pendingSC['Customer Not Sent']			= array_filter($pendingSC['Customer Not Sent']);
						$pendingSC['Customer Not Sent']			= array_unique($pendingSC['Customer Not Sent']);
						if(count($pendingSC['Customer Not Sent']) > 0){
							$notSet	= 0;
							$Pendingbody	.= '<td>Customer Not Sent('.count($pendingSC['Customer Not Sent']).')</td><td>'.implode(", ",$pendingSC['Customer Not Sent']).'</td></tr>';
						}
					}
					if(isset($pendingSC['Customer Company Not Found'])){
						$pendingSC['Customer Company Not Found']			= array_filter($pendingSC['Customer Company Not Found']);
						$pendingSC['Customer Company Not Found']			= array_unique($pendingSC['Customer Company Not Found']);
						if(count($pendingSC['Customer Company Not Found']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Customer Company Not Found('.count($pendingSC['Customer Company Not Found']).')</td><td>'.implode(", ",$pendingSC['Customer Company Not Found']).'</td></tr>';
						}
					}
					if(isset($pendingSC['Missing SKU'])){
						$pendingSC['Missing SKU']				= array_filter($pendingSC['Missing SKU']);
						$pendingSC['Missing SKU']				= array_unique($pendingSC['Missing SKU']);
						if(count($pendingSC['Missing SKU']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Missing SKU('.count($pendingSC['Missing SKU']).')</td><td>'.implode(", ",$pendingSC['Missing SKU']).'</td></tr>';
						}
					}
					if(isset($pendingSC['Duplicate CreditNoteNumber Exist In Xero'])){
						$pendingSC['Duplicate CreditNoteNumber Exist In Xero']				= array_filter($pendingSC['Duplicate CreditNoteNumber Exist In Xero']);
						$pendingSC['Duplicate CreditNoteNumber Exist In Xero']				= array_unique($pendingSC['Duplicate CreditNoteNumber Exist In Xero']);
						if(count($pendingSC['Duplicate CreditNoteNumber Exist In Xero']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Duplicate CreditNoteNumber Exist In Xero('.count($pendingSC['Duplicate CreditNoteNumber Exist In Xero']).')</td><td>'.implode(", ",$pendingSC['Duplicate CreditNoteNumber Exist In Xero']).'</td></tr>';
						}
					}
					if(isset($pendingSC['Order Un-invoiced in Brightpearl'])){
						$pendingSC['Order Un-invoiced in Brightpearl']	= array_filter($pendingSC['Order Un-invoiced in Brightpearl']);
						$pendingSC['Order Un-invoiced in Brightpearl']	= array_unique($pendingSC['Order Un-invoiced in Brightpearl']);
						if(count($pendingSC['Order Un-invoiced in Brightpearl']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingSC['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingSC['Order Un-invoiced in Brightpearl']).'</td></tr>';
						}
					}
					if(isset($pendingSC['Lock Date Issue'])){
						$pendingSC['Lock Date Issue']	= array_filter($pendingSC['Lock Date Issue']);
						$pendingSC['Lock Date Issue']	= array_unique($pendingSC['Lock Date Issue']);
						if(count($pendingSC['Lock Date Issue']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Lock Date Issue('.count($pendingSC['Lock Date Issue']).')</td><td>'.implode(", ",$pendingSC['Lock Date Issue']).'</td></tr>';
						}
					}
					if(isset($pendingSC['MonthlyConsol'])){
						$pendingSC['MonthlyConsol']	= array_filter($pendingSC['MonthlyConsol']);
						$pendingSC['MonthlyConsol']	= array_unique($pendingSC['MonthlyConsol']);
						if(count($pendingSC['MonthlyConsol']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Waiting For Monthly Consol('.count($pendingSC['MonthlyConsol']).')</td><td>'.implode(", ",$pendingSC['MonthlyConsol']).'</td></tr>';
						}
					}
					if(isset($pendingSC['Others'])){
						$pendingSC['Others']	= array_filter($pendingSC['Others']);
						$pendingSC['Others']	= array_unique($pendingSC['Others']);
						if(count($pendingSC['Others']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Others('.count($pendingSC['Others']).')</td><td>'.implode(", ",$pendingSC['Others']).'</td></tr>';
						}
					}
				}
			}
			if($stuckedPO){
				$pendingPO	= array();
				foreach($stuckedPO as $stuckedPOs){
					if((strlen($stuckedPOs['message']) > 0) or ($stuckedPOs['uninvoiced'])){
						if(substr_count(strtolower($stuckedPOs['message']),'supplier not sent')){
							$pendingPO['Supplier Not Sent'][$stuckedPOs['orderId']]					= $stuckedPOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPOs['message']),'supplier company not found')){
							$pendingPO['Supplier Company Not Found'][$stuckedPOs['orderId']]		= $stuckedPOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPOs['message']),'missing sku')){
							$pendingPO['Missing SKU'][$stuckedPOs['orderId']]						= $stuckedPOs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPOs['message']),'parent so is not fetched yet')){
							$pendingPO['Parent SO is Not fetched yet'][$stuckedPOs['orderId']]		= $stuckedPOs['orderId'];
						}
						elseif($stuckedPOs['uninvoiced']){
							$pendingPO['Order Un-invoiced in Brightpearl'][$stuckedPOs['orderId']]	= $stuckedPOs['orderId'];
						}
						else{
							$pendingPO['Others'][$stuckedPOs['orderId']]	= $stuckedPOs['orderId'];	
						}
					}
					else{
						$pendingPO['Others'][$stuckedPOs['orderId']]	= $stuckedPOs['orderId'];	
					}
					if($stuckedPOs['createdRowData']){
						$createdRowData	= json_decode($stuckedPOs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError']){
							$AllErroLogs['PO'][$stuckedPOs['orderId']]	= $createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError'];
						}
					}
				}
				if($AllErroLogs['PO']){
					foreach($AllErroLogs['PO'] as $BpOrderIds => $AllErroLogsData){
						$messageFound	= 0;
						foreach($AllErroLogsData as $ErrMessage){
							if(is_array($ErrMessage)){
								if(substr_count(strtolower($ErrMessage['Message']),'lock date')){
									$pendingPO['Lock Date Issue'][]	= $BpOrderIds;
									$messageFound	= 1;
								}
							}
							else{
								if(substr_count(strtolower($ErrMessage),'lock date')){
									$pendingPO['Lock Date Issue'][]	= $BpOrderIds;
									$messageFound	= 1;
								}
							}
						}
						if($messageFound){
							unset($AllErroLogs['PO'][$BpOrderIds]);
							unset($pendingPO['Others'][$BpOrderIds]);
						}
					}
				}
				if(isset($AllErroLogs['PO'])){
					if(count($AllErroLogs['PO']) == 0){
						unset($AllErroLogs['PO']);
					}
				}
				if($pendingPO){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>PurchaseOrder</td></td>';
					if(isset($pendingPO['Supplier Not Sent'])){
						$pendingPO['Supplier Not Sent']			= array_filter($pendingPO['Supplier Not Sent']);
						$pendingPO['Supplier Not Sent']			= array_unique($pendingPO['Supplier Not Sent']);
						if(isset($pendingPO['Supplier Not Sent'])){
							if(count($pendingPO['Supplier Not Sent']) > 0){
								$notSet	= 0;
								$Pendingbody	.= '<td>Supplier Not Sent('.count($pendingPO['Supplier Not Sent']).')</td><td>'.implode(", ",$pendingPO['Supplier Not Sent']).'</td></tr>';
							}
						}
					}
					if(isset($pendingPO['Supplier Company Not Found'])){
						$pendingPO['Supplier Company Not Found']	= array_filter($pendingPO['Supplier Company Not Found']);
						$pendingPO['Supplier Company Not Found']	= array_unique($pendingPO['Supplier Company Not Found']);
						if(count($pendingPO['Supplier Company Not Found']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Supplier Company Not Found('.count($pendingPO['Supplier Company Not Found']).')</td><td>'.implode(", ",$pendingPO['Supplier Company Not Found']).'</td></tr>';
						}
					}
					if(isset($pendingPO['Missing SKU'])){
						$pendingPO['Missing SKU']				= array_filter($pendingPO['Missing SKU']);
						$pendingPO['Missing SKU']				= array_unique($pendingPO['Missing SKU']);
						if(count($pendingPO['Missing SKU']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Missing SKU('.count($pendingPO['Missing SKU']).')</td><td>'.implode(", ",$pendingPO['Missing SKU']).'</td></tr>';
						}
					}
					if(isset($pendingPO['Order Un-invoiced in Brightpearl'])){
						$pendingPO['Order Un-invoiced in Brightpearl']	= array_filter($pendingPO['Order Un-invoiced in Brightpearl']);
						$pendingPO['Order Un-invoiced in Brightpearl']	= array_unique($pendingPO['Order Un-invoiced in Brightpearl']);
						if(count($pendingPO['Order Un-invoiced in Brightpearl']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingPO['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingPO['Order Un-invoiced in Brightpearl']).'</td></tr>';
						}
					}
					if(isset($pendingPO['Parent SO is Not fetched yet'])){
						$pendingPO['Parent SO is Not fetched yet']		= array_filter($pendingPO['Parent SO is Not fetched yet']);
						$pendingPO['Parent SO is Not fetched yet']		= array_unique($pendingPO['Parent SO is Not fetched yet']);
						if(count($pendingPO['Parent SO is Not fetched yet']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Parent SO is Not fetched yet('.count($pendingPO['Parent SO is Not fetched yet']).')</td><td>'.implode(", ",$pendingPO['Parent SO is Not fetched yet']).'</td></tr>';
						}
					}
					if(isset($pendingPO['Lock Date Issue'])){
						$pendingPO['Lock Date Issue']	= array_filter($pendingPO['Lock Date Issue']);
						$pendingPO['Lock Date Issue']	= array_unique($pendingPO['Lock Date Issue']);
						if(count($pendingPO['Lock Date Issue']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Lock Date Issue('.count($pendingPO['Lock Date Issue']).')</td><td>'.implode(", ",$pendingPO['Lock Date Issue']).'</td></tr>';
						}
					}
					if(isset($pendingPO['Others'])){
						$pendingPO['Others']	= array_filter($pendingPO['Others']);
						$pendingPO['Others']	= array_unique($pendingPO['Others']);
						if(count($pendingPO['Others']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Others('.count($pendingPO['Others']).')</td><td>'.implode(", ",$pendingPO['Others']).'</td></tr>';
						}
					}
				}
			}
			if($stuckedPC){
				$pendingPC	= array();
				foreach($stuckedPC as $stuckedPCs){
					if((strlen($stuckedPCs['message']) > 0) or ($stuckedPCs['uninvoiced'])){
						if(substr_count(strtolower($stuckedPCs['message']),'supplier not sent')){
							$pendingPC['Supplier Not Sent'][$stuckedPCs['orderId']]					= $stuckedPCs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPCs['message']),'supplier company not found')){
							$pendingPC['Supplier Company Not Found'][$stuckedPCs['orderId']]		= $stuckedPCs['orderId'];
						}
						elseif(substr_count(strtolower($stuckedPCs['message']),'missing sku')){
							$pendingPC['Missing SKU'][$stuckedPCs['orderId']]						= $stuckedPCs['orderId'];
						}
						elseif($stuckedPCs['uninvoiced']){
							$pendingPC['Order Un-invoiced in Brightpearl'][$stuckedPCs['orderId']]	= $stuckedPCs['orderId'];
						}
						else{
							$pendingPC['Others'][$stuckedPCs['orderId']]	= $stuckedPCs['orderId'];	
						}
					}
					else{
						$pendingPC['Others'][$stuckedPCs['orderId']]	= $stuckedPCs['orderId'];	
					}
					if($stuckedPCs['createdRowData']){
						$createdRowData	= json_decode($stuckedPCs['createdRowData'],true);
						if($createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError']){
							$AllErroLogs['PC'][$stuckedPCs['orderId']]	= $createdRowData['Response data	: ']['Elements']['DataContractBase']['ValidationErrors']['ValidationError'];
						}
					}
				}
				if($AllErroLogs['PC']){
					foreach($AllErroLogs['PC'] as $BpOrderIds => $AllErroLogsData){
						$messageFound	= 0;
						foreach($AllErroLogsData as $ErrMessage){
							if(is_array($ErrMessage)){
								if(substr_count(strtolower($ErrMessage['Message']),'lock date')){
									$pendingPC['Lock Date Issue'][]	= $BpOrderIds;
									$messageFound	= 1;
								}
							}
							else{
								if(substr_count(strtolower($ErrMessage),'lock date')){
									$pendingPC['Lock Date Issue'][]	= $BpOrderIds;
									$messageFound	= 1;
								}
							}
						}
						if($messageFound){
							unset($AllErroLogs['PC'][$BpOrderIds]);
							unset($pendingPC['Others'][$BpOrderIds]);
						}
					}
				}
				if(isset($AllErroLogs['PC'])){
					if(count($AllErroLogs['PC']) == 0){
						unset($AllErroLogs['PC']);
					}
				}
				if($pendingPC){
					$notSet	= 1;
					$Pendingbody	.= '<tr><td>PurchaseCredit</td></td>';
					if(isset($pendingPC['Supplier Not Sent'])){
						$pendingPC['Supplier Not Sent']			= array_filter($pendingPC['Supplier Not Sent']);
						$pendingPC['Supplier Not Sent']			= array_unique($pendingPC['Supplier Not Sent']);
						if(count($pendingPC['Supplier Not Sent']) > 0){
							$notSet	= 0;
							$Pendingbody	.= '<td>Supplier Not Sent('.count($pendingPC['Supplier Not Sent']).')</td><td>'.implode(", ",$pendingPC['Supplier Not Sent']).'</td></tr>';
						}
					}
					if(isset($pendingPC['Supplier Company Not Found'])){
						$pendingPC['Supplier Company Not Found']	= array_filter($pendingPC['Supplier Company Not Found']);
						$pendingPC['Supplier Company Not Found']	= array_unique($pendingPC['Supplier Company Not Found']);
						if(count($pendingPC['Supplier Company Not Found']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Supplier Company Not Found('.count($pendingPC['Supplier Company Not Found']).')</td><td>'.implode(", ",$pendingPC['Supplier Company Not Found']).'</td></tr>';
						}
					}
					if(isset($pendingPC['Missing SKU'])){
						$pendingPC['Missing SKU']				= array_filter($pendingPC['Missing SKU']);
						$pendingPC['Missing SKU']				= array_unique($pendingPC['Missing SKU']);
						if(count($pendingPC['Missing SKU']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Missing SKU('.count($pendingPC['Missing SKU']).')</td><td>'.implode(", ",$pendingPC['Missing SKU']).'</td></tr>';
						}
					}
					if(isset($pendingPC['Order Un-invoiced in Brightpearl'])){
						$pendingPC['Order Un-invoiced in Brightpearl']	= array_filter($pendingPC['Order Un-invoiced in Brightpearl']);
						$pendingPC['Order Un-invoiced in Brightpearl']	= array_unique($pendingPC['Order Un-invoiced in Brightpearl']);
						if(count($pendingPC['Order Un-invoiced in Brightpearl']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Order Un-invoiced in Brightpearl('.count($pendingPC['Order Un-invoiced in Brightpearl']).')</td><td>'.implode(", ",$pendingPC['Order Un-invoiced in Brightpearl']).'</td></tr>';
						}
					}
					if(isset($pendingPC['Lock Date Issue'])){
						$pendingPC['Lock Date Issue']	= array_filter($pendingPC['Lock Date Issue']);
						$pendingPC['Lock Date Issue']	= array_unique($pendingPC['Lock Date Issue']);
						if(count($pendingPC['Lock Date Issue']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Lock Date Issue('.count($pendingPC['Lock Date Issue']).')</td><td>'.implode(", ",$pendingPC['Lock Date Issue']).'</td></tr>';
						}
					}
					if(isset($pendingPC['Others'])){
						$pendingPC['Others']	= array_filter($pendingPC['Others']);
						$pendingPC['Others']	= array_unique($pendingPC['Others']);
						if(count($pendingPC['Others']) > 0){
							if(!$notSet){
								$Pendingbody	.= '<tr><td></td>';
							}
							$notSet	= 0;
							$Pendingbody	.= '<td>Others('.count($pendingPC['Others']).')</td><td>'.implode(", ",$pendingPC['Others']).'</td></tr>';
						}
					}
				}
			}
			$Pendingbody	.= '</tbody></table>';
		}
		if($isPending){
			$PendingBodyData['mailBody']		= $Pendingbody;
			if($AllErroLogs){
				$PendingBodyData['ErrorLogs']	= $AllErroLogs;
			}
			return	$PendingBodyData;
		}
	}
	public function pendingPaymentAlert(){
		$disableSOpaymentbptoqbo	= $this->globalConfig['disableSOpaymentbptoqbo'];
		$disableSOpaymentqbotobp	= $this->globalConfig['disableSOpaymentqbotobp'];
		$disableSCpaymentbptoqbo	= $this->globalConfig['disableSCpaymentbptoqbo'];
		$disableSCpaymentqbotobp	= $this->globalConfig['disableSCpaymentqbotobp'];
		$disableConsolSOpayments	= $this->globalConfig['disableConsolSOpayments'];
		$disableConsolSCpayments	= $this->globalConfig['disableConsolSCpayments'];
		$disablePOpaymentqbotobp	= $this->globalConfig['disablePOpaymentqbotobp'];
		$disablePCpaymentqbotobp	= $this->globalConfig['disablePCpaymentqbotobp'];
		
		$ConsolidationMapping		= $this->ci->db->get_where('mapping_aggregation',array('account2ChannelId <>' => ''))->result_array();
		if($ConsolidationMapping){
			$ConsolMappingTempData	= array();
			foreach($ConsolidationMapping as $ConsolidationMappingTemp){
				$mappingAcc1		= $ConsolidationMappingTemp['account1Id'];
				$mappingAcc2		= $ConsolidationMappingTemp['account2Id'];
				$mappingChannel		= $ConsolidationMappingTemp['account1ChannelId'];
				$mappingCurrency	= strtolower($ConsolidationMappingTemp['account1CurrencyId']);
				$ConsolMappingTempData[$mappingAcc1][$mappingAcc2][$mappingChannel][$mappingCurrency]	= $ConsolidationMappingTemp;
			}
		}
		$salesOrdersDatas		= array();
		$salesCreditDatas		= array();
		$purchaseOrderDatas		= array();
		$purchaseCreditDatas	= array();
		if(!$disableSOpaymentbptoqbo OR !$disableSOpaymentqbotobp){
			$salesOrdersDatas		= $this->ci->db->order_by('orderId','desc')->select('status, aggregationId, sendInAggregation, channelId, currency, orderId, createOrderId, paymentDetails, account1Id, account2Id')->get_where('sales_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		}
		
		if(!$disableSCpaymentbptoqbo OR !$disableSCpaymentqbotobp){
			$salesCreditDatas		= $this->ci->db->order_by('orderId','desc')->select('status, aggregationId, sendInAggregation, channelId, currency, orderId, createOrderId, paymentDetails, account1Id, account2Id')->get_where('sales_credit_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		}
		
		if(!$disablePOpaymentqbotobp){
			$purchaseOrderDatas		= $this->ci->db->order_by('orderId','desc')->select('status, orderId, createOrderId, paymentDetails, account1Id, account2Id')->get_where('purchase_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		}
		
		if(!$disablePCpaymentqbotobp){
			$purchaseCreditDatas	= $this->ci->db->order_by('orderId','desc')->select('status, orderId, createOrderId, paymentDetails, account1Id, account2Id')->get_where('purchase_credit_order',array('status <>' => '0', 'paymentDetails <>' => '','createOrderId <>' => ''))->result_array();
		}
			
		$pendingPaymentSalesOrderIds		= array();
		$pendingPaymentSalesCreditIds		= array();
		$pendingPaymentPurchaseOrderIds		= array();
		$pendingPaymentPurchaseCreditIds	= array();
		$TotalPendingpayments				= 0;
		$allUnpaidConsolOrders				= array();
		
		if($salesOrdersDatas){
			$processedXeroIdRes	= array();
			foreach($salesOrdersDatas as $salesOrdersData){
				if($salesOrdersData['status'] == 4){
					continue;
				}
				$paymentDetails	= json_decode($salesOrdersData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if($key != 'amazonFeeinfo'){
						if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
							$isPending	= 1;
							break;
						}
					}
				}
				if($isPending){
					$account1Id	= $salesOrdersData['account1Id'];
					$account2Id	= $salesOrdersData['account2Id'];
					$channelId	= $salesOrdersData['channelId'];
					$currency	= strtolower($salesOrdersData['currency']);
					
					$this->xero->reInitialize();
					$this->brightpearl->reInitialize();
					$xeroID				= $salesOrdersData['createOrderId'];
					$BrightpearlID		= $salesOrdersData['orderId'];
					$updateArray		= array();
					$XeroResult			= array();
					if(!$xeroID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					
					if($processedXeroIdRes[$xeroID]){
						$XeroResult	= $processedXeroIdRes[$xeroID];
					}
					else{
						$Xerourl						= '2.0/Invoices/'.$xeroID;
						$this->xero->initializeConfig($account2Id, 'GET', $Xerourl);
						$XeroResult						= $this->xero->getCurl($Xerourl, "GET", '', 'json', $account2Id)[$account2Id];
						$processedXeroIdRes[$xeroID]	= $XeroResult;
					}
					
					if($XeroResult['Invoices']['Invoice']['InvoiceID']){
						if(abs($XeroResult['Invoices']['Invoice']['AmountDue']) > 0){
							$pendingPaymentSalesOrderIds[]	= $salesOrdersData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['IsReversal']){
										$paymentDetails[$pkey]['DeletedonBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('sales_order',$updateArray);
							}
							else{
								$pendingPaymentSalesOrderIds[]	= $salesOrdersData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		if($salesCreditDatas){
			$processedXeroIdRes	= array();
			foreach($salesCreditDatas as $salesCreditData){
				if($salesCreditData['status'] == 4){
					continue;
				}
				$paymentDetails	= json_decode($salesCreditData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
						$isPending	= 1;
						break;
					}
				}
				if($isPending){
					$this->xero->reInitialize();
					$this->brightpearl->reInitialize();
					$xeroID				= $salesCreditData['createOrderId'];
					$BrightpearlID		= $salesCreditData['orderId'];
					$account1Id			= $salesCreditData['account1Id'];
					$account2Id			= $salesCreditData['account2Id'];
					$updateArray		= array();
					$XeroResult			= array();
					if(!$xeroID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					
					if($processedXeroIdRes[$xeroID]){
						$XeroResult	= $processedXeroIdRes[$xeroID];
					}
					else{
						$Xerourl						= '2.0/CreditNotes/'.$xeroID;
						$this->xero->initializeConfig($account2Id, 'GET', $Xerourl);
						$XeroResult						= $this->xero->getCurl($Xerourl, "GET", '', 'json', $account2Id)[$account2Id];
						$processedXeroIdRes[$xeroID]	= $XeroResult;
					}
					
					$Xerourl	= '2.0/CreditNotes/'.$xeroID;
					$this->xero->initializeConfig($account2Id, 'GET', $Xerourl);
					$XeroResult	= $this->xero->getCurl($Xerourl, "GET", '', 'json', $account2Id)[$account2Id];
					if($XeroResult['CreditNotes']['CreditNote']['CreditNoteID']){
						if(abs($XeroResult['CreditNotes']['CreditNote']['RemainingCredit']) > 0){
							$pendingPaymentSalesCreditIds[]	= $salesCreditData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['IsReversal']){
										$paymentDetails[$pkey]['DeletedonBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('sales_credit_order',$updateArray);
							}
							else{
								$pendingPaymentSalesCreditIds[]	= $salesCreditData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		if($purchaseOrderDatas){
			foreach($purchaseOrderDatas as $purchaseOrderData){
				if($purchaseOrderData['status'] == 4){
					continue;
				}
				$paymentDetails	= json_decode($purchaseOrderData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
						$isPending	= 1;
						break;
					}
				}
				if($isPending){
					$this->xero->reInitialize();
					$this->brightpearl->reInitialize();
					$xeroID				= $purchaseOrderData['createOrderId'];
					$BrightpearlID		= $purchaseOrderData['orderId'];
					$account1Id			= $purchaseOrderData['account1Id'];
					$account2Id			= $purchaseOrderData['account2Id'];
					if(!$xeroID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					
					$Xerourl	= '2.0/Invoices/'.$xeroID;
					$this->xero->initializeConfig($account2Id, 'GET', $Xerourl);
					$XeroResult	= $this->xero->getCurl($Xerourl, "GET", '', 'json', $account2Id)[$account2Id];
					if($XeroResult['Invoices']['Invoice']['InvoiceID']){
						if(abs($XeroResult['Invoices']['Invoice']['AmountDue']) > 0){
							$pendingPaymentPurchaseOrderIds[]	= $purchaseOrderData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['IsReversal']){
										$paymentDetails[$pkey]['DeletedonBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('purchase_order',$updateArray);
							}
							else{
								$pendingPaymentPurchaseOrderIds[]	= $purchaseOrderData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		if($purchaseCreditDatas){
			foreach($purchaseCreditDatas as $purchaseCreditData){
				if($purchaseCreditData['status'] == 4){
					continue;
				}
				$paymentDetails	= json_decode($purchaseCreditData['paymentDetails'],true);
				$isPending		= 0;
				foreach($paymentDetails as $key => $paymentDetail){
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['amount'] > 0)){
						$isPending	= 1;
						break;
					}
				}
				if($isPending){
					$this->xero->reInitialize();
					$this->brightpearl->reInitialize();
					$xeroID				= $purchaseCreditData['createOrderId'];
					$BrightpearlID		= $purchaseCreditData['orderId'];
					$account1Id			= $purchaseCreditData['account1Id'];
					$account2Id			= $purchaseCreditData['account2Id'];
					if(!$xeroID){continue;}
					if(!$BrightpearlID){continue;}
					if(!$account1Id){continue;}
					if(!$account2Id){continue;}
					if(!$paymentDetails){continue;}
					$Xerourl	= '2.0/CreditNotes/'.$xeroID;
					$this->xero->initializeConfig($account2Id, 'GET', $Xerourl);
					$XeroResult	= $this->xero->getCurl($Xerourl, "GET", '', 'json', $account2Id)[$account2Id];
					if($XeroResult['CreditNotes']['CreditNote']['CreditNoteID']){
						if(abs($XeroResult['CreditNotes']['CreditNote']['RemainingCredit']) > 0){
							$pendingPaymentPurchaseCreditIds[]	= $purchaseCreditData['orderId'];
							continue;
						}
						else{
							$BPurl			= '/order-service/order/'.$BrightpearlID;
							$BPresponse		= $this->brightpearl->getCurl($BPurl, "GET", '', 'json', $account1Id)[$account1Id];
							if(($BPresponse[0]['orderPaymentStatus'] == 'PAID') OR ($BPresponse[0]['orderPaymentStatus'] == 'NOT_APPLICABLE')){
								foreach($paymentDetails as $pkey => $paymentDetail){
									$paymentDetails[$pkey]['status']	= 1;
									if($paymentDetail['IsReversal']){
										$paymentDetails[$pkey]['DeletedonBrightpearl']	= 'YES';
									}
								}
								$updateArray['paymentDetails']		= json_encode($paymentDetails);
								$updateArray['isPaymentCreated']	= 1;
								$updateArray['status']				= 3;
								$this->db->where(array('orderId' => $BrightpearlID))->update('purchase_credit_order',$updateArray);
							}
							else{
								$pendingPaymentPurchaseCreditIds[]	= $purchaseCreditData['orderId'];
								continue;
							}
						}
					}
				}
			}
		}
		
		$PaymentErrorLogs	= array();
		if($pendingPaymentSalesOrderIds){
			$PendingSalesPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentSalesOrderIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('sales_order',array('orderId <>' => '',))->result_array();
			if($PendingSalesPaymentsOrderData){
				foreach($PendingSalesPaymentsOrderData as $TempNewData){
					$PaymentInfo	= array();
					$SalesOrderId	= $TempNewData['orderId'];
					$createdRowData	= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'xero payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'gift card response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['Elements']['DataContractBase']['ValidationErrors']['ValidationError'])){
							$PaymentErrorLogs['SO'][$SalesOrderId]			= $PaymentInfo['Elements']['DataContractBase']['ValidationErrors']['ValidationError'];
						}
						elseif(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['SO'][$SalesOrderId]	= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		
		if($pendingPaymentSalesCreditIds){
			$PendingSalesCreditPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentSalesCreditIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('sales_credit_order',array('orderId <>' => '',))->result_array();
			if($PendingSalesCreditPaymentsOrderData){
				foreach($PendingSalesCreditPaymentsOrderData as $TempNewData){
					$PaymentInfo		= array();
					$SalesCreditOrderId	= $TempNewData['orderId'];
					$createdRowData		= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'xero payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'gift card response')){
							$PaymentInfo	= $createdRowDatas;
						}
						elseif(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['Elements']['DataContractBase']['ValidationErrors']['ValidationError'])){
							$PaymentErrorLogs['SC'][$SalesCreditOrderId]		= $PaymentInfo['Elements']['DataContractBase']['ValidationErrors']['ValidationError'];
						}
						elseif(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['SC'][$SalesCreditOrderId]		= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		
		if($pendingPaymentPurchaseOrderIds){
			$PendingPurchaseOrderPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentPurchaseOrderIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('purchase_order',array('orderId <>' => '',))->result_array();
			if($PendingPurchaseOrderPaymentsOrderData){
				foreach($PendingPurchaseOrderPaymentsOrderData as $TempNewData){
					$PaymentInfo		= array();
					$PurchaseOrderId	= $TempNewData['orderId'];
					$createdRowData		= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['PO'][$PurchaseOrderId]	= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		
		if($pendingPaymentPurchaseCreditIds){
			$PendingPurchaseCreditPaymentsOrderData	= $this->ci->db->where_in('orderId',$pendingPaymentPurchaseCreditIds)->order_by('orderId','desc')->select('orderId,createdRowData')->get_where('purchase_credit_order',array('orderId <>' => '',))->result_array();
			if($PendingPurchaseCreditPaymentsOrderData){
				foreach($PendingPurchaseCreditPaymentsOrderData as $TempNewData){
					$PaymentInfo			= array();
					$PurchaseCreditOrderId	= $TempNewData['orderId'];
					$createdRowData			= json_decode($TempNewData['createdRowData'],true);
					foreach($createdRowData as $HeadingKey => $createdRowDatas){
						if(substr_count(strtolower($HeadingKey), 'brightpearl payment response')){
							$PaymentInfo	= $createdRowDatas;
						}
						else{
							continue;
						}
					}
					if($PaymentInfo){
						if(isset($PaymentInfo['errors'])){
							$PaymentErrorLogs['PC'][$PurchaseCreditOrderId]	= $PaymentInfo['errors'];
						}
					}
				}
			}
		}
		
		$PendingPaymentbody	= '';
		if($pendingPaymentSalesOrderIds OR $pendingPaymentSalesCreditIds OR $pendingPaymentPurchaseOrderIds OR $pendingPaymentPurchaseCreditIds){
			$PendingPaymentbody	= '<br></br><b># Pending payments report :-</b><br>';
			$PendingPaymentbody	.= '<table border=1><thead><tr><th>OrderType</th><th>OrderIds</th></tr></thead><tbody>';
			if($pendingPaymentSalesOrderIds){
				$PendingPaymentbody	.= '<tr><td>Sales Order('.count($pendingPaymentSalesOrderIds).')</td><td>'.implode(", ",$pendingPaymentSalesOrderIds).'</td></tr>';
			}
			if($pendingPaymentSalesCreditIds){
				$PendingPaymentbody	.= '<tr><td>Sales Credit('.count($pendingPaymentSalesCreditIds).')</td><td>'.implode(", ",$pendingPaymentSalesCreditIds).'</td></tr>';
			}
			if($pendingPaymentPurchaseOrderIds){
				$PendingPaymentbody	.= '<tr><td>Purchase Order('.count($pendingPaymentPurchaseOrderIds).')</td><td>'.implode(", ",$pendingPaymentPurchaseOrderIds).'</td></tr>';
			}
			if($pendingPaymentPurchaseCreditIds){
				$PendingPaymentbody	.= '<tr><td>Purchase Credit('.count($pendingPaymentPurchaseCreditIds).')</td><td>'.implode(", ",$pendingPaymentPurchaseCreditIds).'</td></tr>';
			}
			$PendingPaymentbody	.= '</tbody></table>';
		}
		$PaymentsMailBody	= array();
		if($PendingPaymentbody){
			$PaymentsMailBody['OrderIds']				= $PendingPaymentbody;
			if($PaymentErrorLogs){
				$PaymentsMailBody['PaymentsErrorLogs']	= $PaymentErrorLogs;
			}
			return	$PaymentsMailBody;
		}
	}
}