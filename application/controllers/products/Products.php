<?php
//xero products controller
if(!defined('BASEPATH')){
	exit('No direct script access allowed');
}
#[\AllowDynamicProperties]
class Products extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('products/products_model','',TRUE);
	}
	public function	index(){
		$data		= array();
		$this->template->load_template("products/products",$data);
	}
	public function	getProduct(){
		$records	= $this->products_model->getProduct();
		echo json_encode($records);
	}
	public function	fetchProducts($productId = '',$fetchedFromBp = '1',$fetchedFromVend = '0'){ 
		$this->products_model->fetchProducts($productId,$fetchedFromBp,$fetchedFromVend);  
	}
	public function postProducts($productId = '',$postedAccount2Id = ''){
		$this->products_model->postProducts($productId,$postedAccount2Id);
	}
	public function productInfo($productId = ''){
		$data['productInfo']	= $this->db->get_where('products', array('productId' => $productId))->row_array();
		$this->template->load_template("products/productInfo",$data);
	}
	public function getVarient(){
		$newSku	= $this->input->post('newSku');
		$color	= $this->input->post('color');
		$datas	= $this->db->select('sku,name,ean,upc,productId,color,size')->get_where('products',array('newSku' => $newSku,'color' => $color))->result_array();
		$str	= '<table class ="table" ><thead> <tr><th>Product Id</th><th>Name</th><th>SKU</th><th>Color</th><th>Size</th></tr></thead><tbody>';
		foreach($datas as $data){
			$str	.=	'<tr><td>'.$data['productId'].'</td><td>'.$data['name'].'</td><td>'.$data['sku'].'</td><td>'.$data['color'].'</td><td>'.$data['size'].'</td></tr>';
		}
		$str	.=	'</tbody></table>';
		echo $str;
	}
	public function exportProductsCSV(){
		error_reporting('0');
		$where				= array();
		$query				= $this->db;
		
		if (trim($this->input->get('account1Id'))) {
			$where['account1Id']		= trim($this->input->get('account1Id'));
		}
		if (trim($this->input->get('account2Id'))) {
			$where['account2Id']		= trim($this->input->get('account2Id'));
		}
		if (trim($this->input->get('productId'))) {
			$where['productId']			= trim($this->input->get('productId'));
		}
		if (trim($this->input->get('createdProductId'))) {
			$where['createdProductId']	= trim($this->input->get('createdProductId'));
		}
		if (trim($this->input->get('sku'))) {
			$where['sku']				= trim($this->input->get('sku'));
		}
		if (trim($this->input->get('color'))) {
			$where['color']				= trim($this->input->get('color'));
		}
		if (trim($this->input->get('size'))) {
			$where['size']				= trim($this->input->get('size'));
		}
		if (trim($this->input->get('name'))) {
			$where['name']				= trim($this->input->get('name'));
		}
		if($this->input->get('status')){
			if (trim($this->input->get('status')) >= 0) {
				$where['status']				= trim($this->input->get('status'));
			}
		}
		if (trim($this->input->get('updated_from'))) {
			$query->where('date(updated) >= ', "date('" . $this->input->get('updated_from') . "')", false);
		}
		if (trim($this->input->get('updated_to'))) {
			$query->where('date(updated) <= ', "date('" . $this->input->get('updated_to') . "')", false);
		}
		if ($where) {
			$query->like($where);
		}
		
		
		$getColumns	= 'id,account1Id,account2Id,productId,createdProductId,sku,color,size,name,status,updated,created,barcode,ean,upc,params';
		$datas		= $query->select($getColumns)->get_where('products',array('productId <>' => ''))->result_array();
		
		$account1Mappings		= array();
		$account2Mappings		= array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}		
		$csvName	= 'BP_Products_'.date('Ymd').".csv";
		$fp			= fopen('php://output', 'w');
		$header		= array('BRIGHTPEARL_ACCOUNT','XERO_ACCOUNT','BRIGHTPEARL_ID','XERO_ID','NAME','SKU','EAN','BARCODE','UPC','COLOR','SIZE','BRIGHTPEARL_STATUS','STOCK_TRACKED','CONNECTOR_STATUS','BRIGHTPEARL_CREATED','BRIGHTPEARL_UPDATED');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$csvName);
		fputcsv($fp, $header);
		$ItemStatus	= array('0' => 'Pending','1' => 'Sent','2' => 'Updated','3' => 'Error','4' => 'Archive',);
		foreach($datas as $data){
			$params		= json_decode($data['params'],true);
			$IsTracked	= 0;
			$IsTracked	= $params['stock']['stockTracked'];
			$bpStatus	= $params['status'];
			$row		= array(
				$account1Mappings[$data['account1Id']]['name'],
				$account2Mappings[$data['account2Id']]['name'], 
				$data['productId'],
				$data['createdProductId'],
				$data['name'],
				$data['sku'],
				$data['ean'],
				$data['barcode'],
				$data['upc'],
				$data['color'],
				$data['size'],
				$bpStatus,
				$IsTracked,
				$ItemStatus[$data['status']],
				date('Y-m-d H:i:s',strtotime($data['created'])),
				date('Y-m-d H:i:s',strtotime($data['updated'])),
			);
			fputcsv($fp, $row);
		}
	}
	
	
	
	// NOT FOR USE ON XERO/QBO
	public function loadProduct(){
		$this->template->load_template("products/loadProduct");  
	}
	public function getLoadProduct(){
		$records	= $this->products_model->getLoadProduct();
		echo json_encode($records);
	}
	public function postLoadedProduct(){
		$records	= $this->products_model->postLoadedProduct();
		$this->getLoadProduct();
	}
	public function clearProducts(){
		$this->db->where('status < ',2)->delete('product_load');
		$this->getLoadProduct();
	}
	public function uploadProduct(){
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){			
			$account2Mappings[strtolower(trim($account2MappingTemp['name']))] = $account2MappingTemp;
		}
		$account2Id				= @$_REQUEST['account2Id'];
		$account1Id				= @$_REQUEST['account1Id'];
		error_reporting('0');
		$batchUpdate	= array();
		if($_FILES['uploadprefile']['tmp_name']) {
			$file	= fopen($_FILES['uploadprefile']['tmp_name'], "r");
			$count	= 0;
			while (!feof($file)) {
				$count++;
                $row	= fgetcsv($file);
				if($count <= 1){
					continue;
				}
				if($row['1']){
					$companyName = @strtolower(trim($row['4']));
					if($companyName){
						if(isset($account2Mappings[$companyName]))
						$batchUpdate[strtolower($row['0'])] = array(
							'sku' 			=> $row['0'],
							'qty' 			=> $row['1'],
							'unitPrice' 	=> $row['2'],
							'availableDate' => $row['3'],
							'account2Id' 	=> $account2Mappings[$companyName]['id'],
							'status' 		=> '0',
						);
					}
                }
			}
			fclose($file);
		}
		if($batchUpdate){
			$skus	= array_keys($batchUpdate);
			$skus	= array_filter($skus);
			sort($skus);
			if($skus){
				$this->db->where_in('sku',$skus)->delete('product_load');
			}
			$batchUpdates	= array_chunk($batchUpdate,200);
			foreach($batchUpdates as $batchUpdate){
				if($batchUpdate['0']){
					$this->db->insert_batch('product_load',$batchUpdate);
				}
			}
		}
		redirect($_SERVER['HTTP_REFERER'] , 'refresh');
	}
	
}