<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Brightpearlapi extends MY_Controller {
	public function __construct(){
		parent::__construct();
	}
	public function apiinfo($entityId = ''){
		$body	=	'<table  width="100%" border=1 style="border-collapse:collapse">
						<thead>
							<tr>
								<th>Method</th>
								<th>Description</th>
								<th>Params</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>getaccountingperiod</td>
								<td>Fetch accounting period information</td>
								<td>ID SET (not required)</td>
							</tr>
							<tr>
								<td>getaccountingperiodlock</td>
								<td>Fetch accounting period lock information</td>
								<td></td>
							</tr>
							<tr>
								<td>searchalltransactionstatement</td>
								<td>Fetch accounting period lock information</td>
								<td></td>
							</tr>
						</tbody>
					</table>';
		echo "<pre>";print_r($body); echo "</pre>";
	}
	public function getaccountingperiod($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		if($entityId){
			$url	= '/accounting-service/accounting-period/'.$entityId;
		}
		else{
			$url	= '/accounting-service/accounting-period';
		}
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id);
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getaccountingperiodlock($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/accounting-period-lock';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id);
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchalltransactionstatement($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/statement/all-transaction-search';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id);
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchcurrency($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/currency-search';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		if($results['results']){
			$header			= array_column($results['metaData']['columns'],"name");
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($header,$result);
				echo "<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function searchcustomerpayment($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/accounting-service/customer-payment-search?orderId='.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function deletecustomerpayment($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/accounting-service/customer-payment/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'delete', '', 'json', $account1Id)[1];
			echo "<pre>";print_r($results); echo "</pre>";
		}
	}
	public function searchsupplierpayment($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/accounting-service/supplier-payment-search?orderId='.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function deletesupplierpayment($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/accounting-service/supplier-payment/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'delete', '', 'json', $account1Id)[1];
			echo "<pre>";print_r($results); echo "</pre>";
		}
	}
	public function getexchangerate($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/exchange-rate';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchjournal($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('orderId'))){
			$searchParameters['orderId']		= trim($this->input->get('orderId'));
		}
		if(trim($this->input->get('journalId'))){
			$searchParameters['journalId']		= trim($this->input->get('journalId'));
		}
		if(trim($this->input->get('journalType'))){
			$searchParameters['journalType']	= trim($this->input->get('journalType'));
		}
		if($searchParameters){
			$added	= 0;
			$url	= '/accounting-service/journal-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getjournal($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/accounting-service/journal/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchnominalcode($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/nominal-code-search';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		if($results['results']){
			$header			= array_column($results['metaData']['columns'],"name");
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($header,$result);
				echo "<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getnominalcode($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/nominal-code/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchpaymentmethod($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/payment-method-search';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		if($results['results']){
			$header			= array_column($results['metaData']['columns'],"name");
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($header,$result);
				echo "<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getpaymentmethod($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/payment-method/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getpurchasepaymenttotal($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/accounting-service/purchase-payment-total/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getsalepaymenttotal($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/accounting-service/sale-payment-total/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function gettaxcode($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/accounting-service/tax-code/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchcompany($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('companyId'))){
			$searchParameters['companyId']	= trim($this->input->get('companyId'));
		}
		if(trim($this->input->get('name'))){
			$searchParameters['name']		= trim($this->input->get('name'));
		}
		if($searchParameters){
			$added	= 0;
			$url	= '/contact-service/company-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/contact-service/company-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getcompany($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/contact-service/company/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchcontact($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('mob'))){$searchParameters['mob']								= trim($this->input->get('mob'));}
		if(trim($this->input->get('isStaff'))){$searchParameters['isStaff']						= trim($this->input->get('isStaff'));}
		if(trim($this->input->get('lastName'))){$searchParameters['lastName']					= trim($this->input->get('lastName'));}
		if(trim($this->input->get('firstName'))){$searchParameters['firstName']					= trim($this->input->get('firstName'));}
		if(trim($this->input->get('contactId'))){$searchParameters['contactId']					= trim($this->input->get('contactId'));}
		if(trim($this->input->get('createdOn'))){$searchParameters['createdOn']					= trim($this->input->get('createdOn'));}
		if(trim($this->input->get('updatedOn'))){$searchParameters['updatedOn']					= trim($this->input->get('updatedOn'));}
		if(trim($this->input->get('isPrimary'))){$searchParameters['isPrimary']					= trim($this->input->get('isPrimary'));}
		if(trim($this->input->get('isSupplier'))){$searchParameters['isSupplier']				= trim($this->input->get('isSupplier'));}
		if(trim($this->input->get('isCustomer'))){$searchParameters['isCustomer']				= trim($this->input->get('isCustomer'));}
		if(trim($this->input->get('nominalCode'))){$searchParameters['nominalCode']				= trim($this->input->get('nominalCode'));}
		if(trim($this->input->get('companyName'))){$searchParameters['companyName']				= trim($this->input->get('companyName'));}
		if(trim($this->input->get('primaryEmail'))){$searchParameters['primaryEmail']			= trim($this->input->get('primaryEmail'));}
		if(trim($this->input->get('tertiaryEmail'))){$searchParameters['tertiaryEmail']			= trim($this->input->get('tertiaryEmail'));}
		if(trim($this->input->get('secondaryEmail'))){$searchParameters['secondaryEmail']		= trim($this->input->get('secondaryEmail'));}
		if(trim($this->input->get('exactCompanyName'))){$searchParameters['exactCompanyName']	= trim($this->input->get('exactCompanyName'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/contact-service/contact-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/contact-service/contact-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getcontact($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/contact-service/contact/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchcontactgroup($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('name'))){$searchParameters['name']						= trim($this->input->get('name'));}
		if(trim($this->input->get('description'))){$searchParameters['description']			= trim($this->input->get('description'));}
		if(trim($this->input->get('contactGroupId'))){$searchParameters['contactGroupId']	= trim($this->input->get('contactGroupId'));}
		if(trim($this->input->get('numberOfMembers'))){$searchParameters['numberOfMembers']	= trim($this->input->get('numberOfMembers'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/contact-service/contact-group-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/contact-service/contact-group-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getcontactgroup($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/contact-service/contact-group/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchcontactgroupmember($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('lastName'))){$searchParameters['lastName']								= trim($this->input->get('lastName'));}
		if(trim($this->input->get('firstName'))){$searchParameters['firstName']								= trim($this->input->get('firstName'));}
		if(trim($this->input->get('contactId'))){$searchParameters['contactId']								= trim($this->input->get('contactId'));}
		if(trim($this->input->get('contactGroupId'))){$searchParameters['contactGroupId']					= trim($this->input->get('contactGroupId'));}
		if(trim($this->input->get('contactGroupName'))){$searchParameters['contactGroupName']				= trim($this->input->get('contactGroupName'));}
		if(trim($this->input->get('contactGroupDescription'))){$searchParameters['contactGroupDescription']	= trim($this->input->get('contactGroupDescription'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/contact-service/contact-group-member-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/contact-service/contact-group-member-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getcustomfieldcontact($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/contact-service/contact/'.$entityId.'/custom-field';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getcustomfieldmetadatacontact($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/contact-service/customer/custom-field-meta-data/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "Customer_results<pre>";print_r($results); echo "</pre>";
		$url		= '/contact-service/supplier/custom-field-meta-data/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "Supplier_results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getleadsource($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/contact-service/lead-source';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getpostaladdress($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/contact-service/postal-address'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproject($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/contact-service/project';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function gettag($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/contact-service/tag';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getaccountconfiguration($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/integration-service/account-configuration';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getwebhook($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/integration-service/webhook';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getcustomfieldmetadataorder($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/order-service/sale/custom-field-meta-data/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "Sale_results<pre>";print_r($results); echo "</pre>";
		$url		= '/order-service/purchase/custom-field-meta-data/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "Purchase_results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchpurchaseorderlc($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('sku'))){$searchParameters['sku']									= trim($this->input->get('sku'));}
		if(trim($this->input->get('weight'))){$searchParameters['weight']							= trim($this->input->get('weight'));}
		if(trim($this->input->get('volume'))){$searchParameters['volume']							= trim($this->input->get('volume'));}
		if(trim($this->input->get('brandId'))){$searchParameters['brandId']							= trim($this->input->get('brandId'));}
		if(trim($this->input->get('orderId'))){$searchParameters['orderId']							= trim($this->input->get('orderId'));}
		if(trim($this->input->get('quantity'))){$searchParameters['quantity']						= trim($this->input->get('quantity'));}
		if(trim($this->input->get('productId'))){$searchParameters['productId']						= trim($this->input->get('productId'));}
		if(trim($this->input->get('categoryId'))){$searchParameters['categoryId']					= trim($this->input->get('categoryId'));}
		if(trim($this->input->get('orderRowId'))){$searchParameters['orderRowId']					= trim($this->input->get('orderRowId'));}
		if(trim($this->input->get('supplierId'))){$searchParameters['supplierId']					= trim($this->input->get('supplierId'));}
		if(trim($this->input->get('productName'))){$searchParameters['productName']					= trim($this->input->get('productName'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']					= trim($this->input->get('warehouseId'));}
		if(trim($this->input->get('productPrice'))){$searchParameters['productPrice']				= trim($this->input->get('productPrice'));}
		if(trim($this->input->get('supplierName'))){$searchParameters['supplierName']				= trim($this->input->get('supplierName'));}
		if(trim($this->input->get('currentLandedCosts'))){$searchParameters['currentLandedCosts']	= trim($this->input->get('currentLandedCosts'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/order-service/purchase-order-lc-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/order-service/purchase-order-lc-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getlandedcostestimate($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/order-service/order/'.$entityId.'/landedcost-estimate';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchorder($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('orderId'))){$searchParameters['orderId']													= trim($this->input->get('orderId'));}
		if(trim($this->input->get('taxDate'))){$searchParameters['taxDate']													= trim($this->input->get('taxDate'));}
		if(trim($this->input->get('isClone'))){$searchParameters['isClone']													= trim($this->input->get('isClone'));}
		if(trim($this->input->get('isClosed'))){$searchParameters['isClosed']												= trim($this->input->get('isClosed'));}
		if(trim($this->input->get('placedOn'))){$searchParameters['placedOn']												= trim($this->input->get('placedOn'));}
		if(trim($this->input->get('contactId'))){$searchParameters['contactId']												= trim($this->input->get('contactId'));}
		if(trim($this->input->get('createdOn'))){$searchParameters['createdOn']												= trim($this->input->get('createdOn'));}
		if(trim($this->input->get('updatedOn'))){$searchParameters['updatedOn']												= trim($this->input->get('updatedOn'));}
		if(trim($this->input->get('projectId'))){$searchParameters['projectId']												= trim($this->input->get('projectId'));}
		if(trim($this->input->get('orderState'))){$searchParameters['orderState']											= trim($this->input->get('orderState'));}
		if(trim($this->input->get('orderTypeId'))){$searchParameters['orderTypeId']											= trim($this->input->get('orderTypeId'));}
		if(trim($this->input->get('createdById'))){$searchParameters['createdById']											= trim($this->input->get('createdById'));}
		if(trim($this->input->get('customerRef'))){$searchParameters['customerRef']											= trim($this->input->get('customerRef'));}
		if(trim($this->input->get('externalRef'))){$searchParameters['externalRef']											= trim($this->input->get('externalRef'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']											= trim($this->input->get('warehouseId'));}
		if(trim($this->input->get('departmentId'))){$searchParameters['departmentId']										= trim($this->input->get('departmentId'));}
		if(trim($this->input->get('deliveryDate'))){$searchParameters['deliveryDate']										= trim($this->input->get('deliveryDate'));}
		if(trim($this->input->get('leadSourceId'))){$searchParameters['leadSourceId']										= trim($this->input->get('leadSourceId'));}
		if(trim($this->input->get('externalRefs'))){$searchParameters['externalRefs']										= trim($this->input->get('externalRefs'));}
		if(trim($this->input->get('orderStatusId'))){$searchParameters['orderStatusId']										= trim($this->input->get('orderStatusId'));}
		if(trim($this->input->get('parentOrderId'))){$searchParameters['parentOrderId']										= trim($this->input->get('parentOrderId'));}
		if(trim($this->input->get('channelTypeId'))){$searchParameters['channelTypeId']										= trim($this->input->get('channelTypeId'));}
		if(trim($this->input->get('shippingMethodId'))){$searchParameters['shippingMethodId']								= trim($this->input->get('shippingMethodId'));}
		if(trim($this->input->get('stockAllocationId'))){$searchParameters['stockAllocationId']								= trim($this->input->get('stockAllocationId'));}
		if(trim($this->input->get('isHistoricalOrder'))){$searchParameters['isHistoricalOrder']								= trim($this->input->get('isHistoricalOrder'));}
		if(trim($this->input->get('orderStockStatusId'))){$searchParameters['orderStockStatusId']							= trim($this->input->get('orderStockStatusId'));}
		if(trim($this->input->get('staffOwnerContactId'))){$searchParameters['staffOwnerContactId']							= trim($this->input->get('staffOwnerContactId'));}
		if(trim($this->input->get('orderPaymentStatusId'))){$searchParameters['orderPaymentStatusId']						= trim($this->input->get('orderPaymentStatusId'));}
		if(trim($this->input->get('orderShippingStatusId'))){$searchParameters['orderShippingStatusId']						= trim($this->input->get('orderShippingStatusId'));}
		if(trim($this->input->get('idOrExternalReference'))){$searchParameters['idOrExternalReference']						= trim($this->input->get('idOrExternalReference'));}
		if(trim($this->input->get('externalRefSearchString'))){$searchParameters['externalRefSearchString']					= trim($this->input->get('externalRefSearchString'));}
		if(trim($this->input->get('installedIntegrationInstanceId'))){$searchParameters['installedIntegrationInstanceId']	= trim($this->input->get('installedIntegrationInstanceId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/order-service/order-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/order-service/order-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getorder($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/order-service/order/'.$entityId.'?includeOptional=customFields,nullCustomFields';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getcustomfieldorder($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/order-service/order/'.$entityId.'/custom-field';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchordernote($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('noteId'))){$searchParameters['noteId']				= trim($this->input->get('noteId'));}
		if(trim($this->input->get('orderId'))){$searchParameters['orderId']				= trim($this->input->get('orderId'));}
		if(trim($this->input->get('addedBy'))){$searchParameters['addedBy']				= trim($this->input->get('addedBy'));}
		if(trim($this->input->get('noteText'))){$searchParameters['noteText']			= trim($this->input->get('noteText'));}
		if(trim($this->input->get('isPublic'))){$searchParameters['isPublic']			= trim($this->input->get('isPublic'));}
		if(trim($this->input->get('contactId'))){$searchParameters['contactId']			= trim($this->input->get('contactId'));}
		if(trim($this->input->get('dateAdded'))){$searchParameters['dateAdded']			= trim($this->input->get('dateAdded'));}
		if(trim($this->input->get('orderStatusId'))){$searchParameters['orderStatusId']	= trim($this->input->get('orderStatusId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/order-service/order-note-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/order-service/order-note-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getordernote($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/order-service/order/'.$entityId.'/note/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getordershippingstatus($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/order-service/order-shipping-status';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getorderstatus($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/order-service/order-status';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getorderstockstatus($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/order-service/order-stock-status';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getordertype($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/order-service/order-type';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getsalescredit($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/order-service/sales-credit/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchsalesorder($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('taxDate'))){$searchParameters['taxDate']													= trim($this->input->get('taxDate'));}
		if(trim($this->input->get('createdOn'))){$searchParameters['createdOn']												= trim($this->input->get('createdOn'));}
		if(trim($this->input->get('updatedOn'))){$searchParameters['updatedOn']												= trim($this->input->get('updatedOn'));}
		if(trim($this->input->get('channelId'))){$searchParameters['channelId']												= trim($this->input->get('channelId'));}
		if(trim($this->input->get('customerId'))){$searchParameters['customerId']											= trim($this->input->get('customerId'));}
		if(trim($this->input->get('createdById'))){$searchParameters['createdById']											= trim($this->input->get('createdById'));}
		if(trim($this->input->get('customerRef'))){$searchParameters['customerRef']											= trim($this->input->get('customerRef'));}
		if(trim($this->input->get('externalRef'))){$searchParameters['externalRef']											= trim($this->input->get('externalRef'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']											= trim($this->input->get('warehouseId'));}
		if(trim($this->input->get('salesOrderId'))){$searchParameters['salesOrderId']										= trim($this->input->get('salesOrderId'));}
		if(trim($this->input->get('staffOwnerId'))){$searchParameters['staffOwnerId']										= trim($this->input->get('staffOwnerId'));}
		if(trim($this->input->get('deliveryDate'))){$searchParameters['deliveryDate']										= trim($this->input->get('deliveryDate'));}
		if(trim($this->input->get('orderStatusId'))){$searchParameters['orderStatusId']										= trim($this->input->get('orderStatusId'));}
		if(trim($this->input->get('orderStockStatus'))){$searchParameters['orderStockStatus']								= trim($this->input->get('orderStockStatus'));}
		if(trim($this->input->get('isHistoricalOrder'))){$searchParameters['isHistoricalOrder']								= trim($this->input->get('isHistoricalOrder'));}
		if(trim($this->input->get('orderPaymentStatus'))){$searchParameters['orderPaymentStatus']							= trim($this->input->get('orderPaymentStatus'));}
		if(trim($this->input->get('orderShippingStatus'))){$searchParameters['orderShippingStatus']							= trim($this->input->get('orderShippingStatus'));}
		if(trim($this->input->get('installedIntegrationInstanceId'))){$searchParameters['installedIntegrationInstanceId']	= trim($this->input->get('installedIntegrationInstanceId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/order-service/sales-order-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/order-service/sales-order-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getsalesorder($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/order-service/sales-order/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchbrand($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('brandId'))){$searchParameters['brandId']		= trim($this->input->get('brandId'));}
		if(trim($this->input->get('brandName'))){$searchParameters['brandName']	= trim($this->input->get('brandName'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/product-service/brand-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/product-service/brand-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getbrand($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/brand/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchbrightpearlcategory($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('id'))){$searchParameters['id']					= trim($this->input->get('id'));}
		if(trim($this->input->get('name'))){$searchParameters['name']				= trim($this->input->get('name'));}
		if(trim($this->input->get('active'))){$searchParameters['active']			= trim($this->input->get('active'));}
		if(trim($this->input->get('parentId'))){$searchParameters['parentId']		= trim($this->input->get('parentId'));}
		if(trim($this->input->get('createdOn'))){$searchParameters['createdOn']		= trim($this->input->get('createdOn'));}
		if(trim($this->input->get('updatedOn'))){$searchParameters['updatedOn']		= trim($this->input->get('updatedOn'));}
		if(trim($this->input->get('updatedBy'))){$searchParameters['updatedBy']		= trim($this->input->get('updatedBy'));}
		if(trim($this->input->get('createdById'))){$searchParameters['createdById']	= trim($this->input->get('createdById'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/product-service/brightpearl-category-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/product-service/brightpearl-category-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getbrightpearlcategory($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/brightpearl-category/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getchannel($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/product-service/channel/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getchannelbrand($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/product-service/channel-brand/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchcollection($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('collectionId'))){$searchParameters['collectionId']		= trim($this->input->get('collectionId'));}
		if(trim($this->input->get('collectionName'))){$searchParameters['collectionName']	= trim($this->input->get('collectionName'));}
		if(trim($this->input->get('brandId'))){$searchParameters['brandId']					= trim($this->input->get('brandId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/product-service/collection-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/product-service/collection-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function searchoption($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('id'))){$searchParameters['id']						= trim($this->input->get('id'));}
		if(trim($this->input->get('name'))){$searchParameters['name']					= trim($this->input->get('name'));}
		if(trim($this->input->get('productTypeId'))){$searchParameters['productTypeId']	= trim($this->input->get('productTypeId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/product-service/option-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/product-service/option-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getoption($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/option/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
		else{
			$this->brightpearl->reInitialize();
			$url		= '/product-service/option/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchoptionvalue($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('optionId'))){$searchParameters['optionId']				= trim($this->input->get('optionId'));}
		if(trim($this->input->get('optionName'))){$searchParameters['optionName']			= trim($this->input->get('optionName'));}
		if(trim($this->input->get('optionValueId'))){$searchParameters['optionValueId']		= trim($this->input->get('optionValueId'));}
		if(trim($this->input->get('optionValueName'))){$searchParameters['optionValueName']	= trim($this->input->get('optionValueName'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/product-service/option-value-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/product-service/option-value-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getoptionvalue($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/option/'.$entityId.'/value';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getpricelist($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/price-list/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
		else{
			$this->brightpearl->reInitialize();
			$url		= '/product-service/price-list/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchproduct($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('SKU'))){$searchParameters['SKU']											= trim($this->input->get('SKU'));}
		if(trim($this->input->get('EAN'))){$searchParameters['EAN']											= trim($this->input->get('EAN'));}
		if(trim($this->input->get('UPC'))){$searchParameters['UPC']											= trim($this->input->get('UPC'));}
		if(trim($this->input->get('MPN'))){$searchParameters['MPN']											= trim($this->input->get('MPN'));}
		if(trim($this->input->get('ISBN'))){$searchParameters['ISBN']										= trim($this->input->get('ISBN'));}
		if(trim($this->input->get('barcode'))){$searchParameters['barcode']									= trim($this->input->get('barcode'));}
		if(trim($this->input->get('brandId'))){$searchParameters['brandId']									= trim($this->input->get('brandId'));}
		if(trim($this->input->get('productId'))){$searchParameters['productId']								= trim($this->input->get('productId'));}
		if(trim($this->input->get('createdOn'))){$searchParameters['createdOn']								= trim($this->input->get('createdOn'));}
		if(trim($this->input->get('updatedOn'))){$searchParameters['updatedOn']								= trim($this->input->get('updatedOn'));}
		if(trim($this->input->get('channelId'))){$searchParameters['channelId']								= trim($this->input->get('channelId'));}
		if(trim($this->input->get('productName'))){$searchParameters['productName']							= trim($this->input->get('productName'));}
		if(trim($this->input->get('stockTracked'))){$searchParameters['stockTracked']						= trim($this->input->get('stockTracked'));}
		if(trim($this->input->get('productTypeId'))){$searchParameters['productTypeId']						= trim($this->input->get('productTypeId'));}
		if(trim($this->input->get('productStatus'))){$searchParameters['productStatus']						= trim($this->input->get('productStatus'));}
		if(trim($this->input->get('productGroupId'))){$searchParameters['productGroupId']					= trim($this->input->get('productGroupId'));}
		if(trim($this->input->get('salesChannelName'))){$searchParameters['salesChannelName']				= trim($this->input->get('salesChannelName'));}
		if(trim($this->input->get('primarySupplierId'))){$searchParameters['primarySupplierId']				= trim($this->input->get('primarySupplierId'));}
		if(trim($this->input->get('brightpearlCategoryCode'))){$searchParameters['brightpearlCategoryCode']	= trim($this->input->get('brightpearlCategoryCode'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/product-service/product-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/product-service/product-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getproduct($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproductbundle($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product/'.$entityId.'/bundle';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproductcustomfield($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product-custom-field/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getcustomfieldmetadataproduct($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/product-service/product/custom-field-meta-data/';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function getproductcustomfieldproduct($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product/'.$entityId.'/custom-field';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproductgroup($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product-group/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproductoptionvalue($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product/'.$entityId.'/option-value/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproductprice($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product-price/'.$entityId.'/price-list/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproductsuppplier($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/product/'.$entityId.'/supplier';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchproducttype($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('id'))){$searchParameters['id']		= trim($this->input->get('id'));}
		if(trim($this->input->get('name'))){$searchParameters['name']	= trim($this->input->get('name'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/product-service/product-type-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/product-service/product-type-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getseason($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/product-service/season/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
		else{
			$this->brightpearl->reInitialize();
			$url		= '/product-service/season';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getbundleavailability($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/bundle-availability/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getdefaultlocation($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/warehouse/'.$entityId.'/location/default';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getdropshipnote($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/order/'.$entityId.'/goods-note/drop-ship/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getfulfilmentsource($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/fulfilment-source/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getorderfulfilmentstatus($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/order/'.$entityId.'/fulfilment-status';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchgoodinnote($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('price'))){$searchParameters['price']							= trim($this->input->get('price'));}
		if(trim($this->input->get('onHand'))){$searchParameters['onHand']						= trim($this->input->get('onHand'));}
		if(trim($this->input->get('batchId'))){$searchParameters['batchId']						= trim($this->input->get('batchId'));}
		if(trim($this->input->get('currCode'))){$searchParameters['currCode']					= trim($this->input->get('currCode'));}
		if(trim($this->input->get('productId'))){$searchParameters['productId']					= trim($this->input->get('productId'));}
		if(trim($this->input->get('groupingA'))){$searchParameters['groupingA']					= trim($this->input->get('groupingA'));}
		if(trim($this->input->get('groupingB'))){$searchParameters['groupingB']					= trim($this->input->get('groupingB'));}
		if(trim($this->input->get('groupingC'))){$searchParameters['groupingC']					= trim($this->input->get('groupingC'));}
		if(trim($this->input->get('groupingD'))){$searchParameters['groupingD']					= trim($this->input->get('groupingD'));}
		if(trim($this->input->get('locationId'))){$searchParameters['locationId']				= trim($this->input->get('locationId'));}
		if(trim($this->input->get('landedCosts'))){$searchParameters['landedCosts']				= trim($this->input->get('landedCosts'));}
		if(trim($this->input->get('createdDate'))){$searchParameters['createdDate']				= trim($this->input->get('createdDate'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']				= trim($this->input->get('warehouseId'));}
		if(trim($this->input->get('userBatchRef'))){$searchParameters['userBatchRef']			= trim($this->input->get('userBatchRef'));}
		if(trim($this->input->get('receivedDate'))){$searchParameters['receivedDate']			= trim($this->input->get('receivedDate'));}
		if(trim($this->input->get('purchaseOrderId'))){$searchParameters['purchaseOrderId']		= trim($this->input->get('purchaseOrderId'));}
		if(trim($this->input->get('receivedQuantity'))){$searchParameters['receivedQuantity']	= trim($this->input->get('receivedQuantity'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/warehouse-service/goods-in-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/warehouse-service/goods-in-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getgoodinnoteorderid($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/order/'.$entityId.'/goods-note/goods-in/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getgoodinnotegoodinnoteid($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/order/*/goods-note/goods-in/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchgoodsmovement($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('orderId'))){$searchParameters['orderId']								= trim($this->input->get('orderId'));}
		if(trim($this->input->get('batchId'))){$searchParameters['batchId']								= trim($this->input->get('batchId'));}
		if(trim($this->input->get('quantity'))){$searchParameters['quantity']							= trim($this->input->get('quantity'));}
		if(trim($this->input->get('productId'))){$searchParameters['productId']							= trim($this->input->get('productId'));}
		if(trim($this->input->get('updatedOn'))){$searchParameters['updatedOn']							= trim($this->input->get('updatedOn'));}
		if(trim($this->input->get('isCleared'))){$searchParameters['isCleared']							= trim($this->input->get('isCleared'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']						= trim($this->input->get('warehouseId'));}
		if(trim($this->input->get('goodsNoteId'))){$searchParameters['goodsNoteId']						= trim($this->input->get('goodsNoteId'));}
		if(trim($this->input->get('productValue'))){$searchParameters['productValue']					= trim($this->input->get('productValue'));}
		if(trim($this->input->get('currencyCode'))){$searchParameters['currencyCode']					= trim($this->input->get('currencyCode'));}
		if(trim($this->input->get('isQuarantine'))){$searchParameters['isQuarantine']					= trim($this->input->get('isQuarantine'));}
		if(trim($this->input->get('goodsMovementId'))){$searchParameters['goodsMovementId']				= trim($this->input->get('goodsMovementId'));}
		if(trim($this->input->get('salesOrderRowId'))){$searchParameters['salesOrderRowId']				= trim($this->input->get('salesOrderRowId'));}
		if(trim($this->input->get('receivedQuantity'))){$searchParameters['receivedQuantity']			= trim($this->input->get('receivedQuantity'));}
		if(trim($this->input->get('goodsNoteTypeCode'))){$searchParameters['goodsNoteTypeCode']			= trim($this->input->get('goodsNoteTypeCode'));}
		if(trim($this->input->get('purchaseOrderRowId'))){$searchParameters['purchaseOrderRowId']		= trim($this->input->get('purchaseOrderRowId'));}
		if(trim($this->input->get('destinationLocationId'))){$searchParameters['destinationLocationId']	= trim($this->input->get('destinationLocationId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/warehouse-service/goods-movement-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/warehouse-service/goods-movement-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function searchgoodoutnote($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('picked'))){$searchParameters['picked']						= trim($this->input->get('picked'));}
		if(trim($this->input->get('packed'))){$searchParameters['packed']						= trim($this->input->get('packed'));}
		if(trim($this->input->get('printed'))){$searchParameters['printed']						= trim($this->input->get('printed'));}
		if(trim($this->input->get('shipped'))){$searchParameters['shipped']						= trim($this->input->get('shipped'));}
		if(trim($this->input->get('orderId'))){$searchParameters['orderId']						= trim($this->input->get('orderId'));}
		if(trim($this->input->get('priority'))){$searchParameters['priority']					= trim($this->input->get('priority'));}
		if(trim($this->input->get('labelUri'))){$searchParameters['labelUri']					= trim($this->input->get('labelUri'));}
		if(trim($this->input->get('transfer'))){$searchParameters['transfer']					= trim($this->input->get('transfer'));}
		if(trim($this->input->get('shippedOn'))){$searchParameters['shippedOn']					= trim($this->input->get('shippedOn'));}
		if(trim($this->input->get('createdOn'))){$searchParameters['createdOn']					= trim($this->input->get('createdOn'));}
		if(trim($this->input->get('createdBy'))){$searchParameters['createdBy']					= trim($this->input->get('createdBy'));}
		if(trim($this->input->get('channelId'))){$searchParameters['channelId']					= trim($this->input->get('channelId'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']				= trim($this->input->get('warehouseId'));}
		if(trim($this->input->get('releaseDate'))){$searchParameters['releaseDate']				= trim($this->input->get('releaseDate'));}
		if(trim($this->input->get('goodsOutNoteId'))){$searchParameters['goodsOutNoteId']		= trim($this->input->get('goodsOutNoteId'));}
		if(trim($this->input->get('shippingMethodId'))){$searchParameters['shippingMethodId']	= trim($this->input->get('shippingMethodId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/warehouse-service/goods-note/goods-out-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/warehouse-service/goods-note/goods-out-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getgoodoutnoteorderid($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/order/'.$entityId.'/goods-note/goods-out/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getgoodoutnotegoodinnoteid($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/order/*/goods-note/goods-out/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function searchlocation($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('id'))){$searchParameters['id']					= trim($this->input->get('id'));}
		if(trim($this->input->get('zoneId'))){$searchParameters['zoneId']			= trim($this->input->get('zoneId'));}
		if(trim($this->input->get('barcode'))){$searchParameters['barcode']			= trim($this->input->get('barcode'));}
		if(trim($this->input->get('groupingA'))){$searchParameters['groupingA']		= trim($this->input->get('groupingA'));}
		if(trim($this->input->get('groupingB'))){$searchParameters['groupingB']		= trim($this->input->get('groupingB'));}
		if(trim($this->input->get('groupingC'))){$searchParameters['groupingC']		= trim($this->input->get('groupingC'));}
		if(trim($this->input->get('groupingD'))){$searchParameters['groupingD']		= trim($this->input->get('groupingD'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']	= trim($this->input->get('warehouseId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/warehouse-service/location-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/warehouse-service/location-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getlocationwarehouseid($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/warehouse/'.$entityId.'/location/';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getproductavailability($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/product-availability/'.$entityId;
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getlocationquarantine($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/warehouse/'.$entityId.'/location/quarantine';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getreservation($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/order/'.$entityId.'/reservation';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
	public function getreservations($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/warehouse-service/reservations';
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchshippingmethod($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('id'))){$searchParameters['id']			= trim($this->input->get('id'));}
		if(trim($this->input->get('code'))){$searchParameters['code']		= trim($this->input->get('code'));}
		if(trim($this->input->get('name'))){$searchParameters['name']		= trim($this->input->get('name'));}
		if(trim($this->input->get('carrier'))){$searchParameters['carrier']	= trim($this->input->get('carrier'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/warehouse-service/shipping-method-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/warehouse-service/shipping-method-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getstocktransfer($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/warehouse-service/stock-transfer/'.$entityId;
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchwarehouse($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('id'))){$searchParameters['id']							= trim($this->input->get('id'));}
		if(trim($this->input->get('name'))){$searchParameters['name']						= trim($this->input->get('name'));}
		if(trim($this->input->get('addressId'))){$searchParameters['addressId']				= trim($this->input->get('addressId'));}
		if(trim($this->input->get('typeDescription'))){$searchParameters['typeDescription']	= trim($this->input->get('typeDescription'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/warehouse-service/warehouse-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/warehouse-service/warehouse-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getwarehouse($entityId = ''){
		$account1Id	= 1;
		$this->brightpearl->reInitialize();
		$url		= '/warehouse-service/warehouse/'.$entityId;
		$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
		echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
	}
	public function searchzone($entityId = ''){
		$account1Id	= 1;
		$searchParameters	= array();
		if(trim($this->input->get('id'))){$searchParameters['id']					= trim($this->input->get('id'));}
		if(trim($this->input->get('name'))){$searchParameters['name']				= trim($this->input->get('name'));}
		if(trim($this->input->get('warehouseId'))){$searchParameters['warehouseId']	= trim($this->input->get('warehouseId'));}
		if($searchParameters){
			$added	= 0;
			$url	= '/warehouse-service/zone-search?';
			foreach($searchParameters as $searchParameter => $value){
				if($added){
					$url	= $url.'&'.$searchParameter.'='.$value;
				}
				else{
					$url	= $url.$searchParameter.'='.$value;
				}
				$added	= 1;
			}
			$this->brightpearl->reInitialize();
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
		else{
			$this->brightpearl->reInitialize();
			$url	= '/warehouse-service/zone-search';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			$headerKeys	= array_column($results['metaData']['columns'],'name');
			$resultsAvailable	= $results['metaData']['resultsAvailable'];
			$resultsReturned	= $results['metaData']['resultsReturned'];
			echo "resultsAvailable = $resultsAvailable<pre>";echo "</pre>";
			echo "resultsReturned  = $resultsReturned<pre>";echo "</pre>";
			foreach($results['results'] as $result){
				$row	= array_combine($headerKeys,$result);
				echo "row<pre>";print_r($row); echo "</pre>";
			}
		}
	}
	public function getzone($entityId = ''){
		$account1Id	= 1;
		if($entityId){
			$this->brightpearl->reInitialize();
			$url		= '/warehouse-service/warehouse/'.$entityId.'/zone';
			$results	= $this->brightpearl->getCurl($url, 'get', '', 'json', $account1Id)[1];
			echo "results<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
		}
	}
}