<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Aradvance extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('advance/aradvance_model','',TRUE);
	}
	public function index(){
		$data	= array();
		$this->template->load_template("advance/aradvance",$data,$this->session_data);
	}
	public function getAradvance(){
		$records	= $this->aradvance_model->getAradvance(); 
		echo json_encode($records);
	}
	public function fetchAradvance($id = ''){
		$this->aradvance_model->fetchAradvance($id);
	}
	public function postAradvance($id = ''){
		$this->aradvance_model->postAradvance($id);
	}
	public function applyAradvance($id = ''){
		$this->aradvance_model->applyAradvance($id);
	}
	
	public function aradvanceInfo($id = ''){
		$data['aradvanceInfo']	= $this->db->get_where('arAdvance',array('paymentId' => $id))->row_array();
		$this->template->load_template("advance/aradvanceInfo",$data,$this->session_data);
	}
	public function exportAradvance(){
		error_reporting('0');
		$this->db->reset_query();
		$where	= array();
        $query	= $this->db;
		if(trim($this->input->get('account1Id'))){$where['account1Id'] = trim($this->input->get('account1Id'));}
		if(trim($this->input->get('account2Id'))){$where['account2Id'] = trim($this->input->get('account2Id'));}
		if(trim($this->input->get('paymentId'))){$where['paymentId'] = trim($this->input->get('paymentId'));}
		if(trim($this->input->get('transactionRef'))){$where['transactionRef'] = trim($this->input->get('transactionRef'));}
		if(trim($this->input->get('transactionCode'))){$where['transactionCode'] = trim($this->input->get('transactionCode'));}
		if(trim($this->input->get('paymentMethodCode'))){$where['paymentMethodCode'] = trim($this->input->get('paymentMethodCode'));}
		if(trim($this->input->get('paymentType'))){$where['paymentType'] = trim($this->input->get('paymentType'));}
		if(trim($this->input->get('orderId'))){$where['orderId'] = trim($this->input->get('orderId'));}
		if(trim($this->input->get('currencyId'))){$where['currencyId'] = trim($this->input->get('currencyId'));}
		if(trim($this->input->get('currencyCode'))){$where['currencyCode'] = trim($this->input->get('currencyCode'));}
		if(trim($this->input->get('amountAuthorized'))){$where['amountAuthorized'] = trim($this->input->get('amountAuthorized'));}
		if(trim($this->input->get('amountPaid'))){$where['amountPaid'] = trim($this->input->get('amountPaid'));}
		if(trim($this->input->get('journalId'))){$where['journalId'] = trim($this->input->get('journalId'));}
		if(trim($this->input->get('contactId'))){$where['contactId'] = trim($this->input->get('contactId'));}
		if(trim($this->input->get('journalTypeCode'))){$where['journalTypeCode'] = trim($this->input->get('journalTypeCode'));}
		if(trim($this->input->get('exchangeRate'))){$where['exchangeRate'] = trim($this->input->get('exchangeRate'));}
		if(trim($this->input->get('description'))){$where['description'] = trim($this->input->get('description'));}
		if(trim($this->input->get('orderTotal'))){$where['orderTotal'] = trim($this->input->get('orderTotal'));}
		if(trim($this->input->get('status'))){$where['status'] = trim($this->input->get('status'));}
		if(trim($this->input->get('message'))){$where['message'] = trim($this->input->get('message'));}
		if(trim($this->input->get('channelId'))){$where['channelId'] = trim($this->input->get('channelId'));}
		if(trim($this->input->get('bankTxnId'))){$where['bankTxnId'] = trim($this->input->get('bankTxnId'));}
		if(trim($this->input->get('prePaymentId'))){$where['prePaymentId'] = trim($this->input->get('prePaymentId'));}
		if(trim($this->input->get('applicationId'))){$where['applicationId'] = trim($this->input->get('applicationId'));}
		if(trim($this->input->get('paymentDate_to'))){$query->where('date(paymentDate) <= ', "date('" . $this->input->get('paymentDate_to') . "')", false);}
		if(trim($this->input->get('paymentDate_from'))){$query->where('date(paymentDate) >= ', "date('" . $this->input->get('paymentDate_from') . "')", false);}
		if($where){$query->like($where);}
		
		$datas	= $query->select('id,account1Id,account2Id,paymentId,transactionRef,transactionCode,paymentMethodCode,paymentType,orderId,currencyId,currencyCode,amountAuthorized,amountPaid,expires,paymentDate,createdOn,journalId,contactId,journalTypeCode,exchangeRate,description,orderTotal,status,created,message,channelId,bankTxnId,prePaymentId,applicationId')->get_where('arAdvance')->result_array();	
		
		$account1Mappings		= array();
		$account2Mappings		= array();
		
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$allBPChannelsName	= array();
		$allBPChannel		= $this->brightpearl->getAllChannelMethod();
		if($allBPChannel){
			foreach($allBPChannel[1] as $allBPChannels){
				$allBPChannelsName[$allBPChannels['id']]	= $allBPChannels['name'];
			}
		}
		
		$file	= date('Ymd').'_advance.csv';
		$fp		= fopen('php://output', 'w');
		$header	= array('Brightpearl', 'Xero', 'Payment', 'Journal', 'Order', 'Amount', 'Type', 'Method', 'Contact', 'Date', 'Channel', 'Txn', 'Prepayment', 'Status', 'Message');
		
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$file);
		fputcsv($fp, $header);
		$status	= array('0' => 'Pending', '1' => 'Sent', '2' => 'Reversed', '3' => 'Applied', '4' => 'Archive');
		foreach($datas as $data){
			$row	= array(
				$account1Mappings[$data['account1Id']]['name'],
				$account2Mappings[$data['account2Id']]['name'],
				$data['paymentId'],
				$data['journalId'],
				$data['orderId'],
				$data['amountPaid'],
				$data['paymentType'],
				$data['paymentMethodCode'],
				$data['contactId'],
				date('Y-m-d',strtotime($data['paymentDate'])),
				$allBPChannelsName[$data['channelId']],
				$data['bankTxnId'],
				$data['prePaymentId'],
				$status[$data['status']],
			);
			fputcsv($fp, $row);
		}
	}
}