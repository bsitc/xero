<?php
//XERO : BRIGHTPEARL INTEGRATION CONNECTOR AUTOMATION FILE
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Webhooksv2 extends CI_Controller {
	public $file_path;
	public function __construct(){
		parent::__construct();
		$this->defaultPhp	= "7.0";
		$getVersion			= phpversion();
		if(strlen($getVersion) > 0){
			$this->defaultPhp	= substr($getVersion,0,3);
		}
	}
	
	public function refreshToken($id = ''){
		$this->xero->refreshToken($id);
	}
	
	public function	processXeroAutomation(){
		$isConsolRunning	= $this->getRunningTask('processXeroAutomationConsol');
		if($isConsolRunning){return false;}
		$AutomatuonConfig	= $this->db->get('automationStructure')->row_array();
		if(!$AutomatuonConfig){
			return false;
		}
		else{
			if($AutomatuonConfig['disableAutomation'] == 0){
				if($AutomatuonConfig['productSync']){
					$this->processProductSync();
				}
				if($AutomatuonConfig['customerSync']){
					$this->processCustomerSync();
				}
				if($AutomatuonConfig['purchaseOrderSync']){
					$this->processPurchaseOrderSync();
				}
				if($AutomatuonConfig['stockAdjustmentSync']){
					$this->processStockAdjustmentSync();
				}
				if($AutomatuonConfig['salesOrderSync']){
					if($AutomatuonConfig['amazonFeesSync']){
						$this->processSalesOrderAndAmazonSync();
					}
					else{
						$this->processSalesOrderSync();
					}
				}
				if($AutomatuonConfig['purchaseCreditSync']){
					$this->processPurchaseCreditSync();
				}
				if($AutomatuonConfig['salesCreditSync']){
					$this->processSalesCreditSync();
				}
				if($AutomatuonConfig['cogsJournalSync']){
					$this->processCOGSJournalSync();
				}
				$this->deleteAPICallLogs();
			}
		}
	}
	
	public function	processXeroAutomationConsol(){
		$AutomatuonConfig	= $this->db->get('automationStructure')->row_array();
		if(!$AutomatuonConfig){
			return false;
		}
		else{
			if($AutomatuonConfig['disableAutomation'] == 0){
				if($AutomatuonConfig['salesOrderConsolSync']){
					if($AutomatuonConfig['amazonFeesSync']){
						$this->processSalesOrderAndAmazonConsolSync();
					}
					else{
						$this->processSalesOrderConsolSync();
					}
				}
				if($AutomatuonConfig['salesCreditConsolSync']){
					$this->processSalesCreditConsolSync();
				}
				if($AutomatuonConfig['cogsJournalConsolSync']){
					$this->processCOGSJournalConsolSync();
				}
				$this->deleteAPICallLogs();
			}
		}
	}
	
	public function	processProductSync(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
	}
	public function	processCustomerSync(){
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
	}
	public function	processPurchaseOrderSync(){
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
	}
	public function	processPurchaseCreditSync(){
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
	}
	public function	processStockAdjustmentSync(){
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
	}
	public function	processSalesOrderSync(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
	}
	public function	processSalesOrderAndAmazonSync(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
	}
	public function	processSalesCreditSync(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
	}
	public function	processCOGSJournalSync(){
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
	}
	public function	processSalesOrderConsolSync(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
	}
	public function	processSalesOrderAndAmazonConsolSync(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->sales_overridemodel->postaggregationSales();
	}
	public function	processSalesCreditConsolSync(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
	}
	public function	processCOGSJournalConsolSync(){
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
	}
	public function	deleteAPICallLogs(){
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	GLOBAL FUNCTION FOR PROCESS AUTOMATION OF XERO	*/
	public function runTaskXero($taskName = 'processXeroAutomation'){
		if($taskName){
			$isRunning	= $this->getRunningTask($taskName);
			if(!$isRunning){
				if($this->defaultPhp){
					$isSellRUn	= shell_exec('/opt/plesk/php/'.$this->defaultPhp.'/bin/php '.FCPATH.'index.php webhooks '.$taskName);
				}
				else{
					$isSellRUn	= shell_exec('php '.FCPATH.'index.php webhooks '.$taskName);
				}
			}
		}
		die();
	}
	public function getRunningTask($checkTask = 'processXeroAutomation'){
		$return		= false;
		$checkTask	= trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			$fcpath	= strtolower(FCPATH. 'index.php webhooks '.$checkTask);	
			foreach($outputs as $output){
				$output	= strtolower($output);
				if(substr_count($output,$fcpath)){
					if(substr_count($output,$checkTask)){
						if(!substr_count($output,'runtask')){ 
							$return	= true;
						}
					}
				}
			}
		}
		return $return;
	}
	public function closeRunner(){
		date_default_timezone_set('GMT');
		$cpid	= posix_getpid(); 
		exec('ps aux | grep php', $outputs);
		$currentTime	= gmdate('YmdHis',strtotime('-300 min')); 
		foreach($outputs as $output){
			$ps						= preg_split('/ +/', $output);
			$pid					= $ps[1];
			$cronStartTimeTimeStamp	= strtotime($ps['8']);
			if(substr_count($output,'runTask/')){
				shell_exec("kill $pid");
			}
			elseif(substr_count($output,'/index.php')){
				if(gmdate('Y',$cronStartTimeTimeStamp) == gmdate('Y')){
					$cronStartTime	= date('YmdHis',$cronStartTimeTimeStamp);
					if($cronStartTime < $currentTime){ 
						shell_exec("kill $pid");
					}
				}
			}
		}
	}
}