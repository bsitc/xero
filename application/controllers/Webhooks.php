<?php
//xero_demo
if(!defined('BASEPATH')) exit('No direct script access allowed');
#[\AllowDynamicProperties]
class Webhooks extends CI_Controller {
	public $file_path;
	public function __construct(){
		parent::__construct();
		$this->load->library('mailer');
		$this->defaultPhp	= "8.2";
		$getVersion			= phpversion();
		if(strlen($getVersion) > 0){
			$this->defaultPhp	= substr($getVersion,0,3);
		}
	}
	public function refreshToken($id = ''){
		$this->xero->refreshToken($id);
	}
	
	/*	automation for InventoryControlManagement Xero	-	//bsitc-bridge23.com/inventorycontrol/		*/
	public function	processICM(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for NLG Xero							-	//bsitc-bridge23.com/nlg/					*/
	public function	processNLGXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Arte-N Xero						-	//bsitc-bridge25.com/arten/					*/
	public function processArten(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}	
	
	/*	automation for GrandPrixRaceWear Xero			-	//bsitc-bridge25.com/grandprix/				*/
	public function	processGrandprix(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for GoodStartPackaging Xero			-	//bsitc-bridge25.com/goodstartpackaging/	*/
	public function	processGoodstartpackaging(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for S&G Xero							-	//bsitc-bridge35.com/sandg/					*/
	public function processSandG(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for GBICS Xero						-	//bsitc-bridge39.com/gbics/					*/
	public function processGbics(){ 
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for GreenWayHerbalProductsLtd Xero	-	//bsitc-bridge44.com/greenwayxero/			*/
	public function processGreenway(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Amistry Xero						-	//bsitc-bridge45.com/amistry/				*/
	public function processAmistryXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Innovation1st Xero				-	//bsitc-bridge45.com/innovation1st/			*/
	public function processInnovation1st(){
		/* $this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit(); */
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for CabbagesAndRoses Xero			-	//bsitc-bridge46.com/cabbagesandrosesxero/	*/
	public function processCabbagesAndRosesXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for IvoryEgg Xero					-	//ivoryeggxero.bsitc-apps.com/				*/
	public function processIvoryEggXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for FreeFly Xero						-	//freeflyxero.bsitc-apps.com/				*/
	public function processFreeFlyXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processFreeFlyConsolXero(){
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for GO10 Xero						-	//go10xero.bsitc-apps.com/					*/
	public function processGo10ConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processGo10Xero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for DrivelineBaseBall Xero			-	//drivelinebaseballxero.bsitc-apps.com/		*/
	public function processDrivelineBaseBallXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for ArtOfLiving Xero					-	//artofliving.bsitc-apps.com/				*/
	public function processArtOfLivingConsolXero(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postaggregationSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->credit_overridemodel->postSalesCredit();

		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	public function processArtOfLivingPOPC(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();

		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	
	/*	automation for rossoandAzzurro Xero				-	//rossoandAzzurro.bsitc-apps.com/			*/
	public function processRossoXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Stitch&Story Xero				-	//stitchandstoryxero.bsitc-apps.com/		*/
	public function processStitchandStoryXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processStitchandStoryConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for PigletInBed Xero					-	//pigletxero.bsitc-apps.com/				*/
	public function processPigletInBed(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Lords&Labradors Xero				-	//lordsandlabradors.bsitc-apps.com/			*/
	public function processLordsandlabradors(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();

		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for MillbryHIll Xero					-	//millbryhillxero.bsitc-apps.com/			*/
	public function processMillbryhillXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processMillbryHillxeroConsolXero(){	
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/Stockjournal_model','',TRUE);
		$this->Stockjournal_model->fetchStockjournal();
		$this->Stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for HawkeOptics Xero					-	//hawkeopticsxero.bsitc-apps.com/			*/
	public function processHawkeOpticsXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processHawkeOpticsConsolXero2(){
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for bertFrank Xero					-	//bertfrankxero.bsitc-apps.com/				*/
	public function processBertFrankXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		/* $this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();	 */	
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		/* $this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal(); */
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for pineapple island Xero			-	//pineappleislandxero.bsitc-apps.com/		*/
	public function processPineappleislandcronXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processPineappleislandConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for tombeckbexero Xero				-	//tombeckbexero.bsitc-apps.com/				*/
	public function processTomBeckbeXero(){
		echo"<pre>";print_r('startTime : '.date('c'));echo"<pre>";
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r('endTime : '.date('c'));echo"<pre>";
	}
	public function processTomBeckbeStockConsolXero(){
		echo"<pre>";print_r('startTime : '.date('c'));echo"<pre>";
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r('endTime : '.date('c'));echo"<pre>";
	}
	
	/*	automation for Nisolo Xero						-	//nisoloxero.bsitc-apps.com/				*/
	public function processNisoloXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processNisoloConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for LOGIC OF ENGLISH XERO			-	//logicofenglishxero.bsitc-apps.com			*/
	public function processLogicOfEnglishXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for startTraffic Xero				-	//st-xero.bsitc-apps.com					*/
	public function processSTXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 10 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for bondtouchxero Xero				-	//bondtouchxero.bsitc-apps.com/				*/
	public function processBondTouchMultiXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processBondTouchMultiConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for BONDPORTUGAL XERO				-	//bondportugalxero.bsitc-apps.com/			*/
	public function processBondPortugalXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processBondPortugalConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Albray Xero						-	//albrayxero.bsitc-apps.com/				*/
	public function processAlbarayXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processAlbarayConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->credit_overridemodel->postNetOffConsolOrder();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Samurai Xero						-	//samuraixero.bsitc-apps.com/				*/
	public function processSamuraiXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for DrVapes Xero						-	//drvapesxero.bsitc-apps.com/				*/
	public function processDrVapesXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Heist Xero						-	//heistxero.bsitc-apps.com/					*/
	public function processHeistXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processHeistConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for BusbyandFox Xero					-	//bfxero.bsitc-apps.com/					*/
	public function processBusbyandFoxXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processBusbyandFoxConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for run4ltxero Xero					-	//run4ltxero.bsitc-apps.com/				*/
	public function processRun4ItXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processRun4ItConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for quickplay Xero					-	//quickplayxero.bsitc-apps.com/				*/
	public function processQuickPlayXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processQuickPlayConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for BellsOfSteel Xero				-	//bellsofsteelxero.bsitc-apps.com/			*/
	public function processBOSXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for ivoryeggausxero	AUS Xero		-	//ivoryeggausxero.bsitc-apps.com			*/
	public function processIvoryEggAUSXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processIvoryEggAUSstockConsolXero(){
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
			
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for SurfTurf Xero					-	//surfturfxero.bsitc-apps.com/				*/
	public function processSurfTurfXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processSurfTurfConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for mmandsonsxero Xero				-	//mmandsonsxero.bsitc-apps.com/				*/
	public function processMMandSonsXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/* automation for Keen Xero							-	//keenxero.bsitc-apps.com/					*/
	public function processKeenXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processKeenConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for Javlin Xero						-	//javelinxero.bsitc-apps.com/				*/
	public function processJavlinXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processJavlinConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for lacoquetaxero Xero				-	//lacoquetaxero.bsitc-apps.com/				*/
	public function processLaCoquetaXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processLaCoquetaConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for wildseven Xero					-	//wildsevenxero.bsitc-apps.com/				*/
	public function processWildSevenXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for wildcosmetics Xero				-	//wildcosmeticsxero.bsitc-apps.com/			*/	
	public function processWildCosmeticsXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		/* $this->load->model('stock/adjustment_model','',TRUE);
		$this->adjustment_model->fetchStockAdjustment();
		$this->adjustment_model->postStockAdjustment(); */
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal(); 
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processWildCosmeticsConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for hawkeusxero Xero				-	//hawkeusxero.bsitc-apps.com/				*/
	public function processHawkeUSXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processHawkeUSStockConsolXero(){
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for williesxero Xero				-	//williesxero.bsitc-apps.com/				*/
	public function processWilliesXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processWilliesConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for renfoldxero Xero				-	//renfoldxero.bsitc-apps.com/		*/	
	public function processRenfoldXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal(); 
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for lgrxero Xero				-	//lgrxero.bsitc-apps.com/		*/	
	public function processLGRXero(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->load->model('journal/journal_model','',TRUE);
		$this->journal_model->fetchJournal();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	public function processLGRConsolXero(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	
	/*	automation for FernandoJorge Xero				-	//FernandoJorge.bsitc-apps.com/		*/	
	public function processFernandoJorgeXero(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	public function processFernandoJorgeConsolXero(){
		echo"<pre>";print_r("startTime : ".date('c'));echo"<pre>";
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
		echo"<pre>";print_r("endTime : ".date('c'));echo"<pre>";
	}
	
	/*	automation for shackletonxero Xero				-	//shackletonxero.bsitc-apps.com/			*/
	public function processShackletonXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processShackletonConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	/*	automation for motionmetricsxero Xero				-	//motionmetricsxero.bsitc-apps.com/		*/
	public function processMotionMetricsXero(){
		$this->load->model('products/products_model','',TRUE);
		$this->products_model->fetchProducts('','1','0');
		$this->load->model('customers/customers_model','',TRUE);
		$this->customers_model->fetchCustomers();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchase_overridemodel','',TRUE);
		$this->purchase_overridemodel->fetchPurchase();
		$this->purchase_overridemodel->postPurchase();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postSales();
		$this->load->model('purchase/'.$this->globalConfig['account2Liberary'].'/purchasecredit_overridemodel','',TRUE); 
		$this->purchasecredit_overridemodel->fetchPurchaseCredit();
		$this->purchasecredit_overridemodel->postPurchaseCredit();		
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postSalesCredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postCogsjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	public function processMotionMetricsConsolXero(){
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/sales_overridemodel','',TRUE);
		$this->sales_overridemodel->fetchSales();
		$this->sales_overridemodel->postaggregationSales();
		$this->load->model('sales/'.$this->globalConfig['account2Liberary'].'/credit_overridemodel','',TRUE);
		$this->credit_overridemodel->fetchSalesCredit();
		$this->credit_overridemodel->postaggregationSalescredit();
		$this->load->model('cogsjournal/cogsjournal_model','',TRUE);
		$this->cogsjournal_model->fetchCogsjournal();
		$this->cogsjournal_model->postConsolCogsjournal();
		$this->load->model('stockjournal/stockjournal_model','',TRUE);
		$this->stockjournal_model->fetchStockjournal();
		$this->stockjournal_model->postConsolStockjournal();
		
		$checkDate	= gmdate('Y-m-d H:m:s',strtotime('- 8 days'));
		$this->db->where("date(`CallTime`) < ","date('".$checkDate."')",false)->delete('api_call_log');
	}
	
	
	/*	GLOBAL FUNCTION FOR PROCESS AUTOMATION OF XERO				*/
	public function runTaskXero($taskName = 'processXero'){
		if($taskName){
			$isRunning	= $this->getRunningTask($taskName);
			if(!$isRunning){
				$syncFilePath	= FCPATH.'/jobsoutput/'.date('Y').'/'.date('m').'/'.date('d').'/synclogs/';
				if(!is_dir(($syncFilePath))){
					mkdir(($syncFilePath),0777,true);
					chmod(($syncFilePath), 0777);
				}
				$syncFileName	= $taskName.date('H-i-s').'.logs';
				
				if($this->defaultPhp){
					$isSellRUn	= shell_exec('/opt/plesk/php/'.$this->defaultPhp.'/bin/php '.FCPATH.'index.php webhooks '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				else{
					$isSellRUn	= shell_exec('php '.FCPATH.'index.php webhooks '.$taskName.' >> '.$syncFilePath.$syncFileName.' 2>&1');
				}
				
				if(is_file($syncFilePath.$syncFileName)){
					$filecontent	= file_get_contents($syncFilePath.$syncFileName);
					if(strlen($filecontent) > 0){
						if((substr_count($filecontent,"An uncaught Exception was encountered")) OR (substr_count($filecontent,"Type:        TypeError")) OR (substr_count($filecontent,"Fatal error")) OR (substr_count($filecontent,"Type:        DivisionByZeroError")) OR (substr_count($filecontent,"Database error")) OR (substr_count($filecontent,"Type:        ValueError")) OR (substr_count($filecontent,"Type:        Error"))){
							$appName		= trim($this->globalConfig['app_name']);
							$Receipents		= trim($this->globalConfig['emailReceipents']);
							if($Receipents == ''){
								$Receipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,tushar@businesssolutionsinthecloud.com,krisha@businesssolutionsinthecloud.com,priyanka@businesssolutionsinthecloud.com';
							}
							$smtpUsername	= trim($this->globalConfig['smtpUsername']);
							$smtpPassword	= trim($this->globalConfig['smtpPassword']);
							$subject		= 'Alert '.$appName.' - '.$taskName.' Run time error';
							$from			= ['info@bsitc-apps.com' => $this->appName];
							$mailBody		= 'Hi,<br><br><p>Automation not completed successfully, please check the attached cron output file.</p><br><br><br><br><br>Thanks & Regards<br>BSITC Team';
							$this->mailer->send($Receipents, $subject, $mailBody, $from, $syncFilePath.$syncFileName, $smtpUsername, $smtpPassword);
						}
					}
				}
			}
		}
		die();
	}
	public function getRunningTask($checkTask = 'processXero'){
		$return		= false;
		$checkTask	= trim(strtolower($checkTask));
		if($checkTask){
			exec('ps aux | grep php', $outputs);
			$fcpath	= strtolower(FCPATH. 'index.php webhooks '.$checkTask);	
			foreach($outputs as $output){
				$output	= strtolower($output);
				if(substr_count($output,$fcpath)){
					if(substr_count($output,$checkTask)){
						if(!substr_count($output,'runtask')){ 
							$return	= true;
						}
					}
				}
			}
		}
		return $return;
	}
	public function closeRunner(){
		date_default_timezone_set('GMT');
		$cpid	= posix_getpid(); 
		exec('ps aux | grep php', $outputs);
		$currentTime	= gmdate('YmdHis',strtotime('-300 min')); 
		foreach($outputs as $output){
			$ps						= preg_split('/ +/', $output);
			$pid					= $ps[1];
			$cronStartTimeTimeStamp	= strtotime($ps['8']);
			if(substr_count($output,'runTask/')){
				shell_exec("kill $pid");
			}
			elseif(substr_count($output,'/index.php')){
				if(gmdate('Y',$cronStartTimeTimeStamp) == gmdate('Y')){
					$cronStartTime	= date('YmdHis',$cronStartTimeTimeStamp);
					if($cronStartTime < $currentTime){ 
						shell_exec("kill $pid");
					}
				}
			}
		}
	}
	
	/*	GLOBAL FUNCTION FOR PROCESS CUSTOMERS OF XERO LARGE VOLUME	*/
	public function processSavecustomers(){
		$data	= $this->db->get_where('temp',array('status'=> 0,'type' => 'customer'))->row_array();
		if($data){
			$this->brightpearl->fetchCustomers('',$data['id']);
		}
	}
	/*	GLOBAL FUNCTION FOR PROCESS PRODUCTS OF XERO LARGE VOLUME	*/
	public function processSaveproducts(){
		$data	= $this->db->get_where('temp',array('status'=> 0,'type' => 'products'))->row_array();
		if($data){
			$this->brightpearl->fetchProducts('',$data['id']);
		}
	}
}
//end