<?php
if(!defined('BASEPATH')){
	exit('No direct script access allowed');
}
#[\AllowDynamicProperties]
class SalesReport extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('report/salesReport_model','',TRUE);
	}
	public function index(){
		$data		= array();
		$this->template->load_template("report/salesReport",$data,$this->session_data);
	}
	public function getSalesReport(){
		$records	= $this->salesReport_model->getSalesReport();
		echo json_encode($records);
	}
	public function fetchSalesReport($orderId = ''){
		$this->salesReport_model->fetchSalesReport($orderId);
	}
	public function exportSalesReport($orderId = ''){
		//$this->salesReport_model->exportSalesReport($orderId);
	}
}