<?php
if(!defined('BASEPATH')){
	exit('No direct script access allowed');
}
#[\AllowDynamicProperties]
class ItemChange extends CI_Controller {
	public $ci;
	public function __construct(){
		parent::__construct();
		$this->ci			= &get_instance();
		$this->appName		= $this->globalConfig['app_name'];
	}
	public function changeSku(){
		$account2Id			= 1;
		$this->xero->reInitialize();
		$ProIDs				= array();
		if($ProIDs){
			$AllProductData		= $this->ci->db->where_in('productId',$ProIDs)->order_by('productId','desc')->select('name,createdProductId,productId,sku')->get_where('products',array('account2Id' => $account2Id, 'status <>' => 0,'createdProductId <>' => ''))->result_array();
		}
		else{
			$AllProductData		= $this->ci->db->limit(500)->order_by('productId','desc')->select('name,createdProductId,productId,sku')->get_where('products',array('account2Id' => $account2Id, 'status <>' => 0,'createdProductId <>' => ''))->result_array();
		}
		
		$allUpdatedProduct	= array();
		if($AllProductData){
			foreach($AllProductData as $AllProductDataTemp){
				$request	= array();
				$request	= array(
					'Code'		=> 'z'.$AllProductDataTemp['sku'],
					'Name'		=> 'z'.$AllProductDataTemp['name'],
					'ItemID'	=> $AllProductDataTemp['createdProductId'],
				);
				$request['Name']	= substr($request['Name'],0,50);
				$Xerourl	= '2.0/Items?unitdp=4';
				$this->xero->initializeConfig($account2Id, 'POST', $Xerourl);
				$XeroResult	= $this->xero->getCurl($Xerourl, "POST", json_encode($request), 'json', $account2Id)[$account2Id];
				if((strtolower($XeroResult['Status']) == 'ok') AND ($XeroResult['Items']['Item']['ItemID'])){
					$allUpdatedProduct[]	= $AllProductDataTemp['productId'];
				}
			}
			if($allUpdatedProduct){
				$this->ci->db->where_in('productId',$allUpdatedProduct)->update('products',array('createdProductId' => '', 'status' => 0, 'ceatedParams' => ''),array('account2Id' => $account2Id));
			}
		}
	}
}