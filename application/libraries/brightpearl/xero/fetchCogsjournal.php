<?php
$this->reInitialize();
$allCurrencyCodes	= $this->getAllCurrency();
$returns			= array();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	$return			= array();
	$this->config	= $this->accountConfig[$account1Id];
	$account2Ids	= $this->account2Details[$account1Id];
	$allCurrencies	= $allCurrencyCodes[$account1Id];
	$saveTaxDate	= $this->config['taxDate'];
	
	$saveCronTime	= array();
	$cronTime		= strtotime('-60 days');
	$saveCronTime[]	= strtotime('-60 days');
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	
	$saveJournalTypes	= array();
	$journalTypeCodes	= (trim($this->config['FetchCOGSJournalType']))?(trim($this->config['FetchCOGSJournalType'])):'';
	$journalTypeCodes	= explode(",",$journalTypeCodes);
	$journalTypeCodes	= array_filter($journalTypeCodes);
	$journalTypeCodes	= array_unique($journalTypeCodes);
	if(!empty($journalTypeCodes)){
		foreach($journalTypeCodes as $journalTypeCodess){
			if($journalTypeCodess){
				$saveJournalTypes[]	= trim(strtoupper($journalTypeCodess));
			}
		}
	}
	
	$saveJournalAccount	= array();
	$journalAccountCode	= (trim($this->config['FetchCOGSJournalNominal']))?(trim($this->config['FetchCOGSJournalNominal'])):'';
	$journalAccountCode	= explode(",",$journalAccountCode);
	$journalAccountCode	= array_filter($journalAccountCode);
	$journalAccountCode	= array_unique($journalAccountCode);
	if(!empty($journalAccountCode)){
		foreach($journalAccountCode as $journalAccountCodes){
			if($journalAccountCodes){
				$saveJournalAccount[]	= trim(strtoupper($journalAccountCodes));
			}
		}
	}
	
	if((empty($saveJournalAccount)) OR (empty($saveJournalTypes))){continue;}
	
	$journalIds	= array();
	foreach($saveJournalTypes as $saveJournalTypess){
		foreach($saveJournalAccount as $saveJournalAccounts){
			$url		= '/accounting-service/journal-search?journalType='.$saveJournalTypess.'&nominalCode='.$saveJournalAccounts.'&journalDate='.$cronTime.'/';
			$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
			if((is_array($response)) AND (isset($response['results']))){
				$header	= array_column($response['metaData']['columns'],"name");
				foreach($response['results'] as $result){
					$row	= array_combine($header,$result);
					$journalIds[]	= $row['journalId'];
				}
				if($response['metaData']['resultsAvailable'] > 500){
					for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
						$url1		= $url . '&firstResult=' . $i;
						$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
						if((is_array($response1)) AND (isset($response1['results']))){
							foreach($response1['results'] as $result){
								$row	= array_combine($header,$result);
								$journalIds[]	= $row['journalId'];
							}
						}
					}
				}
			}
		}
	}
	$journalIds	= array_filter($journalIds);
	$journalIds	= array_unique($journalIds);
	sort($journalIds);
	if(empty($journalIds)){continue;}
	
	$returnKey		= 0;
	$jounalResults	= $this->getResultById($journalIds,'/accounting-service/journal',$account1Id,200,'0','');
	if((is_array($jounalResults)) AND (!empty($jounalResults)) AND (isset($jounalResults['journals']))){
		$allStoredSalesdata		= array();
		$allStoredCreditdata	= array();
		$allStoredPurchasedata	= array();
		$allOrdersDBdata		= $this->ci->db->select('account1Id,account2Id,orderId,bpInvoiceNumber')->get_where('sales_order',array('orderId <>' => ''))->result_array();
		if((is_array($allOrdersDBdata)) AND (!empty($allOrdersDBdata))){
			foreach($allOrdersDBdata as $allOrdersDBdatas){
				$allStoredSalesdata[$allOrdersDBdatas['orderId']]	= $allOrdersDBdatas;
			}
		}
		$allOrdersDBdata		= $this->ci->db->select('account1Id,account2Id,orderId,bpInvoiceNumber')->get_where('sales_credit_order',array('orderId <>' => ''))->result_array();
		if((is_array($allOrdersDBdata)) AND (!empty($allOrdersDBdata))){
			foreach($allOrdersDBdata as $allOrdersDBdatas){
				$allStoredCreditdata[$allOrdersDBdatas['orderId']]		= $allOrdersDBdatas;
			}
		}
		$allOrdersDBdata		= $this->ci->db->select('account1Id,account2Id,orderId,bpInvoiceNumber')->get_where('purchase_order',array('orderId <>' => ''))->result_array();
		if((is_array($allOrdersDBdata)) AND (!empty($allOrdersDBdata))){
			foreach($allOrdersDBdata as $allOrdersDBdatas){
				$allStoredPurchasedata[$allOrdersDBdatas['orderId']]	= $allOrdersDBdatas;
			}
		}
		
		foreach($jounalResults['journals'] as $jounalResult){
			$journalTypeCode	= $jounalResult['journalTypeCode'];
			if(!in_array($journalTypeCode,$saveJournalTypes)){continue;}
			
			$saveAccId1		= $account1Id;
			$journalsId		= $jounalResult['id'];
			$taxDate		= $jounalResult['taxDate'];
			$debits			= $jounalResult['debits'];
			$credits		= $jounalResult['credits'];
			
			
			if($clientcode == 'planks'){
				$checkTaxDate	= date('Ymd', strtotime($taxDate));
				if($checkTaxDate < '20241015'){
					continue;
				}
			}
			
			
			$dabitsDatas	= array();
			$creditsDatas	= array();
			if(!isset($debits['0'])){$debits = array($debits);}
			if(!isset($credits['0'])){$credits = array($credits);}
			
			foreach($debits as $debit){
				if($debit['orderId']){
					if(isset($dabitsDatas[$debit['orderId']])){
						$dabitsDatas[$debit['orderId']]['transactionAmount']	+= $debit['transactionAmount'];
						if($debit['assignment']['channelId']){
							$dabitsDatas[$debit['orderId']]['assignment']['channelId'] = $debit['assignment']['channelId'];
						}
					}
					else{
						$dabitsDatas[$debit['orderId']]	= $debit;
					}
				}
			}
			foreach($credits as $credit){
				if($credit['orderId']){
					if(isset($creditsDatas[$credit['orderId']])){
						$creditsDatas[$credit['orderId']]['transactionAmount']	+= $credit['transactionAmount'];
						if($credit['assignment']['channelId']){
							$creditsDatas[$credit['orderId']]['assignment']['channelId'] = $credit['assignment']['channelId'];
						}
					}
					else{
						$creditsDatas[$credit['orderId']]	= $credit;
					}
				}
			}
			
			$channelId			= '';
			$taxCode			= '';
			$BPorderId			= '';
			$invoiceReference	= '';
			$debitAmount		= '';
			$creditAmount		= '';
			$debitNominalCode	= '';
			$creditNominalCode	= '';
			$saveAccId2			= '';
			$skipJournal		= 0;
			$currencyCode		= '';
			$OrderType			= '';
			foreach($dabitsDatas as $orderId => $dabitsData){
				$saveAccId2	= '';
				if((!$allStoredSalesdata[$orderId]) AND (!$allStoredCreditdata[$orderId]) AND (!$allStoredPurchasedata[$orderId])){
					$skipJournal	= 1;
					continue;
				}
				if($allStoredSalesdata[$orderId]){
					$saveAccId2	= $allStoredSalesdata[$orderId]['account2Id'];
				}
				elseif($allStoredCreditdata[$orderId]){
					$saveAccId2	= $allStoredCreditdata[$orderId]['account2Id'];
				}
				elseif($allStoredPurchasedata[$orderId]){
					if($journalTypeCode != 'GO'){
						$skipJournal	= 1;
						continue;
					}
					else{
						$saveAccId2	= $allStoredPurchasedata[$orderId]['account2Id'];
					}
				}
				else{
					$skipJournal	= 1;
					continue;
				}
				$BPorderId			= $orderId;
				if($dabitsData['assignment']['channelId']){
					$channelId			= $dabitsData['assignment']['channelId'];
				}
				if($dabitsData['taxCode']){
					$taxCode			= $dabitsData['taxCode'];
				}
				if($dabitsData['invoiceReference']){
					$invoiceReference	= $dabitsData['invoiceReference'];
				}
				if($dabitsData['transactionAmount']){
					$debitAmount		= $dabitsData['transactionAmount'];
				}
				if($dabitsData['nominalCode']){
					$debitNominalCode	= $dabitsData['nominalCode'];
				}
			}
			foreach($creditsDatas as $orderId => $creditsData){
				if($skipJournal){
					continue;
				}
				$BPorderId			= $orderId;
				if($creditsData['assignment']['channelId']){
					$channelId			= $creditsData['assignment']['channelId'];
				}
				if($creditsData['taxCode']){
					$taxCode			= $creditsData['taxCode'];
				}
				if($creditsData['invoiceReference']){
					$invoiceReference	= $creditsData['invoiceReference'];
				}
				if($creditsData['transactionAmount']){
					$creditAmount		= $creditsData['transactionAmount'];
				}
				if($creditsData['nominalCode']){
					$creditNominalCode	= $creditsData['nominalCode'];
				}
			}
			if(!$BPorderId){continue;}
			if(!$saveAccId2){continue;}
			if(!$invoiceReference){
				if($allStoredSalesdata[$BPorderId]){
					$invoiceReference	= $allStoredSalesdata[$BPorderId]['bpInvoiceNumber'];
				}
				elseif($allStoredCreditdata[$BPorderId]){
					$invoiceReference	= $allStoredCreditdata[$BPorderId]['bpInvoiceNumber'];
				}
				elseif($allStoredPurchasedata[$BPorderId]){
					$invoiceReference	= $allStoredPurchasedata[$BPorderId]['bpInvoiceNumber'];
				}
				else{
					//
				}
			}
			
			if($allStoredSalesdata[$BPorderId]){
				$OrderType	= 'SO';
			}
			elseif($allStoredCreditdata[$BPorderId]){
				$OrderType	= 'SC';
			}
			elseif($allStoredPurchasedata[$BPorderId]){
				$OrderType	= 'PO';
			}
			
			//cogs for Keen DIST LTD. is stopped, req by Neha on 9th Fab 2023
			if($clientcode == 'keenxero'){if($saveAccId2 == 5){continue;}}
			
			$return[$saveAccId1][$journalsId]	= array(
				'account1Id'		=> $saveAccId1,
				'account2Id'		=> $saveAccId2,
				'journalsId'		=> $journalsId,
				'orderId'			=> $BPorderId,
				'invoiceReference'	=> $invoiceReference,
				'creditAmount'		=> $creditAmount,
				'debitAmount'		=> $debitAmount,
				'journalTypeCode'	=> $jounalResult['journalTypeCode'],
				'creditNominalCode'	=> $creditsDatas[$BPorderId]['nominalCode'],
				'debitNominalCode'	=> $dabitsDatas[$BPorderId]['nominalCode'],
				'taxCode'			=> $taxCode,
				'channelId'			=> $channelId,
				'currencyCode'		=> $allCurrencies[$jounalResult['currencyId']]['code'],
				'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
				'params'			=> json_encode($jounalResult),
				'created'			=> date('Y-m-d H:i:s', strtotime($jounalResult['createdOn'])),
				'OrderType'			=> $OrderType,
			);
			$saveCronTime[]	= strtotime($jounalResult['createdOn']);
			$returnKey++;
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime));
}
return $returns;