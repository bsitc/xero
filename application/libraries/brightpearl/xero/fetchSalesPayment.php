<?php
//xero_demo fetch sales payment from bp
$this->reInitialize();
$returns					= array();
$PaymentReversalEnabled		= 1;
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$disableSOpaymentbptoqbo	= $this->ci->globalConfig['disableSOpaymentbptoqbo'];
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	if($disableSOpaymentbptoqbo){
		continue;
	}
	
	$this->ci->db->reset_query();
	$datas		= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'salespayment'.$account1Id))->row_array();
	$cronTime	= $datas['saveTime'];	
	if(!$cronTime){
		$cronTime	= strtotime('-90 days');
	}
	$cronTime		= strtotime('-90 days');
	$return			= array();
	$updatedTimes	= array();
	$responseDatas	= array();
	$orderId		= array();
	$this->config	= $this->accountConfig[$account1Id];
	
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	$url			= '/accounting-service/customer-payment-search?paymentType=RECEIPT&createdOn='.$cronTime.'/';
	if($PaymentReversalEnabled){
		$url		= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
	}
	
	$paymentResponses	= array();
	$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if($response['results']){
		$paymentResponses[]	= $response;
		if($response['metaData']['resultsAvailable'] > 500){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if($response1['results']){
					$paymentResponses[]	= $response1;
				}

			}
		}
	}
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$objectId	= array_filter($objectId);
		$objectId	= array_unique($objectId);
		sort($objectId);
		$objectIds	= array_chunk($objectId,50);
		foreach($objectIds as $objectId){
			if($objectIds){
				$url		= '/accounting-service/customer-payment-search?paymentType=RECEIPT&orderId='.implode(",",$objectId).'&sort=createdOn.DESC';
				if($PaymentReversalEnabled){
					$url	= '/accounting-service/customer-payment-search?orderId='.implode(",",$objectId).'&sort=createdOn.DESC';
				}
				$response1	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if($response1['results']){
					$paymentResponses[]	= $response1;
				}
			}
		}
	}
	if($paymentResponses){
		$pendingPayments		= array();
		$this->ci->db->reset_query();
		$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,channelId,rowData')->get_where('sales_order',array('isPaymentCreated' => '0','orderId <>' => '','account1Id' => $account1Id))->result_array();
		
		$completedpayments		= array();
		$this->ci->db->reset_query();
		$completedpaymentdatas	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,channelId,rowData')->get_where('sales_order',array('isPaymentCreated' => '1','orderId <>' => '','account1Id' => $account1Id))->result_array();
		
		foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
			$pendingPayments[$pendingPaymentsTemp['orderId']]		= $pendingPaymentsTemp;
		}
		foreach($completedpaymentdatas as $completedpaymentdata){
			$completedpayments[$completedpaymentdata['orderId']]	= $completedpaymentdata;
		}
		
		$allPayKeys		= array();
		foreach($pendingPaymentsTemps as $mappedpayment){
			if($mappedpayment['paymentDetails']){
				$allPayments	= json_decode($mappedpayment['paymentDetails'],true);
				foreach($allPayments as $PKEY => $mappedpaymentdatas){
					if($mappedpaymentdatas['amount'] != 0){
						if(($mappedpaymentdatas['amountCreditedIn'] == 'brightpearl') OR ($mappedpaymentdatas['sendPaymentTo'] == 'xero') OR ($mappedpaymentdatas['paymentInitiatedin'] == 'brightpearl')){
							$allPayKeys[]	= $PKEY;
						}
					}
				}
			}
		}
		foreach($completedpayments as $mappedpayment){
			if($mappedpayment['paymentDetails']){
				$allPayments	= json_decode($mappedpayment['paymentDetails'],true);
				foreach($allPayments as $PKEY => $mappedpaymentdatas){
					if($mappedpaymentdatas['amount'] != 0){
						if(($mappedpaymentdatas['amountCreditedIn'] == 'brightpearl') OR ($mappedpaymentdatas['sendPaymentTo'] == 'xero') OR ($mappedpaymentdatas['paymentInitiatedin'] == 'brightpearl')){
							$allPayKeys[]	= $PKEY;
						}
					}
				}
			}
		}
		sort($allPayKeys);
		
		$allSavedPaysInDB		= array();
		$savedPaymentData		= $this->ci->db->select('paymentId')->get_where('arAdvance',array('reverseByuninvoicing' => 0))->result_array();
		if(!empty($savedPaymentData)){
			$allSavedPaysInDB	= array_column($savedPaymentData, 'paymentId');
			$allSavedPaysInDB	= array_filter($allSavedPaysInDB);
			$allSavedPaysInDB	= array_unique($allSavedPaysInDB);
		}
		
		$batchUpdates	= array();
		$orderIds		= array();
		foreach($paymentResponses as $paymentDatas){
			if($paymentDatas['results']){
				$headerKeys	= array_column($paymentDatas['metaData']['columns'],'name');
				foreach($paymentDatas['results'] as $result){
					$result				= array_combine($headerKeys,$result);
					$orderId			= $result['orderId'];
					$paymentId			= $result['paymentId'];
					$transactionRef		= $result['transactionRef'];
					$transactionCode	= $result['transactionCode'];
					$paymentMethod		= $result['paymentMethodCode'];
					$paymentType		= $result['paymentType'];
					$currencyCode		= $result['currencyCode'];
					$amount				= $result['amountPaid'];
					$paymentDate		= $result['paymentDate'];
					$journalId			= $result['journalId'];
					
					if($clientcode == 'homeleisuredirectxero'){
						$paymentDateCheck	= date('Ymd', strtotime($paymentDate));
						if($paymentDateCheck < '20240108'){
							continue;
						}
					}
					if($clientcode == 'bikesonlinexeroau'){
						if($paymentMethod == '1200'){
							continue;
						}
					}
					
					if($this->config['AllowOnlyGiftPayments']){
						if($paymentMethod != $this->config['giftCardPayment']){
							continue;
						}
					}
					
					if($allSavedPaysInDB){
						if(in_array($paymentId,$allSavedPaysInDB)){
							continue;
						}
					}
					
					if($allPayKeys){
						if(in_array($paymentId,$allPayKeys)){
							continue;
						}
					}
					
					if(strtolower($paymentType) == 'payment'){
						$amount	= '-'.$amount;
					}
					
					if(!isset($pendingPayments[$orderId])){
						if(!isset($completedpayments[$orderId])){
							continue;
						}
					}
					
					$paymentDetails		= array();
					if(isset($batchUpdates[$orderId]['paymentDetails'])){
						$paymentDetails	= $batchUpdates[$orderId]['paymentDetails'];
					}
					elseif(isset($completedpayments[$orderId]['paymentDetails'])){
						$paymentDetails	= json_decode($completedpayments[$orderId]['paymentDetails'],true);
					}
					elseif(isset($pendingPayments[$orderId]['paymentDetails'])){
						$paymentDetails	= json_decode($pendingPayments[$orderId]['paymentDetails'],true);
					}
					if($paymentId){
						if(isset($paymentDetails[$paymentId])){
							if($amount != 0){
								if($paymentDetails[$paymentId]['amount'] != 0){
									continue;
								}
							}
							else{
								continue;
							}
						}
					}
					if($journalId){
						if(isset($paymentDetails[$journalId])){
							if(isset($paymentDetails[$paymentId])){
								if($paymentDetails[$paymentId]['sendPaymentTo'] == 'xero'){
									continue;
								}
							}
						}
					}
					$updatedTimes[]	= strtotime($result['createdOn']);
					$paymentDetails[$paymentId]		= array(
						'amount'						=> $amount,
						'sendPaymentTo'					=> $this->ci->globalConfig['account2Liberary'],
						'status'						=> '0',
						'journalId'						=> $journalId,
						'Reference'						=> $transactionRef,
						'currency'						=> $currencyCode,
						'CurrencyRate'					=> '',
						'paymentMethod'					=> $paymentMethod,
						'paymentType'					=> $paymentType,
						'paymentDate'					=> $paymentDate,
						'paymentInitiatedin'			=> 'brightpearl',
					);
					if(strtolower($paymentDetails[$paymentId]['paymentType']) == 'auth'){
						$paymentDetails[$paymentId]['status']	= 1;
					}
					if($journalId){
						if($result['journalId'] != $result['paymentId']){
							$paymentDetails[$journalId]	= array(
								'amount'					=> 0.00,
								'sendPaymentTo'				=> $this->ci->globalConfig['account2Liberary'],
								'status'					=> '1',
								'journalId'					=> $journalId,
								'Reference'					=> $transactionRef,
								'currency'					=> $currencyCode,
								'CurrencyRate'				=> '',
								'paymentMethod'				=> $paymentMethod,
							);					
						}
					}
					$batchUpdates[$orderId]			= array(
						'paymentDetails'				=> $paymentDetails,
						'orderId'						=> $orderId,
						'sendPaymentTo'					=> $this->ci->globalConfig['account2Liberary'],
					);
				}
			}
		}
		if($batchUpdates){
			if($updatedTimes){
				$saveTime	= max($updatedTimes) - (60*60);
				$this->ci->db->insert('cron_management', array('type' => 'salespayment'.$account1Id, 'runTime' => date('c'), 'saveTime' => $saveTime)); 
			}
			foreach($batchUpdates as $key => $batchUpdate){
				$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
			} 
			$batchUpdates	= array_chunk($batchUpdates,200);
			foreach($batchUpdates as $batchUpdate){
				if($batchUpdate){
					$this->ci->db->update_batch('sales_order',$batchUpdate,'orderId');
				}
			}
		}
	}
}
?>