<?php
$this->reInitialize();
$allCurrencyCodes	= $this->getAllCurrency();
$returns			= array();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$clientcode){continue;}
	
	$channelMaped		= array();
	$AllBPchannelsData	= $this->getAllChannelMethod();
	if($AllBPchannelsData){
		foreach($AllBPchannelsData as $acc1 => $AllBPchannelsDatas){
			foreach($AllBPchannelsDatas as $cID	=> $AllBPchannelsDatass){
				$channelMaped[$cID]	= $AllBPchannelsDatass;
			}
		}
	}
	$rowDataPath	= FCPATH.'rowData'. DIRECTORY_SEPARATOR .'cogs'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR;
	if(!is_dir($rowDataPath)){
		mkdir($rowDataPath,0777,true);
		chmod(dirname($rowDataPath), 0777);
		chown($rowDataPath, 'bsitc');
	}
	
	$return				= array();
	$allJournalsIds		= array();
	$cogsIdAndOrderId	= array();
	$allCogsOrderIds	= array();
	$saveCronTime		= array();
	$this->config		= $this->accountConfig[$account1Id];
	$fetchDataCurrency	= (@$this->config['fetchDataCurrency']) ? (explode(',',$this->config['fetchDataCurrency'])) : array();
	$account2Ids		= $this->account2Details[$account1Id];
	$allCurrencyCode	= $allCurrencyCodes[$account1Id];
	
	$datas		= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'cogswoinvoicejournal'.$account1Id))->row_array();
	$cronTime	= $datas['saveTime'];
	if(!$cronTime){
		$cronTime	= strtotime('-60 days');
	}
	$saveCronTime[]	= $cronTime;
	
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	$goLiveTaxDate	= $this->config['taxDate'];
	
	$savedJournalsIds		= array();
	$savedJournalsIdsData	= $this->ci->db->select('id,journalsId')->get_where('cogs_woinvoice_journal')->result_array();
	if(!empty($savedJournalsIdsData)){
		foreach($savedJournalsIdsData as $tempSavedData){
			$savedJournalsIds[$tempSavedData['journalsId']]	= $tempSavedData;
		}
	}
	$savedJournalsIdsData	= $this->ci->db->select('id,journalsId')->get_where('cogs_woinvoice_journal_archived')->result_array();
	if(!empty($savedJournalsIdsData)){
		foreach($savedJournalsIdsData as $tempSavedData){
			$savedJournalsIds[$tempSavedData['journalsId']]	= $tempSavedData;
		}
	}
	$SaveJournalTypeCodes	= array();
	$JournalTypeCodeAll		= explode(",",$this->config['FetchCOGSJournalType']);
	foreach($JournalTypeCodeAll as $journalTypes){
		if($journalTypes){
			$SaveJournalTypeCodes[]	= strtoupper($journalTypes);
		}
	}
	
	$SaveJournalAccount		= array();
	$JournalAccountAll		= explode(",",$this->config['FetchCOGSJournalNominal']);
	foreach($JournalAccountAll as $journalAcc){
		if($journalAcc){
			$SaveJournalAccount[]	= strtoupper($journalAcc);
		}
	}
	
	if((!empty($SaveJournalAccount)) AND (!empty($SaveJournalTypeCodes))){
		foreach($SaveJournalAccount as $SaveJournalAccountCode){
			foreach($SaveJournalTypeCodes as $SaveJournalTypeCode){
				$url		= '/accounting-service/journal-search?journalType='.$SaveJournalTypeCode.'&nominalCode='.$SaveJournalAccountCode.'&journalDateEntered=' . $cronTime . '/';
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response)) AND (isset($response['results']))){
					$header	= array_column($response['metaData']['columns'],"name");
					foreach($response['results'] as $result){
						$rowResult			= array_combine($header,$result);
						if(!empty($savedJournalsIds)){
							if(isset($savedJournalsIds[$rowResult['journalId']])){continue;}
						}
						$cogsIdAndOrderId[$rowResult['journalId']]	= $rowResult['orderId'];
						$allJournalsIds[]	= $rowResult['journalId'];
						$allCogsOrderIds[]	= $rowResult['orderId'];
					}
					if($response['metaData']){
						for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if((is_array($response1)) AND (isset($response1['results']))){
								foreach($response1['results'] as $result){
									$rowResult			= array_combine($header,$result);
									if(!empty($savedJournalsIds)){
										if(isset($savedJournalsIds[$rowResult['journalId']])){continue;}
									}
									$cogsIdAndOrderId[$rowResult['journalId']]	= $rowResult['orderId'];
									$allJournalsIds[]	= $rowResult['journalId'];
									$allCogsOrderIds[]	= $rowResult['orderId'];
								}
							}
						}
					}
				}
			}
			
		}
	}
	
	$allCogsOrderIds	= array_filter($allCogsOrderIds);
	$allCogsOrderIds	= array_unique($allCogsOrderIds);
	sort($allCogsOrderIds);
	if(empty($allCogsOrderIds)){continue;}
	
	$allJournalsIds		= array_filter($allJournalsIds);
	$allJournalsIds		= array_unique($allJournalsIds);
	sort($allJournalsIds);
	if(empty($allJournalsIds)){continue;}
	
	$journalFetchDatas	= $this->getResultById($allJournalsIds,'/accounting-service/journal',$account1Id,200,'0','');
	if((is_array($journalFetchDatas)) AND (isset($journalFetchDatas['journals'][0]))){
		$cogsOrdersInfos	= array();
		$orderFetchDatas	= $this->getResultById($allCogsOrderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
		if((!is_array($orderFetchDatas)) OR (empty($orderFetchDatas))){continue;}
		foreach($orderFetchDatas as $orderFetchDatasTemp){
			$cogsOrdersInfos[$orderFetchDatasTemp['id']]	= $orderFetchDatasTemp;
		};
		
		foreach($journalFetchDatas as $journalFetchData){
			foreach($journalFetchData as $journalData){
				foreach($account2Ids as $account2Id){
					if(!in_array($journalData['journalTypeCode'],$SaveJournalTypeCodes)){continue;}
					$taxDate		= $journalData['taxDate'];
					$BPDateOffset	= (int)substr($taxDate,23,3);
					$xeroOffset		= 0;
					$diff			= $BPDateOffset - $xeroOffset;
					$date			= new DateTime($taxDate);
					$BPTimeZone		= 'GMT';
					$date->setTimezone(new DateTimeZone($BPTimeZone));
					if($diff){
						$diff	.= ' hour';
						$date->modify($diff);
					}
					$taxDate	= $date->format('Ymd');
					if($taxDate){
						if($taxDate < $goLiveTaxDate){
							continue;
						}
					}
					
					$journalsId		= $journalData['id'];
					$saveAccId1		= $account1Id;
					$saveAccId2		= '';
					$cogsOrderId	= '';
					$cogsOrderinfo	= array();
					if(isset($cogsIdAndOrderId[$journalsId])){
						$cogsOrderId	= $cogsIdAndOrderId[$journalsId];
					}
					else{
						continue;
					}
					if(isset($cogsOrdersInfos[$cogsOrderId])){
						$cogsOrderinfo	= $cogsOrdersInfos[$cogsOrderId];
					}
					else{
						continue;
					}
					if(!empty($savedJournalsIds)){
						if(isset($savedJournalsIds[$journalsId])){continue;}
					}
					
					if(!empty($fetchDataCurrency)){
						if(!in_array(strtolower($cogsOrderinfo['currency']['orderCurrencyCode']),$fetchDataCurrency)){
							continue;
						}
					}
					$config2			= $this->account2Config[$account2Id['id']];
					$assignment			= $cogsOrderinfo['assignment']['current'];
					$cogsChannelId		= ($assignment['channelId'])?($assignment['channelId']): '';
					$cogsWarehouseId	= ($cogsOrderinfo['warehouseId'])?($cogsOrderinfo['warehouseId']): '';
					$config2Channels	= array();
					$config2Warehouses	= array();
					
					if($config2['channelIds']){
						$config2Channels	= explode(",",$config2['channelIds']);
					}
					if($config2['warehouses']){
						$config2Warehouses	= explode(",",$config2['warehouses']);
					}
					
					if(($cogsOrderinfo['orderTypeCode'] == 'SO') OR ($cogsOrderinfo['orderTypeCode'] == 'SC')){
						$customFieldForSalesFetch			= $config2['customFieldForSalesFetch'];
						$customFieldForSalesFetchValue		= $config2['customFieldForSalesFetchValue'];
						$customFieldForSalesFetchValueAll	= array();
						if($customFieldForSalesFetchValue){
							$customFieldForSalesFetchValue	= explode("||",$customFieldForSalesFetchValue);
							foreach($customFieldForSalesFetchValue as $customFieldForSalesFetchValueTemp){
								$customFieldForSalesFetchValueAll[]	= trim(strtolower($customFieldForSalesFetchValueTemp));
							}
						}
						
						if(($customFieldForSalesFetch) AND (isset($cogsOrderinfo['customFields'][$customFieldForSalesFetch])) AND (is_array($customFieldForSalesFetchValueAll)) AND (!empty($customFieldForSalesFetchValueAll))){
							$orderCustomFieldValue		= '';
							if((is_array($cogsOrderinfo['customFields'][$customFieldForSalesFetch])) AND (isset($cogsOrderinfo['customFields'][$customFieldForSalesFetch]['value']))){
								$orderCustomFieldValue	= trim(strtolower($cogsOrderinfo['customFields'][$customFieldForSalesFetch]['value']));
							}
							else{
								$orderCustomFieldValue	= trim(strtolower($cogsOrderinfo['customFields'][$customFieldForSalesFetch]));
							}
							if($orderCustomFieldValue AND (!in_array($orderCustomFieldValue, $customFieldForSalesFetchValueAll))){
								continue;
							}
						}
					}
					
					if($config2Channels OR $config2Warehouses){
						if((!$cogsChannelId) AND (!$cogsWarehouseId)){
							continue;
						}
						if($config2Channels){
							if(!in_array($cogsChannelId,$config2Channels)){
								if((!$cogsChannelId) AND (in_array('NullChannel',$config2Channels))){
									if(!in_array($cogsWarehouseId,$config2Warehouses)){
										continue;
									}
								}
								else{
									continue;
								}
							}
						}
						if($config2Warehouses){
							if(!in_array($cogsWarehouseId,$config2Warehouses)){
								continue;
							}
						}
						$saveAccId2	= $account2Id['id'];
					}
					else{
						$saveAccId2	= $account2Id['id'];
					}
					if(!$saveAccId2){continue;}
					
					$debitAmount	= 0;
					$creditAmount	= 0;
					$allDebits		= $journalData['debits'];
					$allCredits		= $journalData['credits'];
					if(!$allDebits['0']){
						$allDebits	= array($allDebits);
					}
					if(!$allCredits['0']){
						$allCredits	= array($allCredits);
					}
					foreach($allDebits as $allDebitsTemp){
						if(isset($allDebitsTemp['transactionAmount'])){
							$debitAmount	+= $allDebitsTemp['transactionAmount'];
						}
					};
					foreach($allCredits as $allCreditsTemp){
						if(isset($allCreditsTemp['transactionAmount'])){
							$creditAmount	+= $allCreditsTemp['transactionAmount'];
						}
					};
					$channelName	=  '';
					if($cogsChannelId){
						if($channelMaped[$cogsChannelId]){
							$channelName	= $channelMaped[$cogsChannelId]['name'];
						}
					}
					$return[$account1Id][$journalsId]	= array(
						'account1Id'			=> $saveAccId1,
						'account2Id'			=> $saveAccId2,
						'journalsId'			=> $journalsId,
						'taxDate'				=> date('Y-m-d H:i:s', strtotime($taxDate)),
						'journalTypeCode'		=> $journalData['journalTypeCode'],
						'currencyCode'			=> $allCurrencyCode[$journalData['currencyId']]['code'],
						'channelId'				=> $cogsChannelId,
						'channelName'			=> $channelName,
						'creditAmount'			=> $creditAmount,
						'debitAmount'			=> $debitAmount,
						'orderId'				=> $cogsOrderId,
						'OrderType'				=> ($cogsOrderinfo['orderTypeCode'])?($cogsOrderinfo['orderTypeCode']): '',
						'invoiceReference'		=> ($cogsOrderinfo['invoices'][0]['invoiceReference'])?($cogsOrderinfo['invoices'][0]['invoiceReference']): '',
						'invoiceTaxDate'		=> ($cogsOrderinfo['invoices'][0]['taxDate'])?($cogsOrderinfo['invoices'][0]['taxDate']): '',
						'invoiceCurrencyCode'	=> ($cogsOrderinfo['currency']['orderCurrencyCode'])?($cogsOrderinfo['currency']['orderCurrencyCode']): '',
						'invoiceContactID'		=> ($cogsOrderinfo['parties']['customer']['contactId'])?($cogsOrderinfo['parties']['customer']['contactId']): $cogsOrderinfo['parties']['supplier']['contactId'],
						'invoiceAssignment'		=> ($cogsOrderinfo['assignment']['current'])?(json_encode($cogsOrderinfo['assignment']['current'])): '',
						'invoiceExchangeRate'	=> ($cogsOrderinfo['currency']['exchangeRate'])?($cogsOrderinfo['currency']['exchangeRate']): '',
						'invoiceCustomerRef'	=> ($cogsOrderinfo['reference'])?($cogsOrderinfo['reference']): '',
						'invoiceCustomFields'	=> ($cogsOrderinfo['customFields'])?(json_encode($cogsOrderinfo['customFields'])): '',
						'created'				=> date('Y-m-d H:i:s', strtotime($journalData['createdOn'])),
						'params'				=> json_encode($journalData),
					);
					$saveCronTime[]	= strtotime($journalData['createdOn']);
					unset($cogsOrderinfo['orderRows']);
					$storeTempInfo	= json_encode($cogsOrderinfo);
					file_put_contents($rowDataPath.DIRECTORY_SEPARATOR.$cogsOrderId.'.json', $storeTempInfo);
					chown($rowDataPath.DIRECTORY_SEPARATOR.$cogsOrderId.'.json', 'bsitc'); 
				}
			}
		}
		$returns[$account1Id]	= array('return' => $return,'saveTime' => @max($saveCronTime));
	}	
}
return $returns;