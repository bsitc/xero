<?php
$this->reInitialize();
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$this->ci->globalConfig['enableAmazonFeeOther']){continue;}
	
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	
	$journalIds			= array();
	$saveCronTime		= array();
	$cronData			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'amazonfeeother'.$account1Id))->row_array();
	$cronTime			= ((is_array($cronData)) AND (isset($cronData['saveTime'])) AND ($cronData['saveTime'])) ? ($cronData['saveTime']) : (strtotime('-60 days'));
	
	$excludeJournalDes	= (trim($this->config['details'])) ? (trim($this->config['details'])) : ('');
	$saveTaxDate		= ($this->config['taxDate']) ? ($this->config['taxDate']) : ('');
	
	$saveCronTime[]		= $cronTime;
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);
	
	$saveJournalAccount	= array();
	$journalAccountCode	= (trim($this->config['amazonFeeOtherJournalAccount']))?(trim($this->config['amazonFeeOtherJournalAccount'])):'';
	$journalAccountCode	= explode(",",$journalAccountCode);
	$journalAccountCode	= array_filter($journalAccountCode);
	$journalAccountCode	= array_unique($journalAccountCode);
	if(!empty($journalAccountCode)){
		foreach($journalAccountCode as $journalAccountCodes){
			if($journalAccountCodes){
				$saveJournalAccount[]	= trim(strtoupper($journalAccountCodes));
			}
		}
	}
	
	$saveJournalTypes	= array();
	$journalTypeCodes	= (trim($this->config['amazonFeeOtherJournalType']))?(trim($this->config['amazonFeeOtherJournalType'])):'';
	$journalTypeCodes	= explode(",",$journalTypeCodes);
	$journalTypeCodes	= array_filter($journalTypeCodes);
	$journalTypeCodes	= array_unique($journalTypeCodes);
	if(!empty($journalTypeCodes)){
		foreach($journalTypeCodes as $journalTypeCodess){
			if($journalTypeCodess){
				$saveJournalTypes[]	= trim(strtoupper($journalTypeCodess));
			}
		}
	}
	
	if(!$excludeJournalDes OR (empty($saveJournalTypes)) OR (empty($saveJournalAccount))){continue;}
	
	foreach($saveJournalTypes as $saveJournalTypess){
		foreach($saveJournalAccount as $saveJournalAccounts){
			$url		= '/accounting-service/journal-search?journalType='.$saveJournalTypess.'&nominalCode='.$saveJournalAccounts.'&journalDateEntered='.$cronTime.'/';
			$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
			if((is_array($response)) AND (isset($response['results']))){
				$header	= array_column($response['metaData']['columns'],"name");
				foreach($response['results'] as $result){
					$row			= array_combine($header,$result);
					$journalIds[]	= $row['journalId'];
				}
				if($response['metaData']['resultsAvailable'] > 500){
					for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
						$url1		= $url . '&firstResult=' . $i;
						$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
						if((is_array($response1)) AND (isset($response1['results']))){
							foreach($response1['results'] as $result){
								$row			= array_combine($header,$result);
								$journalIds[]	= $row['journalId'];
							}
						}
					}
				}
			}
		}
	}
	
	if(!empty($journalIds)){
		$journalIds	= array_filter($journalIds);
		$journalIds	= array_unique($journalIds);
		sort($journalIds);
	}
	if(empty($journalIds)){continue;}
	
	$isInserted			= 0;
	$batchJournalIds	= array_chunk($journalIds, 200);
	foreach($batchJournalIds as $batchJournalId){
		$jounalResults	= $this->getResultById($batchJournalId,'/accounting-service/journal',$account1Id,200,'0','');
		if((is_array($jounalResults)) AND (!empty($jounalResults)) AND (isset($jounalResults['journals']))){
			$return				= array();
			$allStoredFesData	= array();
			$allStoredFesTemp	= $this->ci->db->select('journalId')->get_where('amazonFeesOther',array('journalId <>' => ''))->result_array();
			if(!empty($allStoredFesTemp)){
				foreach($allStoredFesTemp as $allStoredFesTemps){
					$allStoredFesData[$allStoredFesTemps['journalId']]	= $allStoredFesTemps;
				}
			}
			foreach($jounalResults['journals'] as $jounalResult){
				foreach($account2Ids as $account2Id){
					$journalId		= $jounalResult['id'];
					$channelid		= '';
					$totalAmt		= 0;
					$saveAccId1		= $account1Id;
					$saveAccId2		= '';
					$config2     	= $this->account2Config[$account2Id['id']];
					if(isset($allStoredFesData[$journalId])){continue;}
					if(substr_count(strtolower($jounalResult['description']),strtolower($excludeJournalDes))){continue;}
					
					$taxDate		= $jounalResult['taxDate'];
					$BPDateOffset	= (int)substr($taxDate,23,3);
					$xeroOffset		= 0;
					$diff			= $BPDateOffset - $xeroOffset;
					$date			= new DateTime($taxDate);
					$BPTimeZone		= 'GMT';
					$date->setTimezone(new DateTimeZone($BPTimeZone));
					if($diff){
						$diff	.= ' hour';
						$date->modify($diff);
					}
					$taxDate	= $date->format('Ymd');
					if($taxDate AND $saveTaxDate){
						if($taxDate < $saveTaxDate){continue;}
					}
					
					$debits			= $jounalResult['debits'];
					$credits		= $jounalResult['credits'];
					if(!isset($debits['0'])){$debits = array($debits);}
					if(!isset($credits['0'])){$credits = array($credits);}
					foreach($credits as $credit){
						if(!$channelid){
							$channelid	= $credit['assignment']['channelId'];
						}
						$totalAmt	+= $credit['transactionAmount'];
					}
					foreach($debits as $debit){
						if(!$channelid){
							$channelid	= $debit['assignment']['channelId'];
						}
					}
					
					if($channelid){
						if($config2['channelIds']){
							$config2Channels	= explode(",",$config2['channelIds']);
							if($config2Channels){
								if(in_array($channelid,$config2Channels)){
									$saveAccId2	= $account2Id['id'];
								}
							}
						}
						else{
							$saveAccId2	= $account2Id['id'];
						}
					}
					else{continue;}
					if(!$saveAccId2){continue;}
					
					$return[$journalId]	= array(
						'account1Id'		=> $saveAccId1,
						'account2Id'		=> $saveAccId2,
						'journalId'			=> $journalId,
						'journalTypeCode'	=> $jounalResult['journalTypeCode'],
						'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
						'createdOn'			=> date('Y-m-d H:i:s', strtotime($jounalResult['createdOn'])),
						'currencyId'		=> $jounalResult['currencyId'],
						'channelid'			=> $channelid,
						'exchangeRate'		=> $jounalResult['exchangeRate'],
						'totalAmt'			=> $totalAmt,
						'params'			=> json_encode($jounalResult),
					);
					$saveCronTime[]		= strtotime($jounalResult['createdOn']);
				}
			}
			if(!empty($return)){
				$batchInsert	= array();
				foreach($return as $row){
					$batchInsert[]	= $row;
				}
				if(!empty($batchInsert)){
					$insertResponse	= $this->ci->db->insert_batch('amazonFeesOther', $batchInsert);
					$isInserted		= 1;
				}
			}
		}
	}
	if($isInserted){
		if(!empty($saveCronTime)){
			$saveTime	= max($saveCronTime) - (60*10);
			$this->ci->db->insert('cron_management', array('type' => 'amazonfeeother'.$account1Id, 'runTime' => $cronTime, 'saveTime' => $saveTime)); 
		}
	}
}