<?php
$this->reInitialize();
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$this->ci->globalConfig['enablePrepayments']){continue;}
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'aradvance'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];
	if(!$cronTime){
		$cronTime	= strtotime('-90 days');
	}
	
	$methodsForAdvance	= array();
	$saveCronTime		= array();
	$allOrderIds		= array();
	$orderDatas			= array();
	$orderDatasSO		= array();
	$paymentResponses	= array();
	
	if(strlen(trim($this->config['methodForAdvance'])) > 0){
		$methodsForAdvance	= explode(',', trim($this->config['methodForAdvance']));
	}
	
	$saveCronTime[]	= $cronTime;
	$datetime		= new DateTime(date('c',$cronTime));
	$cronTime		= $datetime->format(DateTime::ATOM);
	$cronTime		= str_replace("+","%2B",$cronTime);
	$url			= '/accounting-service/customer-payment-search?createdOn='.$cronTime.'/';
	$response		= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if(is_array($response) AND (isset($response['results']))){
		$headerKeys		= array_column($response['metaData']['columns'],'name');
		foreach($response['results'] as $resultsTemp){
			if(!empty($methodsForAdvance)){
				if(!in_array($resultsTemp[3], $methodsForAdvance)){
					continue;
				}
			}
			$allOrderIds[]	= $resultsTemp[5];
			$row	= array_combine($headerKeys,$resultsTemp);
			$paymentResponses[$resultsTemp[0]]	= $row;
		};
		if($response['metaData']['resultsAvailable'] > 500){
			for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
				$url1		= $url . '&firstResult=' . $i;
				$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
				if(is_array($response1) AND (isset($response1['results']))){
					foreach($response1['results'] as $resultsTemp){
						if(!empty($methodsForAdvance)){
							if(!in_array($resultsTemp[3], $methodsForAdvance)){
								continue;
							}
						}
						$allOrderIds[]	= $resultsTemp[5];
						$row	= array_combine($headerKeys,$resultsTemp);
						$paymentResponses[$resultsTemp[0]]	= $row;
					};
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$AggregationMappings		= array();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account1ChannelId <>' => ''))->result_array();
	if(!empty($AggregationMappingsTemps)){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$account2Id				= $AggregationMappingsTemp['account2Id'];
			$ConsolMappingChannel	= $AggregationMappingsTemp['account1ChannelId'];
			$AggregationMappings[$account2Id][$ConsolMappingChannel]	= $AggregationMappingsTemp;
		}
	}
	
	if(!empty($allOrderIds)){
		$allOrderIds	= array_filter($allOrderIds);
		$allOrderIds	= array_unique($allOrderIds);
		sort($allOrderIds);
		$orderDatas	= $this->getResultById($allOrderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
	}
	if((is_array($orderDatas)) AND (!empty($orderDatas))){
		foreach($orderDatas as $orderDatasTemp){
			if($orderDatasTemp['orderTypeCode'] != 'SO'){continue;}
			if((isset($orderDatasTemp['invoices'][0]['invoiceReference'])) AND (strlen(trim($orderDatasTemp['invoices'][0]['invoiceReference'])) > 0)){continue;}
			
			$orderChannel	= $orderDatasTemp['assignment']['current']['channelId'];
			$orderWarehouse	= $orderDatasTemp['warehouseId'];
			
			foreach($account2Ids as $account2Id){
				$acc2Config				= $this->account2Config[$account2Id['id']];
				$acc2Warehoues			= array();
				$acc2Warehoues			= explode(",",$acc2Config['warehouses']);
				$acc2Warehoues			= array_filter($acc2Warehoues);
				$acc2Warehoues			= array_unique($acc2Warehoues);
				$acc2Channels			= array();
				$acc2Channels			= explode(",",$acc2Config['channelIds']);
				$acc2Channels			= array_filter($acc2Channels);
				$acc2Channels			= array_unique($acc2Channels);
				$acc2ChannelsAdvances	= array();
				$acc2ChannelsAdvances	= explode(",",$acc2Config['channelForAdvance']);
				$acc2ChannelsAdvances	= array_filter($acc2ChannelsAdvances);
				$acc2ChannelsAdvances	= array_unique($acc2ChannelsAdvances);
				
				if(!empty($acc2Warehoues)){
					if($orderWarehouse){
						if(!in_array($orderWarehouse,$acc2Warehoues)){continue;}
					}
				}
				if(!empty($acc2Channels)){
					if($orderChannel){
						if(!in_array($orderChannel,$acc2Channels)){continue;}
					}
					else{
						if(in_array('NullChannel',$acc2Channels)){
							//
						}
						else{continue;}
					}
				}
				if(!empty($acc2ChannelsAdvances)){
					if($orderChannel){
						if(!in_array($orderChannel,$acc2ChannelsAdvances)){continue;}
					}
					else{
						if(in_array('NullChannel',$acc2ChannelsAdvances)){
							//
						}
						else{continue;}
					}
				}
				if($orderChannel){
					if(!empty($AggregationMappings)){
						if(isset($AggregationMappings[$account2Id['id']][$orderChannel])){continue;}
					}
				}
				$orderDatasSO[$orderDatasTemp['id']]			= $orderDatasTemp;
				$orderDatasSO[$orderDatasTemp['id']]['acc2ID']	= $account2Id['id'];
			}
		}
	}
	
	if((!empty($paymentResponses)) AND (!empty($orderDatasSO))){
		$inserted			= 0;
		$allSavedSOsInDB1	= array();
		$allSavedSOsInDB2	= array();
		$allSavedPaysInDB	= array();
		$savedSOData		= $this->ci->db->select('orderId')->get_where('sales_order')->result_array();
		if(!empty($savedSOData)){
			$allSavedSOsInDB1	= array_column($savedSOData, 'orderId');
			$allSavedSOsInDB1	= array_filter($allSavedSOsInDB1);
			$allSavedSOsInDB1	= array_unique($allSavedSOsInDB1);
		}
		$savedSOData		= $this->ci->db->select('orderId')->get_where('sales_order_archived')->result_array();
		if(!empty($savedSOData)){
			$allSavedSOsInDB2	= array_column($savedSOData, 'orderId');
			$allSavedSOsInDB2	= array_filter($allSavedSOsInDB2);
			$allSavedSOsInDB2	= array_unique($allSavedSOsInDB2);
		}
		$savedPaymentData		= $this->ci->db->select('paymentId')->get_where('arAdvance')->result_array();
		if(!empty($savedPaymentData)){
			$allSavedPaysInDB	= array_column($savedPaymentData, 'paymentId');
			$allSavedPaysInDB	= array_filter($allSavedPaysInDB);
			$allSavedPaysInDB	= array_unique($allSavedPaysInDB);
		}
		$paymentResults	= array_chunk($paymentResponses,200);
		foreach($paymentResults as $paymentResultsTemp){
			$alljournalId	= array();
			$journalDatas	= array();
			$alljournalId	= array_column($paymentResultsTemp, 'journalId');
			$alljournalId	= array_filter($alljournalId);
			$alljournalId	= array_unique($alljournalId);
			if(!empty($alljournalId)){
				$journalDatas	= $this->fetchJournalByIds($alljournalId);
			}
			
			$returnDatas	= array();
			$inserJournalId	= array();
			$returnDatasKey	= 0;
			foreach($paymentResultsTemp as $paymentResponsesTemp){
				if(!isset($orderDatasSO[$paymentResponsesTemp['orderId']])){continue;}
				if(!empty($allSavedSOsInDB1)){
					if(in_array($paymentResponsesTemp['orderId'],$allSavedSOsInDB1)){continue;}
				}
				if(!empty($allSavedSOsInDB2)){
					if(in_array($paymentResponsesTemp['orderId'],$allSavedSOsInDB2)){continue;}
				}
				if(!empty($allSavedPaysInDB)){
					if(in_array($paymentResponsesTemp['paymentId'],$allSavedPaysInDB)){continue;}
				}
				$orderInfoData	= $orderDatasSO[$paymentResponsesTemp['orderId']];
				$paymentDate	= $paymentResponsesTemp['paymentDate'];
				
				$acc1Offset		= (int)substr($paymentDate,23,3);
				$acc2Offset		= 0;
				$diff			= 0;
				$diff			= $acc1Offset - $acc2Offset;
				$date			= new DateTime($paymentDate);
				$acc1TimeZone	= 'GMT';
				$date->setTimezone(new DateTimeZone($acc1TimeZone)); 
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$paymentDate	= $date->format('Y-m-d');
				
				$acc2Config		= $this->account2Config[$orderInfoData['acc2ID']];
				if((strtolower($acc2Config['defaultCurrency'])) != (strtolower($paymentResponsesTemp['currencyCode']))){
					continue;
				}
				
				$returnDatas[$returnDatasKey]	= array(
					'account1Id'		=> $account1Id,
					'account2Id'		=> $orderInfoData['acc2ID'],
					'paymentId'			=> $paymentResponsesTemp['paymentId'],
					'transactionRef'	=> $paymentResponsesTemp['transactionRef'],
					'transactionCode'	=> $paymentResponsesTemp['transactionCode'],
					'paymentMethodCode'	=> $paymentResponsesTemp['paymentMethodCode'],
					'paymentType'		=> $paymentResponsesTemp['paymentType'],
					'orderId'			=> $paymentResponsesTemp['orderId'],
					'currencyId'		=> $paymentResponsesTemp['currencyId'],
					'currencyCode'		=> $paymentResponsesTemp['currencyCode'],
					'amountAuthorized'	=> $paymentResponsesTemp['amountAuthorized'],
					'amountPaid'		=> $paymentResponsesTemp['amountPaid'],
					'expires'			=> date('Y-m-d H:i:s', strtotime($paymentResponsesTemp['expires'])),
					'paymentDate'		=> date('Y-m-d H:i:s', strtotime($paymentDate)),
					'createdOn'			=> date('Y-m-d H:i:s', strtotime($paymentResponsesTemp['createdOn'])),
					'journalId'			=> $paymentResponsesTemp['journalId'],
					'contactId'			=> $orderInfoData['parties']['customer']['contactId'],
					'journalTypeCode'	=> (isset($journalDatas[$paymentResponsesTemp['journalId']]))?($journalDatas[$paymentResponsesTemp['journalId']]['journalTypeCode']):'',
					'exchangeRate'		=> (isset($journalDatas[$paymentResponsesTemp['journalId']]))?($journalDatas[$paymentResponsesTemp['journalId']]['exchangeRate']):'',
					'description'		=> (isset($journalDatas[$paymentResponsesTemp['journalId']]))?($journalDatas[$paymentResponsesTemp['journalId']]['description']):'',
					'orderTotal'		=> $orderInfoData['totalValue']['total'],
					'orderLineDetails'	=> json_encode($orderInfoData['orderRows']),
					'addresses'			=> json_encode($orderInfoData['parties']),
					'channelId'			=> $orderInfoData['assignment']['current']['channelId'],
					'customerRef'		=> $orderInfoData['reference'],
				);
				$inserJournalId[]	= $paymentResponsesTemp['journalId'];
				$saveCronTime[]		= strtotime($paymentResponsesTemp['createdOn']);
				$returnDatasKey++;
			}
			if(!empty($returnDatas)){
				$inserted	= $this->ci->db->insert_batch('arAdvance', $returnDatas); 
			}
		}
		if($inserted){
			if(!empty($saveCronTime)){
				$saveTime	= max($saveCronTime) - (60*60);
				$this->ci->db->insert('cron_management', array('type' => 'aradvance'.$account1Id, 'runTime' => date('c'), 'saveTime' => $saveTime)); 
			}
			else{
				$saveCronTime[]	= strtotime('-90 days');
				$saveTime	= max($saveCronTime) - (60*60);
				$this->ci->db->insert('cron_management', array('type' => 'aradvance'.$account1Id, 'runTime' => date('c'), 'saveTime' => $saveTime)); 
			}
		}
	}
}