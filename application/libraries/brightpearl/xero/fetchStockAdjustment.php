<?php
//xero
$this->reInitialize();
$enableInventoryTransfer	= $this->ci->globalConfig['enableInventoryTransfer'];
$clientcode	= $this->ci->config->item('clientcode');
$returns	= array();
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$clientcode){continue;}
	$WareHousedatas			= $this->ci->db->get_where('mapping_warehouse', array('account1Id' => $account1Id))->result_array();
	$account2WareHouses		= array();
	if($WareHousedatas){
		foreach($WareHousedatas as $WareHousedata){
			$account2WareHouses[$WareHousedata['account2Id']][]	= $WareHousedata['account1WarehouseId'];
		}
	}
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'stockadjustment'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];	
	$saveCronTime			= array();
	$saveCronTime[]			= $datas['saveTime'];
	$return				= array();
	
	if(!$cronTime){
		$cronTime	= strtotime('-30 days');
	}
	$datetime			= new DateTime(date('c',$cronTime));
	$cronTime			= $datetime->format(DateTime::ATOM);
	$cronTime			= str_replace("+","%2B",$cronTime);
	$this->config		= $this->accountConfig[$account1Id];			
	$account2Ids		= $this->account2Details[$account1Id];
	$responses			= array();
	if($enableInventoryTransfer){
		$url	= '/warehouse-service/goods-movement-search?sort=goodsNoteId.DESC&updatedOn='.$cronTime.'/';
	}
	else{
		$url	= '/warehouse-service/goods-movement-search?sort=goodsNoteId.DESC&goodsNoteTypeCode=SC&updatedOn='.$cronTime.'/';
	}
	$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id]; 
	if($response['results']){
		$responses[]	= $response; 
	}
	if($response['metaData']){
		for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
			$url1		= $url . '&firstResult=' . $i;
			$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
			if($response1['results']){
				$responses[]	= $response1; 
			}
		}
	}
	$goodoutIDS	= array();
	$headers	= array_column($response['metaData']['columns'],'name');
	foreach($responses as $response){
		foreach($response['results'] as $mydata){
			$row	= array_combine($headers,$mydata);
			if($row['goodsNoteTypeCode'] == 'GO'){
				$goodoutIDS[]	= $row['goodsNoteId'];
			}
		}
	}
	$goodoutIDS	= array_filter($goodoutIDS);
	$goodoutIDS	= array_unique($goodoutIDS);
	sort($goodoutIDS);
	$stockAdjusmentDatas	= array();
	if($goodoutIDS){
		$stockAdjusmentDatas	= $this->getResultById($goodoutIDS,'/warehouse-service/order/*/goods-note/goods-out/',$account1Id,200,1);
	}
	
	$all_goodnote	= array();
	$unique_result	= array();
	foreach($responses as $response){
		$all_goodnote[]	= $response['results'];
	}
	foreach($all_goodnote as $all_goodnotetemp){
		foreach($all_goodnotetemp as $unique_results){
			if($unique_results[14] != 'GO'){
				if(empty($unique_result[$unique_results[0]][$unique_results[1]])){
					$unique_result[$unique_results[0]][$unique_results[1]]	= $unique_results;
				}
			}
			else{
				if($unique_results[4] < 0){
					if(empty($unique_result[$unique_results[0]][$unique_results[1]])){
						$unique_result[$unique_results[0]][$unique_results[1]]	= $unique_results;
					}
				}
			}
		}
	}
	$readyData	= array();
	foreach($unique_result as $TransferDatas){
		foreach($TransferDatas as $TransferData){
			$readyData[]	= $TransferData;	
		}
	}
	$goLiveDate	= $this->config['taxDate'];
	if($readyData){
		$headers	= array_column($response['metaData']['columns'],'name');
		foreach($readyData as $results){
			$row		= array_combine($headers,$results);
			$updated	= date('Ymd',strtotime($row['updatedOn']));
			
			if($row['goodsNoteTypeCode'] == 'GO'){
				$NewTransferData	= $stockAdjusmentDatas[$row['goodsNoteId']];
				$ShippedDate		= $NewTransferData['status']['shippedOn'];
				$ShippedDate		= date('Ymd',strtotime($ShippedDate));
				if($goLiveDate){
					if($ShippedDate < $goLiveDate){
						continue; 
					}
				}
			}
			else{
				if($goLiveDate){
					if($updated < $goLiveDate){
						continue; 
					}
				}
			}
			if($row['salesOrderRowId'] OR $row['purchaseOrderRowId']){
				if($row['orderId']){
					continue;
				}
			}
			$warehouseId	= $row['warehouseId'];
			foreach($account2Ids as $account2Id){
				$tempSaveAcc1	= $account1Id; 
				$tempSaveAcc2	= '';
				$config2		= $this->account2Config[$account2Id['id']];
				if($row['goodsNoteTypeCode'] != 'GO' AND $row['goodsNoteTypeCode'] != 'SC'){
					continue;
				}
				$config2Warehouses	= $account2WareHouses[$account2Id['id']];
				if($config2Warehouses){
					if((!$warehouseId)){
						continue;
					}
					if(!in_array($warehouseId,$config2Warehouses)){
						continue;
					}
					$tempSaveAcc2	= $account2Id['id'];
				}
				else{
					continue;
					$tempSaveAcc2	= $account2Id['id'];
				}
				if(!$tempSaveAcc2){
					continue;
				}
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc1) : $tempSaveAcc2;			
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc2) : $tempSaveAcc1;			
				if($row['goodsNoteTypeCode'] == 'GO'){
					if($fetchType){
						if($fetchType != 'GO'){
							continue;
						}
					}
					$goodsNoteId		= $row['goodsNoteId'];
					$stockAdjusmentData	= $stockAdjusmentDatas[$goodsNoteId];
					if(!$stockAdjusmentData){
						continue;
					}
					if(!$stockAdjusmentData['status']['shipped']){
						continue;
					}
					$shippingDate		= $stockAdjusmentData['status']['shippedOn'];
					$sourceWarehouse	= $stockAdjusmentData['warehouseId'];
					$targetWarehouse	= $stockAdjusmentData['targetWarehouseId'];
					$GOid				= $stockAdjusmentData['stockTransferId'];
					if(!$GOid){
						continue;
					}
					if($stockAdjusmentData['orderId']){
						continue;
					}
					if($sourceWarehouse == $targetWarehouse){
						continue;
					}
					foreach($account2WareHouses as $AccountIdNew => $account2WareHousesData){
						$saveWarehouses	= $account2WareHousesData;
						if((in_array($sourceWarehouse,$saveWarehouses)) AND (!in_array($targetWarehouse,$saveWarehouses))){
							$saveAccId2		= $AccountIdNew;
							$return[$account1Id][$row['goodsMovementId']][uniqid()]	= array(
								'account1Id'	=> $saveAccId1,
								'account2Id'    => $saveAccId2,
								'orderId' 		=> $row['goodsMovementId'],
								'warehouseId'	=> $sourceWarehouse,
								'productId' 	=> $row['productId'],
								'qty' 			=> $row['quantity'],
								'price' 		=> $row['productValue'],
								'created' 		=> $row['updatedOn'],
								'rowData' 		=> json_encode($row),
								'GoodNotetype'	=> 'GO',
								'ActivityId'	=> $GOid,
								'goodsNoteId'	=> $row['goodsNoteId'],
								'shippedDate'	=> $shippingDate,
							);
						}
						if((in_array($targetWarehouse,$saveWarehouses)) AND (!in_array($sourceWarehouse,$saveWarehouses))){
							$saveAccId2		= $AccountIdNew;
							$return[$account1Id][$row['goodsMovementId']][uniqid()]	= array(
								'account1Id'	=> $saveAccId1,
								'account2Id'    => $saveAccId2,
								'orderId' 		=> $row['goodsMovementId'],
								'warehouseId'	=> $targetWarehouse,
								'productId' 	=> $row['productId'],
								'qty' 			=> (-1) * ($row['quantity']),
								'price' 		=> $row['productValue'],
								'created' 		=> $row['updatedOn'],
								'rowData' 		=> json_encode($row),
								'GoodNotetype'	=> 'GO',
								'ActivityId'	=> $GOid,
								'goodsNoteId'	=> $row['goodsNoteId'],
								'shippedDate'	=> $shippingDate,
							);
						}
					}
				}
				else if($row['goodsNoteTypeCode'] == 'SC'){
					if($clientcode == 'stitchandstoryxero'){
						continue;
					}
					if($clientcode == 'pigletxero'){
						continue;
					}
					if($fetchType){
						if($fetchType != 'SC'){
							continue;
						}
					}
					$return[$account1Id][$row['goodsMovementId']][$row['productId']]	= array(
						'account1Id'		=> $saveAccId1,
						'account2Id'        => $saveAccId2,
						'orderId' 		    => $row['goodsMovementId'],
						'warehouseId'	    => $row['warehouseId'],
						'productId' 	    => $row['productId'],
						'qty' 			    => $row['quantity'],
						'price' 		    => $row['productValue'],
						'created' 		    => $row['updatedOn'],
						'rowData' 		    => json_encode($row),
						'GoodNotetype'	    => 'SC',
						'ActivityId'	    => '',
						'goodsNoteId'		=> $row['goodsNoteId'],
						'shippedDate'		=> date('Y-m-d'),
					);
				}
				$saveCronTime[]	= strtotime($results['15']);
			}
		}
	}
	$returns[$account1Id]	= array('return' => $return,'saveTime' => @max($saveCronTime)); 
}
?>