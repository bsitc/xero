<?php
$this->reInitialize();
$PaymentReversalEnabled		= 1;
$disableSOpaymentqbotobp	= $this->ci->globalConfig['disableSOpaymentqbotobp'];
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$clientcode){continue;}
	if($disableSOpaymentqbotobp){continue;}
	
	$exchangeDatas	= $this->getExchangeRate($account1Id)[$account1Id];
	$this->config	= $this->accountConfig[$account1Id];
	if($objectId){
		$this->ci->db->where_in('orderId',$objectId);
	}	
	$orderDatas		= $this->ci->db->get_where('sales_order',array('createOrderId <>' => '', 'paymentDetails <>' => '', 'status <=' => '3', 'account1Id' => $account1Id))->result_array();
	if(empty($orderDatas)){continue;}
	
	
	/****paymentMappings******/
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account1Id' => $account1Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();
	$paymentMappings1		= array();
	$paymentMappings2		= array();
	$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account2Id			= $paymentMappingsTemp['account2Id'];
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$account2PaymentId	= strtolower(trim($paymentMappingsTemp['account2PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if($paymentchannelIds AND $paymentcurrencys){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$account2Id][$paymentchannelId][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
					}
				}
			}
			elseif($paymentchannelIds AND !$paymentcurrencys){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$account2Id][$paymentchannelId][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif(!$paymentchannelIds AND $paymentcurrencys){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[$account2Id][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif(!$paymentchannelIds AND !$paymentcurrencys){
				$paymentMappings[$account2Id][$account2PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	/*****end******/
	
	$orderInfosDatas		= array();
	$orderIds				= array_column($orderDatas,'orderId');
	$orderInfosDatasTemps	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
	if((is_array($orderInfosDatasTemps)) AND (!empty($orderInfosDatasTemps)) AND (isset($orderInfosDatasTemps[0]))){
		foreach($orderInfosDatasTemps as $orderInfosDatasTemp){
			$orderInfosDatas[$orderInfosDatasTemp['id']]	= $orderInfosDatasTemp;
		}
	}
	
	foreach($orderDatas as $orderData){
		if($orderData['sendInAggregation']){continue;}
		if($clientcode == 'amistry'){if($orderData['account2Id'] == 2){continue;}}	//kitKing company sales is disabled for now, account Id of kitKing is 2
		
		$account2Id		= $orderData['account2Id'];
		$config2		= $this->account2Config[$account2Id];
		$orderId		= $orderData['orderId'];
		$timezone		= $this->config['timezone'];
		$rowData		= json_decode($orderData['rowData'],true);
		$channelId		= $rowData['assignment']['current']['channelId'];
		$currencyCode	= $rowData['currency']['accountingCurrencyCode'];
		$createdRowData	= json_decode($orderData['createdRowData'],true);
		$paymentDetails	= json_decode($orderData['paymentDetails'],true);
		$fetchAgain		= 0;
		$orderPaidOnBP	= 0;
		if(!is_array($paymentDetails)){continue;}
		if(empty($paymentDetails)){continue;}
		
		if((!empty($orderInfosDatas)) AND (isset($orderInfosDatas[$orderId]))){
			if($orderInfosDatas[$orderId]['orderPaymentStatus'] == 'PAID'){
				$orderPaidOnBP	= 1;;
			}
		}
		
		/* //	Paymentreversal Code Starts	// */
		if($PaymentReversalEnabled){
			foreach($paymentDetails as $pKey => $paymentDetail){
				if(($paymentDetail['sendPaymentTo'] == 'brightpearl') AND ($paymentDetail['IsReversal'] == '1') AND ($paymentDetail['DeletedonBrightpearl'] == 'NO') AND ($paymentDetail['status'] == 0) AND ($paymentDetail['BrightpearlPayID'])){
					$fetchAgain		= 1;
					$updateArray	= array();
					$parentPKey		= $paymentDetail['BrightpearlPayID'];
					$reversePayDate	= date('Y-m-d');
					if($paymentDetail['paymentDate']){
						$reversePayDate	= $paymentDetail['paymentDate'];
						$dDate			= date('Y-m-d',strtotime($reversePayDate));
						$date			= new DateTime($dDate);
						$date->setTimezone(new DateTimeZone($timezone));
						$reversePayDate	= $date->format('Y-m-d');
					}
					$reverseRequest	= array(
						"paymentMethodCode"	=> $paymentDetails[$parentPKey]['paymentMethod'],
						"paymentType"		=> "PAYMENT",
						"orderId"			=> $orderId,
						"currencyIsoCode"	=> strtoupper($paymentDetails[$parentPKey]['currency']),
						"exchangeRate"		=> $paymentDetails[$parentPKey]['exchangeRate'],
						"amountPaid"		=> $paymentDetails[$pKey]['amount'],
						"paymentDate"		=> $reversePayDate,
						"journalRef"		=> "Reverse Payment From Xero",
					);
					$reversepayurl		= "/accounting-service/customer-payment";
					$reverseResult		= $this->getCurl($reversepayurl, "POST", json_encode($reverseRequest), 'json' , $account1Id )[$account1Id];
					$createdRowData['Send Void Payment to Brightpearl Request	:'.$pKey]	= $reverseRequest;
					$createdRowData['Send Void Payment to Brightpearl Response	:'.$pKey]	= $reverseResult;
					if((!$reverseResult['errors']) AND (is_int($reverseResult))){
						$updateArray	= array();
						$paymentDetails[$pKey]['DeletedonBrightpearl']	= "YES"; 
						$paymentDetails[$pKey]['status']	= 1;
						$paymentDetails[$pKey]['reverseby']	= "xero";
						$paymentDetails[$reverseResult]		= array(
							'amount'				=> $paymentDetails[$pKey]['amount'],
							'status'				=> '1',
							'AmountReversedIn'		=> 'brightpearl',
							'ParentXeroReverseId'	=> $pKey,
						);
						$updateArray['status']				= 1;
						$updateArray['paymentDetails']		= json_encode($paymentDetails);
						$updateArray['createdRowData']		= json_encode($createdRowData);
						$updateArray['isPaymentCreated']	= 0;
						$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order',$updateArray);
					}
					else{
						$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order',array('createdRowData' => json_encode($createdRowData)));
					}
				}
			}
			if($fetchAgain){
				$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $orderId))->row_array();
				$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true); 
			}
		}
		/* PAYMENT REVERSAL CODE ENDS */
		
		foreach($paymentDetails as $keys => $paymentDetail){
			if(($paymentDetail['sendPaymentTo'] == 'brightpearl') AND ($paymentDetail['status'] == '0') AND ($paymentDetail['IsReversal'] != '1')){
				$amount	= 0;
				$amount	= $paymentDetail['amount'];
				if($amount > 0 ){
					if($orderPaidOnBP){
						continue;
					}
					$reference			= '';
					$orderPaymentMethod	= '';
					$exchangeRate		= 1;
					$currencyCode		= $rowData['currency']['accountingCurrencyCode'];
					$paymentMethod		= $this->config['defaultPaymentMethod'];
					if($paymentDetail['Reference']){
						$reference			= $paymentDetail['Reference'];
					}
					if($paymentDetail['paymentMethod']){
						$orderPaymentMethod	= $paymentDetail['paymentMethod'];
					}
					if($paymentDetail['CurrencyRate']){
						$exchangeRate		= $paymentDetail['CurrencyRate'];
					}
					if($paymentDetail['currency']){
						$currencyCode		= $paymentDetail['currency'];
					}		
					if($orderPaymentMethod){
						if(isset($paymentMappings1[$orderData['account2Id']][$channelId][strtolower($currencyCode)][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings1[$orderData['account2Id']][$channelId][strtolower($currencyCode)][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						
						else if(isset($paymentMappings2[$orderData['account2Id']][$channelId][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings2[$orderData['account2Id']][$channelId][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						
						else if(isset($paymentMappings3[$orderData['account2Id']][strtolower($currencyCode)][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings3[$orderData['account2Id']][strtolower($currencyCode)][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						
						else if(isset($paymentMappings[$orderData['account2Id']][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings[$orderData['account2Id']][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						/* $paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
						if($paymentMathodMappings){
							$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
						} */
					}
					if($paymentDetail['paymentDate']){
						$paymentDate	= $paymentDetail['paymentDate'];
						$dDate			= date('Y-m-d',strtotime($paymentDate));
						$date			= new DateTime($dDate);
						$date->setTimezone(new DateTimeZone($timezone));
						$paymentDate	= $date->format('Y-m-d');
					}
					else{
						$paymentDate	= date('Y-m-d');
					}
					$customerPaymentRequest	= array(
						"paymentMethodCode"		=> $paymentMethod,
						"paymentType"			=> "RECEIPT",
						"orderId"				=> $orderId,
						"currencyIsoCode"		=> strtoupper($currencyCode),
						"exchangeRate"			=> $exchangeRate,
						"amountPaid"			=> $amount,
						"paymentDate"			=> $paymentDate,
						"journalRef"			=> ($reference) ? ($reference):"Sales Order Payment for OrderID : ".$orderId,
					);
					$payurl						= '/accounting-service/customer-payment';
					$customerPaymentRequestRes	= $this->getCurl( $payurl, "POST", json_encode($customerPaymentRequest), 'json' , $account1Id )[$account1Id];
					$createdRowData['Brightpearl Payment Request	:'.$keys]	= $customerPaymentRequest;
					$createdRowData['Brightpearl Payment Response	:'.$keys]	= $customerPaymentRequestRes;
					$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order',array('createdRowData' => json_encode($createdRowData)));
					if(!isset($customerPaymentRequestRes['errors'])){
						if(is_int($customerPaymentRequestRes)){
							$updateArray	= array();
							$paymentDetails[$keys]['status']			= '1';
							$paymentDetails[$keys]['BrightpearlPayID']	= $customerPaymentRequestRes;
							$paymentDetails[$customerPaymentRequestRes]	= array(
								'amount' 			=> $amount,
								'status' 			=> '1',
								'amountCreditedIn'	=> 'brightpearl',
								'parentPaymentId'	=> $keys,
								'paymentMethod'		=> $paymentMethod,
								'currency'			=> strtoupper($currencyCode),
								"exchangeRate"		=> $exchangeRate,
							);
							$OrderInfoUrl		= '/order-service/order/'.$orderId;
							$OrderInfoResults	= $this->getCurl($OrderInfoUrl, 'get', '', 'json', $account1Id)[$account1Id];
							if($OrderInfoResults){
								$PaymentStatus	= $OrderInfoResults[0]['orderPaymentStatus'];
								if(($PaymentStatus == 'PAID') OR ($PaymentStatus == 'NOT_APPLICABLE')){
									$updateArray	= array(
										'isPaymentCreated'	=> '1',
										'status'			=> '3',
									);
								}
							}
							$updateArray['paymentDetails']	= json_encode($paymentDetails);
							$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('sales_order',$updateArray); 
						}
					}
				}
			}
		}
	}
}