<?php
$this->reInitialize();
$PaymentReversalEnabled		= 1;
$disablePOpaymentqbotobp	= $this->ci->globalConfig['disablePOpaymentqbotobp'];
foreach($this->accountDetails as $account1Id => $accountDetails){
	if($disablePOpaymentqbotobp){
		continue;
	}
	
	$exchangeDatas	= $this->getExchangeRate($account1Id)[$account1Id];
	$this->config	= $this->accountConfig[$account1Id];
	if($objectId){
		$this->ci->db->where_in('orderId',$objectId);
	}
	$orderDatas		= $this->ci->db->get_where('purchase_order',array('sendInAggregation' => 0, 'createOrderId <>' => '', 'paymentDetails <>' => '', 'status <=' => '3', 'account1Id' => $account1Id))->result_array();
	if(!$orderDatas){
		continue;
	}
	
	/****paymentMappings******/
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account1Id' => $account1Id,'applicableOn' => 'purchase'))->result_array();
	$paymentMappings		= array();
	$paymentMappings1		= array();
	$paymentMappings2		= array();
	$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account2Id			= $paymentMappingsTemp['account2Id'];
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$account2PaymentId	= strtolower(trim($paymentMappingsTemp['account2PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if($paymentchannelIds AND $paymentcurrencys){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$account2Id][$paymentchannelId][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
					}
				}
			}
			elseif($paymentchannelIds AND !$paymentcurrencys){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$account2Id][$paymentchannelId][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif(!$paymentchannelIds AND $paymentcurrencys){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[$account2Id][strtolower($paymentcurrency)][$account2PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif(!$paymentchannelIds AND !$paymentcurrencys){
				$paymentMappings[$account2Id][$account2PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	/*****end******/
	$orderInfosDatas		= array();
	$orderIds				= array_column($orderDatas,'orderId');
	$orderInfosDatasTemps	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
	foreach($orderInfosDatasTemps as $orderInfosDatasTemp){
		$orderInfosDatas[$orderInfosDatasTemp['id']]	= $orderInfosDatasTemp;
	}
	
	foreach($orderDatas as $orderData){
		if($orderData['uninvoiceCount'] > 0){
			if($orderData['PaymentState'] == 2){
				continue;
			}
		}
		$account2Id					= $orderData['account2Id'];
		$config2					= $this->account2Config[$account2Id];
		$orderId					= $orderData['orderId'];
		$timezone					= $this->config['timezone'];
		$rowData					= json_decode($orderData['rowData'],true);
		$channelId					= $rowData['assignment']['current']['channelId'];
		$createdRowData				= json_decode($orderData['createdRowData'],true);
		$paymentDetails				= json_decode($orderData['paymentDetails'],true);
		$FetchAgain					= 0;
		if(!$paymentDetails){
			continue;
		}
		$orderPaidOnBP				= 0;
		$orderInfosData				= array();
		$orderInfosData				= $orderInfosDatas[$orderId];
		if($orderInfosData['orderPaymentStatus'] == 'PAID'){
			$orderPaidOnBP			= 1;
		}
		
		/* //	Paymentreversal Code Starts	// */
		if($PaymentReversalEnabled){
			foreach($paymentDetails as $Mykey => $paymentDetail){
				if(($paymentDetail['sendPaymentTo'] == 'brightpearl') AND ($paymentDetail['IsReversal'] == '1') AND ($paymentDetail['DeletedonBrightpearl'] == 'NO')){
					if(($paymentDetail['status'] == 0)){
						if($paymentDetail['brightpearlPayID']){
							$FetchAgain			= 1;
							$updateArray		= array();
							$paymentKEY			= $paymentDetail['brightpearlPayID'];
							if($paymentDetail['paymentDate']){
								$ReversepaymentDate	= $paymentDetail['paymentDate'];
								$dDate				= date('Y-m-d',strtotime($ReversepaymentDate));
								$date				= new DateTime($dDate); 
								$date->setTimezone(new DateTimeZone($timezone));
								$ReversepaymentDate	= $date->format('Y-m-d');
							}
							else{
								$ReversepaymentDate	= date('Y-m-d');
							}
							$reversepayurl		= "/accounting-service/supplier-payment";
							$reverseRequest		= array(
								"paymentMethodCode"	=> $paymentDetails[$paymentKEY]['paymentMethod'],
								"paymentType"		=> "RECEIPT",
								"orderId"			=> $orderId,
								"currencyIsoCode"	=> strtoupper($paymentDetails[$paymentKEY]['currency']),
								"exchangeRate"		=> $paymentDetails[$paymentKEY]['exchangeRate'],
								"amountPaid"		=> $paymentDetails[$Mykey]['amount'],
								"paymentDate"		=> $ReversepaymentDate,
								"journalRef"		=> "Reverse Payment From Xero",
							);
							if($reverseRequest['paymentDate']){
								$dDate							= date('Y-m-d',strtotime($reverseRequest['paymentDate']));
								$date							= new DateTime($dDate); 
								$date->setTimezone(new DateTimeZone($timezone));
								$reverseRequest['paymentDate']	= $date->format('Y-m-d');
							}
							else{
								$reverseRequest['paymentDate']	= date('c');
							}
							$ReverseResponse	= $this->getCurl($reversepayurl, "POST", json_encode($reverseRequest), 'json' , $account1Id )[$account1Id];
							$createdRowData['Send Void Payment to Brightpearl Request	:'.$Mykey]	= $reverseRequest;
							$createdRowData['Send Void Payment to Brightpearl Response	:'.$Mykey]	= $ReverseResponse;
							$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('purchase_order',array('createdRowData' => json_encode($createdRowData)));
							if(!$ReverseResponse['errors']){
								if(is_int($ReverseResponse)){
									$paymentDetails[$Mykey]['status']				= 1;
									$paymentDetails[$Mykey]['DeletedonBrightpearl']	= "YES";
									$paymentDetails[$Mykey]['reverseby']			= "xero";
									$paymentDetails[$ReverseResponse]	= array(
										'amount' 				=> $paymentDetails[$Mykey]['amount'],
										'status' 				=> '1',
										'AmountReversedIn'		=> 'brightpearl',
										'ParentXeroReverseId'	=> $Mykey,
									);
									$updateArray['isPaymentCreated']	= 0;
									$updateArray['status']				= 1;
									$updateArray['paymentDetails']		= json_encode($paymentDetails);
									$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('purchase_order',$updateArray); 
								}
							}
						}
					}
				}
			}
			if($FetchAgain){
				$updatedPaymentdata	= $this->ci->db->select('paymentDetails')->get_where('purchase_order',array('orderId' => $orderId))->row_array();
				$paymentDetails		= json_decode($updatedPaymentdata['paymentDetails'],true); 
			}
		}
		/*	PAYMENT REVERSAL ENDS */
		
		foreach($paymentDetails as $keys => $paymentDetail){
			if(($paymentDetail['sendPaymentTo'] == 'brightpearl') AND ($paymentDetail['status'] == '0') AND ($paymentDetail['IsReversal'] != '1')){
				$amount	= 0;
				$amount	= $paymentDetail['amount'];
				if($amount > 0 ){
					if($orderPaidOnBP){
						continue;
					}
					$reference			= '';
					$orderPaymentMethod	= '';
					$exchangeRate		= 1;
					$currencyCode		= $rowData['currency']['accountingCurrencyCode'];
					$paymentMethod		= $this->config['defaultPaymentMethod'];
					if($paymentDetail['Reference']){
						$reference			= $paymentDetail['Reference'];
					}
					if($paymentDetail['paymentMethod']){
						$orderPaymentMethod	= $paymentDetail['paymentMethod'];
					}
					if($paymentDetail['CurrencyRate']){
						$exchangeRate		= $paymentDetail['CurrencyRate'];
					}
					if($paymentDetail['currency']){
						$currencyCode		= $paymentDetail['currency'];
					}		
					if($orderPaymentMethod){
						if(isset($paymentMappings1[$orderData['account2Id']][$channelId][strtolower($currencyCode)][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings1[$orderData['account2Id']][$channelId][strtolower($currencyCode)][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						
						else if(isset($paymentMappings2[$orderData['account2Id']][$channelId][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings2[$orderData['account2Id']][$channelId][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						
						else if(isset($paymentMappings3[$orderData['account2Id']][strtolower($currencyCode)][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings3[$orderData['account2Id']][strtolower($currencyCode)][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						
						else if(isset($paymentMappings[$orderData['account2Id']][strtolower($orderPaymentMethod)])){
							$paymentMethod		= $paymentMappings[$orderData['account2Id']][strtolower($orderPaymentMethod)]['account1PaymentId'];
						}
						/* $paymentMathodMappings	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $orderData['account2Id'],'account2PaymentId' => $orderPaymentMethod,'account1Id' => $account1Id))->row_array();
						if($paymentMathodMappings){
							$paymentMethod	= $paymentMathodMappings['account1PaymentId'];
						} */
					}
					if($paymentDetail['paymentDate']){
						$paymentDate	= $paymentDetail['paymentDate'];
						$dDate			= date('Y-m-d',strtotime($paymentDate));
						$date			= new DateTime($dDate); 
						$date->setTimezone(new DateTimeZone($timezone));
						$paymentDate	= $date->format('Y-m-d');
					}
					else{
						$paymentDate	= date('Y-m-d');
					}
					$customerPaymentRequest	= array(
						"paymentMethodCode"		=> $paymentMethod,
						"paymentType"			=> "PAYMENT",
						"orderId"				=> $orderId,
						"currencyIsoCode"		=> strtoupper($currencyCode),
						"exchangeRate"			=> $exchangeRate,
						"amountPaid"			=> $amount,
						"paymentDate"			=> $paymentDate,
						"journalRef"			=> ($reference)?($reference):"Purchase Order Payment for OrderID : ".$orderId,
					);
					$payurl						= '/accounting-service/supplier-payment';
					$customerPaymentRequestRes	= $this->getCurl( $payurl, "POST", json_encode($customerPaymentRequest), 'json' , $account1Id )[$account1Id];
					$createdRowData['Brightpearl Payment Request	:'.$keys]	= $customerPaymentRequest;
					$createdRowData['Brightpearl Payment Response	:'.$keys]	= $customerPaymentRequestRes;
					$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('purchase_order',array('createdRowData' => json_encode($createdRowData)));
					if(!isset($customerPaymentRequestRes['errors'])){
						if(is_int($customerPaymentRequestRes)){
							$updateArray	= array();
							$paymentDetails[$keys]['status']			= '1';
							$paymentDetails[$keys]['brightpearlPayID']	= $customerPaymentRequestRes;
							$paymentDetails[$customerPaymentRequestRes]	= array(
								'amount' 			=> $amount,
								'status' 			=> '1',
								'amountCreditedIn'	=> 'brightpearl',
								'parentPaymentId'	=> $keys,
								'paymentMethod'		=> $paymentMethod,
								'currency'			=> strtoupper($currencyCode),
								"exchangeRate"		=> $exchangeRate,
							);
							$OrderInfoUrl		= '/order-service/order/'.$orderId;
							$OrderInfoResults	= $this->getCurl($OrderInfoUrl, 'get', '', 'json', $account1Id)[$account1Id];
							if($OrderInfoResults){
								$PaymentStatus	= $OrderInfoResults[0]['orderPaymentStatus'];
								if(($PaymentStatus == 'PAID') OR ($PaymentStatus == 'NOT_APPLICABLE')){
									$updateArray	= array(
										'isPaymentCreated'	=> '1',
										'status'			=> '3',
									);
								}
							}
							$updateArray['paymentDetails']	= json_encode($paymentDetails);
							$this->ci->db->where(array('orderId' => $orderId,'account1Id' => $account1Id))->update('purchase_order',$updateArray); 
						}
					}
				}
			}
		}
	}
}