<?php
$this->reInitialize();
$UnInvoicingEnabled	= 1;
$returns	= array();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	if(!$clientcode){continue;}
	$channelMaped		= array();
	$AllBPchannelsData	= $this->getAllChannelMethod();
	if($AllBPchannelsData){
		foreach($AllBPchannelsData as $acc1 => $AllBPchannelsDatas){
			foreach($AllBPchannelsDatas as $cID	=> $AllBPchannelsDatass){
				$channelMaped[$cID]	= $AllBPchannelsDatass;
			}
		}
	}
	//CREATE ALL PUCHASE ORDERS MAPPING TO IDENTIFY DROPSHIP PO's PARENT SALES ORDER
	$purchasedatatemp	= array();
	$allPurchaseData	= $this->ci->db->select('LinkedWithSO,orderId')->get_where('purchase_order',array('LinkedWithSO <>' => ''))->result_array();
	if($allPurchaseData){
		foreach($allPurchaseData as $allPurchaseDatas){
			$purchasedatatemp[$allPurchaseDatas['LinkedWithSO']]	= $allPurchaseDatas;
		}
	}
	
	$return			= array();
	$orderIds		= array();
	
	
	$datas			= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'sales'.$account1Id))->row_array();
	$saveCronTime			= array();
	$saveCronTime[]			= $datas['saveTime'];
	$cronTime		= $datas['saveTime'];	
	if(!$cronTime){
		$cronTime	= strtotime('-60 days');
	}
	$datetime	= new DateTime(date('c',$cronTime));
	$cronTime	= $datetime->format(DateTime::ATOM);
	$cronTime	= str_replace("+","%2B",$cronTime);
	
	$ExtendFetchTime	= 0;
	$newTaxDateCronTime	= 0;
	$checkTimeForFetch	= gmdate('H');
	if(($checkTimeForFetch == '1') OR ($checkTimeForFetch == '2')){
		$ExtendFetchTime	= 1;
		$newTaxDateCronTime	= strtotime('-30 days');
		$datetime			= new DateTime(date('c',$newTaxDateCronTime));
		$newTaxDateCronTime	= $datetime->format(DateTime::ATOM);
		$newTaxDateCronTime	= str_replace("+","%2B",$newTaxDateCronTime);
		$UnInvoicingEnabled	= 0;
	}
	
	$datas		= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'uninvoicesales'.$account1Id))->row_array();
	
	$journalCronTime	= $datas['saveTime'];	
	$journalSaveCronTime	= array();
	if(!$journalCronTime){
		$journalCronTime	= strtotime('-90 days');
	}
	$datetime			= new DateTime(date('c',$journalCronTime));
	$journalCronTime	= $datetime->format(DateTime::ATOM);
	$journalCronTime	= str_replace("+","%2B",$journalCronTime);
	
	$this->config		= $this->accountConfig[$account1Id];
	$journalOrderIds	= array();
	$invoice_refs		= $this->ci->db->select('TempAcc2ID,account2Id,orderId,invoiceRef,generalIds,sendInAggregation,uninvoiced')->get_where('sales_order')->result_array(); 
	$invoicedatas		= array();
	foreach($invoice_refs as $invoice_ref){
		$invoicedatas[$invoice_ref['orderId']]	= $invoice_ref;
	}
	if($UnInvoicingEnabled){
		$url				= '/accounting-service/journal-search?journalType=SI&nominalCode=9999&journalDate='.$journalCronTime.'/';
		$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  	
		$journalOrderIds	= array();
		if (@$response['results']) {
			$header	= array_column($response['metaData']['columns'],'name');
			foreach ($response['results'] as $result) {
				$journalFetchRow	= array_combine($header,$result);
				$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']]	= $journalFetchRow['journalId'];
			}
			if ($response['metaData']) {
				for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
					$url1		= $url . '&firstResult=' . $i;
					$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if ($response1['results']) {
						foreach ($response1['results'] as $result) {
							$journalFetchRow	= array_combine($header,$result);
							$journalOrderIds[$journalFetchRow['orderId']][$journalFetchRow['journalId']]	= $journalFetchRow['journalId'];
						}
					}
				}
			}
		}
	}
	$saveChannelId	= explode(",",$this->config['channelId']);
	$goLiveDate		= $this->config['goLiveDate'];
	$saveTaxDate	= $this->config['taxDate'];
	if($this->config['fetchSalesOrderStatus']){
		$saveOrderStatusId	= explode(",",$this->config['fetchSalesOrderStatus']);
	}		
	$fetchStatusIds = explode(",",$this->config['fetchSalesOrderStatus']);
	$account2Ids	= $this->account2Details[$account1Id];
	
	$url		= '/order-service/order-search?orderTypeId=1&updatedOn='.$cronTime.'/';
	if($ExtendFetchTime){
		$url	= '/order-service/order-search?orderTypeId=1&taxDate='.$newTaxDateCronTime.'/';
	}
	$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
	if(is_array($response)){
		$header		= @array_column($response['metaData']['columns'],'name');
		if (@$response['results']) {
			foreach ($response['results'] as $result) {
				$row		= array_combine($header,$result);
				$created	= date('Ymd',strtotime($row['createdOn']));
				$taxDate	= date('Ymd',strtotime($row['taxDate']));
				$taxDate	= $row['taxDate'];
				//taxdate chanages
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$xeroOffset		= 0;
				$diff			= $BPDateOffset - $xeroOffset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate	= $date->format('Ymd');
				if($taxDate){
					if($taxDate < $saveTaxDate){
						continue;
					}
				}
				if($goLiveDate){
					if($created < $goLiveDate){
						continue;
					}
				}
				$orderIds[$result['0']] = $result['0'];
			}
			if ($response['metaData']) {
				for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
					$url1		= $url . '&firstResult=' . $i;
					$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
					if ($response1['results']) {
						foreach ($response1['results'] as $result) {
							$row		= array_combine($header,$result);
							$created	= date('Ymd',strtotime($row['createdOn']));
							$taxDate	= date('Ymd',strtotime($row['taxDate']));
							$taxDate	= $row['taxDate'];
							$BPDateOffset	= (int)substr($taxDate,23,3);
							$xeroOffset		= 0;
							$diff			= $BPDateOffset - $xeroOffset;
							$date			= new DateTime($taxDate);
							$BPTimeZone		= 'GMT';
							$date->setTimezone(new DateTimeZone($BPTimeZone));
							if($diff){
								$diff	.= ' hour';
								$date->modify($diff);
							}
							$taxDate	= $date->format('Ymd');
							if($taxDate){
								if($taxDate < $saveTaxDate){
									continue;
								}
							}
							if($goLiveDate){
								if($created < $goLiveDate){
									continue;
								}
							}
							$orderIds[$result['0']]	= $result['0'];
						}
					}
				}
			}
		}
	}
	$jOrderIds	= array();
	$jOrderIds	= array_keys($journalOrderIds);
	if($orderIds){
		$orderIds	= array_merge($orderIds,$jOrderIds);
		$orderIds	= array_filter($orderIds);
		sort($orderIds);
		$orderDatas	= $this->getResultById($orderIds,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
		foreach($orderDatas as $OrderInfoList){
			foreach ($account2Ids as $account2Id) {
				$orderId			= $OrderInfoList['id'];
				$channelId			= @$OrderInfoList['assignment']['current']['channelId'];
				$warehouseId		= $OrderInfoList['warehouseId'];
				$statusId			= $OrderInfoList['orderStatus']['orderStatusId'];
				$uninvoiced			= 0;
				$invoiced			= 0;
				$IsDropShip			= 0;
				$channelName		= '';
				$invoiceReference	= $OrderInfoList['invoices']['0']['invoiceReference'];
				if($channelId){
					if($channelMaped[$channelId]){
						$channelName	= $channelMaped[$channelId]['name'];
					}
				}
				if(strtolower($invoiceReference) == 'pending'){
					//	this is a brightpearl side issue, sometimes invoiceNumber is generated as 'pending', which can create issue after posting. no need to fetch this order untill it actually invoiced.	//
					continue;
				}
				if($invoiceReference){
					$invoiced	= 1;
				}
				$created	= date('Ymd',strtotime($OrderInfoList['createdOn']));
				$taxDate	= date('Ymd',strtotime($OrderInfoList['invoices']['0']['taxDate']));
				$taxDate	= $OrderInfoList['invoices']['0']['taxDate'];
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$xeroOffset		= 0;
				$diff			= $BPDateOffset - $xeroOffset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate	= $date->format('Ymd');
				if($taxDate){
					if($taxDate < $saveTaxDate){
						continue;
					} 
				}
				if($goLiveDate){
					if($created < $goLiveDate){
						continue;
					}
				}
				$totalJournalIds	= array();
				if($UnInvoicingEnabled){
					$journalOrderId	= $journalOrderIds[$orderId];
					$saveJournalIds	= $invoicedatas[$orderId];
					if($saveJournalIds){
						$saveJournalIds	= json_decode($saveJournalIds['generalIds'],true);
						foreach($saveJournalIds as $saveJournalId){
							$totalJournalIds[$saveJournalId]	= $saveJournalId;
							unset($journalOrderId[$saveJournalId]);
						}
					}
					if($journalOrderId){
						foreach($journalOrderId as $jid){
							$totalJournalIds[$jid]	= $jid;
						}						
						$uninvoiced	= 1;
					}
				}
				if((!$uninvoiced) AND (!$invoiceReference)){
					continue;
				}
				if(!$uninvoiced){
					if(($fetchStatusIdsExclude) AND (in_array($statusId,$fetchStatusIdsExclude))){
						continue;
					}
				}
				if($uninvoiced){
					if(!isset($invoicedatas[$orderId])){
						$invoiced	= 1;
					}
				}
				
				$config2     						= $this->account2Config[$account2Id['id']];
				$customFieldForSalesFetch			= $config2['customFieldForSalesFetch'];
				$customFieldForSalesFetchValue		= $config2['customFieldForSalesFetchValue'];
				$customFieldForSalesFetchValueAll	= array();
				if($customFieldForSalesFetchValue){
					$customFieldForSalesFetchValue	= explode("||",$customFieldForSalesFetchValue);
					foreach($customFieldForSalesFetchValue as $customFieldForSalesFetchValueTemp){
						$customFieldForSalesFetchValueAll[]	= trim(strtolower($customFieldForSalesFetchValueTemp));
					}
				}
				
				if(($customFieldForSalesFetch) AND (isset($OrderInfoList['customFields'][$customFieldForSalesFetch])) AND (is_array($customFieldForSalesFetchValueAll)) AND (!empty($customFieldForSalesFetchValueAll))){
					$orderCustomFieldValue		= '';
					if((is_array($OrderInfoList['customFields'][$customFieldForSalesFetch])) AND (isset($OrderInfoList['customFields'][$customFieldForSalesFetch]['value']))){
						$orderCustomFieldValue	= trim(strtolower($OrderInfoList['customFields'][$customFieldForSalesFetch]['value']));
					}
					else{
						$orderCustomFieldValue	= trim(strtolower($OrderInfoList['customFields'][$customFieldForSalesFetch]));
					}
					if($orderCustomFieldValue AND (!in_array($orderCustomFieldValue, $customFieldForSalesFetchValueAll))){
						continue;
					}
				}
				
				$tempSaveAcc1 	= $account1Id; 
				$tempSaveAcc2	= '';
				$config2     	= $this->account2Config[$account2Id['id']];
				$config2Warehouses	= array();
				$config2Channels	= array();
				if($config2['warehouses']){
					$config2Warehouses	= explode(",",$config2['warehouses']);
				}
				if($config2['channelIds']){
					$config2Channels	= explode(",",$config2['channelIds']);
				}
				if($config2Channels || $config2Warehouses){
					if((!$channelId) && (!$warehouseId)){
						continue;
					}
					if($config2Channels){
						if(!in_array($channelId,$config2Channels)){
							if((!$channelId) AND (in_array('NullChannel',$config2Channels))){
								/*	if brightpearl channel is null and 'BlankChannel(NullChannel)' option is selected in 
									default channel dropdown then allow the order to come in connector	*/
							}
							else{
								continue;
							}
						}
					}	
					if($config2Warehouses){
						if(!in_array($warehouseId,$config2Warehouses)){
							continue;
						}
					}
					$tempSaveAcc2	= $account2Id['id'];
				}
				else{
					$tempSaveAcc2	= $account2Id['id'];
				}
				if(!$tempSaveAcc2){
					continue;
				}
				if($purchasedatatemp[$orderId]){
					$IsDropShip	= 1;
				}
				if($invoicedatas[$orderId]['uninvoiced'] == 1){
					$uninvoiced	= 1;
				}
				if($invoicedatas[$orderId]['sendInAggregation']){
					continue;
				}
				$TempAcc2ID	= 0;
				if($uninvoiced){
					if($invoicedatas[$orderId]['TempAcc2ID']){
						$TempAcc2ID	= $invoicedatas[$orderId]['TempAcc2ID'];
					}
					else{
						if($invoicedatas[$orderId]['account2Id']){
							$TempAcc2ID	= $invoicedatas[$orderId]['account2Id'];
						}
					}
				}
				$saveAccId1		= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc1) : $tempSaveAcc2;
				$saveAccId2		= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($tempSaveAcc2) : $tempSaveAcc1;
				$delAddress		= $OrderInfoList['parties']['delivery'];
				if($clientcode == 'amistry'){
					//kitKing company sales is disabled for now, account Id of kitKing is 2
					if($saveAccId2	== 2){
						continue;
					}
				}
				
				$return[$account1Id][$orderId]['orders']	= array(
					'account1Id'    	=> $saveAccId1,
					'account2Id'    	=> $saveAccId2,
					'orderId'			=> $orderId,
					'delAddressName'	=> $delAddress['addressFullName'],
					'delPhone' 			=> $delAddress['telephone'],
					'customerId' 		=> $OrderInfoList['parties']['customer']['contactId'], 
					'customerEmail'		=> $OrderInfoList['parties']['customer']['email'], 
					'orderNo'    		=> @($OrderInfoList['reference']), 
					'totalAmount'		=> $OrderInfoList['totalValue']['total'],
					'totalTax'			=> $OrderInfoList['totalValue']['taxAmount'],
					'shippingMethod'	=> @$OrderInfoList['delivery']['shippingMethodId'],
					'deliveryDate'		=> @$OrderInfoList['delivery']['deliveryDate'],
					'currency'			=> @$OrderInfoList['currency']['orderCurrencyCode'],
					'created'       	=> date('Y-m-d H:i:s', strtotime($OrderInfoList['createdOn'])),
					'rowData'       	=> json_encode($OrderInfoList),
					'uninvoiced'		=> $uninvoiced,
					'invoiced'			=> $invoiced,
					'generalIds'		=> json_encode($totalJournalIds),
					'IsDropShip'		=> $IsDropShip,
					'InvoicedTime'		=> date('Y-m-d H:i:s', strtotime('now')),
					'channelId'			=> $channelId,
					'channelName'		=> $channelName,
					'taxDate'			=> date('Y-m-d H:i:s', strtotime($taxDate)),
					'bpInvoiceNumber'	=> $OrderInfoList['invoices']['0']['invoiceReference'],
					'TempAcc2ID'		=> $TempAcc2ID,
				);
				if(!$UnInvoicingEnabled){
					unset($return[$account1Id][$orderId]['orders']['generalIds']);
				}
				if($invoicedatas[$orderId]){
					unset($return[$account1Id][$orderId]['orders']['InvoicedTime']);
				}
				$saveCronTime[] = strtotime($OrderInfoList['updatedOn']);
			}
			
			//new logStoringFunctionality
			$logsData		= array();
			$allStoredLogs	= array();
			$fileLogPath	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'sales'. DIRECTORY_SEPARATOR;
			if(!is_dir($fileLogPath)){
				mkdir($fileLogPath,0777,true);
				chmod(dirname($fileLogPath), 0777);
			}
			$fileLogPath	= $fileLogPath.$OrderInfoList['id'].'.logs';
			if(file_exists($fileLogPath)){
				$latestStoredLogs	= array();
				$allStoredLogs		= json_decode(file_get_contents($fileLogPath), true);
				$tempStoredLogs		= $allStoredLogs;
				krsort($tempStoredLogs);
				foreach($tempStoredLogs as $logsKey => $tempLogsTemp){
					$latestStoredLogs	= $tempLogsTemp;
					break;
				}
				if(!empty($latestStoredLogs)){
					$tempFetchedLogs	= $OrderInfoList;
					unset($tempFetchedLogs['createdOn']);
					unset($tempFetchedLogs['updatedOn']);
					
					unset($latestStoredLogs['createdOn']);
					unset($latestStoredLogs['updatedOn']);
					
					if(md5(json_encode($latestStoredLogs)) != md5(json_encode($tempFetchedLogs))){
						$allStoredLogs[date('c', strtotime("now"))]	= $OrderInfoList;
						file_put_contents($fileLogPath,json_encode($allStoredLogs));
					}
				}
				else{
					file_put_contents($fileLogPath,json_encode($allStoredLogs));
				}
			}
			else{
				$logsData	= array(date('c', strtotime("now"))	=> $OrderInfoList);
				file_put_contents($fileLogPath,json_encode($logsData));
			}
		}
	}
	$returns[$account1Id]	= array( 'return' => $return,'saveTime' => @max($saveCronTime) );
}
?>