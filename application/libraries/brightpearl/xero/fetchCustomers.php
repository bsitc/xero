<?php
$this->reInitialize();
$returns	= array();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account1Id => $accountDetails){
	$path	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'customers'. DIRECTORY_SEPARATOR;
	if(!is_dir($path)){
		mkdir($path,0777,true);
		chmod(dirname($path), 0777);
	}
	
	$allLatestFetchids	= array();
	$customerIds		= array();
	$allcustomerIds		= array();
	$startTime			= '';
	$webTaskID			= '';
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$customerIds		= $objectId;
		$fetchCustomerID	= $objectId;
	}
	elseif($cronTime){
		$fetchIDs	= $this->ci->db->get_where('temp', array('id' => $cronTime))->row_array();
		if($fetchIDs){
			$customerIds	= json_decode($fetchIDs['rowData'],true);
			$this->ci->db->update('temp',array('status' => 1), array('id' => $cronTime));
			$startTime		= strtotime('now');
			$webTaskID		= $cronTime;
			$cronTime		= '';
		}
	}
	
	$datas				= $this->ci->db->order_by('id', 'desc')->get_where('cron_management', array('type' => 'bpcustomer'.$account1Id))->row_array();
	$cronTime			= $datas['saveTime'];
	$this->config		= $this->accountConfig[$account1Id];
	$account2Ids		= $this->account2Details[$account1Id];
	$fetchCustomerID	= '';
	$customerId			= array();
	if($objectId){
		if(!is_array($objectId)){
			$objectId	= array($objectId);
		}
		$customerIds		= $objectId;
		$fetchCustomerID	= $objectId;
	}
	if(!$customerIds){
		if(!$cronTime){
			$url		= '/contact-service/contact/';
			$response	= $this->getCurl($url, "OPTIONS", '', 'json', $account1Id)[$account1Id];
			if((is_array($response)) AND (!empty($response)) AND (isset($response['getUris']))){
				foreach($response['getUris'] as $getUris){
					$url		= '/contact-service/' . $getUris;
					$response	= $this->getCurl($url, 'GET', '', 'json', $account1Id)[$account1Id];
					if((is_array($response)) AND (!empty($response)) AND (isset($response[0]))){
						foreach($response as $result){
							$customerId[]	= $result['contactId'];
						}
					}
				}
			}
			/* $customerId	= array_chunk($customerId,10000);
			foreach($customerId as $rowCust){
				$this->ci->db->insert('temp',array('type' => 'customer','rowData' =>json_encode($rowCust)));
			}
			return false; */
			
		} 
		else{
			$datetime	= new DateTime(date('c',$cronTime));
			$cronTime	= $datetime->format(DateTime::ATOM);
			$cronTime	= str_replace("+","%2B",$cronTime);
			$urls		= array('/contact-service/contact-search?updatedOn=' . $cronTime . '/', '/contact-service/contact-search?createdOn=' . $cronTime . '/');
			foreach($urls as $url){
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if((is_array($response)) AND (!empty($response)) AND (isset($response['results']))){
					foreach($response['results'] as $result){
						$customerId[]	= $result['0'];
					}
					if($response['metaData']['resultsAvailable'] > 500){
						for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if((is_array($response1)) AND (!empty($response1)) AND (isset($response1['results']))){
								foreach($response1['results'] as $result){
									$customerId[]	= $result['0'];
								}
							}
						}
					}
				}
			}
		}
	}
	
	if(is_string($customerIds)){$customerIds = array($customerIds);}
	
	$customerIds	= array_unique($customerId);
	$customerIds	= array_filter($customerId);
	sort($customerIds);
	if(empty($customerIds)){continue;}
	
	$allcustomerIds		= array_chunk($customerIds,200,true);
	$updatedTimes		= array();
	$maxUpdatedTimes	= '';
	$saveTime			= '';
	$checkInsertFlag	= 0;
	foreach($allcustomerIds as $allcustomerId){
		$returnKey		= 0;
		$return			= array();
		$contactResults	= $this->getResultById($allcustomerId,'/contact-service/contact/',$account1Id,200,0,'?includeOptional=customFields,postalAddresses');
		if((is_array($contactResults)) AND (!empty($contactResults)) AND (isset($contactResults[0]))){
			foreach($contactResults as $contactResult){
				$customerId	= $contactResult['contactId'];
				foreach($account2Ids as $account2Id){
					$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
					$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
					$delAddress	= (isset($contactResult['postalAddresses'][$contactResult['postAddressIds']['DEL']]))?($contactResult['postalAddresses'][$contactResult['postAddressIds']['DEL']]):array();
					$return[$saveAccId1][$returnKey]['customers']	= array(
						'account1Id'		=> $saveAccId1,
						'account2Id'		=> $saveAccId2,
						'customerId'		=> $contactResult['contactId'],
						'email'				=> (isset($contactResult['communication']['emails']['PRI']['email']))?($contactResult['communication']['emails']['PRI']['email']):'',
						'fname'				=> ($contactResult['firstName'])?($contactResult['firstName']):'',
						'lname'				=> ($contactResult['lastName'])?($contactResult['lastName']):'',
						'phone'				=> ($contactResult['communication']['telephones']['PRI'])?($contactResult['communication']['telephones']['PRI']):'',
						'addressFname'		=> ($contactResult['firstName'])?($contactResult['firstName']):'',
						'addressLname'		=> ($contactResult['lastName'])?($contactResult['lastName']):'',
						'address1'			=> ($delAddress['addressLine1'])?($delAddress['addressLine1']):'',
						'address2'			=> ($delAddress['addressLine2'])?($delAddress['addressLine2']):'',
						'city'				=> ($delAddress['addressLine3'])?($delAddress['addressLine3']):'',
						'state'				=> ($delAddress['addressLine4'])?($delAddress['addressLine4']):'',
						'zip'				=> ($delAddress['postalCode'])?($delAddress['postalCode']):'',
						'company'			=> ($contactResult['organisation']['name'])?($contactResult['organisation']['name']):'',
						'countryCode'		=> ($delAddress['countryIsoCode'])?($delAddress['countryIsoCode']):'',
						'isSupplier'		=> ($contactResult['relationshipToAccount']['isSupplier'])?1:0,
						'created'			=> date('Y-m-d H:i:s', strtotime($contactResult['createdOn'])),
						'updated'			=> date('Y-m-d H:i:s', strtotime($contactResult['updatedOn'])),
						'params'			=> json_encode($contactResult),
						'accountReference'	=> ($contactResult['assignment']['current']['accountReference'])?($contactResult['assignment']['current']['accountReference']):'',
					);
					if($contactResult['updatedOn']){
						$updatedTimes[]	= strtotime($contactResult['updatedOn']);
					}
					$returnKey++;
				}
				
				//new logStoringFunctionality
				$logsData		= array();
				$allStoredLogs	= array();
				$fileLogPath	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account1'. DIRECTORY_SEPARATOR . $account1Id. DIRECTORY_SEPARATOR .'customers'. DIRECTORY_SEPARATOR;
				$fileLogPath	= $fileLogPath.$customerId.'.logs';
				if(file_exists($fileLogPath)){
					$latestStoredLogs	= array();
					$allStoredLogs		= json_decode(file_get_contents($fileLogPath), true);
					$tempStoredLogs		= $allStoredLogs;
					krsort($tempStoredLogs);
					foreach($tempStoredLogs as $logsKey => $tempLogsTemp){
						$latestStoredLogs	= $tempLogsTemp;
						break;
					}
					if(!empty($latestStoredLogs)){
						$tempFetchedLogs	= $contactResult;
						unset($tempFetchedLogs['createdOn']);
						unset($tempFetchedLogs['updatedOn']);
						
						unset($latestStoredLogs['createdOn']);
						unset($latestStoredLogs['updatedOn']);
						
						if(md5(json_encode($latestStoredLogs)) != md5(json_encode($tempFetchedLogs))){
							$allStoredLogs[date('c', strtotime("now"))]	= $contactResult;
							file_put_contents($fileLogPath,json_encode($allStoredLogs));
						}
					}
					else{
						file_put_contents($fileLogPath,json_encode($allStoredLogs));
					}
				}
				else{
					$logsData	= array(date('c', strtotime("now"))	=> $contactResult);
					file_put_contents($fileLogPath,json_encode($logsData));
				}
			}
		}
		
		$saveDatasTemps			= $this->ci->db->select('id,account1Id,account2Id,customerId,status')->get_where('customers')->result_array(); 
		$saveCustomerDatas		= array();
		if(!empty($saveDatasTemps)){
			foreach($saveDatasTemps as $saveDatasTemp){
				$key	= trim($saveDatasTemp['account1Id'].'_'.$saveDatasTemp['account2Id'].'_'.$saveDatasTemp['customerId']);
				$saveCustomerDatas[$key]	= $saveDatasTemp;
			}
		}
		
		$batchInsert	= array(); 
		$batchUpdate	= array();
		$fetchedDatas	= $return;
		foreach($fetchedDatas as $account1Id => $fetchedData){
			foreach($fetchedData as $customerIdKey => $fetchResult){
				$customerInfo	= $fetchResult['customers'];
				$key			= trim($customerInfo['account1Id'].'_'.$customerInfo['account2Id'].'_'.$customerInfo['customerId']);
				if((is_array($saveCustomerDatas)) AND (!empty($saveCustomerDatas)) AND (isset($saveCustomerDatas[$key]))){
					if(($saveCustomerDatas[$key]['status'] == 3) OR ($saveCustomerDatas[$key]['status'] == 4)){
						continue;
					}
					$customerInfo['id']		= $saveCustomerDatas[$key]['id'];
					$customerInfo['status']	= 2;
					$batchUpdate[]			= $customerInfo;
				} 
				else{
					$batchInsert[]			= $customerInfo;
				}
				$allLatestFetchids[]	= $customerInfo['customerId'];
			}
		}
		
		if(!empty($batchInsert)){
			$checkInsertFlag	= true;
			$this->ci->db->insert_batch('customers', $batchInsert);
		}
		if(!empty($batchUpdate)){
			$checkInsertFlag	= true;
			$this->ci->db->update_batch('customers', $batchUpdate, 'id');
		}
	}
	
	$updatedTimes[]	= strtotime('-5 days');
	$saveTime		= max($updatedTimes);
	$saveTime		= ($saveTime - (60*10));
	
	if($checkInsertFlag){
		if(!$fetchCustomerID){
			$this->ci->db->insert('cron_management', array('type' => 'bpcustomer'.$account1Id, 'saveTime' => $saveTime));
		}
	}
	if($webTaskID){
		if($startTime){
			$endTime			= strtotime('now');
			$processedSecond	= ($endTime - $startTime); 
			$this->ci->db->update('temp',array('executionSeconds' => $processedSecond), array('id' => $webTaskID));
		}
	}
	if($clientcode AND ($clientcode == 'primalstrengthxero') AND (!empty($allLatestFetchids))){
		$allLatestFetchids	= array_filter($allLatestFetchids);
		$allLatestFetchids	= array_unique($allLatestFetchids);
		if(!empty($allLatestFetchids)){
			$this->ci->xero->postCustomers($allLatestFetchids);
		}
	}
}
?>