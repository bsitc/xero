<?php
if(!defined('BASEPATH')){exit('No direct script access allowed');}
#[\AllowDynamicProperties]
class Brightpearl{
	public $apiurl, $headers, $accountDetails, $accountConfig, $account2Details, $account1id, $account2Id, $getByIdKey, $authToken,$response;
	public function __construct(){
		$this->ci			= &get_instance();
		$this->headers		= array();
		$this->devToken		= 'PxurwyOcrRloIbmq/n6hybcD41MUdyNOcMrEZlvJGIo=';
		$this->devSecrete	= 'XDrmqBOo9KFLU09GWw+OVuwHv4ne8Zp9OPmi5oO9JwM=';
	}
	public function reInitialize($account1Id = ''){
		$this->accountDetails	= $this->ci->account1Account;
		$this->account2Details	= array();
		$this->account2Config	= array();
		$this->accountConfig	= array();
		foreach($this->ci->account1Config as $account1Config){
			$this->accountConfig[$account1Config[$this->ci->globalConfig['account1Liberary'].'AccountId']]		= $account1Config;
		}
		foreach($this->ci->account2Account as $account2Id => $account2Account){
			$this->account2Details[$account2Account['account1Id']][$account2Id]									= $account2Account;
		}
		foreach($this->ci->account2Config as $account2Id => $account2Account){
			$this->account2Config[$account2Account[$this->ci->globalConfig['account2Liberary'].'AccountId']]	= $account2Account;
		}
	}
	public function generateToken($accountId = ''){
		$this->reInitialize();
		foreach($this->accountDetails as $accountId => $accountDetail){
			$postDatas	= array(
				'apiAccountCredentials'	=> array(
					'emailAddress'	=> $accountDetail['email'],
					'password'		=> $accountDetail['password'],
				),
			);
			$ch	= curl_init($accountDetail['authUrl']);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postDatas));
			$response	= json_decode(curl_exec($ch), true);
			if($response['response']){
				$this->authToken[$accountId]	= $response['response'];
			}
		}
	}
	public function getCurl($suburl, $method = 'GET', $field = '', $type = 'json', $account2Id = ''){
		$returnData		= array();
		$accountDetails	= array();
		if($account2Id){
			foreach($this->accountDetails as $t1){
				if($t1['id'] == $account2Id){
					$accountDetails	= array($t1);
				}
			}
		}
		else{
			$accountDetails	= $this->accountDetails;
		}
		$orgSubUrl	= $suburl;
		foreach($accountDetails as $accountDetail){
			$milliseconds	= round(microtime(true) * 1000);
			$calMin			= sprintf("%.5f",($milliseconds / 60000));
			$nextMilisec	= ((int)$calMin + 1) - $calMin;
			$nextMilisec	= (int)((int) ($nextMilisec * 100)) / 1.666;
			$remainingSec	= $nextMilisec + 3;
			$limitName		= (int)($calMin); 
			$insertData		= array(
				'name'			=> $limitName,
				'limitcount'	=> 0,
			);
			$sql			= $this->ci->db->insert_string('api_call', $insertData) . ' ON DUPLICATE KEY UPDATE limitcount=limitcount + 1';
			$this->ci->db->query($sql);
			$limitRate		= $this->ci->db->get_where('api_call',array('name' => $limitName))->row_array();
			if($limitRate['limitcount'] >= 190){
				sleep($remainingSec);
			}
			elseif($limitRate['limitUsed']){
				sleep($remainingSec);
			}
			if(!$accountDetail['authmod']){
				$accountDetail['authmod']	= 'user';
			}
			if($accountDetail['authmod'] == 'user'){
				if(!$this->authToken[$accountDetail['id']]){
					$this->generateToken($accountDetail);
				}
			}
			if($accountDetail['authmod'] == 'token'){
				$appToken		= $accountDetail['token'];
				$authToken		= base64_encode(hash_hmac("sha256", $appToken, $this->devSecrete,true));
				$this->headers	= array(
					'Content-Type:application/json;charset=UTF-8',
					'brightpearl-dev-ref:bsitcbpdev',
					'brightpearl-app-ref:'.$accountDetail['reference'],
					'brightpearl-account-token:'.$authToken,
				);
			}
			$this->appurl	= $accountDetail['url'] . '/';
			$url			= $this->appurl . ltrim($suburl, "/");
			if(is_array($field)){
				$postvars	= http_build_query($field);
			} 
			else{
				$postvars	= $field;
			}
			$ch	= curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
			if($postvars){
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			}
			if($accountDetail['authmod'] == 'user'){
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("brightpearl-auth: " . $this->authToken[$accountDetail['id']], 'Content-Type: application/json'));
			}
			if($accountDetail['authmod'] == 'token'){
				curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
			}
			$results	= json_decode(curl_exec($ch), true);
			$account1Id	= ($accountDetail['id']) ? ($accountDetail['id']) : ($accountDetail['account1Id']);
			$this->response[$account1Id] = $results;
			if(@$results['response'] == 'You have sent too many requests. Please wait before sending another request'){
				$insertData		= array(
					'name'		=> $limitName,
					'limitUsed'	=> 1,
				);
				$this->ci->db->where(array('name' => $limitName))->update('api_call',$insertData);
				$remainingSec	= $remainingSec + 5;
				sleep($remainingSec);
				return $this->getCurl($orgSubUrl,$method,$field,$type,$account2Id);
			}
			else{
				$return		= $results;
				if(@$results['response']){
					$return	= $results['response'];
					if(strtolower($method) == 'get'){
						if($this->getByIdKey){
							$return	= array();
							foreach($results['response'] as $result){
								$return[$result[$this->getByIdKey]]	= $result;
							}
						}
					}
				}
				$returnData[$account1Id]	= $return;
			}
		}
		return $returnData;
	}
	function range_string($number_array){
		sort($number_array);
		$previous_number	= intval(array_shift($number_array)); 
		$range				= false;
		$range_string		= "" . $previous_number; 
		foreach($number_array as $number){
			$number	= intval($number);
			if($number == $previous_number + 1){
				$range	= true;
			}
			else{
				if($range){
					$range_string	.= "-$previous_number";
					$range			= false;
				}
				$range_string	.= ",$number";
			}
			$previous_number	= $number;
		}
		if($range){
			$range_string	.= "-$previous_number";
		}
		return $range_string;
	}
	public function getResultById($ids,$subUrl,$account1Id,$chunkLimit = 200,$returnSameKey = 0,$searchUrl = ''){
		$return	= array();
		if($ids){
			$ids	= array_unique($ids);
			sort($ids);
			$ids	= array_chunk($ids,$chunkLimit);
			foreach($ids as $id){
				$range	= $this->range_string($id);
				$url	= rtrim($subUrl,"/").'/'.$range;
				if($searchUrl){
					$url	= $url.$searchUrl;
				}
				$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if(is_array($response)){
					if(@!$response['errors']){
						if(!$returnSameKey){
							if(count($response) == 1){
								foreach($response as $key => $res){
									if(!isset($return[$key])){
										$return[$key]	= array();
									}
									$return[$key]	= array_merge($return[$key],$res);
								}
							}
							else{
								$return	= @array_merge($return,$response);
							}
						}
						else{
							foreach($response as $key => $res){
								$return[$key]	= $res;
							}
						}
					}
				}
			}
		}
		return $return;
	}
	public function fetchProducts($objectId = '', $cronTime=''){
		return $this->callFunction('fetchProducts',$objectId,$cronTime);
	}
	public function fetchCustomers($objectId = '', $cronTime=''){
		return $this->callFunction('fetchCustomers',$objectId,$cronTime);
	}
	public function fetchSales($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchSales',$objectId,$cronTime,$accountId);
	}
	public function fetchSalesPayment($objectId = ''){
		$this->callFunction('fetchSalesPayment',$objectId);
	}
	public function postSalesPayment($objectId = ''){
		$this->callFunction('postSalesPayment', $objectId);
	}
	public function fetchPurchase($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchPurchase',$objectId,$cronTime,$accountId);
	}
	public function postPurchasePayment($objectId = ''){
		$this->callFunction('postPurchasePayment', $objectId);
	}
	public function postPurchaseConsolPayment($objectId = ''){
		$this->callFunction('postPurchaseConsolPayment', $objectId);
	}
	public function postSalesCreditPayment($objectId = ''){
		$this->callFunction('postSalesCreditPayment', $objectId);
	}
	public function fetchSalesCreditPayment($objectId = ''){
		$this->callFunction('fetchSalesCreditPayment',$objectId);
	}
	public function postPurchaseCreditPayment($objectId = ''){
		$this->callFunction('postPurchaseCreditPayment', $objectId);
	}
	public function postGoodsDispatch($objectId = '',$cronTime = ''){ 
		$this->callFunction('postGoodsDispatch',$objectId,$cronTime);
	}
	public function postSalesCreditConfimation($objectId = '',$cronTime = ''){ 
		$this->callFunction('postSalesCreditConfimation',$objectId,$cronTime);
	}
	public function postAcknowledgement($objectId = '',$cronTime = ''){ 
		$this->callFunction('postAcknowledgement',$objectId,$cronTime);
	}	
	public function updateProductCustomFileds($objectId = '', $cronTime=''){
		$this->callFunction('updateProductCustomFileds',$objectId,$cronTime);
	}
	public function fetchSalesCredit($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchSalesCredit',$objectId,$cronTime,$accountId);
	}
	public function fetchPurchaseCredit($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchPurchaseCredit',$objectId,$cronTime,$accountId);
	}
	public function fetchStockAdjustment($objectId = '', $accountId = '',$cronTime = '', $fetchType = ''){
		return $this->callFunction('fetchStockAdjustment',$objectId,$cronTime,$accountId, $fetchType);
	}
	public function fetchJournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchJournal',$objectId,$cronTime,$accountId);
	}
	public function fetchCogsjournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchCogsjournal',$objectId,$cronTime,$accountId);
	}
	public function fetchCogswoinvoice($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchCogswoinvoice',$objectId,$cronTime,$accountId);
	}
	public function fetchStockjournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchStockjournal',$objectId,$cronTime,$accountId);
	}
	public function fetchAmazonFeeOther($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchAmazonFeeOther',$objectId,$cronTime,$accountId);
	}
	public function fetchGrnijournal($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchGrnijournal',$objectId,$cronTime,$accountId);
	}
	public function fetchAradvance($objectId = '', $accountId = '',$cronTime = ''){
		return $this->callFunction('fetchAradvance',$objectId,$cronTime,$accountId);
	}
	public function callFunction($functionName = '',$objectId = '',$cronTime = '',$accountId = '', $fetchType = ''){
		$this->response	= array();
		$return			= array();
		$returns		= array();
		$saveCronTime	= array();
		$updatedTimes	= array();
		if($functionName){
			if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR . APPNAME . DIRECTORY_SEPARATOR .$functionName.'.php')){
				if(file_exists(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR . APPNAME . DIRECTORY_SEPARATOR . CLIENTCODE . DIRECTORY_SEPARATOR .$functionName.'.php')){
					require_once(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR . APPNAME. DIRECTORY_SEPARATOR . CLIENTCODE. DIRECTORY_SEPARATOR .$functionName.'.php');
				}
				else if(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR .APPNAME. DIRECTORY_SEPARATOR .$functionName.'.php'){
					require_once(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'.DIRECTORY_SEPARATOR .APPNAME. DIRECTORY_SEPARATOR .$functionName.'.php');
				}
			}
			else{
				require_once(dirname(__FILE__). DIRECTORY_SEPARATOR .'brightpearl'. DIRECTORY_SEPARATOR .$functionName.'.php');
			}
		}
		if($returns){
			return $returns;
		}
		else if($updatedTimes){
			return array( 'return' => $return,'saveTime' => @max($updatedTimes) );
		}
		else if($saveCronTime){
			return array( 'return' => $return,'saveTime' => @max($saveCronTime) );
		}
		else{
			return $return;
		}
	}
	public function fetchJournalByIds($journalIds = array()){
		if(!$journalIds){return false;}
		$this->reInitialize();
		$return	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$datas	= $this->getResultById($journalIds,'/accounting-service/journal/',$account1Id,'200',0);
			if($datas['journals']){
				foreach($datas['journals'] as $journals){
					$return[$journals['id']]	= $journals;
				}
			}
		}
		return $return;
	}
	public function getProductStock($productIds = array()){
		if(!is_array($productIds)){
			$productIds	= array($productIds);
		}
		sort($productIds);
		$url		= '/warehouse-service/product-availability/'.implode(",", $productIds);
		$this->reInitialize();
		$return		= array();
		$results	= $this->getCurl($url);
		foreach($results as $account1Id => $result){
			$account2Ids	= $this->account2Details[$account1Id];
			foreach($account2Ids as $account2Id){
				$saveAccId1	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account1Id) : $account2Id['id'];
				$saveAccId2	= ($this->ci->globalConfig['account1Liberary'] == 'brightpearl') ? ($account2Id['id']) : $account1Id;
				foreach($result as $productId => $row){
					$return[$saveAccId1][$saveAccId2][$productId]	= $row;
				}
			}
		}
		return $return;
	}
	public function getProductPriceList($proIds = '', $priceListId = '0'){
		$this->reInitialize();
		$this->getByIdKey	= '';
		if(!$proIds){
			return false;
		}
		if(is_string($proIds)){
			$proIds	= array($proIds);
		}
		$return		= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$result	= $this->getResultById($proIds,'/product-service/product-price/',$account1Id);
			foreach($result as $row){
				foreach($row['priceLists'] as $priceLists){
					$return[$row['productId']][$priceLists['priceListId']]	= (@$priceLists['quantityPrice']['1']) ? ($priceLists['quantityPrice']['1']) : '0.00';
				}
			}
		}
		return $return;
	}
	public function getAllShippingMethod($accountId = ''){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/warehouse-service/shipping-method';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getExchangeRate($accountId = ''){
		if(@!$this->accountDetails[$accountId]){
			$this->reInitialize($accountId);
		}
		$url		= '/accounting-service/exchange-rate/';
		$response	= $this->getCurl($url,'get','json','',$accountId);
		return $response;
	}
	public function getAllBPExchangeRate($accountId = ''){
		if(@!$this->accountDetails[$accountId]){$this->reInitialize($accountId);}
		
		$allExRates	= array();
		$url		= '/integration-service/account-configuration';
		$return		= $this->getCurl($url);
		$configurationData = array();
		if(!empty($return)){
			foreach($return as $accountId => $returnData){
				$configurationData[$accountId]	= strtolower($returnData['configuration']['baseCurrencyCode']);
			}
		}
		$allBPCurrency	= $this->getAllCurrency();
		if(empty($allBPCurrency)){return $allExRates;}
		
		$url			= '/accounting-service/exchange-rate/';
		$return		= $this->getCurl($url);
		if(!empty($return)){
			foreach($return as $accountId => $returnData){
				foreach($returnData as $datas){
					$bpCurrencyID	= $datas['currencyId'];
					$exRateDate		= date('Ymd',strtotime($datas['rateActiveFrom']));
					if(isset($allBPCurrency[$accountId][$bpCurrencyID])){
						$allExRates[$accountId][$configurationData[$accountId]][strtolower($allBPCurrency[$accountId][$bpCurrencyID]['code'])][$exRateDate]	= $datas['rate'];
					}
				}
			}
		}
		return $allExRates;
	}
	public function getAllCurrency($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/accounting-service/currency-search';
		$returnDatas		= $this->getCurl($url);
		$return				= array();
		foreach($returnDatas as $accountId => $returnData){
			foreach($returnData['results'] as $key => $results){
				$return[$accountId][$results['0']]	= array(
					'id'		=> $results['0'],
					'name'		=> $results['1'],
					'code'		=> $results['2'],
					'symbol'	=> $results['3'],
				);
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllCurrencyWithRate($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/accounting-service/currency-search';
		$returnDatas		= $this->getCurl($url);
		$return				= array();
		foreach($returnDatas as $accountId => $returnData){
			foreach($returnData['results'] as $key => $results){
				$return[$accountId][strtolower($results['2'])]	= array(
					'id'			=> $results['0'],
					'name'			=> $results['1'],
					'code'			=> $results['2'],
					'symbol'		=> $results['3'],
					'latestExRate'	=> $results['4'],
				);
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllLocation($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/warehouse-service/warehouse';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getAllChannel($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/channel';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach(@$retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getAllOrderStatus($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/order-service/order-status';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $orderStatusId => $orderStatus){
				$return[$accountIId][$orderStatus['statusId']]			= $orderStatus;
				$return[$accountIId][$orderStatus['statusId']]['id']	= $orderStatus['statusId'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllCategoryMethod($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/brightpearl-category';
		$return				= array();
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
		return $return;
	}
	public function getAllTax($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= 'id';
		$url				= '/accounting-service/tax-code';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $taxId => $tax){
				$return[$accountIId][$taxId]			= $tax;
				$return[$accountIId][$taxId]['name']	= $tax['code'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getSeason($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$return				= array();
		$url				= '';
		$returns			= $this->getCurl($url);
		foreach($returns as $account1Id => $retur){
			foreach($retur as $re){
				$return[$account1Id][$re['id']]	= $re;
			}
		}
	}
	public function getAllPriceList($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/price-list';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $priceListId => $pricelist){
				$return[$accountIId][$pricelist['id']]			= $pricelist;
				$return[$accountIId][$pricelist['id']]['name']	= $pricelist['code'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function nominalCode($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/accounting-service/nominal-code-search';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){ 
			foreach($result['results'] as  $nominalCodes){
				$return[$accountIId][$nominalCodes['0']]			= $nominalCodes;
				$return[$accountIId][$nominalCodes['0']]['id']		= $nominalCodes['0'];
				$return[$accountIId][$nominalCodes['0']]['name']	= '( '.$nominalCodes['0'] . ' ) '. $nominalCodes['1'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllTag($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/contact-service/tag';
		$results			= $this->getCurl($url);
		$return				= array();
		foreach($results as $accountIId => $result){
			foreach($result as $priceListId => $pricelist){
				$return[$accountIId][$priceListId]			= $pricelist;
				$return[$accountIId][$priceListId]['name']	= $pricelist['tagName'];
				$return[$accountIId][$priceListId]['id']	= $pricelist['tagId'];
			}
		}
		$this->getByIdKey	= '';
		return $return;
	}
	public function getAllSalesrep(){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/contact-service/contact-search?isStaff=true';
		$resultss			= $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$return[$accountId][$result['0']]	= array('id' => $result['0'],'name' => $result['4'] .' '. $result['5']);
			}
		}
		return $return;
	}
	public function getAllProjects(){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/contact-service/project/';
		$resultss			= $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['projectId']]	= $result;
			}
		}
		return $return;
	}
	public function getAllBrand($accountId = ''){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/product-service/brand-search';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$return[$accountId][$result['0']]	= array('id' => $result['0'],'name' => $result['1']);
			} 
		}		
		return $return;
	}
	public function getAllChannelMethod($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/product-service/channel';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']]	= $result;
			} 
		}		
		return $return;
	}
	public function getAllTeam($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/contact-service/contact-group/';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']]	= $result;
			} 
		}		
		return $return;
	}
	public function getAllLeadsource($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/contact-service/lead-source';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				$return[$accountId][$result['id']]	= $result;
			} 
		}		
		return $return;
	}
	public function getAllPaymentMethod($accountId = ''){
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/accounting-service/payment-method-search';
		$resultss			=  $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results['results'] as $result){
				$result['id']						= $result['1'];
				$result['name']						= $result['2'];
				$return[$accountId][$result['id']]	= $result;
			} 
		}
		return $return;
	}
	public function getAccountInfo($accountId = ''){
		$this->reInitialize();
		$this->getByIdKey	= '';
		$url				= '/integration-service/account-configuration';
		$return				= $this->getCurl($url);
		return $return;
	}
	public function getAllAllowance(){
		$datasTemps	= $this->ci->db->get('products')->result_array();
		$return		= array();
		foreach($datasTemps as $datasTemp){
			$datasTemp['id']											= $datasTemp['productId'];
			$datasTemp['name']											= $datasTemp['sku'];
			$return[$datasTemp['account1Id']][$datasTemp['productId']]	= $datasTemp;
		}
		return $return;
	}
	public function getAllSalesCustomField($accountId = ''){
		$bpconfig	= $this->ci->account1Config[$accountId];
		if(!$bpconfig){
			$bpconfig	= $this->ci->account1Config['1'];
		}
		
		$this->reInitialize($accountId);
		$this->getByIdKey	= '';
		$url				= '/order-service/sale/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		$return				= array();
		foreach($resultss as $accountId => $results){
			foreach($results as $result){
				if($result['code'] != $bpconfig['CustomFieldMappingFieldName']){
					continue;
				}
				else{
					foreach($result['options'] as $options){
						$return[$accountId][$options['id']]	= $options;
					}
				}
			}
		}
		return $return;
	}
	public function getAllSalesCustomFieldForTax($accountId = ''){
		$return		= array();
		$bpconfig	= $this->ci->account1Config[$accountId];
		if(!$bpconfig){
			$bpconfig	= $this->ci->account1Config['1'];
		}
		
		
		if(substr_count(strtolower($bpconfig['taxAsLine']),'pcf_')){
			$this->reInitialize($accountId);
			$this->getByIdKey	= '';
			$url				= '/order-service/sale/custom-field-meta-data/';
			$resultss			= $this->getCurl($url);
			foreach($resultss as $accountId => $results){
				foreach($results as $result){
					$tempCustomField	= explode('.', $bpconfig['taxAsLine']);
					$tempCustomField	= $tempCustomField[1];
					
					
					if($result['code'] != $tempCustomField){
						continue;
					}
					else{
						foreach($result['options'] as $options){
							$return[$accountId][$options['id']]	= $options;
						}
					}
				}
			}
		}
		return $return;
	}
	public function salesOrderAPIField(){
		$this->reInitialize();
		$fieldsDatas	= array("reference" => "reference","createdById" => "createdById","currency.accountingCurrencyCode" => "accountingCurrencyCode","assignment.current.staffOwnerContactId" => "staffOwnerContactId","assignment.current.leadSourceId" => "leadSourceId","parties.customer.contactId" => "Customer contactId","parties.customer.companyName" => "Customer companyName","parties.customer.postalCode" => "Customer postalCode","parties.customer.country" => "Customer country","parties.customer.countryIsoCode" => "Customer countryIsoCode","parties.customer.countryIsoCode3" => "Customer countryIsoCode3","parties.delivery.contactId" => "Delivery contactId","parties.delivery.companyName" => "Delivery companyName","parties.delivery.postalCode" => "Delivery postalCode","parties.delivery.country" => "Delivery country","parties.delivery.countryIsoCode" => "Delivery countryIsoCode","parties.delivery.countryIsoCode3" => "Delivery countryIsoCode3","parties.billing.contactId" => "Billing contactId","parties.billing.companyName" => "Billing companyName","parties.billing.postalCode" => "Billing postalCode","parties.billing.country" => "Billing country","parties.billing.countryIsoCode" => "Billing countryIsoCode","parties.billing.countryIsoCode3" => "Billing countryIsoCode3","warehouseId" => "warehouseId");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array(
				'id'	=> $id,
				'name'	=> $name
			);
		}
		$return				= array();
		/* $url				= '/order-service/sale/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		if($resultss){
			foreach($resultss as $accountId => $results){
				foreach($results as $result){
					if($result['code'] AND $result['name']){
						$row	= array(
							'id'	=> 'customFields.'.$result['code'],
							'name'	=> $result['code'].' - '.$result['name'],
						);
						$return[$row['id']] = $row;	
					}
				} 
			}
		} */
		$returnData	= $returnData + $return;
		return $returnData;
	}
	public function salesOrderFieldConfig(){
		$this->reInitialize();
		$fieldsDatas = array("id" => "id","parentOrderId" => "parentOrderId","reference" =>  "reference","orderPaymentStatus" =>  "orderPaymentStatus","placedOn"=>  "placedOn","createdOn"=>  "createdOn","updatedOn"=>  "updatedOn","closedOn"=>  "closedOn","createdById" =>  "createdById","priceListId"=>  "priceListId","delivery.deliveryDate" =>  "deliveryDate","delivery.shippingMethodId" =>  "shippingMethodId","invoices.0.invoiceReference" =>  "invoice Reference","invoices.0.taxDate" =>  "invoice taxDate","invoices.0.dueDate" =>  "invoice dueDate","currency.accountingCurrencyCode" =>  "accountingCurrencyCode","currency.orderCurrencyCode" =>  "orderCurrencyCode","currency.exchangeRate" =>  "exchangeRate","currency.fixedExchangeRate" =>  "fixedExchangeRate","totalValue.net" =>  "net","totalValue.taxAmount" =>  "taxAmount","totalValue.baseNet" =>  "baseNet","totalValue.baseTaxAmount" =>  "baseTaxAmount","totalValue.baseTotal" =>  "baseTotal","totalValue.total" =>  "total","assignment.current.staffOwnerContactId" =>  "staffOwnerContactId","assignment.current.channelId" =>  "channelId","assignment.current.leadSourceId" =>  "leadSourceId","parties.customer.contactId" => "Customer contactId","parties.customer.addressFullName" => "Customer addressFullName","parties.customer.companyName" => "Customer companyName","parties.customer.addressLine1" => "Customer addressLine1","parties.customer.addressLine2" => "Customer addressLine2","parties.customer.addressLine3" => "Customer addressLine3","parties.customer.addressLine4" => "Customer addressLine4","parties.customer.postalCode" => "Customer postalCode","parties.customer.country" => "Customer country","parties.customer.countryIsoCode" => "Customer countryIsoCode","parties.customer.countryIsoCode3" => "Customer countryIsoCode3","parties.customer.telephone" => "Customer telephone","parties.customer.mobileTelephone" => "Customer mobileTelephone","parties.customer.email" => "Customer email","parties.delivery.contactId" => "Delivery contactId","parties.delivery.addressFullName" => "Delivery addressFullName","parties.delivery.companyName" => "Delivery companyName","parties.delivery.addressLine1" => "Delivery addressLine1","parties.delivery.addressLine2" => "Delivery addressLine2","parties.delivery.addressLine3" => "Delivery addressLine3","parties.delivery.addressLine4" => "Delivery addressLine4","parties.delivery.postalCode" => "Delivery postalCode","parties.delivery.country" => "Delivery country","parties.delivery.countryIsoCode" => "Delivery countryIsoCode","parties.delivery.countryIsoCode3" => "Delivery countryIsoCode3","parties.delivery.telephone" => "Delivery telephone","parties.delivery.mobileTelephone" => "Delivery mobileTelephone","parties.delivery.email" => "Delivery email","parties.billing.contactId" => "Billing contactId","parties.billing.addressFullName" => "Billing addressFullName","parties.billing.companyName" => "Billing companyName","parties.billing.addressLine1" => "Billing addressLine1","parties.billing.addressLine2" => "Billing addressLine2","parties.billing.addressLine3" => "Billing addressLine3","parties.billing.addressLine4" => "Billing addressLine4","parties.billing.postalCode" => "Billing postalCode","parties.billing.country" => "Billing country","parties.billing.countryIsoCode" => "Billing countryIsoCode","parties.billing.countryIsoCode3" => "Billing countryIsoCode3","parties.billing.telephone" => "Billing telephone","parties.billing.mobileTelephone" => "Billing mobileTelephone","parties.billing.email" => "Billing email","warehouseId"=> "warehouseId");
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array(
				'id'	=> $id,
				'name'	=> $name
			);
		}
		$return				= array();
		$url				= '/order-service/sale/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		if($resultss){
			foreach($resultss as $accountId => $results){
				foreach($results as $result){
					if($result['code'] AND $result['name']){
						$row	= array(
							'id'	=> 'customFields.'.$result['code'],
							'name'	=> $result['code'].' - '.$result['name'],
						);
						$return[$row['id']] = $row;	
					}
				} 
			}
		}
		$returnData	= $returnData + $return;
		return $returnData;
	}
	public function purchaseOrderFieldConfig(){
		$this->reInitialize();
		$fieldsDatas	= array("id" => "id","parentOrderId" => "parentOrderId","reference" =>  "reference","orderPaymentStatus" =>  "orderPaymentStatus","placedOn"=>  "placedOn","createdOn"=>  "createdOn","updatedOn"=>  "updatedOn","closedOn"=>  "closedOn","createdById" =>  "createdById","priceListId"=>  "priceListId","delivery.deliveryDate" =>  "deliveryDate","delivery.shippingMethodId" =>  "shippingMethodId","invoices.0.invoiceReference" =>  "invoice Reference","invoices.0.taxDate" =>  "invoice taxDate","invoices.0.dueDate" =>  "invoice dueDate","currency.accountingCurrencyCode" =>  "accountingCurrencyCode","currency.orderCurrencyCode" =>  "orderCurrencyCode","currency.exchangeRate" =>  "exchangeRate","currency.fixedExchangeRate" =>  "fixedExchangeRate","totalValue.net" =>  "net","totalValue.taxAmount" =>  "taxAmount","totalValue.baseNet" =>  "baseNet","totalValue.baseTaxAmount" =>  "baseTaxAmount","totalValue.baseTotal" =>  "baseTotal","totalValue.total" =>  "total","assignment.current.staffOwnerContactId" =>  "staffOwnerContactId","assignment.current.channelId" =>  "channelId","assignment.current.leadSourceId" =>  "leadSourceId","parties.supplier.contactId" => "Supplier contactId","parties.supplier.addressFullName" => "Supplier addressFullName","parties.supplier.companyName" => "Supplier companyName","parties.supplier.addressLine1" => "Supplier addressLine1","parties.supplier.addressLine2" => "Supplier addressLine2","parties.supplier.addressLine3" => "Supplier addressLine3","parties.supplier.addressLine4" => "Supplier addressLine4","parties.supplier.postalCode" => "Supplier postalCode","parties.supplier.country" => "Supplier country","parties.supplier.countryIsoCode" => "Supplier countryIsoCode","parties.supplier.countryIsoCode3" => "Supplier countryIsoCode3","parties.supplier.telephone" => "Supplier telephone","parties.supplier.mobileTelephone" => "Supplier mobileTelephone","parties.supplier.email" => "Supplier email","parties.delivery.contactId" => "Delivery contactId","parties.delivery.addressFullName" => "Delivery addressFullName","parties.delivery.companyName" => "Delivery companyName","parties.delivery.addressLine1" => "Delivery addressLine1","parties.delivery.addressLine2" => "Delivery addressLine2","parties.delivery.addressLine3" => "Delivery addressLine3","parties.delivery.addressLine4" => "Delivery addressLine4","parties.delivery.postalCode" => "Delivery postalCode","parties.delivery.country" => "Delivery country","parties.delivery.countryIsoCode" => "Delivery countryIsoCode","parties.delivery.countryIsoCode3" => "Delivery countryIsoCode3","parties.delivery.telephone" => "Delivery telephone","parties.delivery.mobileTelephone" => "Delivery mobileTelephone","parties.delivery.email" => "Delivery email","parties.billing.contactId" => "Billing contactId","parties.billing.addressFullName" => "Billing addressFullName","parties.billing.companyName" => "Billing companyName","parties.billing.addressLine1" => "Billing addressLine1","parties.billing.addressLine2" => "Billing addressLine2","parties.billing.addressLine3" => "Billing addressLine3","parties.billing.addressLine4" => "Billing addressLine4","parties.billing.postalCode" => "Billing postalCode","parties.billing.country" => "Billing country","parties.billing.countryIsoCode" => "Billing countryIsoCode","parties.billing.countryIsoCode3" => "Billing countryIsoCode3","parties.billing.telephone" => "Billing telephone","parties.billing.mobileTelephone" => "Billing mobileTelephone","parties.billing.email" => "Billing email","warehouseId"=> "warehouseId"); 
		foreach($fieldsDatas as $id => $name){
			$returnData[$id]	= array(
				'id'	=> $id,
				'name'	=> $name
			);
		}
		$return				= array();
		$url				= '/order-service/purchase/custom-field-meta-data/';
		$resultss			= $this->getCurl($url);
		if($resultss){
			foreach($resultss as $accountId => $results){
				foreach($results as $result){
					if($result['code'] AND $result['name']){
						$row	= array(
							'id'	=> 'customFields.'.$result['code'],
							'name'	=> $result['code'].' - '.$result['name'],
						);
						$return[$row['id']] = $row;	
					}
				} 
			}
		}
		$returnData = $returnData + $return;
		return $returnData;
	}
	public function fetchSalesReport(){
		$this->reInitialize();
		$return		= array();
		$orderIds	= array();
		foreach($this->accountDetails as $account1Id => $accountDetails){
			$saveOrdersData	= $this->ci->db->select('orderId,createOrderId')->where_in('account1Id',$account1Id)->get('sales_order')->result_array();
			$dbOrderIDs		= array();
			foreach($saveOrdersData as $saveOrdersDataTemp){
				$dbOrderIDs[$saveOrdersDataTemp['orderId']]	= $saveOrdersDataTemp;
			}
			$url		= '/order-service/order-search?orderTypeId=1&updatedOn='.date('Y-m-d',strtotime("-30 days")).'/';
			$response	= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];  
			$header		= @array_column($response['metaData']['columns'],'name');
			if(@$response['results']){
				foreach($response['results'] as $result){
					if($dbOrderIDs[$result['0']]){
						$orderIds[$result['0']]	= $result['0'];
					}
				}
				if($response['metaData']){
					for($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)){
						$url1		= $url . '&firstResult=' . $i;
						$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
						if($response1['results']){
							foreach($response1['results'] as $result){
								if($dbOrderIDs[$result['0']]){
									$orderIds[$result['0']]	= $result['0'];
								}
							}
						}
					}
				}
			}
			if($orderIds){
				$paymentResponses	= array();
				$url				= '/accounting-service/customer-payment-search?createdOn='.date('Y-m-d',strtotime("-30 days")).'/'.'/&sort=createdOn.DESC';
				$response			= $this->getCurl($url, "GET", '', 'json', $account1Id)[$account1Id];
				if (@$response['results']){
					$paymentResponses[]	= $response;
					if ($response['metaData']['resultsAvailable'] > 500) {
						for ($i = 500; $i <= $response['metaData']['resultsAvailable']; $i = ($i + 500)) {
							$url1		= $url . '&firstResult=' . $i;
							$response1	= $this->getCurl($url1, "GET", '', 'json', $account1Id)[$account1Id];
							if ($response1['results']) {
								$paymentResponses[]	= $response1;
							}
						}
					}
				}
				$paymentResultsDatas	= array();
				$paymentResultsData		= array();
				if($paymentResponses){
					foreach($paymentResponses as $allResults){
						$headers	= array_column($allResults['metaData']['columns'],'name');
						foreach($allResults['results'] as $paymentResultsTemp){
							$paymentResultsData										= array_combine($headers,$paymentResultsTemp);
							$paymentResultsDatas[$paymentResultsData['orderId']][]	= $paymentResultsData;
						}
					}
				}
				sort($orderIds);
				$orderIds		= array_chunk($orderIds,200);
				foreach($orderIds as $orderId){
					$bpOrderDatasTemps	= $this->getResultById($orderId,'/order-service/order/',$account1Id,200,0,'?includeOptional=customFields,nullCustomFields');
					foreach($bpOrderDatasTemps as $bpOrderDatasTemp){
						$paymentResultsTemps	= array();
						$paymentResultsTemps	= $paymentResultsDatas[$bpOrderDatasTemp['id']];
						$positiveAmount			= 0;
						$negativeAmount			= 0;
						$paymentResponses		= array();
						if($paymentResultsTemps){
							foreach($paymentResultsTemps as $paymentResults){
								if($paymentResults['paymentType'] == 'RECEIPT'){
									$positiveAmount	+= $paymentResults['amountPaid'];
								}
								elseif($paymentResults['paymentType'] == 'PAYMENT'){
									$negativeAmount	+= $paymentResults['amountPaid'];
								}
								$paymentResponses[$paymentResults['paymentId']]	= $paymentResults;
							}
						}
						$positiveAmount	= $positiveAmount - $negativeAmount;
						$return[$account1Id][$bpOrderDatasTemp['id']]	= array(
							'orderId'			=> $bpOrderDatasTemp['id'],
							'createOrderId'		=> $dbOrderIDs[$bpOrderDatasTemp['id']]['createOrderId'],
							'type'				=> 'salesOrder',
							'bpInvoice'			=> $bpOrderDatasTemp['invoices'][0]['invoiceReference'],
							'orderAmountBP'		=> $bpOrderDatasTemp['totalValue']['total'],
							'netAmountBP'		=> $bpOrderDatasTemp['totalValue']['net'],
							'taxAmountBP'		=> $bpOrderDatasTemp['totalValue']['taxAmount'],
							'paidAmountBP'		=> $positiveAmount,
							'bpCreateDate'		=> date('Y-m-d H:i:s',strtotime($bpOrderDatasTemp['createdOn'])),
							'bpTaxDate'			=> date('Y-m-d H:i:s',strtotime($bpOrderDatasTemp['invoices']['0']['taxDate'])),
							'bpRowData'			=> json_encode($bpOrderDatasTemp),
							'bpPaymentRowData'	=> json_encode($paymentResponses),
						);
					}
				}
			}
		}
		return $return;
	}
	public function closeAllPayments($orderIds= array(), $dbTable = ''){
		if((!$orderIds) OR (!$dbTable)){
			return false;
		}
		$allpayments	= $this->ci->db->where_in('orderId',$orderIds)->select('orderId,paymentDetails,status')->get_where($dbTable)->result_array();
		if($allpayments){
			foreach($allpayments as $allpaymentsData){
				$updateArray	= array();
				$orderId		= $allpaymentsData['orderId'];
				$paymentDetails	= json_decode($allpaymentsData['paymentDetails'],true);
				if(!$paymentDetails){
					continue;
				}
				foreach($paymentDetails as $pkey => $paymentDetail){
					$paymentDetails[$pkey]['status']	= 1;
					if($paymentDetail['IsReversal'] == 1){
						$paymentDetails[$pkey]['DeletedonBrightpearl']	= 'YES';
					}
				}
				$updateArray['paymentDetails']		= json_encode($paymentDetails);
				$updateArray['isPaymentCreated']	= 1;
				if($allpaymentsData['status'] != 4){
					$updateArray['status']	= 3;
				}
				$this->ci->db->where(array('orderId' => $orderId))->update($dbTable,$updateArray);
			}
		}
	}
	public function removeErrorFlag($ids= array() , $dbTable= ''){
		if((!$ids) OR (!$dbTable)){
			return false;
		}
		$allErrorKeys	= array('lastTry', 'lastUninvoiceTry');
		$tabletype1		= array('sales_order', 'sales_credit_order', 'purchase_order', 'purchase_credit_order');
		$tabletype2		= array('cogs_journal', 'amazon_ledger', 'stock_journals');
		$tabletype3		= array('stock_adjustment');
		
		$allOrderDatas	= array();
		if(in_array($dbTable, $tabletype1)){
			$allOrderDatas	= $this->ci->db->where_in('id',$ids)->select('id,createdRowData,paymentDetails')->get_where($dbTable)->result_array();
		}
		elseif(in_array($dbTable, $tabletype2)){
			$allOrderDatas	= $this->ci->db->where_in('id',$ids)->select('id,createdParams')->get_where($dbTable)->result_array();
		}
		elseif(in_array($dbTable, $tabletype3)){
			$allOrderDatas	= $this->ci->db->where_in('id',$ids)->select('id,createdRowData')->get_where($dbTable)->result_array();
		}
		
		if($allOrderDatas){
			foreach($allOrderDatas as $allOrderData){
				$updateArray	= array();
				$RowId			= $allOrderData['id'];
				
				if((in_array($dbTable, $tabletype1)) OR (in_array($dbTable, $tabletype3))){
					if($allOrderData['createdRowData']){
						$createdRowData	= json_decode($allOrderData['createdRowData'],true);
						foreach($createdRowData as $KeyValue => $createdRowDataTemp){
							if(!in_array($KeyValue, $allErrorKeys)){
								continue;
							}
							else{
								unset($createdRowData[$KeyValue]);
							}
						}
						$updateArray['createdRowData']	= json_encode($createdRowData);
					}
					if(in_array($dbTable, $tabletype1)){
						if($allOrderData['paymentDetails']){
							$paymentDetails	= json_decode($allOrderData['paymentDetails'],true);
							foreach($paymentDetails as $Key => $paymentDetail){
								if($paymentDetail['lastTry']){
									unset($paymentDetails[$Key]['lastTry']);
									continue;
								}
							}
							$updateArray['paymentDetails']	= json_encode($paymentDetails);
						}
					}
					$this->ci->db->where(array('id' => $RowId))->update($dbTable,$updateArray);
				}
				elseif(in_array($dbTable, $tabletype2)){
					if($allOrderData['createdParams']){
						$createdParams	= json_decode($allOrderData['createdParams'],true);
						foreach($createdParams as $KeyValue => $createdParamsTemp){
							if(!in_array($KeyValue, $allErrorKeys)){
								continue;
							}
							else{
								unset($createdParams[$KeyValue]);
							}
						}
						$updateArray['createdParams']	= json_encode($createdParams);
					}
					$this->ci->db->where(array('id' => $RowId))->update($dbTable,$updateArray);
				}
			}
		}
	}
	public function sendAPIalert($accountName = '' ,$errorMessage = ''){
		include_once('Mailer.php');
		$obj	= new Mailer();
		if(!$accountName){
			$accountName	= 'Brightpearl Account';
		}
		$errormsg			= '';
		$appName			= $this->ci->globalConfig['app_name'];
		$from				= 'contact@bsitc-apps.com';
		//$emailReceipents	= $this->globalConfig['emailReceipents'];
		$emailReceipents	= 'deepakgoyal@businesssolutionsinthecloud.com';
		$subject			= $appName.' - Brightpearl Connection Error';
		/* if(!$emailReceipents){
			$emailReceipents	= 'deepakgoyal@businesssolutionsinthecloud.com,rachel@businesssolutionsinthecloud.com,chirag@businesssolutionsinthecloud.com,neha@businesssolutionsinthecloud.com,avinash@businesssolutionsinthecloud.com';
		} */
		if(substr_count($errorMessage,'Unable to authenticate')){
			$errormsg		= 'Brightpearl Password has been expired for your account "'.$accountName.'". Please update the password in connector before making next API call.';
		}
		else{
			
		}
		$body	= 'Hi,<br><br>You Have the following Error for <b>'.$accountName.'</b> Brightpearl Account.<br><br>'.$errormsg.'<br><br><b>Error Message :</b> '.$errorMessage.'<br><br>Thanks & Regards<br>The BSITC Team' ;
		$obj->send($emailReceipents,$subject,$body,$from);
		die;
	}
}