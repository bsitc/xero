<?php
$this->reInitialize();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$this->ci->globalConfig['enablePrepayments']){continue;}
	if(!$clientcode){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$advanceDatas		= $this->ci->db->get_where('arAdvance',array('account2Id' => $account2Id , 'status' => 1))->result_array();
	if(empty($advanceDatas)){continue;}
	
	$allOrderIds		= array_column($advanceDatas,'orderId');
	$allOrderIds		= array_filter($allOrderIds);
	$allOrderIds		= array_unique($allOrderIds);
	if(empty($allOrderIds)){continue;}
	
	$allSalesInfo		= array();
	$salesOrdersDatas	= $this->ci->db->where_in('orderId',$allOrderIds)->get_where('sales_order',array('account2Id' => $account2Id , 'createOrderId <>' => ''))->result_array();
	if(empty($salesOrdersDatas)){continue;}
	foreach($salesOrdersDatas as $salesOrdersData){
		$allSalesInfo[$salesOrdersData['orderId']]	= $salesOrdersData; 
	}
	
	$allPostiveAdvanceInfo	= array();
	$allNegativeAdvanceInfo	= array();
	foreach($advanceDatas as $advanceData){
		if(isset($allSalesInfo[$advanceData['orderId']])){
			if(strtolower($advanceData['paymentType']) == 'receipt'){
				$allPostiveAdvanceInfo[$advanceData['orderId']][$advanceData['paymentMethodCode']][$advanceData['paymentId']] = $advanceData;
			}
			if(strtolower($advanceData['paymentType']) == 'capture'){
				$allPostiveAdvanceInfo[$advanceData['orderId']][$advanceData['paymentMethodCode']][$advanceData['paymentId']] = $advanceData;
			}
			if(strtolower($advanceData['paymentType']) == 'payment'){
				$allNegativeAdvanceInfo[$advanceData['orderId']][$advanceData['paymentMethodCode']][$advanceData['paymentId']] = $advanceData;
			}
		}
	}
	
	$reversePaymentIds	= array();
	if(!empty($allNegativeAdvanceInfo)){
		foreach($allNegativeAdvanceInfo as $nOrderId => $allNegativeAdvanceInfos){
			foreach($allNegativeAdvanceInfos as $nPaymentMethodCode => $allNegativeAdvanceInfoss){
				foreach($allNegativeAdvanceInfoss as $nPaymentId => $allNegativeAdvanceInfoss){
					if(isset($allNegativeAdvanceInfoss['isClosed'])){continue;}
					if((!empty($allPostiveAdvanceInfo)) AND (isset($allPostiveAdvanceInfo[$nOrderId][$nPaymentMethodCode]))){
						foreach($allPostiveAdvanceInfo[$nOrderId][$nPaymentMethodCode] as $pPaymentId => $allPostiveAdvanceInfos){
							if(isset($allPostiveAdvanceInfos['isClosed'])){continue;}
							if($allPostiveAdvanceInfos['amountPaid'] == $allNegativeAdvanceInfoss['amountPaid']){
								$reversePaymentIds[]	= $nPaymentId;
								$reversePaymentIds[]	= $pPaymentId;
								$allPostiveAdvanceInfo[$nOrderId][$nPaymentMethodCode][$pPaymentId]['isClosed']		= 1;
								$allNegativeAdvanceInfo[$nOrderId][$nPaymentMethodCode][$nPaymentId]['isClosed']	= 1;
								break;
							}
						}
					}
					elseif((!empty($allPostiveAdvanceInfo)) AND (isset($allPostiveAdvanceInfo[$nOrderId]))){
						$isReverseFound	= 0;
						foreach($allPostiveAdvanceInfo[$nOrderId] as $pPaymentMethodCode => $allPostiveAdvanceInfos){
							foreach($allPostiveAdvanceInfos as $pPaymentId => $allPostiveAdvanceInfoss){
								if(isset($allPostiveAdvanceInfoss['isClosed'])){continue;}
								if($allPostiveAdvanceInfoss['amountPaid'] == $allNegativeAdvanceInfoss['amountPaid']){
									$reversePaymentIds[]	= $nPaymentId;
									$reversePaymentIds[]	= $pPaymentId;
									$allPostiveAdvanceInfo[$nOrderId][$pPaymentMethodCode][$pPaymentId]['isClosed']		= 1;
									$allNegativeAdvanceInfo[$nOrderId][$nPaymentMethodCode][$nPaymentId]['isClosed']	= 1;
									$isReverseFound			= 1;
									break;
								}
							}
							if($isReverseFound){break;}
						}
					}
				}
			}
		}
	}
	
	if(!empty($reversePaymentIds)){
		$reversePaymentIds	= array_filter($reversePaymentIds);
		$reversePaymentIds	= array_unique($reversePaymentIds);
		$this->ci->db->where_in('paymentId', $reversePaymentIds)->update('arAdvance',array('message' => '', 'status' => 2));
	}
	
	if(!empty($allPostiveAdvanceInfo)){
		foreach($allPostiveAdvanceInfo as $pOrderId => $allPostiveAdvanceInfos){
			foreach($allPostiveAdvanceInfos as $pPaymentMethodCode => $allPostiveAdvanceInfoss){
				foreach($allPostiveAdvanceInfoss as $pPaymentId => $allPostiveAdvanceInfosss){
					if(isset($allPostiveAdvanceInfosss['isClosed'])){continue;}
					if(!isset($allSalesInfo[$allPostiveAdvanceInfosss['orderId']])){continue;}
					if(!$allPostiveAdvanceInfosss['prePaymentId']){continue;}
					if($allPostiveAdvanceInfosss['paymentType'] == 'PAYMENT'){continue;}
					
					$createdRowData		= json_decode($allPostiveAdvanceInfosss['createdRowData'],true);
					$advanceAmount		= $allPostiveAdvanceInfosss['amountPaid'];
					$invoiceDueAmount	= 0;
					$invoiceTotalAmount	= 0;
					$xeroInvoiceId		= $allSalesInfo[$allPostiveAdvanceInfosss['orderId']]['createOrderId'];
					$this->headers		= array();
					$getInvoice			= '2.0/Invoice/'.$xeroInvoiceId;
					$this->initializeConfig($account2Id, 'GET', $getInvoice);
					$getInvoiceRes		= $this->getCurl($getInvoice, 'GET', '', 'json', $account2Id)[$account2Id];
					if((is_array($getInvoiceRes)) AND (!empty($getInvoiceRes)) AND (isset($getInvoiceRes['Status'])) AND (strtolower($getInvoiceRes['Status']) == 'ok')){
						if(isset($getInvoiceRes['Invoices']['Invoice']['InvoiceID'])){
							$invoiceDueAmount	= $getInvoiceRes['Invoices']['Invoice']['AmountDue'];
							$invoiceTotalAmount	= $getInvoiceRes['Invoices']['Invoice']['Total'];
						}
					}
					if($invoiceDueAmount >= $advanceAmount){
						$applyRequest	= array(
							"Amount"		=> $advanceAmount,
							"Invoice"		=> array("InvoiceID" => $xeroInvoiceId),
						);
						$this->headers	= array();
						$applyUrl		= '2.0/Prepayments/'.$allPostiveAdvanceInfosss['prePaymentId'].'/Allocations';
						$this->initializeConfig($account2Id, 'PUT', $applyUrl);
						$applyResponse	= $this->getCurl($applyUrl, 'PUT', json_encode($applyRequest), 'json', $account2Id)[$account2Id];
						
						$createdRowData['Apply Request :']	= $applyRequest;
						$createdRowData['Apply Response :']	= $applyResponse;
						if((is_array($applyResponse)) AND (!empty($applyResponse)) AND (isset($applyResponse['Status'])) AND (strtolower($applyResponse['Status']) == 'ok') AND (isset($applyResponse['Allocations']['Allocation']['AllocationID']))){
							$updateArray	= array(
								'createdRowData'	=> json_encode($createdRowData),
								'status'			=> 3,
								'applicationId'		=> $applyResponse['Allocations']['Allocation']['AllocationID'],
								'message'			=> '',
								'paymentId'			=> $allPostiveAdvanceInfosss['paymentId'],
								'invoiceId'			=> $xeroInvoiceId,
							);
							$this->ci->db->where(array('paymentId' => $allPostiveAdvanceInfosss['paymentId']))->update('arAdvance',$updateArray);
						}
						else{
							$this->ci->db->update('arAdvance',array('createdRowData' => json_encode($createdRowData)),array('paymentId' => $allPostiveAdvanceInfosss['paymentId']));
						}
					}
					elseif($invoiceDueAmount < $advanceAmount){
						$applyRequest	= array(
							"Amount"		=> $invoiceDueAmount,
							"Invoice"		=> array("InvoiceID" => $xeroInvoiceId),
						);
						$this->headers	= array();
						$applyUrl		= '2.0/Prepayments/'.$allPostiveAdvanceInfosss['prePaymentId'].'/Allocations';
						$this->initializeConfig($account2Id, 'PUT', $applyUrl);
						$applyResponse	= $this->getCurl($applyUrl, 'PUT', json_encode($applyRequest), 'json', $account2Id)[$account2Id];
						
						$createdRowData['Apply Request :']	= $applyRequest;
						$createdRowData['Apply Response :']	= $applyResponse;
						if((is_array($applyResponse)) AND (!empty($applyResponse)) AND (isset($applyResponse['Status'])) AND (strtolower($applyResponse['Status']) == 'ok') AND (isset($applyResponse['Allocations']['Allocation']['AllocationID']))){
							$updateArray	= array(
								'createdRowData'	=> json_encode($createdRowData),
								'status'			=> 3,
								'applicationId'		=> $applyResponse['Allocations']['Allocation']['AllocationID'],
								'message'			=> '',
								'paymentId'			=> $allPostiveAdvanceInfosss['paymentId'],
								'invoiceId'			=> $xeroInvoiceId,
							);
							$this->ci->db->where(array('paymentId' => $allPostiveAdvanceInfosss['paymentId']))->update('arAdvance',$updateArray);
						}
						else{
							$this->ci->db->update('arAdvance',array('createdRowData' => json_encode($createdRowData)),array('paymentId' => $allPostiveAdvanceInfosss['paymentId']));
						}
					}
				}
			}
		}
	}
}
$this->reverseAraadvance();