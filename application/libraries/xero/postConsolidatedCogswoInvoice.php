<?php
$this->reInitialize();
$enableAggregation		= $this->ci->globalConfig['enableAggregation'];
$enableConsolidationMappingCustomazation	= $this->ci->globalConfig['enableConsolidationMappingCustomazation'];
$getAllCurrencyWithRate	= $this->ci->brightpearl->getAllCurrencyWithRate();
$clientcode				= $this->ci->config->item('clientcode');
$dateLockSettings		= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	if(!$this->ci->globalConfig['enableCOGSWOInvoice']){continue;}
	if($this->ci->globalConfig['enableCOGSJournals']){continue;}
	//cogs for Keen DIST LTD. is stopped, req by Neha on 9th Fab 2023
	if($clientcode == 'keenxero'){if($account2Id == 5){continue;}}
	
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	$datas	= $this->ci->db->get_where('cogs_woinvoice_journal',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(empty($datas)){continue;}
	$allCogsOrderIds	= array_column($datas,'orderId');
	$allCogsOrderIds	= array_filter($allCogsOrderIds);
	$allCogsOrderIds	= array_unique($allCogsOrderIds);
	
	$this->ci->db->reset_query();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	if(!empty($AggregationMappingsTemps)){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			
			if(!$ConsolMappingCustomField AND !$account1APIFieldId){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]	= $AggregationMappingsTemp;
			}
			else{
				if($account1APIFieldId){
					$allAPIFieldsValues	= explode("||", trim($account1APIFieldId));
					foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
					}
				}
				else{
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
				}
			}
		}
	}
	if((empty($AggregationMappings)) AND (empty($AggregationMappings2))){continue;}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if(!empty($channelMappingsTemps)){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$ChannelcustomfieldMappingKey		= '';
	$ChannelcustomfieldMappings			= array();
	$ChannelcustomfieldMappingsTemps	= $this->ci->db->order_by('id','desc')->get_where('mapping_channelcustomfield',array('account2Id' => $account2Id))->result_array();
	if($ChannelcustomfieldMappingsTemps){
		foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
			if($ChannelcustomfieldMappingsTemp['account1CustomFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1CustomFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1APIFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1APIFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1WarehouseId']){
				$ChannelcustomfieldMappingKey	= 'account1WarehouseId';
				break;
			}
			else{
				break;
			}
		}
		if($ChannelcustomfieldMappingKey){
			foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
				if($ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey]){
					$ChannelcustomfieldChannel		= $ChannelcustomfieldMappingsTemp['account1ChannelId'];
					$ChannelcustomfieldKey2Value	= $ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey];
					$ChannelcustomfieldMappings[$ChannelcustomfieldChannel][$ChannelcustomfieldKey2Value]	= $ChannelcustomfieldMappingsTemp;
				}
			}
		}
	}
		
	$postDatasTmpSO	= array();
	$postDatasTmpSC	= array();
	$jounralDataSO	= array();
	$jounralDataSC	= array();
	if(!empty($datas)){
		$OrdersByChannel				= array();
		$OrdersByChannelCurrency		= array();
		$OrdersByChannelCurrencyTaxDate	= array();
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($orderDatas['createdJournalsId']){continue;}
			if($orderDatas['OrderType']=='PO'){ continue; }
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$rowDataPath			= FCPATH.'rowData'. DIRECTORY_SEPARATOR .'cogs'. DIRECTORY_SEPARATOR . $orderDatas['account1Id']. DIRECTORY_SEPARATOR;
			$taxDate				= '';
			$orderId				= $orderDatas['orderId'];
			$journalsId				= $orderDatas['journalsId'];
			$rowDatas				= json_decode($orderDatas['params'],true);
			$orderRowDatas			= json_decode(file_get_contents($rowDataPath.$orderDatas['orderId'].'.json'),true);
			$taxDate				= $rowDatas['taxDate'];
			$OrderType				= $orderDatas['OrderType'];
			$channelId				= $orderDatas['channelId'];
			$CustomFieldValueID		= '';
			$CustomFieldValueID		= (isset($orderRowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'])) ? ($orderRowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']) : ('NA');
			$CurrencyCode			= strtolower($orderRowDatas['currency']['orderCurrencyCode']);
			$accountingCurrencyCode	= strtolower($orderRowDatas['currency']['accountingCurrencyCode']);
			$ConsolAPIFieldValueID	= '';
			
			if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
				continue;
			}
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$orderRowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			if($enableAggregation){
				if($AggregationMappings){
					if((!$AggregationMappings[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings[$channelId]['bpaccountingcurrency'])){
						continue;
					}
					else{
						if($enableConsolidationMappingCustomazation){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']){
								if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
									$ExcludedStringInCustomField		= array();
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
							else{
								if(($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField'])){
									$ExcludedStringInCustomField		= array();
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
					}
				}
				elseif($AggregationMappings2){
					if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency'])){
						continue;
					}
					else{
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							if((!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'])){
								continue;
							}
							else{
								if($enableConsolidationMappingCustomazation){
									if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $orderRowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
									elseif(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
						else{
							if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA'])){
								continue;
							}
							else{
								if($enableConsolidationMappingCustomazation){
									if(($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
									elseif(($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
					}
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						if(!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
							$CustomFieldValueID	= 'NA';
						}
					}
					else{
						if(!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]){
							$CustomFieldValueID	= 'NA';
						}
					}
				}
			}
			$AggregationMappingData	= array();
			if($AggregationMappings){
				if($AggregationMappings[$channelId]['bpaccountingcurrency']){
					$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
					$CustomFieldValueID		= 'NA';
				}
				else{
					$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)];
					$CustomFieldValueID		= 'NA';
				}
			}
			elseif($AggregationMappings2){
				if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
					$AggregationMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
				}
				else{
					$AggregationMappingData	= $AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID];
				}
			}
			
			$consolFrequency		= $AggregationMappingData['consolFrequency'];
			$netOffConsolidation	= $AggregationMappingData['netOffConsolidation'];
			if(!$consolFrequency){
			}
			$consolFrequency	= 1;
			/* if($netOffConsolidation){
				continue;
			} */
			
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate		= $date->format('Ymd');
			}
			else{
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$Acc2Offset		= 0;
				$diff			= $BPDateOffset - $Acc2Offset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
			}
			
			if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['consolsales'])) AND (strlen($dateLockSettings['consolsales']) > 0)){
				$checkTaxDate	= date('Ymd',strtotime($taxDate));
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['consolsales'])));
				
				if($dateLockSettings['consolsalesCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['consolsalesCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['consolsalesCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			//consolFrequency : 1 => Daily, 2 => Monthly
			if($consolFrequency == 1){
				$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
				/* if($taxDate > $fetchTaxDate){
					continue;
				} */
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$OrderType][$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$journalsId]		= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$OrderType][$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$journalsId]				= $orderDatas;
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$OrderType][$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$journalsId]		= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$OrderType][$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$journalsId]				= $orderDatas;
					}
				}
			}
			elseif($consolFrequency == 2){
				$invoiceMonth	= date('m',strtotime($taxDate));
				$currentMonth	= date('m');
				if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
					continue;
				}
				
				$batchKey	= date('y-m',strtotime($taxDate));
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$OrderType][$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$journalsId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$OrderType][$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$journalsId]				= $orderDatas;
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$OrderType][$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$journalsId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$OrderType][$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$journalsId]				= $orderDatas;
					}
				}
			}
		}
	}
	if($OrdersByChannelCurrencyTaxDate){
		foreach($OrdersByChannelCurrencyTaxDate as $OrderType => $OrdersByChannelOrderType){
			foreach($OrdersByChannelOrderType as $taxDateBP => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $rootCurrency => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $CustomFieldValueID => $OrdersByChannels){
							$ItemSequence			= 0;
							$Narration			    = '';
							$currency			    = '';
							$channelId			    = '';
							$trackingDetails	    = array();
							$JournalRequest		    = array();
							$JournalLines		    = array();
							$AllDebits			    = array();
							$AllCredits				= array();
							$DebitByNominalArray	= array();
							$CreditByNominalArray	= array();
							$allProcessedJournalsId	= array();
							$allProcessedOrdersId	= array();
							$taxDate			    = date('Y-m-d',strtotime($taxDateBP));
							$invalidConsolOrderIds	= array();
							$rootCurrency			= $rootCurrency;
							$CurrencyCode			= $rootCurrency;
							$BrightpealBaseCurrency	= '';
							$CustomFieldValueID		= $CustomFieldValueID;
							$results				= array();
							$AggregationMapping		= array();
							$journalAggregation		= 0;
							$ChannelCustomer		= '';
							$consolFrequency		= 0;
							
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)];
							}
							elseif($AggregationMappings2){
								$AggregationMapping	= $AggregationMappings2[$SalesChannel][strtolower($rootCurrency)][$CustomFieldValueID];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
							$uniqueInvoiceRef		= $AggregationMapping['uniqueChannelName'];
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							
							$firstKey				= array_key_first($OrdersByChannels);
							$bpconfig				= $this->ci->account1Config[$OrdersByChannels[$firstKey]['account1Id']];
							$config1				= $this->ci->account1Config[$OrdersByChannels[$firstKey]['account1Id']];
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								continue;
							}
							if($orderForceCurrency){
								if($config['defaultCurrency'] != $bpconfig['currencyCode']){
									continue;
								}
							}
							if($orderForceCurrency){
								$CurrencyCode	= strtoupper($bpconfig['currencyCode']);
							}
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$MainOrderCurrency			= strtolower($OrdersByChannels[$firstKey]['invoiceCurrencyCode']);
							$BrightpearlBaseCurrency	= strtolower($config1['currencyCode']);
							$JournalCalculationVariable	= 1;
							$APIReturnedExRate			= 0;
							$BPReturnedExRate			= 1;
							$XeroReturnedExRate			= 1;

							if($disableSalesPosting == 1){
								if($BrightpearlBaseCurrency != $XeroBaseCurrency){
									if($getAllCurrencyWithRate[$OrdersByChannels[$firstKey]['account1Id']][strtolower($XeroBaseCurrency)]['latestExRate']){
										$APIReturnedExRate	= $getAllCurrencyWithRate[$OrdersByChannels[$firstKey]['account1Id']][strtolower($XeroBaseCurrency)]['latestExRate'];
									}
									if(!$APIReturnedExRate){
										$this->ci->db->where_in('journalsId',array_keys($OrdersByChannels))->update('cogs_journal',array('message' => 'exchange rate not found'));
										continue;
									}
									else{
										$JournalCalculationVariable	= ($JournalCalculationVariable * $APIReturnedExRate);
									}
								}
								$JournalCalculationVariable	= sprintf("%.6f",($JournalCalculationVariable));
							}
							else{
								if($BrightpearlBaseCurrency != $XeroBaseCurrency){
									if($MainOrderCurrency != $BrightpearlBaseCurrency){
										$JournalCalculationVariable	= ($JournalCalculationVariable * $BPReturnedExRate);
									}
									if($MainOrderCurrency != $XeroBaseCurrency){
										$JournalCalculationVariable	= ($JournalCalculationVariable * (1 / $XeroReturnedExRate));
									}
								}
								$JournalCalculationVariable	= sprintf("%.6f",($JournalCalculationVariable));
							}
							foreach($OrdersByChannels as $journalDatas){
								$config1		= $this->ci->account1Config[$journalDatas['account1Id']];
								$JournalType	= $journalDatas['journalTypeCode'];
								$params			= json_decode($journalDatas['params'],true);
								$channelId		= $journalDatas['channelId'];
								
								$AllCredits	= $params['credits'];
								foreach($AllCredits as $key => $CreditsData){
									$CreditByNominalArray[$JournalType][$CreditsData['nominalCode']] += $CreditsData['transactionAmount'];
								}
								$AllDebits	= $params['debits'];
								foreach($AllDebits as $key => $DebitsData){
									$DebitByNominalArray[$JournalType][$DebitsData['nominalCode']] += $DebitsData['transactionAmount'];
								}
								$allProcessedJournalsId[]	= $journalDatas['journalsId'];
							}
							if($DebitByNominalArray AND $CreditByNominalArray){
								if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['consolcogs'])) AND (strlen($dateLockSettings['consolcogs']) > 0)){
									$checkTaxDate	= date('Ymd',strtotime($taxDate));
									$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['consolcogs'])));
									
									if($dateLockSettings['consolcogsCondition'] == '<='){
										if($checkTaxDate <= $orderDateLock){
											//
										}
										else{continue;}
									}
									elseif($dateLockSettings['consolcogsCondition'] == '>='){
										if($checkTaxDate >= $orderDateLock){
											//
										}
										else{continue;}
									}
									elseif($dateLockSettings['consolcogsCondition'] == '='){
										if($checkTaxDate == $orderDateLock){
											//
										}
										else{continue;}
									}
								}
								
								$trackingDetails1	= array();
								$trackingDetails2	= array();
								
								if(isset($channelId)){
									if(isset($channelMappings[$channelId])){
										$trackingDetails1	= $channelMappings[$channelId]['account2ChannelId'];
										$trackingDetails1	= explode("~=",$trackingDetails1);
									}
								}
								if($ChannelcustomfieldMappings AND (isset($channelId))){
									if(($ChannelcustomfieldMappingKey == 'account1CustomFieldId') OR ($ChannelcustomfieldMappingKey == 'account1APIFieldId')){
										if($ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]){
											$trackingDetails2	= $ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]['account2ChannelId'];
											$trackingDetails2	= explode("~=",$trackingDetails2);
										}
									}
									else{
										$trackingDetails2		= array();
									}
								}
								$RefForInvoice		= $taxDate;
								$Narration		= date('Ymd',strtotime($RefForInvoice)).'-'.$uniqueInvoiceRef.'-'.strtoupper($CurrencyCode);
								$ItemSequence	= 0;
								
								foreach($DebitByNominalArray as $TypeOfJournal => $DebitByNominalArrayData){
									foreach($DebitByNominalArrayData as $NominalOfJournal => $DebitAmt){
										if(!$DebitAmt){
											continue;
										}
										if($TypeOfJournal == 'GO'){
											$debitAccRef	= $config['COGSDebitNominalSO'];
											if(isset($nominalMappings[$NominalOfJournal])){
												if($nominalMappings[$NominalOfJournal]['account2NominalId']){
													$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
												}
											}
											if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
												if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
													$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
												}
											}
										}else{
											$debitAccRef	= $config['COGSDebitNominalSC'];
											if($TypeOfJournal == 'PG'){
												$debitAccRef	= $config['COGSDebitNominalSCPG'];
											}
											if($nominalMappings[$NominalOfJournal]['account2NominalId']){
												$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
											}
										}
										$JournalLines[$ItemSequence]	= array(
											'Description'	=> $TypeOfJournal.' COGS Journal',
											'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ($DebitAmt))),
											'AccountCode'	=> $debitAccRef,
											'TaxType'		=> $config['salesNoTaxCode'],
										);
										if($trackingDetails1 OR $trackingDetails2){
											if($trackingDetails1 AND $trackingDetails2){
												$JournalLines[$ItemSequence]['Tracking'][0]	= array(
													'Name'		=> $trackingDetails1['0'],
													'Option'	=> $trackingDetails1['1']
												);
												$JournalLines[$ItemSequence]['Tracking'][1]	= array(
													'Name'		=> $trackingDetails2['0'],
													'Option'	=> $trackingDetails2['1']
												);
											}
											elseif($trackingDetails1 AND !$trackingDetails2){
												$JournalLines[$ItemSequence]['Tracking'][]	= array(
													'Name'		=> $trackingDetails1['0'],
													'Option'	=> $trackingDetails1['1']
												);
											}
											elseif(!$trackingDetails1 AND $trackingDetails2){
												$JournalLines[$ItemSequence]['Tracking'][]	= array(
													'Name'		=> $trackingDetails2['0'],
													'Option'	=> $trackingDetails2['1']
												);
											}
											else{
												unset($JournalLines[$ItemSequence]['Tracking']);
											}
										}
										$ItemSequence++;
									}
								}
								foreach($CreditByNominalArray as $TypeOfJournal => $CreditByNominalArrayData){
									foreach($CreditByNominalArrayData as $NominalOfJournal => $CreditAmt){
										if(!$CreditAmt){
											continue;
										}
										if($TypeOfJournal == 'GO'){
											$creditAccRef	= $config['COGSCreditNominalSO'];
											if(isset($nominalMappings[$NominalOfJournal])){
												if($nominalMappings[$NominalOfJournal]['account2NominalId']){
													$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
												}
											}
											if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
												if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
													$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
												}
											}
										}else{
											$creditAccRef	= $config['COGSCreditNominalSCPG'];
											if($TypeOfJournal == 'SG'){
												$creditAccRef	= $config['COGSCreditNominalSC'];
											}
											if($nominalMappings[$NominalOfJournal]['account2NominalId']){
												$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
											}
											if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal])) AND ($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'])){
												$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
											}
										}
										$JournalLines[$ItemSequence]	= array(
											'Description'	=> $TypeOfJournal.' COGS Journal',
											'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ((-1) * ($CreditAmt)))),
											'AccountCode'	=> $creditAccRef,
											'TaxType'		=> $config['salesNoTaxCode'],
										);
										if($trackingDetails1 OR $trackingDetails2){
											if($trackingDetails1 AND $trackingDetails2){
												$JournalLines[$ItemSequence]['Tracking'][0]	= array(
													'Name'		=> $trackingDetails1['0'],
													'Option'	=> $trackingDetails1['1']
												);
												$JournalLines[$ItemSequence]['Tracking'][1]	= array(
													'Name'		=> $trackingDetails2['0'],
													'Option'	=> $trackingDetails2['1']
												);
											}
											elseif($trackingDetails1 AND !$trackingDetails2){
												$JournalLines[$ItemSequence]['Tracking'][]	= array(
													'Name'		=> $trackingDetails1['0'],
													'Option'	=> $trackingDetails1['1']
												);
											}
											elseif(!$trackingDetails1 AND $trackingDetails2){
												$JournalLines[$ItemSequence]['Tracking'][]	= array(
													'Name'		=> $trackingDetails2['0'],
													'Option'	=> $trackingDetails2['1']
												);
											}
											else{
												unset($JournalLines[$ItemSequence]['Tracking']);
											}
										}
										$ItemSequence++;
									}
								}
								if($JournalLines){
									$JournalRequest	= array(
										'Narration'					=> $Narration,
										'JournalLines'				=> $JournalLines,
										'Date'						=> $taxDate,
										'LineAmountTypes'			=> "NoTax",
										'Status'					=> "POSTED",
										'ShowOnCashBasisReports'	=> false,
									);
									if($clientcode == 'keenxero'){
										$JournalRequest['LineAmountTypes']	= 'Exclusive';
									}
									if($clientcode == 'nlg'){
										$JournalRequest['LineAmountTypes']	= 'Exclusive';
									}
								}
								if($JournalRequest){
									$this->headers	= array();
									$url			= '2.0/ManualJournals';
									$results		= $this->getCurl($url, 'PUT', json_encode($JournalRequest), 'json', $account2Id)[$account2Id];
									$createdParams['Request data	: ']	= $JournalRequest;
									$createdParams['Response data	: ']	= $results;
									$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_woinvoice_journal',array('createdParams' => json_encode($createdParams)));
									if((strtolower($results['Status']) == 'ok') AND (isset($results['ManualJournals']['ManualJournal']['ManualJournalID']))){
										$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_woinvoice_journal',array('status' => 1, 'createdJournalsId' => $results['ManualJournals']['ManualJournal']['ManualJournalID'], 'message' => '', 'isConsolidated' => 1));
									}
								}
							}
						}
					}
				}
			}
		}
	}
}