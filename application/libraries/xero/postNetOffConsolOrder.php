<?php
//XERO_DEMO		: 	POST NETOFF CONSOL INVOICE OR CREDIT NOTE TO XERO
include_once('timezonesData.php');
$dateResults	= array(1=> array('name'=> 'JAN','lastDate'=> '31',),2=> array('name'=> 'FEB','lastDate'=> '28','lastDate2'=> '29',),3=> array('name'=> 'MAR','lastDate'=> '31',),4=> array('name'=> 'APR','lastDate'=> '30',),5=> array('name'=> 'MAY','lastDate'=> '31',),6=> array('name'=> 'JUN','lastDate'=> '30',),7=> array('name'=> 'JUL','lastDate'=> '31',),8=> array('name'=> 'AUG','lastDate'=> '31',),9=> array('name'=> 'SEP','lastDate'=> '30',),10=> array('name'=> 'OCT','lastDate'=> '31',),11=> array('name'=> 'NOV','lastDate'=> '30',),12=> array('name'=> 'DEC','lastDate'=> '31',),);

$this->reInitialize();
$enableAggregation							= $this->ci->globalConfig['enableAggregation'];
$enableChannelcustomfieldMapping			= $this->ci->globalConfig['enableChannelcustomfieldMapping'];
$enableConsolidationMappingCustomazation	= $this->ci->globalConfig['enableConsolidationMappingCustomazation'];
$clientcode									= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	$config	= $this->accountConfig[$account2Id];
	
	$this->ci->db->reset_query();
	$salesOrderdatas	= $this->ci->db->order_by('orderId', 'asc')->get_where('sales_order',array('status' => '0', 'account2Id' => $account2Id))->result_array();
	
	$this->ci->db->reset_query();
	$salesCreditdatas	= $this->ci->db->order_by('orderId', 'asc')->get_where('sales_credit_order',array('status' => '0', 'account2Id' => $account2Id))->result_array();
	
	if(!$salesOrderdatas AND !$salesCreditdatas){
		continue;
	}
	
	$enableAggregationAdvance		= $this->ci->globalConfig['enableAggregationAdvance'];
	$enableAggregationOnAPIfields	= $this->ci->globalConfig['enableAggregationOnAPIfields'];
	
	$this->ci->db->reset_query();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	$consolOnCustomActive		= 0;
	$consolOnAPIActive			= 0;
	if($AggregationMappingsTemps){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			
			if(!$ConsolMappingCustomField AND !$account1APIFieldId){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
			}
			else{
				if($account1APIFieldId){
					$consolOnAPIActive		= 1;
					$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
					foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
					}
				}
				else{
					$consolOnCustomActive	= 1;
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
				}
			}
		}
	}
	if(!$AggregationMappings AND !$AggregationMappings2){
		continue;
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$ChannelcustomfieldMappingKey		= '';
	$ChannelcustomfieldMappings			= array();
	$ChannelcustomfieldMappingsTemps	= $this->ci->db->order_by('id','desc')->get_where('mapping_channelcustomfield',array('account2Id' => $account2Id))->result_array();
	if($ChannelcustomfieldMappingsTemps){
		foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
			if($ChannelcustomfieldMappingsTemp['account1CustomFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1CustomFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1APIFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1APIFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1WarehouseId']){
				$ChannelcustomfieldMappingKey	= 'account1WarehouseId';
				break;
			}
			else{
				break;
			}
		}
		if($ChannelcustomfieldMappingKey){
			foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
				if($ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey]){
					$ChannelcustomfieldChannel		= $ChannelcustomfieldMappingsTemp['account1ChannelId'];
					$ChannelcustomfieldKey2Value	= $ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey];
					$ChannelcustomfieldMappings[$ChannelcustomfieldChannel][$ChannelcustomfieldKey2Value]	= $ChannelcustomfieldMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <> ' => '2'))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	$productMappings			= array();
	if($this->ci->globalConfig['enableAdvanceTaxMapping']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	
	$nominalCodeForShipping	= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForGiftCard	= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForDiscount	= explode(",",$config['nominalCodeForDiscount']);
	
	if($salesOrderdatas OR $salesCreditdatas){
		$salesOrderBatch			= array();
		$salesCreditBatch			= array();
		$salesOrderProductDatas		= array();
		$salesCreditProductDatas	= array();
		if($salesOrderdatas){
			foreach($salesOrderdatas as $salesOrderdatasTemp){
				$bpconfig				= $this->ci->account1Config[$salesOrderdatasTemp['account1Id']];
				$taxDate				= '';
				$orderId				= $salesOrderdatasTemp['orderId'];
				
				$rowDatas				= json_decode($salesOrderdatasTemp['rowData'],true);
				$taxDate				= $rowDatas['invoices'][0]['taxDate'];
				$channelId				= $rowDatas['assignment']['current']['channelId'];
				$CustomFieldValueID		= '';
				if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
					$CustomFieldValueID	= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				}
				
				$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
				$accountingCurrencyCode	= strtolower($rowDatas['currency']['accountingCurrencyCode']);
				$ConsolAPIFieldValueID	= '';

				if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
					continue;
				}
				
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				if($ConsolAPIFieldValueID){
					$CustomFieldValueID	= $ConsolAPIFieldValueID;
				}
				
				if($enableAggregation){
					if($AggregationMappings){
						if((!$AggregationMappings[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($enableConsolidationMappingCustomazation){
								if($AggregationMappings[$channelId]['bpaccountingcurrency']){
									if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
								else{
									if(($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
					}
					elseif($AggregationMappings2){
						if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
								if((!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
							else{
								if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							if(!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
						else{
							if(!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
					}
				}
				$AggregationMappingData	= array();
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
						$CustomFieldValueID		= 'NA';
					}
					else{
						$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)];
						$CustomFieldValueID		= 'NA';
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
					}
					else{
						$AggregationMappingData	= $AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID];
					}
				}
				
				$consolFrequency		= $AggregationMappingData['consolFrequency'];
				$netOffConsolidation	= $AggregationMappingData['netOffConsolidation'];
				if(!$consolFrequency){
					$consolFrequency	= 1;
				}
				if(!$netOffConsolidation){
					continue;
				}
				
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$Acc2Offset		= 0;
				$diff			= $BPDateOffset - $Acc2Offset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
				
				//consolFrequency : 1 => Daily, 2 => Monthly
				if($consolFrequency == 1){
					$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
					if($taxDate > $fetchTaxDate){
						continue;
					}
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesOrderdatasTemp;
						}
						else{
							$salesOrderBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $salesOrderdatasTemp;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesOrderdatasTemp;
						}
						else{
							$salesOrderBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $salesOrderdatasTemp;
						}
					}
				}
				elseif($consolFrequency == 2){
					$invoiceMonth	= date('m',strtotime($taxDate));
					$currentMonth	= date('m');
					if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
						continue;
					}
					
					$batchKey	= date('y-m',strtotime($taxDate));
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesOrderdatasTemp;
						}
						else{
							$salesOrderBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $salesOrderdatasTemp;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesOrderBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesOrderdatasTemp;
						}
						else{
							$salesOrderBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $salesOrderdatasTemp;
						}
					}
				}
			}
		}
		if($salesCreditdatas){
			foreach($salesCreditdatas as $salesCreditdatasTemp){
				$bpconfig				= $this->ci->account1Config[$salesCreditdatasTemp['account1Id']];
				$taxDate				= '';
				$orderId				= $salesCreditdatasTemp['orderId'];
				$rowDatas				= json_decode($salesCreditdatasTemp['rowData'],true);
				$taxDate				= $rowDatas['invoices'][0]['taxDate'];
				$channelId				= $rowDatas['assignment']['current']['channelId'];
				$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
				$accountingCurrencyCode	= strtolower($rowDatas['currency']['accountingCurrencyCode']);
				$CustomFieldValueID		= '';
				if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
					$CustomFieldValueID	= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				}
				$ConsolAPIFieldValueID	= '';
			
				if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
					continue;
				}
			
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				if($ConsolAPIFieldValueID){
					$CustomFieldValueID	= $ConsolAPIFieldValueID;
				}
			
				if($enableAggregation){
					if($AggregationMappings){
						if((!$AggregationMappings[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($enableConsolidationMappingCustomazation){
								if($AggregationMappings[$channelId]['bpaccountingcurrency']){
									if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
								else{
									if(($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField'])){
										$ExcludedStringInCustomField		= array();
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
					}
					elseif($AggregationMappings2){
						if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency'])){
							continue;
						}
						else{
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
								if((!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
							else{
								if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA'])){
									continue;
								}
								else{
									if($enableConsolidationMappingCustomazation){
										if(($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
												if($IsNonConsolApplicable){
													continue;
												}
											}
										}
										elseif(($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField'])){
											$IsConsolApplicable					= 0;
											$IsNonConsolApplicable				= 0;
											$ExcludedStringInCustomField		= array();
											$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']);
											$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
											$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField']));
											$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
											if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
												foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
													$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
													if(is_array($AggregatedCustomFieldData)){
														if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
													else{
														if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
															$IsNonConsolApplicable	= 1;
															$IsConsolApplicable		= 0;
															break;
														}
														else{
															$IsConsolApplicable		= 1;
															$IsNonConsolApplicable	= 0;
														}
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
								}
							}
						}
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							if(!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
						else{
							if(!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]){
								$CustomFieldValueID	= 'NA';
							}
						}
					}
				}
			
				$AggregationMappingData	= array();
			
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
						$CustomFieldValueID		= 'NA';
					}
					else{
						$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)];
						$CustomFieldValueID		= 'NA';
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$AggregationMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
					}
					else{
						$AggregationMappingData	= $AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID];
					}
				}
			
				$consolFrequency		= $AggregationMappingData['consolFrequency'];
				$netOffConsolidation	= $AggregationMappingData['netOffConsolidation'];
				if(!$consolFrequency){
					$consolFrequency	= 1;
				}
				if(!$netOffConsolidation){
					continue;
				}
			
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$Acc2Offset		= 0;
				$diff			= $BPDateOffset - $Acc2Offset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
			
				//consolFrequency : 1 => Daily, 2 => Monthly
				if($consolFrequency == 1){
					$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
					if($taxDate > $fetchTaxDate){
						continue;
					}
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesCreditdatasTemp;
						}
						else{
							$salesCreditBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $salesCreditdatasTemp;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesCreditdatasTemp;
						}
						else{
							$salesCreditBatch[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $salesCreditdatasTemp;
						}
					}
				}
				elseif($consolFrequency == 2){
					$invoiceMonth	= date('m',strtotime($taxDate));
					$currentMonth	= date('m');
					if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
						continue;
					}
					
					$batchKey	= date('y-m',strtotime($taxDate));
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesCreditdatasTemp;
						}
						else{
							$salesCreditBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $salesCreditdatasTemp;
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							$salesCreditBatch[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $salesCreditdatasTemp;
						}
						else{
							$salesCreditBatch[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $salesCreditdatasTemp;
						}
					}
				}
			}
		}
		
		//salesCreditCode
		if($salesCreditBatch){
			foreach($salesCreditBatch as $taxDateBP => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $CurrencyCode => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $CustomFieldValueID => $OrdersByChannels){
							$invalidConsolOrderIds	= array();
							$rootCurrency			= $CurrencyCode;
							$BrightpealBaseCurrency	= '';
							$CustomFieldValueID		= $CustomFieldValueID;
							$AggregationMapping		= array();
							$ChannelCustomer		= '';
							$BPChannelName			= '';
							$consolFrequency		= 0;
								
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)];
							}
							elseif($AggregationMappings2){
								$AggregationMapping	= $AggregationMappings2[$SalesChannel][strtolower($rootCurrency)][$CustomFieldValueID];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
								
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							$consolFrequency		= $AggregationMapping['consolFrequency'];
							if(!$consolFrequency){
								$consolFrequency	= 1;
							}
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								$invalidConsolOrderIds	= array_keys($OrdersByChannels);
								$this->ci->db->where_in('orderId',$invalidConsolOrderIds)->update('sales_credit_order',array('message' => 'Invalid Consolidation Mapping'));
								continue;
							}
							
							$ChannelCustomer		= $AggregationMapping['account2ChannelId'];
							if(!$ChannelCustomer){continue;}
							
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$uniqueInvoiceRef		= $AggregationMapping['uniqueChannelName'];
							$BPTotalAmount			= 0;
							$BPTotalAmountBase		= 0;
							$OrderCount				= 0;
							$invoiceLineCount		= 0;
							$linNumber				= 1;
							$request				= array();
							$InvoiceLineAdd			= array();
							$AllorderId				= array();
							$ProductArray			= array();
							$TotalTaxLines			= array();
							$ProductArrayCount		= 0;
							$totalItemDiscount		= 0;
							$exchangeRate			= 1;
							$invoiceFormat			= '';
							
							if(!$AggregationMapping['SendSkuDetails']){
								foreach($OrdersByChannels as $orderDatas){
									$rowDatas			= json_decode($orderDatas['rowData'],true);
									$exchangeRate		= 1;
									$exchangeRate		= $rowDatas['currency']['exchangeRate'];
									$CountryIsoCode		= strtolower(trim($rowDatas['parties']['delivery']['countryIsoCode3']));
									$countryState		= strtolower(trim($rowDatas['parties']['delivery']['addressLine4']));
									$brightpearlConfig	= $this->ci->account1Config[$orderDatas['account1Id']];
									if($orderForceCurrency){
										if($config['defaultCurrency'] != $brightpearlConfig['currencyCode']){
											continue;
										}
									}
									
									if(!$rowDatas['invoices']['0']['invoiceReference']){
										continue;
									}
									$isDiscountCouponAdded	= 0;
									$couponItemLineID		= '';
									$couponNominalCodeBP	= '';
										
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										if($rowdatass['productId'] >= 1000){
											if(substr_count(strtolower($rowdatass['productName']),'coupon')){
												if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
													$isDiscountCouponAdded	= 1;
													$couponItemLineID		= $rowId;
													$couponNominalCodeBP	= $rowdatass['nominalCode'];
												}
											}
										}
									}
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										$rowNet					= $rowdatass['rowValue']['rowNet']['value'];
										$rowTax					= $rowdatass['rowValue']['rowTax']['value'];
										$productPrice			= $rowdatass['productPrice']['value'];
										$quantitymagnitude		= $rowdatass['quantity']['magnitude'];
										$bpdiscountPercentage	= $rowdatass['discountPercentage'];
										$bpItemNonimalCode		= $rowdatass['nominalCode'];
										$productId				= $rowdatass['productId'];
										$kidsTaxCustomField		= $brightpearlConfig['customFieldForKidsTax'];
										$isKidsTaxEnabled		= 0;
										if($kidsTaxCustomField){
											if($productMappings[$productId]){
												$productParams	= json_decode($productMappings[$productId]['params'], true);
												if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
													$isKidsTaxEnabled	= 1;
												}
											}
										}
										
										$taxMappingKey	= $bpTaxID;
										$taxMappingKey1	= $bpTaxID;
										if($isStateEnabled){
											if($isChannelEnabled){
												$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState.'-'.$SalesChannel;
												$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode.'-'.$SalesChannel;
											}
											else{
												$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState;
												$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode;
											}
										}
										$taxMapping		= array();
										$taxMappingKey	= strtolower($taxMappingKey);
										$taxMappingKey1	= strtolower($taxMappingKey1);
										if(isset($taxMappings[$taxMappingKey])){
											$taxMapping	= $taxMappings[$taxMappingKey];
										}
										elseif(isset($taxMappings[$taxMappingKey1])){
											$taxMapping	= $taxMappings[$taxMappingKey1];
										}
										elseif(isset($taxMappings[$bpTaxID.'--'.$SalesChannel])){
											$taxMapping	= $taxMappings[$bpTaxID.'--'.$SalesChannel];
										}
									
										if($taxMapping){
											$bpTaxID	= $taxMapping['account2TaxId'];
											if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsTaxId'])){
												$bpTaxID  = $taxMapping['account2KidsTaxId'];
											}
										}
										else{
											$bpTaxID	= $config['salesNoTaxCode'];
										}
										
										if($orderForceCurrency){
											$rowNet				= (($rowdatass['rowValue']['rowNet']['value']) * ((1) / ($exchangeRate)));
											$rowNet				= sprintf("%.4f",$rowNet);
											$rowTax				= (($rowdatass['rowValue']['rowTax']['value']) * ((1) / ($exchangeRate)));
											$rowTax				= sprintf("%.4f",$rowTax);
											$productPrice		= (($rowdatass['productPrice']['value']) * ((1) / ($exchangeRate)));
											$productPrice		= sprintf("%.4f",$productPrice);
										}
										
										$discountCouponAmt	= 0;
										$discountAmt		= 0;
										if($rowId == $couponItemLineID){
											if($rowNet == 0){
												continue;
											}
										}
										if(!$AggregationMapping['SendTaxAsLine']){
											$invoiceFormat	= 'InlineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage	= 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= ($productPrice * $quantitymagnitude);
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpItemNonimalCode][$bpTaxID])){
														$ProductArray['discount'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													$originalPrice		= ($productPrice * $quantitymagnitude);
													if(!$originalPrice){
														$originalPrice	= $rowNet;
													}
													if($originalPrice > $rowNet){
														$discountCouponAmt	= $originalPrice - $rowNet;
														$rowNet				= $originalPrice;
														if($discountCouponAmt > 0){
															if(isset($ProductArray['discountCoupon'][$couponNominalCodeBP][$bpTaxID])){
																$ProductArray['discountCoupon'][$couponNominalCodeBP][$bpTaxID]['TotalNetAmt']	+= $discountCouponAmt;
															}
															else{
																$ProductArray['discountCoupon'][$couponNominalCodeBP][$bpTaxID]['TotalNetAmt']	= $discountCouponAmt;
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if(isset($ProductArray[$bpItemNonimalCode][$bpTaxID])){
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']					+= $rowNet;
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']					+= $rowTax;
												}
												else{
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']					= $rowNet;
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']					= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		+= $rowNet;
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		= $rowNet;
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		+= $rowNet;
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		= $rowNet;
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $rowNet;
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $rowNet;
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
										}
										else{
											$invoiceFormat	= 'SaparatelineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage	= 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= ($productPrice * $quantitymagnitude);
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpItemNonimalCode])){
														$ProductArray['discount'][$bpItemNonimalCode]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpItemNonimalCode]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													$originalPrice		= ($productPrice * $quantitymagnitude);
													if(!$originalPrice){
														$originalPrice	= $rowNet;
													}
													if($originalPrice > $rowNet){
														$discountCouponAmt	= $originalPrice - $rowNet;
														$rowNet				= $originalPrice;
														if($discountCouponAmt > 0){
															if(isset($ProductArray['discountCoupon'][$couponNominalCodeBP])){
																$ProductArray['discountCoupon'][$couponNominalCodeBP]['TotalNetAmt']	+= $discountCouponAmt;
															}
															else{
																$ProductArray['discountCoupon'][$couponNominalCodeBP]['TotalNetAmt']	= $discountCouponAmt;
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if(isset($ProductArray['aggregationItem'][$bpItemNonimalCode])){
													$ProductArray['aggregationItem'][$bpItemNonimalCode]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['aggregationItem'][$bpItemNonimalCode]['TotalNetAmt']	= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpItemNonimalCode])){
													$ProductArray['shipping'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpItemNonimalCode])){
													$ProductArray['giftcard'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpItemNonimalCode])){
													$ProductArray['couponitem'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
										}
									}
									$BPTotalAmount		+= $rowDatas['totalValue']['total'];
									$BPTotalAmountBase	+= $rowDatas['totalValue']['baseTotal'];
									$orderId			= $orderDatas['orderId'];
									$AllorderId[]		= $orderId;
									$BPChannelName		= $orderDatas['channelName'];
									$OrderCount++;
								}
							}
							else{
								continue;
							}
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['exchangeRate']				= $exchangeRate;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPChannelName']			= $BPChannelName;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlConfig']		= $brightpearlConfig;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['config']					= $config;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['productData']				= $ProductArray;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmountBase']		= $BPTotalAmountBase;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmount']			= $BPTotalAmount;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['OrderCount']				= $OrderCount;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['consolMappingSettings']	= $AggregationMapping;
							$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['orderIds']['SC']			= $AllorderId;
						}
					}
				}
			}
		}
		
		//salesOrder Code
		if($salesOrderBatch){
			foreach($salesOrderBatch as $taxDateBP => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $CurrencyCode => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $CustomFieldValueID => $OrdersByChannels){
							$invalidConsolOrderIds	= array();
							$rootCurrency			= $CurrencyCode;
							$BrightpealBaseCurrency	= '';
							$CustomFieldValueID		= $CustomFieldValueID;
							$results				= array();
							$AggregationMapping		= array();
							$journalAggregation		= 0;
							$ChannelCustomer		= '';
							$BPChannelName			= '';
							$consolFrequency		= 0;
							$dueDate				= date('Y-m-d', strtotime($taxDateBP));
							
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)];
							}
							elseif($AggregationMappings2){
								$AggregationMapping	= $AggregationMappings2[$SalesChannel][strtolower($rootCurrency)][$CustomFieldValueID];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
							
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							$consolFrequency		= $AggregationMapping['consolFrequency'];
							if(!$consolFrequency){
								$consolFrequency	= 1;
							}
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								$invalidConsolOrderIds	= array_keys($OrdersByChannels);
								$this->ci->db->where_in('orderId',$invalidConsolOrderIds)->update('sales_order',array('message' => 'Invalid Consolidation Mapping'));
								continue;
							}
							
							$ChannelCustomer		= $AggregationMapping['account2ChannelId'];
							if(!$ChannelCustomer){continue;}
							
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$uniqueInvoiceRef		= $AggregationMapping['uniqueChannelName'];
							$BPTotalAmount			= 0;
							$BPTotalAmountBase		= 0;
							$OrderCount				= 0;
							$invoiceLineCount		= 0;
							$linNumber				= 1;
							$request				= array();
							$InvoiceLineAdd			= array();
							$AllorderId				= array();
							$ProductArray			= array();
							$TotalTaxLines			= array();
							$ProductArrayCount		= 0;
							$totalItemDiscount		= 0;
							$exchangeRate			= 1;
							$invoiceFormat			= '';
							
							if(!$AggregationMapping['SendSkuDetails']){
								foreach($OrdersByChannels as $orderDatas){
									$rowDatas			= json_decode($orderDatas['rowData'],true);
									$exchangeRate		= 1;
									$exchangeRate		= $rowDatas['currency']['exchangeRate'];
									$CountryIsoCode		= strtolower(trim($rowDatas['parties']['delivery']['countryIsoCode3']));
									$countryState		= strtolower(trim($rowDatas['parties']['delivery']['addressLine4']));
									$brightpearlConfig	= $this->ci->account1Config[$orderDatas['account1Id']];
									if($orderForceCurrency){
										if($config['defaultCurrency'] != $brightpearlConfig['currencyCode']){
											continue;
										}
									}
									
									if(!$rowDatas['invoices']['0']['invoiceReference']){
										continue;
									}
									
									//////////////////////////////////////////////////////////////
									$dueDate		= $rowDatas['invoices']['0']['dueDate'];
									//dueDate chanages
									$BPDateOffset	= (int)substr($dueDate,23,3);
									$xeroOffset		= 0;
									$diff			= $BPDateOffset - $xeroOffset;
									$date			= new DateTime($dueDate);
									$BPTimeZone		= 'GMT';
									$date->setTimezone(new DateTimeZone($BPTimeZone));
									if($diff > 0){
										$diff			.= ' hour';
										$date->modify($diff);
									}
									$dueDate		= $date->format('Y-m-d');
									//////////////////////////////////////////////////////////////
									
									
									$isDiscountCouponAdded	= 0;
									$couponItemLineID		= '';
									$couponNominalCodeBP	= '';
									
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										if($rowdatass['productId'] >= 1000){
											if(substr_count(strtolower($rowdatass['productName']),'coupon')){
												if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
													$isDiscountCouponAdded	= 1;
													$couponItemLineID		= $rowId;
													$couponNominalCodeBP	= $rowdatass['nominalCode'];
												}
											}
										}
									}
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										$rowNet					= $rowdatass['rowValue']['rowNet']['value'];
										$rowTax					= $rowdatass['rowValue']['rowTax']['value'];
										$productPrice			= $rowdatass['productPrice']['value'];
										$quantitymagnitude		= $rowdatass['quantity']['magnitude'];
										$bpdiscountPercentage	= $rowdatass['discountPercentage'];
										$bpItemNonimalCode		= $rowdatass['nominalCode'];
										
										$productId				= $rowdatass['productId'];
										$kidsTaxCustomField		= $brightpearlConfig['customFieldForKidsTax'];
										$isKidsTaxEnabled		= 0;
										if($kidsTaxCustomField){
											if($productMappings[$productId]){
												$productParams	= json_decode($productMappings[$productId]['params'], true);
												if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
													$isKidsTaxEnabled	= 1;
												}
											}
										}
										
										$taxMappingKey	= $bpTaxID;
										$taxMappingKey1	= $bpTaxID;
										if($isStateEnabled){
											if($isChannelEnabled){
												$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState.'-'.$SalesChannel;
												$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode.'-'.$SalesChannel;
											}
											else{
												$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState;
												$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode;
											}
										}
										$taxMapping		= array();
										$taxMappingKey	= strtolower($taxMappingKey);
										$taxMappingKey1	= strtolower($taxMappingKey1);
										if(isset($taxMappings[$taxMappingKey])){
											$taxMapping	= $taxMappings[$taxMappingKey];
										}
										elseif(isset($taxMappings[$taxMappingKey1])){
											$taxMapping	= $taxMappings[$taxMappingKey1];
										}
										elseif(isset($taxMappings[$bpTaxID.'--'.$SalesChannel])){
											$taxMapping	= $taxMappings[$bpTaxID.'--'.$SalesChannel];
										}
									
										if($taxMapping){
											$bpTaxID	= $taxMapping['account2TaxId'];
											if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsTaxId'])){
												$bpTaxID  = $taxMapping['account2KidsTaxId'];
											}
										}
										else{
											$bpTaxID	= $config['salesNoTaxCode'];
										}
										
										if($orderForceCurrency){
											$rowNet				= (($rowdatass['rowValue']['rowNet']['value']) * ((1) / ($exchangeRate)));
											$rowNet				= sprintf("%.4f",$rowNet);
											$rowTax				= (($rowdatass['rowValue']['rowTax']['value']) * ((1) / ($exchangeRate)));
											$rowTax				= sprintf("%.4f",$rowTax);
											$productPrice		= (($rowdatass['productPrice']['value']) * ((1) / ($exchangeRate)));
											$productPrice		= sprintf("%.4f",$productPrice);
										}
										
										$discountCouponAmt	= 0;
										$discountAmt		= 0;
										if($rowId == $couponItemLineID){
											if($rowNet == 0){
												continue;
											}
										}
										if(!$AggregationMapping['SendTaxAsLine']){
											$invoiceFormat	= 'InlineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage	= 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= ($productPrice * $quantitymagnitude);
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpItemNonimalCode][$bpTaxID])){
														$ProductArray['discount'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													$originalPrice		= ($productPrice * $quantitymagnitude);
													if(!$originalPrice){
														$originalPrice	= $rowNet;
													}
													if($originalPrice > $rowNet){
														$discountCouponAmt	= $originalPrice - $rowNet;
														$rowNet				= $originalPrice;
														if($discountCouponAmt > 0){
															if(isset($ProductArray['discountCoupon'][$couponNominalCodeBP][$bpTaxID])){
																$ProductArray['discountCoupon'][$couponNominalCodeBP][$bpTaxID]['TotalNetAmt']	+= $discountCouponAmt;
															}
															else{
																$ProductArray['discountCoupon'][$couponNominalCodeBP][$bpTaxID]['TotalNetAmt']	= $discountCouponAmt;
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if(isset($ProductArray[$bpItemNonimalCode][$bpTaxID])){
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']					+= $rowNet;
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']					+= $rowTax;
												}
												else{
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']					= $rowNet;
													$ProductArray[$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']					= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		+= $rowNet;
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		= $rowNet;
													$ProductArray['shipping'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		+= $rowNet;
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		= $rowNet;
													$ProductArray['giftcard'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $rowNet;
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $rowNet;
													$ProductArray['couponitem'][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
										}
										else{
											$invoiceFormat	= 'SaparatelineTax';
											if($bpdiscountPercentage > 0){
												$discountPercentage	= 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= ($productPrice * $quantitymagnitude);
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$bpItemNonimalCode])){
														$ProductArray['discount'][$bpItemNonimalCode]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$bpItemNonimalCode]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													$originalPrice		= ($productPrice * $quantitymagnitude);
													if(!$originalPrice){
														$originalPrice	= $rowNet;
													}
													if($originalPrice > $rowNet){
														$discountCouponAmt	= $originalPrice - $rowNet;
														$rowNet				= $originalPrice;
														if($discountCouponAmt > 0){
															if(isset($ProductArray['discountCoupon'][$couponNominalCodeBP])){
																$ProductArray['discountCoupon'][$couponNominalCodeBP]['TotalNetAmt']	+= $discountCouponAmt;
															}
															else{
																$ProductArray['discountCoupon'][$couponNominalCodeBP]['TotalNetAmt']	= $discountCouponAmt;
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if(isset($ProductArray['aggregationItem'][$bpItemNonimalCode])){
													$ProductArray['aggregationItem'][$bpItemNonimalCode]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['aggregationItem'][$bpItemNonimalCode]['TotalNetAmt']	= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$bpItemNonimalCode])){
													$ProductArray['shipping'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$bpItemNonimalCode])){
													$ProductArray['giftcard'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$bpItemNonimalCode])){
													$ProductArray['couponitem'][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$bpItemNonimalCode])){
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
										}
									}
									$BPTotalAmount		+= $rowDatas['totalValue']['total'];
									$BPTotalAmountBase	+= $rowDatas['totalValue']['baseTotal'];
									$orderId			= $orderDatas['orderId'];
									$AllorderId[]		= $orderId;
									$BPChannelName		= $orderDatas['channelName'];
									$OrderCount++;
								}
							}
							else{
								continue;
							}
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['dueDate']				= $dueDate;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['exchangeRate']			= $exchangeRate;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPChannelName']			= $BPChannelName;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlConfig']		= $brightpearlConfig;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['config']				= $config;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['productData']			= $ProductArray;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmountBase']		= $BPTotalAmountBase;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['BPTotalAmount']			= $BPTotalAmount;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['OrderCount']			= $OrderCount;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['consolMappingSettings']	= $AggregationMapping;
							$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][strtolower($CurrencyCode)][$CustomFieldValueID]['orderIds']['SO']		= $AllorderId;
						}
					}
				}
			}
		}
		
		if($salesOrderProductDatas AND $salesCreditProductDatas){
			foreach($salesOrderProductDatas as $invoiceFormat => $salesOrderProductDatasTemp1){
				foreach($salesOrderProductDatasTemp1 as $taxDateBP => $salesOrderProductDatasTemp2){
					foreach($salesOrderProductDatasTemp2 as $SalesChannel => $salesOrderProductDatasTemp3){
						foreach($salesOrderProductDatasTemp3 as $CurrencyCode => $salesOrderProductDatasTemp4){
							foreach($salesOrderProductDatasTemp4 as $CustomFieldValueID => $salesOrderDatasArray){
								if($salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]){
									$creditDataforNetOff	= $salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['productData'];
									$creditIds				= $salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['orderIds']['SC'];
									$salesDataforNetOff		= $salesOrderDatasArray['productData'];
									$salesIds				= $salesOrderDatasArray['orderIds']['SO'];
									if($invoiceFormat == 'InlineTax'){
										foreach($salesDataforNetOff as $ItmeType => $ProductArrayTemp){
											if(($ItmeType == 'shipping') OR ($ItmeType == 'giftcard') OR ($ItmeType == 'couponitem') OR ($ItmeType == 'discount') OR ($ItmeType == 'discountCoupon')){
												foreach($ProductArrayTemp as $ProductArrayNominal2 => $ProductArrayTemp2){
													foreach($ProductArrayTemp2 as $ProductArrayTaxCode2 => $ProductArrayTempDatas2){
														if($creditDataforNetOff[$ItmeType][$ProductArrayNominal2]){
															foreach($creditDataforNetOff[$ItmeType][$ProductArrayNominal2] as $dataforNetOffTaxCode2 => $dataforNetOffTemp2){
																if($dataforNetOffTaxCode2 == $ProductArrayTaxCode2){
																	if(($ProductArrayTempDatas2['TotalNetAmt'] >= $dataforNetOffTemp2['TotalNetAmt']) AND ($ProductArrayTempDatas2['bpTaxTotal'] >= $dataforNetOffTemp2['bpTaxTotal'])){
																		$salesDataforNetOff[$ItmeType][$ProductArrayNominal2][$ProductArrayTaxCode2]['TotalNetAmt']	= ($ProductArrayTempDatas2['TotalNetAmt'] - $dataforNetOffTemp2['TotalNetAmt']);
																		$salesDataforNetOff[$ItmeType][$ProductArrayNominal2][$ProductArrayTaxCode2]['bpTaxTotal']	= ($ProductArrayTempDatas2['bpTaxTotal'] - $dataforNetOffTemp2['bpTaxTotal']);
																		unset($creditDataforNetOff[$ItmeType][$ProductArrayNominal2][$dataforNetOffTaxCode2]);
																		break;
																	}
																}
															}
														}
													}
												}
											}
											else{
												$ProductArrayNominal	= $ItmeType;
												foreach($ProductArrayTemp as $ProductArrayTaxCode => $ProductArrayTempDatas){
													if($creditDataforNetOff[$ProductArrayNominal]){
														foreach($creditDataforNetOff[$ProductArrayNominal] as $dataforNetOffTaxCode => $dataforNetOffTemp){
															if($dataforNetOffTaxCode == $ProductArrayTaxCode){
																if(($ProductArrayTempDatas['TotalNetAmt'] >= $dataforNetOffTemp['TotalNetAmt']) AND ($ProductArrayTempDatas['bpTaxTotal'] >= $dataforNetOffTemp['bpTaxTotal'])){
																	$salesDataforNetOff[$ProductArrayNominal][$ProductArrayTaxCode]['TotalNetAmt']	= ($ProductArrayTempDatas['TotalNetAmt'] - $dataforNetOffTemp['TotalNetAmt']);
																	$salesDataforNetOff[$ProductArrayNominal][$ProductArrayTaxCode]['bpTaxTotal']	= ($ProductArrayTempDatas['bpTaxTotal'] - $dataforNetOffTemp['bpTaxTotal']);
																	unset($creditDataforNetOff[$ProductArrayNominal][$dataforNetOffTaxCode]);
																	break;
																}
															}
														}
													}
												}
											}
										}
										if($creditDataforNetOff){
											foreach($creditDataforNetOff as $dataforNetOffKey1 => $dataforNetOffTemp1){
												if(($dataforNetOffKey1 == 'shipping') OR ($dataforNetOffKey1 == 'giftcard') OR ($dataforNetOffKey1 == 'couponitem') OR ($dataforNetOffKey1 == 'discount') OR ($dataforNetOffKey1 == 'discountCoupon')){
													foreach($dataforNetOffTemp1 as $dataforNetOffKey2 => $dataforNetOffTemp2){
														if(empty($dataforNetOffTemp2)){
															unset($creditDataforNetOff[$dataforNetOffKey1][$dataforNetOffKey2]);
														}
													}
													if(empty($creditDataforNetOff[$dataforNetOffKey1])){
														unset($creditDataforNetOff[$dataforNetOffKey1]);
													}
												}
												else{
													if(empty($dataforNetOffTemp1)){
														unset($creditDataforNetOff[$dataforNetOffKey1]);
													}
												}
											}
										}
										if($salesDataforNetOff){
											foreach($salesDataforNetOff as $dataforNetOffKey1 => $dataforNetOffTemp1){
												if(($dataforNetOffKey1 == 'shipping') OR ($dataforNetOffKey1 == 'giftcard') OR ($dataforNetOffKey1 == 'couponitem') OR ($dataforNetOffKey1 == 'discount') OR ($dataforNetOffKey1 == 'discountCoupon')){
													foreach($dataforNetOffTemp1 as $dataforNetOffKey2 => $dataforNetOffTemp2){
														if(empty($dataforNetOffTemp2)){
															unset($salesDataforNetOff[$dataforNetOffKey1][$dataforNetOffKey2]);
														}
													}
													if(empty($salesDataforNetOff[$dataforNetOffKey1])){
														unset($salesDataforNetOff[$dataforNetOffKey1]);
													}
												}
												else{
													if(empty($dataforNetOffTemp1)){
														unset($salesDataforNetOff[$dataforNetOffKey1]);
													}
												}
											}
										}
										
										$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['productData']		= $salesDataforNetOff;
										$salesOrderProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['orderIds']['SC']	= $creditIds;
										$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['productData']		= $creditDataforNetOff;
										$salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]['orderIds']['SO']	= $salesIds;
									}
								}
							}
						}
					}
				}
			}
			
			foreach($salesOrderProductDatas as $invoiceFormat => $salesOrderProductDatasTemp1){
				foreach($salesOrderProductDatasTemp1 as $taxDateBP => $salesOrderProductDatasTemp2){
					foreach($salesOrderProductDatasTemp2 as $SalesChannel => $salesOrderProductDatasTemp3){
						foreach($salesOrderProductDatasTemp3 as $CurrencyCode => $salesOrderProductDatasTemp4){
							foreach($salesOrderProductDatasTemp4 as $CustomFieldValueID => $salesOrderDatasArray){
								$netOffTotalInBase		= 0;
								$AllorderId				= $salesOrderDatasArray['orderIds']['SO'];
								$netOffTotalInBase		= $salesOrderDatasArray['BPTotalAmountBase'];
								$netOffTotalInOrder		= 0;
								$netOffTotalInOrder		= $salesOrderDatasArray['BPTotalAmount'];
								$BPChannelName			= $salesOrderDatasArray['BPChannelName'];
								$exchangeRate			= $salesOrderDatasArray['exchangeRate'];
								$dueDate				= $salesOrderDatasArray['dueDate'];
								$journalAggregation		= 0;
								$invoiceLineCount		= 0;
								$linNumber				= 1;
								$request				= array();
								$results				= array();
								$InvoiceLineAdd			= array();
								$brightpearlConfig		= $salesOrderDatasArray['brightpearlConfig'];
								$config					= $salesOrderDatasArray['config'];
								$OrderCount				= $salesOrderDatasArray['OrderCount'];
								$ProductArray			= $salesOrderDatasArray['productData'];
								$consolMappingSettings	= $salesOrderDatasArray['consolMappingSettings'];
								$uniqueInvoiceRef		= $consolMappingSettings['uniqueChannelName'];
								$orderForceCurrency		= $consolMappingSettings['orderForceCurrency'];
								$consolFrequency		= $consolMappingSettings['consolFrequency'];
								$ChannelCustomer		= $consolMappingSettings['account2ChannelId'];
								if(!$ChannelCustomer){continue;}
								$OrderPostCurrencyCode	= strtoupper($CurrencyCode);
								if(!$consolFrequency){
									$consolFrequency	= 1;
								}
								if($orderForceCurrency){
									$OrderPostCurrencyCode	= strtoupper($brightpearlConfig['currencyCode']);
									$exchangeRate			= 1;
								}
								$trackingDetails1		= array();
								$trackingDetails2		= array();
								
								$postOtions				= $consolMappingSettings['postOtions'];
								$disableSalesPosting	= 0;
								$disableCOGSPosting		= 0;
								if($postOtions){
									if($postOtions == 1){
										$disableSalesPosting	= 1;
									}
									elseif($postOtions == 2){
										$disableCOGSPosting		= 1;
									}
								}
								
								if(isset($channelMappings[$SalesChannel])){
									$trackingDetails1	= $channelMappings[$SalesChannel]['account2ChannelId'];
									$trackingDetails1	= explode("~=",$trackingDetails1);
								}
								
								if($ChannelcustomfieldMappings){
									if(($ChannelcustomfieldMappingKey == 'account1CustomFieldId') OR ($ChannelcustomfieldMappingKey == 'account1APIFieldId')){
										if($ChannelcustomfieldMappings[$SalesChannel][$CustomFieldValueID]){
											$trackingDetails2	= $ChannelcustomfieldMappings[$SalesChannel][$CustomFieldValueID]['account2ChannelId'];
											$trackingDetails2	= explode("~=",$trackingDetails2);
										}
									}
								}
								
								$isInvoiceRequired			= 1;
								$isCreditNoteRequired		= 0;
								$totalInvoicePostAmt		= 0;
								$totalCreditNotePostAmt		= 0;
								if($ProductArray){
									if(!$consolMappingSettings['SendTaxAsLine']){
										foreach($ProductArray as $tempKey1 => $tempArray1){
											if(($tempKey1 == 'shipping') OR ($tempKey1 == 'giftcard') OR ($tempKey1 == 'couponitem') OR ($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
												foreach($tempArray1 as $tempKey2 => $tempArray2){
													foreach($tempArray2 as $tempKey3 => $tempArray3){
														if(($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
															$totalInvoicePostAmt	= ($totalInvoicePostAmt - $tempArray3['TotalNetAmt']);
															$totalInvoicePostAmt	= ($totalInvoicePostAmt - $tempArray3['bpTaxTotal']);
														}
														else{
															$totalInvoicePostAmt	= ($totalInvoicePostAmt + $tempArray3['TotalNetAmt']);
															$totalInvoicePostAmt	= ($totalInvoicePostAmt + $tempArray3['bpTaxTotal']);
														}
													}
												}
											}
											else{
												foreach($tempArray1 as $tempKey2 => $tempArray2){
													$totalInvoicePostAmt	= ($totalInvoicePostAmt + $tempArray2['TotalNetAmt']);
													$totalInvoicePostAmt	= ($totalInvoicePostAmt + $tempArray2['bpTaxTotal']);
												}
											}
										}
										
										foreach($ProductArray as $keyproduct => $ProductArrayDatas){
											if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
												foreach($ProductArrayDatas as $MapNominal => $ProductArrayDatasTemp){
													$IncomeAccountRef	= $config['IncomeAccountRef'];
													if(isset($nominalMappings[$MapNominal])){
														if($nominalMappings[$MapNominal]['account2NominalId']){
															$IncomeAccountRef	= $nominalMappings[$MapNominal]['account2NominalId'];
														}
													}
													if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]))){
														if($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId']){
															$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId'];
														}
													}
													foreach($ProductArrayDatasTemp as $keyproduct1 => $datas){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'ItemCode'		=> $consolMappingSettings['AggregationItem'],
															'Description'	=> "Consolidation of ".$OrderCount.' invoices',
															'Quantity'		=> 1,
															'UnitAmount'	=> sprintf("%.4f",$datas['TotalNetAmt']),
															'TaxType'		=> $keyproduct1,
															'TaxAmount' 	=> sprintf("%.4f",$datas['bpTaxTotal']),
															'AccountCode'	=> $IncomeAccountRef,
														);
														if($keyproduct == 'shipping'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
														}
														if($keyproduct == 'giftcard'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
														}
														if($keyproduct == 'couponitem'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
														}
														if($keyproduct == 'discount'){
															if(!$config['OverrideDiscountItemAccRef']){
																unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
															}
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
															$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
														}
														if($keyproduct == 'discountCoupon'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
															$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
														}
														// ADDED THE ORDER TRACKING INFO
														if($trackingDetails1 OR $trackingDetails2){
															if($trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															elseif($trackingDetails1 AND !$trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
															}
															elseif(!$trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															else{
																if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																	unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
																}
															}
														}
														$invoiceLineCount++;
													}
												}
											}
											else{
												$IncomeAccountRef	= $config['IncomeAccountRef'];
												if(isset($nominalMappings[$keyproduct])){
													if($nominalMappings[$keyproduct]['account2NominalId']){
														$IncomeAccountRef	= $nominalMappings[$keyproduct]['account2NominalId'];
													}
												}
												if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]))){
													if($nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]['account2NominalId']){
														$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]['account2NominalId'];
													}
												}
												foreach($ProductArrayDatas as $MapTaxId => $ProductArrayDatasTemp){
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'ItemCode'		=> $consolMappingSettings['AggregationItem'],
														'Description'	=> "Consolidation of ".$OrderCount.' invoices',
														'Quantity'		=> 1,
														'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
														'TaxType'		=> $MapTaxId,
														'TaxAmount'		=> sprintf("%.4f",$ProductArrayDatasTemp['bpTaxTotal']),
														'AccountCode'	=> $IncomeAccountRef,
													);
													
													if($trackingDetails1 OR $trackingDetails2){
														if($trackingDetails1 AND $trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																'Name'		=> $trackingDetails1['0'],
																'Option'	=> $trackingDetails1['1']
															);
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																'Name'		=> $trackingDetails2['0'],
																'Option'	=> $trackingDetails2['1']
															);
														}
														elseif($trackingDetails1 AND !$trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																'Name'		=> $trackingDetails1['0'],
																'Option'	=> $trackingDetails1['1']
															);
														}
														elseif(!$trackingDetails1 AND $trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																'Name'		=> $trackingDetails2['0'],
																'Option'	=> $trackingDetails2['1']
															);
														}
														else{
															if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
															}
														}
													}
													$invoiceLineCount++;
												}
											}
										}
									}
									else{
										foreach($ProductArray as $tempKey1 => $tempArray1){
											foreach($tempArray1 as $tempKey2 => $tempArray2){
												if(($tempKey1 == 'shipping') OR ($tempKey1 == 'giftcard') OR ($tempKey1 == 'couponitem') OR ($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
													if(($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
														$totalInvoicePostAmt	= ($totalInvoicePostAmt - $tempArray2['TotalNetAmt']);
													}
													else{
														$totalInvoicePostAmt	= ($totalInvoicePostAmt + $tempArray2['TotalNetAmt']);
													}
												}
												elseif($tempKey1 == 'allTax'){
													$totalInvoicePostAmt	= ($totalInvoicePostAmt + $tempArray2['TotalNetAmt']);
												}
											}
										}
										
										foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
											foreach($ProductArrayDatasTemp as $MapNominal => $ProductArrayDatas){
												if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
													$IncomeAccountRef	= $config['IncomeAccountRef'];
													if(isset($nominalMappings[$MapNominal])){
														if($nominalMappings[$MapNominal]['account2NominalId']){
															$IncomeAccountRef	= $nominalMappings[$MapNominal]['account2NominalId'];
														}
													}
													if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]))){
														if($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId']){
															$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId'];
														}
													}
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'ItemCode'		=> $consolMappingSettings['AggregationItem'],
														'Description'	=> "Consolidation of ".$OrderCount.' invoices',
														'Quantity'		=> 1,
														'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
														'TaxType'		=> $config['salesNoTaxCode'],
														'AccountCode'	=> $IncomeAccountRef,
													);
													if($keyproduct == 'shipping'){
														$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
													}
													if($keyproduct == 'giftcard'){
														$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
													}
													if($keyproduct == 'couponitem'){
														$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
													}
													if($keyproduct == 'discount'){
														if(!$config['OverrideDiscountItemAccRef']){
															unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
														}
														$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
														$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
													}
													if($keyproduct == 'discountCoupon'){
														$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
														$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
														$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
													}
													
													// ADDED THE ORDER TRACKING INFO
													if($trackingDetails1 OR $trackingDetails2){
														if($trackingDetails1 AND $trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																'Name'		=> $trackingDetails1['0'],
																'Option'	=> $trackingDetails1['1']
															);
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																'Name'		=> $trackingDetails2['0'],
																'Option'	=> $trackingDetails2['1']
															);
														}
														elseif($trackingDetails1 AND !$trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																'Name'		=> $trackingDetails1['0'],
																'Option'	=> $trackingDetails1['1']
															);
														}
														elseif(!$trackingDetails1 AND $trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																'Name'		=> $trackingDetails2['0'],
																'Option'	=> $trackingDetails2['1']
															);
														}
														else{
															if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
															}
														}
													}
													$invoiceLineCount++;
												}
												elseif($keyproduct == 'allTax'){
													if($ProductArrayDatas['TotalNetAmt'] <= 0){
														continue;
													}
													$mappedtaxItem		= '';
													$mappedtaxItem		= $consolMappingSettings['defaultTaxItem'];
													$InvoiceLineAdd[$invoiceLineCount]	= array(
														'ItemCode'		=> $mappedtaxItem,
														'Description'	=> $mappedtaxItem,
														'Quantity'		=> 1,
														'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
														'TaxType'		=> $config['salesNoTaxCode'],
														/* 'AccountCode'	=> $config['TaxItemLineNominal'], */
													);
													// ADDED THE ORDER TRACKING INFO
													if($trackingDetails1 OR $trackingDetails2){
														if($trackingDetails1 AND $trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																'Name'		=> $trackingDetails1['0'],
																'Option'	=> $trackingDetails1['1']
															);
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																'Name'		=> $trackingDetails2['0'],
																'Option'	=> $trackingDetails2['1']
															);
														}
														elseif($trackingDetails1 AND !$trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																'Name'		=> $trackingDetails1['0'],
																'Option'	=> $trackingDetails1['1']
															);
														}
														elseif(!$trackingDetails1 AND $trackingDetails2){
															$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																'Name'		=> $trackingDetails2['0'],
																'Option'	=> $trackingDetails2['1']
															);
														}
														else{
															if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
															}
														}
													}
													$invoiceLineCount++;
												}
											}
										}
									}
								}
								
								$allNetOffSalesCreditData	= array();
								$netOffSalesCreditIds		= array();
								$totalSOCount			 	= $OrderCount;
								$totalSCCount			 	= 0;
								if($salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID]){
									$allNetOffSalesCreditData	= $salesCreditProductDatas[$invoiceFormat][$taxDateBP][$SalesChannel][$CurrencyCode][$CustomFieldValueID];
									$allNetOffCreditItemsData	= $allNetOffSalesCreditData['productData'];
									$netOffSalesCreditIds		= $allNetOffSalesCreditData['orderIds']['SC'];
									$OrderCount					= ($OrderCount + $allNetOffSalesCreditData['OrderCount']);
									$totalSCCount				= ($allNetOffSalesCreditData['OrderCount']);
									$netOffTotalInBase			= ($netOffTotalInBase - $allNetOffSalesCreditData['BPTotalAmountBase']);
									$netOffTotalInOrder			= ($netOffTotalInOrder - $allNetOffSalesCreditData['BPTotalAmount']);
									if(!empty($allNetOffCreditItemsData)){
										if(!$consolMappingSettings['SendTaxAsLine']){
											foreach($allNetOffCreditItemsData as $tempKey1 => $tempArray1){
												if(($tempKey1 == 'shipping') OR ($tempKey1 == 'giftcard') OR ($tempKey1 == 'couponitem') OR ($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
													foreach($tempArray1 as $tempKey2 => $tempArray2){
														foreach($tempArray2 as $tempKey3 => $tempArray3){
															if(($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
																$totalCreditNotePostAmt	= ($totalCreditNotePostAmt - $tempArray3['TotalNetAmt']);
																$totalCreditNotePostAmt	= ($totalCreditNotePostAmt - $tempArray3['bpTaxTotal']);
															}
															else{
																$totalCreditNotePostAmt	= ($totalCreditNotePostAmt + $tempArray3['TotalNetAmt']);
																$totalCreditNotePostAmt	= ($totalCreditNotePostAmt + $tempArray3['bpTaxTotal']);
															}
														}
													}
												}
												else{
													foreach($tempArray1 as $tempKey2 => $tempArray2){
														$totalCreditNotePostAmt	= ($totalCreditNotePostAmt + $tempArray2['TotalNetAmt']);
														$totalCreditNotePostAmt	= ($totalCreditNotePostAmt + $tempArray2['bpTaxTotal']);
													}
												}
											}
											
											foreach($allNetOffCreditItemsData as $keyproduct => $allNetOffCreditItemsDataTemp){
												if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
													foreach($allNetOffCreditItemsDataTemp as $MapNominal => $allNetOffCreditItemsDataTempTemp){
														$IncomeAccountRef	= $config['IncomeAccountRef'];
														if(isset($nominalMappings[$MapNominal])){
															if($nominalMappings[$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalMappings[$MapNominal]['account2NominalId'];
															}
														}
														if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]))){
															if($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId'];
															}
														}
														foreach($allNetOffCreditItemsDataTempTemp as $keyproduct1 => $datasTemp){
															$InvoiceLineAdd[$invoiceLineCount]	= array(
																'ItemCode'		=> $consolMappingSettings['AggregationItem'],
																'Description'	=> "Consolidation of ".$OrderCount.' invoices',
																'Quantity'		=> 1,
																'UnitAmount'	=> (-1) * (sprintf("%.4f",$datasTemp['TotalNetAmt'])),
																'TaxType'		=> $keyproduct1,
																'TaxAmount' 	=> (-1) * (sprintf("%.4f",$datasTemp['bpTaxTotal'])),
																'AccountCode'	=> $IncomeAccountRef,
															);
															if($keyproduct == 'shipping'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
															}
															if($keyproduct == 'giftcard'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
															}
															if($keyproduct == 'couponitem'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
															}
															if($keyproduct == 'discount'){
																if(!$config['OverrideDiscountItemAccRef']){
																	unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
																}
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
																$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= (-1) * (sprintf("%.4f",((-1) * $datasTemp['TotalNetAmt'])));
															}
															if($keyproduct == 'discountCoupon'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
																$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= (-1) * (sprintf("%.4f",((-1) * $datasTemp['TotalNetAmt'])));
															}
															// ADDED THE ORDER TRACKING INFO
															if($trackingDetails1 OR $trackingDetails2){
																if($trackingDetails1 AND $trackingDetails2){
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																		'Name'		=> $trackingDetails1['0'],
																		'Option'	=> $trackingDetails1['1']
																	);
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																		'Name'		=> $trackingDetails2['0'],
																		'Option'	=> $trackingDetails2['1']
																	);
																}
																elseif($trackingDetails1 AND !$trackingDetails2){
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																		'Name'		=> $trackingDetails1['0'],
																		'Option'	=> $trackingDetails1['1']
																	);
																}
																elseif(!$trackingDetails1 AND $trackingDetails2){
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																		'Name'		=> $trackingDetails2['0'],
																		'Option'	=> $trackingDetails2['1']
																	);
																}
																else{
																	if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																		unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
																	}
																}
															}
															$invoiceLineCount++;
														}
													}
												}
												else{
													$IncomeAccountRef	= $config['IncomeAccountRef'];
													if(isset($nominalMappings[$keyproduct])){
														if($nominalMappings[$keyproduct]['account2NominalId']){
															$IncomeAccountRef	= $nominalMappings[$keyproduct]['account2NominalId'];
														}
													}
													if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]))){
														if($nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]['account2NominalId']){
															$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$keyproduct]['account2NominalId'];
														}
													}
													foreach($allNetOffCreditItemsDataTemp as $MapTaxId => $allNetOffCreditItemsDataTempTemp){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'ItemCode'		=> $consolMappingSettings['AggregationItem'],
															'Description'	=> "Consolidation of ".$OrderCount.' invoices',
															'Quantity'		=> 1,
															'UnitAmount'	=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['TotalNetAmt'])),
															'TaxType'		=> $MapTaxId,
															'TaxAmount'		=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['bpTaxTotal'])),
															'AccountCode'	=> $IncomeAccountRef,
														);
														// ADDED THE ORDER TRACKING INFO
														if($trackingDetails1 OR $trackingDetails2){
															if($trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															elseif($trackingDetails1 AND !$trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
															}
															elseif(!$trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															else{
																if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																	unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
																}
															}
														}
														$invoiceLineCount++;
													}
												}
											}
										}
										else{
											foreach($allNetOffCreditItemsData as $tempKey1 => $tempArray1){
												foreach($tempArray1 as $tempKey2 => $tempArray2){
													if(($tempKey1 == 'shipping') OR ($tempKey1 == 'giftcard') OR ($tempKey1 == 'couponitem') OR ($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
														if(($tempKey1 == 'discount') OR ($tempKey1 == 'discountCoupon')){
															$totalCreditNotePostAmt	= ($totalCreditNotePostAmt - $tempArray2['TotalNetAmt']);
														}
														else{
															$totalCreditNotePostAmt	= ($totalCreditNotePostAmt + $tempArray2['TotalNetAmt']);
														}
													}
													elseif($tempKey1 == 'allTax'){
														$totalCreditNotePostAmt	= ($totalCreditNotePostAmt + $tempArray2['TotalNetAmt']);
													}
												}
											}
											
											foreach($allNetOffCreditItemsData as $keyproduct => $allNetOffCreditItemsDataTemp){
												foreach($allNetOffCreditItemsDataTemp as $MapNominal => $allNetOffCreditItemsDataTempTemp){
													if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
														$IncomeAccountRef	= $config['IncomeAccountRef'];
														if(isset($nominalMappings[$MapNominal])){
															if($nominalMappings[$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalMappings[$MapNominal]['account2NominalId'];
															}
														}
														if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]))){
															if($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId'];
															}
														}
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'ItemCode'		=> $consolMappingSettings['AggregationItem'],
															'Description'	=> "Consolidation of ".$OrderCount.' Invoices',
															'Quantity'		=> 1,
															'UnitAmount'	=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['TotalNetAmt'])),
															'TaxType'		=> $config['salesNoTaxCode'],
															'AccountCode'	=> $IncomeAccountRef,
														);
														if($keyproduct == 'shipping'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
														}
														if($keyproduct == 'giftcard'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
														}
														if($keyproduct == 'couponitem'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
														}
														if($keyproduct == 'discount'){
															if(!$config['OverrideDiscountItemAccRef']){
																unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
															}
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
															$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= (-1) * (sprintf("%.4f",((-1) * $allNetOffCreditItemsDataTempTemp['TotalNetAmt'])));
														}
														if($keyproduct == 'discountCoupon'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
															$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= (-1) * (sprintf("%.4f",((-1) * $allNetOffCreditItemsDataTempTemp['TotalNetAmt'])));
														}
														
														// ADDED THE ORDER TRACKING INFO
														if($trackingDetails1 OR $trackingDetails2){
															if($trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															elseif($trackingDetails1 AND !$trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
															}
															elseif(!$trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															else{
																if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																	unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
																}
															}
														}
														$invoiceLineCount++;
													}
													elseif($keyproduct == 'allTax'){
														if($allNetOffCreditItemsDataTempTemp['TotalNetAmt'] <= 0){
															continue;
														}
														$mappedtaxItem		= '';
														$mappedtaxItem		= $consolMappingSettings['defaultTaxItem'];
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'ItemCode'		=> $mappedtaxItem,
															'Description'	=> $mappedtaxItem,
															'Quantity'		=> 1,
															'UnitAmount'	=> (-1) * (sprintf("%.4f",$allNetOffCreditItemsDataTempTemp['TotalNetAmt'])),
															'TaxType'		=> $config['salesNoTaxCode'],
															/* 'AccountCode'	=> $config['TaxItemLineNominal'], */
														);
														// ADDED THE ORDER TRACKING INFO
														if($trackingDetails1 OR $trackingDetails2){
															if($trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															elseif($trackingDetails1 AND !$trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
															}
															elseif(!$trackingDetails1 AND $trackingDetails2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails2['0'],
																	'Option'	=> $trackingDetails2['1']
																);
															}
															else{
																if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){
																	unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
																}
															}
														}
														$invoiceLineCount++;
													}
												}
											}
										}
									}
								}
								
								if($totalCreditNotePostAmt > $totalInvoicePostAmt){
									$isCreditNoteRequired		= 1;
								}
								
								if($consolMappingSettings['disableNominalPosting']){
									foreach($InvoiceLineAdd as $lineSeq => $InvoiceLineAddTemp){
										if(isset($InvoiceLineAddTemp['AccountCode'])){
											unset($InvoiceLineAdd[$lineSeq]['AccountCode']);
										}
									}
								}
								
								if($InvoiceLineAdd){
									if($consolFrequency == 1){
										$sendTaxDate		= $taxDateBP;
										$sendDueDate		= $taxDateBP;
										$RefForInvoice		= $taxDateBP;
										$ReferenceNumber	= $taxDateBP.'-'.$uniqueInvoiceRef.'-'.strtoupper($CurrencyCode);
									}
									else{
										$isLeap				= 0;
										$invMonth			= (int)substr($taxDateBP,3,2);
										$invYear			= (int)substr($taxDateBP,0,2);
										$invYearFull		= '20'.$invYear;
										
										if(((int)$invYearFull % 4) == 0){
											if(((int)$invYearFull % 100) == 0){
												if(((int)$invYearFull % 400) == 0){
													$isLeap	= 1;
												}
												else{
													$isLeap	= 0;
												}
											}
											else{
												$isLeap	= 1;
											}
										}
										else{
											$isLeap	= 0;
										}
										
										$invDate	= $dateResults[$invMonth]['lastDate'];
										if($invMonth == 2){
											if($isLeap){
												$invDate	= $dateResults[$invMonth]['lastDate2'];										
											}
										}
										$invMonthName	= $dateResults[$invMonth]['name'];
										if(strlen($invMonth) == 1){
											$invMonth	= (string)('0'.$invMonth);
										}
										$sendTaxDate		= $invYearFull.'-'.$invMonth.'-'.$invDate;
										$sendDueDate		= $sendTaxDate;
										$RefForInvoice		= $invMonthName.$invYear;
										$ReferenceNumber	= $RefForInvoice.'-'.$uniqueInvoiceRef.'-'.strtoupper($CurrencyCode);
									}
								
									$aggreagationID			= uniqid((strtoupper(trim($BPChannelName))).'-');
									$ReferenceNumber		= substr($ReferenceNumber,0,255);
									
									if($ReferenceNumber){
										if($isCreditNoteRequired){
											$CheckReferenceNumber	= $this->ci->db->get_where('sales_credit_order',array('invoiceRef' => $ReferenceNumber, 'account2Id' => $account2Id))->row_array();
											if($CheckReferenceNumber){
												$this->ci->db->where_in('orderId',$AllorderId)->update('sales_credit_order',array('message' => 'Credit Number already exists in Xero'));
												if($netOffSalesCreditIds){
													$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_order',array('message' => 'Invoice Number already exists in Xero'));
												}
												continue;
											}

										}
										else{
											$CheckReferenceNumber	= $this->ci->db->get_where('sales_order',array('invoiceRef' => $ReferenceNumber, 'account2Id' => $account2Id))->row_array();
											if($CheckReferenceNumber){
												$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('message' => 'Invoice Number already exists in Xero'));
												if($netOffSalesCreditIds){
													$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('message' => 'Credit Number already exists in Xero'));
												}
												continue;
											}
										}
									}
									
									if($isCreditNoteRequired){
										foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
											$InvoiceLineAdd[$LineSeq]['UnitAmount']		= ((-1) * ($InvoiceLineAdd[$LineSeq]['UnitAmount']));
											$InvoiceLineAdd[$LineSeq]['TaxAmount']		= ((-1) * ($InvoiceLineAdd[$LineSeq]['TaxAmount']));
										}
									}
									
									foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
										if($InvoiceLineAddTemp['UnitAmount'] > 0){
											if($InvoiceLineAddTemp['UnitAmount'] < '0.0050'){
												$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
											}
										}
										else{
											if($InvoiceLineAddTemp['UnitAmount'] > '-0.0050'){
												$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
											}
										}
										$InvoiceLineAdd[$LineSeq]['Description']	= "Consolidation of ".$totalSOCount." sales orders";
										if($totalSCCount){
											$InvoiceLineAdd[$LineSeq]['Description']	= "Consolidation of ".$totalSOCount." sales orders & ".$totalSCCount." sales credits";
										}
									}
									
									$request			= array(
										'Type'				=> 'ACCREC',
										'Contact' 			=> array('ContactID' => $ChannelCustomer),
										'InvoiceNumber'		=> $ReferenceNumber,
										'Reference'			=> $ReferenceNumber,
										'Status'			=> 'AUTHORISED',
										'CurrencyCode'		=> $OrderPostCurrencyCode,
										'CurrencyRate'		=> $exchangeRate,
										'Date'				=> date('Y-m-d',strtotime($sendTaxDate)),
										'DueDate'			=> date('Y-m-d',strtotime($sendTaxDate)),
										'LineItems' 		=> $InvoiceLineAdd,
									);
									if($dueDate){
										$request['DueDate']	= $dueDate;
									}
									
									if($config['defaultCurrency'] != $brightpearlConfig['currencyCode']){
										unset($request['CurrencyRate']);
									}
									
									if($isCreditNoteRequired){
										$request['Type']				= 'ACCRECCREDIT';
										$request['CreditNoteNumber']	= $ReferenceNumber;
										unset($request['InvoiceNumber']);
									}
									if($request AND $InvoiceLineAdd){
										if($disableSalesPosting){
											$createdRowData	= array(
												'Request data	: '	=> $request,
												'Response data	: '	=> 'POSTING_IS_DISABLED',
											);
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											if($netOffSalesCreditIds){
												$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											}
										}
										else{
											$this->headers	= array();
											if($isCreditNoteRequired){
												$url			= '2.0/CreditNotes';
												if($accountDetails['OAuthVersion'] == '2'){
													$url		= '2.0/CreditNotes?unitdp=4';
													$this->initializeConfig($account2Id, 'PUT', $url);
												}
												else{
													$url		= '2.0/CreditNotes';
													$UrlParams	= array('unitdp' => '4');
													$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
													$url		= '2.0/CreditNotes?unitdp='.urlencode('4');
												}
											}
											else{
												$url			= '2.0/Invoices';
												if($accountDetails['OAuthVersion'] == '2'){
													$url		= '2.0/Invoices?unitdp=4';
													$this->initializeConfig($account2Id, 'PUT', $url);
												}
												else{
													$url		= '2.0/Invoices';
													$UrlParams	= array('unitdp' => '4');
													$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
													$url		= '2.0/Invoices?unitdp='.urlencode('4');
												}
											}
											$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
											$createdRowData	= array(
												'Request data	: '	=> $request,
												'Response data	: '	=> $results,
											);
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											if($netOffSalesCreditIds){
												$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
											}
										}
									}
									else{
										continue;
									}
									if((strtolower($results['Status']) == 'ok') AND ((isset($results['Invoices']['Invoice']['InvoiceID'])) OR(isset($results['CreditNotes']['CreditNote']['CreditNoteID'])))){
										$consolPostOptions	= 0;
										if($disableCOGSPosting){
											$consolPostOptions	= 2;
										}
										if($isCreditNoteRequired){
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('isNetOff' => 1,  'linkedWithCredit' => $results['CreditNotes']['CreditNote']['CreditNoteID'], 'uninvoiced' => 0, 'status' => '1', 'message' => 'Linked With Credit Note', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['CreditNotes']['CreditNote']['CreditNoteID'], 'sendInAggregation' => 1, 'PostedTime' => date('c') , 'IsJournalposted' => $journalAggregation, 'consolPostOptions'=> $consolPostOptions));
											if($netOffSalesCreditIds){
												$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('isNetOff' => 1, 'uninvoiced' => 0, 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['CreditNotes']['CreditNote']['CreditNoteID'], 'sendInAggregation' => 1, 'PostedTime' => date('c') , 'consolPostOptions'=> $consolPostOptions));
											}

										}
										else{
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('isNetOff' => 1, 'uninvoiced' => 0, 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID'], 'sendInAggregation' => 1, 'PostedTime' => date('c') , 'IsJournalposted' => $journalAggregation, 'consolPostOptions'=> $consolPostOptions));
											if($netOffSalesCreditIds){
												$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('isNetOff' => 1, 'linkedWithInvoice' => $results['Invoices']['Invoice']['InvoiceID'], 'uninvoiced' => 0, 'status' => '1', 'message' => 'Linked With Invoive', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID'], 'sendInAggregation' => 1, 'PostedTime' => date('c') , 'consolPostOptions'=> $consolPostOptions));
											}
										}
										$XeroTotalAmount	= $results['Invoices']['Invoice']['Total'];
										if($orderForceCurrency){
											$NetRoundOff	= $netOffTotalInBase - $XeroTotalAmount;
										}
										else{
											$NetRoundOff	= $netOffTotalInOrder - $XeroTotalAmount;
										}
										$RoundOffCheck		= abs($NetRoundOff);
										$RoundOffCheck		= sprintf("%.4f",$RoundOffCheck);
										$RoundOffApplicable	= 0;
										if($RoundOffCheck != 0){
											if($RoundOffCheck < 0.99){
												$RoundOffApplicable = 1;
											}
											if($RoundOffApplicable){
												$InvoiceLineAdd[$invoiceLineCount] = array(
													'ItemCode'			=> $config['roundOffItem'],
													'Description'		=> $config['roundOffItem'],
													'Quantity'			=> 1,
													'UnitAmount'		=> sprintf("%.4f",$NetRoundOff),
													'TaxType'			=> $config['salesNoTaxCode'],
												);	
												$request['LineItems']	= $InvoiceLineAdd;
												$request['InvoiceID']	= $results['Invoices']['Invoice']['InvoiceID'];
												
												$this->headers	= array();
												if($isCreditNoteRequired){
													$url			= '2.0/CreditNotes';
													if($accountDetails['OAuthVersion'] == '2'){
														$url		= '2.0/CreditNotes?unitdp=4';
														$this->initializeConfig($account2Id, 'POST', $url);
													}
													else{
														$url		= '2.0/CreditNotes';
														$UrlParams	= array('unitdp' => '4');
														$this->initializeConfig($account2Id, 'POST', $url,$UrlParams);
														$url		= '2.0/CreditNotes?unitdp='.urlencode('4');
													}

												}
												else{
													$url			= '2.0/Invoices';
													if($accountDetails['OAuthVersion'] == '2'){
														$url		= '2.0/Invoices?unitdp=4';
														$this->initializeConfig($account2Id, 'POST', $url);
													}
													else{
														$url		= '2.0/Invoices';
														$UrlParams	= array('unitdp' => '4');
														$this->initializeConfig($account2Id, 'POST', $url,$UrlParams);
														$url		= '2.0/Invoices?unitdp='.urlencode('4');
													}
												}
												$results2	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
												$createdRowData['Rounding Request data	: ']	= $request;
												$createdRowData['Rounding Response data	: ']	= $results2;
												if((strtolower($results2['Status']) == 'ok') AND ((isset($results2['Invoices']['Invoice']['InvoiceID'])) OR(isset($results2['CreditNotes']['CreditNote']['CreditNoteID'])))){
													$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('status' => '1', 'createdRowData' => json_encode($createdRowData)));
													if($netOffSalesCreditIds){
														$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('status' => '1', 'createdRowData' => json_encode($createdRowData)));
													
													}
												}
												else{
													$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('PostedWrong' => 1, 'LastResponseCheck' => strtotime("now"), 'message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)));
													if($netOffSalesCreditIds){
														$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)));
													}
												}
											}
										}
									}
									if($disableSalesPosting){
										$fakecreateOrderId	= uniqid("XERO_ID_").strtotime("now");
										$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('isNetOff' => 1, 'linkedWithCredit' => $results['CreditNotes']['CreditNote']['CreditNoteID'], 'uninvoiced' => 0, 'status' => '4', 'message' => 'COGS only', 'invoiced' => '0', 'invoiceRef' => 	$ReferenceNumber,'createOrderId' => $fakecreateOrderId, 'sendInAggregation' => 1, 'PostedTime' => date('c') , 'IsJournalposted' => $journalAggregation, 'consolPostOptions' => 1));
										if($netOffSalesCreditIds){
											$this->ci->db->where_in('orderId',$netOffSalesCreditIds)->update('sales_credit_order',array('isNetOff' => 1, 'linkedWithInvoice' => $fakecreateOrderId, 'uninvoiced' => 0, 'status' => '4', 'message' => 'COGS only', 'invoiced' => '0', 'invoiceRef' => 	$ReferenceNumber,'createOrderId' => $fakecreateOrderId, 'sendInAggregation' => 1, 'PostedTime' => date('c'), 'consolPostOptions' => 1));
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}