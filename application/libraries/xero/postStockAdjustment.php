<?php
$this->reInitialize();
$clientcode						= $this->ci->config->item('clientcode');
$enableCOGSJournals				= $this->ci->globalConfig['enableCOGSJournals'];
$enableStockAdjustment			= $this->ci->globalConfig['enableStockAdjustment'];
$enableConsolStockAdjustment	= $this->ci->globalConfig['enableConsolStockAdjustment'];
$enableSAConsol					= $this->ci->globalConfig['enableSAConsol'];
$dateLockSettings				= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($enableConsolStockAdjustment){continue;}
	if(!$enableStockAdjustment){continue;}
	if(!$clientcode){continue;}
	
	//doNotRemoveThisLine
	$skuTrimmingClientCodes		= array('keenxero','bfxero','arten','logicofenglishxero','tinyexplorerxero','homeleisuredirectxero');
	
	$datasTemps	= array();
	$config		= $this->accountConfig[$account2Id];
	if($enableSAConsol){
		$fetchType = 'GO';
	}
	if($fetchType){
		$this->ci->db->reset_query();
		if($fetchType == 'SC'){
			$datasTemps	= $this->ci->db->where_in('status',array('0'))->order_by('orderId')->get_where('stock_adjustment',array('GoodNotetype' => 'SC', 'account2Id' => $account2Id))->result_array();
		}
		elseif($fetchType == 'GO'){
			$datasTemps	= $this->ci->db->where_in('status',array('0'))->order_by('orderId')->get_where('stock_adjustment',array('GoodNotetype' => 'GO', 'account2Id' => $account2Id))->result_array();
		}
	}
	else{
		$this->ci->db->reset_query();
		if($orgObjectId){
			$this->ci->db->where_in('orderId',$orgObjectId);
		}
		$datasTemps	= $this->ci->db->where_in('status',array('0'))->order_by('orderId')->get_where('stock_adjustment',array('account2Id' => $account2Id))->result_array();
		if(!$datasTemps){
			$this->ci->db->reset_query();
			$this->ci->db->where_in('ActivityId',$orgObjectId);
			$datasTemps	= $this->ci->db->where_in('status',array('0'))->get_where('stock_adjustment',array('account2Id' => $account2Id))->result_array(); 
		}
	}
	if(!$datasTemps){
		continue;
	}
	$productIds	= array_column($datasTemps,'productId');
	if($productIds){
		$this->postProducts($productIds,$account2Id);
	}
	$this->ci->db->reset_query();
	$productMappings		= array();
	$productMappingsTemps	= $this->ci->db->get_where('products',array('account2Id' => $account2Id, 'createdProductId <>' => ''))->result_array();
	if($productMappingsTemps){
		foreach($productMappingsTemps as $productMappingsTemp){
			$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$datass		= array();
	foreach($datasTemps as $datasTemp){
		if($datasTemp['GoodNotetype'] == 'GO'){
			$datass[$datasTemp['ActivityId']][]	= $datasTemp;
		}
		if($datasTemp['GoodNotetype'] == 'SC'){
			$datass[$datasTemp['orderId']][]	= $datasTemp;
		}
	}
	
	foreach($datass as $datas){
		$config1				= array();
		$request				= array();
		$positiveStockRequest	= array();
		$negativeStockRequest	= array();
		$positiveOrderIds		= array();
		$negativeOrderIds		= array(); 
		foreach($datas as $data){
			$goodsMovementId	= $data['orderId'];
			$ActivityId			= $data['ActivityId'];
			$shippedDate		= $data['shippedDate'];
			$ProductParams		= json_decode($productMappings[$data['productId']]['params'],true);
			
			$nominalCodeStock		= $ProductParams['nominalCodeStock'];
			$nominalCodePurchases	= $ProductParams['nominalCodePurchases'];
			$nominalCodeSales		= $ProductParams['nominalCodeSales'];
			
			
			$createdRowData		= json_decode($data['createdRowData'],true);
			if(@$createdRowData['lastTry']){
				if($createdRowData['lastTry'] > strtotime('-3 hour')){
					continue;
				}
			}
			$config1		= $this->ci->account1Config[$data['account1Id']];
			$stockAdjustmentAccountRef	= $config['stockAdjustmentAccountRef'];
			if($data['accountCode']){
				$stockAdjustmentAccountRef	= $data['accountCode']; 
			}
			$LineItem	= array(
				'ItemCode'		=> $productMappings[$data['productId']]['sku'],
				'Description'	=> $productMappings[$data['productId']]['name'],
				'Quantity'		=> (int)$data['qty'],
				'TaxType'		=> $config['salesNoTaxCode'],
				'UnitAmount'	=> $data['price'],
				'LineAmount'	=> sprintf("%.4f",($data['price'] * $data['qty'])),
			);
			
			/* if(in_array($clientcode, $skuTrimmingClientCodes)){ */
			if($config['InventoryManagementEnabled'] == true){
				$LineItem['ItemCode']	= substr($LineItem['ItemCode'],0,30);
			}
			
			if($LineItem['Quantity'] < 0){
				$LineItem['LineAmount']	= sprintf("%.4f",($LineItem['LineAmount'] * (-1)));
			}
			
			$xeroItemInfos	= array();
			$totalPrice		= 0;
			
			if($LineItem['Quantity'] < 0){
				$xeroProductId	= $productMappings[$data['productId']]['createdProductId'];
				//if 'InventoryManagementEnabled' is false, means connector is Inventory managed (products are posted as tracked)
				if($config['InventoryManagementEnabled'] == false){
					if($xeroProductId){
						$this->headers	= array();
						$url			= '2.0/Items/'.$xeroProductId;
						$this->initializeConfig($account2Id, 'GET', $url);
						$results		= $this->getCurl($url, 'GET', '', 'json', $account2Id)[$account2Id]['Items']['Item'];
						if($results){
							$bpAdjustedQty	= abs($LineItem['Quantity']);
							$remainingQty	= $bpAdjustedQty - $results['QuantityOnHand'];
							if($remainingQty == 0){
								if($results['TotalCostPool'] > 0){
									$LineItem['LineAmount']	= $results['TotalCostPool'];
									$LineItem['UnitAmount']	= abs(sprintf("%.4f",($LineItem['LineAmount'] / $data['qty'])));
								}
								else{
									$LineItem['LineAmount']	= 0;
									$LineItem['UnitAmount']	= 0;
								}
							}
						}
					}
				}
				$LineItem['Quantity']	= (-1) * $LineItem['Quantity'];
			}
			
			if($data['GoodNotetype'] ==  'GO'){
				if($data['qty'] > 0){
					$LineItem['TaxType']				= $config['PurchaseNoTaxCode'];
					$LineItem['AccountCode']			= $config['InventoryTransferPurchaseAccountRef'];
					$positiveStockRequest[]				= $LineItem;
					$positiveOrderIds[$data['orderId']]	= $data['orderId'];
					
				}
				else{
					$LineItem['TaxType']				= $config['salesNoTaxCode'];
					$LineItem['AccountCode']			= $config['InventoryTransferSalesAccountRef'];
					$negativeStockRequest[]				= $LineItem;
					$negativeOrderIds[$data['orderId']]	= $data['orderId'];
					if($data['price'] > 0){ 
						$LineItem['Quantity']	= (-1) * $LineItem['Quantity'];
					}
				}
			}
			if($data['GoodNotetype'] ==  'SC'){
				if($data['qty'] > 0){
					$LineItem['TaxType']				= $config['PurchaseNoTaxCode'];
					$positiveStockRequest[]				= $LineItem;
					$positiveOrderIds[$data['orderId']]	= $data['orderId'];
					if($data['price'] > 0){
						$stockLine	= array(
							'Description'	=> 'Inventory Adjustment',
							'Quantity'		=> 1,
							'LineAmount'	=> sprintf("%.4f",((-1) * ($LineItem['UnitAmount'] * $data['qty']))),
							'TaxType'		=> $config['PurchaseNoTaxCode'],
							'AccountCode'	=> $stockAdjustmentAccountRef,
						);
						$positiveStockRequest[]	= $stockLine;
					}
				}
				else{
					$LineItem['TaxType']				= $config['PurchaseNoTaxCode'];
					$negativeStockRequest[]				= $LineItem;
					$negativeOrderIds[$data['orderId']] = $data['orderId'];
					if($data['price'] > 0){ 
						$LineItem['Quantity']	= (-1) * $LineItem['Quantity'];
						$stockLine	= array(
							'Description'	=> 'Inventory Adjustment',
							'Quantity'		=> 1,
							'LineAmount'	=> sprintf("%.4f",(($LineItem['UnitAmount'] * $data['qty']))),
							'TaxType'		=> $config['PurchaseNoTaxCode'],
							'AccountCode'	=> $stockAdjustmentAccountRef,
						);
						$negativeStockRequest[]	= $stockLine;
					}
				}
			}
		}
		if($positiveStockRequest){
			$customer		= array('Name'	=> 'Inventory Adjustments');
			$InvoiceNumber	= $goodsMovementId;
			$invoiceDate	= $data['created'];
			if($data['GoodNotetype'] ==  'GO'){
				$customer		= array('ContactID'	=> $config['InterCoSupplier']);
	 			$InvoiceNumber	= $ActivityId;
				$invoiceDate	= $shippedDate;
			}
			
			if(($invoiceDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['stocks'])) AND (strlen($dateLockSettings['stocks']) > 0)){
				$checkTaxDate	= date('Ymd',strtotime($invoiceDate));
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['stocks'])));
				
				if($dateLockSettings['stocksCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			$this->headers	= array();
			$url			= '2.0/Invoices?unitdp=4';
			$request		= array(
				'Type'			=> 'ACCPAY',
				'Contact'		=> $customer,
				'Date'			=> date('Y-m-d',strtotime($invoiceDate)),
				'DueDate'		=> date('Y-m-d',strtotime($invoiceDate)),
				'CurrencyCode'	=> $config1['currencyCode'],
				'Status'		=> 'AUTHORISED',
				'LineItems'		=> $positiveStockRequest,
				'InvoiceNumber'	=> $InvoiceNumber,
			);	
			$this->initializeConfig($account2Id, 'PUT', $url);
			$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
			
			if(isset($results['Elements']['DataContractBase']['ValidationErrors']['ValidationError']['Message'])){
				$xeroErrorMessage	= $results['Elements']['DataContractBase']['ValidationErrors']['ValidationError']['Message'];
				if(substr_count($xeroErrorMessage,"must be unique")){
					if(isset($request['InvoiceNumber'])){
						$request['InvoiceNumber']		= $request['InvoiceNumber'].'_SA';
					}
					elseif(isset($request['CreditNoteNumber'])){
						$request['CreditNoteNumber']	= $request['CreditNoteNumber'].'_SA';
					}
					$this->headers	= array();
					$this->initializeConfig($account2Id, 'PUT', $url);
					$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
				}
			}
			
			$createdRowData	= array(
				'Request data	: ' => $request,
				'Response data	: ' => $results,
			);
			$this->ci->db->where_in('orderId',array_values($positiveOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
			if((strtolower($results['Status']) == 'ok') AND (isset($results['Invoices']['Invoice']['InvoiceID']))){	
				$this->ci->db->where_in('orderId',array_values($positiveOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdOrderId' => $results['Invoices']['Invoice']['InvoiceID'], 'status' => '1'));
			}
			else{
				$badRequest	= 0;
				$errormsg	= array();
				$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
				$errormsg	= json_decode($errormsg,true);
				if(isset($errormsg['head']['title'])){
					if($errormsg['head']['title'] == '400 Bad Request'){
						$badRequest	= 1;
					}
				}
				if(!$badRequest){
					$createdRowData['lastTry']	= strtotime('now');
					$this->ci->db->where_in('orderId',array_values($positiveOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
				}
			}
		} 
		if($negativeStockRequest){
			$url	= '2.0/CreditNotes';
			$url	= '2.0/CreditNotes?unitdp=4';
			$CreditNoteNumber	= $goodsMovementId;
			$InvoiceNumber		= $ActivityId;
			
			$request			= array(
				'Type'				=> 'ACCPAYCREDIT',
				'Contact'			=> array('Name' => 'Inventory Adjustments',),
				'Date'				=> date('Y-m-d',strtotime($data['created'])),
				'DueDate'			=> date('Y-m-d',strtotime($data['created'])),
				'CurrencyCode'		=> $config1['currencyCode'],
				'Status'			=> 'AUTHORISED',
				'LineItems'			=> $negativeStockRequest,
				'CreditNoteNumber'	=> $CreditNoteNumber
			);
			if($data['GoodNotetype'] ==  'GO'){
				$this->headers	= array();
				$url			= '2.0/Invoices?unitdp=4';
				$request		= array(
					'Type'			=> 'ACCREC',
					'Contact'		=> array('ContactID' => $config['InterCoCustomer'],),
					'Date'			=> date('Y-m-d',strtotime($shippedDate)),
					'DueDate'		=> date('Y-m-d',strtotime($shippedDate)),
					'CurrencyCode'	=> $config1['currencyCode'],
					'Status'		=> 'AUTHORISED',
					'LineItems'		=> $negativeStockRequest,
					'InvoiceNumber'	=> $InvoiceNumber,
				);
				if(!$request['InvoiceNumber']){
					$request['InvoiceNumber']	= $goodsMovementId;
				}
	 		}
			
			if(($request['Date']) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['stocks'])) AND (strlen($dateLockSettings['stocks']) > 0)){
				$checkTaxDate	= date('Ymd',strtotime($request['Date']));
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['stocks'])));
				
				if($dateLockSettings['stocksCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			$this->initializeConfig($account2Id, 'PUT', $url);
			$results	= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
			
			
			if(isset($results['Elements']['DataContractBase']['ValidationErrors']['ValidationError']['Message'])){
				$xeroErrorMessage	= $results['Elements']['DataContractBase']['ValidationErrors']['ValidationError']['Message'];
				if(substr_count($xeroErrorMessage,"must be unique")){
					if(isset($request['InvoiceNumber'])){
						$request['InvoiceNumber']		= $request['InvoiceNumber'].'_SA';
					}
					elseif(isset($request['CreditNoteNumber'])){
						$request['CreditNoteNumber']	= $request['CreditNoteNumber'].'_SA';
					}
					$this->headers	= array();
					$this->initializeConfig($account2Id, 'PUT', $url);
					$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
				}
			}
			
			
			$createdRowData	= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
			$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
			if((strtolower($results['Status']) == 'ok') AND (isset($results['CreditNotes']['CreditNote']['CreditNoteID']))){
				$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdOrderId' => $results['CreditNotes']['CreditNote']['CreditNoteID'], 'status' => '1'));
			}
			elseif((strtolower($results['Status']) == 'ok') AND (isset($results['Invoices']['Invoice']['InvoiceID']))){
				$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdOrderId' => $results['Invoices']['Invoice']['InvoiceID'], 'status' => '1'));
			}
			else{
				$badRequest	= 0;
				$errormsg	= array();
				$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
				$errormsg	= json_decode($errormsg,true);
				if(isset($errormsg['head']['title'])){
					if($errormsg['head']['title'] == '400 Bad Request'){
						$badRequest	= 1;
					}
				}
				if(!$badRequest){
					$createdRowData['lastTry']	= strtotime('now');
					$this->ci->db->where_in('orderId',array_values($negativeOrderIds))->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdRowData)));
				}
			}
		}
	}
}
$this->postConsolStockAdjustment();
?>