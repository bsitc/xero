<?php
$this->reInitialize();
$enableStockAdjustment			= $this->ci->globalConfig['enableStockAdjustment'];
$enableConsolStockAdjustment	= $this->ci->globalConfig['enableConsolStockAdjustment'];
$getAllCurrencyWithRate			= $this->ci->brightpearl->getAllCurrencyWithRate();
$clientcode						= $this->ci->config->item('clientcode');
$dateLockSettings				= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($enableStockAdjustment){continue;}
	if(!$enableConsolStockAdjustment){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);

	$this->ci->db->reset_query();
	$StockDatas	= $this->ci->db->get_where('stock_journals',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(!$StockDatas){
		continue;
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}

	if($StockDatas){
		$aggregatedStockDatas	= array();
		foreach($StockDatas as $StockData){
			$journalId			= $StockData['journalId'];
			$currencyCode		= $StockData['currencyCode'];
			$taxDate			= $StockData['taxDate'];
			$stockJournalParam	= json_decode($StockData['params'],true);
			$taxDate			= $stockJournalParam['taxDate'];
			
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate			= $date->format('Ymd');
			}
			else{
				$BPDateOffset		= (int)substr($taxDate,23,3);
				$Acc2Offset			= 0;
				$diff				= $BPDateOffset - $Acc2Offset;
				$date				= new DateTime($taxDate);
				$BPTimeZone			= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
			}
			
			if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['stocks'])) AND (strlen($dateLockSettings['stocks']) > 0)){
				$checkTaxDate	= date('Ymd',strtotime($taxDate));
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['stocks'])));
				
				if($dateLockSettings['stocksCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
			if($taxDate > $fetchTaxDate){continue;}
			$aggregatedStockDatas[$taxDate][$currencyCode][$journalId]	= $StockData;
		}
		if($aggregatedStockDatas){
			foreach($aggregatedStockDatas as $consolTaxDate => $aggregatedStockDatasTemp1){
				foreach($aggregatedStockDatasTemp1 as $consolCurrency => $aggregatedStockDatasTemp2){
					$consolBillRequest		= array();
					$consolItemLineRequest	= array();
					$lineItmeNominalArray	= array();
					$processedJournalIds	= array();
					$lineItemSequence		= 0;
					$StockCount				= 0;
					foreach($aggregatedStockDatasTemp2 as $journalId => $aggregatedStockData){
						$stockJournalParam	= json_decode($aggregatedStockData['params'],true);
						$credits			= $stockJournalParam['credits'];
						$debits				= $stockJournalParam['debits'];
						if(!$credits['0']){
							$credits	= array($credits);
						}
						if(!$debits['0']){
							$debits		= array($debits);
						}
						foreach($credits as $credit){
							$credit['transactionAmount']	= ((-1) * $credit['transactionAmount']);
							if(($credit['nominalCode'] >= 1000) AND ($credit['nominalCode'] <= 1999)){
								$defaultNominal	= $config['AssetAccountRef'];
							}
							else{
								$defaultNominal	= $config['stockAdjustmentAccountRef'];
							}
							if(isset($nominalMappings[$credit['nominalCode']])){
								if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
									$defaultNominal	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
								}
							}
							if(isset($lineItmeNominalArray['allLine'][$defaultNominal]['netAmt'])){
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	+= $credit['transactionAmount'];
							}
							else{
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	= $credit['transactionAmount'];
							}
						}
						foreach($debits as $debit){
							$debit['transactionAmount']		= ((1) * $debit['transactionAmount']);
							if(($debit['nominalCode'] >= 1000) AND ($debit['nominalCode'] <= 1999)){
								$defaultNominal	= $config['AssetAccountRef'];
							}
							else{
								$defaultNominal	= $config['stockAdjustmentAccountRef'];
							}
							if(isset($nominalMappings[$debit['nominalCode']])){
								if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
									$defaultNominal	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
								}
							}
							if(isset($lineItmeNominalArray['allLine'][$defaultNominal]['netAmt'])){
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	+= $debit['transactionAmount'];
							}
							else{
								$lineItmeNominalArray['allLine'][$defaultNominal]['netAmt']	= $debit['transactionAmount'];
							}
						}
						$processedJournalIds[]	= $journalId;
						$StockCount++;
					}
					if($lineItmeNominalArray){
						foreach($lineItmeNominalArray as $lineType => $lineItmeNominalArrayTemp){
							foreach($lineItmeNominalArrayTemp as $lineNominal => $lineItmeNominalArrayTemp2){
								$LastPrice	= ((1) * $lineItmeNominalArrayTemp2['netAmt']);
								$consolItemLineRequest[$lineItemSequence]	= array(
									'Description'	=> 'Consol of '.$StockCount.' stock adjustments',
									'Quantity'		=> 1,
									'TaxType'		=> $config['salesNoTaxCode'],
									'AccountCode'	=> $lineNominal,
									'UnitAmount'	=> sprintf("%.4f",($LastPrice)),
									'LineAmount'	=> sprintf("%.4f",($LastPrice)),
								);
								$lineItemSequence++;
							}
						}
						if($consolItemLineRequest){
							$InvoiceNumber		= $consolTaxDate.'-SA-'.$consolCurrency;
							$consolBillRequest	= array(
								'InvoiceNumber'		=> $InvoiceNumber,
								'Type'				=> 'ACCPAY',
								'Contact'			=> array('Name'	=> 'Inventory Adjustments'),
								'Date'				=> date('Y-m-d',strtotime($consolTaxDate)),
								'DueDate'			=> date('Y-m-d',strtotime($consolTaxDate)),
								'CurrencyCode'		=> $consolCurrency,
								'Status'			=> 'AUTHORISED',
								'LineItems'			=> $consolItemLineRequest,
							);
							$this->headers		= array();
							$url				= '2.0/Invoices?unitdp=4';
							$this->initializeConfig($account2Id, 'PUT', $url);
							$consolBillResults	= $this->getCurl($url, 'PUT', json_encode($consolBillRequest), 'json', $account2Id)[$account2Id];
							$createdParams		= array(
								'Request data	: ' => $consolBillRequest,
								'Response data	: ' => $consolBillResults,
							);
							$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('stock_journals',array('createdParams' => json_encode($createdParams)));
							if((strtolower($consolBillResults['Status']) == 'ok') AND (isset($consolBillResults['Invoices']['Invoice']['InvoiceID']))){	
								$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('stock_journals',array('created_journalId' => $consolBillResults['Invoices']['Invoice']['InvoiceID'], 'InvoiceNumber' => $InvoiceNumber, 'status' => '1'));
							}
						}
					}
				}
			}
		}
	}
}