<?php
$this->reInitialize();
$enableStockAdjustment			= $this->ci->globalConfig['enableStockAdjustment'];
$enableSAConsol					= $this->ci->globalConfig['enableSAConsol'];
$enableConsolStockAdjustment	= $this->ci->globalConfig['enableConsolStockAdjustment'];
$getAllCurrencyWithRate			= $this->ci->brightpearl->getAllCurrencyWithRate();
$clientcode						= $this->ci->config->item('clientcode');
$dateLockSettings				= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableSAConsol){continue;}
	if($enableConsolStockAdjustment){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);

	$this->ci->db->reset_query();
	$StockDatas	= $this->ci->db->get_where('stock_adjustment',array('account2Id' => $account2Id , 'status' => 0,'GoodNotetype' => 'SC'))->result_array();
	if(!$StockDatas){
		continue;
	}
	
	if($StockDatas){
		$aggregatedStockDatas	= array();
		foreach($StockDatas as $StockData){
			$goodsMovementId		= $StockData['orderId'];
			$taxDate				= $StockData['created'];
			$stockAdjustmentParam	= json_decode($StockData['rowData'],true);
			$currencyCode			= $stockAdjustmentParam['currencyCode'];
			
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate			= $date->format('Ymd');
			}
			else{
				$BPDateOffset		= (int)substr($taxDate,23,3);
				$Acc2Offset			= 0;
				$diff				= $BPDateOffset - $Acc2Offset;
				$date				= new DateTime($taxDate);
				$BPTimeZone			= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
			}
			
			if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['stocks'])) AND (strlen($dateLockSettings['stocks']) > 0)){
				$checkTaxDate	= date('Ymd',strtotime($taxDate));
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['stocks'])));
				
				if($dateLockSettings['stocksCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['stocksCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
			if($taxDate > $fetchTaxDate){continue;}
			$aggregatedStockDatas[$taxDate][$currencyCode][$goodsMovementId]	= $StockData;
		}
		
		if($aggregatedStockDatas){
			foreach($aggregatedStockDatas as $consolTaxDate => $aggregatedStockDatasTemp1){
				foreach($aggregatedStockDatasTemp1 as $consolCurrency => $aggregatedStockDatasTemp2){
					$consolBillRequest		= array();
					$consolItemLineRequest	= array();
					$consolSARequestArray	= array();
					$positiveNominal		= $config['positiveStockAdjustment'];
					$negativeNominal		= $config['negativeStockAdjustment'];
					$inventoryLineNominal	= $config['inventoryLineStockAdjustment'];
					$processedGOIds			= array();
					$allGoodsMovementId		= array_column($aggregatedStockDatasTemp2,'orderId');
					$totalPositiveAmount	= 0;
					$totalNegativeAmount	= 0;
					$lineItemSequence		= 0;
					$StockCount				= 0;
			
					if((!$positiveNominal) OR (!$negativeNominal) OR (!$inventoryLineNominal)){
						$this->ci->db->where_in('orderId',$allGoodsMovementId)->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('message' => 'Nominal is missing'));
						continue;
					}
					
					foreach($aggregatedStockDatasTemp2 as $goodsMovementId => $aggregatedStockData){
						
						if($aggregatedStockData['qty'] < 0){
							if(isset($consolSARequestArray['negative'])){
								$consolSARequestArray['negative']	+= (abs($aggregatedStockData['price'] * $aggregatedStockData['qty']));
							}
							else{
								$consolSARequestArray['negative']	= (abs($aggregatedStockData['price'] * $aggregatedStockData['qty']));
							}
							$totalNegativeAmount += (abs($aggregatedStockData['price'] * $aggregatedStockData['qty']));
						}
						else{
							if(isset($consolSARequestArray['positive'])){
								$consolSARequestArray['positive']	+= (abs($aggregatedStockData['price'] * $aggregatedStockData['qty']));
							}
							else{
								$consolSARequestArray['positive']	= (abs($aggregatedStockData['price'] * $aggregatedStockData['qty']));
							}
							
							$totalPositiveAmount += (abs($aggregatedStockData['price'] * $aggregatedStockData['qty']));
						}
						
						$processedGOIds[]	= $goodsMovementId;
						$StockCount++;
					}
					if($consolSARequestArray){
						foreach($consolSARequestArray as $lineType => $consolSARequestArrayTemp){
								if($lineType == 'positive'){
									$consolItemLineRequest[$lineItemSequence]	= array(
										'Description'	=> 'Consol of '.$StockCount.' stock adjustments',
										'Quantity'		=> 1,
										'TaxType'		=> $config['PurchaseNoTaxCode'],
										'AccountCode'	=> $positiveNominal,
										'UnitAmount'	=> sprintf("%.4f",($consolSARequestArrayTemp)),
										'LineAmount'	=> sprintf("%.4f",($consolSARequestArrayTemp)),
									);
									$lineItemSequence++;
									
									$consolItemLineRequest[$lineItemSequence]	= array(
										//'Description'	=> 'Inventory Adjustments',
										'Description'	=>  'Consol of '.$StockCount.' stock adjustments',
										'Quantity'		=> 1,
										'TaxType'		=> $config['PurchaseNoTaxCode'],
										'AccountCode'	=> $inventoryLineNominal,
										'UnitAmount'	=> sprintf("%.4f",((-1) * $consolSARequestArrayTemp)),
										'LineAmount'	=> sprintf("%.4f",((-1) * $consolSARequestArrayTemp)),
									);
									$lineItemSequence++;
									
								}else{
									$consolItemLineRequest[$lineItemSequence]	= array(
										'Description'	=> 'Consol of '.$StockCount.' stock adjustments',
										'Quantity'		=> 1,
										'TaxType'		=> $config['PurchaseNoTaxCode'],
										'AccountCode'	=> $negativeNominal,
										'UnitAmount'	=> sprintf("%.4f",((-1) * $consolSARequestArrayTemp)),
										'LineAmount'	=> sprintf("%.4f",((-1) * $consolSARequestArrayTemp)),
									);
									$lineItemSequence++;
									
									$consolItemLineRequest[$lineItemSequence]	= array(
										//'Description'	=> 'Inventory Adjustments',
										'Description'	=>  'Consol of '.$StockCount.' stock adjustments',
										'Quantity'		=> 1,
										'TaxType'		=> $config['PurchaseNoTaxCode'],
										'AccountCode'	=> $inventoryLineNominal,
										'UnitAmount'	=> sprintf("%.4f",($consolSARequestArrayTemp)),
										'LineAmount'	=> sprintf("%.4f",($consolSARequestArrayTemp)),
									);
									$lineItemSequence++;
								}
						}
						
						if($consolItemLineRequest){
							$InvoiceNumber		= $consolTaxDate.'-SA-'.$consolCurrency;
							$consolBillRequest	= array(
								'InvoiceNumber'		=> $InvoiceNumber,
								'Type'				=> 'ACCPAY',
								'Contact'			=> array('Name'	=> 'Inventory Adjustments'),
								'Date'				=> date('Y-m-d',strtotime($consolTaxDate)),
								'DueDate'			=> date('Y-m-d',strtotime($consolTaxDate)),
								'CurrencyCode'		=> $consolCurrency,
								'Status'			=> 'AUTHORISED',
								'LineItems'			=> $consolItemLineRequest,
							);
							//echo 'consolBillRequest<pre>';print_r($consolBillRequest);die;
							$this->headers		= array();
							$url				= '2.0/Invoices?unitdp=4';
							$this->initializeConfig($account2Id, 'PUT', $url);
							$consolBillResults	= $this->getCurl($url, 'PUT', json_encode($consolBillRequest), 'json', $account2Id)[$account2Id];
							$createdParams		= array(
								'Request data	: ' => $consolBillRequest,
								'Response data	: ' => $consolBillResults,
							);
							//echo 'consolBillResults<pre>';print_r($consolBillResults);
							$this->ci->db->where_in('orderId',$processedGOIds)->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdRowData' => json_encode($createdParams)));
							//echo $this->ci->db->last_query();
							if((strtolower($consolBillResults['Status']) == 'ok') AND (isset($consolBillResults['Invoices']['Invoice']['InvoiceID']))){	
								$this->ci->db->where_in('orderId',$processedGOIds)->where(array('account2Id' => $account2Id))->update('stock_adjustment',array('createdOrderId' => $consolBillResults['Invoices']['Invoice']['InvoiceID'], 'invoiceNumber' => $InvoiceNumber, 'status' => '1','isConsol' => '1','aggregationId' => $consolBillResults['Invoices']['Invoice']['InvoiceID']));
							}
						}
					}
				}
			}
		}
	}
}