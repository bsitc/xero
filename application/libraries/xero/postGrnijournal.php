<?php
$this->reInitialize();
$clientcode				= $this->ci->config->item('clientcode');
$getAllBPExchangeRate	= $this->ci->brightpearl->getAllBPExchangeRate();

foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$allDatas	= $this->ci->db->get_where('grni_journal',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(empty($allDatas)){continue;}
	
	$allPurchaseOrderData	= array();
	$allPurchaseOrderIds	= array();
	$allPurchaseOrderIds	= array_column($allDatas, 'orderId');
	$allPurchaseInfos		= $this->ci->db->where_in('orderId',$allPurchaseOrderIds)->get_where('purchase_order')->result_array();
	
	if(!empty($allPurchaseInfos)){
		foreach($allPurchaseInfos as $allPurchaseInfosTemp){
			$allPurchaseOrderData[$allPurchaseInfosTemp['orderId']]	= $allPurchaseInfosTemp;
		};
	}
	
	$postDataArray	= array();
	foreach($allDatas as $allDatasTemp){
		if($allDatasTemp['status'] != 0){continue;}
		if($allDatasTemp['createdJournalsId']){continue;}
		$taxDate			= $allDatasTemp['taxDate'];
		$BPDateOffset		= (int)substr($taxDate,23,3);
		$Acc2Offset			= 0;
		$diff				= $BPDateOffset - $Acc2Offset;
		$date				= new DateTime($taxDate);
		$BPTimeZone			= 'GMT';
		$date->setTimezone(new DateTimeZone($BPTimeZone));
		if($diff){
			$diff			.= ' hour';
			$date->modify($diff);
		}
		$taxDate			= $date->format('Y-m-d');
		$postDataArray[$allDatasTemp['orderId']][$taxDate][]	= $allDatasTemp;
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	foreach($postDataArray as $postPoId	=> $postDataArrayTempTemp){
		foreach($postDataArrayTempTemp as $postJournalTaxDate	=> $postDataArrayTemp){
			$request				= array();
			$results				= array();
			$trackingDetails		= array();
			$createdParams			= array();
			$invoiceLineReq			= array();
			$invoiceLineSeq			= 0;
			$invoiceLineArray		= array();
			$config1				= array();
			$allSentJournalRowIds	= array();
			$taxDate				= $postJournalTaxDate;
			
			foreach($postDataArrayTemp as $orderDatas){
				if($orderDatas['status'] != 0){continue;}
				if($orderDatas['createdJournalsId']){continue;}
				$journalsId		= $orderDatas['journalsId'];
				$config1		= $this->ci->account1Config[$orderDatas['account1Id']];
				
				$blockPosting		= 0;
				$debitAccRef		= '';
				$creditAccRef		= '';
				$fetchedExRate		= 1;
				
				$params				= json_decode($orderDatas['params'],true);
				$createdParams		= json_decode($orderDatas['createdParams'],true);
				$bpBaseCurrency		= strtolower($config1['currencyCode']);
				$channelId			= $orderDatas['channelId'];
				$allCredits			= $params['credits'];
				$allDebits			= $params['debits'];
				
				if(isset($allPurchaseOrderData[$postPoId])){
					if(strlen($allPurchaseOrderData[$postPoId]['createOrderId']) > 0){
						$this->ci->db->update('grni_journal',array('message' => 'Purchase order already posted.', 'status' => 4),array('id' => $orderDatas['id']));
						continue;
					}
				}
				
				if(isset($createdParams['lastTry'])){
					if($createdParams['lastTry'] > strtotime('-3 hour')){
						continue;
					}
				}
			
				if(strtolower($XeroBaseCurrency) != strtolower($bpBaseCurrency)){
					if(isset($getAllBPExchangeRate[$orderDatas['account1Id']][strtolower($bpBaseCurrency)][strtolower($XeroBaseCurrency)][date('Ymd',strtotime($taxDate))])){
						$fetchedExRate	= $getAllBPExchangeRate[$orderDatas['account1Id']][strtolower($bpBaseCurrency)][strtolower($XeroBaseCurrency)][date('Ymd',strtotime($taxDate))];
					}
					else{
						$this->ci->db->update('grni_journal',array('message' => 'ExRate Not Found'),array('id' => $orderDatas['id']));
						continue;
					}
				}
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					$trackingDetails	= explode("~=",$trackingDetails);
				}
				
				if((is_array($allCredits)) AND (!empty($allCredits))){
					foreach($allCredits as $creditsData){
						if($creditsData['transactionAmount'] == 0){continue;}
						$creditAccRef	= '';
						if(isset($nominalMappings[$creditsData['nominalCode']])){
							if($nominalMappings[$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalMappings[$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if(!$creditAccRef){
							$blockPosting	= 1;
						}
					}
				}
				if((is_array($allDebits)) AND (!empty($allDebits))){
					foreach($allDebits as $debitsData){
						if($debitsData['transactionAmount'] == 0){continue;}
						$creditAccRef	= '';
						if(isset($nominalMappings[$debitsData['nominalCode']])){
							if($nominalMappings[$debitsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalMappings[$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if(!$creditAccRef){
							$blockPosting	= 1;
						}
					}
				}
				if($blockPosting){
					$this->ci->db->update('grni_journal',array('message' => 'NominalMapping is Missing'),array('id' => $orderDatas['id']));
					continue;
				}
				if((is_array($allCredits)) AND (!empty($allCredits))){
					foreach($allCredits as $key => $creditsData){
						if($creditsData['transactionAmount'] == 0){continue;}
						$creditAccRef	= '';
						if(isset($nominalMappings[$creditsData['nominalCode']])){
							if($nominalMappings[$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalMappings[$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if((isset($invoiceLineArray['credits'][$creditAccRef]))){
							$invoiceLineArray['credits'][$creditAccRef]['lineAmount']	+= (($creditsData['transactionAmount']) *($fetchedExRate));
						}
						else{
							$invoiceLineArray['credits'][$creditAccRef]['lineAmount']	= (($creditsData['transactionAmount']) *($fetchedExRate));
						}
					}
				}
				if((is_array($allDebits)) AND (!empty($allDebits))){
					foreach($allDebits as $key => $debitsData){
						if($debitsData['transactionAmount'] == 0){continue;}
						$debitAccRef	= '';
						if(isset($nominalMappings[$debitsData['nominalCode']])){
							if($nominalMappings[$debitsData['nominalCode']]['account2NominalId']){
								$debitAccRef	= $nominalMappings[$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId']){
								$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if((isset($invoiceLineArray['debits'][$debitAccRef]))){
							$invoiceLineArray['debits'][$debitAccRef]['lineAmount']	+= (($debitsData['transactionAmount']) *($fetchedExRate));
						}
						else{
							$invoiceLineArray['debits'][$debitAccRef]['lineAmount']	= (($debitsData['transactionAmount']) *($fetchedExRate));
						}
					}
				}
				$allSentJournalRowIds[]	= $orderDatas['id'];
			}
			if(empty($invoiceLineArray)){continue;}
			foreach($invoiceLineArray as $txnType => $invoiceLineArrayTemp){
				foreach($invoiceLineArrayTemp as $acclineRef => $invoiceLineArrayTempTemp){
					$invoiceLineReq[$invoiceLineSeq]	= array(
						'LineAmount'	=> ($txnType == 'debits') ? ($invoiceLineArrayTempTemp['lineAmount']) : ((-1) * ($invoiceLineArrayTempTemp['lineAmount'])),
						'AccountCode'	=> $acclineRef,
						'TaxType'		=> $config['salesNoTaxCode'],
					);
					if(!empty($trackingDetails)){
						$invoiceLineReq[$invoiceLineSeq]['Tracking'][0]	= array(
							'Name'		=> $trackingDetails['0'],
							'Option'	=> $trackingDetails['1']
						);
					}
					$invoiceLineSeq++;
				}
			}
			if(empty($invoiceLineReq)){continue;}
			$request	= array(
				'Narration'			=> 'PO#'.$postPoId,
				'JournalLines'		=> $invoiceLineReq,
				'Date'				=> $taxDate,
				'LineAmountTypes'	=> "NoTax",
				'Status'			=> "POSTED",
			);
			
			if(!empty($request)){
				$this->headers	= array();
				$url			= '2.0/ManualJournals';
				$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
				$createdParams	= array(
					'Request data	: '	=> $request,
					'Response data	: '	=> $results,
				);
				
				if(!empty($allSentJournalRowIds)){
					if((strtolower($results['Status']) == 'ok') AND (isset($results['ManualJournals']['ManualJournal']['ManualJournalID']))){
						$updateArray	= array(
							'status'			=> 1,
							'message'			=> '',
							'createdJournalsId'	=> $results['ManualJournals']['ManualJournal']['ManualJournalID'],
							'createdParams'		=> json_encode($createdParams),
						);
						$this->ci->db->where_in('id',$allSentJournalRowIds)->update('grni_journal',$updateArray);
					}
					else{
						$badRequest	= 0;
						$errormsg	= array();
						$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
						$errormsg	= json_decode($errormsg,true);
						if(isset($errormsg['head']['title'])){
							if($errormsg['head']['title'] == '400 Bad Request'){
								$badRequest	= 1;
							}
						}
						if(!$badRequest){
							$updateArray	= array(
								'createdParams'	=> json_encode($createdParams),
							);
							$this->ci->db->where_in('id',$allSentJournalRowIds)->update('grni_journal',$updateArray);
						}
					}
				}
			}
		}
	}
}
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$allDatas	= $this->ci->db->get_where('grni_journal',array('account2Id' => $account2Id , 'status' => 1, 'isOrderInvoiced' => 1))->result_array();
	if(empty($allDatas)){continue;}
	
	$allPurchaseOrderData	= array();
	$allPurchaseOrderIds	= array();
	$allPurchaseOrderIds	= array_column($allDatas, 'orderId');
	$allPurchaseInfos		= $this->ci->db->where_in('orderId',$allPurchaseOrderIds)->get_where('purchase_order',array('status <>' => 0, 'createOrderId <>' => ''))->result_array();
	
	if(empty($allPurchaseInfos)){
		continue;
	}
	else{
		foreach($allPurchaseInfos as $allPurchaseInfosTemp){
			$allPurchaseOrderData[$allPurchaseInfosTemp['orderId']]	= $allPurchaseInfosTemp;
		}
	}
	
	$postDataArray	= array();
	foreach($allDatas as $allDatasTemp){
		if($allDatasTemp['status'] != 1){continue;}
		if($allDatasTemp['reversedJournalsId']){continue;}
		$postDataArray[$allDatasTemp['orderId']][$allDatasTemp['createdJournalsId']][]	= $allDatasTemp;
	}
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	foreach($postDataArray as $postPoId	=> $postDataArrayTempTemp){
		$journalsOrderInfo	= array();
		if(isset($allPurchaseOrderData[$postPoId])){
			if(strlen($allPurchaseOrderData[$postPoId]['createOrderId']) > 0){
				$journalsOrderInfo	= $allPurchaseOrderData[$postPoId];
			}
			else{
				continue;
			}
		}
		else{
			continue;
		}
		if(empty($journalsOrderInfo)){continue;}
		foreach($postDataArrayTempTemp as $postCreatedJournalId	=> $postDataArrayTemp){
			$request				= array();
			$results				= array();
			$createdParams			= array();
			$trackingDetails		= array();
			$invoiceLineReq			= array();
			$invoiceLineSeq			= 0;
			$invoiceLineArray		= array();
			$config1				= array();
			$allSentJournalRowIds	= array();
			$orderRawInfo			= json_decode($journalsOrderInfo['rowData'], true);
			$poOrderTaxDate			= $orderRawInfo['invoices'][0]['taxDate'];
			//overrideTaxDateInReverseJournal
			$taxDate				= $poOrderTaxDate;
			
			//taxdate chanages
			$BPDateOffset			= (int)substr($taxDate,23,3);
			$Acc2Offset				= 0;
			$diff					= $BPDateOffset - $Acc2Offset;
			$date					= new DateTime($taxDate);
			$BPTimeZone				= 'GMT';
			$date->setTimezone(new DateTimeZone($BPTimeZone));
			if($diff){
				$diff				.= ' hour';
				$date->modify($diff);
			}
			$taxDate				= $date->format('Y-m-d');
			
			foreach($postDataArrayTemp as $orderDatas){
				if($orderDatas['status'] != 1){continue;}
				if($orderDatas['reversedJournalsId']){continue;}
				$journalsId		= $orderDatas['journalsId'];
				$config1		= $this->ci->account1Config[$orderDatas['account1Id']];
				
				$blockPosting		= 0;
				$debitAccRef		= '';
				$creditAccRef		= '';
				$fetchedExRate		= 1;
				
				$params				= json_decode($orderDatas['params'],true);
				$createdParams		= json_decode($orderDatas['createdParams'],true);
				$bpBaseCurrency		= strtolower($config1['currencyCode']);
				$channelId			= $orderDatas['channelId'];
				$allCredits			= $params['credits'];
				$allDebits			= $params['debits'];
				
				$fetchedExRate		= 1;
				if(strtolower($XeroBaseCurrency) != strtolower($bpBaseCurrency)){
					if(isset($getAllBPExchangeRate[$orderDatas['account1Id']][strtolower($bpBaseCurrency)][strtolower($XeroBaseCurrency)][date('Ymd',strtotime($taxDate))])){
						$fetchedExRate	= $getAllBPExchangeRate[$orderDatas['account1Id']][strtolower($bpBaseCurrency)][strtolower($XeroBaseCurrency)][date('Ymd',strtotime($taxDate))];
					}
					else{
						$this->ci->db->update('grni_journal',array('message' => 'ExRate Not Found'),array('id' => $orderDatas['id']));
						continue;
					}
				}
				
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					$trackingDetails	= explode("~=",$trackingDetails);
				}
				
				if(isset($createdParams['lastTry'])){
					if($createdParams['lastTry'] > strtotime('-3 hour')){
						continue;
					}
				}
			
				if((is_array($allCredits)) AND (!empty($allCredits))){
					foreach($allCredits as $creditsData){
						if($creditsData['transactionAmount'] == 0){continue;}
						$creditAccRef	= '';
						if(isset($nominalMappings[$creditsData['nominalCode']])){
							if($nominalMappings[$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalMappings[$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if(!$creditAccRef){
							$blockPosting	= 1;
						}
					}
				}
				if((is_array($allDebits)) AND (!empty($allDebits))){
					foreach($allDebits as $debitsData){
						if($debitsData['transactionAmount'] == 0){continue;}
						$creditAccRef	= '';
						if(isset($nominalMappings[$debitsData['nominalCode']])){
							if($nominalMappings[$debitsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalMappings[$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if(!$creditAccRef){
							$blockPosting	= 1;
						}
					}
				}
				if($blockPosting){
					$this->ci->db->update('grni_journal',array('message' => 'NominalMapping is Missing'),array('id' => $orderDatas['id']));
					continue;
				}
				if((is_array($allCredits)) AND (!empty($allCredits))){
					foreach($allCredits as $key => $creditsData){
						if($creditsData['transactionAmount'] == 0){continue;}
						$creditAccRef	= '';
						if(isset($nominalMappings[$creditsData['nominalCode']])){
							if($nominalMappings[$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalMappings[$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId']){
								$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$creditsData['nominalCode']]['account2NominalId'];
							}
						}
						if((isset($invoiceLineArray['credits'][$creditAccRef]))){
							$invoiceLineArray['credits'][$creditAccRef]['lineAmount']	+= (($creditsData['transactionAmount']) *($fetchedExRate));
						}
						else{
							$invoiceLineArray['credits'][$creditAccRef]['lineAmount']	= (($creditsData['transactionAmount']) *($fetchedExRate));
						}
					}
				}
				if((is_array($allDebits)) AND (!empty($allDebits))){
					foreach($allDebits as $key => $debitsData){
						if($debitsData['transactionAmount'] == 0){continue;}
						$debitAccRef	= '';
						if(isset($nominalMappings[$debitsData['nominalCode']])){
							if($nominalMappings[$debitsData['nominalCode']]['account2NominalId']){
								$debitAccRef	= $nominalMappings[$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]))){
							if($nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId']){
								$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$debitsData['nominalCode']]['account2NominalId'];
							}
						}
						if((isset($invoiceLineArray['debits'][$debitAccRef]))){
							$invoiceLineArray['debits'][$debitAccRef]['lineAmount']	+= (($debitsData['transactionAmount']) *($fetchedExRate));
						}
						else{
							$invoiceLineArray['debits'][$debitAccRef]['lineAmount']	= (($debitsData['transactionAmount']) *($fetchedExRate));
						}
					}
				}
				$allSentJournalRowIds[]	= $orderDatas['id'];
			}
			if(empty($invoiceLineArray)){continue;}
			foreach($invoiceLineArray as $txnType => $invoiceLineArrayTemp){
				foreach($invoiceLineArrayTemp as $acclineRef => $invoiceLineArrayTempTemp){
					$invoiceLineReq[$invoiceLineSeq]	= array(
						'LineAmount'	=> ($txnType == 'credits') ? ($invoiceLineArrayTempTemp['lineAmount']) : ((-1) * ($invoiceLineArrayTempTemp['lineAmount'])),
						'AccountCode'	=> $acclineRef,
						'TaxType'		=> $config['salesNoTaxCode'],
					);
					if(!empty($trackingDetails)){
						$invoiceLineReq[$invoiceLineSeq]['Tracking'][0]	= array(
							'Name'		=> $trackingDetails['0'],
							'Option'	=> $trackingDetails['1']
						);
					}
					$invoiceLineSeq++;
				}
			}
			if(empty($invoiceLineReq)){continue;}
			$request	= array(
				'Narration'			=> 'PO#'.$postPoId.'-Reverse',
				'JournalLines'		=> $invoiceLineReq,
				'Date'				=> $taxDate,
				'LineAmountTypes'	=> "NoTax",
				'Status'			=> "POSTED",
			);
			
			if(!empty($request)){
				$this->headers	= array();
				$url			= '2.0/ManualJournals';
				$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
				$createdParams['Reverse Journal Request data	: ']	= $request;
				$createdParams['Reverse Journal Response data	: ']	= $results;
				
				if(isset($createdParams['lastTry'])){
					unset($createdParams['lastTry']);
				}
				
				if(!empty($allSentJournalRowIds)){
					if((strtolower($results['Status']) == 'ok') AND (isset($results['ManualJournals']['ManualJournal']['ManualJournalID']))){
						$updateArray	= array(
							'status'			=> 2,
							'message'			=> '',
							'reversedJournalsId'	=> $results['ManualJournals']['ManualJournal']['ManualJournalID'],
							'createdParams'		=> json_encode($createdParams),
						);
						$this->ci->db->where_in('id',$allSentJournalRowIds)->update('grni_journal',$updateArray);
					}
					else{
						$badRequest	= 0;
						$errormsg	= array();
						$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
						$errormsg	= json_decode($errormsg,true);
						if(isset($errormsg['head']['title'])){
							if($errormsg['head']['title'] == '400 Bad Request'){
								$badRequest	= 1;
							}
						}
						if(!$badRequest){
							$updateArray	= array(
								'createdParams'	=> json_encode($createdParams),
							);
							$this->ci->db->where_in('id',$allSentJournalRowIds)->update('grni_journal',$updateArray);
						}
					}
				}
			}
		}
	}
}