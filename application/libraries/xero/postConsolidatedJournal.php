<?php
$this->reInitialize();
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$enableAmazonFees			= $this->ci->globalConfig['enableAmazonFees'];
$disableConsolSOpayments	= $this->ci->globalConfig['disableConsolSOpayments'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableAmazonFees){continue;}
	$config	= $this->accountConfig[$account2Id];
	
	$allAggregatedSales	= $this->ci->db->get_where('sales_order',array('isNetOff' => '0', 'account2Id' => $account2Id, 'status > ' => '0' , 'IsJournalposted' => '0' , 'sendInAggregation' => 1))->result_array();
	if(!$allAggregatedSales){continue;}
	
	$enableAggregationAdvance		= $this->ci->globalConfig['enableAggregationAdvance'];
	$enableAggregationOnAPIfields	= $this->ci->globalConfig['enableAggregationOnAPIfields'];
	
	$this->ci->db->reset_query();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	$consolOnCustomActive		= 0;
	$consolOnAPIActive			= 0;
	if($AggregationMappingsTemps){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			
			if(!$ConsolMappingCustomField AND !$account1APIFieldId){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
			}
			else{
				if($account1APIFieldId){
					$consolOnAPIActive		= 1;
					$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
					foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
					}
				}
				else{
					$consolOnCustomActive	= 1;
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
				}
			}
		}
	}
	if(!$AggregationMappings AND !$AggregationMappings2){
		continue;
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	if($allAggregatedSales){
		$allAmazonFeeData			= array();
		$allSearchOrderIds			= array();
		
		$aggregatedJournalsData		= array();
		$SalesByAggregationIdData	= array();
		$AggregationGroup			= array();
		$ConsolForceCurrencyMapping	= array();
		foreach($allAggregatedSales as $allAggregatedSalesData){
			if($allAggregatedSalesData['status'] == 4){continue;}
			$allSearchOrderIds[]	= $allAggregatedSalesData['orderId'];
			$AggregationGroup[$allAggregatedSalesData['aggregationId']][]	= $allAggregatedSalesData;
		}
		
		if(!empty($allSearchOrderIds)){
			$allSearchOrderIds	= array_filter($allSearchOrderIds);
			$allSearchOrderIds	= array_unique($allSearchOrderIds);
			$journalDatasTemps	= $this->ci->db->where_in('orderId',$allSearchOrderIds)->get_where('amazon_ledger',array('status ' => 0,'account2Id' => $account2Id))->result_array();
			if(!empty($journalDatasTemps)){
				foreach($journalDatasTemps as $journalDatasTempsTemp){
					$allAmazonFeeData[$journalDatasTempsTemp['orderId']][]	= $journalDatasTempsTemp;
				}
			}
		}
		
		foreach($AggregationGroup as $aggregationId => $AggregationGroupSales){
			$IsJournalPendings	= 0;
			foreach($AggregationGroupSales as $AggregationGroupSalesData){
				$bpconfig				= $this->ci->account1Config[$AggregationGroupSalesData['account1Id']];
				$orderId				= $AggregationGroupSalesData['orderId'];
				$rowDatas				= json_decode($AggregationGroupSalesData['rowData'],true);
				$channelId				= $rowDatas['assignment']['current']['channelId'];
				$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
				$orderExchangeRate		= $rowDatas['currency']['exchangeRate'];
				$CustomFieldValueID		= '';
				if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
					$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				}
				$ConsolAPIFieldValueID	= '';
				
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				if($ConsolAPIFieldValueID){
					$CustomFieldValueID	= $ConsolAPIFieldValueID;
				}
				
				$journalDatasTemps	= array();
				if($enableAggregation){
					if($AggregationMappings){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsJournalAggregated']){
							$ConsolForceCurrencyMapping[$aggregationId]	= $orderExchangeRate;
							if((is_array($allAmazonFeeData)) AND (!empty($allAmazonFeeData)) AND (isset($allAmazonFeeData[$orderId]))){
								$aggregatedJournalsData[$aggregationId][]	= $allAmazonFeeData[$orderId];
							}
							else{
								$IsJournalPendings	= 1;
							}
						}
						elseif($AggregationMappings[$channelId][strtolower($CurrencyCode)]['IsJournalAggregated']){
							if((is_array($allAmazonFeeData)) AND (!empty($allAmazonFeeData)) AND (isset($allAmazonFeeData[$orderId]))){
								$aggregatedJournalsData[$aggregationId][]	= $allAmazonFeeData[$orderId];
							}
							else{
								$IsJournalPendings	= 1;
							}
						}
					}
					elseif($AggregationMappings2){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsJournalAggregated']){
							$ConsolForceCurrencyMapping[$aggregationId]	= $orderExchangeRate;
							if((is_array($allAmazonFeeData)) AND (!empty($allAmazonFeeData)) AND (isset($allAmazonFeeData[$orderId]))){
								$aggregatedJournalsData[$aggregationId][]	= $allAmazonFeeData[$orderId];
							}
							else{
								$IsJournalPendings	= 1;
							}
						}
						elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
							$ConsolForceCurrencyMapping[$aggregationId]	= $orderExchangeRate;
							if((is_array($allAmazonFeeData)) AND (!empty($allAmazonFeeData)) AND (isset($allAmazonFeeData[$orderId]))){
								$aggregatedJournalsData[$aggregationId][]	= $allAmazonFeeData[$orderId];
							}
							else{
								$IsJournalPendings	= 1;
							}
						}
						elseif($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['IsJournalAggregated']){
							if((is_array($allAmazonFeeData)) AND (!empty($allAmazonFeeData)) AND (isset($allAmazonFeeData[$orderId]))){
								$aggregatedJournalsData[$aggregationId][]	= $allAmazonFeeData[$orderId];
							}
							else{
								$IsJournalPendings	= 1;
							}
						}
						elseif($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['IsJournalAggregated']){
							if((is_array($allAmazonFeeData)) AND (!empty($allAmazonFeeData)) AND (isset($allAmazonFeeData[$orderId]))){
								$aggregatedJournalsData[$aggregationId][]	= $allAmazonFeeData[$orderId];
							}
							else{
								$IsJournalPendings	= 1;
							}
						}
					}
				}
				
				if($IsJournalPendings){
					unset($aggregatedJournalsData[$aggregationId]);
					break;
				}
			}
		}
		if($aggregatedJournalsData){
			$allSalesOrderIDs	= array();
			$allAmazonfeesIDs	= array();
			$EditRequest		= array();
			$EditResponse		= array();
			foreach($aggregatedJournalsData as $aggregationId => $aggregatedJournalsDatas){
				$allAmazonfeesIDs	= array();
				$EditRequest		= array();
				$EditResponse		= array();
				$EditSuccess		= 0;
				$totalFeeAmount		= 0;
				$totalFeeByNominal	= array();
				foreach($aggregatedJournalsDatas as $allJournalsTemp){
					foreach($allJournalsTemp as $allJournals){
						$journalParams		= json_decode($allJournals['params'], true);
						$allDebitLines		= $journalParams['debits'];
						$allDebitLines		= (!$allDebitLines['0']) ? (array($allDebitLines)) : ($allDebitLines);
						if((is_array($allDebitLines)) AND (!empty($allDebitLines))){
							foreach($allDebitLines as $debitLine){
								if(isset($totalFeeByNominal[$debitLine['nominalCode']])){
									$totalFeeByNominal[$debitLine['nominalCode']]	+= $debitLine['transactionAmount'];
								}
								else{
									$totalFeeByNominal[$debitLine['nominalCode']]	= $debitLine['transactionAmount'];
								}
							}
						}
						
						$allSalesOrderIDs[]	 = $allJournals['orderId'];
						$allAmazonfeesIDs[]  = $allJournals['journalsId'];
						$totalFeeAmount		+= $allJournals['amount'];
					}
				}
				if($totalFeeAmount AND (!empty($totalFeeByNominal))){
					
					if($ConsolForceCurrencyMapping[$aggregationId]){
						$newExchangeRate	= $ConsolForceCurrencyMapping[$aggregationId];
						$totalFeeAmount		= (($totalFeeAmount) * ((1) / ($newExchangeRate)));
						$totalFeeAmount		= sprintf("%.4f",$totalFeeAmount);
					}
					
					$SalesInfo	= $this->ci->db->get_where('sales_order',array('isNetOff' => '0', 'aggregationId' => $aggregationId, 'status >' => 0))->row_array();
					if($SalesInfo){
						$XeroInvoiceID	= $SalesInfo['createOrderId'];
						$salesRowData	= json_decode($SalesInfo['rowData'],true);
						$saleschannelId	= $salesRowData['assignment']['current']['channelId'];
						
						$trackingDetails	= array();
						if(isset($channelMappings[$saleschannelId])){
							$trackingDetails	= $channelMappings[$saleschannelId]['account2ChannelId'];
							$trackingDetails	= explode("~=",$trackingDetails);
						}
						
						$createdRowData	= json_decode($SalesInfo['createdRowData'],true);
						
						$this->headers	= array();
						$suburl			= '2.0/Invoices/'.$XeroInvoiceID;
						if($accountDetails['OAuthVersion'] == '2'){
							$suburl			= '2.0/Invoices?IDs='.$XeroInvoiceID.'&unitdp=4';
						}
						$this->initializeConfig($account2Id, 'GET', $suburl);
						$XeroOrderInfo	= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
						if(!$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']['0']){
							$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']	= array($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']);
						}
						
						foreach($totalFeeByNominal as $tempAmzNominal => $totalFeeByNominalLine){
							if($ConsolForceCurrencyMapping[$aggregationId]){
								$newExchangeRate		= $ConsolForceCurrencyMapping[$aggregationId];
								$totalFeeByNominalLine	= (($totalFeeByNominalLine) * ((1) / ($newExchangeRate)));
								$totalFeeByNominalLine	= sprintf("%.4f",$totalFeeByNominalLine);
							}
							
							$tempAmzNominalMapped		= '';
							if(isset($nominalMappings[$tempAmzNominal])){
								if($nominalMappings[$tempAmzNominal]['account2NominalId']){
									$tempAmzNominalMapped	= $nominalMappings[$tempAmzNominal]['account2NominalId'];
								}
							}
							if(($saleschannelId) AND (isset($nominalChannelMappings[strtolower($saleschannelId)][$tempAmzNominal]))){
								if($nominalChannelMappings[strtolower($saleschannelId)][$tempAmzNominal]['account2NominalId']){
									$tempAmzNominalMapped	= $nominalChannelMappings[strtolower($saleschannelId)][$tempAmzNominal]['account2NominalId'];
								}
							}
							
							$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'][count($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'])]	= array(
								'Description'	=> 'Amazon Fees',
								'Quantity' 		=> 1,
								'UnitAmount' 	=> (-1) * sprintf("%.4f",$totalFeeByNominalLine),
								'LineAmount' 	=> (-1) * sprintf("%.4f",$totalFeeByNominalLine),
								'AccountCode'	=> ($tempAmzNominalMapped) ? ($tempAmzNominalMapped) : ($config['amazonfeeAccountRef']),
								'TaxType' 		=> $config['salesNoTaxCode'],
							);
						}
						
						$EditRequest	= $XeroOrderInfo['Invoices']['Invoice'];
						$EditRequest['Contact']['Addresses']	= $EditRequest['Contact']['Addresses']['Address'];
						$EditRequest['Contact']['Phones']		= $EditRequest['Contact']['Phones']['Phone'];
						$EditRequest['LineItems']				= $EditRequest['LineItems']['LineItem'];
						foreach($EditRequest['LineItems'] as $LineKey => $EditRequestLineItems){
							if($EditRequestLineItems['Description'] == 'Amazon Fees'){
								if($trackingDetails){
									$EditRequest['LineItems'][$LineKey]['Tracking'][0]	= array(
										'Name'		=> $trackingDetails['0'],
										'Option'	=> $trackingDetails['1']
									);
								}
								continue;
							}
							if($EditRequestLineItems['Tracking']){
								foreach($EditRequestLineItems['Tracking'] as $TrackId => $AllTracking){
									unset($EditRequest['LineItems'][$LineKey]['Tracking'][$TrackId]);
									$EditRequest['LineItems'][$LineKey]['Tracking'][]	= array(
										'Name'			=> $AllTracking['Name'],
										'Option'		=> $AllTracking['Option'],
									);
									
								}
							}
							unset($EditRequest['LineItems'][$LineKey]['LineAmount']);
						}
						unset($EditRequest['SubTotal']);
						unset($EditRequest['TotalTax']);
						unset($EditRequest['Total']);
						unset($EditRequest['AmountDue']);
						unset($EditRequest['HasAttachments']);
						unset($EditRequest['HasErrors']);
						unset($EditRequest['SentToContact']);
						unset($EditRequest['Contact']['ContactPersons']);
						
						if($EditRequest){
							$this->headers	= array();
							$EditUrl		= '2.0/Invoices';
							if($accountDetails['OAuthVersion'] == '2'){
								$EditUrl	= '2.0/Invoices?unitdp=4';
								$this->initializeConfig($account2Id, 'POST', $EditUrl);
							}
							else{
								$EditUrl		= '2.0/Invoices';
								$UrlParams		= array('unitdp' => '4');
								$this->initializeConfig($account2Id, 'POST', $EditUrl,$UrlParams);
								$EditUrl		= '2.0/Invoices?unitdp='.urlencode('4');
							}
							$EditResponse		= $this->getCurl($EditUrl, 'POST', json_encode($EditRequest), 'json', $account2Id)[$account2Id];
							$createdRowData['Edit Invoice Request Data	: ']	= $EditRequest;
							$createdRowData['Edit Invoice Response Data	: ']	= $EditResponse;
							$AmazonAddedData	= array(
								'Amazon Edit Request	:'	=>	$EditRequest,
								'Amazon Edit Response	:'	=>	$EditResponse,
							);
							$this->ci->db->where_in('aggregationId',$aggregationId)->update('sales_order',array('createdRowData' => json_encode($createdRowData)));
							$this->ci->db->where_in('journalsId',$allAmazonfeesIDs)->update('amazon_ledger',array('createdParams' => json_encode($AmazonAddedData)));
							if((strtolower($EditResponse['Status']) == 'ok') AND (isset($EditResponse['Invoices']['Invoice']['InvoiceID']))){
								$EditSuccess	= 1;
								
								$this->ci->db->where_in('aggregationId',$aggregationId)->update('sales_order',array('createdRowData' => json_encode($createdRowData), 'status' => '1', 'message' => '', 'IsJournalposted' => 1, 'totalFeeAmount' => $totalFeeAmount));
								
								$this->ci->db->where_in('journalsId',$allAmazonfeesIDs)->update('amazon_ledger',array('status' => '1', 'createdJournalsId' => 'Linked with Invoice', 'message' => '', 'paymentCreated' => 1,'createdParams' => json_encode($AmazonAddedData)));
							}
						}
					}
				}
			}
		}
	}
}

foreach($this->accountDetails as $account2Id => $accountDetails){
	if($disableConsolSOpayments){
		continue;
	}
	
	$config	= $this->accountConfig[$account2Id];
	
	$this->ci->db->reset_query();
	$allAggregatedSales	= $this->ci->db->get_where('sales_order',array('isNetOff' => '0', 'account2Id' => $account2Id, 'status > ' => '0' , 'IsJournalposted' => '1' , 'sendInAggregation' => 1 ,'isPaymentCreated' => 0))->result_array();
	if(!$allAggregatedSales){
		continue;
	}
	
	$this->ci->db->reset_query();
	$allPendingFees	= array();
	$allSentFees	= array();
	$journalDatasTemps	= $this->ci->db->get_where('amazon_ledger',array('account2Id' => $account2Id))->result_array();
	if($journalDatasTemps){
		foreach($journalDatasTemps as $journalDatasTemp){
			if($journalDatasTemp['status'] == 0){
				$allPendingFees[$journalDatasTemp['orderId']]	= $journalDatasTemp;
			}
			elseif($journalDatasTemp['status'] == 1){
				$allSentFees[$journalDatasTemp['orderId']]		= $journalDatasTemp;
			}
		}
	}
	
	$enableAggregationAdvance		= $this->ci->globalConfig['enableAggregationAdvance'];
	$enableAggregationOnAPIfields	= $this->ci->globalConfig['enableAggregationOnAPIfields'];
	
	$this->ci->db->reset_query();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	$consolOnCustomActive		= 0;
	$consolOnAPIActive			= 0;
	if($AggregationMappingsTemps){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			
			if(!$ConsolMappingCustomField AND !$account1APIFieldId){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
			}
			else{
				if($account1APIFieldId){
					$consolOnAPIActive		= 1;
					$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
					foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
					}
				}
				else{
					$consolOnCustomActive	= 1;
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
				}
			}
		}
	}
	if(!$AggregationMappings AND !$AggregationMappings2){
		continue;
	}
	
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();
	$paymentMappings1		= array();
	$paymentMappings2		= array();
	$paymentMappings3		= array();
	if($paymentMappingsTemps){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}
			elseif((!empty($paymentchannelIds)) && (!$paymentcurrencys)){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif((!empty($paymentcurrencys)) AND (!$paymentchannelIds)){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}
			elseif((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	if($allAggregatedSales){
		$journalIds	= array();
		foreach($allAggregatedSales as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if(!$orderDatas['createOrderId']){
				continue;
			}
			$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
			if($paymentDetails){
				foreach($paymentDetails as $paymentKey => $paymentDetail){
					if($paymentDetail['sendPaymentTo'] == 'xero'){
						if($paymentDetail['status'] == '0'){
							if($paymentKey){
								$journalIds[]	= $paymentDetail['journalId'];
							}
						}
					}
				}
			}
		}
		$journalIds		= array_filter($journalIds);
		$journalIds		= array_unique($journalIds);
		sort($journalIds);	
		$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
		
		$ordersByAggregation		= array();
		$ConsolForceCurrencyMapping	= array();
		foreach($allAggregatedSales as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$orderExchangeRate		= $rowDatas['currency']['exchangeRate'];
			$CustomFieldValueID		= '';
			if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
				$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			}
			$ConsolAPIFieldValueID	= '';
				
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			
			if($enableAggregation){
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
						if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsJournalAggregated']){
								$ConsolForceCurrencyMapping[$orderDatas['aggregationId']]		= $orderExchangeRate;
								$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
							}						
						}
					}
					elseif($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsPaymentAggregated']){
						if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsJournalAggregated']){
								$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
							}						
						}
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsJournalAggregated']){
								$ConsolForceCurrencyMapping[$orderDatas['aggregationId']]		= $orderExchangeRate;
								$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
							}						
						}
					}
					elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
								$ConsolForceCurrencyMapping[$orderDatas['aggregationId']]		= $orderExchangeRate;
								$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
							}						
						}
					}
					elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsPaymentAggregated']){
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsJournalAggregated']){
								$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
							}						
						}
					}
					elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsJournalAggregated']){
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['disablePayments']){
							continue;
						}
						if($orderDatas['sendInAggregation']){
							if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsJournalAggregated']){
								$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
							}						
						}
					}
				}
			}
		}

		if($ordersByAggregation){
			foreach($ordersByAggregation as $aggregationId => $allorderDatas){
				$IsJournalPendings		= 0;
				$PaymentData			= array();
				$allSentIDS				= array();
				$request				= array();
				$Positiveamount			= 0;
				$Negativeamount			= 0;
				$sentableAmount			= 0;
				$aggregatedInvoiceTotal	= 0;
				$paymentDate			= date('Y-m-d');
				$accountCode			= $config['IncomeAccountRef'];
				$totalFeeAmount			= 0;
				foreach($allorderDatas as $orderId => $orderDatas){
					$orderId				= $orderDatas['orderId'];
					$XeroInvoiceId			= $orderDatas['createOrderId'];
					$account1Id				= $orderDatas['account1Id'];
					$config1				= $this->ci->account1Config[$account1Id];
					$rowDatas				= json_decode($orderDatas['rowData'],true);
					$createdRowData			= json_decode($orderDatas['createdRowData'],true);
					$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
					$aggregatedInvoiceTotal	= $createdRowData['Response data	: ']['Invoices']['Invoice']['Total'];
					$InvoiceNumber			= $createdRowData['Response data	: ']['Invoices']['Invoice']['InvoiceNumber'];
					$channelId				= $rowDatas['assignment']['current']['channelId'];
					$CurrencyRate			= 1;
					$CurrencyRateTotal		= 0;
					$countCurrencyRate		= 0;
					$totalFeeAmount			= $orderDatas['totalFeeAmount'];
					$aggregatedInvoiceTotal	= ($aggregatedInvoiceTotal - $totalFeeAmount);
					if(!$paymentDetails){
						continue;
					}
					if(($allPendingFees[$orderId]) OR (!$allSentFees[$orderId])){
						$IsJournalPendings	= 1;
					}					
					foreach($paymentDetails as $key => $paymentDetail){
						if($paymentDetail['sendPaymentTo'] == 'xero'){
							if(($paymentDetail['status'] == 0)){
								
								if($ConsolForceCurrencyMapping[$aggregationId]){
									$newForceExchangeRate		= $ConsolForceCurrencyMapping[$aggregationId];
									$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($newForceExchangeRate)));
									$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
								}
								
								if(($paymentDetail['paymentType'] == 'RECEIPT') OR ($paymentDetail['paymentType'] == 'CAPTURE')){
									if(@$paymentDetail['paymentDate']){
										$paymentDate	= $paymentDetail['paymentDate'];
										$BPDateOffset	= (int)substr($paymentDate,23,3);
										$xeroOffset		= 0;
										$diff			= 0;
										$diff			= $BPDateOffset - $xeroOffset;
										$date			= new DateTime($paymentDate);
										$BPTimeZone		= 'GMT';
										$date->setTimezone(new DateTimeZone($BPTimeZone));
										if($diff){
											$diff			.= ' hour';
											$date->modify($diff);
										}
										$paymentDate	= $date->format('Y-m-d');
									}
									
									if(isset($paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])])){
										$accountCode	= $paymentMappings1[$channelId][strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									elseif(isset($paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])])){
										$accountCode	= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									elseif(isset($paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])])){
										$accountCode	= $paymentMappings3[strtolower($paymentDetail['currency'])][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									elseif(isset($paymentMappings[strtolower($paymentDetail['paymentMethod'])])){
										$accountCode	= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									
									if(isset($journalDatas[$paymentDetail['journalId']])){
										$CurrencyRate		= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
										if($journalDatas[$paymentDetail['journalId']]['exchangeRate']){
											$CurrencyRateTotal	+= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
											$countCurrencyRate++;
										}
									}
									if($paymentDetail['amount'] > 0){
										$allSentIDS[]	= $key;
										$Positiveamount	+= $paymentDetail['amount'];
									}
									else{
										continue;
									}
								}
								elseif($paymentDetail['paymentType'] == 'PAYMENT'){
									$allSentIDS[]	= $key;
									$Negativeamount	+= abs($paymentDetail['amount']);
								}
							}
						}
					}
				}
				if($IsJournalPendings){
					continue;
				}
				$this->headers	= array();
				$suburl			= '2.0/Invoices/'.$XeroInvoiceId;
				if($accountDetails['OAuthVersion'] == '2'){
					$suburl		= '2.0/Invoices?IDs='.$XeroInvoiceId.'&unitdp=4';
				}
				$this->initializeConfig($account2Id, 'GET', $suburl);
				$XeroOrderInfo	= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
				$newFetch	= 0;
				if(isset($XeroOrderInfo['Invoices']['Invoice']['Total'])){
					$aggregatedInvoiceTotal	= $XeroOrderInfo['Invoices']['Invoice']['Total'];
					$newFetch	= 1;
				}
				if($newFetch){
					$aggregatedInvoiceTotal	= $aggregatedInvoiceTotal;
				}
				else{
					$aggregatedInvoiceTotal	= ($aggregatedInvoiceTotal - $totalFeeAmount);
				}
				if($Positiveamount	> 0){
					$sentableAmount	= $Positiveamount;
					if($Negativeamount	> 0){
						$sentableAmount	= $sentableAmount - $Negativeamount;
					}
					if($totalFeeAmount	> 0){
						$sentableAmount	= $sentableAmount - $totalFeeAmount;
					}
					if($sentableAmount){
						$sentableAmount			= sprintf("%.4f",$sentableAmount);
						$aggregatedInvoiceTotal	= sprintf("%.4f",$aggregatedInvoiceTotal);
						
						if(sprintf("%.4f",($sentableAmount)) > sprintf("%.4f",($aggregatedInvoiceTotal))){
							continue;
						}
						if(!$accountCode){continue;}
						if(!$XeroInvoiceId){continue;}
						if($CurrencyRateTotal){
							$CurrencyRate	= ($CurrencyRateTotal / $countCurrencyRate);
						}
						$ReferenceNumber	= 'PMT-'.$InvoiceNumber;
						$request			= array(
							'Invoice'		=> array('InvoiceID' => $XeroInvoiceId),
							'Account'		=> array('Code' => $accountCode),
							'Date'			=> $paymentDate,
							'CurrencyRate'	=> $CurrencyRate,
							'Reference'		=> $ReferenceNumber,
							'Amount'		=> $sentableAmount,
						);
						if($ConsolForceCurrencyMapping[$aggregationId]){
							unset($request['CurrencyRate']);
						}
					}
					if($request){
						$this->headers	= array();
						$url			= '2.0/Payments';
						$this->initializeConfig($account2Id, 'PUT', $url);
						$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Xero Payment Request	: ']	= $request;
						$createdRowData['Xero Payment Response	: ']	= $results;
						$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('aggregationId' => $aggregationId));
						if((strtolower($results['Status']) == 'ok') AND (isset($results['Payments']['Payment']['PaymentID']))){
							foreach($allorderDatas as $orderId => $orderDatas){
								$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
								foreach($paymentDetails as $key => $paymentDetail){
									if(in_array($key,$allSentIDS)){
										$paymentDetails[$key]['status']	= '1';
									}
								}
								$paymentDetails[$results['Payments']['Payment']['PaymentID']]	= array(
									"amount" 				=> $sentableAmount,
									'status'				=> '1',
									'AmountCreditedIn'		=> 'xero',
									'DeletedonBrightpearl'	=> 'NO',
									'paymentMethod'			=> $accountCode,
									'aggregatedPaymentID'	=> $results['Payments']['Payment']['PaymentID'],
								);
								$updateArray		= array();
								$AmountDueOnXero	= $results['Payments']['Payment']['Invoice']['AmountDue'];
								if($AmountDueOnXero == 0){
									$updateArray	= array(
										'isPaymentCreated'	=> '1',
										'status'			=> '3',
										'paymentStatus'		=> '1',
									);
								}
								$updateArray['paymentDetails']	= json_encode($paymentDetails);
								$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
							}
						}
					}
				}
			}
		}
	}
}
?>