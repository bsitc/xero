<?php
//XERO : BRIGHTPEARL INTEGRAION CONSOLIDATED SALESORDER'S PAYMENT POSTING
$this->reInitialize();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($this->ci->globalConfig['disableConsolSOpayments']){continue;}
	$config	= $this->accountConfig[$account2Id];
	
	$AllAggregatedBatchIds	= array();
	$this->ci->db->reset_query();
	$AllAggregatedBatchData	= $this->ci->db->select('DISTINCT createOrderId',false)->get_where('sales_order',array('isNetOff' => '0', 'isPaymentCreated' => '0','status >=' => '1','account2Id' => $account2Id,'sendInAggregation' => 1))->result_array();
	if($AllAggregatedBatchData){
		$allXeroIds	= array();
		foreach($AllAggregatedBatchData as $AllAggregatedBatchDataTemp){
			$allXeroIds[]	= $AllAggregatedBatchDataTemp['createOrderId'];
		}
		$AllAggregatedBatchIds	= array_chunk($allXeroIds,10);
	}
	
	if($AllAggregatedBatchIds){
		foreach($AllAggregatedBatchIds as $ConsolBatchIDs){
			$this->ci->db->reset_query();
			$datas	= $this->ci->db->order_by('orderId','desc')->where_in('createOrderId', $ConsolBatchIDs)->select('id,account1Id,account2Id,orderId,createOrderId,paymentDetails,status,rowData,sendInAggregation,aggregationId')->get_where('sales_order',array('paymentDetails<>' => '', 'isPaymentCreated' => '0','status >=' => '1','account2Id' => $account2Id,'sendInAggregation' => 1, 'isNetOff' => '0'))->result_array();
			if(!$datas){
				continue;
			}
			
			/* $this->ci->db->reset_query();
			$paymentMappingsTemps		= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
			$paymentMappings			= array();
			if($paymentMappingsTemps){
				foreach($paymentMappingsTemps as $paymentMappingsTemp){
					$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
					$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
				}
			} */
			// added by Arzoo on 19 march 2024
			$this->ci->db->reset_query();
			$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
			$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
			if($paymentMappingsTemps){
				foreach($paymentMappingsTemps as $paymentMappingsTemp){
					$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
					$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
					$paymentchannelIds	= array_filter($paymentchannelIds);
					$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
					$paymentcurrencys	= array_filter($paymentcurrencys);
					if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
						foreach($paymentchannelIds as $paymentchannelId){
							foreach($paymentcurrencys as $paymentcurrency){
								$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
							}
						}
					}else if((!empty($paymentchannelIds)) && (!$paymentcurrencys)){
						foreach($paymentchannelIds as $paymentchannelId){
							$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
						}
					}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
						foreach($paymentcurrencys as $paymentcurrency){
							$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
						}
					}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
						$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}
			if($this->ci->globalConfig['enableAggregation']){
				$enableAggregationAdvance		= $this->ci->globalConfig['enableAggregationAdvance'];
				$enableAggregationOnAPIfields	= $this->ci->globalConfig['enableAggregationOnAPIfields'];
				
				$this->ci->db->reset_query();
				$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
				$AggregationMappings		= array();
				$AggregationMappings2		= array();
				$consolOnCustomActive		= 0;
				$consolOnAPIActive			= 0;
				if($AggregationMappingsTemps){
					foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
						$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
						$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
						$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
						$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
						
						if(!$ConsolMappingCustomField AND !$account1APIFieldId){
							$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
						}
						else{
							if($account1APIFieldId){
								$consolOnAPIActive		= 1;
								$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
								foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
									$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
								}
							}
							else{
								$consolOnCustomActive	= 1;
								$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
							}
						}
					}
				}
				if(!$AggregationMappings AND !$AggregationMappings2){
					continue;
				}
			}
			else{
				continue;
			}
		
			if($datas){
				$journalIds	= array();
				foreach($datas as $orderDatas){
					if($orderDatas['status'] == 4){continue;}
					if(!$orderDatas['createOrderId']){continue;}
					
					$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
					if($paymentDetails){
						foreach($paymentDetails as $paymentKey => $paymentDetail){
							if($paymentDetail['sendPaymentTo'] == 'xero'){
								if($paymentDetail['status'] == '0'){
									if($paymentKey){
										$journalIds[]	= $paymentDetail['journalId'];
									}
								}
							}
						}
					}
				}
				$journalIds		= array_filter($journalIds);
				$journalIds		= array_unique($journalIds);
				sort($journalIds);
				$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
	
				$ordersByAggregation	= array();
				foreach($datas as $orderDatas){
					if($orderDatas['status'] == 4){continue;}
					if(!$orderDatas['createOrderId']){continue;}
					
					$account1Id				= $orderDatas['account1Id'];
					$bpconfig				= $this->ci->account1Config[$account1Id];
					$orderId				= $orderDatas['orderId'];
					$rowDatas				= json_decode($orderDatas['rowData'],true);
					$channelId				= $rowDatas['assignment']['current']['channelId'];
					$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
					$sendInAggregation		= $orderDatas['sendInAggregation'];
					$CustomFieldValueID		= '';
					if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
						$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
					}
					$ConsolAPIFieldValueID	= '';
					
					if($this->ci->globalConfig['enableAggregationOnAPIfields']){
						$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
						$APIfieldValueTmps		= '';
						foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
							if(!$APIfieldValueTmps){
								$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
							}
							else{
								$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
							}
						}
						if($APIfieldValueTmps){
							$ConsolAPIFieldValueID	= $APIfieldValueTmps;
						}
					}
					if($ConsolAPIFieldValueID){
						$CustomFieldValueID	= $ConsolAPIFieldValueID;
					}
					
					if($this->ci->globalConfig['enableAggregation']){
						if($AggregationMappings){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
								continue;
							}
							if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsJournalAggregated']){
								continue;
							}
							if($AggregationMappings[$channelId][$orderCurrencyCode]['disablePayments']){
								continue;
							}
							if($AggregationMappings[$channelId][$orderCurrencyCode]['IsJournalAggregated']){
								continue;
							}
							if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
								if($sendInAggregation){
									$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
								}
								else{
									continue;
								}
							}
							elseif($AggregationMappings[$channelId][$orderCurrencyCode]['IsPaymentAggregated']){
								if($sendInAggregation){
									$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
						elseif($AggregationMappings2){
							if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
								continue;
							}
							if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsJournalAggregated']){
								continue;
							}
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
								continue;
							}
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
								continue;
							}
							if($AggregationMappings2[$channelId][$orderCurrencyCode][$CustomFieldValueID]['disablePayments']){
								continue;
							}
							if($AggregationMappings2[$channelId][$orderCurrencyCode][$CustomFieldValueID]['IsJournalAggregated']){
								continue;
							}
							if($AggregationMappings2[$channelId][$orderCurrencyCode]['NA']['disablePayments']){
								continue;
							}
							if($AggregationMappings2[$channelId][$orderCurrencyCode]['NA']['IsJournalAggregated']){
								continue;
							}
							if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']) OR ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsPaymentAggregated'])){
								if($sendInAggregation){
									$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
								}
								else{
									continue;
								}
							}
							elseif(($AggregationMappings2[$channelId][$orderCurrencyCode][$CustomFieldValueID]['IsPaymentAggregated']) OR ($AggregationMappings2[$channelId][$orderCurrencyCode]['NA']['IsPaymentAggregated'])){
								if($sendInAggregation){
									$ordersByAggregation[$orderDatas['aggregationId']][$orderId]	= $orderDatas;
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
					}
				}
				
				if($ordersByAggregation){
					foreach($ordersByAggregation as $aggregationId => $allorderDatas){
						$PaymentData			= array();
						$allSentIDS				= array();
						$allGiftSentIDS			= array();
						$createdRowData			= array();
						$GiftCardAdded			= 0;
						$GiftAmount				= 0;
						$NegativeGiftAmount		= 0;
						$Positiveamount			= 0;
						$Negativeamount			= 0;
						$sentableAmount			= 0;
						$aggregatedInvoiceTotal	= 0;
						$paymentDate			= date('Y-m-d');
						$breakLoop1				= 0;
						$orderForceCurrency		= 0;
						$ConsolMappingData		= array();
						$allPaidOrdersBPIds		= array();
						$allOrdersPaymentInDB	= array();
						$allOrdersTotalAmtInBP	= array();
						$allOrdersSkippedIds	= array();
						$XeroInvoiceId			= '';
						$channelId				= '';
						
						$this->ci->db->reset_query();
						$createdRowDataFromXero	= $this->ci->db->select('createdRowData')->get_where('sales_order',array('aggregationId' => $aggregationId))->row_array();
						if($createdRowDataFromXero){
							$createdRowData		= json_decode($createdRowDataFromXero['createdRowData'],true);
						}
						
						foreach($allorderDatas as $orderId => $orderDatas){
							$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
							$rowDatas				= json_decode($orderDatas['rowData'],true);
							
							if($clientcode == 'oskarswoodenarkxerom'){
								$invoiceDate	= $rowDatas['invoices'][0]['taxDate'];
								$BPDateOffset	= substr($invoiceDate,23,6);
								$BPDateOffset	= explode(":",$BPDateOffset);
								$tempHours		= (int)$BPDateOffset[0];
								$tempMinutes	= (int)$BPDateOffset[1];
								$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
								if($tempHours < 0){
									$totalMinutes = (-1) * $totalMinutes;
								}
								$date			= new DateTime($invoiceDate);
								$BPTimeZone		= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($totalMinutes){
									$totalMinutes	.= ' minute';
									$date->modify($totalMinutes);
								}
								$invoiceDate		= $date->format('Y-m-d');
							}
							else{
								$invoiceDate			= $rowDatas['invoices'][0]['taxDate'];
								$BPDateOffset			= (int)substr($invoiceDate,23,3);
								$Acc2Offset				= 0;
								$diff					= $BPDateOffset - $Acc2Offset;
								$date					= new DateTime($invoiceDate);
								$BPTimeZone				= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff){
									$diff	.= ' hour';
									$date->modify($diff);
								}
								$invoiceDate			= $date->format('Y-m-d');
							}
							
							
							$totalOrderAmtInBase	= $rowDatas['totalValue']['baseTotal'];
							if(!$paymentDetails){
								$allOrdersSkippedIds[]	= $orderId;
								continue;
							}
							if($orderDatas['isPaymentCreated']){
								$allOrdersSkippedIds[]	= $orderId;
								continue;
							}
							if($orderDatas['status'] == 4){
								$allOrdersSkippedIds[]	= $orderId;
								continue;
							}
							
							if(!$ConsolMappingData){
								$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
								$channelId				= $rowDatas['assignment']['current']['channelId'];
								$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
								$CustomFieldValueID		= '';
								if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
									$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
								}
								
								
								$ConsolAPIFieldValueID	= '';
					
								if($this->ci->globalConfig['enableAggregationOnAPIfields']){
									$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
									$APIfieldValueTmps		= '';
									foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
										if(!$APIfieldValueTmps){
											$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
										}
										else{
											$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
										}
									}
									if($APIfieldValueTmps){
										$ConsolAPIFieldValueID	= $APIfieldValueTmps;
									}
								}
								if($ConsolAPIFieldValueID){
									$CustomFieldValueID	= $ConsolAPIFieldValueID;
								}
								
								
								if($AggregationMappings){
									if($AggregationMappings[$channelId]['bpaccountingcurrency']){
										$ConsolMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
									}
									elseif($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]){
										$ConsolMappingData	= $AggregationMappings[$channelId][strtolower($orderCurrencyCode)];
									}
								}
								elseif($AggregationMappings2){
									if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
										if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
											$ConsolMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
										}
										elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']){
											$ConsolMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'];
										}
									}
									elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]){
										if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]){
											$ConsolMappingData	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID];
										}
										elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']){
											$ConsolMappingData	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA'];
										}
									}
								}
							}
							if(!$ConsolMappingData){
								break;
							}
							$orderForceCurrency	= $ConsolMappingData['orderForceCurrency'];
							
							foreach($paymentDetails as $key => $paymentDetail){
								if(($paymentDetail['sendPaymentTo'] == 'xero') AND ($paymentDetail['amount'] != 0)){
									if($allOrdersPaymentInDB[$orderId]){
										$allOrdersPaymentInDB[$orderId]['receiveAmt']	+= $paymentDetail['amount'];
									}
									else{
										$allOrdersPaymentInDB[$orderId]['receiveAmt']	= $paymentDetail['amount'];
									}
								}
							}
							$allOrdersTotalAmtInBP[$orderId]['totalAmt']	= $rowDatas['totalValue']['total'];
							
							$orderId						= $orderDatas['orderId'];
							$createOrderId					= $orderDatas['createOrderId'];
							$XeroInvoiceId					= $createOrderId;
							$account1Id						= $orderDatas['account1Id'];
							$config1						= $this->ci->account1Config[$account1Id];
							//$GiftCardPaymentMethod		= $config1['giftCardPayment'];
							$GiftCardPaymentMethod			= '';
							
							$OrderTotalPositivePayment		= array();
							$OrderTotalNegativePayment		= array();
							$OrderTotalAdjustmentPayment	= array();
							$OrderPaymentMethod				= '';
							$OrderPaymentDate				= date('Y-m-d');
							$OrderPayemntCurrencyRate		= 1;
							$OrderPayemntCurrency			= '';
							$paymentSetCurrency				= '';
							$totalAmtInBaseInDB				= 0;
							
							foreach($paymentDetails as $key => $paymentDetail){
								if($orderForceCurrency){
									$differenceInNetAmt			= 0;
									$absdifferenceInNetAmt		= 0;
									$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($rowDatas['currency']['exchangeRate'])));
									$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
									if($paymentDetail['sendPaymentTo'] == 'xero'){
										$totalAmtInBaseInDB		= ($totalAmtInBaseInDB + $paymentDetail['amount']);
									}
									if($totalOrderAmtInBase AND $totalAmtInBaseInDB){
										if($totalOrderAmtInBase != $totalAmtInBaseInDB){
											$differenceInNetAmt		= ($totalOrderAmtInBase - $totalAmtInBaseInDB);
											$absdifferenceInNetAmt	= abs($differenceInNetAmt);
											if(($absdifferenceInNetAmt) AND ($absdifferenceInNetAmt < 1)){
												if($totalOrderAmtInBase > $totalAmtInBaseInDB){
													$paymentDetail['amount']	= ($paymentDetail['amount'] + $differenceInNetAmt);
												}
												else{
													$paymentDetail['amount']	= ($paymentDetail['amount'] - $absdifferenceInNetAmt);
												}
											}
										}
									}
								}
								
								if($paymentDetail['sendPaymentTo'] == 'xero'){
									if(($paymentDetail['status'] == 0)){
										if(($clientcode == 'popcultchaxero') OR ($clientcode == 'mockaxero') OR ($clientcode == 'mockaxeronz')  OR ($clientcode == 'metierxero')){
											$paymentDate	= $invoiceDate;
										}
										else{
											if($paymentDetail['paymentDate']){
												$paymentDate	= $paymentDetail['paymentDate'];
												
												if($clientcode == 'oskarswoodenarkxerom'){
													$BPDateOffset	= substr($paymentDate,23,6);
													$BPDateOffset	= explode(":",$BPDateOffset);
													$tempHours		= (int)$BPDateOffset[0];
													$tempMinutes	= (int)$BPDateOffset[1];
													$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
													if($tempHours < 0){
														$totalMinutes = (-1) * $totalMinutes;
													}
													$date			= new DateTime($paymentDate);
													$BPTimeZone		= 'GMT';
													$date->setTimezone(new DateTimeZone($BPTimeZone));
													if($totalMinutes){
														$totalMinutes	.= ' minute';
														$date->modify($totalMinutes);
													}
													$paymentDate		= $date->format('Y-m-d');
												}
												else{
													$BPDateOffset	= (int)substr($paymentDate,23,3);
													$xeroOffset		= 0;
													$diff			= 0;
													$diff			= $BPDateOffset - $xeroOffset;
													$date			= new DateTime($paymentDate);
													$BPTimeZone		= 'GMT';
													$date->setTimezone(new DateTimeZone($BPTimeZone));
													if($diff){
														$diff			.= ' hour';
														$date->modify($diff);
													}
													$paymentDate	= $date->format('Y-m-d');
												}
											}
										}
										if(($paymentDetail['paymentType'] == 'RECEIPT') OR ($paymentDetail['paymentType'] == 'CAPTURE')){
											if(isset($journalDatas[$paymentDetail['journalId']])){
												if($paymentDetail['paymentMethod'] != $GiftCardPaymentMethod){
													$OrderPayemntCurrencyRate	= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
												}
											}
											if($paymentDetail['paymentMethod']){
												if($paymentDetail['paymentMethod'] == $GiftCardPaymentMethod){
													if($paymentDetail['amount'] > 0){
														$GiftAmount			+=	$paymentDetail['amount'];
														$allGiftSentIDS[]	= $key;
													}
												}
												else{
													if($paymentDetail['amount'] > 0){
														$paymentSetCurrency			= $paymentDetail['currency'];
														$OrderPayemntCurrency		= $paymentDetail['currency'];
														$OrderPaymentMethod			= $paymentDetail['paymentMethod'];
														$OrderPaymentDate			= $paymentDate;
														if(strtolower($OrderPaymentMethod) == 'other'){
															$parentOrderId	= $rowDatas['parentOrderId'];
															if($parentOrderId){
																$parentOrderPaymentDatas		= array();
																$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
																if($parentOrderPaymentDataTemp){
																	$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
																}
																if($parentOrderPaymentDatas){
																	foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
																		if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
																			if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
																				$OrderPaymentMethod	= $parentOrderPaymentData['paymentMethod'];
																			}
																		}
																	}
																}
															}
														}
														if(isset($OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT'])){
															$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	+= $paymentDetail['amount'];
														}
														else{
															$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	= $paymentDetail['amount'];
														}
														$allSentIDS[]	= $key;
														$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['Ids'][]		= $key;
														$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['Currency']	= $OrderPayemntCurrency;
														$OrderTotalPositivePayment[$OrderPaymentMethod][$OrderPaymentDate]['Erate']		= $OrderPayemntCurrencyRate;
													}
												}
											}
										}
										elseif($paymentDetail['paymentType'] == 'PAYMENT'){
											$OrderPaymentMethod	= $paymentDetail['paymentMethod'];
											$OrderPaymentDate	= $paymentDate;
											if($OrderPaymentMethod == $GiftCardPaymentMethod){
												$NegativeGiftAmount						+=	abs($paymentDetail['amount']);
												$allGiftSentIDS[]						= $key;
											}
											elseif($OrderPaymentMethod == 'ADJUSTMENT'){
												$allSentIDS[]	= $key;
												if(isset($OrderTotalAdjustmentPayment['AMT'])){
													$OrderTotalAdjustmentPayment['AMT']		+=	abs($paymentDetail['amount']);
												}
												else{
													$OrderTotalAdjustmentPayment['AMT']		=	abs($paymentDetail['amount']);
												}
												$OrderTotalAdjustmentPayment['Ids'][]	= $key;
											}
											else{
												$allSentIDS[]	= $key;
												if(strtolower($OrderPaymentMethod) == 'other'){
													$parentOrderId	= $rowDatas['parentOrderId'];
													if($parentOrderId){
														$parentOrderPaymentDatas		= array();
														$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
														if($parentOrderPaymentDataTemp){
															$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
														}
														if($parentOrderPaymentDatas){
															foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
																if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
																	if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
																		$OrderPaymentMethod	= $parentOrderPaymentData['paymentMethod'];
																	}
																}
															}
														}
													}
												}
												
												if(isset($OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT'])){
													$OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	+= abs($paymentDetail['amount']);
												}
												else{
													$OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['AMT']	= abs($paymentDetail['amount']);
												}
												$OrderTotalNegativePayment[$OrderPaymentMethod][$OrderPaymentDate]['Ids'][]		= $key;
											}
										}
										else{
											continue;
										}
									}
								}
							}
							
							if($OrderTotalAdjustmentPayment AND $OrderTotalPositivePayment AND $OrderTotalNegativePayment){
								$allPostiveAmt	= 0;
								$allNegativeAmt	= 0;
								$OneRate		= 1;
								$OneCurrency	= '';
								$OneMethod		= '';
								$OneDate		= date('Y-m-d');
								$AllPIds		= array();
								$AllNIds		= array();
								$AllAIds		= array();
								
								foreach($OrderTotalNegativePayment as $Method => $DataWithMethod){
									foreach($DataWithMethod as $Date => $DataWithMethodDate){
										$NewAmt	= 0;
										if($OrderTotalPositivePayment[$Method][$Date] AND ($OrderTotalPositivePayment[$Method][$Date]['AMT'] >= $DataWithMethodDate['AMT'])){
											$NewAmt	= ($OrderTotalPositivePayment[$Method][$Date]['AMT'] - $DataWithMethodDate['AMT']);
											$OrderTotalPositivePayment[$Method][$Date]['AMT']	= $NewAmt;
											$OrderTotalPositivePayment[$Method][$Date]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$Method][$Date]['Ids']);
											unset($OrderTotalNegativePayment[$Method][$Date]);
										}
										else{
											foreach($OrderTotalPositivePayment as $pMethod => $OrderTotalPositivePaymentData){
												$isFound	= 0;
												if($Method  == $pMethod){
													foreach($OrderTotalPositivePaymentData as $pDate => $OrderTotalPositivePaymentDataTemps){
														if($OrderTotalPositivePaymentDataTemps['AMT'] >= $DataWithMethodDate['AMT']){
															$isFound	= 1;
															$NewAmt		= ($OrderTotalPositivePaymentDataTemps['AMT'] - $DataWithMethodDate['AMT']);
															$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= $NewAmt;
															$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
															unset($OrderTotalNegativePayment[$Method][$Date]);
															break;
														}
													}
													if($isFound){
														break;
													}
												}
											}
										}
									}
								}
								
								if($OrderTotalNegativePayment){
									foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
										if(!$dataWithMethod){
											unset($OrderTotalNegativePayment[$Method]);
										}
									}
									if($OrderTotalNegativePayment){
										foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
											foreach($dataWithMethod as $Date => $dataWithMethodDate){
												$isFound2	= 0;
												foreach($OrderTotalPositivePayment as $pMethod => $MethodData){
													foreach($MethodData as $pDate => $MethodDateData){
														if($MethodDateData['AMT'] >= $dataWithMethodDate['AMT']){
															$isFound2	= 1;
															$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= ($MethodDateData['AMT'] - $dataWithMethodDate['AMT']);
															$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($dataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
															unset($OrderTotalNegativePayment[$Method][$Date]);
															break;
														}
													}
													if($isFound2){
														break;
													}
												}
											}
										}
									}
								}
								
								foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
									foreach($DataWithMethod as $Date => $DataWithMethodDate){
										$NewAmt	= 0;
										if((isset($OrderTotalAdjustmentPayment['AMT'])) AND ($DataWithMethodDate['AMT'] >= $OrderTotalAdjustmentPayment['AMT'])){
											$NewAmt	= $DataWithMethodDate['AMT'] - $OrderTotalAdjustmentPayment['AMT'];
											if(isset($PaymentData[$Method][$Date]['AMT'])){
												$PaymentData[$Method][$Date]['AMT']				+= $NewAmt;
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
												$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
												unset($OrderTotalAdjustmentPayment);
											}
											else{
												$PaymentData[$Method][$Date]['AMT']				= $NewAmt;
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
												$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
												unset($OrderTotalAdjustmentPayment);
											}
										}
										else{
											if(isset($PaymentData[$Method][$Date]['AMT'])){
												$PaymentData[$Method][$Date]['AMT']				+= $DataWithMethodDate['AMT'];
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
											}
											else{
												$PaymentData[$Method][$Date]['AMT']				= $DataWithMethodDate['AMT'];
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
											}
										}
									}
								}
							}
							elseif($OrderTotalAdjustmentPayment AND $OrderTotalPositivePayment AND !$OrderTotalNegativePayment){
								$allPostiveAmt	= 0;
								$allNegativeAmt	= 0;
								$OneRate		= 1;
								$OneCurrency	= '';
								$OneMethod		= '';
								$OneDate		= date('Y-m-d');
								$AllPIds		= array();
								$AllNIds		= array();
								$AllAIds		= array();
								foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
									foreach($DataWithMethod as $Date => $DataWithMethodDate){
										$NewAmt	= 0;
										if((isset($OrderTotalAdjustmentPayment['AMT'])) AND ($DataWithMethodDate['AMT'] >= $OrderTotalAdjustmentPayment['AMT'])){
											$NewAmt	= $DataWithMethodDate['AMT'] - $OrderTotalAdjustmentPayment['AMT'];
											if(isset($PaymentData[$Method][$Date]['AMT'])){
												$PaymentData[$Method][$Date]['AMT']				+= $NewAmt;
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
												$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
												unset($OrderTotalAdjustmentPayment);
											}
											else{
												$PaymentData[$Method][$Date]['AMT']				= $NewAmt;
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
												$PaymentData[$Method][$Date]['Ids'][]			= $OrderTotalAdjustmentPayment['Ids'];
												unset($OrderTotalAdjustmentPayment);
											}
										}
										else{
											if(isset($PaymentData[$Method][$Date]['AMT'])){
												$PaymentData[$Method][$Date]['AMT']				+= $DataWithMethodDate['AMT'];
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
											}
											else{
												$PaymentData[$Method][$Date]['AMT']				= $DataWithMethodDate['AMT'];
												$PaymentData[$Method][$Date]['Erate']			= $DataWithMethodDate['Erate'];
												$PaymentData[$Method][$Date]['Currency']		= $DataWithMethodDate['Currency'];
												$PaymentData[$Method][$Date]['Ids'][]			= $DataWithMethodDate['Ids'];
											}
										}
									}
								}
							}
							elseif($OrderTotalNegativePayment AND $OrderTotalPositivePayment AND !$OrderTotalAdjustmentPayment){
								$allPostiveAmt	= array();
								$allNegativeAmt	= array();
								$OneRate		= 1;
								$OneCurrency	= '';
								$OneMethod		= '';
								$OneDate		= date('Y-m-d');
								$AllPIds		= array();
								$AllNIds		= array();
								$AllAIds		= array();
								
								foreach($OrderTotalNegativePayment as $Method => $DataWithMethod){
									foreach($DataWithMethod as $Date => $DataWithMethodDate){
										$NewAmt	= 0;
										if($OrderTotalPositivePayment[$Method][$Date] AND ($OrderTotalPositivePayment[$Method][$Date]['AMT'] >= $DataWithMethodDate['AMT'])){
											$NewAmt	= ($OrderTotalPositivePayment[$Method][$Date]['AMT'] - $DataWithMethodDate['AMT']);
											$OrderTotalPositivePayment[$Method][$Date]['AMT']	= $NewAmt;
											$OrderTotalPositivePayment[$Method][$Date]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$Method][$Date]['Ids']);
											unset($OrderTotalNegativePayment[$Method][$Date]);
										}
										else{
											foreach($OrderTotalPositivePayment as $pMethod => $OrderTotalPositivePaymentData){
												$isFound	= 0;
												if($Method  == $pMethod){
													foreach($OrderTotalPositivePaymentData as $pDate => $OrderTotalPositivePaymentDataTemps){
														if($OrderTotalPositivePaymentDataTemps['AMT'] >= $DataWithMethodDate['AMT']){
															$isFound	= 1;
															$NewAmt		= ($OrderTotalPositivePaymentDataTemps['AMT'] - $DataWithMethodDate['AMT']);
															$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= $NewAmt;
															$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($DataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
															unset($OrderTotalNegativePayment[$Method][$Date]);
															break;
														}
													}
													if($isFound){
														break;
													}
												}
											}
										}
									}
								}
								
								if($OrderTotalNegativePayment){
									foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
										if(!$dataWithMethod){
											unset($OrderTotalNegativePayment[$Method]);
										}
									}
									if($OrderTotalNegativePayment){
										foreach($OrderTotalNegativePayment as $Method => $dataWithMethod){
											foreach($dataWithMethod as $Date => $dataWithMethodDate){
												$isFound2	= 0;
												foreach($OrderTotalPositivePayment as $pMethod => $MethodData){
													foreach($MethodData as $pDate => $MethodDateData){
														if($MethodDateData['AMT'] >= $dataWithMethodDate['AMT']){
															$isFound2	= 1;
															$OrderTotalPositivePayment[$pMethod][$pDate]['AMT']	= ($MethodDateData['AMT'] - $dataWithMethodDate['AMT']);
															$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']	= array_merge($dataWithMethodDate['Ids'],$OrderTotalPositivePayment[$pMethod][$pDate]['Ids']);
															unset($OrderTotalNegativePayment[$Method][$Date]);
															break;
														}
													}
													if($isFound2){
														break;
													}
												}
											}
										}
									}
								}
								
								foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
									foreach($DataWithMethod as $Date => $DataWithMethodDate){
										if(isset($PaymentData[$Method][$Date]['AMT'])){
											$PaymentData[$Method][$Date]['AMT']			+= $DataWithMethodDate['AMT'];
											$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
											$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
											$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
										}
										else{
											$PaymentData[$Method][$Date]['AMT']			= $DataWithMethodDate['AMT'];
											$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
											$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
											$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
										}
									}
								}
							}
							elseif(!$OrderTotalNegativePayment AND $OrderTotalPositivePayment AND !$OrderTotalAdjustmentPayment){
								$AllPIds		= array();
								foreach($OrderTotalPositivePayment as $Method => $DataWithMethod){
									foreach($DataWithMethod as $Date => $DataWithMethodDate){
										if(isset($PaymentData[$Method][$Date]['AMT'])){
											$PaymentData[$Method][$Date]['AMT']			+= $DataWithMethodDate['AMT'];
											$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
											$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
											$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
										}
										else{
											$PaymentData[$Method][$Date]['AMT']			= $DataWithMethodDate['AMT'];
											$PaymentData[$Method][$Date]['Erate']		= $DataWithMethodDate['Erate'];
											$PaymentData[$Method][$Date]['Currency']	= $DataWithMethodDate['Currency'];
											$PaymentData[$Method][$Date]['Ids'][]		= $DataWithMethodDate['Ids'];
										}
									}
								}
							}
							else{
								if($GiftAmount OR ($rowDatas['totalValue']['total'] == 0)){
								}
								else{
									$allOrdersSkippedIds[]	= $orderId;
									continue;
									break;
								}
							}
							$allPaidOrdersBPIds[]	= $orderId;
						}
						if($breakLoop1){
							continue;
						}
						if($PaymentData OR $GiftAmount){
							$isGiftCardApplicable	= 0;
							$isGiftCardAdded		= 0;
							if($GiftAmount){
								$isGiftCardApplicable	= 1;
							}
							
							$aggregatedInvoiceNumber	= 'PMT-';
							if($XeroInvoiceId){
								$this->headers	= array();
								$suburl			= '2.0/Invoices/'.$XeroInvoiceId;
								if($accountDetails['OAuthVersion'] == '2'){
									$suburl		= '2.0/Invoices?IDs='.$XeroInvoiceId.'&unitdp=4';
								}
								$this->initializeConfig($account2Id, 'GET', $suburl);
								$XeroOrderInfo	= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
								if(isset($XeroOrderInfo['Invoices']['Invoice']['Total'])){
									$aggregatedInvoiceTotal		= $XeroOrderInfo['Invoices']['Invoice']['Total'];
									$aggregatedInvoiceNumber	.= $XeroOrderInfo['Invoices']['Invoice']['InvoiceNumber'];
								}
							}
							else{
								continue;
							}
							
							$sentableAmount			= 0;
							$AllSentableAmount		= 0;
							if($PaymentData){
								foreach($PaymentData as $PMethod => $PositiveamountData){
									foreach($PositiveamountData as $PDate => $PositiveamountDataTemp){
										$AllSentableAmount	+= $PositiveamountDataTemp['AMT'];
									}
								}
							}
							$sentableAmount	= $AllSentableAmount;
							if($GiftAmount){
								$sentableAmount	= ($sentableAmount + $GiftAmount);
							}
							if($NegativeGiftAmount){
								$sentableAmount	= ($sentableAmount - $NegativeGiftAmount);
							}
							if($aggregatedInvoiceTotal == 0){
								continue;
							}
							if(sprintf("%.4f",$sentableAmount) > sprintf("%.4f",$aggregatedInvoiceTotal)){
								continue;
							}
							$GiftOrderIds	= array();
							if($GiftAmount){
								$EditRequest	= array();
								
								$this->headers	= array();
								$url			= '2.0/Invoices/'.$XeroInvoiceId;
								if($accountDetails['OAuthVersion'] == '2'){
									$url		= '2.0/Invoices?IDs='.$XeroInvoiceId.'&unitdp=4';
								}
								$this->initializeConfig($account2Id, 'GET', $url);
								$XeroOrderInfo	= $this->getCurl($url, 'get', '', 'json', $account2Id)[$account2Id];
								if(strtolower($XeroOrderInfo['Status']) == 'ok'){
									if(isset($XeroOrderInfo['Invoices']['Invoice'])){
										if(!$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']['0']){
											$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']	= array($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']);
										}
										$SentableGiftAmt	= $GiftAmount;
										if($NegativeGiftAmount){
											$SentableGiftAmt	= ($SentableGiftAmt - $NegativeGiftAmount);
										}
										$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'][count($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'])]	= array(
											'ItemCode' 		=> $config['giftCardItem'],
											'Description'	=> 'Gift Card',
											'Quantity' 		=> 1,
											'UnitAmount' 	=> (-1) * sprintf("%.4f",$SentableGiftAmt),
											'LineAmount' 	=> (-1) * sprintf("%.4f",$SentableGiftAmt),
											'TaxType' 		=> $config['salesNoTaxCode'],
											'TaxAmount' 	=> 0.00,
										);
										$EditRequest	= $XeroOrderInfo['Invoices']['Invoice'];
										$EditRequest['Contact']['Addresses']	= $EditRequest['Contact']['Addresses']['Address'];
										$EditRequest['Contact']['Phones']		= $EditRequest['Contact']['Phones']['Phone'];
										$EditRequest['LineItems']				= $EditRequest['LineItems']['LineItem'];
										foreach($EditRequest['LineItems'] as $LineKey => $EditRequestLineItems){
											if($EditRequestLineItems['Tracking']){
												foreach($EditRequestLineItems['Tracking'] as $TrackId => $AllTracking){
													unset($EditRequest['LineItems'][$LineKey]['Tracking'][$TrackId]);
													$EditRequest['LineItems'][$LineKey]['Tracking'][]	= array(
														'Name'			=> $AllTracking['Name'],
														'Option'		=> $AllTracking['Option'],
													);
												}
											}
											unset($EditRequest['LineItems'][$LineKey]['LineAmount']);
										}
										unset($EditRequest['SubTotal']);
										unset($EditRequest['TotalTax']);
										unset($EditRequest['Total']);
										unset($EditRequest['Payments']);
										unset($EditRequest['AmountDue']);
										unset($EditRequest['HasAttachments']);
										unset($EditRequest['HasErrors']);
										unset($EditRequest['SentToContact']);
										unset($EditRequest['Contact']['ContactPersons']);
										if($EditRequest){
											$this->headers	= array();
											$EditUrl		= '2.0/Invoices';
											if($accountDetails['OAuthVersion'] == '2'){
												$EditUrl	= '2.0/Invoices?unitdp=4';
												$this->initializeConfig($account2Id, 'POST', $EditUrl);
											}
											else{
												$EditUrl	= '2.0/Invoices';
												$UrlParams	= array('unitdp' => '4');
												$this->initializeConfig($account2Id, 'POST', $EditUrl,$UrlParams);
												$EditUrl	= '2.0/Invoices?unitdp='.urlencode('4');
											}
											$EditResponse	= $this->getCurl($EditUrl, 'POST', json_encode($EditRequest), 'json', $account2Id)[$account2Id];
											$createdRowData['Gift Card Request  : '.$aggregationId]	= $EditRequest;
											$createdRowData['Gift Card Response : '.$aggregationId]	= $EditResponse;
											$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('aggregationId' => $aggregationId));
											if((strtolower($EditResponse['Status']) == 'ok') AND (isset($EditResponse['Invoices']['Invoice']['InvoiceID']))){
												$isGiftCardAdded	= 1;
												$dueAmount			= '';
												if(isset($EditResponse['Invoices']['Invoice']['AmountDue'])){
													$dueAmount	= $EditResponse['Invoices']['Invoice']['AmountDue'];
												}
												$GiftCardAdded		= 1;
												foreach($allorderDatas as $orderId => $orderDatas){
													$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
													foreach($paymentDetails as $key => $paymentDetail){
														if(in_array($key,$allGiftSentIDS)){
															$paymentDetails[$key]['status']	= '1';
														}
													}
													$paymentDetails[uniqid()]	= array(
														"giftamount"		=> $SentableGiftAmt,
														'status'			=> '1',
														'AmountCreditedIn'	=> 'Xero',
													);
													$updateArray	= array();
													$GiftOrderIds[$orderId]['paymentDetails']	= json_encode($paymentDetails);
													$allorderDatas[$orderId]['paymentDetails']	= json_encode($paymentDetails);
													$updateArray['paymentDetails']				= json_encode($paymentDetails);
													$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
												}
											}
										}
									}
								}
							}
							if($sentableAmount	> 0){
								if(!$isGiftCardAdded AND $isGiftCardApplicable){
									continue;
								}
								$paymentCounter	= 1;
								foreach($PaymentData as $Method => $PaymentDatasTemp){
									foreach($PaymentDatasTemp as $Date => $PaymentDatasTempData){
										$RequestAmt		= $PaymentDatasTempData['AMT'];
										$request		= array();
										$accountCode	= '';
										if($RequestAmt > 0){
											$allUpdatedIds	= array();
											foreach($PaymentDatasTempData['Ids'] as $IdsData){
												foreach($IdsData as $IdsDatas){
													$allUpdatedIds[]	= $IdsDatas;
												}
											}
											
											$allUpdatedIds	= array_filter($allUpdatedIds);
											$allUpdatedIds	= array_unique($allUpdatedIds);
											//$accountCode	= $paymentMappings[strtolower($Method)]['account2PaymentId'];
											if(isset($paymentMappings1[$channelId][strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)])){
												$accountCode		= $paymentMappings1[$channelId][strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)]['account2PaymentId'];
											}
											else if(isset($paymentMappings2[$channelId][strtolower($Method)])){
												$accountCode		= $paymentMappings2[$channelId][strtolower($Method)]['account2PaymentId'];
											}
											else if(isset($paymentMappings3[strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)])){
												$accountCode		= $paymentMappings3[strtolower($PaymentDatasTempData['Currency'])][strtolower($Method)]['account2PaymentId'];
											}
											else if(isset($paymentMappings[strtolower($Method)])){
												$accountCode		= $paymentMappings[strtolower($Method)]['account2PaymentId'];
											}
											if(!$XeroInvoiceId){continue;};
											if(!$accountCode){continue;};
											if(!$RequestAmt){continue;};
											$request		= array(
												'Invoice'			=> array('InvoiceID' => $XeroInvoiceId),
												'Account'			=> array('Code' => $accountCode),
												'Date'				=> $Date,
												'CurrencyRate'		=> $PaymentDatasTempData['Erate'],
												'Reference'			=> $aggregatedInvoiceNumber,
												'Amount'			=> $RequestAmt,
											);
											if($orderForceCurrency){
												$request['CurrencyRate']	= 1;
											}
											if($request){
												$this->headers	= array();
												$url			= '2.0/Payments';
												$this->initializeConfig($account2Id, 'PUT', $url);
												$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
												$createdRowData['Xero Payment Request	: '.$paymentCounter]	= $request;
												$createdRowData['Xero Payment Response	: '.$paymentCounter]	= $results;
												$paymentCounter++;
												$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('aggregationId' => $aggregationId));
												if((strtolower($results['Status']) == 'ok') AND (isset($results['Payments']['Payment']['PaymentID']))){
													$dueAmount		= '';
													if(isset($results['Payments']['Payment']['Invoice']['AmountDue'])){
														$dueAmount	= $results['Payments']['Payment']['Invoice']['AmountDue'];
													}
													foreach($allorderDatas as $orderId => $orderDatas){
														if($GiftCardAdded){
															$paymentDetails	= json_decode($GiftOrderIds[$orderId]['paymentDetails'],true);
														}
														else{
															$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
														}
														$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
														foreach($paymentDetails as $key => $paymentDetail){
															if(in_array($key,$allUpdatedIds)){
																$paymentDetails[$key]['status']	= '1';
															}
														}
														$paymentDetails[$results['Payments']['Payment']['PaymentID']]	= array(
															"amount" 				=> $RequestAmt,
															'status'				=> '1',
															'AmountCreditedIn'		=> 'xero',
															'DeletedonBrightpearl'	=> 'NO',
															'paymentMethod'			=> $accountCode,
															'aggregatedPaymentID'	=> $results['Payments']['Payment']['PaymentID'],
														);
														$updateArray	= array();
														if($GiftCardAdded){
															$GiftOrderIds[$orderId]['paymentDetails']	= json_encode($paymentDetails);
														}
														$allorderDatas[$orderId]['paymentDetails']		= json_encode($paymentDetails);
														$updateArray['paymentDetails']					= json_encode($paymentDetails);
														$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
													}
												}
											}
										}
									}
								}
							}
						
							$NewUpdateArray	= array();
							$batchUpdates	= array();
							foreach($allorderDatas as $orderId => $orderDatas){
								$paymentDetails		= array();
								$totalPaidAmt		= 0;
								if($GiftCardAdded){
									$paymentDetails	= json_decode($GiftOrderIds[$orderId]['paymentDetails'],true);
								}
								else{
									$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
								}
								foreach($paymentDetails as $key => $paymentDetail){
									if(($paymentDetail['sendPaymentTo'] == 'xero') AND ($paymentDetail['amount'] != 0) AND ($paymentDetail['status'] == 1)){
										$totalPaidAmt		= ($totalPaidAmt + $paymentDetail['amount']);
									}
								}
								if($totalPaidAmt AND $allOrdersTotalAmtInBP[$orderId]['totalAmt']){
									if($totalPaidAmt >= $allOrdersTotalAmtInBP[$orderId]['totalAmt']){
										$NewUpdateArray[$orderId]	= array(
											'orderId'			=> $orderId,
											'isPaymentCreated'	=> '1',
											'status' 			=> '3',
											'paymentStatus'		=> '1',
										);
									}
								}
							}
							if($NewUpdateArray){
								$batchUpdates	= array_chunk($NewUpdateArray,200);
								foreach($batchUpdates as $batchUpdate){
									if($batchUpdate){
										$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('sales_order',$batchUpdate,'orderId');
									}
								}
							}
						}
					}
				}
			}
		}
	}
}