<?php
$this->reInitialize();
$UnInvoicingEnabled	= 1;
$enableAvalaraTax	= 0;
$clientcode			= $this->ci->config->item('clientcode');
$dateLockSettings	= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	if($clientcode == 'amistry'){
		//kitKing company sales is disabled for now, account Id of kitKing is 2
		if($account2Id	== 2){
			continue;
		}
	}
	
	//doNotRemoveThisLine
	$forceClientCodesForInvLine	= array('hawkeopticsxero','hawkeusxero');
	$skuTrimmingClientCodes		= array('keenxero','bfxero','arten','logicofenglishxero','tinyexplorerxero','homeleisuredirectxero');
	
	$config	= $this->accountConfig[$account2Id];
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->order_by('orderId', 'asc')->get_where('sales_order',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$enableAggregationAdvance		= $this->ci->globalConfig['enableAggregationAdvance'];
	$enableAggregationOnAPIfields	= $this->ci->globalConfig['enableAggregationOnAPIfields'];
	
	$this->ci->db->reset_query();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	$consolOnCustomActive		= 0;
	$consolOnAPIActive			= 0;
	if($AggregationMappingsTemps){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			
			if(!$ConsolMappingCustomField AND !$account1APIFieldId){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
			}
			else{
				if($account1APIFieldId){
					$consolOnAPIActive		= 1;
					$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
					foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
					}
				}
				else{
					$consolOnCustomActive	= 1;
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$genericcustomerMappingsTemps		= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	$genericcustomerMappings			= array();
	$AllCustomFieldsForGenericMapping	= array();
	$GenericCustomerMappingAdvanced		= 0;
	if($genericcustomerMappingsTemps){
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$MappedChannel		= '';
			$MappedChannel		= $genericcustomerMappingsTemp['account1ChannelId'];
			
			if($this->ci->globalConfig['enableGenericCustomerMappingCustomazation']){
				$GenericCustomerMappingAdvanced		= 1;
				$brightpearlCustomFieldName			= trim(strtolower($genericcustomerMappingsTemp['brightpearlCustomFieldName']));
				$IncludedStringInCustomField		= explode("||",trim($genericcustomerMappingsTemp['IncludedStringInCustomField']));
				$AllCustomFieldsForGenericMapping[$brightpearlCustomFieldName]	= $brightpearlCustomFieldName; 
				if($IncludedStringInCustomField AND $brightpearlCustomFieldName){
					foreach($IncludedStringInCustomField as $IncludedStringInCustomFieldTemp){
						$IncludedDataString			= trim(strtolower($IncludedStringInCustomFieldTemp));
						$genericcustomerMappings[$MappedChannel][$brightpearlCustomFieldName][$IncludedDataString]	= $genericcustomerMappingsTemp;
					}
				}
				else{
					$genericcustomerMappings[$MappedChannel]['Blank']	= $genericcustomerMappingsTemp;
				}
			}
			else{
				$genericcustomerMappings[$MappedChannel]	= $genericcustomerMappingsTemp;
			}
			
		}
	}
	
	$this->ci->db->reset_query();
	$AssignmentMappingKey		= '';
	$AssignmentMappingKeyTemp	= '';
	$AssignmentMappings			= array();
	$AssignmentMappingsTemps	= $this->ci->db->order_by('id','asc')->get_where('mapping_assignment',array('account2Id' => $account2Id))->result_array();
	if($AssignmentMappingsTemps){
		foreach($AssignmentMappingsTemps as $AssignmentMappingsTemp){
			if($AssignmentMappingsTemp['account1StaffId']){
				$AssignmentMappingKey		= 'account1StaffId';
				break;
			}
			elseif($AssignmentMappingsTemp['account1ProjectId']){
				$AssignmentMappingKey		= 'account1ProjectId';
				break;
			}
			elseif($AssignmentMappingsTemp['account1ChannelId']){
				$AssignmentMappingKey		= 'account1ChannelId';
				break;
			}
			elseif($AssignmentMappingsTemp['account1LeadSourceId']){
				$AssignmentMappingKey		= 'account1LeadSourceId';
				break;
			}
			elseif($AssignmentMappingsTemp['account1TeamId']){
				$AssignmentMappingKey		= 'account1TeamId';
				break;
			}
			elseif($AssignmentMappingsTemp['account1APIFieldValueId']){
				$account1APIFieldValueId	= trim($AssignmentMappingsTemp['account1APIFieldValueId']);
				$account1APIFieldValueId	= explode("||", $account1APIFieldValueId);
				$account1APIFieldValueId	= array_filter($account1APIFieldValueId);
				$account1APIFieldValueId	= array_unique($account1APIFieldValueId);
				$AssignmentMappingKey		= trim($account1APIFieldValueId[0]);
				$AssignmentMappingKeyTemp	= 'customAPIField';
				break;
			}
			else{
				break;
			}
		}
		if($AssignmentMappingKey){
			foreach($AssignmentMappingsTemps as $AssignmentMappingsTemp){
				if($AssignmentMappingsTemp['account1APIFieldValueId']){
					$account1APIFieldValueId	= trim($AssignmentMappingsTemp['account1APIFieldValueId']);
					$account1APIFieldValueId	= explode("||", $account1APIFieldValueId);
					if((is_array($account1APIFieldValueId)) AND (!empty($account1APIFieldValueId))){
						$account1APIFieldValueId	= array_filter($account1APIFieldValueId);
						$account1APIFieldValueId	= array_unique($account1APIFieldValueId);
						$mapAPIFieldName			= trim($account1APIFieldValueId[0]);
						$mapAPIFieldValues			= trim($account1APIFieldValueId[1]);
						$mapAPIFieldValues			= explode("&&", $mapAPIFieldValues);
						if((is_array($mapAPIFieldValues)) AND (!empty($mapAPIFieldValues))){
							$mapAPIFieldValues	= array_filter($mapAPIFieldValues);
							$mapAPIFieldValues	= array_unique($mapAPIFieldValues);
							foreach($mapAPIFieldValues as $mapAPIFieldValuess){
								$AssignmentMappings[trim($AssignmentMappingKey)][strtolower(trim($mapAPIFieldValuess))]	= $AssignmentMappingsTemp;
							}
						}
					}
				}
				else{
					if($AssignmentMappingsTemp[$AssignmentMappingKey]){
						$AssignmentMappings[$AssignmentMappingsTemp[$AssignmentMappingKey]]	= $AssignmentMappingsTemp;
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$ChannelcustomfieldMappingKey		= '';
	$ChannelcustomfieldMappings			= array();
	$ChannelcustomfieldMappingsTemps	= $this->ci->db->order_by('id','desc')->get_where('mapping_channelcustomfield',array('account2Id' => $account2Id))->result_array();
	if($ChannelcustomfieldMappingsTemps){
		foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
			if($ChannelcustomfieldMappingsTemp['account1CustomFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1CustomFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1APIFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1APIFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1WarehouseId']){
				$ChannelcustomfieldMappingKey	= 'account1WarehouseId';
				break;
			}
			else{
				break;
			}
		}
		if($ChannelcustomfieldMappingKey){
			foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
				if($ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey]){
					$ChannelcustomfieldChannel		= $ChannelcustomfieldMappingsTemp['account1ChannelId'];
					$ChannelcustomfieldKey2Value	= $ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey];
					$ChannelcustomfieldMappings[$ChannelcustomfieldChannel][$ChannelcustomfieldKey2Value]	= $ChannelcustomfieldMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxAsLineMappingTemps	= $this->ci->db->get_where('mapping_taxasline',array('account2Id' => $account2Id))->result_array();
	$taxAsLineMappings		= array();
	$taxAsLineMappings2		= array();
	if($taxAsLineMappingTemps){
		foreach($taxAsLineMappingTemps as $taxAsLineMappingTemp){
			if(!$taxAsLineMappingTemp['account1ChannelId']){continue;}
			if($taxAsLineMappingTemp['account1ApiFieldId']){
				$allTempChannelIDs	= explode(",",trim($taxAsLineMappingTemp['account1ChannelId']));
				$allTempApiFields	= explode(",",trim($taxAsLineMappingTemp['account1ApiFieldId']));
				foreach($allTempChannelIDs as $channelkey => $allTempChannelID){
					foreach($allTempApiFields as $apikey => $allTempApiField){
						$taxAsLineMappings[$allTempChannelID][strtolower($allTempApiField)] = $taxAsLineMappingTemp;
					}
				}
			}else{
				$allTempChannelIDs	= explode(",",trim($taxAsLineMappingTemp['account1ChannelId']));
				foreach($allTempChannelIDs as $channelkey => $allTempChannelID){
						$taxAsLineMappings2[$allTempChannelID] = $taxAsLineMappingTemp;
				}
			}
		}
	}
	
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <> ' => '2'))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$referenceMappingsTemps	= $this->ci->db->get_where('mapping_reference',array('account2Id' => $account2Id))->result_array();
	$referenceMappings		= array();
	if($referenceMappingsTemps){
		foreach($referenceMappingsTemps as $referenceMappingsTemp){
			$referenceMappings[$referenceMappingsTemp['account1ChannelId']]		= $referenceMappingsTemp;
		}
	}
	
	if(!$config['disableSkuDetails']){
		if(!empty($datas)){
			$allPostItmeIds	= array();
			foreach($datas as $datasTemp){
				$rowDatas			= json_decode($datasTemp['rowData'], true);
				$channelId			= $rowDatas['assignment']['current']['channelId'];
				$orderCurrencyCode	= strtolower($rowDatas['currency']['orderCurrencyCode']);
				
				if(isset($AggregationMappings[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings[$channelId]['bpAccountingCurrency'])){continue;}
				if(isset($AggregationMappings2[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings2[$channelId]['bpAccountingCurrency'])){continue;}
				
				foreach($rowDatas['orderRows'] as $orderRowsTemp){
					if($orderRowsTemp['productId'] > 1001){
						$allPostItmeIds[]	= $orderRowsTemp['productId'];
					}
				}
			}
			$allPostItmeIds = array_filter($allPostItmeIds);
			$allPostItmeIds = array_unique($allPostItmeIds);
			if(!empty($allPostItmeIds)){
				$this->postProducts($allPostItmeIds,$account2Id);
			}
		}
	}
	
	if(!empty($datas)){
		$allPostContactIds	= array();
		foreach($datas as $datasTemp){
			$tempRowData		= json_decode($datasTemp['rowData'],true);
			$channelId			= $tempRowData['assignment']['current']['channelId'];
			$orderCurrencyCode	= strtolower($tempRowData['currency']['orderCurrencyCode']);
			if($channelId){
				if(isset($genericcustomerMappings[$channelId])){continue;}
				if(isset($AggregationMappings[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings[$channelId]['bpAccountingCurrency'])){continue;}
				if(isset($AggregationMappings2[$channelId][$orderCurrencyCode])){continue;}
				if(isset($AggregationMappings2[$channelId]['bpAccountingCurrency'])){continue;}
				$allPostContactIds[]	= $datasTemp['customerId'];
			}
			else{
				$allPostContactIds[]	= $datasTemp['customerId'];
			}
		}
		$allPostContactIds = array_filter($allPostContactIds);
		$allPostContactIds = array_unique($allPostContactIds);
		if(!empty($allPostContactIds)){
			$this->postCustomers($allPostContactIds,$account2Id);
		}
	}
	
	if($this->ci->globalConfig['enableAdvanceTaxMapping'] OR $this->ci->globalConfig['enableProductcategoryMapping']){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku')->get_where('products',array('createdProductId <>' => '', 'account2Id' => $account2Id))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	
	if($clientcode == 'hawkeopticsxero_invalid'){
		$this->ci->db->reset_query();
		$customerMappingsTemps	= $this->ci->db->select('customerId, createdCustomerId, email, company, status')->get_where('customers',array('isSupplier' => 0, 'account2Id' => $account2Id))->result_array();
		$customerMappings		= array();
		if($customerMappingsTemps){
			foreach($customerMappingsTemps as $customerMappingsTemp){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$customerMappingsTemps	= $this->ci->db->select('customerId, createdCustomerId, email, company, status')->get_where('customers',array('isSupplier' => 0, 'createdCustomerId <>'=> '', 'account2Id' => $account2Id))->result_array();
		$customerMappings		= array();
		if($customerMappingsTemps){
			foreach($customerMappingsTemps as $customerMappingsTemp){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$productcategoryMappingTemps	= $this->ci->db->get_where('mapping_productcategory',array('account1CategoryId <>' => '','account2Id' => $account2Id))->result_array();
	$productcategoryMappings		= array();
	$allMappedCategory				= array();
	if($productcategoryMappingTemps){
		foreach($productcategoryMappingTemps as $productcategoryMappingTemp){
			$allMappedCategory[]	= $productcategoryMappingTemp['account1CategoryId'];
			$productcategoryMappings[$productcategoryMappingTemp['account1CategoryId']]	= $productcategoryMappingTemp;
		}
	}
	$allMappedCategory	= array_filter($allMappedCategory);
	$allMappedCategory	= array_unique($allMappedCategory);
	
	$nominalCodeForShipping		= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForGiftCard		= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForDiscount		= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForLandedCost	= explode(",",$config['nominalCodeForLandedCost']);
	
	$this->ci->db->reset_query();
	$contactLinkingTemps	= $this->ci->db->get_where('mapping_contactlinking',array('account2Id' => $account2Id))->result_array();
	$contactLinkingMapping	= array();
	if($contactLinkingTemps){
		foreach($contactLinkingTemps as $contactLinkingTempss){
			$account1FieldValues	= trim($contactLinkingTempss['account1FieldValue']);
			$account1FieldValues	= explode("||", $account1FieldValues);
			if((is_array($account1FieldValues)) AND (!empty($account1FieldValues))){
				$account1FieldValues	= array_filter($account1FieldValues);
				$account1FieldValues	= array_unique($account1FieldValues);
				$customFieldName		= trim($account1FieldValues[0]);
				$customFieldValues		= trim($account1FieldValues[1]);
				$customFieldValues		= explode("&&", $customFieldValues);
				if((is_array($customFieldValues)) AND (!empty($customFieldValues))){
					$customFieldValues	= array_filter($customFieldValues);
					$customFieldValues	= array_unique($customFieldValues);
					foreach($customFieldValues as $customFieldValuess){
						$contactLinkingMapping[strtolower(trim($customFieldName))][strtolower(trim($customFieldValuess))]	= $contactLinkingTempss;
					}
				}
			}
		}
	}
	
	if($datas){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($orderDatas['sendInAggregation']){continue;}
			
			$config					= $this->accountConfig[$account2Id];
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$CustomFieldValueID		= '';
			if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
				$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			}
			
			$ConsolAPIFieldValueID	= '';
			
			//Customcode for Traten Xero STARTS
			$bpTaxAsLineFieldId		= '';
			$bpTaxAsLineField		= $bpconfig['taxAsLine'];
			if(substr_count(strtolower($bpTaxAsLineField),'pcf_')){
				$bpTaxAsLineFieldsAll	= explode(".",$bpTaxAsLineField);
				$bpTaxAsLineFieldId	= $rowDatas['customFields'][$bpTaxAsLineFieldsAll[1]];
				if(is_array($bpTaxAsLineFieldId)){					
					$bpTaxAsLineFieldId		= $bpTaxAsLineFieldId['id'];					
				}
				else{
					$bpTaxAsLineFieldId		= strtolower($bpTaxAsLineFieldId);
				}
			}
			else{
				$bpTaxAsLineFieldsAll	= explode(".",$bpTaxAsLineField);
				$fieldValue		= '';
				$fieldValueTmps	= '';
				foreach($bpTaxAsLineFieldsAll as $account1FieldId){
					if(!$fieldValueTmps){
						$fieldValueTmps	= $rowDatas[$account1FieldId];
					}
					else{
						$fieldValueTmps = $fieldValueTmps[$account1FieldId];
					}
				}
				$bpTaxAsLineFieldId	= strtolower($fieldValueTmps);
			}
			if(isset($taxAsLineMappings[$channelId][$bpTaxAsLineFieldId])){
				$config['SendTaxAsLineItem']	= 1;
				$config['TaxItemLineNominal']	= $taxAsLineMappings[$channelId][$bpTaxAsLineFieldId]['account2TaxAccountIds'];
			}
			elseif(isset($taxAsLineMappings2[$channelId])){
				$config['SendTaxAsLineItem']	= 1;
				$config['TaxItemLineNominal']	= $taxAsLineMappings2[$channelId]['account2TaxAccountIds'];
			}
			else{
				//
			}
			//Customcode for Traten Xero ENDS
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields'] OR $this->ci->globalConfig['enableChannelcustomfieldMapping']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			$IsConsolApplicable		= 0;
			$IsNonConsolApplicable	= 0;
			
			if($this->ci->globalConfig['enableAggregation']){
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						if($this->ci->globalConfig['enableConsolidationMappingCustomazation']){
							if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
								$IsConsolApplicable					= 0;
								$IsNonConsolApplicable				= 0;
								$ExcludedStringInCustomField		= array();
								$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
								$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
								$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
								$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
								if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
									foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
										$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
										if(is_array($AggregatedCustomFieldData)){
											if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
										else{
											if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
									}
								}
								else{
									continue;
								}
								if($IsConsolApplicable){
									continue;
								}
							}
							else{
								continue;
							}
						}
						else{
							continue;
						}
					}
					if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]){
						if($this->ci->globalConfig['enableConsolidationMappingCustomazation']){
							if(($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['ExcludedStringInCustomField'])){
								$IsConsolApplicable					= 0;
								$IsNonConsolApplicable				= 0;
								$ExcludedStringInCustomField		= array();
								$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['brightpearlCustomFieldName']);
								$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
								$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['ExcludedStringInCustomField']));
								$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
								if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
									foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
										$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
										if(is_array($AggregatedCustomFieldData)){
											if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
										else{
											if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
												$IsNonConsolApplicable	= 1;
												$IsConsolApplicable		= 0;
												break;
											}
											else{
												$IsConsolApplicable		= 1;
												$IsNonConsolApplicable	= 0;
											}
										}
									}
								}
								else{
									continue;
								}
								if($IsConsolApplicable){
									continue;
								}
							}
							else{
								continue;
							}
						}
						else{
							continue;
						}
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
							if($this->ci->globalConfig['enableConsolidationMappingCustomazation']){
								if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
											$APIfieldValueTmps		= '';
											foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
												if(!$APIfieldValueTmps){
													$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
												}
												else{
													$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
												}
											}
											if($APIfieldValueTmps){
												$AggregatedCustomFieldData	= $APIfieldValueTmps;
											}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
						elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']){
							if($this->ci->globalConfig['enableConsolidationMappingCustomazation']){
								if(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
									$IsNonConsolApplicable				= 0;
									$IsConsolApplicable					= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
											$APIfieldValueTmps		= '';
											foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
												if(!$APIfieldValueTmps){
													$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
												}
												else{
													$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
												}
											}
											if($APIfieldValueTmps){
												$AggregatedCustomFieldData	= $APIfieldValueTmps;
											}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
					}
					if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]){
						if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]){
							if($this->ci->globalConfig['enableConsolidationMappingCustomazation']){
								if(($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
											$APIfieldValueTmps		= '';
											foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
												if(!$APIfieldValueTmps){
													$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
												}
												else{
													$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
												}
											}
											if($APIfieldValueTmps){
												$AggregatedCustomFieldData	= $APIfieldValueTmps;
											}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
						elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']){
							if($this->ci->globalConfig['enableConsolidationMappingCustomazation']){
								if(($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['ExcludedStringInCustomField'])){
									$IsNonConsolApplicable				= 0;
									$IsConsolApplicable					= 0;
									$ExcludedStringInCustomField		= array();
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									if(!$AggregatedCustomFieldData){
										$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
											$APIfieldValueTmps		= '';
											foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
												if(!$APIfieldValueTmps){
													$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
												}
												else{
													$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
												}
											}
											if($APIfieldValueTmps){
												$AggregatedCustomFieldData	= $APIfieldValueTmps;
											}
									}
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
									}
									else{
										continue;
									}
									if($IsConsolApplicable){
										continue;
									}
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
					}
				}
			}
			
			/* //		UNINVOICING CODE STARTS		// */
			if($UnInvoicingEnabled){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createOrderId']){
						$createOrderId	= $orderDatas['createOrderId'];
						$uninvoiceCount	= $orderDatas['uninvoiceCount'];
						$TempAcc2ID		= '';
						if($orderDatas['TempAcc2ID']){
							$TempAcc2ID	= $orderDatas['TempAcc2ID'];
						}
						else{
							$TempAcc2ID	= $account2Id;
						}
						$createdRowData	= json_decode($orderDatas['createdRowData'],true);
						if($createdRowData['lastUninvoiceTry']){
							if($createdRowData['lastUninvoiceTry'] > strtotime('-3 hour')){
								continue;
							}
						}
						$this->headers	= array();
						$getInvoice		= '2.0/Invoice/'.$createOrderId;
						$this->initializeConfig($TempAcc2ID, 'GET', $getInvoice);
						$getInvoiceRes	= $this->getCurl($getInvoice, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
						if((strtolower($getInvoiceRes['Status']) == 'ok') AND (strtolower($getInvoiceRes['Invoices']['Invoice']['Status']) == 'voided')){
							$uninvoiceCount++;
							$this->ci->db->update('sales_order',array('paymentDetails' => '', 'TempAcc2ID' => 0, 'status' => '0', 'uninvoiced' => '2', 'createOrderId' => '', 'invoiceRef' => '', 'isPaymentCreated' => 0, 'sendPaymentTo' => '', 'createInvoiceId' => '', 'uninvoiceCount' => $uninvoiceCount),array('id' => $orderDatas['id']));
						}
						elseif(strtolower($getInvoiceRes['Status']) == 'ok'){
							$ErrorInPaymentVoid	= 0;
							if($getInvoiceRes['Invoices']['Invoice']['Payments']['Payment']){
								$AllPaymentInfoXero	= array();
								if(!$getInvoiceRes['Invoices']['Invoice']['Payments']['Payment']['0']){
									$AllPaymentInfoXero	= array($getInvoiceRes['Invoices']['Invoice']['Payments']['Payment']);
								}
								else{
									$AllPaymentInfoXero	= $getInvoiceRes['Invoices']['Invoice']['Payments']['Payment'];
								}
								foreach($AllPaymentInfoXero as $xeroPaymentInfo){
									$VoidPaymentRequest	= array(
										"PaymentID"			=>	$xeroPaymentInfo['PaymentID'],
										"Status"			=>	"DELETED",
									);
									$this->headers		= array();
									$VoidpayemntURL		= '2.0/Payments';
									$this->initializeConfig($TempAcc2ID, 'POST', $VoidpayemntURL);
									$DeleteResponse		= $this->getCurl($VoidpayemntURL, 'POST', json_encode($VoidPaymentRequest), 'json', $TempAcc2ID)[$TempAcc2ID];
									if(strtolower($DeleteResponse['Status']) == 'ok'){
										//
									}
									else{
										$ErrorInPaymentVoid	= 1;
									}
								}
							}
							if(isset($getInvoiceRes['Invoices']['Invoice']['Prepayments']['Prepayment'])){	//chnages after prePayments
								$allPrepaymentsOfOrder	= $this->ci->db->where_in('orderId',$orderDatas['orderId'])->get_where('arAdvance',array('account2Id' => $TempAcc2ID , 'prePaymentId<>' => ''))->result_array();
								if(!empty($allPrepaymentsOfOrder)){
									foreach($allPrepaymentsOfOrder as $prePaymentData){
										if($prePaymentData['applicationId']){
											$createdRowDataPP	= json_decode($prePaymentData['createdRowData'],true);
											$removeAllocation	= '2.0/Prepayments/'.$prePaymentData['prePaymentId'].'/Allocations/'.$prePaymentData['applicationId'];
											$this->headers		= array();
											$this->initializeConfig($TempAcc2ID, 'DELETE', $removeAllocation);
											$removeResponse		= $this->getCurl($removeAllocation, 'DELETE', '', 'json', $TempAcc2ID)[$TempAcc2ID];
											if((strtolower($removeResponse['Status']) == 'ok')){
												$createdRowDataPP['removed Allocation URL']		= $removeAllocation;
												$createdRowDataPP['removed Allocation Result']	= $removeResponse;
												
												$updatearray						= array();
												$updatearray['status']				= 1;
												$updatearray['applicationId']		= '';
												$updatearray['createdRowData']		= json_encode($createdRowDataPP);
												$updatearray['status']	= 4;
												$updatearray['message']	= 'Application removed due to un-invoicing';
												$this->ci->db->update('arAdvance',$updatearray,array('paymentId' => $prePaymentData['paymentId']));
											}
											else{
												$ErrorInPaymentVoid	= 1;
											}
										}
									}
								}
							}
							if($ErrorInPaymentVoid == 0){
								$InvoiceVoidRequest	= array(
									"InvoiceID"			=>	$createOrderId,
									"Status"			=>	"VOIDED",
								);
								$this->headers	= array();
								$url			= '2.0/Invoices';
								if($accountDetails['OAuthVersion'] == '2'){
									$url		= '2.0/Invoices?unitdp=4';
									$this->initializeConfig($TempAcc2ID, 'POST', $url);
								}
								else{
									$url		= '2.0/Invoices';
									$UrlParams	= array('unitdp' => '4');
									$this->initializeConfig($TempAcc2ID, 'POST', $url,$UrlParams);
									$url		= '2.0/Invoices?unitdp='.urlencode('4');
								}
								$VoidResult		= $this->getCurl($url, 'POST', json_encode($InvoiceVoidRequest), 'json', $TempAcc2ID)[$TempAcc2ID];
								$createdRowData['Void Request Data	:']	= $InvoiceVoidRequest;
								$createdRowData['Void Response Data	:']	= $VoidResult;
								$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
								if((strtolower($VoidResult['Status']) == 'ok') AND (isset($VoidResult['Invoices']['Invoice']['InvoiceID']))){
									$uninvoiceCount++;
									$this->ci->db->update('sales_order',array('paymentDetails' => '','TempAcc2ID' => 0, 'status' => '0','createOrderId' => '','uninvoiced' => '2', 'isPaymentCreated' => '0', 'sendPaymentTo' => '', 'invoiceRef' => '', 'createInvoiceId' => '', 'uninvoiceCount' => $uninvoiceCount),array('id' => $orderDatas['id']));
								}
								else{
									$badRequest	= 0;
									$errormsg	= array();
									$errormsg	= json_encode(simplexml_load_string($this->response[$TempAcc2ID]));
									$errormsg	= json_decode($errormsg,true);
									if(isset($errormsg['head']['title'])){
										if($errormsg['head']['title'] == '400 Bad Request'){
											$badRequest	= 1;
										}
									}
									if(!$badRequest){
										$createdRowData['lastUninvoiceTry'] = strtotime('now');
										$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
									}
								}
							}
						}
						continue;
					}
				}
			}
			/* //		UNINVOICING CODE ENDS		// */
			
			if(($rowDatas['invoices']['0']['taxDate']) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['sales'])) AND (strlen($dateLockSettings['sales']) > 0)){
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['sales'])));
				
				$checkTaxDate	= $rowDatas['invoices']['0']['taxDate'];
				$BPDateOffset	= (int)substr($checkTaxDate,23,3);
				$xeroOffset		= 0;
				$diff			= $BPDateOffset - $xeroOffset;
				$date			= new DateTime($checkTaxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff > 0){
					$diff		.= ' hour';
					$date->modify($diff);
				}
				$checkTaxDate	= $date->format('Ymd');
				
				if($dateLockSettings['salesCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['salesCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['salesCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			if($orderDatas['createOrderId']){continue;}
			if(!$orderDatas['invoiced']){continue;}
			
			/*	CODE TO CHECK DROPSHIP PO'S ARE SENT OR NOT, IF NOT THEN SKIP THAT SALES ORDER			*/
			/*	$CONFIG['INVENTORYMANAGEMENTENABLED'] == 0 THAT MEANS CONNECTOR IS INVENTORY MANAGED	*/
			if($config['InventoryManagementEnabled'] == 0){
				if($orderDatas['IsDropShip']){
					$this->ci->db->reset_query();
					$DropShipPODetail	= $this->ci->db->get_where('purchase_order',array('LinkedWithSO' => $orderId,'createOrderId' => NULL))->result_array();
					if($DropShipPODetail){
						$this->ci->db->reset_query();
						$this->ci->db->update('sales_order',array('message' => 'DropShipPO is not Sent Yet'),array('orderId' => $orderId));
						continue;
					}
				}
			}
			
			$this->ci->db->reset_query();
			$invoiceNumberDatas	= array();
			$allInvoiceNumbers	= $this->ci->db->select('createInvoiceId')->get_where('sales_order',array('account2Id' => $account2Id))->result_array();
			if($allInvoiceNumbers){
				foreach($allInvoiceNumbers as $allInvoiceNumbersTemp){
					if($allInvoiceNumbersTemp['createInvoiceId']){
						$invoiceNumberDatas[$allInvoiceNumbersTemp['createInvoiceId']]	= $allInvoiceNumbersTemp;
					}
				}
			}
			
			if(!$rowDatas['invoices']['0']['invoiceReference']){continue;}
			
			$XeroCustomerID			= '';
			$genericCustomerFound	= 0;
			$orderId                = $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$createdRowData			= json_decode($orderDatas['createdRowData'],true);
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$warehouseId			= $rowDatas['warehouseId'];
			$billAddress			= $rowDatas['parties']['billing'];
			$shipAddress			= $rowDatas['parties']['delivery'];
			$orderCustomer			= $rowDatas['parties']['customer'];
			$BrightpearlTotalAmount = $rowDatas['totalValue']['total'];
			$uninvoiceCount			= $orderDatas['uninvoiceCount'];
			$CustomerCompanyName	= trim($customerMappings[$orderCustomer['contactId']]['company']);
			$XeroCustomerID			= $customerMappings[$orderCustomer['contactId']]['createdCustomerId'];
			$genericcustomerMapping	= array();
			
			if($GenericCustomerMappingAdvanced){
				foreach($rowDatas['customFields'] as $fieldNameKey => $AllCustFieldDataTemp){
					$fieldNameKey	= strtolower($fieldNameKey);
					if(in_array($fieldNameKey,$AllCustomFieldsForGenericMapping)){
						if($genericcustomerMappings[$channelId][$fieldNameKey]){
							foreach($genericcustomerMappings[$channelId][$fieldNameKey] as $IncludedString => $GenericMappingTemp){
								if(is_array($AllCustFieldDataTemp)){
									if(substr_count(strtolower($AllCustFieldDataTemp['value']),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
								else{
									if(substr_count(strtolower($AllCustFieldDataTemp),$IncludedString)){
										$genericcustomerMapping	= $GenericMappingTemp;
									}
								}
							}
						}
					}
				}
				if(!$genericcustomerMapping){
					if($genericcustomerMappings[$channelId]['Blank']){
						$genericcustomerMapping	= $genericcustomerMappings[$channelId]['Blank'];
					}
				}
			}
			else{
				if($genericcustomerMappings[$channelId]){
					$genericcustomerMapping	= $genericcustomerMappings[$channelId];
				}
			}
			
			if($genericcustomerMapping){
				if($genericcustomerMapping['account2ChannelId']){
					$genericCustomerFound	= 1;
					$XeroCustomerID			= $genericcustomerMapping['account2ChannelId'];
				}
			}
			elseif(!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
				if($clientcode == 'hawkeopticsxero_invalid'){
					/* TEMPORARY HARD CODE FOR ONLY HAWKEOPTICSXERO */
					/* CUSTOMER POSTING IS NOT IN SCOPE IN HAWKEOPTICS */
				}
				else{
					$this->ci->db->reset_query();
					if($CustomerCompanyName){
						$CreatedCustomerData	= $this->ci->db->get_where('customers',array('account2Id' => $account2Id, 'company' => $CustomerCompanyName))->result_array();
						if($CreatedCustomerData){
							if((count($CreatedCustomerData)) < 5){
								continue;
							}
							else{
								foreach($CreatedCustomerData as $CreatedCustomerDatas){
									$NewParam	= json_decode($CreatedCustomerDatas['params'],true);
									if($NewParam['isPrimaryContact'] == true){
										$XeroCustomerID	= $CreatedCustomerDatas['createdCustomerId'];
										break;
									}
								}
							}
						}
					}
				}
			}
			
			if(!$XeroCustomerID){
				if($clientcode == 'hawkeopticsxero_invalid'){
					/* TEMPORARY HARD CODE FOR ONLY HAWKEOPTICSXERO */
					/* CUSTOMER POSTING IS NOT IN SCOPE IN HAWKEOPTICS */
				}
				else{
					$this->ci->db->reset_query();
					$this->ci->db->update('sales_order',array('message' => 'Customer not sent yet.'),array('orderId' => $orderId));
					continue;
				}
			}
			
			if($clientcode == 'hawkeopticsxero_invalid'){
				if(!$CustomerCompanyName AND !$genericCustomerFound){
					$this->ci->db->reset_query();
					$this->ci->db->update('sales_order',array('message' => 'Customer Company Not Found.'),array('orderId' => $orderId));
					continue;
				}
			}
			
			if($createdRowData['lastTry']){
				if($createdRowData['lastTry'] > strtotime('-3 hour')){
					continue;
				}
			}
			
			if($genericCustomerFound){
				$contactLinkingId		= '';
				$originalContactData	= $this->ci->db->get_where('customers',array('account2Id' => $account2Id, 'customerId' => $orderCustomer['contactId']))->row_array();
				if((is_array($originalContactData)) AND (!empty($originalContactData))){
					$originalContactDataParams	= json_decode($originalContactData['params'], true);
					if((is_array($originalContactDataParams)) AND (!empty($originalContactDataParams)) AND (is_array($contactLinkingMapping)) AND (!empty($contactLinkingMapping))){
						$allContactsCustomFields	= $originalContactDataParams['customFields'];
						if((is_array($allContactsCustomFields)) AND (!empty($allContactsCustomFields))){
							foreach($allContactsCustomFields as $pcf_name => $allContactsCustomFieldss){
								if((isset($contactLinkingMapping[strtolower($pcf_name)]))){
									if(((is_array($allContactsCustomFieldss))) AND ((isset($allContactsCustomFieldss['value'])))){
										if((isset($contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss['value'])]))){
											$contactLinkingId	= $contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss['value'])]['account2ContactId'];
										}
									}
									else{
										if((isset($contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss)]))){
											$contactLinkingId	= $contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss)]['account2ContactId'];
										}
									}
								}
								if($contactLinkingId){
									break;
								}
							}
						}
					}
					if($contactLinkingId AND $originalContactData['createdCustomerId']){
						$genericCustomerFound	= 0;
						$XeroCustomerID			= $originalContactData['createdCustomerId'];
					}
					elseif($contactLinkingId AND (!$originalContactData['createdCustomerId'])){
						$genericCustomerFound	= 0;
						$XeroCustomerID			= $contactLinkingId;
						$this->ci->db->update('customers',array('createdCustomerId' => $contactLinkingId, 'status' => 1),array('customerId' => $orderCustomer['contactId'], 'account2Id' => $account2Id));
					}
				}
			}
			
			$TaxComponents					= array();
			$InsertTaxResponse				= array();
			$InsertZipCodeInfo				= array();
			$TaxRatefromAvalara				= array();
			$TaxTypeData					= array();
			$taxDatas						= array();
			$avaZipDatas					= array();
			$isAvalaraTaxApplicable			= 0;
			$avalaraTaxError				= 0;
			$this->countRequest				= 0;
			$TaxType						= '';
			$TaxName						= '';
			
			$BrightpearlTotalAmount			= $rowDatas['totalValue']['total'];
			$missingSkus					= array();
			$InvoiceLineAdd					= array();
			$totalItemDiscount				= array();
			$discountCouponAmt				= array();
			$invoiceLineCount				= 0;
			$isDiscountCouponAdded			= 0;
			$xeroInvoiceRefrence			= '';
			$couponItemLineID				= '';
			$DiscountItemAccountRef			= '';
			$PostalCode						= $rowDatas['parties']['delivery']['postalCode'];
			$CountryIsoCode					= strtolower(trim($shipAddress['countryIsoCode3']));
			$countryState					= strtolower(trim($shipAddress['addressLine4']));
			$staffOwnerContactId			= $rowDatas['assignment']['current']['staffOwnerContactId'];
			$projectId						= $rowDatas['assignment']['current']['projectId'];
			$leadSourceId					= $rowDatas['assignment']['current']['leadSourceId'];
			$teamId							= $rowDatas['assignment']['current']['teamId'];
			$CustomFieldValueID		= '';
			if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
				$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			}
			$ConsolAPIFieldValueID			= '';
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields'] OR $this->ci->globalConfig['enableChannelcustomfieldMapping']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			
			$trackingDetails1				= array();
			$trackingDetails2				= array();
			$trackingDetails3				= array();
			
			if($AssignmentMappings){
				if($AssignmentMappingKey == 'account1StaffId'){
					if($AssignmentMappings[$staffOwnerContactId]){
						$trackingDetails1	= $AssignmentMappings[$staffOwnerContactId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1ProjectId'){
					if($AssignmentMappings[$projectId]){
						$trackingDetails1	= $AssignmentMappings[$projectId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1ChannelId'){
					if($AssignmentMappings[$channelId]){
						$trackingDetails1	= $AssignmentMappings[$channelId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1LeadSourceId'){
					if($AssignmentMappings[$leadSourceId]){
						$trackingDetails1	= $AssignmentMappings[$leadSourceId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1TeamId'){
					if($AssignmentMappings[$teamId]){
						$trackingDetails1	= $AssignmentMappings[$teamId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				else{
					if($AssignmentMappingKeyTemp == 'customAPIField'){
						$AssignmentMappingKeyPaths	= explode(".",$AssignmentMappingKey);
						$valueForAssignmentMapValue	= '';
						if((is_array($AssignmentMappingKeyPaths)) AND (!empty($AssignmentMappingKeyPaths))){
							foreach($AssignmentMappingKeyPaths as $AssignmentMappingKeyPath){
								if(!$valueForAssignmentMapValue){
									if(isset($rowDatas[$AssignmentMappingKeyPath])){
										$valueForAssignmentMapValue	= $rowDatas[$AssignmentMappingKeyPath];
									}
								}
								else{
									if(isset($valueForAssignmentMapValue[$AssignmentMappingKeyPath])){
										$valueForAssignmentMapValue	= $valueForAssignmentMapValue[$AssignmentMappingKeyPath];
									}
								}
							}
						}
						if($valueForAssignmentMapValue AND (isset($AssignmentMappings[$AssignmentMappingKey][strtolower($valueForAssignmentMapValue)]))){
							$trackingDetails1	= $AssignmentMappings[$AssignmentMappingKey][strtolower($valueForAssignmentMapValue)]['account2ChannelId'];
							$trackingDetails1	= explode("~=",$trackingDetails1);
						}
					}
					else{
						$trackingDetails1			= array();
					}
				}
			}
			
			if(isset($channelMappings[$channelId])){
				$trackingDetails2	= $channelMappings[$channelId]['account2ChannelId'];
				if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
					$trackingDetails2	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
				}
				$trackingDetails2	= explode("~=",$trackingDetails2);
			}
			
			if($ChannelcustomfieldMappings){
				if($ChannelcustomfieldMappingKey == 'account1CustomFieldId'){
					if($ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]){
						$trackingDetails3	= $ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]['account2ChannelId'];
						$trackingDetails3	= explode("~=",$trackingDetails3);
					}
				}
				elseif($ChannelcustomfieldMappingKey == 'account1APIFieldId'){
					if($ChannelcustomfieldMappings[$channelId][$ConsolAPIFieldValueID]){
						$trackingDetails3	= $ChannelcustomfieldMappings[$channelId][$ConsolAPIFieldValueID]['account2ChannelId'];
						$trackingDetails3	= explode("~=",$trackingDetails3);
					}
				}
				elseif($ChannelcustomfieldMappingKey == 'account1WarehouseId'){
					if($ChannelcustomfieldMappings[$channelId][$warehouseId]){
						$trackingDetails3	= $ChannelcustomfieldMappings[$channelId][$warehouseId]['account2ChannelId'];
						$trackingDetails3	= explode("~=",$trackingDetails3);
					}
				}
				else{
					$trackingDetails3		= array();
				}
			}
			
			if($trackingDetails3){
				$trackingDetails2	= array();
				$trackingDetails2	= $trackingDetails3;
			}
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] >= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
				}
				if($enableAvalaraTax){
					if($orderRows['productId'] > 1001){
						if(strtolower($CountryIsoCode) == 'us'){
							if(($orderRows['rowValue']['taxCode'] == '-') && ($orderRows['rowValue']['rowTax']['value'] > 0)){
								$isAvalaraTaxApplicable	= 1;
							}
						}
					}
				}
			}
			
			if(in_array($clientcode, $forceClientCodesForInvLine)){
				$extraOrderLine	= '';
				$linePrefix		= '';
				if($referenceMappings[$channelId]['account1CustomFieldIdLine']){
					$allExtraOrderLine		= array();
					$account1CustomFieldIdLineTemp	= explode(",",$referenceMappings[$channelId]['account1CustomFieldIdLine']);
					foreach($account1CustomFieldIdLineTemp as $account1CustomFieldIdLineTemps){
						if($referenceMappings[$channelId]['account1PrefixForLine']){
							$linePrefix	= $referenceMappings[$channelId]['account1PrefixForLine'];
						}
						
						$mappedValueLine	= '';
						$account1FieldIds	= explode(".",$account1CustomFieldIdLineTemps);
						$fieldValue			= '';
						$fieldValueTmps		= '';
							
						foreach($account1FieldIds as $account1FieldId){
							if(!$fieldValueTmps){
								$fieldValueTmps	= @$rowDatas[$account1FieldId];
							}
							else{
								$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
							}
						}
						$mappedValueLine	= $fieldValueTmps;
						if($mappedValueLine){
							$allExtraOrderLine[]	= trim($mappedValueLine);
						}
					}
					if($allExtraOrderLine){
						if(count($allExtraOrderLine) > 1){
							$extraOrderLine	= implode(", ",$allExtraOrderLine);
						}
						else{
							$extraOrderLine	= $allExtraOrderLine[0];
						}
						if($extraOrderLine AND $linePrefix){
							$extraOrderLine	= $linePrefix. " : ". $extraOrderLine;
						}
					}
				}
				if($extraOrderLine){
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'Description'	=> $extraOrderLine,
					);
					$invoiceLineCount++;
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'Description'	=> '-------------------',
					);
					$invoiceLineCount++;
				}
			}
			else{
				//processed the line mapping at the End of lineitem array
			}
			
			$missingCategoryMapping	= array();
			if($config['disableSkuDetails']){
				$ProductArray		= array();
				foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
					if(!empty($productcategoryMappings)){
						if($rowdatass['productId'] > 1000){
							if($productMappings[$rowdatass['productId']]){
								$productParams	= array();
								$productParams	= json_decode($productMappings[$rowdatass['productId']]['params'], true);
								if(!empty($productParams)){
									
									//PRODUCTCATEGORYMAPPING_CHANGES
									if(isset($productParams['salesChannels'])){
										if(isset($productParams['salesChannels'][0]['categories'][0])){
											if(isset($productParams['salesChannels'][0]['categories'][0]['categoryCode'])){
												if(in_array($productParams['salesChannels'][0]['categories'][0]['categoryCode'], $allMappedCategory)){
												}
												else{
													$missingCategoryMapping[]	= $rowdatass['productSku'];
												}
											}
										}
									}
									
								}
								else{
									$missingSkus[]	= $rowdatass['productSku'];
								}
							}
							else{
								$missingSkus[]	= $rowdatass['productSku'];
							}
						}
					}
				}
				if(!empty($missingSkus)){
					if($missingSkus){
						$missingSkus	= array_unique($missingSkus);
						$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
						continue;
					}
				}
				
				foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
					$bpNominal			= $rowdatass['nominalCode'];
					$bpTaxID			= $rowdatass['rowValue']['taxClassId'];
					$rowNet				= $rowdatass['rowValue']['rowNet']['value'];
					$rowTax				= $rowdatass['rowValue']['rowTax']['value'];
					$discountCouponAmt	= 0;
					$discountAmt		= 0;
					
					$bundleParentID		= '';
					$isBundleChild		= $rowdatass['composition']['bundleChild'];
					if($isBundleChild){
						$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
					}
					if($isBundleChild AND $bundleParentID){
						$bpTaxID		= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
					}
					
					$productCategoryID	= 'NOCATEGORY';
					$xeroTaxId			= '';
					$productId			= $rowdatass['productId'];
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax'];
					$isKidsTaxEnabled	= 0;
					
					if($kidsTaxCustomField OR $this->ci->globalConfig['enableProductcategoryMapping']){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
							
							//PRODUCTCATEGORYMAPPING_CHANGES
							if(isset($productParams['salesChannels'])){
								if(isset($productParams['salesChannels'][0]['categories'][0])){
									if(isset($productParams['salesChannels'][0]['categories'][0]['categoryCode'])){
										if(in_array($productParams['salesChannels'][0]['categories'][0]['categoryCode'], $allMappedCategory)){
											$productCategoryID	= $productParams['salesChannels'][0]['categories'][0]['categoryCode'];
										}
									}
								}
							}
						}
					}
					
					$taxMappingKey	= $bpTaxID;
					$taxMappingKey1	= $bpTaxID;
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode;
						}
					}
					$taxMapping		= array();
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$bpTaxID.'--'.$channelId])){
						$taxMapping	= $taxMappings[$bpTaxID.'--'.$channelId];
					}
					if($taxMapping){
						$xeroTaxId	= $taxMapping['account2TaxId'];
						if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsTaxId'])){
							$xeroTaxId  = $taxMapping['account2KidsTaxId'];
						}
					}
					else{
						$xeroTaxId	= $config['salesNoTaxCode'];
					}
					
					if($rowId == $couponItemLineID){
						if($rowdatass['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					if(!$config['SendTaxAsLineItem']){
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$productCategoryID][$bpNominal][$xeroTaxId])){
									$ProductArray['discount'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								if($isBundleChild){
									//
								}
								else{
									$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
									if(!$originalPrice){
										$originalPrice	= $rowNet;
									}
									if($originalPrice > $rowNet){
										$discountCouponAmt	= $originalPrice - $rowNet;
										$rowNet				= $originalPrice;
										if($discountCouponAmt > 0){
											if(isset($ProductArray['discountCoupon'][$productCategoryID][$bpNominal][$xeroTaxId])){
												$ProductArray['discountCoupon'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']	+= $discountCouponAmt;
											}
											else{
												$ProductArray['discountCoupon'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']	= $discountCouponAmt;
											}
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if(isset($ProductArray['aggregationItem'][$productCategoryID][$bpNominal][$xeroTaxId])){
								$ProductArray['aggregationItem'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']	+= $rowNet;
								$ProductArray['aggregationItem'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']	+= $rowTax;
							}
							else{
								$ProductArray['aggregationItem'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']	= $rowNet;
								$ProductArray['aggregationItem'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$productCategoryID][$bpNominal][$xeroTaxId])){
								$ProductArray['landedCost'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['landedCost'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['landedCost'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['landedCost'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$productCategoryID][$bpNominal][$xeroTaxId])){
								$ProductArray['shipping'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['shipping'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['shipping'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['shipping'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$productCategoryID][$bpNominal][$xeroTaxId])){
								$ProductArray['giftcard'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['giftcard'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['giftcard'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['giftcard'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$productCategoryID][$bpNominal][$xeroTaxId])){
								$ProductArray['couponitem'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['couponitem'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['couponitem'][$productCategoryID][$bpNominal][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['couponitem'][$productCategoryID][$bpNominal][$xeroTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
					}
					else{
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$productCategoryID][$bpNominal])){
									$ProductArray['discount'][$productCategoryID][$bpNominal]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$productCategoryID][$bpNominal]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice	= $rowNet;
								}
								if($originalPrice > $rowNet){
									$discountCouponAmt	= $originalPrice - $rowNet;
									$rowNet				= $originalPrice;
									if($discountCouponAmt > 0){
										if(isset($ProductArray['discountCoupon'][$productCategoryID][$bpNominal])){
											$ProductArray['discountCoupon'][$productCategoryID][$bpNominal]['TotalNetAmt']	+= $discountCouponAmt;
										}
										else{
											$ProductArray['discountCoupon'][$productCategoryID][$bpNominal]['TotalNetAmt']	= $discountCouponAmt;
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if(isset($ProductArray['aggregationItem'][$productCategoryID][$bpNominal])){
								$ProductArray['aggregationItem'][$productCategoryID][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['aggregationItem'][$productCategoryID][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'][$productCategoryID])){
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$productCategoryID][$bpNominal])){
								$ProductArray['landedCost'][$productCategoryID][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['landedCost'][$productCategoryID][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'][$productCategoryID])){
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$productCategoryID][$bpNominal])){
								$ProductArray['shipping'][$productCategoryID][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['shipping'][$productCategoryID][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'][$productCategoryID])){
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$productCategoryID][$bpNominal])){
								$ProductArray['giftcard'][$productCategoryID][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['giftcard'][$productCategoryID][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'][$productCategoryID])){
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$productCategoryID][$bpNominal])){
								$ProductArray['couponitem'][$productCategoryID][$bpNominal]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['couponitem'][$productCategoryID][$bpNominal]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'][$productCategoryID])){
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	+= $rowTax;
							}
							else{
								$ProductArray['allTax'][$productCategoryID]['TotalNetAmt']	= $rowTax;
							}
						}
					}
				}
				
				if($ProductArray){
					if(!$config['SendTaxAsLineItem']){
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							foreach($ProductArrayDatasTemp as $CategoryKeyproduct => $ProductArrayDatasTemps){
								//productTrackingCategoryChnages
								$tempTrackingCategory	= array();
								$tempTrackingCategory1	= array();
								if(isset($productcategoryMappings[$CategoryKeyproduct])){
									$tempTrackingCategory	= $productcategoryMappings[$CategoryKeyproduct]['account2ChannelId'];
									$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
								}
								
								if(!empty($tempTrackingCategory)){
									$tempTrackingCategory1	= $tempTrackingCategory;
								}
								else{
									$tempTrackingCategory1	= $trackingDetails1;
								}
								
								if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
									foreach($ProductArrayDatasTemps as $BPNominalCode => $ProductArrayDatas){
										$IncomeAccountRef	= $config['IncomeAccountRef'];
										if(isset($nominalMappings[$BPNominalCode])){
											if($nominalMappings[$BPNominalCode]['account2NominalId']){
												$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
											}
										}
										if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]))){
											if($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId']){
												$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
											}
										}
										$couponNominalCodeInXero	= '';
										if($keyproduct == 'discountCoupon'){
											$couponNominalCodeInBP		= $rowDatas['orderRows'][$couponItemLineID]['nominalCode'];
											if(isset($nominalMappings[$couponNominalCodeInBP])){
												if($nominalMappings[$couponNominalCodeInBP]['account2NominalId']){
													$couponNominalCodeInXero	= $nominalMappings[$couponNominalCodeInBP]['account2NominalId'];
												}
											}
											if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]))){
												if($nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]['account2NominalId']){
													$couponNominalCodeInXero	= $nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]['account2NominalId'];
												}
											}
										}
										
										foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'ItemCode'		=> $config['genericSku'],
												'Quantity'		=> 1,
												'UnitAmount'	=> sprintf("%.4f",$datas['TotalNetAmt']),
												'TaxType'		=> $Taxkeyproduct1,
												'AccountCode'	=> $IncomeAccountRef,
												'TaxAmount' 	=> sprintf("%.4f",$datas['bpTaxTotal']),
											);
											if($keyproduct == 'landedCost'){
												$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['landedCostItem'];
												$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Landed Cost';
											}
											if($keyproduct == 'shipping'){
												$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
												$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
											}
											if($keyproduct == 'giftcard'){
												$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
												$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
											}
											if($keyproduct == 'couponitem'){
												$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
												$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
											}
											if($keyproduct == 'discount'){
												$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
												$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
												$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
												if($clientcode != 'tartanblanketcoxero'){
													unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
												}
											}
											if($keyproduct == 'discountCoupon'){
												$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
												$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
												$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
												if($couponNominalCodeInXero){
													$InvoiceLineAdd[$invoiceLineCount]['AccountCode']	= $couponNominalCodeInXero;
												}
												else{
													//added after coupon changes (change req from Neha M)
													unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
												}
											}
											
											if($tempTrackingCategory1 OR $trackingDetails2){
												if($tempTrackingCategory1 AND $trackingDetails2){
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
														'Name'		=> $tempTrackingCategory1['0'],
														'Option'	=> $tempTrackingCategory1['1']
													);
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
														'Name'		=> $trackingDetails2['0'],
														'Option'	=> $trackingDetails2['1']
													);
												}
												elseif($tempTrackingCategory1 AND !$trackingDetails2){
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
														'Name'		=> $tempTrackingCategory1['0'],
														'Option'	=> $tempTrackingCategory1['1']
													);
												}
												elseif(!$tempTrackingCategory1 AND $trackingDetails2){
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
														'Name'		=> $trackingDetails2['0'],
														'Option'	=> $trackingDetails2['1']
													);
												}
												else{
													unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
												}
											}
											$invoiceLineCount++;
										}
									}
								}
								else{
									foreach($ProductArrayDatasTemps as $BPNominalCode => $ProductArrayDatas){
										$IncomeAccountRef	= $config['IncomeAccountRef'];
										if(isset($nominalMappings[$BPNominalCode])){
											if($nominalMappings[$BPNominalCode]['account2NominalId']){
												$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
											}
										}
										if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]))){
											if($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId']){
												$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
											}
										}
										foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
											$InvoiceLineAdd[$invoiceLineCount]	= array(
												'ItemCode'		=> $config['genericSku'],
												'Quantity'		=> 1,
												'UnitAmount'	=> sprintf("%.4f",($datas['TotalNetAmt'])),
												'TaxType'		=> $Taxkeyproduct1,
												'AccountCode'	=> $IncomeAccountRef,
												'TaxAmount' 	=> sprintf("%.4f",$datas['bpTaxTotal']),
											);
											
											if($tempTrackingCategory1 OR $trackingDetails2){
												if($tempTrackingCategory1 AND $trackingDetails2){
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
														'Name'		=> $tempTrackingCategory1['0'],
														'Option'	=> $tempTrackingCategory1['1']
													);
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
														'Name'		=> $trackingDetails2['0'],
														'Option'	=> $trackingDetails2['1']
													);
												}
												elseif($tempTrackingCategory1 AND !$trackingDetails2){
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
														'Name'		=> $tempTrackingCategory1['0'],
														'Option'	=> $tempTrackingCategory1['1']
													);
												}
												elseif(!$tempTrackingCategory1 AND $trackingDetails2){
													$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
														'Name'		=> $trackingDetails2['0'],
														'Option'	=> $trackingDetails2['1']
													);
												}
												else{
													if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);}
												}
											}
											$invoiceLineCount++;
										}
									}
								}
							}
						}
					}
					else{
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $CategoryKeyproduct => $ProductArrayDatasTempsTemp){
									//productTrackingCategoryChnages
									$tempTrackingCategory	= array();
									$tempTrackingCategory1	= array();
									if(isset($productcategoryMappings[$CategoryKeyproduct])){
										$tempTrackingCategory	= $productcategoryMappings[$CategoryKeyproduct]['account2ChannelId'];
										$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
									}

									if(!empty($tempTrackingCategory)){
										$tempTrackingCategory1	= $tempTrackingCategory;
									}
									else{
										$tempTrackingCategory1	= $trackingDetails1;
									}
									
									
									foreach($ProductArrayDatasTempsTemp as $BPNominalCode => $ProductArrayDatas){
										$IncomeAccountRef	= $config['IncomeAccountRef'];
										if(isset($nominalMappings[$BPNominalCode])){
											if($nominalMappings[$BPNominalCode]['account2NominalId']){
												$IncomeAccountRef	= $nominalMappings[$BPNominalCode]['account2NominalId'];
											}
										}
										if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]))){
											if($nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId']){
												$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$BPNominalCode]['account2NominalId'];
											}
										}
										$couponNominalCodeInXero	= '';
										if($keyproduct == 'discountCoupon'){
											$couponNominalCodeInBP		= $rowDatas['orderRows'][$couponItemLineID]['nominalCode'];
											if(isset($nominalMappings[$couponNominalCodeInBP])){
												if($nominalMappings[$couponNominalCodeInBP]['account2NominalId']){
													$couponNominalCodeInXero	= $nominalMappings[$couponNominalCodeInBP]['account2NominalId'];
												}
											}
											if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]))){
												if($nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]['account2NominalId']){
													$couponNominalCodeInXero	= $nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]['account2NominalId'];
												}
											}
										}
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'ItemCode'		=> $config['genericSku'],
											'Quantity'		=> 1,
											'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
											'TaxType'		=> $config['salesNoTaxCode'],
											'AccountCode'	=> $IncomeAccountRef,
										);
										if($keyproduct == 'landedCost'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['landedCostItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Landed Cost';
										}
										if($keyproduct == 'shipping'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
										}
										if($keyproduct == 'giftcard'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
										}
										if($keyproduct == 'couponitem'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
										}
										if($keyproduct == 'discount'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
											$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
											if($clientcode != 'tartanblanketcoxero'){
													unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
											}
										}
										if($keyproduct == 'discountCoupon'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
											$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
											
											if($couponNominalCodeInXero){
												$InvoiceLineAdd[$invoiceLineCount]['AccountCode']	= $couponNominalCodeInXero;
											}
											else{
												//added after coupon changes (change req from Neha M)
												unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
											}
											
										}
										if($tempTrackingCategory1 OR $trackingDetails2){
											if($tempTrackingCategory1 AND $trackingDetails2){
												$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
													'Name'		=> $tempTrackingCategory1['0'],
													'Option'	=> $tempTrackingCategory1['1']
												);
												$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
													'Name'		=> $trackingDetails2['0'],
													'Option'	=> $trackingDetails2['1']
												);
											}
											elseif($tempTrackingCategory1 AND !$trackingDetails2){
												$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
													'Name'		=> $tempTrackingCategory1['0'],
													'Option'	=> $tempTrackingCategory1['1']
												);
											}
											elseif(!$tempTrackingCategory1 AND $trackingDetails2){
												$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
													'Name'		=> $trackingDetails2['0'],
													'Option'	=> $trackingDetails2['1']
												);
											}
											else{
												unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
											}
										}
										$invoiceLineCount++;
									}
								}
							}
							elseif($keyproduct == 'allTax'){
								foreach($ProductArrayDatasTemp as $CategoryKeyproduct => $ProductArrayDatasTempsTemp){
									//productTrackingCategoryChnages
									$tempTrackingCategory	= array();
									$tempTrackingCategory1	= array();
									if(isset($productcategoryMappings[$CategoryKeyproduct])){
										$tempTrackingCategory	= $productcategoryMappings[$CategoryKeyproduct]['account2ChannelId'];
										$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
									}
									
									if(!empty($tempTrackingCategory)){
										$tempTrackingCategory1	= $tempTrackingCategory;
									}
									else{
										$tempTrackingCategory1	= $trackingDetails1;
									}
									
									
									if($ProductArrayDatasTempsTemp['TotalNetAmt'] <= 0){continue;}
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'Description' 	=> 'Total Tax Amount',
										'Quantity' 		=> 1,
										'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatasTempsTemp['TotalNetAmt']),
										'AccountCode' 	=> $config['TaxItemLineNominal'],
										'TaxType'		=> $config['salesNoTaxCode'],
									);
									if($tempTrackingCategory1 OR $trackingDetails2){
										if($tempTrackingCategory1 AND $trackingDetails2){
											$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
												'Name'		=> $tempTrackingCategory1['0'],
												'Option'	=> $tempTrackingCategory1['1']
											);
											$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
												'Name'		=> $trackingDetails2['0'],
												'Option'	=> $trackingDetails2['1']
											);
										}
										elseif($tempTrackingCategory1 AND !$trackingDetails2){
											$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
												'Name'		=> $tempTrackingCategory1['0'],
												'Option'	=> $tempTrackingCategory1['1']
											);
										}
										elseif(!$tempTrackingCategory1 AND $trackingDetails2){
											$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
												'Name'		=> $trackingDetails2['0'],
												'Option'	=> $trackingDetails2['1']
											);
										}
										else{
											if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);}
										}
									}
									$invoiceLineCount++;
								}
							}
						}
					}
				}
			}
			else{
				$tempTrackingCategory	= array();
				$tempTrackingCategory1	= array();
				ksort($rowDatas['orderRows']);
				foreach($rowDatas['orderRows'] as $rowId => $orderRows){
					$productId	= $orderRows['productId'];
					$LineTaxId	= $config['salesNoTaxCode'];
					
					if($rowId == $couponItemLineID){
						if($orderRows['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					if($config['BundleSuppression']){
						/*** Bundle component suppression****/
						if(($orderRows['composition']['bundleChild']) && ($orderRows['rowValue']['rowNet']['value'] == 0)){
							continue;
						}
						/***end***/
					}
					if($productId > 1001){
						if(!$productMappings[$productId]['createdProductId']){
							$missingSkus[]	= $orderRows['productSku'];
							continue;
						}
						$ItemRefName	= $productMappings[$productId]['sku'];
						$ItemRefValue	= $productMappings[$productId]['sku'];
					}
					else{
						if($orderRows['rowValue']['rowNet']['value'] > 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= 'Shipping';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= 'Landed Cost';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else if($orderRows['rowValue']['rowNet']['value'] < 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else{
							$ItemRefValue		= $config['genericSku'];
							$ItemRefName		= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= 'Shipping';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= 'Landed Cost';
							}
						}
					}
					
					$bundleParentID	= '';
					$isBundleChild	= $orderRows['composition']['bundleChild'];
					if($isBundleChild){
						$bundleParentID	= $orderRows['composition']['parentOrderRowId'];
					}
					
					$kidsTaxCustomField		= $bpconfig['customFieldForKidsTax'];
					$isKidsTaxEnabled		= 0;
					$productCategoryID		= 'NOCATEGORY';
					if($kidsTaxCustomField OR (!empty($productcategoryMappings))){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
							
							//PRODUCTCATEGORYMAPPING_CHANGES
							if(!empty($productcategoryMappings)){
								if(isset($productParams['salesChannels'])){
									if(isset($productParams['salesChannels'][0]['categories'][0])){
										if(isset($productParams['salesChannels'][0]['categories'][0]['categoryCode'])){
											if(in_array($productParams['salesChannels'][0]['categories'][0]['categoryCode'], $allMappedCategory)){
												$productCategoryID	= $productParams['salesChannels'][0]['categories'][0]['categoryCode'];
											}
											else{
												$missingCategoryMapping[]	= $orderRows['productSku'];
											}
										}
									}
								}
							}
						}
					}
					
					
					/*	CALCULATE THE TAX BASED ON AVALARA RATE AND APPLY TAX AMOUNT BASEDN ON CALCULATED TAX	*/
					$LineTaxId			= $config['salesNoTaxCode'];
					$taxAmount			= $orderRows['rowValue']['rowTax']['value'];
					$taxClassId			= $orderRows['rowValue']['taxClassId'];
					if($isBundleChild AND $bundleParentID){
						$taxClassId			= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
					}
					$price				= $orderRows['rowValue']['rowNet']['value'];
					$originalPrice		= $price;
					$discountPercentage	= 0;
					
					$taxMappingKey	= $taxClassId;
					$taxMappingKey1	= $taxClassId;
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $taxClassId.'-'.$CountryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $taxClassId.'-'.$CountryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $taxClassId.'-'.$CountryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $taxClassId.'-'.$CountryIsoCode;
						}
					}
					
					$taxMapping		= array();
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$taxClassId.'--'.$channelId])){
						$taxMapping	= $taxMappings[$taxClassId.'--'.$channelId];
					}
					
					if($taxMapping){
						$LineTaxId  = $taxMapping['account2TaxId'];
						if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsTaxId'])){
							$LineTaxId  = $taxMapping['account2KidsTaxId'];
						}
					}
					elseif($taxAmount > 0){
						$LineTaxId	= $config['TaxCode'];
					}
					
					
					if(($orderRows['discountPercentage'] > 0) && (($config['discountPercentFormat'] == 1) OR ($config['discountPercentFormat'] == 0))){
						$DiscountItemAccountRef	= $orderRows['nominalCode'];
						$discountPercentage		= 100 - $orderRows['discountPercentage'];
						if($discountPercentage == 0){
							$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
						}
						else{
							$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
						}
						$tempTaxAmt	= ($originalPrice - $price);
						if($tempTaxAmt > 0){
							if(isset($totalItemDiscount[$productCategoryID][$LineTaxId])){
								$totalItemDiscount[$productCategoryID][$LineTaxId]	+= $tempTaxAmt;
							}
							else{
								$totalItemDiscount[$productCategoryID][$LineTaxId]	= $tempTaxAmt;
							}
						}
					}
					else if($isDiscountCouponAdded){
						if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							if($isBundleChild){
								//
							}
							else{
								$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
								}
								if($originalPrice > $orderRows['rowValue']['rowNet']['value']){
									$discountCouponAmtTemp	= ($originalPrice - $price);
									if($discountCouponAmtTemp > 0){
										if(isset($discountCouponAmt[$productCategoryID][$LineTaxId])){
											$discountCouponAmt[$productCategoryID][$LineTaxId]	+= $discountCouponAmtTemp;
										}
										else{
											$discountCouponAmt[$productCategoryID][$LineTaxId]	= $discountCouponAmtTemp;
										}
									}
								}
								else{
									$originalPrice	= $orderRows['rowValue']['rowNet']['value'];
								}
							}
						}
					}
					
					$nominalMappingSet	= 0;
					$IncomeAccountRef	= $config['IncomeAccountRef'];
					if(isset($nominalMappings[$orderRows['nominalCode']])){
						if($nominalMappings[$orderRows['nominalCode']]['account2NominalId']){
							$IncomeAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
							$nominalMappingSet	= 1;
						}
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]))){
						if($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId']){
							$IncomeAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
							$nominalMappingSet	= 1;
						}
					}
					
					
					$UnitAmount	= 0.00;
					if($originalPrice != 0){
						$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
					}
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'ItemCode'		=> $ItemRefValue,
						'Description'	=> $orderRows['productName'],
						'Quantity'		=> (int)$orderRows['quantity']['magnitude'],
						'UnitAmount'	=> sprintf("%.4f",$UnitAmount),
						'TaxType'		=> $LineTaxId,
						'AccountCode'	=> $IncomeAccountRef,
						'TaxAmount'		=> $taxAmount,
					);
					
					if(!$InvoiceLineAdd[$invoiceLineCount]['Description']){
						$InvoiceLineAdd[$invoiceLineCount]['Description'] = $InvoiceLineAdd[$invoiceLineCount]['ItemCode'];
					}
					
					if($InvoiceLineAdd[$invoiceLineCount]['ItemCode'] == $config['couponItem']){
						if($nominalMappingSet == 0){
							unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
						}
					}
					
					if($config['InventoryManagementEnabled'] == true){
						$InvoiceLineAdd[$invoiceLineCount]['ItemCode']	= substr($InvoiceLineAdd[$invoiceLineCount]['ItemCode'],0,30);
					}
					
					if(($orderRows['discountPercentage'] > 0) && ($config['discountPercentFormat'] == 2)){
						$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= $orderRows['productPrice']['value'];
						$InvoiceLineAdd[$invoiceLineCount]['DiscountRate']	= $orderRows['discountPercentage'];
					}
					if(!$LineTaxId){
						unset($InvoiceLineAdd[$invoiceLineCount]['TaxType']);
					}
					
					/*	IF ORDER IS APPLICABLE FOR AVALARA TAX WE ARE OVERRIDING THE TAX CODE	*/
					if(($orderRows['rowValue']['taxCode'] == '-') && ($orderRows['rowValue']['rowTax']['value'] > 0)){
						/* if($TaxType){
							$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxType;
							if($taxDatas['TaxRate']){
								unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']);
							}
						} */
					}
					if($config['SendTaxAsLineItem']){
						$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
					}
					//productTrackingCategoryChnages
					$tempTrackingCategory	= array();
					$tempTrackingCategory1	= array();
					if(isset($productcategoryMappings[$productCategoryID])){
						$tempTrackingCategory	= $productcategoryMappings[$productCategoryID]['account2ChannelId'];
						$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
					}
					
					if(!empty($tempTrackingCategory)){
						$tempTrackingCategory1	= $tempTrackingCategory;
					}
					else{
						$tempTrackingCategory1	= $trackingDetails1;
					}
					
					if($tempTrackingCategory1 OR $trackingDetails2){
						if($tempTrackingCategory1 AND $trackingDetails2){
							$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
								'Name'		=> $tempTrackingCategory1['0'],
								'Option'	=> $tempTrackingCategory1['1']
							);
							$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
								'Name'		=> $trackingDetails2['0'],
								'Option'	=> $trackingDetails2['1']
							);
						}
						elseif($tempTrackingCategory1 AND !$trackingDetails2){
							$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
								'Name'		=> $tempTrackingCategory1['0'],
								'Option'	=> $tempTrackingCategory1['1']
							);
						}
						elseif(!$tempTrackingCategory1 AND $trackingDetails2){
							$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
								'Name'		=> $trackingDetails2['0'],
								'Option'	=> $trackingDetails2['1']
							);
						}
						else{
							unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
						}
					}
					if($config['SendTaxAsLineItem']){
						if(($rowDatas['totalValue']['taxAmount']) > 0){
							$InvoiceLineAdd[$invoiceLineCount]['TaxAmount']	= 0.00;
						}
					}
					$invoiceLineCount++;
				}
				if($missingSkus){
					$missingSkus	= array_unique($missingSkus);
					$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
					continue;
				}
				if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
					if($discountCouponAmt){
						$couponNominalCodeInBP		= $rowDatas['orderRows'][$couponItemLineID]['nominalCode'];
						$couponNominalCodeInXero	= '';
						if(isset($nominalMappings[$couponNominalCodeInBP])){
							if($nominalMappings[$couponNominalCodeInBP]['account2NominalId']){
								$couponNominalCodeInXero	= $nominalMappings[$couponNominalCodeInBP]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]))){
							if($nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]['account2NominalId']){
								$couponNominalCodeInXero	= $nominalChannelMappings[strtolower($channelId)][$couponNominalCodeInBP]['account2NominalId'];
							}
						}
						
						foreach($discountCouponAmt as $productCategoryID	=> $discountCouponAmtTemps){
							//productTrackingCategoryChnages
							$tempTrackingCategory	= array();
							$tempTrackingCategory1	= array();
							if(isset($productcategoryMappings[$productCategoryID])){
								$tempTrackingCategory	= $productcategoryMappings[$productCategoryID]['account2ChannelId'];
								$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
							}
							
							if(!empty($tempTrackingCategory)){
								$tempTrackingCategory1	= $tempTrackingCategory;
							}
							else{
								$tempTrackingCategory1	= $trackingDetails1;
							}
							
							foreach($discountCouponAmtTemps as $TaxID => $discountCouponLineAmount){
								$InvoiceLineAdd[$invoiceLineCount]	= array(
									'ItemCode'		=> $config['couponItem'],
									'Description'	=> 'Coupon Discount',
									'Quantity'		=> 1,
									'UnitAmount'	=> (-1) * sprintf("%.4f",$discountCouponLineAmount),
									'TaxAmount'		=> 0.00,
								);
								if($couponNominalCodeInXero){
									$InvoiceLineAdd[$invoiceLineCount]['AccountCode']	= $couponNominalCodeInXero;
								}
								if($TaxType){
									$InvoiceLineAdd[$invoiceLineCount]['TaxType']		= $TaxType;
									unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']);
								}
								else{
									if($TaxID){
										$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxID;
									}
									else{
										$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
									}
								}
								if($config['SendTaxAsLineItem']){
									$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
								}
								if($tempTrackingCategory1 OR $trackingDetails2){
									if($tempTrackingCategory1 AND $trackingDetails2){
										$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
											'Name'		=> $tempTrackingCategory1['0'],
											'Option'	=> $tempTrackingCategory1['1']
										);
										$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									elseif($tempTrackingCategory1 AND !$trackingDetails2){
										$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
											'Name'		=> $tempTrackingCategory1['0'],
											'Option'	=> $tempTrackingCategory1['1']
										);
									}
									elseif(!$tempTrackingCategory1 AND $trackingDetails2){
										$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									else{
										unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
									}
								}
								if($config['SendTaxAsLineItem']){
									if(($rowDatas['totalValue']['taxAmount']) > 0){
										$InvoiceLineAdd[$invoiceLineCount]['TaxAmount']	= 0.00;
									}
								}
								$invoiceLineCount++;
							}
						}
						
					}
				}
				if($totalItemDiscount){
					$mappedDiscountItemAccRef	= '';
					if($config['OverrideDiscountItemAccRef']){
						if($DiscountItemAccountRef){
							if(isset($nominalMappings[$DiscountItemAccountRef])){
								if($nominalMappings[$DiscountItemAccountRef]['account2NominalId']){
									$mappedDiscountItemAccRef	= $nominalMappings[$DiscountItemAccountRef]['account2NominalId'];
								}
							}
							if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$DiscountItemAccountRef]))){
								if($nominalChannelMappings[strtolower($channelId)][$DiscountItemAccountRef]['account2NominalId']){
									$mappedDiscountItemAccRef	= $nominalChannelMappings[strtolower($channelId)][$DiscountItemAccountRef]['account2NominalId'];
								}
							}
						}
					}
					foreach($totalItemDiscount as $productCategoryID	=> $totalItemDiscountTemps){
						//productTrackingCategoryChnages
						$tempTrackingCategory	= array();
						$tempTrackingCategory1	= array();
						if(isset($productcategoryMappings[$productCategoryID])){
							$tempTrackingCategory	= $productcategoryMappings[$productCategoryID]['account2ChannelId'];
							$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
						}
						
						if(!empty($tempTrackingCategory)){
							$tempTrackingCategory1	= $tempTrackingCategory;
						}
						else{
							$tempTrackingCategory1	= $trackingDetails1;
						}
						
						foreach($totalItemDiscountTemps as $TaxID => $totalItemDiscountLineAmount){
							$InvoiceLineAdd[$invoiceLineCount]	= array(
								'ItemCode'		=> $config['discountItem'],
								'Description'	=> 'Item Discount',
								'Quantity'		=> 1,
								'UnitAmount'	=> (-1) * sprintf("%.4f",$totalItemDiscountLineAmount),
								'TaxAmount'		=> 0.00,
							);
							if($config['OverrideDiscountItemAccRef']){
								if($mappedDiscountItemAccRef){
									$InvoiceLineAdd[$invoiceLineCount]['AccountCode']	= $mappedDiscountItemAccRef;
								}
							}
							if($TaxType){
								$InvoiceLineAdd[$invoiceLineCount]['TaxType']		= $TaxType;
								unset($InvoiceLineAdd[$invoiceLineCount]['TaxAmount']);
							}
							else{
								if($TaxID){
									$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxID;
								}
								else{
									$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
								}
							}
							if($config['SendTaxAsLineItem']){
								$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
							}
							if($tempTrackingCategory1 OR $trackingDetails2){
								if($tempTrackingCategory1 AND $trackingDetails2){
									$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
										'Name'		=> $tempTrackingCategory1['0'],
										'Option'	=> $tempTrackingCategory1['1']
									);
									$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
										'Name'		=> $trackingDetails2['0'],
										'Option'	=> $trackingDetails2['1']
									);
								}
								elseif($tempTrackingCategory1 AND !$trackingDetails2){
									$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
										'Name'		=> $tempTrackingCategory1['0'],
										'Option'	=> $tempTrackingCategory1['1']
									);
								}
								elseif(!$tempTrackingCategory1 AND $trackingDetails2){
									$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
										'Name'		=> $trackingDetails2['0'],
										'Option'	=> $trackingDetails2['1']
									);
								}
								else{
									unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
								}
							}
							if($config['SendTaxAsLineItem']){
								if(($rowDatas['totalValue']['taxAmount']) > 0){
									$InvoiceLineAdd[$invoiceLineCount]['TaxAmount']	= 0.00;
								}
							}
							$invoiceLineCount++;
						}
					}
				}
				if($config['SendTaxAsLineItem']){
					if(($rowDatas['totalValue']['taxAmount']) > 0){
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'Description'	=> 'Total Tax Amount',
							'Quantity'		=> 1,
							'UnitAmount'	=> sprintf("%.4f",$rowDatas['totalValue']['taxAmount']),
							'AccountCode'	=> $config['TaxItemLineNominal'],
							'TaxType'		=> $config['salesNoTaxCode'],
						);
						if($tempTrackingCategory1 OR $trackingDetails2){
							if($tempTrackingCategory1 AND $trackingDetails2){
								$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
									'Name'		=> $tempTrackingCategory1['0'],
									'Option'	=> $tempTrackingCategory1['1']
								);
								$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
									'Name'		=> $trackingDetails2['0'],
									'Option'	=> $trackingDetails2['1']
								);
							}
							elseif($tempTrackingCategory1 AND !$trackingDetails2){
								$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
									'Name'		=> $tempTrackingCategory1['0'],
									'Option'	=> $tempTrackingCategory1['1']
								);
							}
							elseif(!$tempTrackingCategory1 AND $trackingDetails2){
								$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
									'Name'		=> $trackingDetails2['0'],
									'Option'	=> $trackingDetails2['1']
								);
							}
							else{
								if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);}
							}
						}
						
						$invoiceLineCount++;
					}
				}
			}
			
			$dueDate	= date('c');
			$taxDate	= date('c');
			$ShipDate	= '';
			if(@$rowDatas['invoices']['0']['dueDate']){
				$dueDate	= $rowDatas['invoices']['0']['dueDate'];
			}
			if(@$rowDatas['invoices']['0']['taxDate']){
				$taxDate	= $rowDatas['invoices']['0']['taxDate'];
			}
			if(@$rowDatas['delivery']['deliveryDate']){
				$ShipDate	= $rowDatas['delivery']['deliveryDate'];
			}
			
			//taxdate chanages
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate		= $date->format('Y-m-d');
				
				
				$BPDateOffset	= substr($dueDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($dueDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$dueDate		= $date->format('Y-m-d');
			}
			else{
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$xeroOffset		= 0;
				$diff			= $BPDateOffset - $xeroOffset;
				$date1			= new DateTime($dueDate);
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date1->setTimezone(new DateTimeZone($BPTimeZone));
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff > 0){
					$diff			.= ' hour';
					$date->modify($diff);
					$date1->modify($diff);
				}
				$taxDate		= $date->format('Y-m-d');
				$dueDate		= $date1->format('Y-m-d');
			}
			
			if(in_array($clientcode, $forceClientCodesForInvLine)){
				if($rowDatas['customFields']['PCF_TERMS']['value']){
					$termDays	= trim($rowDatas['customFields']['PCF_TERMS']['value']);
					if((substr_count($termDays,'net')) OR (substr_count($termDays,'NET'))){
						$termsArray	= array();
						$termDay	= 0;
						if(substr_count($termDays,'net')){
							$termsArray	= explode("net",$termDays);
							$termDay	= trim($termsArray[1]);
						}
						if(substr_count($termDays,'NET')){
							$termsArray	= explode("NET",$termDays);
							$termDay	= trim($termsArray[1]);
						}
						if($termDay){
							$dateAddObj	= date_create($taxDate);
							date_add($dateAddObj,date_interval_create_from_date_string($termDay.' days'));
							$dueDate	= date_format($dateAddObj,"Y-m-d");
						}
					}
					if((substr_count($termDays,'eom')) OR (substr_count($termDays,'EOM'))){
						$termsArray	= array();
						$termDay	= 0;
						if(substr_count($termDays,'eom')){
							$termsArray	= explode("eom",$termDays);
							$termDay	= trim($termsArray[1]);
						}
						if(substr_count($termDays,'EOM')){
							$termsArray	= explode("EOM",$termDays);
							$termDay	= trim($termsArray[1]);
						}
						if($termDay){
							$newMonth		= '';
							$newYear		= '';
							$newDate		= '';
							$invoiceDate	= date('d',strtotime($taxDate));
							$invoiceMonth	= date('m',strtotime($taxDate));
							$invoiceYear	= date('Y',strtotime($taxDate));
							if($invoiceMonth == '12'){
								$newMonth	= '01';
								$newYear	= ($invoiceYear + 1);
								$newDate	= '01';
							}
							else{
								$newMonth	= ($invoiceMonth + 1);
								$newYear	= $invoiceYear;
								$newDate	= '01';
								if(strlen($newMonth) == 1){
									$newMonth	= '0'.$newMonth;
								}
							}
							$termDay		= ((int)$termDay - 1);
							$tempTaxDate	= $newYear.'-'.$newMonth.'-'.$newDate;
							$dateAddObj		= date_create($tempTaxDate);
							date_add($dateAddObj,date_interval_create_from_date_string($termDay.' days'));
							$dueDate		= date_format($dateAddObj,"Y-m-d");
						}
					}
				}
			}
			
			
			$xeroInvoiceRefrence	= '';
			$InvoiceNumber			= '';
			$InvoiceNumber			= $orderId;
			if($rowDatas['invoices']['0']['invoiceReference']){
				$InvoiceNumber	= $rowDatas['invoices']['0']['invoiceReference'];
			}
			if($uninvoiceCount > 0){
				$InvoiceNumber	= $InvoiceNumber.'/New-0'.$uninvoiceCount;
			}
			if($invoiceNumberDatas[$InvoiceNumber]){
				$this->ci->db->update('sales_order',array('message' => 'Duplicate InvoiceNumber - '.$InvoiceNumber.' exist in Xero.'),array('orderId' => $orderId));
				continue;
			}
			if($config['TakeAsRefrence'] == 'BrightpearlCustomerReference'){
				$xeroInvoiceRefrence	= $rowDatas['reference'];
			}
			else{
				$xeroInvoiceRefrence	= $orderId;
			}
			if($config['TakeAsRefrence']){
				if(($config['TakeAsRefrence'] != 'BrightpearlCustomerReference') AND ($config['TakeAsRefrence'] != 'BrightpearlOrderID')){
					$account1FieldIds	= explode(".",$config['TakeAsRefrence']);
					$fieldValueTmps		= '';
					foreach($account1FieldIds as $account1FieldId){
						if(!$fieldValueTmps){
							$fieldValueTmps	= $rowDatas[$account1FieldId];
						}
						else{
							$fieldValueTmps = $fieldValueTmps[$account1FieldId];
						}
					}
					if($fieldValueTmps){
						$xeroInvoiceRefrence	= $fieldValueTmps;
					}
					else{
						$xeroInvoiceRefrence	= $orderId;
					}
				}
			}
			
			if(!$xeroInvoiceRefrence){
				$xeroInvoiceRefrence	= $orderId;
			}
			
			foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
				if($InvoiceLineAddTemp['UnitAmount'] > 0){
					if($InvoiceLineAddTemp['UnitAmount'] < '0.0050'){
						$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
					}
				}
				else{
					if($InvoiceLineAddTemp['UnitAmount'] > '-0.0050'){
						$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
					}
				}
			}
			
			if($config['disableSkuDetails']){
				$invoiceLineAddNew		= array();
				$invoiceLineFormatted	= array();
				$newItemLineCount		= 0;
				foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
					if($InvoiceLineAddTemp['ItemCode'] == $config['genericSku']){
						$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['discountItem']){
						$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['couponItem']){
						$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['landedCostItem']){
						$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['shippingItem']){
						$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['Description'] == 'Total Tax Amount'){
						$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['giftCardItem']){
						$invoiceLineAddNew[7][]	= $InvoiceLineAddTemp;
						continue;
					}
				}
				ksort($invoiceLineAddNew);
				foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
					foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
						$invoiceLineFormatted[$newItemLineCount]	= $invoiceLineAddNewTempTemp;
						$newItemLineCount++;
					}
				}
				if($invoiceLineFormatted){
					$InvoiceLineAdd		= $invoiceLineFormatted;
					$invoiceLineCount	= $newItemLineCount;
				}
			}
			
			$request	= array(
				'Type'			=> 'ACCREC',
				'Contact'		=> array(
					'ContactID'		=> $XeroCustomerID,
					'Addresses'		=> array(
						array(
							'AddressType'	=> 'STREET',
							'AddressLine1'	=> @$shipAddress['addressLine1'],
							'AddressLine2'	=> @$shipAddress['addressLine2'],
							'City'			=> @$shipAddress['addressLine3'],
							'Region'		=> @$shipAddress['addressLine4'],
							'PostalCode'	=> @$shipAddress['postalCode'],
							'Country'		=> @$shipAddress['countryIsoCode'],
						),
						array(
							'AddressType'	=> 'POBOX',
							'AddressLine1'	=> @$billAddress['addressLine1'],
							'AddressLine2'	=> @$billAddress['addressLine2'],
							'City'			=> @$billAddress['addressLine3'],
							'Region'		=> @$billAddress['addressLine4'],
							'PostalCode'	=> @$billAddress['postalCode'],
							'Country'		=> @$billAddress['countryIsoCode'],
						),
					),
				),
				'InvoiceNumber'	=> $InvoiceNumber,
				'Reference'		=> $xeroInvoiceRefrence,
				'Status'		=> 'AUTHORISED',
				'CurrencyCode'	=> $rowDatas['currency']['orderCurrencyCode'],
				'CurrencyRate'	=> ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1,
				'Date'			=> $taxDate,
				'DueDate'		=> @$dueDate,
				'LineItems'		=> $InvoiceLineAdd,
			);
			
			
			
			if(in_array($clientcode, $forceClientCodesForInvLine)){ 
				//already processed the line mapping at the start of lineitem array
			}
			else{
				$extraOrderLine	= '';
				$linePrefix		= '';
				if($referenceMappings[$channelId]['account1CustomFieldIdLine']){
					$allExtraOrderLine		= array();
					$account1CustomFieldIdLineTemp	= explode(",",$referenceMappings[$channelId]['account1CustomFieldIdLine']);
					foreach($account1CustomFieldIdLineTemp as $account1CustomFieldIdLineTemps){
						if($referenceMappings[$channelId]['account1PrefixForLine']){
							$linePrefix	= $referenceMappings[$channelId]['account1PrefixForLine'];
						}
						
						$mappedValueLine	= '';
						$account1FieldIds	= explode(".",$account1CustomFieldIdLineTemps);
						$fieldValue			= '';
						$fieldValueTmps		= '';
							
						foreach($account1FieldIds as $account1FieldId){
							if(!$fieldValueTmps){
								$fieldValueTmps	= @$rowDatas[$account1FieldId];
							}
							else{
								$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
							}
						}
						$mappedValueLine	= $fieldValueTmps;
						if($mappedValueLine){
							$allExtraOrderLine[]	= trim($mappedValueLine);
						}
					}
					if($allExtraOrderLine){
						if(count($allExtraOrderLine) > 1){
							$extraOrderLine	= implode(", ",$allExtraOrderLine);
						}
						else{
							$extraOrderLine	= $allExtraOrderLine[0];
						}
						if($extraOrderLine AND $linePrefix){
							$extraOrderLine	= $linePrefix. " : ". $extraOrderLine;
						}
					}
				}
				if($extraOrderLine){
					$request['LineItems'][$invoiceLineCount]['Description']	= $extraOrderLine;
					$invoiceLineCount++;
				}
			}
			
			if($referenceMappings){
				if($referenceMappings[$channelId]){
					$ValueForreference	= '';
					$account1FieldIds	= explode(".",$referenceMappings[$channelId]['account1CustomFieldId']);
					$fieldValue			= '';
					$fieldValueTmps		= '';
					foreach($account1FieldIds as $account1FieldId){
						if(!$fieldValueTmps){
							$fieldValueTmps	= @$rowDatas[$account1FieldId];
						}
						else{
							$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
						}
					}
					$ValueForreference	= $fieldValueTmps;
					if($ValueForreference){
						$request['Reference']	= $ValueForreference;
					}
				}
			}
			
			$finalOrderNotes	= '';
			$notesType			= '';
			if($referenceMappings[$channelId]){
				if($referenceMappings[$channelId]['account1CustomFieldIdforNotes']){
					$allOrderNotes			= array();
					$notesOnSalesInvoice	= explode(",",$referenceMappings[$channelId]['account1CustomFieldIdforNotes']);
					foreach($notesOnSalesInvoice as $notesOnSalesInvoiceTemp){
						if($referenceMappings[$channelId]['account1PrefixForNotes']){
							$notesType	= $referenceMappings[$channelId]['account1PrefixForNotes'];
						}
						
						$ValueForreference	= '';
						$account1FieldIds	= explode(".",$notesOnSalesInvoiceTemp);
						$fieldValue			= '';
						$fieldValueTmps		= '';
							
						foreach($account1FieldIds as $account1FieldId){
							if(!$fieldValueTmps){
								$fieldValueTmps	= @$rowDatas[$account1FieldId];
							}
							else{
								$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
							}
						}
						$ValueForreference	= $fieldValueTmps;
						if($ValueForreference){
							$allOrderNotes[]	= trim($ValueForreference);
						}
					}
					if($allOrderNotes){
						if(count($allOrderNotes) > 1){
							$finalOrderNotes	= implode(", ",$allOrderNotes);
						}
						else{
							$finalOrderNotes	= $allOrderNotes[0];
						}
						if($finalOrderNotes AND $notesType){
							$finalOrderNotes	= $notesType. " : ". $finalOrderNotes;
						}
					}
				}
			}
			
			if($config['defaultCurrency'] != $bpconfig['currencyCode']){
				unset($request['CurrencyRate']);
			}
			
			$this->headers	= array();
			$url			= '2.0/Invoices';
			if($accountDetails['OAuthVersion'] == '2'){
				$url		= '2.0/Invoices?unitdp=4';
				$this->initializeConfig($account2Id, 'PUT', $url);
			}
			else{
				$url		= '2.0/Invoices';
				$UrlParams	= array('unitdp' => '4');
				$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
				$url		= '2.0/Invoices?unitdp='.urlencode('4');
			}
			$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
			
			if(isset($results['Elements']['DataContractBase']['ValidationErrors']['ValidationError']['Message'])){
				$xeroErrorMessage	= $results['Elements']['DataContractBase']['ValidationErrors']['ValidationError']['Message'];
				if($xeroErrorMessage == 'The Total for this document must be greater than or equal to zero.'){
					$currentTotalAmt = $results['Elements']['DataContractBase']['Total'];
					foreach($InvoiceLineAdd as $lineNewKey => $InvoiceLineAddTemp){
						if(($InvoiceLineAddTemp['ItemCode'] == $config['discountItem']) OR ($InvoiceLineAddTemp['ItemCode'] == $config['couponItem'])){
							$InvoiceLineAdd[$lineNewKey]['UnitAmount']	= ($InvoiceLineAddTemp['UnitAmount'] - $currentTotalAmt);
						}
					}
					$request['LineItems'] = $InvoiceLineAdd;
					$this->headers			= array();
					$url					= '2.0/Invoices';
					if($accountDetails['OAuthVersion'] == '2'){
						$url				= '2.0/Invoices?unitdp=4';
						$this->initializeConfig($account2Id, 'PUT', $url);
					}
					else{
						$url				= '2.0/Invoices';
						$UrlParams			= array('unitdp' => '4');
						$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
						$url				= '2.0/Invoices?unitdp='.urlencode('4');
					}
					$results				= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
				}
			}
			
			$createdRowData	= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
			$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
			
			if((strtolower($results['Status']) == 'ok') AND (isset($results['Invoices']['Invoice']['InvoiceID']))){
				if($finalOrderNotes){
					$HistoryRequest	= array(
						"HistoryRecords"	=> array(
							array(
								"Details" => $finalOrderNotes,
							),
						),
					);
					$Historyurl		= '2.0/Invoices/'.$results['Invoices']['Invoice']['InvoiceID'].'/history';
					$this->initializeConfig($account2Id, 'PUT', $Historyurl);
					$Historyresults	= $this->getCurl($Historyurl, 'PUT', json_encode($HistoryRequest), 'json', $account2Id)[$account2Id];
					$createdRowData['Notes Request']	= $HistoryRequest;
					$createdRowData['Notes Results']	= $Historyresults;
				}
				
				$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData), 'status' => '1','createInvoiceId' => $InvoiceNumber,'message' => '','invoiced' => '0','invoiceRef' => $xeroInvoiceRefrence,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID'],'PostedTime' => date('c')),array('orderId' => $orderId));
				
				$XeroTotalAmount	= $results['Invoices']['Invoice']['Total'];
				$NetRoundOff		= $BrightpearlTotalAmount - $XeroTotalAmount;
				$RoundOffCheck		= abs($NetRoundOff);
				$RoundOffApplicable	= 0;
				if($RoundOffCheck != 0){
					if($RoundOffCheck < 0.99){
						$RoundOffApplicable	= 1;
					}
					if($RoundOffApplicable){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'ItemCode'		=> $config['roundOffItem'],
							'Description'	=> $config['roundOffItem'],
							'Quantity'		=> 1,
							'UnitAmount'	=> sprintf("%.4f",$NetRoundOff),
							'TaxType'		=> $config['salesNoTaxCode'],
						);
						$request['LineItems']		= $InvoiceLineAdd;
						$request['InvoiceID']		= $results['Invoices']['Invoice']['InvoiceID'];
						
						$this->headers	= array();
						$url			= '2.0/Invoices';
						if($accountDetails['OAuthVersion'] == '2'){
							$url		= '2.0/Invoices?unitdp=4';
							$this->initializeConfig($account2Id, 'POST', $url);
						}
						else{
							$url		= '2.0/Invoices';
							$UrlParams	= array('unitdp' => '4');
							$this->initializeConfig($account2Id, 'POST', $url,$UrlParams);
							$url		= '2.0/Invoices?unitdp='.urlencode('4');
						}
						$results2	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Rounding Request data	: ']	= $request;
						$createdRowData['Rounding Response data	: ']	= $results2;
						if((strtolower($results2['Status']) == 'ok') AND (isset($results2['Invoices']['Invoice']['InvoiceID']))){
							$this->ci->db->update('sales_order',array('createOrderId' => $results2['Invoices']['Invoice']['InvoiceID'],'createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
						}
						else{
							$this->ci->db->update('sales_order',array('message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
						}
					}
				}
			}
			else{
				$badRequest	= 0;
				$errormsg	= array();
				$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
				$errormsg	= json_decode($errormsg,true);
				if(isset($errormsg['head']['title'])){
					if($errormsg['head']['title'] == '400 Bad Request'){
						$badRequest	= 1;
					}
				}
				if(!$badRequest){
					$createdRowData['lastTry']	= strtotime('now');
					$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
				}
			}
		}
	}
}
$this->postJournal($orgObjectId);
$this->postSalesPayment($orgObjectId);
$this->fetchSalesPayment();
?>