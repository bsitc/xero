<?php
$this->reInitialize();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$this->ci->globalConfig['enablePrepayments']){continue;}
	if(!$clientcode){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	$salesOrdersDatas	= $this->ci->db->get_where('sales_order',array('account2Id' => $account2Id , 'lookupInAdvance' => 1))->result_array();
	if(!empty($salesOrdersDatas)){
		$advanceDatas		= array();
		$advanceOrderIds	= array();
		$advanceOrderIds	= array_column($salesOrdersDatas, 'orderId');
		$advanceOrderIds	= array_filter($advanceOrderIds);
		$advanceOrderIds	= array_unique($advanceOrderIds);
		
		$this->ci->db->reset_query();
		$advanceDatasTemp	= $this->ci->db->where_in('orderId',$advanceOrderIds)->get_where('arAdvance',array('account2Id' => $account2Id , 'applicationId <>' => '', 'status' => 3))->result_array();
		if(empty($salesOrdersDatas)){
			foreach($advanceDatasTemp as $advanceDatasTemps){
				$advanceDatas[$advanceDatasTemps['orderId']][]	= $advanceDatasTemps;
			}
			
			foreach($salesOrdersDatas as $salesOrdersData){
				if(!isset($advanceDatas[$salesOrdersData['orderId']])){continue;}
				$paymentDetails		= json_decode($salesOrdersData['paymentDetails'],true);
				$ordersAllAdvance	= $advanceDatas[$salesOrdersData['orderId']];
				$isProcessed		= 0;
				foreach($paymentDetails as $orderPaykey => $paymentDetail){
					$reversalFound	= 0;
					if(($paymentDetail['status'] == 0) AND ($paymentDetail['sendPaymentTo'] == 'xero') AND ($paymentDetail['amount'] < 0)){
						foreach($ordersAllAdvance as $ordersAdvance){
							if(($ordersAdvance['paymentType'] == 'RECEIPT') OR ($ordersAdvance['paymentType'] == 'CAPTURE')){
								if(($ordersAdvance['paymentMethodCode'] == $paymentDetail['paymentMethod']) AND ($ordersAdvance['amountPaid'] == abs($paymentDetail['amount']))){
									$applicationId		= $ordersAdvance['applicationId'];
									$prePaymentId		= $ordersAdvance['prePaymentId'];
									$createdRowData		= json_decode($ordersAdvance['createdRowData'],true);
									$removeAllocation	= '2.0/Prepayments/'.$prePaymentId.'/Allocations/'.$applicationId;
									$this->headers		= array();
									$this->initializeConfig($account2Id, 'DELETE', $removeAllocation);
									$results			= $this->getCurl($removeAllocation, 'DELETE', '', 'json', $account2Id)[$account2Id];
									if((strtolower($results['Status']) == 'ok')){
										$reversalFound	= 1;
										$createdRowData['removed Allocation URL']		= $removeAllocation;
										$createdRowData['removed Allocation Result']	= $results;
										$updatearray						= array();
										$updatearray['status']				= 1;
										$updatearray['applicationId']		= '';
										$updatearray['createdRowData']		= json_encode($createdRowData);
										$updatearray['reversalPending']		= 1;
										$spendPaymentRequest				= array();
										$spendPaymentRequest				= $createdRowData['prePayment Request'];
										$spendPaymentRequest['Type']		= 'SPEND-PREPAYMENT';
										$spendPaymentRequest['Reference']	= $spendPaymentRequest['Reference'].'_Reversal';
										
										$this->headers	= array();
										$url			= '2.0/BankTransactions';
										if($accountDetails['OAuthVersion'] == '2'){
											$url		= '2.0/BankTransactions?unitdp=4';
											$this->initializeConfig($account2Id, 'PUT', $url);
										}
										else{
											$url		= '2.0/BankTransactions';
											$UrlParams	= array('unitdp' => '4');
											$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
											$url		= '2.0/BankTransactions?unitdp='.urlencode('4');
										}
										$reversalResults	= $this->getCurl($url, 'PUT', json_encode($spendPaymentRequest), 'json', $account2Id)[$account2Id];
										$createdRowData['reversal prePayment Request']	= $spendPaymentRequest;
										$createdRowData['reversal prePayment Response']	= $reversalResults;
										if((strtolower($reversalResults['Status']) == 'ok') AND (isset($reversalResults['BankTransactions']['BankTransaction']['BankTransactionID']))){
											$updatearray['createdRowData']			= json_encode($createdRowData);
											$updatearray['status']					= 2;
											$updatearray['message']					= '';
											$updatearray['reversalPending']			= 0;
											$updatearray['reversalBankTxnId']		= $reversalResults['BankTransactions']['BankTransaction']['BankTransactionID'];
											$updatearray['reversalPrePaymentId']	= $reversalResults['BankTransactions']['BankTransaction']['PrepaymentID'];
											$this->ci->db->update('arAdvance',$updatearray,array('paymentId' => $ordersAdvance['paymentId']));
										}
										else{
											$this->ci->db->update('arAdvance',$updatearray,array('paymentId' => $ordersAdvance['paymentId']));
										}
									}
								}
							}
							if($reversalFound){break;}
						}
					}
					if($reversalFound){
						$isProcessed	= 1;
						$paymentDetails[$orderPaykey]['status']	= 1;
						$paymentDetails[$orderPaykey]['closedWithArAdvance'] = 1;
					}
				}
				if($isProcessed){
					$updateArray	= array(
						'isPaymentCreated'	=> '0',
						'status' 			=> '1',
						'paymentStatus'		=> '2',
						'lookupInAdvance'	=> '0',
						'paymentDetails'	=> json_encode($paymentDetails),
					);
					$this->ci->db->update('sales_order',$updateArray,array('orderId' => $salesOrdersData['orderId']));
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$advanceDatasTemp	= $this->ci->db->get_where('arAdvance',array('account2Id' => $account2Id, 'reversalPending' => 1))->result_array();
	if(!empty($advanceDatasTemp)){
		foreach($advanceDatasTemp as $advanceData){
			$createdRowData			= json_decode($advanceData['createdRowData'],true);
			$spendPaymentRequest	= array();
			$spendPaymentRequest	= $createdRowData['prePayment Request'];
			$spendPaymentRequest['Type']		= 'SPEND-PREPAYMENT';
			$spendPaymentRequest['Reference']	= $spendPaymentRequest['Reference'].'_ReversalNew';
			$this->headers	= array();
			$url			= '2.0/BankTransactions';
			if($accountDetails['OAuthVersion'] == '2'){
				$url		= '2.0/BankTransactions?unitdp=4';
				$this->initializeConfig($account2Id, 'PUT', $url);
			}
			else{
				$url		= '2.0/BankTransactions';
				$UrlParams	= array('unitdp' => '4');
				$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
				$url		= '2.0/BankTransactions?unitdp='.urlencode('4');
			}
			$reversalResults	= $this->getCurl($url, 'PUT', json_encode($spendPaymentRequest), 'json', $account2Id)[$account2Id];
			$createdRowData['reversal prePayment Request']	= $spendPaymentRequest;
			$createdRowData['reversal prePayment Response']	= $reversalResults;
			if((strtolower($reversalResults['Status']) == 'ok') AND (isset($reversalResults['BankTransactions']['BankTransaction']['BankTransactionID']))){
				$updatearray	= array();
				$updatearray['createdRowData']			= json_encode($createdRowData);
				$updatearray['status']					= 2;
				$updatearray['message']					= '';
				$updatearray['reversalPending']			= 0;
				$updatearray['reversalBankTxnId']		= $reversalResults['BankTransactions']['BankTransaction']['BankTransactionID'];
				$updatearray['reversalPrePaymentId']	= $reversalResults['BankTransactions']['BankTransaction']['PrepaymentID'];
				$this->ci->db->update('arAdvance',$updatearray,array('paymentId' => $advanceData['paymentId']));
			}
			else{
				$updatearray	= array();
				$updatearray['createdRowData']			= json_encode($createdRowData);
				$this->ci->db->update('arAdvance',$updatearray,array('paymentId' => $advanceData['paymentId']));
			}
		}
	}
	
}