<?php
$this->reInitialize();
$disablePOpaymentqbotobp	= $this->ci->globalConfig['disablePOpaymentqbotobp'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	$batchUpdates				= array();
	$creditBatchUpdates			= array();
	if($disablePOpaymentqbotobp){
		continue;
	}
	
	$pendingPayments		= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('purchase_order',array('sendInAggregation' => 0, 'account2Id' => $account2Id, 'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp['createOrderId']){
			$pendingPayments[$pendingPaymentsTemp['createOrderId']] = $pendingPaymentsTemp;
		}
	}
	
	$this->headers	= array();
	$url			= '2.0/Payment';
	$subParams		= array('where' => 'PaymentType="ACCPAYPAYMENT"');
	$this->initializeConfig($account2Id, 'GET', $url,$subParams);
	$url			= '2.0/Payment?where='.urlencode('PaymentType="ACCPAYPAYMENT"');
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-3 days'));
	$results		= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	if($results){
		$PaymentsRes	= $results['Payments']['Payment'];
		if(!isset($PaymentsRes['0'])){
			$PaymentsRes	= array($PaymentsRes);
		}
		foreach($PaymentsRes as $Payments){
			$Invoices	= $Payments['Invoice']; 
			if(!isset($Invoices['0'])){
				$Invoices	= array($Invoices);
			}
			foreach($Invoices as $Invoice){
				$paymentDetails	= array();
				$inoviceId		= @$Invoice['InvoiceID'];
				if(!$inoviceId){
					continue;
				}
				if(!isset($pendingPayments[$inoviceId])){
					continue;
				}
				if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
					$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
				}
				else{
					$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
				}
				if(isset($paymentDetails[$Payments['PaymentID']]) && ($Payments['Status'] == 'AUTHORISED')){
					continue;
				}
				if(isset($paymentDetails[$Payments['PaymentID']])){
					if($Payments['Status'] == 'DELETED'){
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'yes'){
							continue;
						}
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'no'){
							$brightpearlpaykey	= $paymentDetails[$Payments['PaymentID']]['brightpearlPayID'];
							@$paymentDetails[$Payments['PaymentID']]	= array(
								'amount'				=> $Payments['Amount'],
								'sendPaymentTo'			=> 'brightpearl',
								'status'				=> '0',
								'Reference'				=> $Payments['Reference'],
								'currency'				=> $Payments['Invoice']['CurrencyCode'],
								'CurrencyRate'			=> $Payments['CurrencyRate'],
								'PaymentStatus'			=> $Payments['Status'],
								'paymentMethod'			=> $Payments['Account']['Code'],
								'paymentDate'			=> $Payments['Date'],
								'DeletedonBrightpearl'	=> 'NO',
								'paymentInitiateIn'		=> 'xero',
								'IsReversal'			=> 1,
								'brightpearlPayID'		=> $brightpearlpaykey,
							);
						}
					}
					else{
						continue;
					}
				}
				else{
					if($Payments['Status'] == 'DELETED'){
						continue;
					}
					else{
						@$paymentDetails[$Payments['PaymentID']]	= array(
							'amount'				=> $Payments['Amount'],
							'sendPaymentTo'			=> 'brightpearl',
							'status'				=> '0',
							'Reference'				=> $Payments['Reference'],
							'currency'				=> $Payments['Invoice']['CurrencyCode'],
							'CurrencyRate'			=> $Payments['CurrencyRate'],
							'PaymentStatus'			=> $Payments['Status'],
							'paymentMethod'			=> $Payments['Account']['Code'],
							'paymentDate'			=> $Payments['Date'],
							'DeletedonBrightpearl'	=> 'NO',
							'paymentInitiateIn'		=> 'xero',
							'IsReversal'			=> 0,
						);
					}
				}
				$batchUpdates[$inoviceId]					= array(
					'paymentDetails'	=> $paymentDetails,
					'createOrderId'		=> $inoviceId,
					'sendPaymentTo'		=> 'brightpearl',
				);
			}
		}
	}
	/* STARTING CODE TO GET CREDIT APPLIED PAYMENTDETAILS */
	$creditPendingPayments	= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('purchase_credit_order',array('account2Id' => $account2Id, 'isPaymentCreated' => '0','createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp['createOrderId']){
			$creditPendingPayments[$pendingPaymentsTemp['createOrderId']]	= $pendingPaymentsTemp;
		}
	}

	$this->headers	= array();
	$url			= '2.0/CreditNotes';
	$subParams		= array('where' => 'Status="PAID"');
	$this->initializeConfig($account2Id, 'GET', $url,$subParams);
	$url			= '2.0/CreditNotes?where='.urlencode('Status="PAID"');
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-3 days'));
	$results		= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	$CreditNotes	= $results['CreditNotes']['CreditNote'];
	if(!$CreditNotes['0']){
		$CreditNotes	= array($CreditNotes);
	}
	foreach($CreditNotes as $CreditNote){
		if($CreditNote['Type'] == 'ACCRECCREDIT'){
			continue;
		}
		$allocations	= $CreditNote['Allocations']['Allocation'];
		if(!$allocations['0']){
			$allocations	= array($allocations);
		}
		foreach($allocations as $Invoice){
			$paymentDetails	= array();
			$inoviceId		= @$Invoice['Invoice']['InvoiceID'];
			if(!$inoviceId){
				continue;
			}
			if(!isset($pendingPayments[$inoviceId])){
				continue;
			}
			if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments	= $CreditNote;
			if(@$paymentDetails[$Payments['CreditNoteID']]['amount'] > 0){
				continue;
			}
			@$paymentDetails[$Payments['CreditNoteID']]	= array(
				'amount'			=> $Invoice['AppliedAmount'],
				'sendPaymentTo'		=> 'brightpearl',
				'status' 			=> '0',
				'Reference'			=> 'Applied amount',
				'currency'			=> $Payments['CurrencyCode'],
				'CurrencyRate'		=> $Payments['CurrencyRate'],
				'isAppliedPayment'	=> '1',
				'IsReversal'		=> 0,
				'paymentDate'		=> $Payments['Date'],
			);
			$batchUpdates[$inoviceId]					= array(
				'paymentDetails'	=> $paymentDetails,
				'createOrderId'		=> $inoviceId,
				'sendPaymentTo'		=> 'brightpearl',
			);
		}
		/* ASSING APPLIED PAYMENT TO CREDIT */
		foreach($allocations as $Invoice){
			$paymentDetails	= array();
			$inoviceId		= @$CreditNote['CreditNoteID'];	
			if(!$inoviceId){
				continue;
			}		
			if(!isset($creditPendingPayments[$inoviceId])){
				continue;
			}
			if(!isset($creditBatchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($creditPendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails = $creditBatchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments	= $CreditNote;
			if(@$paymentDetails[$Invoice['Invoice']['InvoiceID']]['amount'] > 0){
				continue;
			}
			@$paymentDetails[$Invoice['Invoice']['InvoiceID']]	= array(
				'amount'			=> $Invoice['AppliedAmount'],
				'sendPaymentTo'		=> 'brightpearl',
				'status'			=> '0',
				'Reference'			=> 'Applied amount',
				'currency'			=> $Payments['CurrencyCode'],
				'CurrencyRate'		=> $Payments['CurrencyRate'],
				'isAppliedPayment'	=> '1',
				'IsReversal'		=> 0,
				'paymentDate'		=> $Payments['Date'],
			);
			$creditBatchUpdates[$inoviceId]						= array(
				'paymentDetails'	=> $paymentDetails,
				'createOrderId'			=> $inoviceId,
				'sendPaymentTo'		=> 'brightpearl',
			);
		}
	}	
	
	/* STARTING CODE TO GET PREPAYMENT APPLIED PAYMENTDETAILS */
	$this->headers	= array();
	$url			= '2.0/Prepayments';
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-3 days'));
	$results		= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	$CreditNotes	= $results['Prepayments']['Prepayment'];
	if(!$CreditNotes['0']){
		$CreditNotes	= array($CreditNotes);
	}
	foreach($CreditNotes as $CreditNote){
		if($CreditNote['Type'] != 'SPEND-PREPAYMENT'){
			continue;
		}
		$allocations = $CreditNote['Allocations']['Allocation'];
		if(!$allocations){
			continue;
		}
		if(!$allocations['0']){
			$allocations	= array($allocations);
		}
		foreach($allocations as $Invoice){
			$paymentDetails	= array();
			$inoviceId		= @$Invoice['Invoice']['InvoiceID'];
			if(!$inoviceId){
				continue;
			}
			if(!isset($pendingPayments[$inoviceId])){
				continue;
			}
			if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments	= $CreditNote;
			if(@$paymentDetails[$Payments['PrepaymentID']]['amount'] > 0){
				continue;
			}
			@$paymentDetails[$Payments['PrepaymentID']]	= array(
				'amount'			=> $Invoice['AppliedAmount'],
				'sendPaymentTo'		=> 'brightpearl',
				'status'			=> '0',
				'Reference'			=> 'Prepayment',
				'currency'			=> $Payments['CurrencyCode'],
				'CurrencyRate'		=> $Payments['CurrencyRate'],
				'isPrePayment'		=> '1',
				'paymentDate'		=> $Payments['Date'],
			);
			$batchUpdates[$inoviceId]					= array(
				'paymentDetails'	=> $paymentDetails,
				'createOrderId'		=> $inoviceId,
				'sendPaymentTo'		=> 'brightpearl',
			);
		}
		/* ASSING APPLIED PAYMENT TO CREDIT */
	}
	
	 /* OVERPAYMENT CODE STARTS */
	$this->headers		= array();
	$url				= '2.0/Overpayments';
	$this->headers['If-Modified-Since']	= gmdate('c',strtotime('-3 days'));
	$results			= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	$OverPaymentDatas	= $results['Overpayments']['Overpayment'];
	if(!$OverPaymentDatas['0']){
		$OverPaymentDatas	= array($OverPaymentDatas);
	}
	foreach($OverPaymentDatas as $OverPaymentData){
		if($OverPaymentData['Type'] != 'SPEND-OVERPAYMENT'){
			continue;
		}
		$allocations = $OverPaymentData['Allocations']['Allocation'];
		if(!$allocations){
			continue;
		}
		if(!$allocations['0']){
			$allocations	= array($allocations);
		}
		foreach($allocations as $Invoice){
			$paymentDetails	= array();
			$inoviceId		= @$Invoice['Invoice']['InvoiceID'];
			if(!$inoviceId){
				continue;
			}
			if(!isset($pendingPayments[$inoviceId])){
				continue;
			}
			if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
				$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
			}
			else{
				$paymentDetails	= $batchUpdates[$inoviceId]['paymentDetails'];
			}
			$Payments	= $OverPaymentData;
			if(@$paymentDetails[$Payments['OverpaymentID']]['amount'] > 0){
				continue;
			}
			@$paymentDetails[$Payments['OverpaymentID']]	= array(
				'amount'			=> $Invoice['AppliedAmount'],
				'sendPaymentTo'		=> 'brightpearl',
				'status'			=> '0',
				'Reference'			=> 'Overpayment',
				'currency'			=> $Payments['CurrencyCode'],
				'CurrencyRate'		=> $Payments['CurrencyRate'],
				'isOverPayment'		=> '1',
				'paymentDate'		=> $Payments['Date'],
			);
			$batchUpdates[$inoviceId]						= array(
				'paymentDetails'	=> $paymentDetails,
				'createOrderId'		=> $inoviceId,
				'sendPaymentTo'		=> 'brightpearl',
			);
		}
	}
	if($batchUpdates){
		foreach($batchUpdates as $key => $batchUpdate){
			$batchUpdates[$key]['paymentDetails']		= json_encode($batchUpdate['paymentDetails']);
			
		}
		$batchUpdates	= array_chunk($batchUpdates,200);
		foreach($batchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('purchase_order',$batchUpdate,'createOrderId');
			}
		}
	}
	if($creditBatchUpdates){
		foreach($creditBatchUpdates as $key => $batchUpdate){
			$creditBatchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
			
		}
		$creditBatchUpdates	= array_chunk($creditBatchUpdates,200);
		foreach($creditBatchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('purchase_credit_order',$batchUpdate,'createOrderId');
			}
		}
	}
}
?>