<?php
$this->reInitialize();
$disableSCpaymentqbotobp	= $this->ci->globalConfig['disableSCpaymentqbotobp'];
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	if($clientcode == 'amistry'){
		//kitKing company sales is disabled for now, account Id of kitKing is 2
		if($account2Id	== 2){
			continue;
		}
	}
	
	$batchUpdates	= array();
	if($disableSCpaymentqbotobp){
		continue;
	}
	
	$pendingPayments		= array();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails,sendInAggregation')->get_where('sales_credit_order',array('account2Id' => $account2Id, 'createOrderId <>' => ''))->result_array();
	foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
		if($pendingPaymentsTemp['sendInAggregation']){
			continue;
		}
		if($pendingPaymentsTemp['createOrderId']){
			$pendingPayments[$pendingPaymentsTemp['createOrderId']] = $pendingPaymentsTemp;
		}
	}
	
	$this->headers	= array();
	$url			= '2.0/Payment';
	$subParams		= array('where' => 'PaymentType="ARCREDITPAYMENT"');
	$this->initializeConfig($account2Id, 'GET', $url,$subParams);
	$url			= '2.0/Payment?where='.urlencode('PaymentType="ARCREDITPAYMENT"');
	$this->headers['If-Modified-Since'] = gmdate('c',strtotime('-3 days'));
	$results		= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	if($results){
		$PaymentsRes	= $results['Payments']['Payment'];
		if(!isset($PaymentsRes['0'])){
			$PaymentsRes	= array($PaymentsRes);
		}
		foreach($PaymentsRes as $Payments){
			$Invoices	= $Payments['Invoice']; 
			if(!isset($Invoices['0'])){
				$Invoices	= array($Invoices);
			}
			foreach($Invoices as $Invoice){
				$paymentDetails	= array();
				$inoviceId		= $Invoice['InvoiceID'];
				if(!$inoviceId){
					continue;
				}	
				if(!isset($pendingPayments[$inoviceId])){
					continue;
				}
				if($pendingPayments[$inoviceId]){
					if($pendingPayments[$inoviceId]['sendInAggregation']){
						continue;
					}
				}
				if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
					$paymentDetails	= @json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
				}
				else{
					$paymentDetails = $batchUpdates[$inoviceId]['paymentDetails'];
				}
				if(isset($paymentDetails[$Payments['PaymentID']]) AND ($Payments['Status'] == 'AUTHORISED')){
					continue;
				}
				if(isset($paymentDetails[$Payments['PaymentID']])){
					if($Payments['Status'] == 'DELETED'){
						if(strtolower($paymentDetails[$Payments['PaymentID']]['reverseby']) == 'brightpearl'){
							continue;
						}
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'yes'){
							continue;
						}
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'no'){
							if(($paymentDetails[$Payments['PaymentID']]['AmountCreditedIn'] == 'xero') OR (!isset($paymentDetails[$Payments['PaymentID']]['brightpearlPayID']))){
								continue;
							}
							else{
								$brightpearlpaykey						= $paymentDetails[$Payments['PaymentID']]['brightpearlPayID'];
								$paymentDetails[$Payments['PaymentID']]	= array(
									'amount'				=> $Payments['Amount'],
									'sendPaymentTo'			=> 'brightpearl',
									'status'				=> '0',
									'Reference'				=> $Payments['Reference'],
									'currency'				=> $Payments['Invoice']['CurrencyCode'],
									'CurrencyRate'			=> $Payments['CurrencyRate'],
									'PaymentStatus'			=> $Payments['Status'],
									'paymentMethod'			=> $Payments['Account']['Code'],
									'paymentDate'			=> $Payments['Date'],
									'DeletedonBrightpearl'	=> 'NO',
									'paymentInitiateIn'		=> 'xero',
									'IsReversal'			=> 1,
									'brightpearlPayID'		=> $brightpearlpaykey,
								);
							}
						}
					}
					else{
						continue;
					}
				}
				else{
					if($Payments['Status'] == 'DELETED'){
						continue;
					}
					else{
						@$paymentDetails[$Payments['PaymentID']]	= array(
							'amount'				=> $Payments['Amount'],
							'sendPaymentTo'			=> 'brightpearl',
							'status'				=> '0',
							'Reference'				=> $Payments['Reference'],
							'currency'				=> $Payments['Invoice']['CurrencyCode'],
							'CurrencyRate'			=> $Payments['CurrencyRate'],
							'paymentMethod'			=> $Payments['Account']['Code'],
							'paymentDate'			=> $Payments['Date'],
							'DeletedonBrightpearl'	=> 'NO',
							'paymentInitiateIn'		=> 'xero',
							'IsReversal'			=> 0,
						);
					}
				}
				$batchUpdates[$inoviceId]	= array(
					'paymentDetails'			=> $paymentDetails,
					'createOrderId'				=> $inoviceId,
					'sendPaymentTo'				=> 'brightpearl',
				);
			}
		}
	}
	if($batchUpdates){
		foreach($batchUpdates as $key => $batchUpdate){
			$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
		}
		$batchUpdates	= array_chunk($batchUpdates,200);
		foreach($batchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('sales_credit_order',$batchUpdate,'createOrderId'); 
			}
		}
	}
}
?>