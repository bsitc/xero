<?php
/* XERO : BRIGHTPEARL INTEGRATION */
$this->reInitialize();
$enableAvalaraTax	= 0;
$UnInvoicingEnabled	= 1;
$clientcode			= $this->ci->config->item('clientcode');
$dateLockSettings	= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	$config	= $this->accountConfig[$account2Id];
	
	//doNotRemoveThisLine
	$forceClientCodesForInvLine	= array('hawkeopticsxero','hawkeusxero');
	$skuTrimmingClientCodes		= array('keenxero','bfxero','arten','logicofenglishxero','tinyexplorerxero','homeleisuredirectxero');
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	else{
		$this->ci->db->group_start()->where('status', '0')->or_group_start()->where('uninvoiced', '1')->group_end()->group_end();
	}
	$datas	= $this->ci->db->get_where('purchase_order',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$supplierInfos		= array();
	$batchInvoicePO		= array();
	$allBatchInvoiceIDs	= array();
	if($config['enablePurchaseConsol']){
		$allDistnictSuppliers	= array();
		if($config['purchaseConsolBasedON'] == 'suppplierAccountCode'){
			$allSupplierInfo		= array();
			$allDistnictSuppliers	= array_column($datas,'customerId');
			$allDistnictSuppliers	= array_unique($allDistnictSuppliers);
			$allDistnictSuppliers	= array_unique($allDistnictSuppliers);
			if($allDistnictSuppliers){
				$this->ci->db->reset_query();
				$allSupplierInfo	= $this->ci->db->where_in('customerId',$allDistnictSuppliers)->select('customerId, accountReference')->get_where('customers',array('account2Id' => $account2Id))->result_array();;
				if($allSupplierInfo){
					foreach($allSupplierInfo as $allSupplierInfoTemp){
						if($allSupplierInfoTemp['accountReference']){
							$supplierInfos[$allSupplierInfoTemp['customerId']]	= $allSupplierInfoTemp['accountReference'];
						}
					}
				}
			}
		}
		
		foreach($datas as $datasTemp){
			if((!$config['purchaseConsolBasedON']) OR ($config['purchaseConsolBasedON'] == 'supplierID')){
				if($datasTemp['customerId'] AND $datasTemp['bpInvoiceNumber']){
					$batchInvoicePO[$datasTemp['customerId'].'~'.$datasTemp['bpInvoiceNumber']][]	= $datasTemp;
				}
			}
			else{
				if($supplierInfos[$datasTemp['customerId']]){
					$uniqueSupplierRef	= $supplierInfos[$datasTemp['customerId']];
					if($datasTemp['customerId'] AND $datasTemp['bpInvoiceNumber']){
						$batchInvoicePO[$uniqueSupplierRef.'~'.$datasTemp['bpInvoiceNumber']][]	= $datasTemp;
					}
				}
			}
		}
		if($batchInvoicePO){
			foreach($batchInvoicePO as $invoiceKey => $batchInvoicePOTemp){
				if(count($batchInvoicePOTemp) > 1){
					foreach($batchInvoicePOTemp as $batchInvoicePOTempData){
						$allBatchInvoiceIDs[$batchInvoicePOTempData['orderId']]	= $batchInvoicePOTempData['orderId'];
					}
				}
			}
		}
		if($orgObjectId){
			if(count($datas) == 1){
				$singleInvoiceNumber	= $datas[0]['bpInvoiceNumber'];
				$singleSupplierID		= $datas[0]['customerId'];
				if((!$config['purchaseConsolBasedON']) OR ($config['purchaseConsolBasedON'] == 'supplierID')){
					if($singleInvoiceNumber AND $singleSupplierID){
						$this->ci->db->reset_query();
						$batchDatas	= $this->ci->db->get_where('purchase_order',array('status' => 0, 'bpInvoiceNumber' => $singleInvoiceNumber, 'customerId' => $singleSupplierID, 'account2Id' => $account2Id))->result_array();
						if(count($batchDatas) > 1){
							continue;
						}
					}
				}
				else{
					if($singleInvoiceNumber AND $singleSupplierID){
						if($supplierInfos[$singleSupplierID]){
							$batchDatas			= $this->ci->db->get_where('purchase_order',array('status' => 0, 'bpInvoiceNumber' => $singleInvoiceNumber, 'account2Id' => $account2Id))->result_array();
							if(count($batchDatas) > 1){
								$allSupplierIdsDB	= array_column($batchDatas,'customerId');
								$allSupplierIdsDB	= array_unique($allSupplierIdsDB);
								$allSupplierIdsDB	= array_unique($allSupplierIdsDB);
								if($allSupplierIdsDB){
									$this->ci->db->reset_query();
									$distinctSupplierInfo	= $this->ci->db->where_in('customerId',$allSupplierIdsDB)->select('DISTINCT accountReference',false)->get_where('customers',array('account2Id' => $account2Id))->result_array();
									if($distinctSupplierInfo){
										if(count($distinctSupplierInfo) == 1){
											continue;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	if(!$config['disableSkuDetails']){
		if($datas){
			$allPostItmeIds	= array();
			foreach($datas as $datasTemp){
				$rowDatas	= json_decode($datasTemp['rowData'], true);
				foreach($rowDatas['orderRows'] as $orderRowsTemp){
					if($orderRowsTemp['productId'] > 1001){
						$allPostItmeIds[]	= $orderRowsTemp['productId'];
					}
				}
			}
			$allPostItmeIds = array_filter($allPostItmeIds);
			$allPostItmeIds = array_unique($allPostItmeIds);
			if($allPostItmeIds){
				$this->postProducts($allPostItmeIds,$account2Id);
			}
		}
	}
	
	if($clientcode == 'hawkeopticsxero_invalid'){
		/* TEMPORARY HARD CODE FOR ONLY HAWKEOPTICSXERO */
		/* CUSTOMER POSTING IS NOT IN SCOPE IN HAWKEOPTICS */
	}
	else{
		$this->ci->db->reset_query();
		if($orgObjectId){
			$this->ci->db->where_in('orderId',$orgObjectId);
		}
		$allSalesCustomerTemps	= $this->ci->db->select('customerId')->get_where('purchase_order',array('status' => '0','customerId <>' => ''))->result_array();
		if($allSalesCustomerTemps){
			$allSalesCustomer	= array();
			$allSalesCustomer	= array_column($allSalesCustomerTemps,'customerId');
			$allSalesCustomer	= array_unique($allSalesCustomer);
			if($allSalesCustomer){
				$this->postCustomers($allSalesCustomer,$account2Id);
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	if(($this->ci->globalConfig['enableAdvanceTaxMapping']) OR ($config['disableSkuDetails'])){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('createdProductId <>' => '', 'account2Id' => $account2Id))->result_array();
		$productMappings		= array();
		if($productMappingsTemps){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	
	if($clientcode == 'hawkeopticsxero_invalid'){
		$this->ci->db->reset_query();
		$customerMappingsTemps	= $this->ci->db->select('customerId, createdCustomerId, email, company, status')->get_where('customers',array('isSupplier' => 1, 'account2Id' => $account2Id))->result_array();
		$customerMappings		= array();
		if($customerMappingsTemps){
			foreach($customerMappingsTemps as $customerMappingsTemp){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$customerMappingsTemps	= $this->ci->db->select('customerId, createdCustomerId, email, company, status')->get_where('customers',array('createdCustomerId <>'=> '', 'isSupplier' => 1, 'account2Id' => $account2Id))->result_array();
		$customerMappings		= array();
		if($customerMappingsTemps){
			foreach($customerMappingsTemps as $customerMappingsTemp){
				$customerMappings[$customerMappingsTemp['customerId']]	= $customerMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <> ' => '1'))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if($taxMappingsTemps){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp 			= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$nominalCodeForShipping		= explode(",",$config['nominalCodeForShipping']);
	$nominalCodeForGiftCard		= explode(",",$config['nominalCodeForGiftCard']);
	$nominalCodeForDiscount		= explode(",",$config['nominalCodeForDiscount']);
	$nominalCodeForLandedCost	= explode(",",$config['nominalCodeForLandedCost']);
	
	if($datas){
		foreach($datas as $orderDatas){
			$bpconfig		= $this->ci->account1Config[$orderDatas['account1Id']];
			$orderId		= $orderDatas['orderId'];
			$rowDatas		= json_decode($orderDatas['rowData'],true);
			$createdRowData	= json_decode($orderDatas['createdRowData'],true);
			$warehouseId	= $rowDatas['warehouseId'];
			
			if($allBatchInvoiceIDs[$orderId]){continue;}
			if($orderDatas['sendInAggregation']){continue;}
			
			/* //		UNINVOICING CODE STARTS		// */
			if($UnInvoicingEnabled){
				if($orderDatas['uninvoiced'] == 1){
					if($orderDatas['createOrderId']){
						$createOrderId	= $orderDatas['createOrderId'];
						$uninvoiceCount	= $orderDatas['uninvoiceCount'];
						$TempAcc2ID		= '';
						$BillVoided		= 0;
						$PaymentState	= 1;
						if($orderDatas['TempAcc2ID']){
							$TempAcc2ID	= $orderDatas['TempAcc2ID'];
						}
						else{
							$TempAcc2ID	= $account2Id;
						}
						if($createdRowData['lastUninvoiceTry']){
							if($createdRowData['lastUninvoiceTry'] > strtotime('-3 hour')){
								continue;
							}
						}
						if($orderDatas['paymentDetails']){
							$paymentDetails		= json_decode($orderDatas['paymentDetails'],true);
							foreach($paymentDetails as $pKey => $paymentDetail){
								if($paymentDetail['amount'] > 0){
									if($paymentDetail['status'] == 1){
										$PaymentState	= 2;
										break;
									}
								}
							}
						}
						
						if($orderDatas['PaymentState'] == 2){
							$PaymentState	= 2;
						}
						
						$this->headers	= array();
						$getInvoice		= '2.0/Invoice/'.$createOrderId;
						$this->initializeConfig($TempAcc2ID, 'GET', $getInvoice);
						$getInvoiceRes	= $this->getCurl($getInvoice, 'GET', '', 'json', $TempAcc2ID)[$TempAcc2ID];
						if((strtolower($getInvoiceRes['Status']) == 'ok') AND ((strtolower($getInvoiceRes['Invoices']['Invoice']['Status']) == 'voided') OR (strtolower($getInvoiceRes['Invoices']['Invoice']['Status']) == 'deleted'))){
							$uninvoiceCount++;
							$BillVoided		= 1;
							$this->ci->db->update('purchase_order',array('paymentDetails' => '','TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'isPaymentCreated' => 0, 'sendPaymentTo' => '', 'uninvoiceCount' => $uninvoiceCount, 'PaymentState' => $PaymentState),array('id' => $orderDatas['id']));
						}
						elseif(strtolower($getInvoiceRes['Status']) == 'ok'){
							$purchaseStatusOnXero	= $getInvoiceRes['Invoices']['Invoice']['Status'];
							$ErrorInPaymentVoid		= 0;
							if($getInvoiceRes['Invoices']['Invoice']['Payments']['Payment']){
								$AllPaymentInfoXero	= array();
								if(!$getInvoiceRes['Invoices']['Invoice']['Payments']['Payment']['0']){
									$AllPaymentInfoXero	= array($getInvoiceRes['Invoices']['Invoice']['Payments']['Payment']);
								}
								else{
									$AllPaymentInfoXero	= $getInvoiceRes['Invoices']['Invoice']['Payments']['Payment'];
								}
								foreach($AllPaymentInfoXero as $xeroPaymentInfo){
									$VoidPaymentRequest	= array(
										"PaymentID"			=>	$xeroPaymentInfo['PaymentID'],
										"Status"			=>	"DELETED",
									);
									$this->headers		= array();
									$VoidpayemntURL		= '2.0/Payments';
									$this->initializeConfig($TempAcc2ID, 'POST', $VoidpayemntURL);
									$DeleteResponse		= $this->getCurl($VoidpayemntURL, 'POST', json_encode($VoidPaymentRequest), 'json', $TempAcc2ID)[$TempAcc2ID];
									if(strtolower($DeleteResponse['Status']) == 'ok'){
										//
									}
									else{
										$ErrorInPaymentVoid	= 1;
									}
								}
							}
							if($ErrorInPaymentVoid == 0){
								$InvoiceVoidRequest	= array(
									"InvoiceID"			=>	$createOrderId,
									"Status"			=>	"VOIDED",
								);
								
								if($purchaseStatusOnXero == 'DRAFT'){
									$InvoiceVoidRequest['Status']	= 'DELETED';
								}
								if($purchaseStatusOnXero == 'SUBMITTED'){
									$InvoiceVoidRequest['Status']	= 'DELETED';
								}
								
								$this->headers		= array();
								$url				= '2.0/Invoices';
								if($accountDetails['OAuthVersion'] == '2'){
									$url			= '2.0/Invoices?unitdp=4';
									$this->initializeConfig($TempAcc2ID, 'POST', $url);
								}
								else{
									$url			= '2.0/Invoices';
									$UrlParams		= array('unitdp' => '4');
									$this->initializeConfig($TempAcc2ID, 'POST', $url,$UrlParams);
									$url			= '2.0/Invoices?unitdp='.urlencode('4');
								}
								$VoidResult	= $this->getCurl($url, 'POST', json_encode($InvoiceVoidRequest), 'json', $TempAcc2ID)[$TempAcc2ID];
								$createdRowData['Void Request Data	:']	= $InvoiceVoidRequest;
								$createdRowData['Void Response Data	:']	= $VoidResult;
								$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
								if((strtolower($VoidResult['Status']) == 'ok') AND (isset($VoidResult['Invoices']['Invoice']['InvoiceID']))){
									$uninvoiceCount++;
									$BillVoided		= 1;
									$this->ci->db->update('purchase_order',array('paymentDetails' => '','TempAcc2ID' => 0, 'status' => '0','uninvoiced' => '2','createOrderId' => '', 'invoiceRef' => '', 'isPaymentCreated' => 0, 'sendPaymentTo' => '', 'uninvoiceCount' => $uninvoiceCount, 'PaymentState' => $PaymentState),array('id' => $orderDatas['id']));
								}
								else{
									$badRequest	= 0;
									$errormsg	= array();
									$errormsg	= json_encode(simplexml_load_string($this->response[$TempAcc2ID]));
									$errormsg	= json_decode($errormsg,true);
									if(isset($errormsg['head']['title'])){
										if($errormsg['head']['title'] == '400 Bad Request'){
											$badRequest	= 1;
										}
									}
									if(!$badRequest){
										$createdRowData['lastUninvoiceTry'] = strtotime('now');
										$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
									}
								}
							}
						}
						if($BillVoided){
							$PurchaseCogsData	= $this->ci->db->get_where('cogs_journal',array('orderId' => $orderId , 'status' => 1, 'createdJournalsId <>' => ''))->result_array();
							if($PurchaseCogsData){
								foreach($PurchaseCogsData as $PurchaseCogsDataTemp){
									$createdJournalsId	= $PurchaseCogsDataTemp['createdJournalsId'];
									$JournalAccout2Id	= $PurchaseCogsDataTemp['account2Id'];
									$JournalVoidRequest	= array(
										"ManualJournalID"	=>	$createdJournalsId,
										"Status"			=>	"VOIDED",
									);
									$this->headers		= array();
									$JournalVoidUrl		= '2.0/ManualJournals';
									$JournalVoidResult	= $this->getCurl($JournalVoidUrl, 'POST', json_encode($JournalVoidRequest), 'json', $JournalAccout2Id)[$JournalAccout2Id];
									if((strtolower($JournalVoidResult['Status']) == 'ok') AND (isset($JournalVoidResult['ManualJournals']['ManualJournal']['ManualJournalID']))){
										$this->ci->db->where('createdJournalsId', $createdJournalsId)->delete('cogs_journal');
									}
								}
							}
							$grniJournalData	= $this->ci->db->get_where('grni_journal',array('orderId' => $orderId , 'status' => 2, 'reversedJournalsId <>' => ''))->result_array();
							if($grniJournalData){
								$alreadyVoided	= array();
								foreach($grniJournalData as $grniJournalDataTemp){
									$reversedJournalsId		= $grniJournalDataTemp['reversedJournalsId'];
									if((!empty($alreadyVoided)) AND (in_array($reversedJournalsId, $alreadyVoided))){continue;}
									$JournalAccout2Id		= $grniJournalDataTemp['account2Id'];
									$JournalVoidRequest	= array(
										"ManualJournalID"	=>	$reversedJournalsId,
										"Status"			=>	"VOIDED",
									);
									$this->headers		= array();
									$JournalVoidUrl		= '2.0/ManualJournals';
									$JournalVoidResult	= $this->getCurl($JournalVoidUrl, 'POST', json_encode($JournalVoidRequest), 'json', $JournalAccout2Id)[$JournalAccout2Id];
									if((strtolower($JournalVoidResult['Status']) == 'ok') AND (isset($JournalVoidResult['ManualJournals']['ManualJournal']['ManualJournalID']))){
										$alreadyVoided[]	= $reversedJournalsId;
										$this->ci->db->update('grni_journal',array('reversedJournalsId' => '', 'status' => 1, 'isReversed' => 0),array('reversedJournalsId' => $reversedJournalsId));
									}
								}
							}
						}
						continue;
					}
				}
			}
			/* //		UNINVOICING CODE STARTS		// */
			
			if(($rowDatas['invoices']['0']['taxDate']) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['purchase'])) AND (strlen($dateLockSettings['purchase']) > 0)){
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['purchase'])));
				
				$checkTaxDate	= $rowDatas['invoices']['0']['taxDate'];
				$BPDateOffset	= (int)substr($checkTaxDate,23,3);
				$xeroOffset		= 0;
				$diff			= $BPDateOffset - $xeroOffset;
				$date			= new DateTime($checkTaxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff > 0){
					$diff		.= ' hour';
					$date->modify($diff);
				}
				$checkTaxDate	= $date->format('Ymd');
				
				if($dateLockSettings['purchaseCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['purchaseCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['purchaseCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			if($orderDatas['createOrderId']){continue;}
			if(!$orderDatas['invoiced']){continue;}
			if($allBatchInvoiceIDs[$orderId]){continue;}
			
			$CheckInvoiceNumbers	= array();
			$CheckInvoiceNumber		= array();
			$XeroInvoiceNumberArray	= array();
			
			$this->ci->db->reset_query();
			$CheckInvoiceNumbers	= $this->ci->db->select('invoiceRef')->get_where('purchase_order',array('account2Id' => $account2Id, 'invoiceRef <>'=> ''))->result_array();
			if($CheckInvoiceNumbers){
				foreach($CheckInvoiceNumbers as $CheckInvoiceNumber){
					if($CheckInvoiceNumber['invoiceRef']){
						$XeroInvoiceNumberArray[$CheckInvoiceNumber['invoiceRef']]	= $CheckInvoiceNumber;
					}
				}
			}
			
			//	CODE TO CHECK LINKED SO IS IN CONNECTOR OR NOT
			//	$config['InventoryManagementEnabled'] == 0 that means connector is inventory managed
			if($config['InventoryManagementEnabled'] == 0){
				if($orderDatas['LinkedWithSO']){
					$GetLiinkedSO	= $this->ci->db->get_where('sales_order',array('account2Id' => $account2Id, 'orderId' => $orderDatas['LinkedWithSO']))->row_array();
					if(!$GetLiinkedSO){
						$this->ci->db->update('purchase_order',array('message' => 'Parent SO is Not fetched yet'),array('orderId' => $orderDatas['orderId'],'account2Id' => $account2Id));
						continue;
					}
				}
			}
			
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$billAddress			= $rowDatas['parties']['billing'];
			$shipAddress			= $rowDatas['parties']['delivery'];
			$orderCustomer			= $rowDatas['parties']['supplier'];
			$uninvoiceCount			= $orderDatas['uninvoiceCount'];
			$CustomerCompanyName	= trim($customerMappings[$orderCustomer['contactId']]['company']);
			
			//temporary hard code for only hawkeopticsxero
			if($clientcode != 'hawkeopticsxero_invalid'){
				if(!$customerMappings[$orderCustomer['contactId']]['createdCustomerId']){
					$this->ci->db->update('purchase_order',array('message' => 'Supplier not sent yet.'),array('orderId' => $orderId));
					continue;
				}
			}
			//temporary hard code for only hawkeopticsxero
			if($clientcode == 'hawkeopticsxero_invalid'){
				if(!$CustomerCompanyName){
					$this->ci->db->reset_query();
					$this->ci->db->update('purchase_order',array('message' => 'Supplier Company Not Found.'),array('orderId' => $orderId));
					continue;
				}
			}
			
			if($createdRowData['lastTry']){
				if($createdRowData['lastTry'] > strtotime('-3 hour')){
					continue;
				}
			}
			
			$missingSkus			= array();
			$InvoiceLineAdd			= array();
			$productCreateIds		= array();
			$TaxComponents			= array();
			$InsertTaxResponse		= array();
			$InsertZipCodeInfo		= array();
			$TaxRatefromAvalara		= array();
			$taxDatas				= array();
			$avaZipDatas			= array();
			$totalItemDiscount		= array();
			$discountCouponAmt		= array();
			$isAvalaraTaxApplicable	= 0;
			$avalaraTaxError		= 0;
			$this->countRequest		= 0;
			$invoiceLineCount		= 0;
			$XeroTaxType			= '';
			$TaxType				= '';
			$TaxName				= '';
			$isDiscountCouponAdded	= '';
			$couponItemLineID		= '';
			$shippingLineID			= '';
			$DiscountItemAccountRef	= '';
			$BrightpearlTotalAmount	= $rowDatas['totalValue']['total'];
			$PostalCode				= $rowDatas['parties']['delivery']['postalCode'];
			$CountryIsoCode			= $rowDatas['parties']['delivery']['countryIsoCode'];
			$CountryIsoCode			= strtolower(trim($shipAddress['countryIsoCode3']));
			$countryState			= strtolower(trim($shipAddress['addressLine4']));
			
			foreach($rowDatas['orderRows'] as $rowId => $orderRows){
				if($orderRows['productId'] >= 1000){
					if(substr_count(strtolower($orderRows['productName']),'coupon')){
						if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
							$isDiscountCouponAdded	= 1;
							$couponItemLineID		= $rowId;
						}
					}
					if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){ 
						$shippingLineID	= $rowId;
					}
				}
				if($enableAvalaraTax){
					if($orderRows['productId'] > 1001){
						if(($orderRows['rowValue']['taxCode'] == '-') AND ($orderRows['rowValue']['rowTax']['value'] > 0)){
							if(strtolower($CountryIsoCode) == 'us'){
								$isAvalaraTaxApplicable	= 1; 
							}
						}
					}
				}
			}
			
			if($config['disableSkuDetails']){
				$ProductArray		= array();
				foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
					$bpNominal			= $rowdatass['nominalCode'];
					$bpTaxID			= $rowdatass['rowValue']['taxClassId'];
					$rowNet				= $rowdatass['rowValue']['rowNet']['value'];
					$rowTax				= $rowdatass['rowValue']['rowTax']['value'];
					$productId			= $rowdatass['productId'];
					$discountCouponAmt	= 0;
					$discountAmt		= 0;
					$AccountCode		= '';
					$params				= '';
					
					if($productId > 1001){
						if(!$productMappings[$productId]){
							$missingSkus[]	= $rowdatass['productSku'];
							continue;
						}
					}
					
					$params				= $productMappings[$productId]['params'];
					$params				= json_decode($params,true);
					
					$xeroTaxId			= '';
					$productId			= $rowdatass['productId'];
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax'];
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$taxMappingKey	= $bpTaxID;
					$taxMappingKey1	= $bpTaxID;
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode;
						}
					}
					$taxMapping		= array();
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$bpTaxID.'--'.$channelId])){
						$taxMapping	= $taxMappings[$bpTaxID.'--'.$channelId];
					}
					
					if($taxMapping){
						$xeroTaxId	= $taxMapping['account2TaxId'];
						if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsTaxId'])){
							$xeroTaxId  = $taxMapping['account2KidsTaxId'];
						}
					}
					else{
						$xeroTaxId	= $config['salesNoTaxCode'];
					}
					
					$AssetAccountRef	= $config['AssetAccountRef'];
					$ExpenseAccountRef	= $config['ExpenseAccountRef'];
					
					
					//if $config['InventoryManagementEnabled'] == true, means connector is non-inventory managed
					if($config['InventoryManagementEnabled'] == true){
						$AssetAccountRef	= $config['InventoryManagementProductPurchaseNominalCode'];
						$ExpenseAccountRef	= $config['InventoryManagementProductPurchaseNominalCode'];
						if($params){
							if(($params['id']) AND (!$params['stock']['stockTracked'])){
								$AssetAccountRef	= $config['ExpenseAccountRef'];
								$ExpenseAccountRef	= $config['ExpenseAccountRef'];
							}
						}
					}
					if(isset($nominalMappings[$rowdatass['nominalCode']])){
						if($nominalMappings[$rowdatass['nominalCode']]['account2NominalId']){
							$ExpenseAccountRef	= $nominalMappings[$rowdatass['nominalCode']]['account2NominalId'];
							$AssetAccountRef	= $nominalMappings[$rowdatass['nominalCode']]['account2NominalId'];
						}
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$rowdatass['nominalCode']]))){
						if($nominalChannelMappings[strtolower($channelId)][$rowdatass['nominalCode']]['account2NominalId']){
							$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$rowdatass['nominalCode']]['account2NominalId'];
							$AssetAccountRef	= $nominalChannelMappings[strtolower($channelId)][$rowdatass['nominalCode']]['account2NominalId'];
						}
					}
					
					$AccountCode	= $AssetAccountRef;
					if($params){
						if(($params['id']) AND (!$params['stock']['stockTracked'])){
							$AccountCode	= $ExpenseAccountRef;
						}
					}
					if($rowdatass['productId'] <= 1001){
						$AccountCode	= $ExpenseAccountRef;
					}
					//changes aftre cogs journals starts
					if($this->ci->globalConfig['enableCOGSJournals'] AND (!$orderDatas['LinkedWithSO'])){
						if($config['InventoryManagementEnabled'] == true){
							if($params['stock']['stockTracked']){
								$AccountCode	= $config['AssetAccountRef'];
								if(isset($nominalMappings[$rowdatass['nominalCode']])){
									if($nominalMappings[$rowdatass['nominalCode']]['account2NominalId']){
										$AccountCode	= $nominalMappings[$rowdatass['nominalCode']]['account2NominalId'];
									}
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$rowdatass['nominalCode']]))){
									if($nominalChannelMappings[strtolower($channelId)][$rowdatass['nominalCode']]['account2NominalId']){
										$AccountCode	= $nominalChannelMappings[strtolower($channelId)][$rowdatass['nominalCode']]['account2NominalId'];
									}
								}
							}
						}
					}
					//changes aftre cogs journals starts ends
					
					if($rowId == $couponItemLineID){
						if($rowdatass['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					if(!$config['SendTaxAsLineItem']){
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$AccountCode][$xeroTaxId])){
									$ProductArray['discount'][$AccountCode][$xeroTaxId]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$AccountCode][$xeroTaxId]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice	= $rowNet;
								}
								if($originalPrice > $rowNet){
									$discountCouponAmt	= $originalPrice - $rowNet;
									$rowNet				= $originalPrice;
									if($discountCouponAmt > 0){
										if(isset($ProductArray['discountCoupon'][$AccountCode][$xeroTaxId])){
											$ProductArray['discountCoupon'][$AccountCode][$xeroTaxId]['TotalNetAmt']	+= $discountCouponAmt;
										}
										else{
											$ProductArray['discountCoupon'][$AccountCode][$xeroTaxId]['TotalNetAmt']	= $discountCouponAmt;
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if(isset($ProductArray['aggregationItem'][$AccountCode][$xeroTaxId])){
								$ProductArray['aggregationItem'][$AccountCode][$xeroTaxId]['TotalNetAmt']	+= $rowNet;
								$ProductArray['aggregationItem'][$AccountCode][$xeroTaxId]['bpTaxTotal']	+= $rowTax;
							}
							else{
								$ProductArray['aggregationItem'][$AccountCode][$xeroTaxId]['TotalNetAmt']	= $rowNet;
								$ProductArray['aggregationItem'][$AccountCode][$xeroTaxId]['bpTaxTotal']	= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$AccountCode][$xeroTaxId])){
								$ProductArray['landedCost'][$AccountCode][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['landedCost'][$AccountCode][$xeroTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['landedCost'][$AccountCode][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['landedCost'][$AccountCode][$xeroTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$AccountCode][$xeroTaxId])){
								$ProductArray['shipping'][$AccountCode][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['shipping'][$AccountCode][$xeroTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['shipping'][$AccountCode][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['shipping'][$AccountCode][$xeroTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$AccountCode][$xeroTaxId])){
								$ProductArray['giftcard'][$AccountCode][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['giftcard'][$AccountCode][$xeroTaxId]['bpTaxTotal']			+= $rowTax;
							}
							else{
								$ProductArray['giftcard'][$AccountCode][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['giftcard'][$AccountCode][$xeroTaxId]['bpTaxTotal']			= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$AccountCode][$xeroTaxId])){
								$ProductArray['couponitem'][$AccountCode][$xeroTaxId]['TotalNetAmt']		+= $rowNet;
								$ProductArray['couponitem'][$AccountCode][$xeroTaxId]['bpTaxTotal']		+= $rowTax;
							}
							else{
								$ProductArray['couponitem'][$AccountCode][$xeroTaxId]['TotalNetAmt']		= $rowNet;
								$ProductArray['couponitem'][$AccountCode][$xeroTaxId]['bpTaxTotal']		= $rowTax;
							}
						}
					}
					else{
						if(($rowdatass['discountPercentage'] > 0)){
							$discountPercentage	= 100 - $rowdatass['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
							}
							$discountAmt	= $originalPrice - $rowNet;
							$rowNet			= $originalPrice;
							if($discountAmt > 0){
								if(isset($ProductArray['discount'][$AccountCode])){
									$ProductArray['discount'][$AccountCode]['TotalNetAmt']	+= $discountAmt;
								}
								else{
									$ProductArray['discount'][$AccountCode]['TotalNetAmt']	= $discountAmt;
								}
							}
						}
						elseif($isDiscountCouponAdded){
							if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
								$originalPrice		= $rowdatass['productPrice']['value'] * $rowdatass['quantity']['magnitude'];
								if(!$originalPrice){
									$originalPrice	= $rowNet;
								}
								if($originalPrice > $rowNet){
									$discountCouponAmt	= $originalPrice - $rowNet;
									$rowNet				= $originalPrice;
									if($discountCouponAmt > 0){
										if(isset($ProductArray['discountCoupon'][$AccountCode])){
											$ProductArray['discountCoupon'][$AccountCode]['TotalNetAmt']	+= $discountCouponAmt;
										}
										else{
											$ProductArray['discountCoupon'][$AccountCode]['TotalNetAmt']	= $discountCouponAmt;
										}
									}
								}
							}
						}
						if((!in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForShipping)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)) AND (!in_array($rowdatass['nominalCode'],$nominalCodeForDiscount))){
							if(isset($ProductArray['aggregationItem'][$AccountCode])){
								$ProductArray['aggregationItem'][$AccountCode]['TotalNetAmt']	+= $rowNet;
							}
							else{
								$ProductArray['aggregationItem'][$AccountCode]['TotalNetAmt']	= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']							+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']							= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForLandedCost)){
							if(isset($ProductArray['landedCost'][$AccountCode])){
								$ProductArray['landedCost'][$AccountCode]['TotalNetAmt']		+= $rowNet;
							}
							else{
								$ProductArray['landedCost'][$AccountCode]['TotalNetAmt']		= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']							+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']							= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForShipping)){
							if(isset($ProductArray['shipping'][$AccountCode])){
								$ProductArray['shipping'][$AccountCode]['TotalNetAmt']			+= $rowNet;
							}
							else{
								$ProductArray['shipping'][$AccountCode]['TotalNetAmt']			= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']							+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']							= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForGiftCard)){
							if(isset($ProductArray['giftcard'][$AccountCode])){
								$ProductArray['giftcard'][$AccountCode]['TotalNetAmt']			+= $rowNet;
							}
							else{
								$ProductArray['giftcard'][$AccountCode]['TotalNetAmt']			= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']							+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']							= $rowTax;
							}
						}
						if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
							if(isset($ProductArray['couponitem'][$AccountCode])){
								$ProductArray['couponitem'][$AccountCode]['TotalNetAmt']		+= $rowNet;
							}
							else{
								$ProductArray['couponitem'][$AccountCode]['TotalNetAmt']		= $rowNet;
							}
							if(isset($ProductArray['allTax'])){
								$ProductArray['allTax']['TotalNetAmt']							+= $rowTax;
							}
							else{
								$ProductArray['allTax']['TotalNetAmt']							= $rowTax;
							}
						}
					}
				}
				if($missingSkus){
					$missingSkus	= array_unique($missingSkus);
					$this->ci->db->update('purchase_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
					continue;
				}
				if($ProductArray){
					if(!$config['SendTaxAsLineItem']){
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'ItemCode'		=> $config['genericSku'],
											'Quantity'		=> 1,
											'UnitAmount'	=> sprintf("%.4f",$datas['TotalNetAmt']),
											'TaxType'		=> $Taxkeyproduct1,
											'AccountCode'	=> $BPNominalCode,
											'TaxAmount' 	=> sprintf("%.4f",$datas['bpTaxTotal']),
										);
										if($keyproduct == 'landedCost'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['landedCostItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Landed Cost';
											unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
										}
										if($keyproduct == 'shipping'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
										}
										if($keyproduct == 'giftcard'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
										}
										if($keyproduct == 'couponitem'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
										}
										if($keyproduct == 'discount'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
											$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
											unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
										}
										if($keyproduct == 'discountCoupon'){
											$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
											$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
											$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
										}
										
										$trackingDetails	= array();
										if(isset($channelMappings[$channelId])){
											$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
											if($trackingDetails){
												$trackingDetails	= explode("~=",$trackingDetails);
												$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
											}
										}
										$invoiceLineCount++;
									}
								}
							}
							else{
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									foreach($ProductArrayDatas as $Taxkeyproduct1 => $datas){
										$InvoiceLineAdd[$invoiceLineCount]	= array(
											'ItemCode'		=> $config['genericSku'],
											'Quantity'		=> 1,
											'UnitAmount'	=> sprintf("%.4f",($datas['TotalNetAmt'])),
											'TaxType'		=> $Taxkeyproduct1,
											'AccountCode'	=> $BPNominalCode,
											'TaxAmount'		=> sprintf("%.4f",$datas['bpTaxTotal']),
										);
										
										$trackingDetails	= array();
										if(isset($channelMappings[$channelId])){
											$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
											if($trackingDetails){
												$trackingDetails	= explode("~=",$trackingDetails);
												$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
											}
										}
										$invoiceLineCount++;
									}
								}
							}
						}
					}
					else{
						foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
							if(($keyproduct == 'landedCost') OR ($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
								foreach($ProductArrayDatasTemp as $BPNominalCode => $ProductArrayDatas){
									$InvoiceLineAdd[$invoiceLineCount]	= array(
										'ItemCode'		=> $config['genericSku'],
										'Quantity'		=> 1,
										'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
										'TaxType'		=> $config['salesNoTaxCode'],
										'AccountCode'	=> $BPNominalCode,
									);
									if($keyproduct == 'landedCost'){
										$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['landedCostItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Landed Cost';
										unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
									}
									if($keyproduct == 'shipping'){
										$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
									}
									if($keyproduct == 'giftcard'){
										$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
									}
									if($keyproduct == 'couponitem'){
										$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
									}
									if($keyproduct == 'discount'){
										$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
										$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
										unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
									}
									if($keyproduct == 'discountCoupon'){
										$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
										$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
										$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
									}
									$trackingDetails	= array();
									if(isset($channelMappings[$channelId])){
										$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
										if($trackingDetails){
											$trackingDetails	= explode("~=",$trackingDetails);
											$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
										}
									}
									$invoiceLineCount++;
								}
							}
							elseif($keyproduct == 'allTax'){
								if($ProductArrayDatasTemp['TotalNetAmt'] <= 0){
									continue;
								}
								$InvoiceLineAdd[$invoiceLineCount]	= array(
									'Description'	=> 'Total Tax Amount',
									'Quantity'		=> 1,
									'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
									'AccountCode'	=> $config['TaxItemLineNominal'],
									'TaxType'		=> $config['salesNoTaxCode'],
								);
								$trackingDetails	= array();
								if(isset($channelMappings[$channelId])){
									$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
									if($trackingDetails){
										$trackingDetails	= explode("~=",$trackingDetails);
										$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
									}
								}
								$invoiceLineCount++;
							}
						}
					}
				}
			}
			else{
				ksort($rowDatas['orderRows']);
				foreach($rowDatas['orderRows'] as $rowId => $orderRows){
					$isLandedCost	= 0;
					$ItemRefName	= '';
					$ItemRefValue	= '';
					$LineTaxId		= '';
					$productId		= '';
					if($rowId == $couponItemLineID){
						if($orderRows['rowValue']['rowNet']['value'] == 0){
							continue;
						}
					}
					$LineTaxId	= $config['PurchaseNoTaxCode'];
					$productId	= $orderRows['productId'];
					if($productId > 1001){
						if(@!$productMappings[$productId]['createdProductId']){
							$missingSkus[]	= $orderRows['productSku'];
							continue;
						}
						$ItemRefName	= $productMappings[$productId]['sku'];
						$ItemRefValue	= $productMappings[$productId]['sku'];
					}
					else{
						if($orderRows['rowValue']['rowNet']['value'] > 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= 'Shipping';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= 'Landed Cost';
								$isLandedCost	= 1;
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else if($orderRows['rowValue']['rowNet']['value'] < 0){
							$ItemRefValue	= $config['genericSku'];
							$ItemRefName	= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
						}
						else{
							$ItemRefValue		= $config['genericSku'];
							$ItemRefName		= $orderRows['productName'];
							if(in_array($orderRows['nominalCode'],$nominalCodeForDiscount)){
								$ItemRefValue	= $config['couponItem'];
								$ItemRefName	= 'Coupon Item';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForGiftCard)){
								$ItemRefValue	= $config['giftCardItem'];
								$ItemRefName	= $orderRows['productName'];
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForShipping)){
								$ItemRefValue	= $config['shippingItem'];
								$ItemRefName	= 'Shipping';
							}
							if(in_array($orderRows['nominalCode'],$nominalCodeForLandedCost)){
								$ItemRefValue	= $config['landedCostItem'];
								$ItemRefName	= 'Landed Cost';
								$isLandedCost	= 1;
							}
						}
					}
					
					$taxAmount			= $orderRows['rowValue']['rowTax']['value'];
					$kidsTaxCustomField	= $bpconfig['customFieldForKidsTax'];
					$isKidsTaxEnabled	= 0;
					if($kidsTaxCustomField){
						if($productMappings[$productId]){
							$productParams	= json_decode($productMappings[$productId]['params'], true);
							if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
								$isKidsTaxEnabled	= 1;
							}
						}
					}
					
					$taxMappingKey	= $orderRows['rowValue']['taxClassId'];
					$taxMappingKey1	= $orderRows['rowValue']['taxClassId'];
					if($isStateEnabled){
						if($isChannelEnabled){
							$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode.'-'.$countryState.'-'.$channelId;
							$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode.'-'.$channelId;
						}
						else{
							$taxMappingKey	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode.'-'.$countryState;
							$taxMappingKey1	= $orderRows['rowValue']['taxClassId'].'-'.$CountryIsoCode;
						}
					}
					
					$taxMapping		= array();
					$taxMappingKey	= strtolower($taxMappingKey);
					$taxMappingKey1	= strtolower($taxMappingKey1);
					if(isset($taxMappings[$taxMappingKey])){
						$taxMapping	= $taxMappings[$taxMappingKey];
					}
					elseif(isset($taxMappings[$taxMappingKey1])){
						$taxMapping	= $taxMappings[$taxMappingKey1];
					}
					elseif(isset($taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId])){
						$taxMapping	= $taxMappings[$orderRows['rowValue']['taxClassId'].'--'.$channelId];
					}
					
					if($taxMapping){
						$LineTaxId  = $taxMapping['account2TaxId'];
						if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsTaxId'])){
							$LineTaxId  = $taxMapping['account2KidsTaxId'];
						}
					}
					elseif($taxAmount > 0){
						$LineTaxId	= $config['PurchaseTaxCode'];
					}
					
					$price				= $orderRows['rowValue']['rowNet']['value'];
					$originalPrice		= $price;
					$discountPercentage	= 0;
					if(($orderRows['discountPercentage'] > 0)){
						if($config['discountPercentFormat'] != 3){	//check the discount format setting should not on 'Send net price'	//
							$DiscountItemAccountRef	= $orderRows['nominalCode'];
							$discountPercentage		= 100 - $orderRows['discountPercentage'];
							if($discountPercentage == 0){
								$originalPrice	= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							}
							else{
								$originalPrice	= round((($price * 100) / ($discountPercentage)),2);
							}
							$tempTaxAmt	= $originalPrice - $price;
							if($tempTaxAmt > 0){
								if(isset($totalItemDiscount[$LineTaxId])){
									$totalItemDiscount[$LineTaxId]	+= $tempTaxAmt;
								}
								else{
									$totalItemDiscount[$LineTaxId]	= $tempTaxAmt;
								}
							}
						}
					}
					elseif($isDiscountCouponAdded){
						if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
							$originalPrice			= $orderRows['productPrice']['value'] * $orderRows['quantity']['magnitude'];
							if(!$originalPrice){
								$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
							}
							if($originalPrice > $orderRows['rowValue']['rowNet']['value']){
								$discountCouponAmtTemp	= ($originalPrice - $price);
								if($discountCouponAmtTemp > 0){
									if(isset($discountCouponAmt[$LineTaxId])){
										$discountCouponAmt[$LineTaxId]	+= $discountCouponAmtTemp;
									}
									else{
										$discountCouponAmt[$LineTaxId]	= $discountCouponAmtTemp;
									}
								}
							}
							else{
								$originalPrice		= $orderRows['rowValue']['rowNet']['value'];
							}
						}
					}
					
					$params				= '';
					$params				= $productMappings[$productId]['params'];
					$params				= json_decode($params,true);
					
					$nominalMappingSet	= 0;
					$AssetAccountRef	= $config['AssetAccountRef'];
					$ExpenseAccountRef	= $config['ExpenseAccountRef'];
					
					//if $config['InventoryManagementEnabled'] == true, means connector is non-inventory managed
					if($config['InventoryManagementEnabled'] == true){
						$AssetAccountRef	= $config['InventoryManagementProductPurchaseNominalCode'];
						$ExpenseAccountRef	= $config['InventoryManagementProductPurchaseNominalCode'];
						if($params){
							if(($params['id']) AND (!$params['stock']['stockTracked'])){
								$AssetAccountRef	= $config['ExpenseAccountRef'];
								$ExpenseAccountRef	= $config['ExpenseAccountRef'];
							}
						}
					}
					
					if(isset($nominalMappings[$orderRows['nominalCode']])){
						if($nominalMappings[$orderRows['nominalCode']]['account2NominalId']){
							$AssetAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
							$ExpenseAccountRef	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
						}
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]))){
						if($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId']){
							$AssetAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
							$ExpenseAccountRef	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
						}
					}
					
					$AccountCode	= $AssetAccountRef;
					if($params){
						if(($params['id']) AND (!$params['stock']['stockTracked'])){
							$AccountCode	= $ExpenseAccountRef;
						}
					}
					if($orderRows['productId'] <= 1001){
						$AccountCode	= $ExpenseAccountRef;
					}
					
					//changes aftre cogs journals starts
					if($this->ci->globalConfig['enableCOGSJournals'] AND (!$orderDatas['LinkedWithSO'])){
						if($config['InventoryManagementEnabled'] == true){
							if($params['stock']['stockTracked']){
								$AccountCode	= $config['AssetAccountRef'];
								if(isset($nominalMappings[$orderRows['nominalCode']])){
									if($nominalMappings[$orderRows['nominalCode']]['account2NominalId']){
										$AccountCode	= $nominalMappings[$orderRows['nominalCode']]['account2NominalId'];
									}
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]))){
									if($nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId']){
										$AccountCode	= $nominalChannelMappings[strtolower($channelId)][$orderRows['nominalCode']]['account2NominalId'];
									}
								}
							}
						}
					}
					//changes aftre cogs journals starts ends

					$description	= $orderRows['productName'];
					$UnitAmount		= 0.00;
					if($originalPrice != 0){
						$UnitAmount	= $originalPrice / $orderRows['quantity']['magnitude'];
					}
					$InvoiceLineAdd[$invoiceLineCount]	= array(
						'ItemCode'		=> $ItemRefValue,
						'Description'	=> $description,
						'Quantity'		=> (int)$orderRows['quantity']['magnitude'],
						'UnitAmount'	=> sprintf("%.4f",$UnitAmount),
						'TaxType'		=> $LineTaxId,
						'AccountCode'	=> $AccountCode,
						'TaxAmount'		=> sprintf("%.4f",$orderRows['rowValue']['rowTax']['value']),
					);
					
					if($InvoiceLineAdd[$invoiceLineCount]['ItemCode'] == $config['couponItem']){
						if($nominalMappingSet == 0){
							unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
						}
					}
					
					if($isLandedCost){
						unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
					}
					
					/* if(in_array($clientcode, $skuTrimmingClientCodes)){ */
					if($config['InventoryManagementEnabled'] == true){
						$InvoiceLineAdd[$invoiceLineCount]['ItemCode']	= substr($InvoiceLineAdd[$invoiceLineCount]['ItemCode'],0,30);
					}
					
					if(!$LineTaxId){
						unset($InvoiceLineAdd[$invoiceLineCount]['TaxType']);
					}
					
					if(isset($channelMappings[$channelId])){
						$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
						if($channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
							$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
						}
						if($trackingDetails){
							$trackingDetails	= explode("~=",$trackingDetails);
							$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
						}
					}
					$invoiceLineCount++;
				}
				if($missingSkus){
					$missingSkus	= array_unique($missingSkus);
					$this->ci->db->update('purchase_order',array('message' => 'Missing sku : '.implode(",",$missingSkus)),array('orderId' => $orderId));
					continue;
				}
				if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
					if($discountCouponAmt){
						foreach($discountCouponAmt as $TaxID => $discountCouponLineAmount){
							$InvoiceLineAdd[$invoiceLineCount]	= array(
								'ItemCode'		=> $config['couponItem'],
								'Description'	=> 'Coupon Discount',
								'Quantity'		=> 1,
								'UnitAmount'	=> (-1) * sprintf("%.4f",$discountCouponLineAmount),
								'TaxAmount'		=> 0.00,
							);
							if($TaxID){
								$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxID;
							}
							else{
								$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
							}
							if(isset($channelMappings[$channelId])){
								$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
								if($channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
									$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
								}
								if($trackingDetails){
									$trackingDetails	= explode("~=",$trackingDetails);
									$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
								}
							}				
							$invoiceLineCount++;
						}
					}
				}
				if($totalItemDiscount){
					foreach($totalItemDiscount as $TaxID => $totalItemDiscountLineAmount){
						$mappedDiscountItemAccRef	= '';
						if($config['OverrideDiscountItemAccRef']){
							if($DiscountItemAccountRef){
								if(isset($nominalMappings[$DiscountItemAccountRef])){
									if($nominalMappings[$DiscountItemAccountRef]['account2NominalId']){
										$mappedDiscountItemAccRef	= $nominalMappings[$DiscountItemAccountRef]['account2NominalId'];
									}
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$DiscountItemAccountRef]))){
									if($nominalChannelMappings[strtolower($channelId)][$DiscountItemAccountRef]['account2NominalId']){
										$mappedDiscountItemAccRef	= $nominalChannelMappings[strtolower($channelId)][$DiscountItemAccountRef]['account2NominalId'];
									}
								}
							}
						}
						$InvoiceLineAdd[$invoiceLineCount]	= array(
							'ItemCode'		=> $config['discountItem'],
							'Description'	=> 'Item Discount',
							'Quantity'		=> 1,
							'UnitAmount'	=> (-1) * sprintf("%.4f",$totalItemDiscountLineAmount),
							'TaxAmount'		=> 0.00,
						);
						if($config['OverrideDiscountItemAccRef']){
							if($mappedDiscountItemAccRef){
								$InvoiceLineAdd[$invoiceLineCount]['AccountCode']	= $mappedDiscountItemAccRef;
							}
						}
						if($TaxID){
							$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $TaxID;
						}
						else{
							$InvoiceLineAdd[$invoiceLineCount]['TaxType']	= $config['salesNoTaxCode'];
						}
						if(isset($channelMappings[$channelId])){
							$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
							if($channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
								$trackingDetails	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
							}
							if($trackingDetails){
								$trackingDetails	= explode("~=",$trackingDetails);
								$InvoiceLineAdd[$invoiceLineCount]['Tracking']	= array(array('Name' => $trackingDetails['0'],'Option' => $trackingDetails['1']));
							}
						}
						$invoiceLineCount++;
					}
				}
			}
			
			$DeliveryDate	= date('c');
			$taxDate		= date('c');
			$dueDate		= date('c');
			if($rowDatas['delivery']['deliveryDate']){
				$DeliveryDate	= $rowDatas['delivery']['deliveryDate'];
			}
			if($rowDatas['invoices']['0']['taxDate']){
				$taxDate		= $rowDatas['invoices']['0']['taxDate'];
			}
			if($rowDatas['invoices']['0']['dueDate']){
				$dueDate		= $rowDatas['invoices']['0']['dueDate'];
			}
			
			//taxdate chanages
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate		= $date->format('Y-m-d');
				
				
				$BPDateOffset	= substr($dueDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($dueDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$dueDate		= $date->format('Y-m-d');
			}
			else{
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$xeroOffset		= 0;
				$diff			= $BPDateOffset - $xeroOffset;
				$date1			= new DateTime($dueDate);
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date1->setTimezone(new DateTimeZone($BPTimeZone));
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff > 0){
					$diff			.= ' hour';
					$date->modify($diff);
					$date1->modify($diff);
				}
				$taxDate		= $date->format('Y-m-d');
				$dueDate		= $date1->format('Y-m-d');
			}
			
			$InvoiceNumber	= '';
			$InvoiceNumber	= $orderId;
			if($rowDatas['invoices']['0']['invoiceReference']){
				$InvoiceNumber	= $rowDatas['invoices']['0']['invoiceReference'];
			}
			if($clientcode == 'dhallandnashxero'){
				$InvoiceNumber	= $orderId;
			}
			if($uninvoiceCount > 0){
				$InvoiceNumber	= $InvoiceNumber.'/New-0'.$uninvoiceCount;
			}
			
			//CODE TO CHECK EXISTING DUPLICATE INVOICE NUMBER
			if($XeroInvoiceNumberArray[$InvoiceNumber]){
				$InvoiceNumber	= $InvoiceNumber.' - '.$orderId;
			}
			
			
			$finalOrderNotes	= '';
			$notesType			= '';
			if($config['notesOnPurchaseInvoice']){
				$allOrderNotes			= array();
				$notesOnPurchaseInvoice	= explode(",",$config['notesOnPurchaseInvoice']);
				foreach($notesOnPurchaseInvoice as $notesOnPurchaseInvoiceTemp){
					if($config['PrefixForNotes']){
						$notesType	= $config['PrefixForNotes'];
					}
					
					$ValueForreference	= '';
					$account1FieldIds	= explode(".",$notesOnPurchaseInvoiceTemp);
					$fieldValue			= '';
					$fieldValueTmps		= '';
					
					foreach($account1FieldIds as $account1FieldId){
						if(!$fieldValueTmps){
							$fieldValueTmps	= @$rowDatas[$account1FieldId];
						}
						else{
							$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
						}
					}
					$ValueForreference	= $fieldValueTmps;
					if($ValueForreference){
						$allOrderNotes[]	= trim($ValueForreference);
					}
				}
				if($allOrderNotes){
					if(count($allOrderNotes) > 1){
						$finalOrderNotes	= implode(", ",$allOrderNotes);
					}
					else{
						$finalOrderNotes	= $allOrderNotes[0];
					}
					if($finalOrderNotes AND $notesType){
						$finalOrderNotes	= $notesType. ' : '. $finalOrderNotes;
					}
				}
			}
			
			foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
				if($InvoiceLineAddTemp['UnitAmount'] > 0){
					if($InvoiceLineAddTemp['UnitAmount'] < '0.0050'){
						$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
					}
				}
				else{
					if($InvoiceLineAddTemp['UnitAmount'] > '-0.0050'){
						$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
					}
				}
			}
			
			if($config['disableSkuDetails']){
				$invoiceLineAddNew		= array();
				$invoiceLineFormatted	= array();
				$newItemLineCount		= 0;
				foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
					if($InvoiceLineAddTemp['ItemCode'] == $config['genericSku']){
						$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['discountItem']){
						$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['couponItem']){
						$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['landedCostItem']){
						$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['shippingItem']){
						$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['Description'] == $config['Total Tax Amount']){
						$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
						continue;
					}
					if($InvoiceLineAddTemp['ItemCode'] == $config['giftCardItem']){
						$invoiceLineAddNew[7][]	= $InvoiceLineAddTemp;
						continue;
					}
				}
				ksort($invoiceLineAddNew);
				foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
					foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
						$invoiceLineFormatted[$newItemLineCount]	= $invoiceLineAddNewTempTemp;
						$newItemLineCount++;
					}
				}
				if($invoiceLineFormatted){
					$InvoiceLineAdd		= $invoiceLineFormatted;
					$invoiceLineCount	= $newItemLineCount;
				}
			}
			
			$request	= array(
				'Type'				=> 'ACCPAY',
				'Contact'			=> array(
					'ContactID'			=> $customerMappings[$orderCustomer['contactId']]['createdCustomerId'],
					'Addresses'			=> array(
						array(
							'AddressType'	=> 'STREET',
							'AddressLine1'	=> $shipAddress['addressLine1'],
							'AddressLine2'	=> $shipAddress['addressLine2'],
							'City'			=> $shipAddress['addressLine3'],
							'Region'		=> $shipAddress['addressLine4'],
							'PostalCode'	=> $shipAddress['postalCode'],
							'Country'		=> $shipAddress['countryIsoCode'],
						),
						array(
							'AddressType'	=> 'POBOX',
							'AddressLine1'	=> $billAddress['addressLine1'],
							'AddressLine2'	=> $billAddress['addressLine2'],
							'City'			=> $billAddress['addressLine3'],
							'Region'		=> $billAddress['addressLine4'],
							'PostalCode'	=> $billAddress['postalCode'],
							'Country'		=> $billAddress['countryIsoCode'],
						),
					),
				),
				'InvoiceNumber'		=> $InvoiceNumber,
				'Reference'			=> $orderId,
				'Status'			=> 'AUTHORISED',
				'CurrencyCode'		=> $rowDatas['currency']['orderCurrencyCode'],
				'CurrencyRate'		=> ($rowDatas['currency']['exchangeRate'])?($rowDatas['currency']['exchangeRate']):1,
				'DeliveryAddress'	=> implode(",",array_filter(array_unique($shipAddress))),
				'Telephone'			=> $shipAddress['currency']['telephone'],
				'Date'				=> $taxDate,
				'dueDate'			=> $dueDate,
				'LineItems'			=> $InvoiceLineAdd
			);
			
			if($config['sendPurchaseAs'] == 2){
				$request['Status']	= 'DRAFT';
			}
			if($config['sendPurchaseAs'] == 3){
				$request['Status']	= 'SUBMITTED';
			}
			
			//temporary hard code for only hawkeopticsxero
			if($clientcode == 'hawkeopticsxero_invalid'){
				unset($request['Contact']['ContactID']);
				$request['Contact']['Name']	= $CustomerCompanyName;
			}
			
			if($config['defaultCurrency'] != $bpconfig['currencyCode']){
				unset($request['CurrencyRate']);
			}
			
			$this->headers	= array();
			$url			= '2.0/Invoices';
			if($accountDetails['OAuthVersion'] == 2){
				$url		= '2.0/Invoices?unitdp=4';
				$this->initializeConfig($account2Id, 'PUT', $url);
			}
			else{
				$url		= '2.0/Invoices';
				$UrlParams	= array('unitdp' => '4');
				$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
				$url		= '2.0/Invoices?unitdp='.urlencode('4');
			}
			$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
			$createdRowData	= array(
				'Request data	: '	=> $request,
				'Response data	: '	=> $results,
			);
		
			$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
			if((strtolower($results['Status']) == 'ok') AND (isset($results['Invoices']['Invoice']['InvoiceID']))){
				if($finalOrderNotes){
					$HistoryRequest	= array(
						"HistoryRecords"	=> array(
							array(
								"Details" => $finalOrderNotes,
							),
						),
					);
					$Historyurl		= '2.0/Invoices/'.$results['Invoices']['Invoice']['InvoiceID'].'/history';
					$this->initializeConfig($account2Id, 'PUT', $Historyurl);
					$Historyresults	= $this->getCurl($Historyurl, 'PUT', json_encode($HistoryRequest), 'json', $account2Id)[$account2Id];
					$createdRowData['Notes Request']	= $HistoryRequest;
					$createdRowData['Notes Results']	= $Historyresults;
				}
				
				$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData), 'status' => '1','invoiced' => '0','invoiceRef' => $InvoiceNumber,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID'],'message'=>'','PostedTime' => date('c'), 'postedType' => $request['Status']),array('orderId' => $orderId));
			
				$XeroTotalAmount	= $results['Invoices']['Invoice']['Total'];
				$NetRoundOff		= $BrightpearlTotalAmount - $XeroTotalAmount;
				$RoundOffCheck		= abs($NetRoundOff);
				$RoundOffApplicable	= 0;
				if($RoundOffCheck != 0){
					if($RoundOffCheck < 0.99){
						$RoundOffApplicable	= 1;
					}
					if($RoundOffApplicable){
						$InvoiceLineAdd[$invoiceLineCount] = array(
							'ItemCode'		=> $config['roundOffItem'],
							'Description'	=> $config['roundOffItem'],
							'Quantity'		=> 1,
							'UnitAmount'	=> sprintf("%.4f",$NetRoundOff),
							'TaxType'		=> $config['salesNoTaxCode'],
						);
						$request['LineItems']		= $InvoiceLineAdd;
						$request['InvoiceID']		= $results['Invoices']['Invoice']['InvoiceID'];
						
						$this->headers	= array();
						$url			= '2.0/Invoices';
						if($accountDetails['OAuthVersion'] == 2){
							$url	= '2.0/Invoices?unitdp=4';
							$this->initializeConfig($account2Id, 'POST', $url);
						}
						else{
							$url		= '2.0/Invoices';
							$UrlParams	= array('unitdp' => '4');
							$this->initializeConfig($account2Id, 'POST', $url,$UrlParams);
							$url		= '2.0/Invoices?unitdp='.urlencode('4');
						}
						$results2		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$createdRowData['Rounding Request data	: ']	= $request;
						$createdRowData['Rounding Response data	: ']	= $results2;
						if((strtolower($results2['Status']) == 'ok') AND (isset($results2['Invoices']['Invoice']['InvoiceID']))){
							$this->ci->db->update('purchase_order',array('createOrderId' => $results2['Invoices']['Invoice']['InvoiceID'], 'createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
						}
						else{
							$this->ci->db->update('purchase_order',array('message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)),array('id' => $orderDatas['id']));
						}
					}
				}
			}
			else{
				$badRequest	= 0;
				$errormsg	= array();
				$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
				$errormsg	= json_decode($errormsg,true);
				if(isset($errormsg['head']['title'])){
					if($errormsg['head']['title'] == '400 Bad Request'){
						$badRequest	= 1;
					}
				}
				if(!$badRequest){
					$createdRowData['lastTry']	= strtotime('now');
					$this->ci->db->update('purchase_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
				}
			}
		}
	}
}
$this->postPurchaseBatchInvoice($orgObjectId);
$this->fetchPurchasePayment();
$this->fetchPurchaseConsolPayment();
?>