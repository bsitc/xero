<?php
//XERO_DEMO		: 	POST CONSOL SALES TO XERO
include_once('timezonesData.php');
$dateResults	= array(1=> array('name'=> 'JAN','lastDate'=> '31',),2=> array('name'=> 'FEB','lastDate'=> '28','lastDate2'=> '29',),3=> array('name'=> 'MAR','lastDate'=> '31',),4=> array('name'=> 'APR','lastDate'=> '30',),5=> array('name'=> 'MAY','lastDate'=> '31',),6=> array('name'=> 'JUN','lastDate'=> '30',),7=> array('name'=> 'JUL','lastDate'=> '31',),8=> array('name'=> 'AUG','lastDate'=> '31',),9=> array('name'=> 'SEP','lastDate'=> '30',),10=> array('name'=> 'OCT','lastDate'=> '31',),11=> array('name'=> 'NOV','lastDate'=> '30',),12=> array('name'=> 'DEC','lastDate'=> '31',),);

$this->reInitialize();
$enableAggregation							= $this->ci->globalConfig['enableAggregation'];
$enableConsolidationMappingCustomazation	= $this->ci->globalConfig['enableConsolidationMappingCustomazation'];
$clientcode									= $this->ci->config->item('clientcode');
$dateLockSettings							= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$this->ci->globalConfig['enableAggregation']){continue;}
	$config	= $this->accountConfig[$account2Id];
	
	$this->ci->db->reset_query();
	$datas	= $this->ci->db->order_by('orderId', 'asc')->get_where('sales_order',array('status' => '0', 'account2Id' => $account2Id))->result_array();
	if(empty($datas)){continue;}
	
	$this->ci->db->reset_query();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	if(!empty($AggregationMappingsTemps)){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			
			if(!$ConsolMappingCustomField AND !$account1APIFieldId){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]	= $AggregationMappingsTemp;
			}
			else{
				if($account1APIFieldId){
					$allAPIFieldsValues	= explode("||", trim($account1APIFieldId));
					foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
					}
				}
				else{
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
				}
			}
		}
	}
	if((empty($AggregationMappings)) AND (empty($AggregationMappings2))){continue;}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if(!empty($channelMappingsTemps)){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$ChannelcustomfieldMappingsTemps	= $this->ci->db->order_by('id','desc')->get_where('mapping_channelcustomfield',array('account2Id' => $account2Id))->result_array();
	$ChannelcustomfieldMappingKey		= '';
	$ChannelcustomfieldMappings			= array();
	if(!empty($ChannelcustomfieldMappingsTemps)){
		foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
			if($ChannelcustomfieldMappingsTemp['account1CustomFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1CustomFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1APIFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1APIFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1WarehouseId']){
				$ChannelcustomfieldMappingKey	= 'account1WarehouseId';
				break;
			}
			else{
				break;
			}
		}
		if($ChannelcustomfieldMappingKey){
			foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
				if($ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey]){
					$ChannelcustomfieldChannel		= $ChannelcustomfieldMappingsTemp['account1ChannelId'];
					$ChannelcustomfieldKey2Value	= $ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey];
					$ChannelcustomfieldMappings[$ChannelcustomfieldChannel][$ChannelcustomfieldKey2Value]	= $ChannelcustomfieldMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <> ' => '2'))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	$taxMappings		= array();
	if(!empty($taxMappingsTemps)){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){
					$isStateEnabled	= 1;
				}
				if($taxMappingsTemp['countryName']){
					$isStateEnabled = 1;
				}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp	= explode(",",trim($taxMappingsTemp['stateName']));
			if($taxMappingsTemp['stateName']){
				foreach($stateTemp as $Statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$productcategoryMappingTemps	= $this->ci->db->get_where('mapping_productcategory',array('account1CategoryId <>' => '','account2Id' => $account2Id))->result_array();
	$productcategoryMappings		= array();
	$allMappedCategory				= array();
	if(!empty($productcategoryMappingTemps)){
		foreach($productcategoryMappingTemps as $productcategoryMappingTemp){
			$allMappedCategory[]	= $productcategoryMappingTemp['account1CategoryId'];
			$productcategoryMappings[$productcategoryMappingTemp['account1CategoryId']]	= $productcategoryMappingTemp;
		}
	}
	$allMappedCategory	= array_filter($allMappedCategory);
	$allMappedCategory	= array_unique($allMappedCategory);
	
	$productMappings	= array();
	if($this->ci->globalConfig['enableAdvanceTaxMapping'] OR ($this->ci->globalConfig['enableProductcategoryMapping'])){
		$this->ci->db->reset_query();
		$productMappingsTemps	= $this->ci->db->select('productId,createdProductId,name,sku,params')->get_where('products',array('account2Id' => $account2Id))->result_array();
		if(!empty($productMappingsTemps)){
			foreach($productMappingsTemps as $productMappingsTemp){
				$productMappings[$productMappingsTemp['productId']]		= $productMappingsTemp;
			}
		}
	}
	
	$nominalCodeForShipping	= explode(",",trim($config['nominalCodeForShipping']));
	$nominalCodeForGiftCard	= explode(",",trim($config['nominalCodeForGiftCard']));
	$nominalCodeForDiscount	= explode(",",trim($config['nominalCodeForDiscount']));
	
	if(!empty($datas)){
		$OrdersByChannel				= array();
		$OrdersByChannelCurrency		= array();
		$OrdersByChannelCurrencyTaxDate	= array();
		foreach($datas as $orderDatas){
			$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
			$taxDate				= '';
			$orderId				= $orderDatas['orderId'];
			$rowDatas				= json_decode($orderDatas['rowData'],true);
			$taxDate				= $rowDatas['invoices'][0]['taxDate'];
			$channelId				= $rowDatas['assignment']['current']['channelId'];
			$CustomFieldValueID			= '';
			if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
				$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
			}
			$CurrencyCode			= strtolower($rowDatas['currency']['orderCurrencyCode']);
			$accountingCurrencyCode	= strtolower($rowDatas['currency']['accountingCurrencyCode']);
			$ConsolAPIFieldValueID	= '';
			
			if(!$channelId OR !$CurrencyCode OR !$taxDate OR !$accountingCurrencyCode){
				continue;
			}
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			if($ConsolAPIFieldValueID){
				$CustomFieldValueID	= $ConsolAPIFieldValueID;
			}
			
			if($enableAggregation){
				if($AggregationMappings){
					if((!$AggregationMappings[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings[$channelId]['bpaccountingcurrency'])){
						continue;
					}
					else{
						if($enableConsolidationMappingCustomazation){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']){
								if(($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField'])){
									$ExcludedStringInCustomField		= array();
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId]['bpaccountingcurrency']['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId]['bpaccountingcurrency']['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
							else{
								if(($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']) AND ($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField'])){
									$ExcludedStringInCustomField		= array();
									$IsConsolApplicable					= 0;
									$IsNonConsolApplicable				= 0;
									$AggregatedCustomFieldName			= strtoupper($AggregationMappings[$channelId][strtolower($CurrencyCode)]['brightpearlCustomFieldName']);
									$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
									$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings[$channelId][strtolower($CurrencyCode)]['ExcludedStringInCustomField']));
									$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
									if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
										foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
											$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
											if(is_array($AggregatedCustomFieldData)){
												if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
											else{
												if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
													$IsNonConsolApplicable	= 1;
													$IsConsolApplicable		= 0;
													break;
												}
												else{
													$IsConsolApplicable		= 1;
													$IsNonConsolApplicable	= 0;
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
					}
				}
				elseif($AggregationMappings2){
					if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency'])){
						continue;
					}
					else{
						if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
							if((!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'])){
								continue;
							}
							else{
								if($enableConsolidationMappingCustomazation){
									if(($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
									elseif(($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
						else{
							if((!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]) AND (!$AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA'])){
								continue;
							}
							else{
								if($enableConsolidationMappingCustomazation){
									if(($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
											if($IsNonConsolApplicable){
												continue;
											}
										}
									}
									elseif(($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']) AND ($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField'])){
										$IsConsolApplicable					= 0;
										$IsNonConsolApplicable				= 0;
										$ExcludedStringInCustomField		= array();
										$AggregatedCustomFieldName			= strtoupper($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['brightpearlCustomFieldName']);
										$AggregatedCustomFieldData			= $rowDatas['customFields'][$AggregatedCustomFieldName];
										if(!$AggregatedCustomFieldData){
											$account1APIFieldIds	= explode(".",$AggregatedCustomFieldName);
												$APIfieldValueTmps		= '';
												foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
													if(!$APIfieldValueTmps){
														$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
													}
													else{
														$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
													}
												}
												if($APIfieldValueTmps){
													$AggregatedCustomFieldData	= $APIfieldValueTmps;
												}
										}
										$AggregatedCustomFieldDataInMapping	= trim(strtolower($AggregationMappings2[$channelId][strtolower($CurrencyCode)]['NA']['ExcludedStringInCustomField']));
										$ExcludedStringInCustomField		= explode("||",trim($AggregatedCustomFieldDataInMapping));
										if($AggregatedCustomFieldData AND $ExcludedStringInCustomField){
											foreach($ExcludedStringInCustomField as $ExcludedStringInCustomFieldTemp){
												$ExcludedStringInCustomFieldTemp	= trim(strtolower($ExcludedStringInCustomFieldTemp));
												if(is_array($AggregatedCustomFieldData)){
													if(substr_count(strtolower($AggregatedCustomFieldData['value']),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
												else{
													if(substr_count(strtolower($AggregatedCustomFieldData),$ExcludedStringInCustomFieldTemp)){
														$IsNonConsolApplicable	= 1;
														$IsConsolApplicable		= 0;
														break;
													}
													else{
														$IsConsolApplicable		= 1;
														$IsNonConsolApplicable	= 0;
													}
												}
											}
										}
										if($IsNonConsolApplicable){
											continue;
										}
									}
								}
							}
						}
					}
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						if(!$AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
							$CustomFieldValueID	= 'NA';
						}
					}
					else{
						if(!$AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID]){
							$CustomFieldValueID	= 'NA';
						}
					}
				}
			}
			
			$AggregationMappingData	= array();
			if($AggregationMappings){
				if($AggregationMappings[$channelId]['bpaccountingcurrency']){
					$AggregationMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
					$CustomFieldValueID		= 'NA';
				}
				else{
					$AggregationMappingData	= $AggregationMappings[$channelId][strtolower($CurrencyCode)];
					$CustomFieldValueID		= 'NA';
				}
			}
			elseif($AggregationMappings2){
				if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
					$AggregationMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
				}
				else{
					$AggregationMappingData	= $AggregationMappings2[$channelId][strtolower($CurrencyCode)][$CustomFieldValueID];
				}
			}
			
			$consolFrequency		= $AggregationMappingData['consolFrequency'];
			$netOffConsolidation	= $AggregationMappingData['netOffConsolidation'];
			if(!$consolFrequency){
				$consolFrequency	= 1;
			}
			if($netOffConsolidation){
				continue;
			}
			
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate		= $date->format('Ymd');
			}
			else{
				$BPDateOffset	= (int)substr($taxDate,23,3);
				$Acc2Offset		= 0;
				$diff			= $BPDateOffset - $Acc2Offset;
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
			}
			
			if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['consolsales'])) AND (strlen($dateLockSettings['consolsales']) > 0)){
				$checkTaxDate	= date('Ymd',strtotime($taxDate));
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['consolsales'])));
				
				if($dateLockSettings['consolsalesCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['consolsalesCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['consolsalesCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			//consolFrequency : 1 => Daily, 2 => Monthly
			if($consolFrequency == 1){
				$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
				if($taxDate > $fetchTaxDate){
					continue;
				}
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$taxDate][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]				= $orderDatas;
					}
				}
			}
			elseif($consolFrequency == 2){
				$invoiceMonth	= date('m',strtotime($taxDate));
				$currentMonth	= date('m');
				if((($currentMonth - $invoiceMonth) != 1) AND (($currentMonth - $invoiceMonth) != '-11')){
					continue;
				}
				
				$batchKey	= date('y-m',strtotime($taxDate));
				if($AggregationMappings){
					if($AggregationMappings[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
					}
				}
				elseif($AggregationMappings2){
					if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId]['bpaccountingcurrency'][$CustomFieldValueID][$orderId]	= $orderDatas;
					}
					else{
						$OrdersByChannelCurrencyTaxDate[$batchKey][$channelId][$CurrencyCode][$CustomFieldValueID][$orderId]			= $orderDatas;
					}
				}
			}
		}
		
		if($OrdersByChannelCurrencyTaxDate){
			foreach($OrdersByChannelCurrencyTaxDate as $taxDateBP => $OrdersByChannelCurrency){
				foreach($OrdersByChannelCurrency as $SalesChannel => $OrdersByCurrency){
					foreach($OrdersByCurrency as $rootCurrency => $OrdersByChannelsCustom){
						foreach($OrdersByChannelsCustom as $CustomFieldValueID => $OrdersByChannels){
							$invalidConsolOrderIds	= array();
							$rootCurrency			= $rootCurrency;
							$CurrencyCode			= $rootCurrency;
							$BrightpealBaseCurrency	= '';
							$CustomFieldValueID		= $CustomFieldValueID;
							$results				= array();
							$AggregationMapping		= array();
							$journalAggregation		= 0;
							$ChannelCustomer		= '';
							$consolFrequency		= 0;
							
							if($AggregationMappings){
								$AggregationMapping	= $AggregationMappings[$SalesChannel][strtolower($rootCurrency)];
							}
							elseif($AggregationMappings2){
								$AggregationMapping	= $AggregationMappings2[$SalesChannel][strtolower($rootCurrency)][$CustomFieldValueID];
							}
							else{
								continue;
							}
							if(!$AggregationMapping){
								continue;
							}
							
							$orderForceCurrency		= $AggregationMapping['orderForceCurrency'];
							$consolFrequency		= $AggregationMapping['consolFrequency'];
							if(!$consolFrequency){
								$consolFrequency	= 1;
							}
							
							if(($rootCurrency == 'bpaccountingcurrency') AND (!$orderForceCurrency)){
								$invalidConsolOrderIds	= array_keys($OrdersByChannels);
								$this->ci->db->where_in('orderId',$invalidConsolOrderIds)->update('sales_order',array('message' => 'Invalid Consolidation Mapping'));
								continue;
							}
							
							$ChannelCustomer		= $AggregationMapping['account2ChannelId'];
							if(!$ChannelCustomer){continue;}
							
							$postOtions				= $AggregationMapping['postOtions'];
							$disableSalesPosting	= 0;
							$disableCOGSPosting		= 0;
							if($postOtions){
								if($postOtions == 1){
									$disableSalesPosting	= 1;
								}
								elseif($postOtions == 2){
									$disableCOGSPosting		= 1;
								}
							}
							
							$uniqueInvoiceRef		= $AggregationMapping['uniqueChannelName'];
							$BPTotalAmount			= 0;
							$BPTotalAmountBase		= 0;
							$OrderCount				= 0;
							$invoiceLineCount		= 0;
							$linNumber				= 1;
							$request				= array();
							$InvoiceLineAdd			= array();
							$AllorderId				= array();
							$ProductArray			= array();
							$TotalTaxLines			= array();
							$ProductArrayCount		= 0;
							$totalItemDiscount		= 0;
							$exchangeRate			= 1;
							$taxDate				= '';
							$dueDate				= '';
							$BPChannelName			= '';
							$aggreagationID			= '';
							$ReferenceNumber		= '';
							
							$trackingDetails1	= array();
							$trackingDetails2	= array();
							
							if(isset($channelMappings[$SalesChannel])){
								$trackingDetails1	= $channelMappings[$SalesChannel]['account2ChannelId'];
								$trackingDetails1	= explode("~=",$trackingDetails1);
							}
							
							if($ChannelcustomfieldMappings){
								if(($ChannelcustomfieldMappingKey == 'account1CustomFieldId') OR ($ChannelcustomfieldMappingKey == 'account1APIFieldId')){
									if($ChannelcustomfieldMappings[$SalesChannel][$CustomFieldValueID]){
										$trackingDetails2	= $ChannelcustomfieldMappings[$SalesChannel][$CustomFieldValueID]['account2ChannelId'];
										$trackingDetails2	= explode("~=",$trackingDetails2);
									}
								}
							}
							
							if(!$AggregationMapping['SendSkuDetails']){
								foreach($OrdersByChannels as $orderDatas){
									$bpconfig			= $this->ci->account1Config[$orderDatas['account1Id']];
									$rowDatas			= json_decode($orderDatas['rowData'],true);
									$exchangeRate		= 1;
									$exchangeRate		= $rowDatas['currency']['exchangeRate'];
									$CountryIsoCode		= strtolower(trim($rowDatas['parties']['delivery']['countryIsoCode3']));
									$countryState		= strtolower(trim($rowDatas['parties']['delivery']['addressLine4']));
									$brightpearlConfig	= $this->ci->account1Config[$orderDatas['account1Id']];
									if($orderForceCurrency){
										if($config['defaultCurrency'] != $brightpearlConfig['currencyCode']){
											continue;
										}
									}
									
									if(!$rowDatas['invoices']['0']['invoiceReference']){
										continue;
									}
									$isDiscountCouponAdded	= 0;
									$couponItemLineID		= '';
									$couponNominalCodeBP	= '';
									
									$missingCategoryMapping	= array();
									$missingSKU				= array();
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										if($rowdatass['productId'] >= 1000){
											if(substr_count(strtolower($rowdatass['productName']),'coupon')){
												if(in_array($rowdatass['nominalCode'],$nominalCodeForDiscount)){
													$isDiscountCouponAdded	= 1;
													$couponItemLineID		= $rowId;
													$couponNominalCodeBP	= $rowdatass['nominalCode'];
												}
											}
										}
										if(!empty($productcategoryMappings)){
											if($rowdatass['productId'] > 1000){
												if($productMappings[$rowdatass['productId']]){
													$productParams	= array();
													$productParams	= json_decode($productMappings[$rowdatass['productId']]['params'], true);
													if(!empty($productParams)){
														//PRODUCTCATEGORYMAPPING_CHANGES
														if(isset($productParams['salesChannels'])){
															if(isset($productParams['salesChannels'][0]['categories'][0])){
																if(isset($productParams['salesChannels'][0]['categories'][0]['categoryCode'])){
																	if(in_array($productParams['salesChannels'][0]['categories'][0]['categoryCode'], $allMappedCategory)){
																	}
																	else{
																		$missingCategoryMapping[]	= $rowdatass['productSku'];
																	}
																}
															}
														}
													}
													else{
														$missingSKU[]	= $rowdatass['productSku'];
													}
												}
												else{
													$missingSKU[]	= $rowdatass['productSku'];
												}
											}
										}
									}
									if(!empty($missingSKU)){
										if($missingSKU){
											$missingSKU	= array_unique($missingSKU);
											$this->ci->db->update('sales_order',array('message' => 'Missing sku : '.implode(",",$missingSKU)),array('orderId' => $orderDatas['orderId']));
											continue;
										}
									}
									foreach($rowDatas['orderRows'] as $rowId => $rowdatass){
										$bundleParentID	= '';
										$isBundleChild	= $rowdatass['composition']['bundleChild'];
										if($isBundleChild){
											$bundleParentID	= $rowdatass['composition']['parentOrderRowId'];
										}
										
										$bpTaxID				= $rowdatass['rowValue']['taxClassId'];
										if($isBundleChild AND $bundleParentID){
											$bpTaxID			= $rowDatas['orderRows'][$bundleParentID]['rowValue']['taxClassId'];
										}
										$rowNet					= $rowdatass['rowValue']['rowNet']['value'];
										$rowTax					= $rowdatass['rowValue']['rowTax']['value'];
										$productPrice			= $rowdatass['productPrice']['value'];
										$quantitymagnitude		= $rowdatass['quantity']['magnitude'];
										$bpdiscountPercentage	= $rowdatass['discountPercentage'];
										$bpItemNonimalCode		= $rowdatass['nominalCode'];
										
										$productId				= $rowdatass['productId'];
										$kidsTaxCustomField		= $bpconfig['customFieldForKidsTax'];
										$isKidsTaxEnabled		= 0;
										
										$productCategoryID		= 'NOCATEGORY';
										if($kidsTaxCustomField OR (!empty($productcategoryMappings))){
											if($productMappings[$productId]){
												$productParams	= json_decode($productMappings[$productId]['params'], true);
												if(($productParams['customFields'][$kidsTaxCustomField] == 1) OR ($productParams['customFields'][$kidsTaxCustomField] == true)){
													$isKidsTaxEnabled	= 1;
												}
												
												//PRODUCTCATEGORYMAPPING_CHANGES
												if(isset($productParams['salesChannels'])){
													if(isset($productParams['salesChannels'][0]['categories'][0])){
														if(isset($productParams['salesChannels'][0]['categories'][0]['categoryCode'])){
															if(in_array($productParams['salesChannels'][0]['categories'][0]['categoryCode'], $allMappedCategory)){
																$productCategoryID	= $productParams['salesChannels'][0]['categories'][0]['categoryCode'];
															}
														}
													}
												}
											}
										}
										
										$taxMappingKey	= $bpTaxID;
										$taxMappingKey1	= $bpTaxID;
										if($isStateEnabled){
											if($isChannelEnabled){
												$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState.'-'.$SalesChannel;
												$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode.'-'.$SalesChannel;
											}
											else{
												$taxMappingKey	= $bpTaxID.'-'.$CountryIsoCode.'-'.$countryState;
												$taxMappingKey1	= $bpTaxID.'-'.$CountryIsoCode;
											}
										}
										$taxMapping		= array();
										$taxMappingKey	= strtolower($taxMappingKey);
										$taxMappingKey1	= strtolower($taxMappingKey1);
										if(isset($taxMappings[$taxMappingKey])){
											$taxMapping	= $taxMappings[$taxMappingKey];
										}
										elseif(isset($taxMappings[$taxMappingKey1])){
											$taxMapping	= $taxMappings[$taxMappingKey1];
										}
										elseif(isset($taxMappings[$bpTaxID.'--'.$SalesChannel])){
											$taxMapping	= $taxMappings[$bpTaxID.'--'.$SalesChannel];
										}
									
										if($taxMapping){
											$bpTaxID	= $taxMapping['account2TaxId'];
											if(($isKidsTaxEnabled) AND ($taxMapping['account2KidsTaxId'])){
												$bpTaxID  = $taxMapping['account2KidsTaxId'];
											}
										}
										else{
											$bpTaxID	= $config['salesNoTaxCode'];
										}
										
										if($orderForceCurrency){
											$rowNet				= (($rowdatass['rowValue']['rowNet']['value']) * ((1) / ($exchangeRate)));
											$rowNet				= sprintf("%.4f",$rowNet);
											$rowTax				= (($rowdatass['rowValue']['rowTax']['value']) * ((1) / ($exchangeRate)));
											$rowTax				= sprintf("%.4f",$rowTax);
											$productPrice		= (($rowdatass['productPrice']['value']) * ((1) / ($exchangeRate)));
											$productPrice		= sprintf("%.4f",$productPrice);
										}
										
										$discountCouponAmt	= 0;
										$discountAmt		= 0;
										if($rowId == $couponItemLineID){
											if($rowNet == 0){
												continue;
											}
										}
										if(!$AggregationMapping['SendTaxAsLine']){
											if($bpdiscountPercentage > 0){
												$discountPercentage	= 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= ($productPrice * $quantitymagnitude);
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$productCategoryID][$bpItemNonimalCode][$bpTaxID])){
														$ProductArray['discount'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= ($productPrice * $quantitymagnitude);
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$productCategoryID][$couponNominalCodeBP][$bpTaxID])){
																	$ProductArray['discountCoupon'][$productCategoryID][$couponNominalCodeBP][$bpTaxID]['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$productCategoryID][$couponNominalCodeBP][$bpTaxID]['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if(isset($ProductArray[$productCategoryID][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray[$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']					+= $rowNet;
													$ProductArray[$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']					+= $rowTax;
												}
												else{
													$ProductArray[$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']					= $rowNet;
													$ProductArray[$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']					= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		+= $rowNet;
													$ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		= $rowNet;
													$ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		+= $rowNet;
													$ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']		= $rowNet;
													$ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode][$bpTaxID])){
													$ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	+= $rowNet;
													$ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		+= $rowTax;
												}
												else{
													$ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['TotalNetAmt']	= $rowNet;
													$ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode][$bpTaxID]['bpTaxTotal']		= $rowTax;
												}
											}
										}
										else{
											if($bpdiscountPercentage > 0){
												$discountPercentage	= 100 - $bpdiscountPercentage;
												if($discountPercentage == 0){
													$originalPrice	= ($productPrice * $quantitymagnitude);
												}
												else{
													$originalPrice	= round((($rowNet * 100) / ($discountPercentage)),2);
												}
												$discountAmt	= $originalPrice - $rowNet;
												$rowNet			= $originalPrice;
												if($discountAmt > 0){
													if(isset($ProductArray['discount'][$productCategoryID][$bpItemNonimalCode])){
														$ProductArray['discount'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']	+= $discountAmt;
													}
													else{
														$ProductArray['discount'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']	= $discountAmt;
													}
												}
											}
											elseif($isDiscountCouponAdded){
												if($rowDatas['orderRows'][$couponItemLineID]['rowValue']['rowNet']['value'] == 0){
													if($isBundleChild){
														//
													}
													else{
														$originalPrice		= ($productPrice * $quantitymagnitude);
														if(!$originalPrice){
															$originalPrice	= $rowNet;
														}
														if($originalPrice > $rowNet){
															$discountCouponAmt	= $originalPrice - $rowNet;
															$rowNet				= $originalPrice;
															if($discountCouponAmt > 0){
																if(isset($ProductArray['discountCoupon'][$productCategoryID][$couponNominalCodeBP])){
																	$ProductArray['discountCoupon'][$productCategoryID][$couponNominalCodeBP]['TotalNetAmt']	+= $discountCouponAmt;
																}
																else{
																	$ProductArray['discountCoupon'][$productCategoryID][$couponNominalCodeBP]['TotalNetAmt']	= $discountCouponAmt;
																}
															}
														}
													}
												}
											}
											if((!in_array($bpItemNonimalCode,$nominalCodeForShipping)) AND (!in_array($bpItemNonimalCode,$nominalCodeForGiftCard)) AND (!in_array($bpItemNonimalCode,$nominalCodeForDiscount))){
												if(isset($ProductArray['aggregationItem'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['aggregationItem'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']	+= $rowNet;
												}
												else{
													$ProductArray['aggregationItem'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']	= $rowNet;
												}
												if(isset($ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForShipping)){
												if(isset($ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['shipping'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForGiftCard)){
												if(isset($ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['giftcard'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
											if(in_array($bpItemNonimalCode,$nominalCodeForDiscount)){
												if(isset($ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']		+= $rowNet;
												}
												else{
													$ProductArray['couponitem'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']		= $rowNet;
												}
												if(isset($ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode])){
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			+= $rowTax;
												}
												else{
													$ProductArray['allTax'][$productCategoryID][$bpItemNonimalCode]['TotalNetAmt']			= $rowTax;
												}
											}
										}
									}
									
									$dueDate		= $rowDatas['invoices']['0']['dueDate'];
									$taxDate		= $rowDatas['invoices']['0']['taxDate'];
									$orderId		= $orderDatas['orderId'];
									$AllorderId[]	= $orderId;
									$BPChannelName	= $orderDatas['channelName'];
									$CurrencyCode	= $rowDatas['currency']['orderCurrencyCode'];
									$exchangeRate	= $rowDatas['currency']['exchangeRate'];
									
									if($orderForceCurrency){
										$CurrencyCode	= strtoupper($brightpearlConfig['currencyCode']);
										$exchangeRate	= 1;
									}
									
									$BPTotalAmount		+= $rowDatas['totalValue']['total'];
									$BPTotalAmountBase	+= $rowDatas['totalValue']['baseTotal'];
									$OrderCount++;
								}
								
								if($ProductArray){
									if(!$AggregationMapping['SendTaxAsLine']){
										foreach($ProductArray as $keyproduct => $ProductArrayDatas){
											if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
												
												foreach($ProductArrayDatas as $CategoryKeyproduct => $ProductArrayDatasNew){
													foreach($ProductArrayDatasNew as $MapNominal => $ProductArrayDatasTemp){
														$nominalMappingSet	= 0;
														$IncomeAccountRef	= $config['IncomeAccountRef'];
														if(isset($nominalMappings[$MapNominal])){
															if($nominalMappings[$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalMappings[$MapNominal]['account2NominalId'];
																$nominalMappingSet	= 1;
															}
														}
														if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]))){
															if($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId'];
																$nominalMappingSet	= 1;
															}
														}
														foreach($ProductArrayDatasTemp as $keyproduct1 => $datas){
															$InvoiceLineAdd[$invoiceLineCount]	= array(
																'ItemCode'		=> $AggregationMapping['AggregationItem'],
																'Description'	=> "Consolidation of ".$OrderCount.' invoices',
																'Quantity'		=> 1,
																'UnitAmount'	=> sprintf("%.4f",$datas['TotalNetAmt']),
																'TaxType'		=> $keyproduct1,
																'TaxAmount' 	=> sprintf("%.4f",$datas['bpTaxTotal']),
																'AccountCode'	=> $IncomeAccountRef,
															);
															if($keyproduct == 'shipping'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
															}
															if($keyproduct == 'giftcard'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
															}
															if($keyproduct == 'couponitem'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
																if($nominalMappingSet == 0){
																	unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
																}
															}
															if($keyproduct == 'discount'){
																if(!$config['OverrideDiscountItemAccRef']){
																	unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
																}
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
																$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
															}
															if($keyproduct == 'discountCoupon'){
																$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
																$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
																$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $datas['TotalNetAmt']));
																if($nominalMappingSet == 0){
																	unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
																}
															}
															
															//productTrackingCategoryChnages
															$tempTrackingCategory	= array();
															$tempTrackingCategory2	= array();
															if(isset($productcategoryMappings[$CategoryKeyproduct])){
																$tempTrackingCategory	= $productcategoryMappings[$CategoryKeyproduct]['account2ChannelId'];
																$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
															}
															
															if(!empty($tempTrackingCategory)){
																$tempTrackingCategory2	= $tempTrackingCategory;
															}
															else{
																$tempTrackingCategory2	= $trackingDetails2;
															}
															
															// ADDED THE ORDER TRACKING INFO
															if($trackingDetails1 OR $tempTrackingCategory2){
																if($trackingDetails1 AND $tempTrackingCategory2){
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																		'Name'		=> $trackingDetails1['0'],
																		'Option'	=> $trackingDetails1['1']
																	);
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																		'Name'		=> $tempTrackingCategory2['0'],
																		'Option'	=> $tempTrackingCategory2['1']
																	);
																}
																elseif($trackingDetails1 AND !$tempTrackingCategory2){
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																		'Name'		=> $trackingDetails1['0'],
																		'Option'	=> $trackingDetails1['1']
																	);
																}
																elseif(!$trackingDetails1 AND $tempTrackingCategory2){
																	$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																		'Name'		=> $tempTrackingCategory2['0'],
																		'Option'	=> $tempTrackingCategory2['1']
																	);
																}
																else{
																	unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
																}
															}
															$invoiceLineCount++;
														}
													}
												}
											}
											else{
												foreach($ProductArrayDatas as $nominalKeyproduct => $ProductArrayDatasNew){
													$IncomeAccountRef	= $config['IncomeAccountRef'];
													if(isset($nominalMappings[$nominalKeyproduct])){
														if($nominalMappings[$nominalKeyproduct]['account2NominalId']){
															$IncomeAccountRef	= $nominalMappings[$nominalKeyproduct]['account2NominalId'];
														}
													}
													if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$nominalKeyproduct]))){
														if($nominalChannelMappings[strtolower($SalesChannel)][$nominalKeyproduct]['account2NominalId']){
															$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$nominalKeyproduct]['account2NominalId'];
														}
													}
													foreach($ProductArrayDatasNew as $MapTaxId => $ProductArrayDatasTemp){
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'ItemCode'		=> $AggregationMapping['AggregationItem'],
															'Description'	=> "Consolidation of ".$OrderCount.' invoices',
															'Quantity'		=> 1,
															'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatasTemp['TotalNetAmt']),
															'TaxType'		=> $MapTaxId,
															'TaxAmount'		=> sprintf("%.4f",$ProductArrayDatasTemp['bpTaxTotal']),
															'AccountCode'	=> $IncomeAccountRef,
														);
														
														//productTrackingCategoryChnages
														$tempTrackingCategory	= array();
														$tempTrackingCategory2	= array();
														if(isset($productcategoryMappings[$keyproduct])){
															$tempTrackingCategory	= $productcategoryMappings[$keyproduct]['account2ChannelId'];
															$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
														}
														
														if(!empty($tempTrackingCategory)){
															$tempTrackingCategory2	= $tempTrackingCategory;
														}
														else{
															$tempTrackingCategory2	= $trackingDetails2;
														}
														
														// ADDED THE ORDER TRACKING INFO
														if($trackingDetails1 OR $tempTrackingCategory2){
															if($trackingDetails1 AND $tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																	'Name'		=> $tempTrackingCategory2['0'],
																	'Option'	=> $tempTrackingCategory2['1']
																);
															}
															elseif($trackingDetails1 AND !$tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
															}
															elseif(!$trackingDetails1 AND $tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $tempTrackingCategory2['0'],
																	'Option'	=> $tempTrackingCategory2['1']
																);
															}
															else{
																if(isset($InvoiceLineAdd[$invoiceLineCount]['Tracking'])){unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);}
															}
														}
														$invoiceLineCount++;
													}
												}
											}
										}
									}
									else{
										foreach($ProductArray as $keyproduct => $ProductArrayDatasTemp){
											foreach($ProductArrayDatasTemp as $CategoryKeyproduct => $ProductArrayDatasNew){
												//productTrackingCategoryChnages
												$tempTrackingCategory	= array();
												$tempTrackingCategory2	= array();
												if(isset($productcategoryMappings[$CategoryKeyproduct])){
													$tempTrackingCategory	= $productcategoryMappings[$CategoryKeyproduct]['account2ChannelId'];
													$tempTrackingCategory	= explode("~=",$tempTrackingCategory);
												}
												
												if(!empty($tempTrackingCategory)){
													$tempTrackingCategory2	= $tempTrackingCategory;
												}
												else{
													$tempTrackingCategory2	= $trackingDetails2;
												}
												
												foreach($ProductArrayDatasNew as $MapNominal => $ProductArrayDatas){
													if(($keyproduct == 'shipping') OR ($keyproduct == 'giftcard') OR ($keyproduct == 'couponitem') OR ($keyproduct == 'aggregationItem') OR ($keyproduct == 'discount') OR ($keyproduct == 'discountCoupon')){
														$nominalMappingSet	= 0;
														$IncomeAccountRef	= $config['IncomeAccountRef'];
														if(isset($nominalMappings[$MapNominal])){
															if($nominalMappings[$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalMappings[$MapNominal]['account2NominalId'];
																$nominalMappingSet	= 1;
															}
														}
														if(($SalesChannel) AND (isset($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]))){
															if($nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId']){
																$IncomeAccountRef	= $nominalChannelMappings[strtolower($SalesChannel)][$MapNominal]['account2NominalId'];
																$nominalMappingSet	= 1;
																
															}
														}
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'ItemCode'		=> $AggregationMapping['AggregationItem'],
															'Description'	=> "Consolidation of ".$OrderCount.' invoices',
															'Quantity'		=> 1,
															'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
															'TaxType'		=> $config['salesNoTaxCode'],
															'AccountCode'	=> $IncomeAccountRef,
														);
														if($keyproduct == 'shipping'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['shippingItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Shipping';
														}
														if($keyproduct == 'giftcard'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['giftCardItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'GiftCard';
														}
														if($keyproduct == 'couponitem'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
															if($nominalMappingSet == 0){
																unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
															}
														}
														if($keyproduct == 'discount'){
															if(!$config['OverrideDiscountItemAccRef']){
																unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
															}
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['discountItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Item Discount';
															$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
														}
														if($keyproduct == 'discountCoupon'){
															$InvoiceLineAdd[$invoiceLineCount]['ItemCode']		= $config['couponItem'];
															$InvoiceLineAdd[$invoiceLineCount]['Description']	= 'Coupon Discount';
															$InvoiceLineAdd[$invoiceLineCount]['UnitAmount']	= sprintf("%.4f",((-1) * $ProductArrayDatas['TotalNetAmt']));
															if($nominalMappingSet == 0){
																unset($InvoiceLineAdd[$invoiceLineCount]['AccountCode']);
															}
														}
														
														// ADDED THE ORDER TRACKING INFO
														if($trackingDetails1 OR $tempTrackingCategory2){
															if($trackingDetails1 AND $tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																	'Name'		=> $tempTrackingCategory2['0'],
																	'Option'	=> $tempTrackingCategory2['1']
																);
															}
															elseif($trackingDetails1 AND !$tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
															}
															elseif(!$trackingDetails1 AND $tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $tempTrackingCategory2['0'],
																	'Option'	=> $tempTrackingCategory2['1']
																);
															}
															else{
																unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
															}
														}
														$invoiceLineCount++;
													}
													elseif($keyproduct == 'allTax'){
														if($ProductArrayDatas['TotalNetAmt'] <= 0){
															continue;
														}
														$InvoiceLineAdd[$invoiceLineCount]	= array(
															'ItemCode'		=> $AggregationMapping['defaultTaxItem'],
															'Description'	=> $AggregationMapping['defaultTaxItem'],
															'Quantity'		=> 1,
															'UnitAmount'	=> sprintf("%.4f",$ProductArrayDatas['TotalNetAmt']),
															'TaxType'		=> $config['salesNoTaxCode'],
															/* 'AccountCode'	=> $config['TaxItemLineNominal'], */
														);
														// ADDED THE ORDER TRACKING INFO
														if($trackingDetails1 OR $tempTrackingCategory2){
															if($trackingDetails1 AND $tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][0]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][1]	= array(
																	'Name'		=> $tempTrackingCategory2['0'],
																	'Option'	=> $tempTrackingCategory2['1']
																);
															}
															elseif($trackingDetails1 AND !$tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $trackingDetails1['0'],
																	'Option'	=> $trackingDetails1['1']
																);
															}
															elseif(!$trackingDetails1 AND $tempTrackingCategory2){
																$InvoiceLineAdd[$invoiceLineCount]['Tracking'][]	= array(
																	'Name'		=> $tempTrackingCategory2['0'],
																	'Option'	=> $tempTrackingCategory2['1']
																);
															}
															else{
																unset($InvoiceLineAdd[$invoiceLineCount]['Tracking']);
															}
														}
														$invoiceLineCount++;
													}
												}
											}
										}
									}
								}
								
								//taxdate chanages
								if($clientcode == 'oskarswoodenarkxerom'){
									$BPDateOffset	= substr($taxDate,23,6);
									$BPDateOffset	= explode(":",$BPDateOffset);
									$tempHours		= (int)$BPDateOffset[0];
									$tempMinutes	= (int)$BPDateOffset[1];
									$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
									if($tempHours < 0){
										$totalMinutes = (-1) * $totalMinutes;
									}
									$date			= new DateTime($taxDate);
									$BPTimeZone		= 'GMT';
									$date->setTimezone(new DateTimeZone($BPTimeZone));
									if($totalMinutes){
										$totalMinutes	.= ' minute';
										$date->modify($totalMinutes);
									}
									$taxDate		= $date->format('Y-m-d');
									
									
									$BPDateOffset	= substr($dueDate,23,6);
									$BPDateOffset	= explode(":",$BPDateOffset);
									$tempHours		= (int)$BPDateOffset[0];
									$tempMinutes	= (int)$BPDateOffset[1];
									$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
									if($tempHours < 0){
										$totalMinutes = (-1) * $totalMinutes;
									}
									$date			= new DateTime($dueDate);
									$BPTimeZone		= 'GMT';
									$date->setTimezone(new DateTimeZone($BPTimeZone));
									if($totalMinutes){
										$totalMinutes	.= ' minute';
										$date->modify($totalMinutes);
									}
									$dueDate		= $date->format('Y-m-d');
								}
								else{
									$BPDateOffset	= (int)substr($taxDate,23,3);
									$Acc2Offset		= 0;
									$diff			= $BPDateOffset - $Acc2Offset;
									$date1			= new DateTime($dueDate);
									$date			= new DateTime($taxDate);
									$BPTimeZone		= 'GMT';
									$date1->setTimezone(new DateTimeZone($BPTimeZone));
									$date->setTimezone(new DateTimeZone($BPTimeZone));
									if($diff > 0){
										$diff			.= ' hour';
										$date->modify($diff);
										$date1->modify($diff);
									}
									$taxDate		= $date->format('Y-m-d');
									$dueDate		= $date1->format('Y-m-d');
								}
							}
							else{
								continue;
							}
							
							if($AggregationMapping['disableNominalPosting']){
								foreach($InvoiceLineAdd as $lineSeq => $InvoiceLineAddTemp){
									if(isset($InvoiceLineAddTemp['AccountCode'])){
										unset($InvoiceLineAdd[$lineSeq]['AccountCode']);
									}
								}
							}
							
							if($InvoiceLineAdd){
								if($consolFrequency == 1){
									$sendTaxDate		= $taxDate;
									$sendDueDate		= $dueDate;
									$RefForInvoice		= $taxDate;
									$ReferenceNumber	= date('Ymd',strtotime($RefForInvoice)).'-'.$uniqueInvoiceRef.'-'.$CurrencyCode;
								}
								else{
									$isLeap				= 0;
									$invMonth			= (int)substr($taxDateBP,3,2);
									$invYear			= (int)substr($taxDateBP,0,2);
									$invYearFull		= '20'.$invYear;
									
									if(((int)$invYearFull % 4) == 0){
										if(((int)$invYearFull % 100) == 0){
											if(((int)$invYearFull % 400) == 0){
												$isLeap	= 1;
											}
											else{
												$isLeap	= 0;
											}
										}
										else{
											$isLeap	= 1;
										}
									}
									else{
										$isLeap	= 0;
									}
									
									$invDate	= $dateResults[$invMonth]['lastDate'];
									if($invMonth == 2){
										if($isLeap){
											$invDate	= $dateResults[$invMonth]['lastDate2'];										
										}
									}
									$invMonthName	= $dateResults[$invMonth]['name'];
									if(strlen($invMonth) == 1){
										$invMonth	= (string)('0'.$invMonth);
									}
									$sendTaxDate		= $invYearFull.'-'.$invMonth.'-'.$invDate;
									$sendDueDate		= $sendTaxDate;
									$RefForInvoice		= $invMonthName.$invYear;
									$ReferenceNumber	= $RefForInvoice.'-'.$uniqueInvoiceRef.'-'.$CurrencyCode;
								}
								
								$aggreagationID			= uniqid((strtoupper(trim($BPChannelName))).'-');
								
								
								
								//newCode for add suffix in invoice Number::
								$invoiceNumberSuffix	= '';
								$allSameRefConsols		= $this->ci->db->like('invoiceRef', $ReferenceNumber, 'both')->select('DISTINCT createOrderId',false)->get_where('sales_order',array('account2Id' => $account2Id))->result_array();
								if(count($allSameRefConsols) > 0){
									$invoiceNumberSuffix	= '_0'.(count($allSameRefConsols));
								}
								
								if(strlen($invoiceNumberSuffix) > 0){
									$ReferenceNumber	= $ReferenceNumber.$invoiceNumberSuffix;
								}
								//newCode for add suffix in invoice Number::
								
								
								
								
								$ReferenceNumber		= substr($ReferenceNumber,0,255);
								
								$CheckReferenceNumber	= $this->ci->db->get_where('sales_order',array('invoiceRef' => $ReferenceNumber, 'account2Id' => $account2Id))->row_array();
								if($CheckReferenceNumber){
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('message' => 'Invoice Number already exists in Xero'));
									continue;
								}
								
								foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
									if($InvoiceLineAddTemp['UnitAmount'] > 0){
										if($InvoiceLineAddTemp['UnitAmount'] < '0.0050'){
											$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
										}
									}
									else{
										if($InvoiceLineAddTemp['UnitAmount'] > '-0.0050'){
											$InvoiceLineAdd[$LineSeq]['UnitAmount']	= 0;
										}
									}
								}
								
								$invoiceLineAddNew		= array();
								$invoiceLineFormatted	= array();
								$newItemLineCount		= 0;
								foreach($InvoiceLineAdd as $LineSeq => $InvoiceLineAddTemp){
									if($InvoiceLineAddTemp['ItemCode'] == $AggregationMapping['AggregationItem']){
										$invoiceLineAddNew[1][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['ItemCode'] == $config['discountItem']){
										$invoiceLineAddNew[2][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['ItemCode'] == $config['couponItem']){
										$invoiceLineAddNew[3][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['ItemCode'] == $config['shippingItem']){
										$invoiceLineAddNew[4][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['ItemCode'] == $AggregationMapping['defaultTaxItem']){
										$invoiceLineAddNew[5][]	= $InvoiceLineAddTemp;
										continue;
									}
									if($InvoiceLineAddTemp['ItemCode'] == $config['giftCardItem']){
										$invoiceLineAddNew[6][]	= $InvoiceLineAddTemp;
										continue;
									}
								}
								ksort($invoiceLineAddNew);
								foreach($invoiceLineAddNew as $itemseqId => $invoiceLineAddNewTemp){
									foreach($invoiceLineAddNewTemp as $invoiceLineAddNewTempTemp){
										$invoiceLineFormatted[$newItemLineCount]	= $invoiceLineAddNewTempTemp;
										$newItemLineCount++;
									}
								}
								if($invoiceLineFormatted){
									$InvoiceLineAdd		= $invoiceLineFormatted;
									$invoiceLineCount	= $newItemLineCount;
								}
								
								$request			= array(
									'Type'				=> 'ACCREC',
									'Contact' 			=> array('ContactID' => $ChannelCustomer),
									'InvoiceNumber'		=> $ReferenceNumber,
									'Reference'			=> $ReferenceNumber,
									'Status'			=> 'AUTHORISED',
									'CurrencyCode'		=> $CurrencyCode,
									'CurrencyRate'		=> $exchangeRate,
									'Date'				=> $sendTaxDate,
									'DueDate'			=> $sendTaxDate,
									'LineItems' 		=> $InvoiceLineAdd,
								);
								
								if($config['defaultCurrency'] != $brightpearlConfig['currencyCode']){
									unset($request['CurrencyRate']);
								}
							}
							
							if($request AND $InvoiceLineAdd){
								if($disableSalesPosting){
									$createdRowData	= array(
										'Request data	: '	=> $request,
										'Response data	: '	=> 'POSTING_IS_DISABLED',
									);
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
								}
								else{
									$this->headers	= array();
									$url			= '2.0/Invoices';
									if($accountDetails['OAuthVersion'] == '2'){
										$url		= '2.0/Invoices?unitdp=4';
										$this->initializeConfig($account2Id, 'PUT', $url);
									}
									else{
										$url		= '2.0/Invoices';
										$UrlParams	= array('unitdp' => '4');
										$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
										$url		= '2.0/Invoices?unitdp='.urlencode('4');
									}
									$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
									$createdRowData	= array(
										'Request data	: '	=> $request,
										'Response data	: '	=> $results,
									);
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('createdRowData' => json_encode($createdRowData),'aggregationId' => $aggreagationID));
								}
							}
							else{
								continue;
							}
							
							if((strtolower($results['Status']) == 'ok') AND (isset($results['Invoices']['Invoice']['InvoiceID']))){
								
								$consolPostOptions	= 0;
								if($disableCOGSPosting){
									$consolPostOptions	= 2;
								}
								
								$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('uninvoiced' => 0, 'status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID'], 'sendInAggregation' => 1, 'PostedTime' => date('c') , 'IsJournalposted' => $journalAggregation, 'consolPostOptions'=> $consolPostOptions));
								
								$XeroTotalAmount	= $results['Invoices']['Invoice']['Total'];
								if($orderForceCurrency){
									$NetRoundOff	= $BPTotalAmountBase - $XeroTotalAmount;
								}
								else{
									$NetRoundOff	= $BPTotalAmount - $XeroTotalAmount;
								}
								$RoundOffCheck		= abs($NetRoundOff);
								$RoundOffCheck		= sprintf("%.4f",$RoundOffCheck);
								$RoundOffApplicable	= 0;
								if($RoundOffCheck != 0){
									if($RoundOffCheck < 0.99){
										$RoundOffApplicable = 1;
									}
									if($RoundOffApplicable){
										$InvoiceLineAdd[$invoiceLineCount] = array(
											'ItemCode'			=> $config['roundOffItem'],
											'Description'		=> $config['roundOffItem'],
											'Quantity'			=> 1,
											'UnitAmount'		=> sprintf("%.4f",$NetRoundOff),
											'TaxType'			=> $config['salesNoTaxCode'],
										);	
										$request['LineItems']	= $InvoiceLineAdd;
										$request['InvoiceID']	= $results['Invoices']['Invoice']['InvoiceID'];
										
										$this->headers	= array();
										$url			= '2.0/Invoices';
										if($accountDetails['OAuthVersion'] == '2'){
											$url		= '2.0/Invoices?unitdp=4';
											$this->initializeConfig($account2Id, 'POST', $url);
										}
										else{
											$url		= '2.0/Invoices';
											$UrlParams	= array('unitdp' => '4');
											$this->initializeConfig($account2Id, 'POST', $url,$UrlParams);
											$url		= '2.0/Invoices?unitdp='.urlencode('4');
										}
										$results2	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
										$createdRowData['Rounding Request data	: ']	= $request;
										$createdRowData['Rounding Response data	: ']	= $results2;
										if((strtolower($results2['Status']) == 'ok') AND (isset($results2['Invoices']['Invoice']['InvoiceID']))){
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('status' => '1', 'message' => '', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $results['Invoices']['Invoice']['InvoiceID'], 'sendInAggregation' => 1, 'PostedTime' => date('c'),'createdRowData' => json_encode($createdRowData)));
										}
										else{
											$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('PostedWrong' => 1, 'LastResponseCheck' => strtotime("now"), 'message' => 'Unable to add Rounding Item','createdRowData' => json_encode($createdRowData)));
										}
									}
									else{
										$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('PostedWrong' => 1,'LastResponseCheck' => strtotime("now")));
									}
								}
							}
							else{
								if($disableSalesPosting){
									$fakecreateOrderId	= uniqid("XERO_ID_").strtotime("now");
									$this->ci->db->where_in('orderId',$AllorderId)->update('sales_order',array('uninvoiced' => 0, 'status' => '4', 'message' => 'COGS only', 'invoiced' => '0', 'invoiceRef' => $ReferenceNumber,'createOrderId' => $fakecreateOrderId, 'sendInAggregation' => 1, 'PostedTime' => date('c') , 'IsJournalposted' => $journalAggregation, 'consolPostOptions' => 1));
								}
							}
						}
					}
				}
			}
		}
	}
}

$this->postConsolidatedJournal();
$this->postaggregationSalesPayment();