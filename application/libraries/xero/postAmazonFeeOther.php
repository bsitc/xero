<?php
//xero_demo
$this->reInitialize();
$enableAmazonFeeOther	= $this->ci->globalConfig['enableAmazonFeeOther'];
$allBpCurrencies		= $this->ci->brightpearl->getAllCurrency();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$enableAmazonFeeOther){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);

	$this->ci->db->reset_query();
	$amazonFeeData	= $this->ci->db->get_where('amazonFeesOther',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(!$amazonFeeData){continue;}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]		= $channelMappingsTemp;
			}
		}
	}
	
	if($amazonFeeData){
		$consolAmazonFeeDatas	= array();
		foreach($amazonFeeData as $amazonFeeDataTemp){
			$journalId			= $amazonFeeDataTemp['journalId'];
			$account1Id			= $amazonFeeDataTemp['account1Id'];
			$journalTypeCode	= $amazonFeeDataTemp['journalTypeCode'];
			$channelid			= $amazonFeeDataTemp['channelid'];
			$currencyId			= $amazonFeeDataTemp['currencyId'];
			$currencyCode		= $allBpCurrencies[$account1Id][$currencyId]['code'];
			$amazonFeeParam		= json_decode($amazonFeeDataTemp['params'],true);
			$taxDate			= $amazonFeeParam['taxDate'];
			
			
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate		= $date->format('Ymd');
			}
			else{
				$BPDateOffset		= (int)substr($taxDate,23,3);
				$Acc2Offset			= 0;
				$diff				= $BPDateOffset - $Acc2Offset;
				$date				= new DateTime($taxDate);
				$BPTimeZone			= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff	.= ' hour';
					$date->modify($diff);
				}
				$taxDate		= $date->format('Ymd');
			}
			
			$fetchTaxDate	= date('Ymd',strtotime('-1 day'));
			
			if($taxDate > $fetchTaxDate){
				continue;
			}
			
			$consolAmazonFeeDatas[$taxDate][$journalTypeCode][$channelid][$currencyCode][$journalId]	= $amazonFeeDataTemp;
		}
		
		
		if($consolAmazonFeeDatas){
			foreach($consolAmazonFeeDatas as $consolTaxDate => $consolAmazonFeeDatas11){
				foreach($consolAmazonFeeDatas11 as $consolJournalType => $consolAmazonFeeDatas1){
					foreach($consolAmazonFeeDatas1 as $consolChannel => $consolAmazonFeeDatas2){
						foreach($consolAmazonFeeDatas2 as $consolCurrency => $consolAmazonFeeDatas3){
							$consolBankTxnRequest	= array();
							$consolItemLineRequest	= array();
							$lineItmeNominalArray	= array();
							$processedJournalIds	= array();
							$bpconfig				= array();
							$lineItemSequence		= 0;
							$amazonFeeCount			= 0;
							$exchangeRate			= '';
							$defaultBankAccount		= '';
							$Reference				= '';
							$amazonFeeContactId		= $config['otherAmazonFeeContactId'];
							
							if(strtolower($consolJournalType) == 'bp'){
								foreach($consolAmazonFeeDatas3 as $journalId => $consolAmazonFeeData){
									$bpconfig		= $this->ci->account1Config[$consolAmazonFeeData['account1Id']];
									$missingNominal	= 0;
									$amazonFeeParam	= json_decode($consolAmazonFeeData['params'],true);
									$credits		= $amazonFeeParam['credits'];
									$debits			= $amazonFeeParam['debits'];
									$exchangeRate	= $consolAmazonFeeData['exchangeRate'];
									if(!$credits['0']){
										$credits	= array($credits);
									}
									if(!$debits['0']){
										$debits		= array($debits);
									}
									foreach($debits as $debit){
										$defaultNominal	= '';
										$transactionAmount	= abs($debit['transactionAmount']);
										if(isset($nominalMappings[$debit['nominalCode']])){
											if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
											}
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]))){
											if($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'];
											}
										}
										if(!$defaultNominal){
											$missingNominal	= 1;
											break;
										}
									}
									if($missingNominal){
										$this->ci->db->where(array('journalId' => $journalId))->update('amazonFeesOther',array('message' => 'Nominal Mapping Not Found'));
										continue;
									}
									foreach($credits as $credit){
										if(isset($nominalMappings[$credit['nominalCode']])){
											if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
												$defaultBankAccount	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
											}
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]))){
											if($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId']){
												$defaultBankAccount	= $nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'];
											}
										}
									}
									foreach($debits as $debit){
										$defaultNominal		= '';
										$transactionAmount	= abs($debit['transactionAmount']);
										if(isset($nominalMappings[$debit['nominalCode']])){
											if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
											}
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]))){
											if($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'];
											}
										}
										if(isset($lineItmeNominalArray[$defaultNominal]['netAmt'])){
											$lineItmeNominalArray[$defaultNominal]['netAmt']	+= $transactionAmount;
										}
										else{
											$lineItmeNominalArray[$defaultNominal]['netAmt']	= $transactionAmount;
										}
									}
									$processedJournalIds[]	= $journalId;
									$amazonFeeCount++;
								}
							}
							elseif(strtolower($consolJournalType) == 'br'){
								foreach($consolAmazonFeeDatas3 as $journalId => $consolAmazonFeeData){
									$bpconfig		= $this->ci->account1Config[$consolAmazonFeeData['account1Id']];
									$missingNominal	= 0;
									$amazonFeeParam	= json_decode($consolAmazonFeeData['params'],true);
									$credits		= $amazonFeeParam['credits'];
									$debits			= $amazonFeeParam['debits'];
									$exchangeRate	= $consolAmazonFeeData['exchangeRate'];
									if(!$credits['0']){
										$credits	= array($credits);
									}
									if(!$debits['0']){
										$debits		= array($debits);
									}
									foreach($credits as $credit){
										$defaultNominal	= '';
										$transactionAmount	= abs($credit['transactionAmount']);
										if(isset($nominalMappings[$credit['nominalCode']])){
											if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
											}
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]))){
											if($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'];
											}
										}
										if(!$defaultNominal){
											$missingNominal	= 1;
											break;
										}
									}
									if($missingNominal){
										$this->ci->db->where(array('journalId' => $journalId))->update('amazonFeesOther',array('message' => 'Nominal Mapping Not Found'));
										continue;
									}
									foreach($debits as $debit){
										if(isset($nominalMappings[$debit['nominalCode']])){
											if($nominalMappings[$debit['nominalCode']]['account2NominalId']){
												$defaultBankAccount	= $nominalMappings[$debit['nominalCode']]['account2NominalId'];
											}
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]))){
											if($nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId']){
												$defaultBankAccount	= $nominalChannelMappings[strtolower($consolChannel)][$debit['nominalCode']]['account2NominalId'];
											}
										}
									}
									foreach($credits as $credit){
										$defaultNominal		= '';
										$transactionAmount	= abs($credit['transactionAmount']);
										if(isset($nominalMappings[$credit['nominalCode']])){
											if($nominalMappings[$credit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalMappings[$credit['nominalCode']]['account2NominalId'];
											}
										}
										if(($consolChannel) AND (isset($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]))){
											if($nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId']){
												$defaultNominal	= $nominalChannelMappings[strtolower($consolChannel)][$credit['nominalCode']]['account2NominalId'];
											}
										}
										if(isset($lineItmeNominalArray[$defaultNominal]['netAmt'])){
											$lineItmeNominalArray[$defaultNominal]['netAmt']	+= $transactionAmount;
										}
										else{
											$lineItmeNominalArray[$defaultNominal]['netAmt']	= $transactionAmount;
										}
									}
									$processedJournalIds[]	= $journalId;
									$amazonFeeCount++;
								}
							}
							
							if($lineItmeNominalArray){
								if(!$defaultBankAccount){
									echo "<pre>";print_r("Bank Account Nominal Mapping Missing"); echo "</pre>";
									continue;
								}
								if(!$amazonFeeContactId){
									echo "<pre>";print_r("Amazon Contact ID Missing"); echo "</pre>";
									continue;
								}
								
								foreach($lineItmeNominalArray as $lineNominal => $lineItmeNominalArray1){
									$consolItemLineRequest[$lineItemSequence]	= array(
										//'Description'	=> 'Consol of '.$amazonFeeCount.' Amazon Fee',
										'Description'	=> 'Consolidated Non-order related Amazon Fees',
										'Quantity'		=> 1,
										'TaxType'		=> $config['salesNoTaxCode'],
										'AccountCode'	=> $lineNominal,
										'UnitAmount'	=> sprintf("%.4f",($lineItmeNominalArray1['netAmt'])),
										'LineAmount'	=> sprintf("%.4f",($lineItmeNominalArray1['netAmt'])),
									);
									/* if(strtolower($consolJournalType) == 'br'){
										$consolItemLineRequest[$lineItemSequence]['UnitAmount']	= ((-1) * (sprintf("%.4f",($lineItmeNominalArray1['netAmt']))));
										$consolItemLineRequest[$lineItemSequence]['LineAmount']	= ((-1) * (sprintf("%.4f",($lineItmeNominalArray1['netAmt']))));
									} */
									if(isset($channelMappings[$consolChannel])){
										$trackingDetails	= $channelMappings[$consolChannel]['account2ChannelId'];
										$trackingDetails	= explode("~=",$trackingDetails);
										$consolItemLineRequest[$lineItemSequence]['Tracking'][0]	= array(
											'Name'		=> $trackingDetails['0'],
											'Option'	=> $trackingDetails['1']
										);
									}
									$lineItemSequence++;
								}
								if($consolItemLineRequest){
									$Reference			= $consolTaxDate.'-AMZ-'.$consolCurrency.'-'.strtoupper($consolJournalType);
									$consolBankTxnRequest	= array(
										'Type'				=> 'SPEND',
										'Contact'			=> array('ContactID' => $amazonFeeContactId),
										'Lineitems'			=> $consolItemLineRequest,
										'BankAccount'		=> array('Code' => $defaultBankAccount),
										'Date'				=> date('Y-m-d',strtotime($consolTaxDate)),
										'Reference'			=> $Reference,
										'CurrencyCode'		=> $consolCurrency,
										'CurrencyRate'		=> $exchangeRate,
										'Status'			=> 'AUTHORISED',
										'LineAmountTypes'	=> 'NoTax',
									);
									if(strtolower($consolJournalType) == 'br'){
										$consolBankTxnRequest['Type']	= 'RECEIVE';
									}
									if($config['defaultCurrency'] != $bpconfig['currencyCode']){
										unset($consolBankTxnRequest['CurrencyRate']);
									}
									
									$this->headers		= array();
									$url				= '2.0/BankTransactions?unitdp=4';
									$this->initializeConfig($account2Id, 'PUT', $url);
									$bankTxnResults		= $this->getCurl($url, 'PUT', json_encode($consolBankTxnRequest), 'json', $account2Id)[$account2Id];
									$createdParams		= array(
										'Request data	: ' => $consolBankTxnRequest,
										'Response data	: ' => $bankTxnResults,
									);
									$createdParams		= json_encode($createdParams);
									if((strtolower($bankTxnResults['Status']) == 'ok') AND (isset($bankTxnResults['BankTransactions']['BankTransaction']['BankTransactionID']))){	
										$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('amazonFeesOther',array('xeroTxnId' => $bankTxnResults['BankTransactions']['BankTransaction']['BankTransactionID'], 'xeroRefNo' => $Reference, 'status' => '1', 'createdParams' => $createdParams));
									}
									else{
										$this->ci->db->where_in('journalId',$processedJournalIds)->where(array('account2Id' => $account2Id))->update('amazonFeesOther',array('createdParams' => $createdParams));
									}
								}
							}
						}
					}
				}
			}
		}
	}
}