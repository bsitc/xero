<?php
$this->reInitialize();
$enableCOGSJournals					= $this->ci->globalConfig['enableCOGSJournals'];
$enableAssignmentMapping			= $this->ci->globalConfig['enableAssignmentMapping'];
$enableChannelcustomfieldMapping	= $this->ci->globalConfig['enableChannelcustomfieldMapping'];
$clientcode							= $this->ci->config->item('clientcode');
$dateLockSettings					= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	if(!$enableCOGSJournals){continue;}
	//cogs for Keen DIST LTD. is stopped, req by Neha on 9th Fab 2023
	if($clientcode == 'keenxero'){if($account2Id == 5){continue;}}
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	$cogsDataPO	= $this->ci->db->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 0, 'OrderType' => 'PO'))->result_array();
	
	if($cogsDataPO){
		$AllPOCogsByOrderID	= array();
		foreach($cogsDataPO as $cogsDataPOTemp){
			if($cogsDataPOTemp['OrderType'] == 'PO'){
				$AllPOCogsByOrderID[$cogsDataPOTemp['orderId']][]	= $cogsDataPOTemp;
			}
		}
		if($AllPOCogsByOrderID){
			$AllVoidableIds	= array();
			foreach($AllPOCogsByOrderID as $purchaseOrderID => $AllPOCogsByOrderIDTemp){
				$allPOCogsIds	= array();
				if(count($AllPOCogsByOrderIDTemp) > 1){
					$allPOCogsIds	= array_column($AllPOCogsByOrderIDTemp,'journalsId');
					$allPOCogsIds	= array_filter($allPOCogsIds);
					$allPOCogsIds	= array_unique($allPOCogsIds);
					rsort($allPOCogsIds);
				}
				if($allPOCogsIds){
					foreach($allPOCogsIds as $poCogsIdKey => $allPOCogsIdsTemp){
						if($poCogsIdKey != 0){
							$AllVoidableIds[]	= $allPOCogsIdsTemp;							
						}
					}
				}
			}
			if($AllVoidableIds){
				$this->ci->db->where_in('journalsId',$AllVoidableIds)->update('cogs_journal',array('status' => 4, 'message' => 'Mispostings Journal'));
			}
		}
	}
	
	$this->ci->db->reset_query();
	$allPostedPOCogs	= $this->ci->db->select('orderId,journalsId')->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 1, 'OrderType' => 'PO', 'createdJournalsId <>' => ''))->result_array();
	$allPostedPOCogsAll	= array();
	if($allPostedPOCogs){
		foreach($allPostedPOCogs as $allPostedPOCogsTemp){
			$allPostedPOCogsAll[$allPostedPOCogsTemp['orderId']][]	= $allPostedPOCogsTemp;
		}
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('journalsId',$orgObjectId);
	}
	$datas	= $this->ci->db->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(!$datas){continue;}
	
	$AllProcessableIds	= array();
	if($datas){
		foreach($datas as $datasForIDs){
			$AllProcessableIds[]	= $datasForIDs['orderId'];
		}
		$AllProcessableIds	= array_filter($AllProcessableIds);
		$AllProcessableIds	= array_unique($AllProcessableIds);
	}
	
	if($clientcode == 'freeflyxero_notApplicableNow'){
		//fetchOrdersfromDB	- posted or not(modification for FreeFly)
		$this->ci->db->reset_query();
		$allSaveSalesCredits		= array();
		if($AllProcessableIds){
			$this->ci->db->where_in('orderId',$AllProcessableIds);
		}
		$allSaveSalesCreditsDatas	= $this->ci->db->select('orderId,createOrderId,rowData,createdRowData,sendInAggregation')->get_where('sales_credit_order',array('isNetOff' => '0', 'sendInAggregation <>' => 1, 'account2Id' => $account2Id))->result_array();
		if($allSaveSalesCreditsDatas){
			foreach($allSaveSalesCreditsDatas as $allSaveSalesCreditsData){
				if($allSaveSalesCreditsData['sendInAggregation']){
					continue;
				}
				$allSaveSalesCredits[$allSaveSalesCreditsData['orderId']]		= $allSaveSalesCreditsData;
			}
		}
		
		//fetchOrdersfromDB	- posted or not(modification for FreeFly)
		$this->ci->db->reset_query();
		$allSaveSalesOrders			= array();
		if($AllProcessableIds){
			$this->ci->db->where_in('orderId',$AllProcessableIds);
		}
		$allSaveSalesOrdersDatas	= $this->ci->db->select('orderId,createOrderId,rowData,createdRowData,sendInAggregation')->get_where('sales_order',array('isNetOff' => '0', 'sendInAggregation <>' => 1, 'account2Id' => $account2Id))->result_array();
		if($allSaveSalesOrdersDatas){
			foreach($allSaveSalesOrdersDatas as $allSaveSalesOrdersData){
				if($allSaveSalesOrdersData['sendInAggregation']){
					continue;
				}
				$allSaveSalesOrders[$allSaveSalesOrdersData['orderId']]			= $allSaveSalesOrdersData;
			}
		}
	}
	else{
		$this->ci->db->reset_query();
		$allSaveSalesCredits		= array();
		if($AllProcessableIds){
			$this->ci->db->where_in('orderId',$AllProcessableIds);
		}
		$allSaveSalesCreditsDatas	= $this->ci->db->select('orderId,createOrderId,rowData,createdRowData,sendInAggregation')->get_where('sales_credit_order',array('isNetOff' => '0', 'status <>' => '0' ,'sendInAggregation <>' => 1, 'account2Id' => $account2Id))->result_array();
		if($allSaveSalesCreditsDatas){
			foreach($allSaveSalesCreditsDatas as $allSaveSalesCreditsData){
				if($allSaveSalesCreditsData['sendInAggregation']){
					continue;
				}
				if($allSaveSalesCreditsData['createOrderId']){
					$allSaveSalesCredits[$allSaveSalesCreditsData['orderId']]		= $allSaveSalesCreditsData;
				}
			}
		}
		
		$this->ci->db->reset_query();
		$allSaveSalesOrders			= array();
		if($AllProcessableIds){
			$this->ci->db->where_in('orderId',$AllProcessableIds);
		}
		$allSaveSalesOrdersDatas	= $this->ci->db->select('orderId,createOrderId,rowData,createdRowData,sendInAggregation')->get_where('sales_order',array('isNetOff' => '0', 'status <>' => '0','sendInAggregation <>' => 1, 'account2Id' => $account2Id))->result_array();
		if($allSaveSalesOrdersDatas){
			foreach($allSaveSalesOrdersDatas as $allSaveSalesOrdersData){
				if($allSaveSalesOrdersData['sendInAggregation']){
					continue;
				}
				if($allSaveSalesOrdersData['createOrderId']){
					$allSaveSalesOrders[$allSaveSalesOrdersData['orderId']]			= $allSaveSalesOrdersData;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$allSavePurchaseOrders		= array();
	if($AllProcessableIds){
		$this->ci->db->where_in('orderId',$AllProcessableIds);
	}
	$allSavePurchaseOrdersDatas	= $this->ci->db->select('orderId,createOrderId,rowData,createdRowData')->get_where('purchase_order',array('status <>' => '0', 'account2Id' => $account2Id))->result_array();
	if($allSavePurchaseOrdersDatas){
		foreach($allSavePurchaseOrdersDatas as $allSavePurchaseOrdersData){
			if($allSavePurchaseOrdersData['createOrderId']){
				$allSavePurchaseOrders[$allSavePurchaseOrdersData['orderId']]	= $allSavePurchaseOrdersData;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings		= array();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}
	
	if($enableAssignmentMapping){
		$this->ci->db->reset_query();
		$AssignmentMappingKey		= '';
		$AssignmentMappingKeyTemp	= '';
		$AssignmentMappings			= array();
		$AssignmentMappingsTemps	= $this->ci->db->order_by('id','asc')->get_where('mapping_assignment',array('account2Id' => $account2Id))->result_array();
		if($AssignmentMappingsTemps){
			foreach($AssignmentMappingsTemps as $AssignmentMappingsTemp){
				if($AssignmentMappingsTemp['account1StaffId']){
					$AssignmentMappingKey		= 'account1StaffId';
					break;
				}
				elseif($AssignmentMappingsTemp['account1ProjectId']){
					$AssignmentMappingKey		= 'account1ProjectId';
					break;
				}
				elseif($AssignmentMappingsTemp['account1ChannelId']){
					$AssignmentMappingKey		= 'account1ChannelId';
					break;
				}
				elseif($AssignmentMappingsTemp['account1LeadSourceId']){
					$AssignmentMappingKey		= 'account1LeadSourceId';
					break;
				}
				elseif($AssignmentMappingsTemp['account1TeamId']){
					$AssignmentMappingKey		= 'account1TeamId';
					break;
				}
				elseif($AssignmentMappingsTemp['account1APIFieldValueId']){
					$account1APIFieldValueId	= trim($AssignmentMappingsTemp['account1APIFieldValueId']);
					$account1APIFieldValueId	= explode("||", $account1APIFieldValueId);
					$account1APIFieldValueId	= array_filter($account1APIFieldValueId);
					$account1APIFieldValueId	= array_unique($account1APIFieldValueId);
					$AssignmentMappingKey		= trim($account1APIFieldValueId[0]);
					$AssignmentMappingKeyTemp	= 'customAPIField';
					break;
				}
				else{
					break;
				}
			}
			if($AssignmentMappingKey){
				foreach($AssignmentMappingsTemps as $AssignmentMappingsTemp){
					if($AssignmentMappingsTemp['account1APIFieldValueId']){
						$account1APIFieldValueId	= trim($AssignmentMappingsTemp['account1APIFieldValueId']);
						$account1APIFieldValueId	= explode("||", $account1APIFieldValueId);
						if((is_array($account1APIFieldValueId)) AND (!empty($account1APIFieldValueId))){
							$account1APIFieldValueId	= array_filter($account1APIFieldValueId);
							$account1APIFieldValueId	= array_unique($account1APIFieldValueId);
							$mapAPIFieldName			= trim($account1APIFieldValueId[0]);
							$mapAPIFieldValues			= trim($account1APIFieldValueId[1]);
							$mapAPIFieldValues			= explode("&&", $mapAPIFieldValues);
							if((is_array($mapAPIFieldValues)) AND (!empty($mapAPIFieldValues))){
								$mapAPIFieldValues	= array_filter($mapAPIFieldValues);
								$mapAPIFieldValues	= array_unique($mapAPIFieldValues);
								foreach($mapAPIFieldValues as $mapAPIFieldValuess){
									$AssignmentMappings[trim($AssignmentMappingKey)][strtolower(trim($mapAPIFieldValuess))]	= $AssignmentMappingsTemp;
								}
							}
						}
					}
					else{
						if($AssignmentMappingsTemp[$AssignmentMappingKey]){
							$AssignmentMappings[$AssignmentMappingsTemp[$AssignmentMappingKey]]	= $AssignmentMappingsTemp;
						}
					}
				}
			}
		}
	}
	
	if($enableChannelcustomfieldMapping){
		$this->ci->db->reset_query();
		$ChannelcustomfieldMappingKey		= '';
		$ChannelcustomfieldMappings			= array();
		$ChannelcustomfieldMappingsTemps	= $this->ci->db->order_by('id','desc')->get_where('mapping_channelcustomfield',array('account2Id' => $account2Id))->result_array();
		if($ChannelcustomfieldMappingsTemps){
			foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
				if($ChannelcustomfieldMappingsTemp['account1CustomFieldId']){
					$ChannelcustomfieldMappingKey	= 'account1CustomFieldId';
					break;
				}
				elseif($ChannelcustomfieldMappingsTemp['account1APIFieldId']){
					$ChannelcustomfieldMappingKey	= 'account1APIFieldId';
					break;
				}
				elseif($ChannelcustomfieldMappingsTemp['account1WarehouseId']){
					$ChannelcustomfieldMappingKey	= 'account1WarehouseId';
					break;
				}
				else{
					break;
				}
			}
			if($ChannelcustomfieldMappingKey){
				foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
					if($ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey]){
						$ChannelcustomfieldChannel		= $ChannelcustomfieldMappingsTemp['account1ChannelId'];
						$ChannelcustomfieldKey2Value	= $ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey];
						$ChannelcustomfieldMappings[$ChannelcustomfieldChannel][$ChannelcustomfieldKey2Value]	= $ChannelcustomfieldMappingsTemp;
					}
				}
			}
		}
	}
	
	if($datas){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($orderDatas['createdJournalsId']){continue;}
			$bpOrderId			= $orderDatas['orderId'];
			$createdParams		= array();
			
			if($orderDatas['createdParams']){
				$createdParams	= json_decode($orderDatas['createdParams'],true);
			}
			else{
				$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'cogsJournal'. DIRECTORY_SEPARATOR .$orderDatas['account2Id']. DIRECTORY_SEPARATOR .$orderDatas['journalTypeCode']. DIRECTORY_SEPARATOR;
				$rowDataFilePath	= $rowDataFilePath.$orderDatas['journalsId'].'.json';
				$createdParams		= file_get_contents($rowDataFilePath);
				$createdParams		= json_decode($createdParams,true);
			}
			if($createdParams['lastTry']){
				if($createdParams['lastTry'] > strtotime('-3 hour')){
					continue;
				}
			}
			
			if($allPostedPOCogsAll[$bpOrderId]){continue;}
			
			$journalTypeCode	= $orderDatas['journalTypeCode'];
			if($journalTypeCode == 'GO'){
				if((!$allSaveSalesOrders[$bpOrderId]) AND (!$allSavePurchaseOrders[$bpOrderId])){
					continue;
				}
				if($allSaveSalesOrders[$bpOrderId]['sendInAggregation']){
					continue;
				}
			}
			elseif(($journalTypeCode == 'SG') OR ($journalTypeCode == 'PG')){
				if(!$allSaveSalesCredits[$bpOrderId]){
					continue;
				}
				if($allSaveSalesCredits[$bpOrderId]['sendInAggregation']){
					continue;
				}
			}
			else{
				continue;
			}
			
			$MainOrderRowData			= array();
			$MainOrderCreatedRowData	= array();
			$OrderTypeData				= '';
			if($journalTypeCode == 'GO'){
				if($allSaveSalesOrders[$bpOrderId]){
					$MainOrderRowData			= json_decode($allSaveSalesOrders[$bpOrderId]['rowData'],true);
					$MainOrderCreatedRowData	= json_decode($allSaveSalesOrders[$bpOrderId]['createdRowData'],true);
					$OrderTypeData				= 'SO';
				}
				elseif($allSavePurchaseOrders[$bpOrderId]){
					$MainOrderRowData			= json_decode($allSavePurchaseOrders[$bpOrderId]['rowData'],true);
					$MainOrderCreatedRowData	= json_decode($allSavePurchaseOrders[$bpOrderId]['createdRowData'],true);
					$OrderTypeData				= 'PO';
				}
			}
			elseif(($journalTypeCode == 'SG') OR ($journalTypeCode == 'PG')){
				$MainOrderRowData			= json_decode($allSaveSalesCredits[$bpOrderId]['rowData'],true);
				$MainOrderCreatedRowData	= json_decode($allSaveSalesCredits[$bpOrderId]['createdRowData'],true);
				$OrderTypeData				= 'SC';
			}
			else{
				continue;
			}
			$config1					= $this->ci->account1Config[$orderDatas['account1Id']];
			$invoiceReference			= $orderDatas['invoiceReference'];
			$invoiceReference			= $MainOrderRowData['invoices']['0']['invoiceReference'];
			$CustomerRef				= $MainOrderRowData['reference'];
			$journalsId					= $orderDatas['journalsId'];
			$params						= json_decode($orderDatas['params'],true);
			$taxCode					= $orderDatas['taxCode'];
			$creditAmount				= $orderDatas['creditAmount'];
			$debitAmount				= $orderDatas['debitAmount'];
			$creditNominalCode			= $orderDatas['creditNominalCode'];
			$debitNominalCode			= $orderDatas['debitNominalCode'];
			$BrightpearlBaseCurrency	= strtolower($config1['currencyCode']);
			$MainOrderCurrency			= strtolower($MainOrderRowData['currency']['orderCurrencyCode']);
			$JournalCurrency			= strtolower($orderDatas['currencyCode']);
			$BPReturnedExRate			= $MainOrderRowData['currency']['exchangeRate'];
			$XeroReturnedExRate			= 1;
			$JournalCalculationVariable	= 1;
			
			if(($OrderTypeData == 'SO') OR ($OrderTypeData == 'PO')){
				$XeroReturnedExRate			= $MainOrderCreatedRowData['Response data	: ']['Invoices']['Invoice']['CurrencyRate'];
			}
			elseif($OrderTypeData == 'SC'){
				$XeroReturnedExRate			= $MainOrderCreatedRowData['Response data	: ']['CreditNotes']['CreditNote']['CurrencyRate'];
			}
			
			if($BrightpearlBaseCurrency != $XeroBaseCurrency){
				if($MainOrderCurrency != $JournalCurrency){
					$JournalCalculationVariable	= ($JournalCalculationVariable * $BPReturnedExRate);
				}
				if($MainOrderCurrency != $XeroBaseCurrency){
					$JournalCalculationVariable	= ($JournalCalculationVariable * (1 / $XeroReturnedExRate));
				}
			}
			$JournalCalculationVariable	= sprintf("%.6f",($JournalCalculationVariable));
			
			
			$channelId					= $orderDatas['channelId'];
			$staffOwnerContactId		= $MainOrderRowData['assignment']['current']['staffOwnerContactId'];
			$projectId					= $MainOrderRowData['assignment']['current']['projectId'];
			$leadSourceId				= $MainOrderRowData['assignment']['current']['leadSourceId'];
			$teamId						= $MainOrderRowData['assignment']['current']['teamId'];
			
			$CustomFieldValueID			= '';
			if((isset($MainOrderRowData['customFields'])) AND (isset($MainOrderRowData['customFields'][$config1['CustomFieldMappingFieldName']])) AND (isset($MainOrderRowData['customFields'][$config1['CustomFieldMappingFieldName']]['id']))){
				$CustomFieldValueID		= $MainOrderRowData['customFields'][$config1['CustomFieldMappingFieldName']]['id'];
			}
			
			if($this->ci->globalConfig['enableAggregationOnAPIfields']){
				$account1APIFieldIds	= explode(".",$config1['apiFieldForConsol']);
				$APIfieldValueTmps		= '';
				foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
					if(!$APIfieldValueTmps){
						$APIfieldValueTmps	= @$MainOrderRowData[$account1APIFieldIdsTemp];
					}
					else{
						$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
					}
				}
				if($APIfieldValueTmps){
					$ConsolAPIFieldValueID	= $APIfieldValueTmps;
				}
			}
			
			$trackingDetails1		= array();
			$trackingDetails2		= array();
			$trackingDetails3		= array();
			
			if($AssignmentMappings){
				if($AssignmentMappingKey == 'account1StaffId'){
					if($AssignmentMappings[$staffOwnerContactId]){
						$trackingDetails1	= $AssignmentMappings[$staffOwnerContactId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1ProjectId'){
					if($AssignmentMappings[$projectId]){
						$trackingDetails1	= $AssignmentMappings[$projectId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1ChannelId'){
					if($AssignmentMappings[$channelId]){
						$trackingDetails1	= $AssignmentMappings[$channelId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1LeadSourceId'){
					if($AssignmentMappings[$leadSourceId]){
						$trackingDetails1	= $AssignmentMappings[$leadSourceId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				elseif($AssignmentMappingKey == 'account1TeamId'){
					if($AssignmentMappings[$teamId]){
						$trackingDetails1	= $AssignmentMappings[$teamId]['account2ChannelId'];
						$trackingDetails1	= explode("~=",$trackingDetails1);
					}
				}
				else{
					if($AssignmentMappingKeyTemp == 'customAPIField'){
						$AssignmentMappingKeyPaths	= explode(".",$AssignmentMappingKey);
						$valueForAssignmentMapValue	= '';
						if((is_array($AssignmentMappingKeyPaths)) AND (!empty($AssignmentMappingKeyPaths))){
							foreach($AssignmentMappingKeyPaths as $AssignmentMappingKeyPath){
								if(!$valueForAssignmentMapValue){
									if(isset($MainOrderRowData[$AssignmentMappingKeyPath])){
										$valueForAssignmentMapValue	= $MainOrderRowData[$AssignmentMappingKeyPath];
									}
								}
								else{
									if(isset($valueForAssignmentMapValue[$AssignmentMappingKeyPath])){
										$valueForAssignmentMapValue	= $valueForAssignmentMapValue[$AssignmentMappingKeyPath];
									}
								}
							}
						}
						if($valueForAssignmentMapValue AND (isset($AssignmentMappings[$AssignmentMappingKey][strtolower($valueForAssignmentMapValue)]))){
							$trackingDetails1	= $AssignmentMappings[$AssignmentMappingKey][strtolower($valueForAssignmentMapValue)]['account2ChannelId'];
							$trackingDetails1	= explode("~=",$trackingDetails1);
						}
					}
					else{
						$trackingDetails1			= array();
					}
				}
			}
			
			if(isset($channelMappings[$channelId])){
				$trackingDetails2	= $channelMappings[$channelId]['account2ChannelId'];
				if(@$channelMappings[$channelId]['warehouseDetails'][$warehouseId]){
					$trackingDetails2	= $channelMappings[$channelId]['warehouseDetails'][$warehouseId]['account2ChannelId'];
				}
				$trackingDetails2	= explode("~=",$trackingDetails2);
			}
			
			if($ChannelcustomfieldMappings){
				if($ChannelcustomfieldMappingKey == 'account1CustomFieldId'){
					if($ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]){
						$trackingDetails3	= $ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]['account2ChannelId'];
						$trackingDetails3	= explode("~=",$trackingDetails3);
					}
				}
				elseif($ChannelcustomfieldMappingKey == 'account1APIFieldId'){
					if($ChannelcustomfieldMappings[$channelId][$ConsolAPIFieldValueID]){
						$trackingDetails3	= $ChannelcustomfieldMappings[$channelId][$ConsolAPIFieldValueID]['account2ChannelId'];
						$trackingDetails3	= explode("~=",$trackingDetails3);
					}
				}
				elseif($ChannelcustomfieldMappingKey == 'account1WarehouseId'){
					if($ChannelcustomfieldMappings[$channelId][$warehouseId]){
						$trackingDetails3	= $ChannelcustomfieldMappings[$channelId][$warehouseId]['account2ChannelId'];
						$trackingDetails3	= explode("~=",$trackingDetails3);
					}
				}
				else{
					$trackingDetails3		= array();
				}
			}
			
			if($trackingDetails3){
				$trackingDetails2	= array();
				$trackingDetails2	= $trackingDetails3;
			}
			
			$taxDate			= $params['taxDate'];
			//taxdate chanages
			
			
			if($clientcode == 'oskarswoodenarkxerom'){
				$BPDateOffset	= substr($taxDate,23,6);
				$BPDateOffset	= explode(":",$BPDateOffset);
				$tempHours		= (int)$BPDateOffset[0];
				$tempMinutes	= (int)$BPDateOffset[1];
				$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
				if($tempHours < 0){
					$totalMinutes = (-1) * $totalMinutes;
				}
				$date			= new DateTime($taxDate);
				$BPTimeZone		= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($totalMinutes){
					$totalMinutes	.= ' minute';
					$date->modify($totalMinutes);
				}
				$taxDate			= $date->format('Y-m-d');
			}
			else{
				$BPDateOffset		= (int)substr($taxDate,23,3);
				$Acc2Offset			= 0;
				$diff				= $BPDateOffset - $Acc2Offset;
				$date				= new DateTime($taxDate);
				$BPTimeZone			= 'GMT';
				$date->setTimezone(new DateTimeZone($BPTimeZone));
				if($diff){
					$diff			.= ' hour';
					$date->modify($diff);
				}
				$taxDate			= $date->format('Y-m-d');
			}
			
			if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['cogs'])) AND (strlen($dateLockSettings['cogs']) > 0)){
				$checkTaxDate	= date('Ymd',strtotime($taxDate));
				$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['cogs'])));
				
				if($dateLockSettings['cogsCondition'] == '<='){
					if($checkTaxDate <= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['cogsCondition'] == '>='){
					if($checkTaxDate >= $orderDateLock){
						//
					}
					else{continue;}
				}
				elseif($dateLockSettings['cogsCondition'] == '='){
					if($checkTaxDate == $orderDateLock){
						//
					}
					else{continue;}
				}
			}
			
			$InvoiceLineAdd			= array();
			$AllCredits				= array();
			$AllDebits				= array();
			$ItemSequence			= 0;
			$debitAccRef			= '';
			$creditAccRef			= '';
			$defaultNominalPurchase	= $config['DefaultCOGSforPurchase'];
			
			
			if($allSavePurchaseOrders[$bpOrderId]){
				$blockPosting	= 0;
				$AllCredits		= $params['credits'];
				foreach($AllCredits as $key => $CreditsData){
					if($CreditsData['transactionAmount'] == 0){
						continue;
					}
					$creditAccRef	= '';
					$journalNominal	= $CreditsData['nominalCode'];
					if(($journalNominal >= 5000) AND ($journalNominal <= 5999)){
						if(isset($nominalMappings[$journalNominal])){
							if($nominalMappings[$journalNominal]['account2NominalId']){
								$creditAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal]))){
							if($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId']){
								$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
							}
						}
						if(!$creditAccRef){
							$blockPosting	= 1;
							break;
						}
					}
					else{
						$creditAccRef	= $defaultNominalPurchase;
					}
					
					if($creditAccRef){
						$InvoiceLineAdd[$ItemSequence]	= array(
							'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ((-1) * ($CreditsData['transactionAmount'])))),
							'AccountCode'	=> $creditAccRef,
							'TaxType'		=> $config['salesNoTaxCode'],
						);
						if($trackingDetails1 OR $trackingDetails2){
							if($trackingDetails1 AND $trackingDetails2){
								$InvoiceLineAdd[$ItemSequence]['Tracking'][0]	= array(
									'Name'		=> $trackingDetails1['0'],
									'Option'	=> $trackingDetails1['1']
								);
								$InvoiceLineAdd[$ItemSequence]['Tracking'][1]	= array(
									'Name'		=> $trackingDetails2['0'],
									'Option'	=> $trackingDetails2['1']
								);
							}
							elseif($trackingDetails1 AND !$trackingDetails2){
								$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
									'Name'		=> $trackingDetails1['0'],
									'Option'	=> $trackingDetails1['1']
								);
							}
							elseif(!$trackingDetails1 AND $trackingDetails2){
								$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
									'Name'		=> $trackingDetails2['0'],
									'Option'	=> $trackingDetails2['1']
								);
							}
							else{
								unset($InvoiceLineAdd[$ItemSequence]['Tracking']);
							}
						}
						$ItemSequence++;
					}
				}
				$AllDebits	= $params['debits'];
				foreach($AllDebits as $key => $DebitsData){
					if($DebitsData['transactionAmount'] == 0){
						continue;
					}
					$debitAccRef	= '';
					$journalNominal	= $DebitsData['nominalCode'];
					if(($journalNominal >= 5000) AND ($journalNominal <= 5999)){
						if(isset($nominalMappings[$journalNominal])){
							if($nominalMappings[$journalNominal]['account2NominalId']){
								$debitAccRef	= $nominalMappings[$journalNominal]['account2NominalId'];
							}
						}
						if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$journalNominal]))){
							if($nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId']){
								$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$journalNominal]['account2NominalId'];
							}
						}
						if(!$debitAccRef){
							$blockPosting	= 1;
							break;
						}
					}
					else{
						$debitAccRef	= $defaultNominalPurchase;
					}
					if($debitAccRef){
						$InvoiceLineAdd[$ItemSequence]	= array(
							'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ($DebitsData['transactionAmount']))),
							'AccountCode'	=> $debitAccRef,
							'TaxType'		=> $config['salesNoTaxCode'],
						);
						if($trackingDetails1 OR $trackingDetails2){
							if($trackingDetails1 AND $trackingDetails2){
								$InvoiceLineAdd[$ItemSequence]['Tracking'][0]	= array(
									'Name'		=> $trackingDetails1['0'],
									'Option'	=> $trackingDetails1['1']
								);
								$InvoiceLineAdd[$ItemSequence]['Tracking'][1]	= array(
									'Name'		=> $trackingDetails2['0'],
									'Option'	=> $trackingDetails2['1']
								);
							}
							elseif($trackingDetails1 AND !$trackingDetails2){
								$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
									'Name'		=> $trackingDetails1['0'],
									'Option'	=> $trackingDetails1['1']
								);
							}
							elseif(!$trackingDetails1 AND $trackingDetails2){
								$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
									'Name'		=> $trackingDetails2['0'],
									'Option'	=> $trackingDetails2['1']
								);
							}
							else{
								unset($InvoiceLineAdd[$ItemSequence]['Tracking']);
							}
						}
						$ItemSequence++;
					}
				}
				if($blockPosting){
					$this->ci->db->update('cogs_journal',array('message' => 'NominalMapping is Missing'),array('id' => $orderDatas['id']));
					continue;
				}
			}
			else{
				$AllCredits			= $params['credits'];
				foreach($AllCredits as $key => $CreditsData){
					if($CreditsData['transactionAmount'] == 0){
						continue;
					}
					$mappedCreditNominal	= '';
					if(isset($nominalMappings[$CreditsData['nominalCode']])){
						if($nominalMappings[$CreditsData['nominalCode']]['account2NominalId']){
							$mappedCreditNominal	= $nominalMappings[$CreditsData['nominalCode']]['account2NominalId'];
						}
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$CreditsData['nominalCode']]))){
						if($nominalChannelMappings[strtolower($channelId)][$CreditsData['nominalCode']]['account2NominalId']){
							$mappedCreditNominal	= $nominalChannelMappings[strtolower($channelId)][$CreditsData['nominalCode']]['account2NominalId'];
						}
					}
					if($journalTypeCode == 'GO'){
						$creditAccRef	= $config['COGSCreditNominalSO'];
						if($mappedCreditNominal){
							$creditAccRef	= $mappedCreditNominal;
						}
					}
					if($journalTypeCode == 'PG'){
						$creditAccRef	= $config['COGSCreditNominalSCPG'];
						if($mappedCreditNominal){
							$creditAccRef	= $mappedCreditNominal;
						}
					}
					if($journalTypeCode == 'SG'){
						$creditAccRef	= $config['COGSCreditNominalSC'];
						if($mappedCreditNominal){
							$creditAccRef	= $mappedCreditNominal;
						}
					}
					$InvoiceLineAdd[$ItemSequence]	= array(
						'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ((-1) * ($CreditsData['transactionAmount'])))),
						'AccountCode'	=> $creditAccRef,
						'TaxType'		=> $config['salesNoTaxCode'],
					);
					if($trackingDetails1 OR $trackingDetails2){
						if($trackingDetails1 AND $trackingDetails2){
							$InvoiceLineAdd[$ItemSequence]['Tracking'][0]	= array(
								'Name'		=> $trackingDetails1['0'],
								'Option'	=> $trackingDetails1['1']
							);
							$InvoiceLineAdd[$ItemSequence]['Tracking'][1]	= array(
								'Name'		=> $trackingDetails2['0'],
								'Option'	=> $trackingDetails2['1']
							);
						}
						elseif($trackingDetails1 AND !$trackingDetails2){
							$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
								'Name'		=> $trackingDetails1['0'],
								'Option'	=> $trackingDetails1['1']
							);
						}
						elseif(!$trackingDetails1 AND $trackingDetails2){
							$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
								'Name'		=> $trackingDetails2['0'],
								'Option'	=> $trackingDetails2['1']
							);
						}
						else{
							unset($InvoiceLineAdd[$ItemSequence]['Tracking']);
						}
					}
					$ItemSequence++;
				}
				
				$AllDebits	= $params['debits'];
				foreach($AllDebits as $key => $DebitsData){
					if($DebitsData['transactionAmount'] == 0){
						continue;
					}
					$mappedDebitNominal	= '';
					if(isset($nominalMappings[$DebitsData['nominalCode']])){
						if($nominalMappings[$DebitsData['nominalCode']]['account2NominalId']){
							$mappedDebitNominal	= $nominalMappings[$DebitsData['nominalCode']]['account2NominalId'];
						}
					}
					if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$DebitsData['nominalCode']]))){
						if($nominalChannelMappings[strtolower($channelId)][$DebitsData['nominalCode']]['account2NominalId']){
							$mappedDebitNominal	= $nominalChannelMappings[strtolower($channelId)][$DebitsData['nominalCode']]['account2NominalId'];
						}
					}
					if($journalTypeCode == 'SG'){
						$debitAccRef	= $config['COGSDebitNominalSC'];
						if($mappedDebitNominal){
							$debitAccRef	= $mappedDebitNominal;
						}
					}
					if($journalTypeCode == 'GO'){
						$debitAccRef	= $config['COGSDebitNominalSO'];
						if($mappedDebitNominal){
							$debitAccRef	= $mappedDebitNominal;
						}
					}
					if($journalTypeCode == 'PG'){
						$debitAccRef	= $config['COGSDebitNominalSCPG'];
						if($mappedDebitNominal){
							$debitAccRef	= $mappedDebitNominal;
						}
					}
					$InvoiceLineAdd[$ItemSequence]	= array(
						'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ($DebitsData['transactionAmount']))),
						'AccountCode'	=> $debitAccRef,
						'TaxType'		=> $config['salesNoTaxCode'],
					);
					if($trackingDetails1 OR $trackingDetails2){
						if($trackingDetails1 AND $trackingDetails2){
							$InvoiceLineAdd[$ItemSequence]['Tracking'][0]	= array(
								'Name'		=> $trackingDetails1['0'],
								'Option'	=> $trackingDetails1['1']
							);
							$InvoiceLineAdd[$ItemSequence]['Tracking'][1]	= array(
								'Name'		=> $trackingDetails2['0'],
								'Option'	=> $trackingDetails2['1']
							);
						}
						elseif($trackingDetails1 AND !$trackingDetails2){
							$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
								'Name'		=> $trackingDetails1['0'],
								'Option'	=> $trackingDetails1['1']
							);
						}
						elseif(!$trackingDetails1 AND $trackingDetails2){
							$InvoiceLineAdd[$ItemSequence]['Tracking'][]	= array(
								'Name'		=> $trackingDetails2['0'],
								'Option'	=> $trackingDetails2['1']
							);
						}
						else{
							unset($InvoiceLineAdd[$ItemSequence]['Tracking']);
						}
					}
					$ItemSequence++;
				}
			}
			
			if(!$InvoiceLineAdd){
				continue;
			}
			
			$request	= array();
			$Narration	= '';
			$Narration	= $invoiceReference;
			if(!$Narration){
				$Narration	= $bpOrderId;						
			}
			if($InvoiceLineAdd){
				$request	= array(
					'Narration'					=> $Narration,
					'JournalLines'				=> $InvoiceLineAdd,
					'Date'						=> $taxDate,
					'LineAmountTypes'			=> "NoTax",
					'Status'					=> "POSTED",
					'ShowOnCashBasisReports'	=> false,
				);
				
				if($clientcode == 'keenxero'){
					$request['LineAmountTypes']	= 'Exclusive';
				}
				if($clientcode == 'nlg'){
					$request['LineAmountTypes']	= 'Exclusive';
				}
				
				//removed on 21st Oct. 2024 req by client via ticket.
				/* if(($CustomerRef) AND ($clientcode == 'pigletxero')){
					$request['Narration']		= $CustomerRef;
				}
				if(($CustomerRef) AND ($clientcode == 'pigletxerous')){
					$request['Narration']		= $CustomerRef;
				} */
			}
			if($request){
				$this->headers	= array();
				$url			= '2.0/ManualJournals';
				$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
				$createdParams	= array(
					'Request data	: '	=> $request,
					'Response data	: '	=> $results,
				);
				$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'cogsJournal'. DIRECTORY_SEPARATOR .$account2Id. DIRECTORY_SEPARATOR .$journalTypeCode. DIRECTORY_SEPARATOR;
				if(!is_dir($rowDataFilePath)){
					mkdir($rowDataFilePath,0777,true);
					chmod(dirname($rowDataFilePath), 0777);
				}
				file_put_contents($rowDataFilePath.$journalsId.'.json',json_encode($createdParams));
				//$this->ci->db->update('cogs_journal',array('createdParams' => json_encode($createdParams)),array('id' => $orderDatas['id']));
				
				if((strtolower($results['Status']) == 'ok') AND (isset($results['ManualJournals']['ManualJournal']['ManualJournalID']))){
					$this->ci->db->update('cogs_journal',array('status' => '1','invoiceReference' => $Narration,'createdJournalsId' => $results['ManualJournals']['ManualJournal']['ManualJournalID'],'message' => ''),array('id' => $orderDatas['id']));
				}
				else{
					$badRequest	= 0;
					$errormsg	= array();
					$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
					$errormsg	= json_decode($errormsg,true);
					if(isset($errormsg['head']['title'])){
						if($errormsg['head']['title'] == '400 Bad Request'){
							$badRequest	= 1;
						}
					}
					if(!$badRequest){
						$createdParams['lastTry']	= strtotime('now');
						file_put_contents($rowDataFilePath.$journalsId.'.json',json_encode($createdParams));
						//$this->ci->db->update('cogs_journal',array('createdParams' => json_encode($createdParams)),array('id' => $orderDatas['id']));
					}
				}
			}
		}
	}
}
?>