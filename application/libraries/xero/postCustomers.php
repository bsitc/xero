<?php
$this->reInitialize();
$countryLists		= array();
$countryListTemps	= json_decode(file_get_contents(FCPATH . 'application' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'country' . DIRECTORY_SEPARATOR . 'countries.json'),true);
foreach($countryListTemps as $countryListTemp){
	$countryLists[strtolower($countryListTemp['alpha-3'])]	= $countryListTemp;
}
$currencyDetails	= $this->ci->brightpearl->getAllCurrency();
$clientcode			= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	
	$config	= $this->accountConfig[$account2Id];
	
	if(($postedAccount2Id) AND ($account2Id != $postedAccount2Id)){
		continue;
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('customerId',$orgObjectId);
	}
	$datas		= $this->ci->db->where_in('status',array('0','2'))->get_where('customers',array('account2Id' => $account2Id))->result_array();
	if(!$datas){
		continue;
	}
	
	$this->ci->db->reset_query();
	$taxMappingTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$taxMapppings		= array();
	if($taxMappingTemps){
		foreach($taxMappingTemps as $taxMappingTemp){
			$orderType	= (int)$taxMappingTemp['orderType'];
			$taxMapppings[$taxMappingTemp['account1TaxId']][$orderType]	= $taxMappingTemp;
		}
	}
	
	$this->ci->db->reset_query();
	$contactLinkingTemps	= $this->ci->db->get_where('mapping_contactlinking',array('account2Id' => $account2Id))->result_array();
	$contactLinkingMapping	= array();
	if($contactLinkingTemps){
		foreach($contactLinkingTemps as $contactLinkingTempss){
			$account1FieldValues	= trim($contactLinkingTempss['account1FieldValue']);
			$account1FieldValues	= explode("||", $account1FieldValues);
			if((is_array($account1FieldValues)) AND (!empty($account1FieldValues))){
				$account1FieldValues	= array_filter($account1FieldValues);
				$account1FieldValues	= array_unique($account1FieldValues);
				$customFieldName		= trim($account1FieldValues[0]);
				$customFieldValues		= trim($account1FieldValues[1]);
				$customFieldValues		= explode("&&", $customFieldValues);
				if((is_array($customFieldValues)) AND (!empty($customFieldValues))){
					$customFieldValues	= array_filter($customFieldValues);
					$customFieldValues	= array_unique($customFieldValues);
					foreach($customFieldValues as $customFieldValuess){
						$contactLinkingMapping[strtolower(trim($customFieldName))][strtolower(trim($customFieldValuess))]	= $contactLinkingTempss;
					}
				}
			}
		}
	}
	
	if($datas){
		foreach($datas as $customerDatas){
			if(($customerDatas['status'] == 4) OR ($customerDatas['status'] == 3)){continue;}
			
			if($clientcode == 'oskarswoodenarkxerom'){
				$rowDatas	= json_decode($customerDatas['params'],true);
				if((!$rowDatas['isPrimaryContact']) AND (trim($customerDatas['company']))){
					$this->ci->db->reset_query();
					$allContactsData	= $this->ci->db->get_where('customers',array('account2Id' => $account2Id, 'company' => $customerDatas['company']))->result_array();
					if((is_array($allContactsData)) AND (!empty($allContactsData))){
						foreach($allContactsData as $allContactsDatas){
							$allContactsDatasRowData	= json_decode($allContactsDatas['params'],true);
							if($allContactsDatasRowData['isPrimaryContact']){
								$customerDatas	= $allContactsDatas;
								break;
							}
						}
					}
				}
			}
			
			$linkingId			= '';
			$linkingName		= '';
			$duplicateNameFound = 0;
			$availableName		= '';
			$request			= array();
			$ContactPersons		= array();
			$rowDatas			= json_decode($customerDatas['params'],true);
			$account1Id			= $customerDatas['account1Id'];
			$currencyDetail		= $currencyDetails[$account1Id];
			$config1			= $this->ci->account1Config[$account1Id];
			$companyName		= trim($customerDatas['company']);
			
			if(($clientcode == 'gailardexero') AND (isset($rowDatas['customFields']['PCF_ACCOUNTE'])) AND (strlen(trim($rowDatas['customFields']['PCF_ACCOUNTE'])) > 0)){
				$customerDatas['email']	= trim($rowDatas['customFields']['PCF_ACCOUNTE']);
			}
			
			$contactLinkingId	= '';
			if((is_array($contactLinkingMapping)) AND (!empty($contactLinkingMapping))){
				$allContactsCustomFields	= $rowDatas['customFields'];
				if((is_array($allContactsCustomFields)) AND (!empty($allContactsCustomFields))){
					foreach($allContactsCustomFields as $pcf_name => $allContactsCustomFieldss){
						if((isset($contactLinkingMapping[strtolower($pcf_name)]))){
							if(((is_array($allContactsCustomFieldss))) AND ((isset($allContactsCustomFieldss['value'])))){
								if((isset($contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss['value'])]))){
									$contactLinkingId	= $contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss['value'])]['account2ContactId'];
								}
							}
							else{
								if((isset($contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss)]))){
									$contactLinkingId	= $contactLinkingMapping[strtolower($pcf_name)][strtolower($allContactsCustomFieldss)]['account2ContactId'];
								}
							}
						}
						if($contactLinkingId){
							break;
						}
					}
				}
			}
			if($contactLinkingId){
				$ceatedParams	= array('Request data	: '	=> 'Linked by Contact linking mapping',);
				$this->ci->db->reset_query();
				$this->ci->db->update('customers',array('ceatedParams' => json_encode($ceatedParams), 'message' => '', 'status' => '1', 'createdCustomerId' => $contactLinkingId),array('id' => $customerDatas['id'],'account2Id' => $account2Id));
				continue;
			}
			
			
			$accQueryResults	= array();
			$accQueryContacts	= array();
			$accountReference	= '';
			if($clientcode == 'lacoquetaxero'){
				if($customerDatas['createdCustomerId']){
					$this->ci->db->update('customers',array('status' => 1),array('id' => $customerDatas['id']));
					continue;
				}
				else{
					$accQueryResults	= '';
					$accountReference	= $rowDatas['assignment']['current']['accountReference'];
					$this->headers		= array();
					$accQueryUrl		= '2.0/Contacts?where='.urlencode('AccountNumber="'.$accountReference.'"');
					$this->initializeConfig($account2Id, 'GET', $accQueryUrl);
					$accQueryResults	= $this->getCurl($accQueryUrl, 'GET', '', 'json', $account2Id)[$account2Id];
					if(strtolower($accQueryResults['Status']) == 'ok'){
						$accQueryContacts	= $accQueryResults['Contacts']['Contact'];
						if(!isset($accQueryContacts[0])){
							$accQueryContacts	= array($accQueryResults['Contacts']['Contact']);
						}
					}
					if($accQueryContacts){
						foreach($accQueryContacts as $accQueryContactsTemp){
							if($accQueryContactsTemp['AccountNumber'] == $accountReference){
								$linkingId	= $accQueryContactsTemp['ContactID'];
								break;
							}
						}
					}
					if($linkingId){
						$ceatedParams	= array(
							'Request data	: '	=> 'Linked by AccountNumber',
						);
						$this->ci->db->reset_query();
						$this->ci->db->update('customers',array('ceatedParams' => json_encode($ceatedParams), 'message' => '', 'status' => '1', 'createdCustomerId' => $linkingId),array('id' => $customerDatas['id'],'account2Id' => $account2Id));
						continue;
					}
				}
			}
			
			if($clientcode != 'oskarswoodenarkxerom'){
				if($companyName){
					$customerCount			= 0;
					$this->ci->db->reset_query();
					$ContactPersonsTemps	= $this->ci->db->get_where('customers',array('account2Id' => $account2Id, 'company' => $companyName))->result_array();
					if($ContactPersonsTemps){
						foreach($ContactPersonsTemps as $ContactPersonsTemp){
							$ContactPersonsTempRowData	= json_decode($ContactPersonsTemp['params'],true);
							if(!$ContactPersonsTempRowData['isPrimaryContact']){
								$customerCount++;
								if($customerCount < 6){
									$ContactPersons[]	= array(
										'FirstName'			=> $ContactPersonsTemp['fname'],
										'LastName'			=> $ContactPersonsTemp['lname'],
										'EmailAddress'		=> $ContactPersonsTemp['email'],
										'IncludeInEmails'	=> false,
									);
								}
							}
							else{
								$customerDatas	= $ContactPersonsTemp;
							}
						}
					}
				}
			}
			
			$companyName		= trim($customerDatas['company']);
			$rowDatas			= json_decode($customerDatas['params'],true);
			$currencyCode		= $config1['currencyCode'];
			$customerId			= $customerDatas['customerId'];
			$FullName			= (trim($customerDatas['fname'])).' '.(trim($customerDatas['lname']));
			$taxNumber			= $rowDatas['financialDetails']['taxNumber'];
			$creditTermDays		= $rowDatas['financialDetails']['creditTermDays'];
			$creditTermTypeId	= $rowDatas['financialDetails']['creditTermTypeId'];
			$bpTaxId			= $rowDatas['financialDetails']['taxCodeId'];
			$billAddress		= $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['BIL']];
			$shipAddress		= $rowDatas['postalAddresses'][$rowDatas['postAddressIds']['DEL']];
			$billCountry		= $billAddress['countryIsoCode'];
			$shipCountry		= $shipAddress['countryIsoCode'];
			
			if(($clientcode == 'gailardexero') AND (isset($rowDatas['customFields']['PCF_ACCOUNTE'])) AND (strlen(trim($rowDatas['customFields']['PCF_ACCOUNTE'])) > 0)){
				$customerDatas['email']	= trim($rowDatas['customFields']['PCF_ACCOUNTE']);
			}
			
			if(($billCountry) AND (isset($countryLists[strtolower($billCountry)]))){
				$billCountry	= $countryLists[strtolower($billCountry)]['name'];
			}
			if(($shipCountry) AND (isset($countryLists[strtolower($shipCountry)]))){
				$shipCountry	= $countryLists[strtolower($shipCountry)]['name'];
			}
			if(isset($rowDatas['financialDetails']['currencyId'])){
				if(isset($currencyDetail[$rowDatas['financialDetails']['currencyId']])){
					$currencyCode	= $currencyDetail[$rowDatas['financialDetails']['currencyId']]['code'];
				}
			}
			
			$request			= array(
				'Name'				=> ($companyName) ? ($companyName) : $FullName,
				'FirstName'			=> trim($customerDatas['fname']), 
				'LastName'			=> trim($customerDatas['lname']),
				'Addresses'			=> array(
					array(
						'AddressType'	=> 'STREET',
						'AddressLine1'	=> @$billAddress['addressLine1'],
						'AddressLine2'	=> @$billAddress['addressLine2'],
						'City'			=> @$billAddress['addressLine3'],
						'Region'		=> @$billAddress['addressLine4'],
						'PostalCode'	=> @$billAddress['postalCode'],
						'Country'		=> @$billCountry,
					),
					array(
						'AddressType'	=> 'POBOX',
						'AddressLine1'	=> @$shipAddress['addressLine1'],
						'AddressLine2'	=> @$shipAddress['addressLine2'],
						'City'			=> @$shipAddress['addressLine3'],
						'Region'		=> @$shipAddress['addressLine4'],
						'PostalCode'	=> @$shipAddress['postalCode'],
						'Country'		=> @$shipCountry,
					),
				),
				'Phones'			=> array(
					array(
						'PhoneType'		=> 'DEFAULT',
						'PhoneNumber'	=> $customerDatas['phone'], 
					),
				), 
				'IsCustomer'		=> ($customerDatas['isSupplier']) ? false : true,
				'IsSupplier'		=> ($customerDatas['isSupplier']) ? true  : false,
				'EmailAddress'		=> $customerDatas['email'],
				'DefaultCurrency'	=> $currencyCode,
				'TaxNumber'			=> $taxNumber,
			);
			
			/* if(($clientcode == 'logicofenglishxero') OR ($clientcode == 'gailardexero')){
				$request['Addresses'][0]['AddressType']	= 'POBOX';
				$request['Addresses'][1]['AddressType']	= 'STREET';
			} */
			
			$request['Addresses'][0]['AddressType']	= 'POBOX';
			$request['Addresses'][1]['AddressType']	= 'STREET';
			
			if($creditTermDays){
				if($customerDatas['isSupplier']){
					if($creditTermTypeId){
						if($creditTermTypeId == 1){
							$request['PaymentTerms']['Bills']['Day']	= $creditTermDays;
							$request['PaymentTerms']['Bills']['Type']	= 'DAYSAFTERBILLDATE';
						}
						elseif($creditTermTypeId == 2){
							$request['PaymentTerms']['Bills']['Day']	= $creditTermDays;
							$request['PaymentTerms']['Bills']['Type']	= 'DAYSAFTERBILLMONTH';
						}
						else{
							$request['PaymentTerms']['Bills']['Day']	= $creditTermDays;
							$request['PaymentTerms']['Bills']['Type']	= 'DAYSAFTERBILLDATE';
						}
					}
					else{
						$request['PaymentTerms']['Bills']['Day']		= $creditTermDays;
						$request['PaymentTerms']['Bills']['Type']		= 'DAYSAFTERBILLDATE';
					}
				}
				else{
					if($creditTermTypeId){
						if($creditTermTypeId == 1){
							$request['PaymentTerms']['Sales']['Day']	= $creditTermDays;
							$request['PaymentTerms']['Sales']['Type']	= 'DAYSAFTERBILLDATE';
						}
						elseif($creditTermTypeId == 2){
							$request['PaymentTerms']['Sales']['Day']	= $creditTermDays;
							$request['PaymentTerms']['Sales']['Type']	= 'DAYSAFTERBILLMONTH';
						}
						else{
							$request['PaymentTerms']['Sales']['Day']	= $creditTermDays;
							$request['PaymentTerms']['Sales']['Type']	= 'DAYSAFTERBILLDATE';
						}
					}
					else{
						$request['PaymentTerms']['Sales']['Day']		= $creditTermDays;
						$request['PaymentTerms']['Sales']['Type']		= 'DAYSAFTERBILLDATE';
					}
				}
			}
			if($ContactPersons){
				$request['ContactPersons']	= $ContactPersons;
			}
			if(isset($taxMapppings[$bpTaxId])){
				$taxMappping	= $taxMapppings[$bpTaxId];
				if(!isset($taxMappping['1'])){
					$taxMappping['1']	= $taxMappping['0'];
				}
				if(!isset($taxMappping['2'])){
					$taxMappping['2']	= $taxMappping['0'];
				}
				$request['AccountsReceivableTaxType']	= $taxMappping['1']['account2TaxId'];
				$request['AccountsPayableTaxType']		= $taxMappping['2']['account2TaxId'];
			}
			
			if(!$customerDatas['isSupplier']){
				if(!$rowDatas['isPrimaryContact']){
					$bpCompanyName	= $rowDatas['organisation']['name'];
					if($bpCompanyName){
						$isPrimaryCompanySent	= 0;
						$this->ci->db->reset_query();
						$bpCompanyDatas			= $this->ci->db->get_where('customers',array('createdCustomerId <> ' => '','company' => $bpCompanyName))->result_array();
						$bpCompanyDatasInfo		= array();
						foreach($bpCompanyDatas as $bpCompanyData){
							$bpCompanyDataRowData	= json_decode($bpCompanyData['params'],true);
							if($bpCompanyDataRowData['isPrimaryContact']){
								$isPrimaryCompanySent	= 1;
								$bpCompanyDatasInfo		= $bpCompanyData;
								break;
							}
						}
						if($isPrimaryCompanySent){
							$request['Job']			= true;
							$request['ParentRef']	= array('value' => $bpCompanyDatasInfo['createdCustomerId']);
						}
						else{
							$this->ci->db->reset_query();
							$this->ci->db->update('customers',array('message' => 'Primary customer not sent yet'),array('id' => $customerDatas['id']));
						}
					}
				}
			}
			if(@$rowDatas['communication']['telephones']['MOB']){
				$request['Phones'][]	= array(
					'PhoneType'		=> 'MOBILE',
					'PhoneNumber'	=> $rowDatas['communication']['telephones']['MOB'], 
				);
			}
			
			
			if($customerDatas['createdCustomerId']){
				//unset($request['Name']);
				$request['ContactID']	= $customerDatas['createdCustomerId'];
				$queryContacts	= array();
				$xeroQueryName	= $request['Name'];
				$contactCounter	= 0;
					
				$this->headers	= array();
				$queryUrl		= '2.0/Contacts?searchTerm='.urlencode($xeroQueryName);
				$this->initializeConfig($account2Id, 'GET', $queryUrl);
				$queryResults	= $this->getCurl($queryUrl, 'GET', '', 'json', $account2Id)[$account2Id];
				
				if(strtolower($queryResults['Status']) == 'ok'){
					$queryContacts	= $queryResults['Contacts']['Contact'];
					if(!isset($queryContacts[0])){
						$queryContacts	= array($queryResults['Contacts']['Contact']);
					}
				}
				if($queryContacts){
					foreach($queryContacts as $queryContactsTemp){
						if($xeroQueryName){
							if(substr_count($queryContactsTemp['Name'],$xeroQueryName)){
								if($queryContactsTemp['ContactID'] == $request['ContactID']){
									if(trim($queryContactsTemp['Name']) == trim($xeroQueryName)){
										$linkingName = $queryContactsTemp['Name'];
										break;
									}
									else{
										$contactCounter++;
										continue;
									}
								}
								else{
									$matchName	= str_replace(".","",$queryContactsTemp['Name']);
									if(trim($matchName) == trim($xeroQueryName)){
										$contactCounter++;
									}
								}
							}
						}
					}
					
					$duplicateNameFound	= 0;
					$newQueryName		= $xeroQueryName;
					$availableName		= '';
					for($i = 1; $i <= 6; $i++){
						if($i != 1){
							$newQueryName = $newQueryName.'.';
						}
						foreach($queryContacts as $queryContactsTemp){
							if($xeroQueryName){
								if(substr_count($queryContactsTemp['Name'],$xeroQueryName)){
									if($newQueryName == $queryContactsTemp['Name']){
										$duplicateNameFound = 1;
										$availableName		= '';
										break;
									}
									else{
										$duplicateNameFound = 0;
										$availableName		= $newQueryName;
									}
								}
							}
						}
						if(($duplicateNameFound == 0) AND ($availableName)){
							break;
						}
					}
				}
				if($linkingName){
					$request['Name']	= $linkingName;
				}
				else{
					if(($duplicateNameFound == 0) AND ($availableName)){
						$request['Name']	= $availableName;
					}
					elseif($contactCounter){
						for($i = 1; $i <= $contactCounter; $i++){
							$request['Name']	= $request['Name'].'.';
						}
					}
				}
			}
			else{
				if(!$companyName){
					$queryContacts	= array();
					$xeroQueryName	= $request['Name'];
					$xeroQueryEmail	= $request['EmailAddress'];
					$contactCounter	= 0;
					
					$this->headers	= array();
					$queryUrl		= '2.0/Contacts?searchTerm='.urlencode($xeroQueryName);
					$this->initializeConfig($account2Id, 'GET', $queryUrl);
					$queryResults	= $this->getCurl($queryUrl, 'GET', '', 'json', $account2Id)[$account2Id];
					
					if(strtolower($queryResults['Status']) == 'ok'){
						$queryContacts	= $queryResults['Contacts']['Contact'];
						if(!isset($queryContacts[0])){
							$queryContacts	= array($queryResults['Contacts']['Contact']);
						}
					}
					
					if($queryContacts){
						foreach($queryContacts as $queryContactsTemp){
							if($xeroQueryName){
								if(substr_count($queryContactsTemp['Name'],$xeroQueryName)){
									$matchName	= str_replace(".","",$queryContactsTemp['Name']);
									if(trim($matchName) != trim($xeroQueryName)){
										continue;
									}
									if(trim($xeroQueryEmail) == trim($queryContactsTemp['EmailAddress'])){
										$linkingId		= $queryContactsTemp['ContactID'];
										$linkingName	= $queryContactsTemp['Name'];
										break;
									}
									else{
										$contactCounter++;
									}
								}
							}
						}
						
						$duplicateNameFound	= 0;
						$newQueryName		= $xeroQueryName;
						$availableName		= '';
						for($i = 1; $i <= 6; $i++){
							if($i != 1){
								$newQueryName = $newQueryName.'.';
							}
							foreach($queryContacts as $queryContactsTemp){
								if($xeroQueryName){
									if(substr_count($queryContactsTemp['Name'],$xeroQueryName)){
										if($newQueryName == $queryContactsTemp['Name']){
											$duplicateNameFound = 1;
											$availableName		= '';
											break;
										}
										else{
											$duplicateNameFound = 0;
											$availableName		= $newQueryName;
										}
									}
								}
							}
							if(($duplicateNameFound == 0) AND ($availableName)){
								break;
							}
						}
					}
					if($linkingId){
						$request['ContactID']	= $linkingId;
						$request['Name']		= $linkingName;
					}
					else{
						if(($duplicateNameFound == 0) AND ($availableName)){
							$request['Name']	= $availableName;
						}
						elseif($contactCounter){
							for($i = 1; $i <= $contactCounter; $i++){
								$request['Name']	= $request['Name'].'.';
							}
						}
					}
				}
			}
			
			if(($clientcode == 'hawkeusxero') OR ($clientcode == 'hawkeopticsxero')){
				if($request['ContactID']){
					$requiredFields	= array('Name', 'Addresses', 'ContactID');
					foreach($request as $fieldName => $requestTemp){
						if(!in_array($fieldName, $requiredFields)){
							unset($request[$fieldName]);
						}
					}
				}
			}
			
			if($clientcode == 'oskarswoodenarkxerom'){
				if(isset($request['ContactPersons'])){
					unset($request['ContactPersons']);
				}
			}
			
			if($clientcode == 'xerouk'){
				if(isset($rowDatas['customFields']['PCF_STATEMEN'])){
					if(strlen(trim($rowDatas['customFields']['PCF_STATEMEN'])) > 0){
						if(isset($request['ContactPersons'])){
							if(count($request['ContactPersons']) == 5){
								$request['ContactPersons'][4]	= array(
									'FirstName'			=> 'Statements',
									'EmailAddress'		=> $rowDatas['customFields']['PCF_STATEMEN'],
									'IncludeInEmails'	=> true,
								);
							}
							elseif(count($request['ContactPersons']) < 5){
								$request['ContactPersons'][]	= array(
									'FirstName'			=> 'Statements',
									'EmailAddress'		=> $rowDatas['customFields']['PCF_STATEMEN'],
									'IncludeInEmails'	=> true,
								);
							}
						}
						else{
							$request['ContactPersons'][]	= array(
								'FirstName'			=> 'Statements',
								'EmailAddress'		=> $rowDatas['customFields']['PCF_STATEMEN'],
								'IncludeInEmails'	=> true,
							);
						}
					}
				}
			}
			
			if($request){
				$this->headers	= array();
				$url			= '2.0/Contacts';
				$this->initializeConfig($account2Id, 'POST', $url);
				$results		= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
				$ceatedParams		= array(
					'Request data	: '	=> $request,
					'Response data	: '	=> $results,
				);
				$this->ci->db->reset_query();
				$this->ci->db->update('customers',array('ceatedParams' => json_encode($ceatedParams)),array('account2Id' => $account2Id, 'id' => $customerDatas['id']));
				if((strtolower($results['Status']) == 'ok') AND ($results['Contacts']['Contact']['ContactID'])){
					$xeroContactID		= $results['Contacts']['Contact']['ContactID'];
					$xeroContactName	= $results['Contacts']['Contact']['Name'];
					$this->ci->db->reset_query();
					$this->ci->db->update('customers',array('message' => '', 'status' => '1', 'createdCustomerId' => $xeroContactID),array('id' => $customerDatas['id'],'account2Id' => $account2Id));
					if($companyName){
						$this->ci->db->reset_query();
						$this->ci->db->update('customers',array('message' => '', 'status' => '1', 'createdCustomerId' => $xeroContactID),array('company' => $companyName,'account2Id' => $account2Id));
					}
				}
				
				//new logStoringFunctionality
				$path	= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account2'. DIRECTORY_SEPARATOR . $account2Id. DIRECTORY_SEPARATOR .'customers'. DIRECTORY_SEPARATOR;
				if(!is_dir($path)){
					mkdir($path,0777,true);
					chmod(dirname($path), 0777);
				}
				file_put_contents($path.$customerId.'.logs',"\n\n Xero Log Added On : ".date('c')." \n". json_encode($ceatedParams),FILE_APPEND);
			}
			
		}
	}
}
?>