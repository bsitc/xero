<?php
$this->reInitialize();
$disablePCpaymentqbotobp	= $this->ci->globalConfig['disablePCpaymentqbotobp'];
foreach($this->accountDetails as $account2Id => $accountDetails){
	if($disablePCpaymentqbotobp){
		continue;
	}
	$batchUpdates		= array();
	$pendingPayments	= array();
	
	$this->ci->db->reset_query();
	$pendingPaymentsTemps	= $this->ci->db->select('orderId,createOrderId,isPaymentCreated,sendPaymentTo,paymentDetails')->get_where('purchase_credit_order',array('createOrderId <>' => '','account2Id' => $account2Id))->result_array();
	if($pendingPaymentsTemps){
		foreach($pendingPaymentsTemps as $pendingPaymentsTemp){
			if($pendingPaymentsTemp['createOrderId']){
				$pendingPayments[$pendingPaymentsTemp['createOrderId']] = $pendingPaymentsTemp;
			}
		}
	}
	
	$this->headers		= array();
	$url				= '2.0/Payment?where='.urlencode('PaymentType="APCREDITPAYMENT"');
	$this->headers['If-Modified-Since']	= gmdate('c',strtotime('-3 days'));
	$results			= $this->getCurl($url, 'GET','', 'json', $account2Id)[$account2Id];
	if($results){
		$PaymentsRes	= $results['Payments']['Payment'];
		if(!isset($PaymentsRes['0'])){
			$PaymentsRes	= array($PaymentsRes);
		}
		foreach($PaymentsRes as $Payments){
			$Invoices	= $Payments['Invoice'];
			if(!isset($Invoices['0'])){
				$Invoices	= array($Invoices);
			}
			foreach($Invoices as $Invoice){
				$inoviceId		= $Invoice['InvoiceID'];
				$paymentDetails	= array();
				if(!$inoviceId){
					continue;
				}
				if(!isset($pendingPayments[$inoviceId])){
					continue;
				}
				if(!isset($batchUpdates[$inoviceId]['paymentDetails'])){
					$paymentDetails	= json_decode($pendingPayments[$inoviceId]['paymentDetails'],true);
				}
				else{
					$paymentDetails = $batchUpdates[$inoviceId]['paymentDetails'];
				}
				if(isset($paymentDetails[$Payments['PaymentID']]) AND ($Payments['Status'] == 'AUTHORISED')){
					continue;
				}
				if(isset($paymentDetails[$Payments['PaymentID']])){
					if($Payments['Status'] == 'DELETED'){
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'yes'){
							continue;
						}
						if(strtolower($paymentDetails[$Payments['PaymentID']]['DeletedonBrightpearl']) == 'no'){
							$brightpearlpaykey	= $paymentDetails[$Payments['PaymentID']]['brightpearlPayID'];
							$paymentDetails[$Payments['PaymentID']]	= array(
								'amount'				=> $Payments['Amount'],
								'sendPaymentTo'			=> 'brightpearl',
								'status'				=> '0',
								'Reference'				=> $Payments['Reference'],
								'currency'				=> $Payments['Invoice']['CurrencyCode'],
								'CurrencyRate'			=> $Payments['CurrencyRate'],
								'PaymentStatus'			=> $Payments['Status'],
								'paymentMethod'			=> $Payments['Account']['Code'],
								'paymentDate'			=> $Payments['Date'],
								'DeletedonBrightpearl'	=> 'NO',
								'paymentInitiateIn'		=> 'xero',
								'IsReversal'			=> 1,
								'brightpearlPayID'		=> $brightpearlpaykey,
							);
						}
					}
					else{
						continue;
					}
				}
				else{
					if($Payments['Status'] == 'DELETED'){
						continue;
					}
					else{
						$paymentDetails[$Payments['PaymentID']]	= array(
							'amount'				=> $Payments['Amount'],
							'sendPaymentTo'			=> 'brightpearl',
							'status'				=> '0',
							'Reference'				=> $Payments['Reference'],
							'currency'				=> $Payments['Invoice']['CurrencyCode'],
							'CurrencyRate'			=> $Payments['CurrencyRate'],
							'PaymentStatus'			=> $Payments['Status'],
							'paymentMethod'			=> $Payments['Account']['Code'],
							'paymentDate'			=> $Payments['Date'],
							'DeletedonBrightpearl'	=> 'NO',
							'paymentInitiateIn'		=> 'xero',
							'IsReversal'			=> 0,
						);
					}
				}
				$batchUpdates[$inoviceId]						= array(
					'paymentDetails'	=> $paymentDetails,
					'createOrderId'		=> $inoviceId,
					'sendPaymentTo'		=> 'brightpearl',
				);
			}
		}
	}
	if($batchUpdates){
		foreach($batchUpdates as $key => $batchUpdate){
			$batchUpdates[$key]['paymentDetails']	= json_encode($batchUpdate['paymentDetails']);
		}
		$batchUpdates	= array_chunk($batchUpdates,200);
		foreach($batchUpdates as $batchUpdate){
			if($batchUpdate){
				$this->ci->db->where(array('account2Id' => $account2Id))->update_batch('purchase_credit_order',$batchUpdate,'createOrderId'); 
			}
		}
	}
}
?>