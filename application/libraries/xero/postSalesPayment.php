<?php
//XERO_DEMO POSTSALESPAYMENT TO XERO

$this->reInitialize();
$enableAggregation			= $this->ci->globalConfig['enableAggregation'];
$disableSOpaymentbptoqbo	= $this->ci->globalConfig['disableSOpaymentbptoqbo'];
$PaymentReversalEnabled		= 1;
$clientcode					= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	if($clientcode == 'amistry'){
		//kitKing company sales is disabled for now, account Id of kitKing is 2
		if($account2Id	== 2){continue;}
	}
	if($disableSOpaymentbptoqbo){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$brightpearlconfig	= $this->ci->account1Config[$accountDetails['account1Id']];
	
	//mapping all pending and sent AmazonFee Data
	$this->ci->db->reset_query();
	$AmazonFeesChannels		= array();
	$allPendingFees			= array();
	$allSentFees			= array();
	$journalDatasTemps		= $this->ci->db->select('id, journalsId, createdJournalsId, orderId, amount, withheldAmount, journalTypeCode, taxDate, creditNominalCode, debitNominalCode, taxCode, status, message, account1Id, account2Id, withhelTax, paymentCreated, fetchDate')->get_where('amazon_ledger',array('account2Id' => $account2Id))->result_array();
	if(!empty($journalDatasTemps)){
		foreach($journalDatasTemps as $journalDatasTemp){
			if($journalDatasTemp['status'] == 0){
				$allPendingFees[$journalDatasTemp['orderId']]	= $journalDatasTemp;
			}
			elseif($journalDatasTemp['status'] == 1){
				$allSentFees[$journalDatasTemp['orderId']]		= $journalDatasTemp;
			}
		}
		$journalDatasTemps	= array();
	}
	if($config['AmazonFeesChannels']){
		$AmazonFeesChannels	= explode(",",$config['AmazonFeesChannels']);
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$totalRecord	= $this->ci->db->select('count("id") as totalSales')->get_where('sales_order',array('isNetOff' => '0', 'createOrderId <>' => '', 'status >=' => '1', 'paymentDetails <>' => '', 'account2Id' => $account2Id))->row_array()['totalSales'];
	
	if($totalRecord > 0){
		for($startPoint=0; $startPoint <= $totalRecord; $startPoint = ($startPoint+10000)){
			$this->ci->db->reset_query();
			if($orgObjectId){
				$this->ci->db->where_in('orderId',$orgObjectId);
			}
			$datas	= $this->ci->db->limit(10000, $startPoint)->get_where('sales_order',array('isNetOff' => '0', 'createOrderId <>' => '', 'status >=' => '1', 'paymentDetails <>' => '', 'account2Id' => $account2Id))->result_array();
			if(!$datas){continue;}
			
			$TotalBpAmounts	= array();
			foreach($datas as $orderDatas){
				if($orderDatas['sendInAggregation']){
					if(isset($TotalBpAmounts[$orderDatas['aggregationId']])){
						$TotalBpAmounts[$orderDatas['aggregationId']]	+= sprintf("%.4f",$orderDatas['totalAmount']);
					}
					else{
						$TotalBpAmounts[$orderDatas['aggregationId']]	= sprintf("%.4f",$orderDatas['totalAmount']);
					}
				}
			}
			
			/* $this->ci->db->reset_query();
			$paymentMappings		= array();
			$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id))->result_array();
			if(!empty($paymentMappingsTemps)){
				foreach($paymentMappingsTemps as $paymentMappingsTemp){
					$paymentMappings[strtolower(trim($paymentMappingsTemp['account1PaymentId']))]	= $paymentMappingsTemp;
				}
			} */
			// added by Arzoo on 19 march 2024
			$this->ci->db->reset_query();
			$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
			$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
			if($paymentMappingsTemps){
				foreach($paymentMappingsTemps as $paymentMappingsTemp){
					$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
					$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
					$paymentchannelIds	= array_filter($paymentchannelIds);
					$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
					$paymentcurrencys	= array_filter($paymentcurrencys);
					if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
						foreach($paymentchannelIds as $paymentchannelId){
							foreach($paymentcurrencys as $paymentcurrency){
								$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
							}
						}
					}else if((!empty($paymentchannelIds)) && (!$paymentcurrencys)){
						foreach($paymentchannelIds as $paymentchannelId){
							$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
						}
					}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
						foreach($paymentcurrencys as $paymentcurrency){
							$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
						}
					}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
						$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}
			
			/* if($this->ci->globalConfig['enableReferenceMapping']){
			} */
			$this->ci->db->reset_query();
			$referenceMappings		= array();
			$referenceMappingsTemps	= $this->ci->db->get_where('mapping_reference',array('account2Id' => $account2Id))->result_array();
			if(!empty($referenceMappingsTemps)){
				foreach($referenceMappingsTemps as $referenceMappingsTemp){
					$referenceMappings[$referenceMappingsTemp['account1ChannelId']]	= $referenceMappingsTemp;
				}
			}
			
			/* if($enableAggregation){
			} */
			$enableAggregationAdvance		= $this->ci->globalConfig['enableAggregationAdvance'];
			$enableAggregationOnAPIfields	= $this->ci->globalConfig['enableAggregationOnAPIfields'];
			
			$this->ci->db->reset_query();
			$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
			$AggregationMappings		= array();
			$AggregationMappings2		= array();
			$consolOnCustomActive		= 0;
			$consolOnAPIActive			= 0;
			if(!empty($AggregationMappingsTemps)){
				foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
					$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
					$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
					$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
					$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
					
					if(!$ConsolMappingCustomField AND !$account1APIFieldId){
						$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
					}
					else{
						if($account1APIFieldId){
							$consolOnAPIActive		= 1;
							$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
							foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
								$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
							}
						}
						else{
							$consolOnCustomActive	= 1;
							$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
						}
					}
				}
			}
		
			$XeroResponseUpdate	= array();
			$OrderGroupForGC	= array();
			if(!empty($datas)){
				foreach($datas as $orderDatas){
					if($orderDatas['status'] == 4){continue;}
					if(!$orderDatas['createOrderId']){continue;}
					
					$sendInAggregation		= $orderDatas['sendInAggregation'];
					$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
					$orderId				= $orderDatas['orderId'];
					$rowDatas				= json_decode($orderDatas['rowData'],true);
					$channelId				= $rowDatas['assignment']['current']['channelId'];
					$orderCurrencyCode		= $rowDatas['currency']['orderCurrencyCode'];
					$CustomFieldValueID		= '';
					if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
						$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
					}
					$ConsolAPIFieldValueID	= '';
					
					if($this->ci->globalConfig['enableAggregationOnAPIfields']){
						$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
						$APIfieldValueTmps		= '';
						foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
							if(!$APIfieldValueTmps){
								$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
							}
							else{
								$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
							}
						}
						if($APIfieldValueTmps){
							$ConsolAPIFieldValueID	= $APIfieldValueTmps;
						}
					}
					if($ConsolAPIFieldValueID){
						$CustomFieldValueID	= $ConsolAPIFieldValueID;
					}
					
					if($enableAggregation){
						if($sendInAggregation){
							if($AggregationMappings){
								if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
									continue;
								}
								if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['disablePayments']){
									continue;
								}
							}
							elseif($AggregationMappings2){
								if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
									continue;
								}
								if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['disablePayments']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['disablePayments']){
									continue;
								}
							}
							if($orderDatas['PostedWrong']){
								if($orderDatas['LastResponseCheck'] < strtotime('- 6 hour')){
									$XeroResponseUpdate[$orderDatas['createOrderId']]	= $orderDatas['aggregationId'];
								}
								continue;
							}
							$OrderGroupForGC[$orderDatas['aggregationId']][]	= $orderDatas;
						}
					}
				}
				
				if($OrderGroupForGC){
					foreach($OrderGroupForGC as $BatchId => $OrderGroupForGCDatas){
						$positiveGiftAmount		= 0;
						$negativeGiftAmount		= 0;
						$applicableGiftAmount	= 0;
						$orderPaymentIds		= array();
						foreach($OrderGroupForGCDatas as $OrderGroupForGCData){
							$orderId			= $OrderGroupForGCData['orderId'];
							$account1Id			= $OrderGroupForGCData['account1Id'];
							$rowDatas			= json_decode($OrderGroupForGCData['rowData'],true);
							$paymentDetails		= json_decode($OrderGroupForGCData['paymentDetails'],true);
							
							if(!$paymentDetails){continue;}
							
							$BrightpearlConfig	= $this->ci->account1Config[$account1Id];
							$giftCardPayment	= $BrightpearlConfig['giftCardPayment'];
							$brightpearlAmount	= $rowDatas['totalValue']['total'];
							foreach($paymentDetails as $key => $paymentDetail){
								if(strtolower($paymentDetail['sendPaymentTo']) == 'xero'){
									if(($paymentDetail['status'] == 0)){
										if((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
											if(($paymentDetail['paymentMethod'] == $giftCardPayment)){
												$positiveGiftAmount				+= $paymentDetail['amount'];
												$orderPaymentIds[$orderId][]	= $key;
												
											}
										}
										elseif((strtolower($paymentDetail['paymentType']) == 'payment')){
											if(($paymentDetail['paymentMethod'] == $giftCardPayment)){
												$negativeGiftAmount				+= abs($paymentDetail['amount']);
												$orderPaymentIds[$orderId][]	= $key;
											}
										}
									}
								}
							}
						}
						$applicableGiftAmount	= $positiveGiftAmount - $negativeGiftAmount;
						if($applicableGiftAmount){
							$TempSalesInfo		= $OrderGroupForGC[$BatchId][0];
							if(!$TempSalesInfo){continue;}
							$createdRowData		= json_decode($TempSalesInfo['createdRowData'],true);	
							$EditRequest		= array();
							$giftAmount			= $applicableGiftAmount;
							$this->headers		= array();
							$suburl				= '2.0/Invoices/'.$TempSalesInfo['createOrderId'];
							if($accountDetails['OAuthVersion'] == '2'){
								$suburl			= '2.0/Invoices?IDs='.$TempSalesInfo['createOrderId'].'&unitdp=4';
							}
							$this->initializeConfig($account2Id, 'GET', $suburl);
							$XeroOrderInfo	= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
							if(strtolower($XeroOrderInfo['Status']) == 'ok'){
								if(isset($XeroOrderInfo['Invoices']['Invoice'])){
									if(!$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']['0']){
										$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']	= array($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']);
									}
									$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'][count($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'])]	= array(
										'ItemCode' 		=> $config['giftCardItem'],
										'Description'	=> 'Gift Card',
										'Quantity' 		=> 1,
										'UnitAmount' 	=> (-1) * sprintf("%.4f",$giftAmount),
										'LineAmount' 	=> (-1) * sprintf("%.4f",$giftAmount),
										'TaxType' 		=> $config['salesNoTaxCode'],
										'TaxAmount' 	=> 0.00,
									);
									$EditRequest	= $XeroOrderInfo['Invoices']['Invoice'];
									$EditRequest['Contact']['Addresses']	= $EditRequest['Contact']['Addresses']['Address'];
									$EditRequest['Contact']['Phones']		= $EditRequest['Contact']['Phones']['Phone'];
									$EditRequest['LineItems']				= $EditRequest['LineItems']['LineItem'];
									foreach($EditRequest['LineItems'] as $LineKey => $EditRequestLineItems){
										if($EditRequestLineItems['Tracking']){
											foreach($EditRequestLineItems['Tracking'] as $TrackId => $AllTracking){
												unset($EditRequest['LineItems'][$LineKey]['Tracking'][$TrackId]);
												$EditRequest['LineItems'][$LineKey]['Tracking'][]	= array(
													'Name'			=> $AllTracking['Name'],
													'Option'		=> $AllTracking['Option'],
												);
												
											}
										}
										unset($EditRequest['LineItems'][$LineKey]['LineAmount']);
									}
									unset($EditRequest['SubTotal']);
									unset($EditRequest['TotalTax']);
									unset($EditRequest['Total']);
									unset($EditRequest['Payments']);
									unset($EditRequest['AmountDue']);
									unset($EditRequest['HasAttachments']);
									unset($EditRequest['HasErrors']);
									unset($EditRequest['SentToContact']);
									unset($EditRequest['Contact']['ContactPersons']);
									if($EditRequest){
										$this->headers	= array();
										$EditUrl		= '2.0/Invoices';
										if($accountDetails['OAuthVersion'] == '2'){
											$EditUrl	= '2.0/Invoices?unitdp=4';
											$this->initializeConfig($account2Id, 'POST', $EditUrl);
										}
										else{
											$EditUrl	= '2.0/Invoices';
											$UrlParams	= array('unitdp' => '4');
											$this->initializeConfig($account2Id, 'POST', $EditUrl,$UrlParams);
											$EditUrl	= '2.0/Invoices?unitdp='.urlencode('4');
										}
										$EditResponse	= $this->getCurl($EditUrl, 'POST', json_encode($EditRequest), 'json', $account2Id)[$account2Id];
										$createdRowData['Gift Card Request  : '.$BatchId]	= $EditRequest;
										$createdRowData['Gift Card Response : '.$BatchId]	= $EditResponse;
										$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('aggregationId' => $BatchId));
										if((strtolower($EditResponse['Status']) == 'ok') AND (isset($EditResponse['Invoices']['Invoice']['InvoiceID']))){
											foreach($orderPaymentIds as $TempOrderIds => $GiftPaymentKey){
												$UpdateOrderDatas	= $this->ci->db->get_where('sales_order',array('orderId' => $TempOrderIds))->row_array();
												$paymentDetails		= json_decode($UpdateOrderDatas['paymentDetails'],true);
												foreach($paymentDetails as $UpdatedKey	=> $paymentDetail){
													if(in_array($UpdatedKey,$GiftPaymentKey)){
														$paymentDetails[$UpdatedKey]['status']	= 1;
													}
												}
												$updateArray										= array();
												$paymentDetails['GiftCard : '.$BatchId]['status']	= '1';
												$paymentDetails['GiftCard : '.$BatchId]['amount']	= $giftAmount;
												$updateArray['paymentDetails']						= json_encode($paymentDetails);
												$this->ci->db->where(array('orderId' => $TempOrderIds))->update('sales_order',$updateArray);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$totalRecord	= $this->ci->db->select('count("id") as totalSales')->get_where('sales_order',array('isNetOff' => '0', 'createOrderId <>' => '', 'status >=' => '1', 'paymentDetails <>' => '', 'account2Id' => $account2Id))->row_array()['totalSales'];
	if($totalRecord > 0){
		for($startPoint=0; $startPoint <= $totalRecord; $startPoint = ($startPoint+10000)){
			$this->ci->db->reset_query();
			if($orgObjectId){
				$this->ci->db->where_in('orderId',$orgObjectId);
			}
			$datas	= $this->ci->db->limit(10000, $startPoint)->get_where('sales_order',array('isNetOff' => '0', 'createOrderId <>' => '', 'status >=' => '1', 'paymentDetails <>' => '', 'account2Id' => $account2Id))->result_array();
			if(!$datas){continue;}
			if($datas){
				
				$this->ci->db->reset_query();
				$referenceMappings		= array();
				$referenceMappingsTemps	= $this->ci->db->get_where('mapping_reference',array('account2Id' => $account2Id))->result_array();
				if(!empty($referenceMappingsTemps)){
					foreach($referenceMappingsTemps as $referenceMappingsTemp){
						$referenceMappings[$referenceMappingsTemp['account1ChannelId']]	= $referenceMappingsTemp;
					}
				}
				
				$enableAggregationAdvance		= $this->ci->globalConfig['enableAggregationAdvance'];
				$enableAggregationOnAPIfields	= $this->ci->globalConfig['enableAggregationOnAPIfields'];
				
				$this->ci->db->reset_query();
				$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
				$AggregationMappings		= array();
				$AggregationMappings2		= array();
				$consolOnCustomActive		= 0;
				$consolOnAPIActive			= 0;
				if(!empty($AggregationMappingsTemps)){
					foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
						$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
						$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
						$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
						$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
						
						if(!$ConsolMappingCustomField AND !$account1APIFieldId){
							$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]											= $AggregationMappingsTemp;
						}
						else{
							if($account1APIFieldId){
								$consolOnAPIActive		= 1;
								$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
								foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
									$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
								}
							}
							else{
								$consolOnCustomActive	= 1;
								$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]			= $AggregationMappingsTemp;
							}
						}
					}
				}
				
				$TotalBpAmounts	= array();
				foreach($datas as $orderDatas){
					if($orderDatas['sendInAggregation']){
						if(isset($TotalBpAmounts[$orderDatas['aggregationId']])){
							$TotalBpAmounts[$orderDatas['aggregationId']]	+= sprintf("%.4f",$orderDatas['totalAmount']);
						}
						else{
							$TotalBpAmounts[$orderDatas['aggregationId']]	= sprintf("%.4f",$orderDatas['totalAmount']);
						}
					}
				}
				
				
				$XeroResponseUpdate	= array();
				//fetch all PaymentJournalsInfo for Reference and ExchangeRate
				$journalIds	= array();
				foreach($datas as $orderDatas){
					if($orderDatas['status'] == 4){continue;}
					if(!$orderDatas['createOrderId']){continue;}
					$paymentDetails	= json_decode($orderDatas['paymentDetails'],true);
					if($paymentDetails){
						foreach($paymentDetails as $paymentKey => $paymentDetail){
							if($paymentDetail['sendPaymentTo'] == 'xero'){
								if($paymentDetail['status'] == '0'){
									if($paymentDetail['journalId']){
										$journalIds[]	= $paymentDetail['journalId'];
									}
								}
							}
						}
					}
				}
				$journalIds		= array_filter($journalIds);
				$journalIds		= array_unique($journalIds);
				sort($journalIds);	
				$journalDatas	= $this->ci->brightpearl->fetchJournalByIds($journalIds);
				
				foreach($datas as $orderDatas){
					if($orderDatas['status'] == 4){continue;}
					if(!$orderDatas['createOrderId']){continue;}
					if(!$orderDatas['paymentDetails']){continue;}
					
					$bpconfig				= $this->ci->account1Config[$orderDatas['account1Id']];
					$orderId				= $orderDatas['orderId'];
					$rowDatas				= json_decode($orderDatas['rowData'],true);
					$channelId				= $rowDatas['assignment']['current']['channelId'];
					$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
					$createdRowData			= json_decode($orderDatas['createdRowData'],true);
					$paymentDetails			= json_decode($orderDatas['paymentDetails'],true);
					$sendInAggregation		= $orderDatas['sendInAggregation'];
					$CustomFieldValueID		= '';
					if((isset($rowDatas['customFields'])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
						$CustomFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
					}
					$ConsolAPIFieldValueID	= '';
					$consolMappingData		= array();
					if(!$paymentDetails){continue;}
					
					if($this->ci->globalConfig['enableAggregationOnAPIfields']){
						$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
						$APIfieldValueTmps		= '';
						foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
							if(!$APIfieldValueTmps){
								$APIfieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
							}
							else{
								$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
							}
						}
						if($APIfieldValueTmps){
							$ConsolAPIFieldValueID	= $APIfieldValueTmps;
						}
					}
					if($ConsolAPIFieldValueID){
						$CustomFieldValueID	= $ConsolAPIFieldValueID;
					}
					if($enableAggregation){
						if($sendInAggregation){
							if($AggregationMappings){
								if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings[$channelId]['bpaccountingcurrency']['disablePayments']){
									continue;
								}
								if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['disablePayments']){
									continue;
								}
								if($AggregationMappings[$channelId]['bpaccountingcurrency']){
									$consolMappingData	= $AggregationMappings[$channelId]['bpaccountingcurrency'];
								}
								elseif($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]){
									$consolMappingData	= $AggregationMappings[$channelId][strtolower($orderCurrencyCode)];
								}
							}
							elseif($AggregationMappings2){
								if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]['disablePayments']){
									continue;
								}
								if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['disablePayments']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]['disablePayments']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsPaymentAggregated']){
									continue;
								}
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['disablePayments']){
									continue;
								}
								
								if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID]){
									$consolMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency'][$CustomFieldValueID];
								}
								elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']){
									$consolMappingData	= $AggregationMappings2[$channelId]['bpaccountingcurrency']['NA'];
								}
								elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID]){
									$consolMappingData	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$CustomFieldValueID];
								}
								elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']){
									$consolMappingData	= $AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA'];
								}
							}
						}
						if($orderDatas['PostedWrong']){
							if($orderDatas['LastResponseCheck'] < strtotime('- 6 hour')){
								$XeroResponseUpdate[$orderDatas['createOrderId']]	= $orderDatas['aggregationId'];
							}
							continue;
						}
					}
					
					if($sendInAggregation){
						if(empty($consolMappingData)){continue;}
					}
					
					if($allPendingFees[$orderId]){continue;}
					if($AmazonFeesChannels){
						if(in_array($channelId,$AmazonFeesChannels)){
							if(!$allSentFees[$orderId]){continue;}
						}
					}
					
					$totalOrderAmtInBase		= $rowDatas['totalValue']['baseTotal'];
					$orderForceCurrency			= $consolMappingData['orderForceCurrency'];
					$account1Id					= $orderDatas['account1Id'];
					$paymentMethod				= $orderDatas['paymentMethod'];
					$BrightpearlConfig			= $this->ci->account1Config[$account1Id];
					$giftCardPayment			= $BrightpearlConfig['giftCardPayment'];
					$parentOrderId				= $rowDatas['parentOrderId'];
					$brightpearlAmount			= $rowDatas['totalValue']['total'];
					$reference					= '';
					$CurrencyRate				= 1;
					$amount						= 0;
					$paidAmount					= 0;
					$giftAmount					= 0;
					$totalReceivedPaidAmount	= 0;
					$adjustmentPaymentAmount	= 0;
					$totalPaymentSentAmount		= 0;
					$mainPaymentFound			= 0;
					$parentPaymentInfos			= array();
					$PaidPayment				= array();
					$ReversePayment				= array();
					$updateArray				= array();
					$deletedKey					= array();
					$xeroReversePaymentIDs		= array();
					$adjutmentUsedPaymentId		= array();
					$adjPayType					= array('adjustment','other');
					$positiveGiftKey			= array();
					$negativeGiftKey			= array();
					
					$invoiceDate			= $rowDatas['invoices'][0]['taxDate'];
					if($clientcode == 'oskarswoodenarkxerom'){
						$BPDateOffset	= substr($invoiceDate,23,6);
						$BPDateOffset	= explode(":",$BPDateOffset);
						$tempHours		= (int)$BPDateOffset[0];
						$tempMinutes	= (int)$BPDateOffset[1];
						$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
						if($tempHours < 0){
							$totalMinutes = (-1) * $totalMinutes;
						}
						$date			= new DateTime($invoiceDate);
						$BPTimeZone		= 'GMT';
						$date->setTimezone(new DateTimeZone($BPTimeZone));
						if($totalMinutes){
							$totalMinutes	.= ' minute';
							$date->modify($totalMinutes);
						}
						$invoiceDate			= $date->format('Y-m-d');
					}
					else{
						$BPDateOffset			= (int)substr($invoiceDate,23,3);
						$Acc2Offset				= 0;
						$diff					= $BPDateOffset - $Acc2Offset;
						$date					= new DateTime($invoiceDate);
						$BPTimeZone				= 'GMT';
						$date->setTimezone(new DateTimeZone($BPTimeZone));
						if($diff){
							$diff	.= ' hour';
							$date->modify($diff);
						}
						$invoiceDate			= $date->format('Y-m-d');
					}
					
					foreach($paymentDetails as $paymentKey => $paymentDetail){
						if($paymentDetail['paymentMethod'] == $giftCardPayment){
							if($paymentDetail['status'] == 0){
								if((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
									$positiveGiftKey[$paymentKey]	= $paymentDetail;
								}
								else{
									$negativeGiftKey[$paymentKey]	= $paymentDetail;
								}
							}
						}
					}
					if($positiveGiftKey AND $negativeGiftKey){
						foreach($negativeGiftKey as $NGKey => $negativeGiftKeyData){
							foreach($positiveGiftKey as $PGKey => $positiveGiftKeyData){
								if($positiveGiftKeyData['amount'] == abs($negativeGiftKeyData['amount'])){
									$paymentDetails[$NGKey]['status']			= 1;
									$paymentDetails[$NGKey]['IsClosedManually']	= 1;
									$paymentDetails[$PGKey]['status']			= 1;
									$paymentDetails[$PGKey]['IsClosedManually']	= 1;
								}
							}
						}
					}
					
					//code to manage roundOff in case of baseCurrency Consol
					$totalAmtInBaseInDB	= 0;
					foreach($paymentDetails as $paymentKey => $paymentDetail){
						if($sendInAggregation){
							if($orderForceCurrency){
								$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($rowDatas['currency']['exchangeRate'])));
								$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
								
								////newCode
								$differenceInNetAmt			= 0;
								$absdifferenceInNetAmt		= 0;
								if($paymentDetail['sendPaymentTo'] == 'xero'){
									$totalAmtInBaseInDB		= ($totalAmtInBaseInDB + $paymentDetail['amount']);
								}
								if($totalOrderAmtInBase AND $totalAmtInBaseInDB){
									if($totalOrderAmtInBase != $totalAmtInBaseInDB){
										$differenceInNetAmt		= ($totalOrderAmtInBase - $totalAmtInBaseInDB);
										$absdifferenceInNetAmt	= abs($differenceInNetAmt);
										if(($absdifferenceInNetAmt) AND ($absdifferenceInNetAmt < 1)){
											if($totalOrderAmtInBase > $totalAmtInBaseInDB){
												$paymentDetail['amount']	= ($paymentDetail['amount'] + $differenceInNetAmt);
											}
											else{
												$paymentDetail['amount']	= ($paymentDetail['amount'] - $absdifferenceInNetAmt);
											}
										}
									}
								}
								////newCode
							}
						}
						if($paymentDetail['lastTry']){
							if($paymentDetail['lastTry'] > strtotime('-6 hour')){
								continue;
							}
						}
						if($paymentDetail['status'] == 1){continue;}
						if($paymentDetail['amount'] <= 0){continue;}
						if($paymentDetail['paymentMethod'] == $giftCardPayment){
							$GiftEditRequest	= array();
							$giftAmount			= $paymentDetail['amount'];
							
							$this->headers		= array();
							$suburl				= '2.0/Invoices/'.$orderDatas['createOrderId'];
							if($accountDetails['OAuthVersion'] == '2'){
								$suburl			= '2.0/Invoices?IDs='.$orderDatas['createOrderId'].'&unitdp=4';
							}
							$this->initializeConfig($account2Id, 'GET', $suburl);
							$XeroOrderInfo		= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
							if(strtolower($XeroOrderInfo['Status']) == 'ok'){
								if(isset($XeroOrderInfo['Invoices']['Invoice'])){
									if(!$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']['0']){
										$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']	= array($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']);
									}
									$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'][count($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'])]	= array(
										'ItemCode' 		=> $config['giftCardItem'],
										'Description'	=> 'Gift Card',
										'Quantity' 		=> 1,
										'UnitAmount' 	=> (-1) * sprintf("%.4f",$giftAmount),
										'LineAmount' 	=> (-1) * sprintf("%.4f",$giftAmount),
										'TaxType' 		=> $config['salesNoTaxCode'],
										'TaxAmount' 	=> 0.00,
									);
									$GiftEditRequest	= $XeroOrderInfo['Invoices']['Invoice'];
									$GiftEditRequest['Contact']['Addresses']	= $GiftEditRequest['Contact']['Addresses']['Address'];
									$GiftEditRequest['Contact']['Phones']		= $GiftEditRequest['Contact']['Phones']['Phone'];
									$GiftEditRequest['LineItems']				= $GiftEditRequest['LineItems']['LineItem'];
									foreach($GiftEditRequest['LineItems'] as $LineKey => $GiftEditRequestLineItems){
										if($GiftEditRequestLineItems['Tracking']){
											foreach($GiftEditRequestLineItems['Tracking'] as $TrackId => $AllTracking){
												unset($GiftEditRequest['LineItems'][$LineKey]['Tracking'][$TrackId]);
												$GiftEditRequest['LineItems'][$LineKey]['Tracking'][]	= array(
													'Name'			=> $AllTracking['Name'],
													'Option'		=> $AllTracking['Option'],
												);
											}
										}
										unset($GiftEditRequest['LineItems'][$LineKey]['LineAmount']);
									}
									unset($GiftEditRequest['SubTotal']);
									unset($GiftEditRequest['TotalTax']);
									unset($GiftEditRequest['Total']);
									unset($GiftEditRequest['Payments']);
									unset($GiftEditRequest['AmountDue']);
									unset($GiftEditRequest['HasAttachments']);
									unset($GiftEditRequest['HasErrors']);
									unset($GiftEditRequest['SentToContact']);
									unset($GiftEditRequest['Contact']['ContactPersons']);
									if($GiftEditRequest){
										$this->headers		= array();
										$GiftEditUrl		= '2.0/Invoices';
										if($accountDetails['OAuthVersion'] == '2'){
											$GiftEditUrl	= '2.0/Invoices?unitdp=4';
											$this->initializeConfig($account2Id, 'POST', $GiftEditUrl);
										}
										else{
											$GiftEditUrl	= '2.0/Invoices';
											$UrlParams		= array('unitdp' => '4');
											$this->initializeConfig($account2Id, 'POST', $GiftEditUrl,$UrlParams);
											$GiftEditUrl	= '2.0/Invoices?unitdp='.urlencode('4');
										}
										$GiftEditResponse	= $this->getCurl($GiftEditUrl, 'POST', json_encode($GiftEditRequest), 'json', $account2Id)[	$account2Id];
										$createdRowData['Gift Card Request  : '.$paymentKey]	= $GiftEditRequest;
										$createdRowData['Gift Card Response : '.$paymentKey]	= $GiftEditResponse;
										$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
										if((strtolower($GiftEditResponse['Status']) == 'ok') AND (isset($GiftEditResponse['Invoices']['Invoice']['InvoiceID']))){
											$updateArray		= array();
											$AmountDueOnXero	= $GiftEditResponse['Invoices']['Invoice']['AmountDue'];
											$paymentDetails[$paymentKey]['status']	= '1';
											$paymentDetails['GiftCard : '.$paymentKey]['status']	= '1';
											$paymentDetails['GiftCard : '.$paymentKey]['amount']	= $giftAmount;
											if($AmountDueOnXero == 0){
												$updateArray	= array(
													'isPaymentCreated'	=> '1',
													'status' 			=> '3',
												);
											}
											$updateArray['paymentDetails']	= json_encode($paymentDetails);
											$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
											
											if($sendInAggregation AND ($AmountDueOnXero == 0)){
												$this->ci->db->where(array('createOrderId' => $orderDatas['createOrderId']))->update('sales_order',array('isPaymentCreated' => 1, 'paymentStatus' => 1));
											}
										}
										else{
											$badRequest	= 0;
											$errormsg	= array();
											$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
											$errormsg	= json_decode($errormsg,true);
											if(isset($errormsg['head']['title'])){
												if($errormsg['head']['title'] == '400 Bad Request'){
													$badRequest	= 1;
												}
											}
											if(!$badRequest){
												$updateArray	= array();
												$paymentDetails[$paymentKey]['lastTry']	= strtotime('now');
												$updateArray['paymentDetails']			= json_encode($paymentDetails);
												$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
											}
										}
									}
								}
							}
						}
						else{
							continue;
						}
					}
					
					//Manage Adustment/Negative Payments at Initial // START
					$PositivePaymentsDatas		= array();
					$NegativePaymentsDatas		= array();
					$AdjustmentPaymentsDatas	= array();
					if(!$orderDatas['totalFeeAmount']){
						
						//code to manage roundOff in case of baseCurrency Consol
						$totalAmtInBaseInDB	= 0;
						foreach($paymentDetails as $key => $paymentDetail){
							////newCode
							if($sendInAggregation){
								if($orderForceCurrency){
									$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($rowDatas['currency']['exchangeRate'])));
									$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
									
									
									$differenceInNetAmt			= 0;
									$absdifferenceInNetAmt		= 0;
									if($paymentDetail['sendPaymentTo'] == 'xero'){
										$totalAmtInBaseInDB		= ($totalAmtInBaseInDB + $paymentDetail['amount']);
									}
									if($totalOrderAmtInBase AND $totalAmtInBaseInDB){
										if($totalOrderAmtInBase != $totalAmtInBaseInDB){
											$differenceInNetAmt		= ($totalOrderAmtInBase - $totalAmtInBaseInDB);
											$absdifferenceInNetAmt	= abs($differenceInNetAmt);
											if(($absdifferenceInNetAmt) AND ($absdifferenceInNetAmt < 1)){
												if($totalOrderAmtInBase > $totalAmtInBaseInDB){
													$paymentDetail['amount']	= ($paymentDetail['amount'] + $differenceInNetAmt);
												}
												else{
													$paymentDetail['amount']	= ($paymentDetail['amount'] - $absdifferenceInNetAmt);
												}
											}
										}
									}
								}
							}
							////newCode
							
							if(($paymentDetail['status'] == 0) AND ($paymentDetail['sendPaymentTo'] == 'xero')){
								if((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
									if($paymentDetail['paymentMethod'] != $giftCardPayment){
										$PositivePaymentsDatas[$key]		= $paymentDetail;
									}
								}
								elseif((strtolower($paymentDetail['paymentType']) == 'payment')){
									if($paymentDetail['paymentMethod'] != $giftCardPayment){
										if($paymentDetail['paymentMethod'] == 'ADJUSTMENT'){
											$AdjustmentPaymentsDatas[$key]	= $paymentDetail;
										}
										else{
											$NegativePaymentsDatas[$key]	= $paymentDetail;
										}
									}
								}
							}
						}
						if($PositivePaymentsDatas AND $NegativePaymentsDatas){
							foreach($PositivePaymentsDatas as $PositivePaymentsKey => $PositivePaymentsData){
								foreach($NegativePaymentsDatas as $NegativePaymentsKey => $NegativePaymentsData){
									if((((float)$PositivePaymentsData['amount'] + (float)$NegativePaymentsData['amount']) == (float)(0)) AND ($PositivePaymentsData['paymentMethod'] == $NegativePaymentsData['paymentMethod'])){
										$paymentDetails[$PositivePaymentsKey]['status']				= 1;
										$paymentDetails[$PositivePaymentsKey]['IsClosedManually']	= 1;
										$paymentDetails[$PositivePaymentsKey]['LinkedWithID']		= $NegativePaymentsKey;
										$paymentDetails[$NegativePaymentsKey]['status']				= 1;
										$paymentDetails[$NegativePaymentsKey]['IsClosedManually']	= 1;
										$paymentDetails[$NegativePaymentsKey]['LinkedWithID']		= $PositivePaymentsKey;
										unset($PositivePaymentsDatas[$PositivePaymentsKey]);
										unset($NegativePaymentsDatas[$NegativePaymentsKey]);
										$Break1	= 1;
										break;
									}
								}
							}
						}
						if($PositivePaymentsDatas AND $AdjustmentPaymentsDatas){
							$PostiveMethodCount	= array();
							foreach($paymentDetails as $ALLPKEY => $paymentDetail){
								if(($paymentDetail['paymentType'] == 'RECEIPT') OR ($paymentDetail['paymentType'] == 'CAPTURE')){
									if($paymentDetail['paymentMethod'] != $giftCardPayment){
										$PostiveMethodCount[]	= $paymentDetail['paymentMethod'];
									}
								}
							}
							/* foreach($PositivePaymentsDatas as $PosKey => $PositivePaymentsData){
								$PostiveMethodCount[]	= $PositivePaymentsData['paymentMethod'];
							} */
							$PostiveMethodCount	= array_filter($PostiveMethodCount);
							$PostiveMethodCount	= array_unique($PostiveMethodCount);
							if(count($PostiveMethodCount) == 1){
								foreach($AdjustmentPaymentsDatas as $AdjKey => $AdjustmentPaymentsData){
									foreach($PositivePaymentsDatas as $PosKey => $PositivePaymentsData){
										if($PositivePaymentsData['amount'] >= abs($AdjustmentPaymentsData['amount'])){
											$PositivePaymentsDatas[$PosKey]['amount']	= ($PositivePaymentsData['amount'] - abs($AdjustmentPaymentsData['amount']));
											$AdjustmentPaymentsDatas[$AdjKey]['status']	= 1;
											$paymentDetails[$AdjKey]['status']			= 1;
											$paymentDetails[$AdjKey]['LinkedWithID']	= $PosKey;
											$paymentDetails[$PosKey]['LinkedWithID']	= $AdjKey;
											unset($AdjustmentPaymentsDatas[$AdjKey]);
											break;
										}
									}
								}
							}
						}
						
						if($PositivePaymentsDatas AND $NegativePaymentsDatas){
							foreach($NegativePaymentsDatas as $NegKey => $NegativePaymentsData){
								foreach($PositivePaymentsDatas as $PosKey => $PositivePaymentsData){
									if($PositivePaymentsData['paymentMethod'] == $NegativePaymentsData['paymentMethod']){
										if($PositivePaymentsData['amount'] >= abs($NegativePaymentsData['amount'])){
											$PositivePaymentsDatas[$PosKey]['amount']	= ($PositivePaymentsData['amount'] - abs($NegativePaymentsData['amount']));
											$NegativePaymentsDatas[$NegKey]['status']	= 1;
											$paymentDetails[$NegKey]['status']			= 1;
											$paymentDetails[$NegKey]['LinkedWithID']	= $PosKey;
											unset($NegativePaymentsDatas[$NegKey]);
											break;
										}
									}
								}
							}
						}
						
						if($PositivePaymentsDatas){
							foreach($PositivePaymentsDatas as $NewPositiveKey => $PositivePaymentsData){
								/* if($sendInAggregation){
									if($orderForceCurrency){
										$PositivePaymentsData['amount']	= (($PositivePaymentsData['amount']) * ((1) / ($rowDatas['currency']['exchangeRate'])));
										$PositivePaymentsData['amount']	= sprintf("%.2f",$PositivePaymentsData['amount']);
									}
								} */
									
								if($PositivePaymentsData['amount'] == 0){
									$updateArray	= array();
									$paymentDetails[$NewPositiveKey]['status']					= 1;
									$paymentDetails[$NewPositiveKey]['AdjustMentSattlement']	= '1';
									$updateArray['paymentDetails']	= json_encode($paymentDetails);
									$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
								}
								else{
									if(($PositivePaymentsData['amount'] > 0) AND ($PositivePaymentsData['status'] == 0)){
										if($PositivePaymentsData['lastTry']){
											if($PositivePaymentsData['lastTry'] > strtotime('-6 hour')){
												continue;
											}
										}
										$ApplyAmt					= $PositivePaymentsData['amount'];
										$accountCode				= '';
										$paymentMethod				= $PositivePaymentsData['paymentMethod'];
										$OrderPayemntCurrency		= $PositivePaymentsData['currency'];
										/* if($paymentMappings[strtolower($paymentMethod)]['account2PaymentId']){
											$accountCode	= $paymentMappings[strtolower($paymentMethod)]['account2PaymentId'];
										} */
										if(isset($paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)])){
											$accountCode		= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)]['account2PaymentId'];
										}
										
										else if(isset($paymentMappings2[$channelId][strtolower($paymentMethod)])){
											$accountCode		= $paymentMappings2[$channelId][strtolower($paymentMethod)]['account2PaymentId'];
										}
										
										else if(isset($paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)])){
											$accountCode		= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)]['account2PaymentId'];
										}
										
										else if(isset($paymentMappings[strtolower($paymentMethod)])){
											$accountCode	= $paymentMappings[strtolower($paymentMethod)]['account2PaymentId'];
										}
										$paymentDate	= date('Y-m-d');
										$NewReference	= '';
										$NewExRate		= 1;
										
										if($PositivePaymentsData['paymentDate']){
											$paymentDate	= $PositivePaymentsData['paymentDate'];
											
											if($clientcode == 'oskarswoodenarkxerom'){
												$BPDateOffset	= substr($paymentDate,23,6);
												$BPDateOffset	= explode(":",$BPDateOffset);
												$tempHours		= (int)$BPDateOffset[0];
												$tempMinutes	= (int)$BPDateOffset[1];
												$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
												if($tempHours < 0){
													$totalMinutes = (-1) * $totalMinutes;
												}
												$date			= new DateTime($paymentDate);
												$BPTimeZone		= 'GMT';
												$date->setTimezone(new DateTimeZone($BPTimeZone));
												if($totalMinutes){
													$totalMinutes	.= ' minute';
													$date->modify($totalMinutes);
												}
												$paymentDate			= $date->format('Y-m-d');
											}
											else{
												$BPDateOffset	= (int)substr($paymentDate,23,3);
												$xeroOffset		= 0;
												$diff			= 0;
												$diff			= $BPDateOffset - $xeroOffset;
												$date			= new DateTime($paymentDate);
												$BPTimeZone		= 'GMT';
												$date->setTimezone(new DateTimeZone($BPTimeZone)); 
												if($diff){
													$diff			.= ' hour';
													$date->modify($diff);
												}
												$paymentDate	= $date->format('Y-m-d');
											}
										}
										if(($clientcode == 'popcultchaxero') OR ($clientcode == 'mockaxero') OR ($clientcode == 'mockaxeronz')  OR ($clientcode == 'metierxero')){
											$paymentDate	= $invoiceDate;
										}
										
										if($PositivePaymentsData['journalId']){
											if(isset($journalDatas[$PositivePaymentsData['journalId']])){
												$NewReference	= $journalDatas[$PositivePaymentsData['journalId']]['description'];
												$NewExRate		= $journalDatas[$PositivePaymentsData['journalId']]['exchangeRate'];
												if($config['useRefOnPayments']){
													if($config['useRefOnPayments'] == 'Transactionref'){
														$NewReference	= $PositivePaymentsData['Reference'];
													}
													if(!$NewReference){
														$NewReference	= $journalDatas[$PositivePaymentsData['journalId']]['description'];
													}
												}
											}
										}
										if($config['useRefOnPayments']){
											if(($config['useRefOnPayments'] != 'Transactionref') AND ($config['useRefOnPayments'] != 'Reference')){
												$account1FieldIds	= explode(".",$config['useRefOnPayments']);
												$fieldValueTmps		= '';
												foreach($account1FieldIds as $account1FieldId){
													if(!$fieldValueTmps){
														$fieldValueTmps	= $rowDatas[$account1FieldId];
													}
													else{
														$fieldValueTmps = $fieldValueTmps[$account1FieldId];
													}
												}
												if($fieldValueTmps){
													$NewReference	= $fieldValueTmps;
												}
												else{
													if(isset($journalDatas[$PositivePaymentsData['journalId']])){
														$NewReference	= $journalDatas[$PositivePaymentsData['journalId']]['description'];
													}
												}
											}
										}
										if($referenceMappings){
											if($referenceMappings[$channelId]['account1CustomFieldIdforPayRef']){
												$ValueForreference	= '';
												$account1FieldIds	= explode(".",$referenceMappings[$channelId]['account1CustomFieldIdforPayRef']);
												$fieldValue			= '';
												$fieldValueTmps		= '';
												foreach($account1FieldIds as $account1FieldId){
													if(!$fieldValueTmps){
														$fieldValueTmps	= @$rowDatas[$account1FieldId];
													}
													else{
														$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
													}
												}
												$ValueForreference	= $fieldValueTmps;
												if($ValueForreference){
													$NewReference	= trim($ValueForreference);
												}
											}
										}
										if(!$accountCode){
											$this->ci->db->update('sales_order',array('message' => 'Payment Mapping missing for '.$paymentMethod),array('orderId' => $orderId));
											continue;
										}
										$XeroPaymentRequest	= array(
											'Invoice'			=> array('InvoiceID' => $orderDatas['createOrderId']),
											'Account'			=> array('Code' => $accountCode),
											'Date'				=> $paymentDate,
											'CurrencyRate'		=> $NewExRate,
											'Reference'			=> $NewReference,
											'Amount'			=> $ApplyAmt,
										);
										if($orderForceCurrency){
											$XeroPaymentRequest['CurrencyRate']	= 1;
										}
										if(!$XeroPaymentRequest['Reference']){
											unset($XeroPaymentRequest['Reference']);
										}
										if($XeroPaymentRequest){
											$this->headers		= array();
											$url				= '2.0/Payments';
											$this->initializeConfig($account2Id, 'PUT', $url);
											$XeroPaymentresults	= $this->getCurl($url, 'PUT', json_encode($XeroPaymentRequest), 'json', $account2Id)[$account2Id];
											$createdRowData['Xero Payment Request	: ']	= $XeroPaymentRequest;
											$createdRowData['Xero Payment Response	: ']	= $XeroPaymentresults;
											$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
											if((strtolower($XeroPaymentresults['Status']) == 'ok') AND (isset($XeroPaymentresults['Payments']['Payment']['PaymentID']))){
												$updateArray	= array();
												
												$paymentDetails[$NewPositiveKey]['status']					= 1;
												$paymentDetails[$NewPositiveKey]['AdjustMentSattlement']	= '1';
												$paymentDetails[$NewPositiveKey]['xeroPaymentID']			= $XeroPaymentresults['Payments']['Payment']['PaymentID'];
												
												$paymentDetails[$XeroPaymentresults['Payments']['Payment']['PaymentID']]	= array(
													'amount' 				=> $ApplyAmt,
													'status'				=> '1',
													'AmountCreditedIn'		=> 'xero',
													'DeletedonBrightpearl'	=> 'NO',
													'paymentMethod'			=> $accountCode,
													'PaymentGroup'			=> 'Adjusted',
												);
												$AmountDueOnXero	= $XeroPaymentresults['Payments']['Payment']['Invoice']['AmountDue'];
												if($AmountDueOnXero == 0){
													$updateArray	= array(
														'isPaymentCreated'	=> '1',
														'status' 			=> '3',
													);
												}
												if($AmountDueOnXero == 0){
													$updateArray['paymentStatus']	= 1;
												}
												else{
													$updateArray['paymentStatus']	= 2;
												}
												$updateArray['paymentDetails']	= json_encode($paymentDetails);
												$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
												
												if($sendInAggregation AND ($AmountDueOnXero == 0)){
													$this->ci->db->where(array('createOrderId' => $orderDatas['createOrderId']))->update('sales_order',array('isPaymentCreated' => 1, 'paymentStatus' => 1));
												}
											}
										}
									}
								}
							}
						}
					}
					//Manage Adustment/Negative Payments at Initial // END
					
					//Payment Rerversal Code	// START
					$PostedPayments			= array();
					$ReversePayments		= array();
					$reversalDone			= 0;
					foreach($paymentDetails as $paymentKeys => $paymentDetail){
						if($paymentDetail['status'] == 1){
							if($paymentDetail['sendPaymentTo'] == 'xero'){
								if((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
									if($paymentDetail['paymentMethod'] != $giftCardPayment){
										if($paymentDetail['amount'] >= 0){
											if(!$paymentDetail['PaymentDeleted']){
												if($paymentDetail['xeroPaymentID']){
													$PostedPayments[$paymentKeys]	= $paymentDetail;
												}
											}
										}
									}
								}
							}
						}
						else{
							if($paymentDetail['sendPaymentTo'] == 'xero'){
								if((strtolower($paymentDetail['paymentType']) == 'payment')){
									if($paymentDetail['paymentMethod'] != $giftCardPayment){
										if($paymentDetail['amount'] < 0){
											$ReversePayments[$paymentKeys]		= $paymentDetail;
										}
									}
								}
							}
						}
					}
					
					if($ReversePayments AND $PostedPayments){
						foreach($PostedPayments as $SentKeys => $PostedPayment){
							if($sendInAggregation){
								continue;
							}
							foreach($ReversePayments as $ReverseKeys => $ReversePayment){
								$DeletePaymentRequest	= array();
								$isDeleted				= 0;
								if(((float)$ReversePayment['amount'] + (float)$PostedPayment['amount']) == (float)(0)){
									if(($ReversePayment['paymentMethod'] == $PostedPayment['paymentMethod'])){
										$DeletePaymentRequest	= array(
											"PaymentID"				=>	$PostedPayment['xeroPaymentID'],
											"Status"				=>	"DELETED",
										);
										$this->headers	= array();
										$DeleteUrl		= '2.0/Payments';
										$this->initializeConfig($account2Id, 'POST', $DeleteUrl);
										$DeleteResponse	= $this->getCurl($DeleteUrl, 'POST', json_encode($DeletePaymentRequest), 'json', $account2Id)[$account2Id];
										if(strtolower($DeleteResponse['Status'])=='ok'){
											$isDeleted		= 1;
											$reversalDone	= 1;
											$paymentDetails[$SentKeys]['PaymentDelete']		= 'yes';
											$paymentDetails[$ReverseKeys]['PaymentDelete']	= 'yes';
											$paymentDetails[$ReverseKeys]['reverseby']		= 'brightpearl';
											$paymentDetails[$ReverseKeys]['status']			= '1';
											$paymentDetails[$ReverseKeys]['deletedPayment']	= $PostedPayment['xeroPaymentID'];
											$updateArray['paymentDetails']					= json_encode($paymentDetails);
											$updateArray['status']							= 1;
											$updateArray['isPaymentCreated']				= 0;
											$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
											unset($ReversePayments[$ReverseKeys]);
										}
									}
								}
								if($isDeleted){
									break;
								}
							}
						}
						if($reversalDone){
							continue;
						}
					}
					//Payment Rerversal Code	// END
					
					
					if(($this->ci->globalConfig['enablePrepayments']) AND (!empty($ReversePayments))){
						$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',array('lookupInAdvance' => 1));
					}
					
					//AmazonFeesOrder Payment Code	// START
					$paymentDate				= date('Y-m-d');
					$totalAmazonOrderAmount		= 0;
					$Negativeamount				= 0;
					$sentableAmount				= 0;
					$AmazonOrderrequest			= array();
					$allAmazonPaymentIds[]		= array();
					$accountCode				= '';
					
					if($orderDatas['totalFeeAmount']){
						
						//code to manage roundOff in case of baseCurrency Consol
						$totalAmtInBaseInDB	= 0;
						foreach($paymentDetails as $key => $paymentDetail){
							if($sendInAggregation){
								if($orderForceCurrency){
									$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($rowDatas['currency']['exchangeRate'])));
									$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
									
									$differenceInNetAmt			= 0;
									$absdifferenceInNetAmt		= 0;
									if($paymentDetail['sendPaymentTo'] == 'xero'){
										$totalAmtInBaseInDB		= ($totalAmtInBaseInDB + $paymentDetail['amount']);
									}
									if($totalOrderAmtInBase AND $totalAmtInBaseInDB){
										if($totalOrderAmtInBase != $totalAmtInBaseInDB){
											$differenceInNetAmt		= ($totalOrderAmtInBase - $totalAmtInBaseInDB);
											$absdifferenceInNetAmt	= abs($differenceInNetAmt);
											if(($absdifferenceInNetAmt) AND ($absdifferenceInNetAmt < 1)){
												if($totalOrderAmtInBase > $totalAmtInBaseInDB){
													$paymentDetail['amount']	= ($paymentDetail['amount'] + $differenceInNetAmt);
												}
												else{
													$paymentDetail['amount']	= ($paymentDetail['amount'] - $absdifferenceInNetAmt);
												}
											}
										}
									}
								}
							}
							
							
							if(($paymentDetail['status'] == 0) AND ($paymentDetail['sendPaymentTo'] == 'xero')){
								if((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
									$OrderPayemntCurrency	= $paymentDetail['currency'];
									if($paymentDetail['paymentDate']){
										$paymentDate		= $paymentDetail['paymentDate'];
										
										if($clientcode == 'oskarswoodenarkxerom'){
											$BPDateOffset	= substr($paymentDate,23,6);
											$BPDateOffset	= explode(":",$BPDateOffset);
											$tempHours		= (int)$BPDateOffset[0];
											$tempMinutes	= (int)$BPDateOffset[1];
											$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
											if($tempHours < 0){
												$totalMinutes = (-1) * $totalMinutes;
											}
											$date			= new DateTime($paymentDate);
											$BPTimeZone		= 'GMT';
											$date->setTimezone(new DateTimeZone($BPTimeZone));
											if($totalMinutes){
												$totalMinutes	.= ' minute';
												$date->modify($totalMinutes);
											}
											$paymentDate			= $date->format('Y-m-d');
										}
										else{
											$BPDateOffset	= (int)substr($paymentDate,23,3);
											$xeroOffset		= 0;
											$diff			= 0;
											$diff			= $BPDateOffset - $xeroOffset;
											$date			= new DateTime($paymentDate);
											$BPTimeZone		= 'GMT';
											$date->setTimezone(new DateTimeZone($BPTimeZone)); 
											if($diff){
												$diff			.= ' hour';
												$date->modify($diff);
											}
											$paymentDate	= $date->format('Y-m-d');
										}
									}
									if(($clientcode == 'popcultchaxero') OR ($clientcode == 'mockaxero')  OR ($clientcode == 'mockaxeronz')  OR ($clientcode == 'metierxero')){
										$paymentDate	= $invoiceDate;
									}
									/* if($paymentDetail['paymentMethod']){
										if(isset($paymentMappings[strtolower($paymentDetail['paymentMethod'])])){
											$accountCode	= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
										}
									} */
									if(isset($paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
										$accountCode		= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									
									else if(isset($paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])])){
										$accountCode		= $paymentMappings2[$channelId][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									
									else if(isset($paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])])){
										$accountCode		= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									
									else if(isset($paymentMappings[strtolower($paymentDetail['paymentMethod'])])){
										$accountCode	= $paymentMappings[strtolower($paymentDetail['paymentMethod'])]['account2PaymentId'];
									}
									if($paymentDetail['journalId']){
										if(isset($journalDatas[$paymentDetail['journalId']])){
											$reference		= $journalDatas[$paymentDetail['journalId']]['description'];
											$CurrencyRate	= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
											if($config['useRefOnPayments']){
												if($config['useRefOnPayments'] == 'Transactionref'){
													$reference	= $paymentDetail['Reference'];
												}
												if(!$reference){
													$reference		= $journalDatas[$paymentDetail['journalId']]['description'];
												}
											}
										}
									}
									if($config['useRefOnPayments']){
										if(($config['useRefOnPayments'] != 'Transactionref') AND ($config['useRefOnPayments'] != 'Reference')){
											$account1FieldIds	= explode(".",$config['useRefOnPayments']);
											$fieldValueTmps		= '';
											foreach($account1FieldIds as $account1FieldId){
												if(!$fieldValueTmps){
													$fieldValueTmps	= $rowDatas[$account1FieldId];
												}
												else{
													$fieldValueTmps = $fieldValueTmps[$account1FieldId];
												}
											}
											if($fieldValueTmps){
												$reference	= $fieldValueTmps;
											}
											else{
												if(isset($journalDatas[$paymentDetail['journalId']])){
													$reference	= $journalDatas[$paymentDetail['journalId']]['description'];
												}
											}
										}
									}
									$allAmazonPaymentIds[]	= $key;
									$totalAmazonOrderAmount	+= $paymentDetail['amount'];
								}
								elseif(strtolower($paymentDetail['paymentType']) == 'payment'){
									$allAmazonPaymentIds[]	= $key;
									$Negativeamount			+= abs($paymentDetail['amount']);
								}
							}
						}
						if($totalAmazonOrderAmount){
							$sentableAmount	= $totalAmazonOrderAmount - ($Negativeamount + $orderDatas['totalFeeAmount']);
							if($sentableAmount){
								if($referenceMappings){
									if($referenceMappings[$channelId]['account1CustomFieldIdforPayRef']){
										$ValueForreference	= '';
										$account1FieldIds	= explode(".",$referenceMappings[$channelId]['account1CustomFieldIdforPayRef']);
										$fieldValue			= '';
										$fieldValueTmps		= '';
										foreach($account1FieldIds as $account1FieldId){
											if(!$fieldValueTmps){
												$fieldValueTmps	= @$rowDatas[$account1FieldId];
											}
											else{
												$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
											}
										}
										$ValueForreference	= $fieldValueTmps;
										if($ValueForreference){
											$reference	= trim($ValueForreference);
										}
									}
								}
								if(!$accountCode){
									$this->ci->db->update('sales_order',array('message' => 'Payment Mapping missing for '.$paymentMethod),array('orderId' => $orderId));
									continue;
								}
								$AmazonOrderrequest	= array(
									'Invoice'			=> array('InvoiceID' => $orderDatas['createOrderId']),
									'Account'			=> array('Code' => $accountCode),
									'Date'				=> $paymentDate,
									'CurrencyRate'		=> $CurrencyRate,
									'Reference'			=> $reference,
									'Amount'			=> $sentableAmount,
								);
							}
							if($orderForceCurrency){
								$AmazonOrderrequest['CurrencyRate']	= 1;
							}
							if(!$AmazonOrderrequest['Reference']){
								unset($AmazonOrderrequest['Reference']);
							}
							$this->headers	= array();
							$url			= '2.0/Payments';
							$this->initializeConfig($account2Id, 'PUT', $url);
							$results		= $this->getCurl($url, 'PUT', json_encode($AmazonOrderrequest), 'json', $account2Id)[$account2Id];
							$createdRowData['Xero Payment Request	: ']	= $AmazonOrderrequest;
							$createdRowData['Xero Payment Response	: ']	= $results;
							$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
							if((strtolower($results['Status']) == 'ok') AND (isset($results['Payments']['Payment']['PaymentID']))){
								$isPaymentPosted	= 1;
								$updateArray		= array();
								foreach($paymentDetails as $key => $paymentDetail){
									if(in_array($key,$allAmazonPaymentIds)){
										$paymentDetails[$key]['status']				= '1';
										$paymentDetails[$key]['amazonSettlement']	= '1';
									}
								}
								$paymentDetails['amazonFeeinfo']	= array(
									'amount'	=> $orderDatas['totalFeeAmount'],
									'status'	=> '1',
								);
								$paymentDetails[$results['Payments']['Payment']['PaymentID']]	= array(
									"amount" 				=> $sentableAmount,
									'status'				=> '1',
									'AmountCreditedIn'		=> 'xero',
									'DeletedonBrightpearl'	=> 'NO',
									'paymentMethod'			=> $accountCode,
								);
								$AmountDueOnXero	= $results['Payments']['Payment']['Invoice']['AmountDue'];
								if($AmountDueOnXero == 0){
									$updateArray	= array(
										'isPaymentCreated'	=> '1',
										'status' 			=> '3',
									);
								}
								if($AmountDueOnXero == 0){
									$updateArray['paymentStatus']	= 1;
								}
								else{
									$updateArray['paymentStatus']	= 2;
								}
								$updateArray['paymentDetails']	= json_encode($paymentDetails);
								$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
								
								if($sendInAggregation AND ($AmountDueOnXero == 0)){
									$this->ci->db->where(array('createOrderId' => $orderDatas['createOrderId']))->update('sales_order',array('isPaymentCreated' => 1, 'paymentStatus' => 1));
								}
								
							}
						}
						continue;
					}
					//AmazonFeesOrder Payment Code	// END
					
					
					//code to manage roundOff in case of baseCurrency Consol
					$totalAmtInBaseInDB	= 0;
					
					//Main Payment Sending CODE		// START
					foreach($paymentDetails as $paymentKey => $paymentDetail){
						if(($paymentDetail['amount'] > 0) AND (!$paymentDetail['AmountCreditedIn']) AND (!$paymentDetail['DeletedonBrightpearl']) AND (!$paymentDetail['AmountReversedIn'])){
							$paidAmount	+= $paymentDetail['amount'];
						}
						
						if($sendInAggregation){
							if($orderForceCurrency){
								$paymentDetail['amount']	= (($paymentDetail['amount']) * ((1) / ($rowDatas['currency']['exchangeRate'])));
								$paymentDetail['amount']	= sprintf("%.2f",$paymentDetail['amount']);
								
								$differenceInNetAmt			= 0;
								$absdifferenceInNetAmt		= 0;
								if($paymentDetail['sendPaymentTo'] == 'xero'){
									$totalAmtInBaseInDB		= ($totalAmtInBaseInDB + $paymentDetail['amount']);
								}
								if($totalOrderAmtInBase AND $totalAmtInBaseInDB){
									if($totalOrderAmtInBase != $totalAmtInBaseInDB){
										$differenceInNetAmt		= ($totalOrderAmtInBase - $totalAmtInBaseInDB);
										$absdifferenceInNetAmt	= abs($differenceInNetAmt);
										if(($absdifferenceInNetAmt) AND ($absdifferenceInNetAmt < 1)){
											if($totalOrderAmtInBase > $totalAmtInBaseInDB){
												$paymentDetail['amount']	= ($paymentDetail['amount'] + $differenceInNetAmt);
											}
											else{
												$paymentDetail['amount']	= ($paymentDetail['amount'] - $absdifferenceInNetAmt);
											}
										}
									}
								}
							}
						}
						
						if((strtolower($paymentDetail['paymentType']) == 'receipt') OR (strtolower($paymentDetail['paymentType']) == 'capture')){
							if(strtolower($paymentDetail['sendPaymentTo']) == 'xero'){
								if(($paymentDetail['status'] == '0') AND ($paymentDetail['amount'] > 0)){
									if($paymentDetail['lastTry']){
										if($paymentDetail['lastTry'] > strtotime('-6 hour')){
											continue;
										}
									}
									if($paymentDetail['paymentMethod'] == $giftCardPayment){
										$GiftEditRequest	= array();
										$giftAmount			= $paymentDetail['amount'];
										$this->headers		= array();
										$suburl				= '2.0/Invoices/'.$orderDatas['createOrderId'];
										if($accountDetails['OAuthVersion'] == '2'){
											$suburl			= '2.0/Invoices?IDs='.$orderDatas['createOrderId'].'&unitdp=4';
										}
										$this->initializeConfig($account2Id, 'GET', $suburl);
										$XeroOrderInfo		= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
										if(strtolower($XeroOrderInfo['Status']) == 'ok'){
											if(isset($XeroOrderInfo['Invoices']['Invoice'])){
												if(!$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']['0']){
													$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']	= array($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']);
												}
												$XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'][count($XeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'])]	= array(
													'ItemCode' 		=> $config['giftCardItem'],
													'Description'	=> 'Gift Card',
													'Quantity' 		=> 1,
													'UnitAmount' 	=> (-1) * sprintf("%.4f",$giftAmount),
													'LineAmount' 	=> (-1) * sprintf("%.4f",$giftAmount),
													'TaxType' 		=> $config['salesNoTaxCode'],
													'TaxAmount' 	=> 0.00,
												);
												$GiftEditRequest	= $XeroOrderInfo['Invoices']['Invoice'];
												$GiftEditRequest['Contact']['Addresses']	= $GiftEditRequest['Contact']['Addresses']['Address'];
												$GiftEditRequest['Contact']['Phones']		= $GiftEditRequest['Contact']['Phones']['Phone'];
												$GiftEditRequest['LineItems']				= $GiftEditRequest['LineItems']['LineItem'];
												foreach($GiftEditRequest['LineItems'] as $LineKey => $GiftEditRequestLineItems){
													if($GiftEditRequestLineItems['Tracking']){
														foreach($GiftEditRequestLineItems['Tracking'] as $TrackId => $AllTracking){
															unset($GiftEditRequest['LineItems'][$LineKey]['Tracking'][$TrackId]);
															$GiftEditRequest['LineItems'][$LineKey]['Tracking'][]	= array(
																'Name'			=> $AllTracking['Name'],
																'Option'		=> $AllTracking['Option'],
															);
														}
													}
													unset($GiftEditRequest['LineItems'][$LineKey]['LineAmount']);
												}
												unset($GiftEditRequest['SubTotal']);
												unset($GiftEditRequest['TotalTax']);
												unset($GiftEditRequest['Total']);
												unset($GiftEditRequest['Payments']);
												unset($GiftEditRequest['AmountDue']);
												unset($GiftEditRequest['HasAttachments']);
												unset($GiftEditRequest['HasErrors']);
												unset($GiftEditRequest['SentToContact']);
												unset($GiftEditRequest['Contact']['ContactPersons']);
												if($GiftEditRequest){
													$this->headers		= array();
													$GiftEditUrl		= '2.0/Invoices';
													if($accountDetails['OAuthVersion'] == '2'){
														$GiftEditUrl	= '2.0/Invoices?unitdp=4';
														$this->initializeConfig($account2Id, 'POST', $GiftEditUrl);
													}
													else{
														$GiftEditUrl	= '2.0/Invoices';
														$UrlParams		= array('unitdp' => '4');
														$this->initializeConfig($account2Id, 'POST', $GiftEditUrl,$UrlParams);
														$GiftEditUrl	= '2.0/Invoices?unitdp='.urlencode('4');
													}
													$GiftEditResponse	= $this->getCurl($GiftEditUrl, 'POST', json_encode($GiftEditRequest), 'json', $account2Id)[$account2Id];
													$createdRowData['Gift Card Request  : '.$paymentKey]	= $GiftEditRequest;
													$createdRowData['Gift Card Response : '.$paymentKey]	= $GiftEditResponse;
													$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
													if((strtolower($GiftEditResponse['Status']) == 'ok') AND (isset($GiftEditResponse['Invoices']['Invoice']['InvoiceID']))){
														$updateArray		= array();
														$AmountDueOnXero	= $GiftEditResponse['Invoices']['Invoice']['AmountDue'];
														$paymentDetails[$paymentKey]['status']	= '1';
														$paymentDetails['GiftCard : '.$paymentKey]['status']	= '1';
														$paymentDetails['GiftCard : '.$paymentKey]['amount']	= $giftAmount;
														if($AmountDueOnXero == 0){
															$updateArray	= array(
																'isPaymentCreated'	=> '1',
																'status' 			=> '3',
															);
														}
														$updateArray['paymentDetails']	= json_encode($paymentDetails);
														$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
														
														if($sendInAggregation AND ($AmountDueOnXero == 0)){
															$this->ci->db->where(array('createOrderId' => $orderDatas['createOrderId']))->update('sales_order',array('isPaymentCreated' => 1, 'paymentStatus' => 1));
														}
													}
													else{
														$badRequest	= 0;
														$errormsg	= array();
														$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
														$errormsg	= json_decode($errormsg,true);
														if(isset($errormsg['head']['title'])){
															if($errormsg['head']['title'] == '400 Bad Request'){
																$badRequest	= 1;
															}
														}
														if(!$badRequest){
															$updateArray	= array();
															$paymentDetails[$paymentKey]['lastTry']	= strtotime('now');
															$updateArray['paymentDetails']			= json_encode($paymentDetails);
															$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
														}
													}
												}
											}
										}
										continue;
									}
									else{
										$parentOrderId	    = '';
										$reference		    = '';
										$MainPaymentRequest	= array();
										$updateArray	    = array();
										$CurrencyRate	    = 1;
										$amount			    = $paymentDetail['amount'];
										if($amount == 0){
											$paymentDetails[$paymentKey]['status']			= '1';
											$paymentDetails[$paymentKey]['closedDueToZero']	= '1';
											$updateArray['paymentDetails']					= json_encode($paymentDetails);
											$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
										}
										if($amount <= 0){
											continue;
										}
										$accountCode	= '';
										$OrderPayemntCurrency	= '';
										if($paymentDetail['paymentMethod']){
											$paymentMethod	= $paymentDetail['paymentMethod'];
										}
										$OrderPayemntCurrency	= $paymentDetail['currency'];
										if($paymentMethod){
											if(strtolower($paymentMethod) == 'other'){
												$parentOrderId	= $rowDatas['parentOrderId'];
												if($parentOrderId){
													$parentOrderPaymentDatas		= array();
													$parentOrderPaymentDataTemp		= $this->ci->db->select('paymentDetails')->get_where('sales_order',array('orderId' => $parentOrderId))->row_array();
													if($parentOrderPaymentDataTemp){
														$parentOrderPaymentDatas	= json_decode($parentOrderPaymentDataTemp['paymentDetails'],true);
													}
													if($parentOrderPaymentDatas){
														foreach($parentOrderPaymentDatas as $parentOrderPaymentData){
															if((strtolower($parentOrderPaymentData['paymentType']) == 'receipt') OR (strtolower($parentOrderPaymentData['paymentType']) == 'capture')){
																if((strtolower($parentOrderPaymentData['paymentMethod']) != 'other') AND (strtolower($parentOrderPaymentData['paymentMethod']) != 'adjustment')){
																	$paymentMethod	= $parentOrderPaymentData['paymentMethod'];
																}
															}
														}
													}
												}
											}
											/* if(isset($paymentMappings[strtolower($paymentMethod)])){
												$accountCode	= $paymentMappings[$paymentMethod]['account2PaymentId'];
											} */
											if(isset($paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)])){
												$accountCode		= $paymentMappings1[$channelId][strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)]['account2PaymentId'];
											}
											
											else if(isset($paymentMappings2[$channelId][strtolower($paymentMethod)])){
												$accountCode		= $paymentMappings2[$channelId][strtolower($paymentMethod)]['account2PaymentId'];
											}
											
											else if(isset($paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)])){
												$accountCode		= $paymentMappings3[strtolower($OrderPayemntCurrency)][strtolower($paymentMethod)]['account2PaymentId'];
											}
											
											else if(isset($paymentMappings[strtolower($paymentMethod)])){
												$accountCode	= $paymentMappings[strtolower($paymentMethod)]['account2PaymentId'];
											}
										}
										if(@$paymentDetail['paymentDate']){
											$paymentDate	= $paymentDetail['paymentDate'];
											if($clientcode == 'oskarswoodenarkxerom'){
												$BPDateOffset	= substr($paymentDate,23,6);
												$BPDateOffset	= explode(":",$BPDateOffset);
												$tempHours		= (int)$BPDateOffset[0];
												$tempMinutes	= (int)$BPDateOffset[1];
												$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
												if($tempHours < 0){
													$totalMinutes = (-1) * $totalMinutes;
												}
												$date			= new DateTime($paymentDate);
												$BPTimeZone		= 'GMT';
												$date->setTimezone(new DateTimeZone($BPTimeZone));
												if($totalMinutes){
													$totalMinutes	.= ' minute';
													$date->modify($totalMinutes);
												}
												$paymentDate			= $date->format('Y-m-d');
											}
											else{
												$BPDateOffset	= (int)substr($paymentDate,23,3);
												$xeroOffset		= 0;
												$diff			= 0;
												$diff			= $BPDateOffset - $xeroOffset;
												$date			= new DateTime($paymentDate);
												$BPTimeZone		= 'GMT';
												$date->setTimezone(new DateTimeZone($BPTimeZone));
												if($diff){
													$diff			.= ' hour';
													$date->modify($diff);
												}
												$paymentDate	= $date->format('Y-m-d');
											}
										}
										else{
											$paymentDate	= date('c');
										}
										if(($clientcode == 'popcultchaxero') OR ($clientcode == 'mockaxero')  OR ($clientcode == 'mockaxeronz')  OR ($clientcode == 'metierxero')){
											$paymentDate	= $invoiceDate;
										}
										if($paymentDetail['journalId']){
											if(isset($journalDatas[$paymentDetail['journalId']])){
												@$reference			= $journalDatas[$paymentDetail['journalId']]['description'];
												$CurrencyRate		= $journalDatas[$paymentDetail['journalId']]['exchangeRate'];
												if($config['useRefOnPayments']){
													if($config['useRefOnPayments'] == 'Transactionref'){
														$reference	= $paymentDetail['Reference'];
													}
													if(!$reference){
														$reference		= $journalDatas[$paymentDetail['journalId']]['description'];
													}
												}
											}
										}
										if($config['useRefOnPayments']){
											if(($config['useRefOnPayments'] != 'Transactionref') AND ($config['useRefOnPayments'] != 'Reference')){
												$account1FieldIds	= explode(".",$config['useRefOnPayments']);
												$fieldValueTmps		= '';
												foreach($account1FieldIds as $account1FieldId){
													if(!$fieldValueTmps){
														$fieldValueTmps	= $rowDatas[$account1FieldId];
													}
													else{
														$fieldValueTmps = $fieldValueTmps[$account1FieldId];
													}
												}
												if($fieldValueTmps){
													$reference	= $fieldValueTmps;
												}
												else{
													$reference	= $journalDatas[$paymentDetail['journalId']]['description'];
												}
											}
										}
										if($referenceMappings){
											if($referenceMappings[$channelId]['account1CustomFieldIdforPayRef']){
												$ValueForreference	= '';
												$account1FieldIds	= explode(".",$referenceMappings[$channelId]['account1CustomFieldIdforPayRef']);
												$fieldValue			= '';
												$fieldValueTmps		= '';
												foreach($account1FieldIds as $account1FieldId){
													if(!$fieldValueTmps){
														$fieldValueTmps	= @$rowDatas[$account1FieldId];
													}
													else{
														$fieldValueTmps = @$fieldValueTmps[$account1FieldId];
													}
												}
												$ValueForreference	= $fieldValueTmps;
												if($ValueForreference){
													$reference	= trim($ValueForreference);
												}
											}
										}
										
										if(!$accountCode){
											$this->ci->db->update('sales_order',array('message' => 'Payment Mapping missing for '.$paymentMethod),array('orderId' => $orderId));
											continue;
										}
										
										
										$MainPaymentRequest	= array(
											'Invoice'			=> array('InvoiceID' => $orderDatas['createOrderId']),
											'Account'			=> array('Code' => $accountCode),
											'Date'				=> $paymentDate,
											'CurrencyRate'		=> $CurrencyRate,
											'Reference'			=> $reference,
											'Amount'			=> $amount,
										);
										if($orderForceCurrency){
											$MainPaymentRequest['CurrencyRate']	= 1;
										}
										if(!$MainPaymentRequest['Reference']){
											unset($MainPaymentRequest['Reference']);
										}
										$this->headers		= array();
										$MainPaymentUrl		= '2.0/Payments';
										$this->initializeConfig($account2Id, 'PUT', $MainPaymentUrl);
										$MainPaymentResults	= $this->getCurl($MainPaymentUrl, 'PUT', json_encode($MainPaymentRequest), 'json', $account2Id)[$account2Id];
										$createdRowData['Xero Payment Request	:'.$paymentKey]	= $MainPaymentRequest;
										$createdRowData['Xero Payment Response	:'.$paymentKey]	= $MainPaymentResults;
										$this->ci->db->update('sales_order',array('createdRowData' => json_encode($createdRowData)),array('orderId' => $orderId));
										if((strtolower($MainPaymentResults['Status']) == 'ok') AND (isset($MainPaymentResults['Payments']['Payment']['PaymentID']))){
											$updateArray		= array();
											$paymentDetails[$paymentKey]['status']			= '1';
											$paymentDetails[$paymentKey]['xeroPaymentID']	= $MainPaymentResults['Payments']['Payment']['PaymentID'];
											$paymentDetails[$MainPaymentResults['Payments']['Payment']['PaymentID']]	= array(
												"amount" 				=> $amount,
												"ParentOrderId"			=> $paymentKey,
												'status'				=> '1',
												'AmountCreditedIn'		=> 'xero',
												'DeletedonBrightpearl'	=> 'NO',
												'paymentMethod'			=> $paymentDetail['paymentMethod'],
											);
											$AmountDueOnXero	= $MainPaymentResults['Payments']['Payment']['Invoice']['AmountDue'];
											if($AmountDueOnXero == 0){
												$updateArray	= array(
													'isPaymentCreated'	=> '1',
													'status' 			=> '3',
													'paymentStatus'		=> '1',
												);
											}
											if($sendInAggregation){
												if($paidAmount >= $brightpearlAmount){
													$updateArray	= array(
														'isPaymentCreated'	=> '1',
														'status' 			=> '3',
														'paymentStatus'		=> '1',
													);
												}
												else{
													$updateArray	= array(
														'paymentStatus'		=> '2',
													);
												}
											}
											$updateArray['paymentDetails']	= json_encode($paymentDetails);
											$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
											
											if($sendInAggregation AND ($AmountDueOnXero == 0)){
												$this->ci->db->where(array('createOrderId' => $orderDatas['createOrderId']))->update('sales_order',array('isPaymentCreated' => 1, 'paymentStatus' => 1));
											}
										}
										else{
											$badRequest	= 0;
											$errormsg	= array();
											$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
											$errormsg	= json_decode($errormsg,true);
											if(isset($errormsg['head']['title'])){
												if($errormsg['head']['title'] == '400 Bad Request'){
													$badRequest	= 1;
												}
											}
											if(!$badRequest){
												$updateArray	= array();
												$paymentDetails[$paymentKey]['lastTry']	= strtotime('now');
												$updateArray['paymentDetails']			= json_encode($paymentDetails);
												$this->ci->db->where(array('orderId' => $orderId))->update('sales_order',$updateArray);
											}
										}
									}
								}
							}
						}
					}
					//Main Payment Sending CODE		// ENDS
				}
				foreach($XeroResponseUpdate as $XeroID => $aggregationId){
					$this->headers		= array();
					$suburl				= '2.0/Invoices/'.$XeroID;
					if($accountDetails['OAuthVersion'] == '2'){
						$suburl			= '2.0/Invoices?IDs='.$XeroID.'&unitdp=4';
					}
					$this->initializeConfig($account2Id, 'GET', $suburl);
					$XeroOrderInfo	= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
					if(strtolower($XeroOrderInfo['Status']) == 'ok'){
						if(isset($XeroOrderInfo['Invoices']['Invoice'])){
							if(sprintf("%.4f",$XeroOrderInfo['Invoices']['Invoice']['Total']) == sprintf("%.4f",$TotalBpAmounts[$aggregationId])){
								$this->ci->db->where_in('aggregationId',$aggregationId)->update('sales_order',array('PostedWrong' => 0,'LastResponseCheck' => ''));
							}
							else{
								$this->ci->db->where_in('aggregationId',$aggregationId)->update('sales_order',array('PostedWrong' => 1,'LastResponseCheck' => strtotime("now")));
							}
						}
					}
				}
			}
		}
	}
}
?>