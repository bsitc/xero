<?php
$this->reInitialize();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$this->ci->globalConfig['enableAmazonFees']){continue;}
	$config	= $this->accountConfig[$account2Id];
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$journalDatasTemps	= $this->ci->db->get_where('amazon_ledger',array('account2Id' => $account2Id, 'status' => '0'))->result_array();
	if((!is_array($journalDatasTemps)) OR (empty($journalDatasTemps))){continue;}
	
	$this->ci->db->reset_query();
	$AggregationMappings		= array();
	$AggregationMappings2		= array();
	$AggregationMappingsTemps	= $this->ci->db->get_where('mapping_aggregation',array('account2Id' => $account2Id))->result_array();
	if((is_array($AggregationMappingsTemps)) AND (!empty($AggregationMappingsTemps))){
		foreach($AggregationMappingsTemps as $AggregationMappingsTemp){
			$ConsolMappingChannel		= $AggregationMappingsTemp['account1ChannelId'];
			$ConsolMappingCurrency		= strtolower($AggregationMappingsTemp['account1CurrencyId']);
			$ConsolMappingCustomField	= $AggregationMappingsTemp['account1CustomFieldId'];
			$account1APIFieldId			= $AggregationMappingsTemp['account1APIFieldId'];
			
			if(!$this->ci->globalConfig['enableAggregationAdvance'] AND !$this->ci->globalConfig['enableAggregationOnAPIfields']){
				$AggregationMappings[$ConsolMappingChannel][$ConsolMappingCurrency]	= $AggregationMappingsTemp;
			}
			else{
				if($this->ci->globalConfig['enableAggregationAdvance']){
					$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][$ConsolMappingCustomField]	= $AggregationMappingsTemp;
				}
				elseif($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$allAPIFieldsValues		= explode("||", trim($account1APIFieldId));
					foreach($allAPIFieldsValues as $allAPIFieldsValuesTemp){
						$AggregationMappings2[$ConsolMappingChannel][$ConsolMappingCurrency][trim($allAPIFieldsValuesTemp)]	= $AggregationMappingsTemp;						
					}
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappings			= array();
	$channelMappingsTemps		= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	if((is_array($channelMappingsTemps)) AND (!empty($channelMappingsTemps))){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
			}
			if($channelMappingsTemp['account1WarehouseId']){
				$channelMappings[$channelMappingsTemp['account1ChannelId']]['warehouseDetails'][$channelMappingsTemp['account1WarehouseId']]	= $channelMappingsTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	if((is_array($nominalMappingTemps)) AND (!empty($nominalMappingTemps))){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$totalFeeByNominal	= array();
	$journalDatas		= array();
	$this->ci->db->reset_query();
	foreach($journalDatasTemps as $journalDatasTemp){
		$journalParams	= json_decode($journalDatasTemp['params'],true);
		$createdParams	= json_decode($journalDatasTemp['createdParams'],true);
		if($createdParams['lastTry']){
			if($createdParams['lastTry'] > strtotime('-3 hour')){
				continue;
			}
		}
		if(isset($journalDatas[$journalDatasTemp['orderId']])){
			$journalDatas[$journalDatasTemp['orderId']]['amount']	+= $journalDatasTemp['amount'];
		}
		else{
			$journalDatas[$journalDatasTemp['orderId']]['amount']	= $journalDatasTemp['amount'];
		}
		
		$allDebitLines		= $journalParams['debits'];
		$allDebitLines		= (!$allDebitLines['0']) ? (array($allDebitLines)) : ($allDebitLines);
		if((is_array($allDebitLines)) AND (!empty($allDebitLines))){
			foreach($allDebitLines as $debitLine){
				if(isset($totalFeeByNominal[$journalDatasTemp['orderId']][$debitLine['nominalCode']])){
					$totalFeeByNominal[$journalDatasTemp['orderId']][$debitLine['nominalCode']]	+= $debitLine['transactionAmount'];
				}
				else{
					$totalFeeByNominal[$journalDatasTemp['orderId']][$debitLine['nominalCode']]	= $debitLine['transactionAmount'];
				}
			}
		}
	}
	
	if($journalDatas){
		foreach($journalDatas as $bpOrderID => $journalData){
			if((is_array($totalFeeByNominal)) AND (!empty($totalFeeByNominal)) AND (isset($totalFeeByNominal[$bpOrderID]))){
				$salesInfo		= $this->ci->db->get_where('sales_order',array('orderId' => $bpOrderID, 'status >' => 0))->row_array();
				$totalFeeAmount	= 0;
				$totalFeeAmount	= $journalData['amount'];
				if($salesInfo){
					$orderId				= $salesInfo['orderId'];
					$bpconfig				= $this->ci->account1Config[$salesInfo['account1Id']];
					$rowDatas				= json_decode($salesInfo['rowData'],true);
					$channelId				= $rowDatas['assignment']['current']['channelId'];
					$orderCurrencyCode		= strtolower($rowDatas['currency']['orderCurrencyCode']);
					$customFieldValueID		= $rowDatas['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
					$consolAPIFieldValueID	= '';
					$apifieldValueTmps		= '';
					
					if($this->ci->globalConfig['enableAggregationOnAPIfields']){
						$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
						foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
							if(!$apifieldValueTmps){
								$apifieldValueTmps	= @$rowDatas[$account1APIFieldIdsTemp];
							}
							else{
								$apifieldValueTmps	= @$apifieldValueTmps[$account1APIFieldIdsTemp];
							}
						}
					}
					if($apifieldValueTmps){
						$consolAPIFieldValueID	= $apifieldValueTmps;
					}
					if($consolAPIFieldValueID){
						$customFieldValueID	= $consolAPIFieldValueID;
					}
					
					if($salesInfo['isNetOff']){continue;}
					if($salesInfo['sendInAggregation']){continue;}
					
					if($this->ci->globalConfig['enableAggregation']){
						if($AggregationMappings){
							if($AggregationMappings[$channelId]['bpaccountingcurrency']['IsJournalAggregated']){
								continue;
							}
							if($AggregationMappings[$channelId][strtolower($orderCurrencyCode)]['IsJournalAggregated']){
								continue;
							}
						}
						elseif($AggregationMappings2){
							if($AggregationMappings2[$channelId]['bpaccountingcurrency']){
								if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$customFieldValueID]){
									if($AggregationMappings2[$channelId]['bpaccountingcurrency'][$customFieldValueID]['IsJournalAggregated']){
										continue;
									}
								}
								elseif($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']){
									if($AggregationMappings2[$channelId]['bpaccountingcurrency']['NA']['IsJournalAggregated']){
										continue;
									}
								}
							}
							if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]){
								if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$customFieldValueID]){
									if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)][$customFieldValueID]['IsJournalAggregated']){
										continue;
									}
								}
								elseif($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']){
									if($AggregationMappings2[$channelId][strtolower($orderCurrencyCode)]['NA']['IsJournalAggregated']){
										continue;
									}
								}
							}
						}
					}
					
					$trackingDetails	= array();
					if(isset($channelMappings[$channelId])){
						$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
						$trackingDetails	= explode("~=",$trackingDetails);
					}
					
					$xeroInvoiceID	= $salesInfo['createOrderId'];
					if($xeroInvoiceID){
						$editRequest	= array();
						$createdParams	= array();
						$createdRowData	= json_decode($salesInfo['createdRowData'],true);
						$this->headers	= array();
						$suburl			= '2.0/Invoices/'.$xeroInvoiceID;
						$suburl			= ($accountDetails['OAuthVersion'] == '2') ? ('2.0/Invoices?IDs='.$xeroInvoiceID.'&unitdp=4') : ($suburl);
						$this->initializeConfig($account2Id, 'GET', $suburl);
						$xeroOrderInfo	= $this->getCurl($suburl, 'get', '', 'json', $account2Id)[$account2Id];
						if((strtolower($xeroOrderInfo['Status']) == 'ok') AND ($xeroOrderInfo['Invoices']['Invoice'])){
							if(!$xeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']['0']){
								$xeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']	= array($xeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem']);
							}
							
							foreach($totalFeeByNominal[$bpOrderID] as $tempAmzNominal => $totalFeeByNominalLine){
								$tempAmzNominalMapped		= '';
								if(isset($nominalMappings[$tempAmzNominal])){
									if($nominalMappings[$tempAmzNominal]['account2NominalId']){
										$tempAmzNominalMapped	= $nominalMappings[$tempAmzNominal]['account2NominalId'];
									}
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$tempAmzNominal]))){
									if($nominalChannelMappings[strtolower($channelId)][$tempAmzNominal]['account2NominalId']){
										$tempAmzNominalMapped	= $nominalChannelMappings[strtolower($channelId)][$tempAmzNominal]['account2NominalId'];
									}
								}
								
								$xeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'][count($xeroOrderInfo['Invoices']['Invoice']['LineItems']['LineItem'])]	= array(
									'Description'	=> 'Amazon Fees',
									'Quantity' 		=> 1,
									'UnitAmount' 	=> (-1) * sprintf("%.4f",$totalFeeByNominalLine),
									'LineAmount' 	=> (-1) * sprintf("%.4f",$totalFeeByNominalLine),
									'AccountCode'	=> ($tempAmzNominalMapped) ? ($tempAmzNominalMapped) : ($config['amazonfeeAccountRef']),
									'TaxType' 		=> $config['salesNoTaxCode'],
								);
							}
							
							$editRequest							= $xeroOrderInfo['Invoices']['Invoice'];
							$editRequest['Contact']['Addresses']	= $editRequest['Contact']['Addresses']['Address'];
							$editRequest['Contact']['Phones']		= $editRequest['Contact']['Phones']['Phone'];
							$editRequest['LineItems']				= $editRequest['LineItems']['LineItem'];
							foreach($editRequest['LineItems'] as $lineKey => $editRequestLineItems){
								if($editRequestLineItems['Description'] == 'Amazon Fees'){
									if($editRequest['LineItems'][$lineKey]['Tracking']){
										unset($editRequest['LineItems'][$lineKey]['Tracking']);
									}
									if($trackingDetails){
										$editRequest['LineItems'][$lineKey]['Tracking'][0]	= array(
											'Name'		=> $trackingDetails['0'],
											'Option'	=> $trackingDetails['1']
										);
									}
									continue;
								}
								if($editRequestLineItems['Tracking']){
									foreach($editRequestLineItems['Tracking'] as $TrackId => $AllTracking){
										unset($editRequest['LineItems'][$lineKey]['Tracking'][$TrackId]);
										$editRequest['LineItems'][$lineKey]['Tracking'][]	= array(
											'Name'			=> $AllTracking['Name'],
											'Option'		=> $AllTracking['Option'],
										);
										
									}
								}
								unset($editRequest['LineItems'][$lineKey]['LineAmount']);
							}
							unset($editRequest['SubTotal']);
							unset($editRequest['TotalTax']);
							unset($editRequest['Total']);
							unset($editRequest['AmountDue']);
							unset($editRequest['HasAttachments']);
							unset($editRequest['HasErrors']);
							unset($editRequest['SentToContact']);
							unset($editRequest['Contact']['ContactPersons']);
						}
						
						if($editRequest){
							$this->headers	= array();
							$editUrl		= '2.0/Invoices';
							if($accountDetails['OAuthVersion'] == '2'){
								$editUrl	= '2.0/Invoices?unitdp=4';
								$this->initializeConfig($account2Id, 'POST', $editUrl);
							}
							else{
								$editUrl	= '2.0/Invoices';
								$urlParams	= array('unitdp' => '4');
								$this->initializeConfig($account2Id, 'POST', $editUrl,$urlParams);
								$editUrl	= '2.0/Invoices?unitdp='.urlencode('4');
							}
							$editResponse	= $this->getCurl($editUrl, 'POST', json_encode($editRequest), 'json', $account2Id)[$account2Id];
							$createdRowData['Edit Invoice Request Data	: ']	= $editRequest;
							$createdRowData['Edit Invoice Response Data	: ']	= $editResponse;
							$createdParams	= array(
								'Amazon Edit Request	:'	=>	$editRequest,
								'Amazon Edit Response	:'	=>	$editResponse,
							);
							$this->ci->db->where_in('orderId',$bpOrderID)->update('sales_order',array('createdRowData' => json_encode($createdRowData)));
							$this->ci->db->where_in('orderId',$bpOrderID)->update('amazon_ledger',array('createdParams' => json_encode($createdParams)));
							if((strtolower($editResponse['Status']) == 'ok') AND (isset($editResponse['Invoices']['Invoice']['InvoiceID']))){
								$this->ci->db->where_in('orderId',$bpOrderID)->update('sales_order',array('createdRowData' => json_encode($createdRowData), 'status' => '1', 'message' => '', 'IsJournalposted' => 1, 'totalFeeAmount' => $totalFeeAmount));
								
								$this->ci->db->where_in('orderId',$bpOrderID)->update('amazon_ledger',array('status' => '1', 'createdJournalsId' => 'Linked with Invoice', 'message' => '', 'paymentCreated' => 1,'createdParams' => json_encode($createdParams)));
							}
							else{
								$badRequest	= 0;
								$errormsg	= array();
								$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
								$errormsg	= json_decode($errormsg,true);
								if(isset($errormsg['head']['title'])){
									if($errormsg['head']['title'] == '400 Bad Request'){
										$badRequest	= 1;
									}
								}
								if(!$badRequest){
									$createdParams['lastTry']	= strtotime('now');
									$this->ci->db->where_in('orderId',$bpOrderID)->update('amazon_ledger',array('createdParams' => json_encode($createdParams)));
								}
							}
						}
					}
				}
			}
		}
	}
}
?>