<?php
$this->reInitialize();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$this->ci->globalConfig['enablePrepayments']){continue;}
	if(!$clientcode){continue;}
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('orderId',$orgObjectId);
	}
	$advanceDatas		= $this->ci->db->get_where('arAdvance',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(empty($advanceDatas)){continue;}
	
	$this->ci->db->reset_query();
	$genericcustomerMappingsTemps	= $this->ci->db->get_where('mapping_genericcustomer',array('account2Id' => $account2Id))->result_array();
	$genericcustomerMappings			= array();
	$AllCustomFieldsForGenericMapping	= array();
	if($genericcustomerMappingsTemps){
		$GenericCustomerMappingAdvanced		= 0;
		foreach($genericcustomerMappingsTemps as $genericcustomerMappingsTemp){
			$MappedChannel		= '';
			$MappedChannel		= $genericcustomerMappingsTemp['account1ChannelId'];
			
			if($this->ci->globalConfig['enableGenericCustomerMappingCustomazation']){
				$GenericCustomerMappingAdvanced		= 1;
				$brightpearlCustomFieldName			= trim(strtolower($genericcustomerMappingsTemp['brightpearlCustomFieldName']));
				$IncludedStringInCustomField		= explode("||",trim($genericcustomerMappingsTemp['IncludedStringInCustomField']));
				$AllCustomFieldsForGenericMapping[$brightpearlCustomFieldName]	= $brightpearlCustomFieldName; 
				if($IncludedStringInCustomField AND $brightpearlCustomFieldName){
					foreach($IncludedStringInCustomField as $IncludedStringInCustomFieldTemp){
						$IncludedDataString			= trim(strtolower($IncludedStringInCustomFieldTemp));
						$genericcustomerMappings[$MappedChannel][$brightpearlCustomFieldName][$IncludedDataString]	= $genericcustomerMappingsTemp;
					}
				}
				else{
					$genericcustomerMappings[$MappedChannel]['Blank']	= $genericcustomerMappingsTemp;
				}
			}
			else{
				$genericcustomerMappings[$MappedChannel]	= $genericcustomerMappingsTemp;
			}
			
		}
	}
	
	if(!empty($advanceDatas)){
		$allPostContactIds	= array();
		foreach($advanceDatas as $advanceDatass){
			$channelId	= $advanceDatass['channelId'];
			if($channelId){
				if(isset($genericcustomerMappings[$channelId])){continue;}
				$allPostContactIds[]	= $advanceDatass['contactId'];
			}
			else{
				$allPostContactIds[]	= $advanceDatass['contactId'];
			}
		}
		$allPostContactIds = array_filter($allPostContactIds);
		$allPostContactIds = array_unique($allPostContactIds);
		if(!empty($allPostContactIds)){
			$this->postCustomers($allPostContactIds,$account2Id);
		}
	}
	
	$this->ci->db->reset_query();
	$customerMappings		= array();
	$customerMappingsTemp	= $this->ci->db->select('customerId, createdCustomerId, email, company, status')->get_where('customers',array('isSupplier' => 0, 'createdCustomerId <>'=> '', 'account2Id' => $account2Id))->result_array();
	if(!empty($customerMappingsTemp)){
		foreach($customerMappingsTemp as $customerMappingsTemps){
			if(trim($customerMappingsTemps['createdCustomerId'])){
				$customerMappings[$customerMappingsTemps['customerId']]	= $customerMappingsTemps;
			}
		}
	}
	
	if($this->ci->globalConfig['enableChannelMapping']){
		$this->ci->db->reset_query();
		$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
		$channelMappings		= array();
		if($channelMappingsTemps){
			foreach($channelMappingsTemps as $channelMappingsTemp){
				if(!isset($channelMappings[$channelMappingsTemp['account1ChannelId']])){
					$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$taxMappings		= array();
	$taxMappingsTemps	= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id,'orderType <> ' => '2'))->result_array();
	$isStateEnabled		= 0;
	$isChannelEnabled	= 0;
	if(!empty($taxMappingsTemps)){
		foreach($taxMappingsTemps as $taxMappingsTemp){
			if($this->ci->globalConfig['enableAdvanceTaxMapping']){
				if($taxMappingsTemp['stateName']){$isStateEnabled	= 1;}
				if($taxMappingsTemp['countryName']){$isStateEnabled = 1;}
			}
		}
		foreach($taxMappingsTemps as $taxMappingsTemp){
			$stateTemp	= array();
			$stateTemp	= explode(",",trim($taxMappingsTemp['stateName']));
			$stateTemp	= array_filter($stateTemp);
			if(!empty($stateTemp)){
				foreach($stateTemp as $statekey => $stateTemps){
					$stateName			= strtolower(trim($stateTemps));
					$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
					$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
					if($this->ci->globalConfig['enableAdvanceTaxMapping']){
						if($isStateEnabled){
							if($account1ChannelId){
								$isChannelEnabled		= 1;
								$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
								foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
									$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName.'-'.$account1ChannelIdTemp;
									$taxMappings[strtolower($key)]	= $taxMappingsTemp;
								}
							}
							else{
								$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$stateName;
								$taxMappings[strtolower($key)]	= $taxMappingsTemp;
							}
						}
						else{
							$key	= $taxMappingsTemp['account1TaxId'];
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'];
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}
			}
			else{
				$countryName		= strtolower(trim($taxMappingsTemp['countryName']));
				$account1ChannelId	= strtolower(trim($taxMappingsTemp['account1ChannelId']));
				if($isStateEnabled){
					if($account1ChannelId){
						$isChannelEnabled	= 1;
						$account1ChannelIdTemps	= explode(",",trim($account1ChannelId));
						foreach($account1ChannelIdTemps as $account1ChannelIdTemp){
							$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName.'-'.$account1ChannelIdTemp;
							$taxMappings[strtolower($key)]	= $taxMappingsTemp;
						}
					}
					else{
						$key	= $taxMappingsTemp['account1TaxId'].'-'.$countryName;
						$taxMappings[strtolower($key)]	= $taxMappingsTemp;
					}
				}			
			}
			if(!$isStateEnabled){
				$key							= $taxMappingsTemp['account1TaxId'];
				$taxMappings[strtolower($key)]	= $taxMappingsTemp;
			}
		}
	}
	
	
	$this->ci->db->reset_query();
	$paymentMappingsTemps	= $this->ci->db->get_where('mapping_payment',array('account2Id' => $account2Id,'applicableOn' => 'sales'))->result_array();
	$paymentMappings		= array();$paymentMappings1		= array();$paymentMappings2		= array();$paymentMappings3		= array();
	if(!empty($paymentMappingsTemps)){
		foreach($paymentMappingsTemps as $paymentMappingsTemp){
			$account1PaymentId	= strtolower(trim($paymentMappingsTemp['account1PaymentId']));
			$paymentchannelIds	= explode(",",trim($paymentMappingsTemp['channelIds']));
			$paymentchannelIds	= array_filter($paymentchannelIds);
			$paymentcurrencys	= explode(",",trim($paymentMappingsTemp['currency']));
			$paymentcurrencys	= array_filter($paymentcurrencys);
			if((!empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentchannelIds as $paymentchannelId){
					foreach($paymentcurrencys as $paymentcurrency){
						$paymentMappings1[$paymentchannelId][strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
					}
				}
			}else if((!empty($paymentchannelIds)) && (!$paymentcurrencys)){
				foreach($paymentchannelIds as $paymentchannelId){
					$paymentMappings2[$paymentchannelId][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (!empty($paymentcurrencys))){
				foreach($paymentcurrencys as $paymentcurrency){
					$paymentMappings3[strtolower($paymentcurrency)][$account1PaymentId]	= $paymentMappingsTemp;
				}
			}else if((empty($paymentchannelIds)) && (empty($paymentcurrencys))){
				$paymentMappings[$account1PaymentId]	= $paymentMappingsTemp;
			}
		}
	}
	
	$noNeedToSentIds	= array();
	$positiveAdvances	= array();
	$negativeAdvances	= array();
	foreach($advanceDatas as $advanceDatasTemp){
		if(($advanceDatasTemp['paymentType'] == 'RECEIPT') OR ($advanceDatasTemp['paymentType'] == 'CAPTURE')){
			$positiveAdvances[$advanceDatasTemp['orderId']][$advanceDatasTemp['paymentMethodCode']][$advanceDatasTemp['paymentId']]	= $advanceDatasTemp;
		}
		if($advanceDatasTemp['paymentType'] == 'PAYMENT'){
			$negativeAdvances[$advanceDatasTemp['orderId']][$advanceDatasTemp['paymentMethodCode']][$advanceDatasTemp['paymentId']]	= $advanceDatasTemp;
		}
	}
	
	if(!empty($negativeAdvances)){
		foreach($negativeAdvances as $tempOrderId => $negativeAdvancesTemps){
			foreach($negativeAdvancesTemps as $tempPaymentMethod => $negativeAdvancesTempsTemp){
				foreach($negativeAdvancesTempsTemp as $tempNegativeId => $negativeAdvancesTempsTempTemp){
					if((!empty($positiveAdvances)) AND (isset($positiveAdvances[$tempOrderId][$tempPaymentMethod]))){
						foreach($positiveAdvances[$tempOrderId][$tempPaymentMethod] as $tempPositiveId => $positivePaymentTemp){
							if(($positivePaymentTemp['status'] == 0) AND ($negativeAdvancesTempsTempTemp['amountPaid'] == $positivePaymentTemp['amountPaid'])){
								$noNeedToSentIds[]	= $tempPositiveId;
								$noNeedToSentIds[]	= $tempNegativeId;
								$negativeAdvances[$tempOrderId][$tempPaymentMethod][$tempNegativeId]['status']	= 4;
								$positiveAdvances[$tempOrderId][$tempPaymentMethod][$tempPositiveId]['status']	= 4;
								break;
							}
						}
					}
				}
			}
		}
	}
	
	if(!empty($noNeedToSentIds)){
		$noNeedToSentIds	= array_filter($noNeedToSentIds);
		$noNeedToSentIds	= array_unique($noNeedToSentIds);
		$this->ci->db->where_in('paymentId',$noNeedToSentIds)->update('arAdvance',array('status' => '4', 'message' => 'Archived due to Net-Off'));
	}
	
	foreach($advanceDatas as $advanceData){
		if(!empty($noNeedToSentIds)){if(in_array($advanceData['paymentId'], $noNeedToSentIds)){continue;}}
		if($advanceData['status'] == 4){continue;}
		if($advanceData['orderTotal'] == 0){
			$this->ci->db->update('arAdvance',array('message' => 'Order total is 0'),array('paymentId' => $advanceData['paymentId']));
			continue;
		}
		if(strtolower($advanceData['paymentType']) == 'auth'){continue;}
		if(strtolower($advanceData['paymentType']) == 'void'){continue;}
		$lines			= array();
		$linesSeq		= 0;
		$linesBatch		= array();
		$errors			= array();
		$createdRowData	= json_decode($advanceData['createdRowData'], true);
		$contactId		= (isset($customerMappings[$advanceData['contactId']])) ? ($customerMappings[$advanceData['contactId']]['createdCustomerId']) : '';
		$contactId		= (isset($genericcustomerMappings[$advanceData['contactId']])) ? ($genericcustomerMappings[$advanceData['contactId']]['account2ChannelId']) : ($contactId);
		$txnType		= (strtolower($advanceData['paymentType']) == 'receipt') ? ('RECEIVE-PREPAYMENT') : ('SPEND-PREPAYMENT');
		$txnType		= (strtolower($advanceData['paymentType']) == 'capture') ? ('RECEIVE-PREPAYMENT') : ($txnType);
		$channelId		= $advanceData['channelId'];
		$bankAcc		= '';
		$Method			= $advanceData['paymentMethodCode'];
		if(isset($paymentMappings1[$channelId][strtolower($advanceData['currencyCode'])][strtolower($Method)])){
			$bankAcc	= $paymentMappings1[$channelId][strtolower($advanceData['currencyCode'])][strtolower($Method)]['account2PaymentId'];
		}
		
		else if(isset($paymentMappings2[$channelId][strtolower($Method)])){
			$bankAcc	= $paymentMappings2[$channelId][strtolower($Method)]['account2PaymentId'];
		}
		
		else if(isset($paymentMappings3[strtolower($advanceData['currencyCode'])][strtolower($Method)])){
			$bankAcc	= $paymentMappings3[strtolower($advanceData['currencyCode'])][strtolower($Method)]['account2PaymentId'];
		}
		
		else if(isset($paymentMappings[strtolower($Method)])){
			$bankAcc		= $paymentMappings[strtolower($Method)]['account2PaymentId'];
		}
		$lineAcc		= ($config['prePaymentNominal']) ? ($config['prePaymentNominal']) : '';
		if((is_array($createdRowData)) AND (!empty($createdRowData)) AND (isset($createdRowData['lastTry']))){
			if($createdRowData['lastTry'] > strtotime('-3 hour')){
				continue;
			}
		}
		
		if(!$contactId){$errors[]	= 'Missing Contact';}
		if(!$bankAcc){$errors[]	= 'Missing Bank Account';}
		if(!$lineAcc){$errors[]	= 'Missing Line Account';}
		if(!empty($errors)){
			$this->ci->db->update('arAdvance',array('message' => implode(",",$errors)),array('paymentId' => $advanceData['paymentId']));
			continue;
		}
		
		$bpconfig			= $this->ci->account1Config[$advanceData['account1Id']];
		$addresses			= json_decode($advanceData['addresses'], true);
		$countryIsoCode3	= strtolower(trim($addresses['delivery']['countryIsoCode3']));
		$addressLine4		= strtolower(trim($addresses['delivery']['addressLine4']));
		$orderLines			= json_decode($advanceData['orderLineDetails'], true);
		
		$tempReference		= '';
		$tempReference		= 'SO#'.$advanceData['orderId'];
		/* if($advanceData['customerRef']){
			$tempReference	= $tempReference.' | '.$advanceData['customerRef'];
		} */
		if($advanceData['description']){
			$tempReference	= $tempReference.' | '.$advanceData['description'];
		}
		
		if((is_array($orderLines)) AND (!empty($orderLines))){
			foreach($orderLines as $orderLine){
				if(isset($linesBatch[$orderLine['rowValue']['taxClassId']])){
					$linesBatch[$orderLine['rowValue']['taxClassId']]['taxable']	+= $orderLine['rowValue']['rowNet']['value'];
					$linesBatch[$orderLine['rowValue']['taxClassId']]['tax']		+= $orderLine['rowValue']['rowTax']['value'];
				}
				else{
					$linesBatch[$orderLine['rowValue']['taxClassId']]['taxable']	= $orderLine['rowValue']['rowNet']['value'];
					$linesBatch[$orderLine['rowValue']['taxClassId']]['tax']		= $orderLine['rowValue']['rowTax']['value'];
				}
			}
		}
		if(!empty($linesBatch)){
			foreach($linesBatch as $taxCode => $linesData){
				$unitAmount	= 0;
				$unitAmount	= ((($linesData['taxable'] + $linesData['tax']) / ($advanceData['orderTotal'])) * ($advanceData['amountPaid']));
				
				$taxMap1	= strtolower($taxCode);
				$taxMap2	= strtolower($taxCode);
				if($isStateEnabled){
					if($isChannelEnabled){
						$taxMap1	= strtolower($taxCode.'-'.$countryIsoCode3.'-'.$addressLine4.'-'.$channelId);
						$taxMap2	= strtolower($taxCode.'-'.$countryIsoCode3.'-'.$channelId);
					}
					else{
						$taxMap1	= strtolower($taxCode.'-'.$countryIsoCode3.'-'.$addressLine4);
						$taxMap2	= strtolower($taxCode.'-'.$countryIsoCode3);
					}
				}
				
				$taxMapping	= array();
				if(isset($taxMappings[$taxMap1])){
					$taxMapping	= $taxMappings[$taxMap1];
				}
				elseif(isset($taxMappings[$taxMap2])){
					$taxMapping	= $taxMappings[$taxMap2];
				}
				elseif(isset($taxMappings[$taxCode.'--'.$channelId])){
					$taxMapping	= $taxMappings[$taxCode.'--'.$channelId];
				}
				$taxType	= (isset($taxMapping['account2TaxId'])) ? ($taxMapping['account2TaxId']) : '';
				if(!$taxType){
					$errors[]	= 'Tax Mapping missing :'.$taxCode;
					continue;
				}
				$lines[$linesSeq]	= array(
					'Description'		=> $tempReference,
					'Quantity'			=> 1,
					'UnitAmount'		=> sprintf("%.4f",$unitAmount),
					'AccountCode'		=> $lineAcc,
					'TaxType'			=> $taxType,
				
				);
				
				if(isset($channelMappings[$channelId])){
					$trackingDetails	= $channelMappings[$channelId]['account2ChannelId'];
					$trackingDetails	= explode("~=",$trackingDetails);
					
					$lines[$linesSeq]['Tracking'][]	= array(
						'Name'		=> $trackingDetails['0'],
						'Option'	=> $trackingDetails['1']
					);
				}
				
				$linesSeq++;
			}
			if(!empty($errors)){
				$this->ci->db->update('arAdvance',array('message' => implode(",",$errors)),array('paymentId' => $advanceData['paymentId']));
				continue;
			}
		}
		if(!empty($lines)){
			
			$finalLineItemArray	= array();
			$finalLineItemSeq	= 0;
			foreach($lines as $linesSeqTemp => $linesTemp){
				if($linesTemp['UnitAmount'] == 0){
					unset($lines[$linesSeqTemp]);
				}
			};
			if($lines){
				foreach($lines as $linesTemp){
					$finalLineItemArray[$finalLineItemSeq]	= $linesTemp;
					$finalLineItemSeq++;
				};
			}
			
			if($finalLineItemArray){
				$request	= array(
					'Type'				=> $txnType,
					'Contact'			=> array('ContactID' => $contactId),
					'LineItems'			=> $finalLineItemArray,
					'BankAccount'		=> array('Code' => $bankAcc),
					'Date'				=> date('Y-m-d', strtotime($advanceData['paymentDate'])),
					'Reference'			=> $tempReference,
					'CurrencyCode'		=> $advanceData['currencyCode'],
					'CurrencyRate'		=> $advanceData['exchangeRate'],
					'Status'			=> 'AUTHORISED',
					'LineAmountTypes'	=> 'Inclusive',
				);
				if((strtolower($config['defaultCurrency'])) != (strtolower($bpconfig['currencyCode']))){
					unset($request['CurrencyRate']);
				}
				$this->headers	= array();
				$url			= '2.0/BankTransactions';
				if($accountDetails['OAuthVersion'] == '2'){
					$url		= '2.0/BankTransactions?unitdp=4';
					$this->initializeConfig($account2Id, 'PUT', $url);
				}
				else{
					$url		= '2.0/BankTransactions';
					$UrlParams	= array('unitdp' => '4');
					$this->initializeConfig($account2Id, 'PUT', $url,$UrlParams);
					$url		= '2.0/BankTransactions?unitdp='.urlencode('4');
				}
				$results		= $this->getCurl($url, 'PUT', json_encode($request), 'json', $account2Id)[$account2Id];
				$createdRowData['prePayment Request']	= $request;
				$createdRowData['prePayment Response']	= $results;
				if((strtolower($results['Status']) == 'ok') AND (isset($results['BankTransactions']['BankTransaction']['BankTransactionID']))){
					$updateArray	= array(
						'createdRowData'	=> json_encode($createdRowData),
						'status'			=> 1,
						'bankTxnId'			=> $results['BankTransactions']['BankTransaction']['BankTransactionID'],
						'prePaymentId'		=> $results['BankTransactions']['BankTransaction']['PrepaymentID'],
						'message'			=> '',
						'paymentId'			=> $advanceData['paymentId'],
					);
					$this->ci->db->where(array('paymentId' => $advanceData['paymentId']))->update('arAdvance',$updateArray);
				}
				else{
					$badRequest	= 0;
					$errormsg	= array();
					$errormsg	= json_encode(simplexml_load_string($this->response[$account2Id]));
					$errormsg	= json_decode($errormsg,true);
					if(isset($errormsg['head']['title'])){
						if($errormsg['head']['title'] == '400 Bad Request'){
							$badRequest	= 1;
						}
					}
					if(!$badRequest){
						$createdRowData['lastTry']	= strtotime('now');
						$this->ci->db->update('arAdvance',array('createdRowData' => json_encode($createdRowData)),array('paymentId' => $advanceData['paymentId']));
					}
				}
			}
		}
	}
}