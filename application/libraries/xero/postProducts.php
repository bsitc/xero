<?php
//post brightpearl products on xero
$this->reInitialize();
$clientcode	= $this->ci->config->item('clientcode');
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	
	//doNotRemoveThisLine
	$skuTrimmingClientCodes	= array('keenxero','bfxero','arten','logicofenglishxero','tinyexplorerxero','homeleisuredirectxero');
	$config	= $this->accountConfig[$account2Id];
	
	if(($postedAccount2Id) AND ($account2Id != $postedAccount2Id)){
		continue;
	}
	
	$this->ci->db->reset_query();
	if($orgObjectId){
		$this->ci->db->where_in('productId',$orgObjectId);
	}
	$datas	= $this->ci->db->where_in('status',array('0','2'))->get_where('products',array('account2Id' => $account2Id))->result_array();
	if(empty($datas)){continue;}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	
	
	$this->ci->db->reset_query();
	$taxMappingTemps		= $this->ci->db->get_where('mapping_tax',array('account2Id' => $account2Id))->result_array();
	$taxMapppings			= array();
	if($taxMappingTemps){
		foreach($taxMappingTemps as $taxMappingTemp){
			$orderType	= (int)$taxMappingTemp['orderType'];
			$taxMapppings[$taxMappingTemp['account1TaxId']][$orderType]	= $taxMappingTemp;
		}
	}
	
	$batchDatas	= array();
	foreach($datas as $datasTemp){
		$batchDatas[$datasTemp['isError']][]	= $datasTemp;
	}
	
	if($batchDatas){
		foreach($batchDatas as $isError => $datas){
			if(!empty($datas)){
				$batchData	= array();
				if($isError){
					$batchData	= array_chunk($datas,1);
				}
				else{
					$batchData	= array_chunk($datas,5);
				}
				foreach($batchData as $datas){
					$request			= array();
					$requestLogs		= array();
					$allSKUandID		= array();
					$requestCount		= 0;
					$productPriceLists	= $this->ci->{$this->ci->globalConfig['account1Liberary']}->getProductPriceList(array_column($datas, 'productId'));
					foreach($datas as $productDatas){
						if(($productDatas['status'] == 4) OR ($productDatas['status'] == 3)){continue;}
						if(!$productDatas['sku']){continue;}
						
						$productDatas['sku']		= trim($productDatas['sku']);
						
						/* if(in_array($clientcode, $skuTrimmingClientCodes)){ */
						if($config['InventoryManagementEnabled'] == true){
							$productDatas['sku']	= substr($productDatas['sku'],0,30);
							$allSKUandID[$productDatas['sku']]	= $productDatas['productId'];
						}
						
						$productDatas['description']	= trim(strip_tags($productDatas['description']));
						$productDatas['description']	= htmlspecialchars_decode($productDatas['description'],ENT_QUOTES );
						$productDatas['name']			= trim(strip_tags($productDatas['name']));
						$productDatas['name']			= htmlspecialchars_decode($productDatas['name'],ENT_QUOTES );
						
						
						$config1			= $this->ci->account1Config[$productDatas['account1Id']];
						$productId			= $productDatas['productId'];
						$description		= trim(strip_tags($productDatas['description'])) ? trim(strip_tags($productDatas['description'])) : $productDatas['name'];
						$productPriceList	= $productPriceLists[$productId];
						$SalesPrice			= 0.00;
						$PurchaseCost		= 0.00;
						if($productPriceList[$config['productRetailPriceList']] > 0){
							$SalesPrice		= $productPriceList[$config['productRetailPriceList']]; 
						}
						if($productPriceList[$config['productCostPriceList']] > 0){
							$PurchaseCost	= $productPriceList[$config['productCostPriceList']];
						}
						
						$params				= json_decode($productDatas['params'],true);
						$COGSAccountRef		= $config['COGSAccountRef'];
						$AssetAccountRef	= $config['AssetAccountRef'];
						$IncomeAccountRef	= $config['IncomeAccountRef'];
						$ExpenseAccountRef	= $config['ExpenseAccountRef'];
						
						if(isset($nominalMappings[$params['nominalCodeSales']]['account2NominalId'])){
							$IncomeAccountRef	= $nominalMappings[$params['nominalCodeSales']]['account2NominalId'];
						}
						if(isset($nominalMappings[$params['nominalCodePurchases']]['account2NominalId'])){
							$ExpenseAccountRef	= $nominalMappings[$params['nominalCodePurchases']]['account2NominalId'];
							$COGSAccountRef		= $nominalMappings[$params['nominalCodePurchases']]['account2NominalId'];
						}
						if(isset($nominalMappings[$params['nominalCodeStock']]['account2NominalId'])){
							$AssetAccountRef	= $nominalMappings[$params['nominalCodeStock']]['account2NominalId'];
						}
						
						$request['items'][$requestCount]	= array(
							'Code'					=> $productDatas['sku'],
							'Name'					=> substr(utf8_encode(substr($productDatas['name'],0,50)), 0, 50),
							'Description'			=> substr($description,0,4000),
							'PurchaseDescription'	=> substr($description,0,4000),
							'IsSold'				=> true,
							'IsPurchased'			=> true,
							'PurchaseDetails'		=> array(
								'UnitPrice'				=> $PurchaseCost,
								'COGSAccountCode'		=> $COGSAccountRef, 
								'AccountCode'			=> $ExpenseAccountRef,
							),
							'SalesDetails'			=> array(
								'UnitPrice'				=> $SalesPrice,
								'AccountCode'			=> $IncomeAccountRef,
							),
							'InventoryAssetAccountCode'	=> $AssetAccountRef
						);
						if($productDatas['createdProductId']){
							$request['items'][$requestCount]['ItemID']	= $productDatas['createdProductId'];
						}
						
						$itmeQueryResults		= array();
						if(($productDatas['ceatedParams']) AND (!$productDatas['createdProductId'])){
							$this->headers		= array();
							$itmeQueryURL		= '2.0/Items/'.rawurlencode($productDatas['sku']);					
							$this->initializeConfig($account2Id, 'POST', $itmeQueryURL);
							$itmeQueryResults	= $this->getCurl($itmeQueryURL, 'get', '', 'json', $account2Id)[$account2Id]['Items']['Item'];
						}
						
						$bpTaxId			= $params['financialDetails']['taxCode']['id'];
						if(isset($taxMapppings[$bpTaxId])){
							$taxMappping	= $taxMapppings[$bpTaxId];
							if(!isset($taxMappping['1'])){
								$taxMappping['1']	= $taxMappping['0'];
							}
							if(!isset($taxMappping['2'])){
								$taxMappping['2']	= $taxMappping['0'];
							}
							$request['items'][$requestCount]['PurchaseDetails']['TaxType']	= $taxMappping['2']['account2TaxId'];
							$request['items'][$requestCount]['SalesDetails']['TaxType']		= $taxMappping['1']['account2TaxId'];
						}
						else{
							if($params['financialDetails']['taxable']){
								$request['items'][$requestCount]['SalesDetails']['TaxType']	= $config['TaxCode'];
							}
						}
						if($params['stock']['stockTracked']){
							unset($request['items'][$requestCount]['PurchaseDetails']['AccountCode']);
							$request['items'][$requestCount]['IsTrackedAsInventory']	= true;
						}
						else{
							unset($request['items'][$requestCount]['PurchaseDetails']['COGSAccountCode']);
							unset($request['items'][$requestCount]['InventoryAssetAccountCode']); 
							$request['items'][$requestCount]['IsTrackedAsInventory']	= false;
						}
						
						//if $config['InventoryManagementEnabled'] == true that means connector is non-inventory managed
						if($config['InventoryManagementEnabled'] == true){
							$InventMgt_SellNominalCode		= $config['InventoryManagementProductSellNominalCode'];
							$InventMgt_PurchaseNominalCode	= $config['InventoryManagementProductPurchaseNominalCode'];
							
							if(isset($nominalMappings[$params['nominalCodeSales']])){
								if($nominalMappings[$params['nominalCodeSales']]['account2NominalId']){
									$InventMgt_SellNominalCode		= $nominalMappings[$params['nominalCodeSales']]['account2NominalId'];
								}
							}
							if(isset($nominalMappings[$params['nominalCodePurchases']])){
								if($nominalMappings[$params['nominalCodePurchases']]['account2NominalId']){
									$InventMgt_PurchaseNominalCode	= $nominalMappings[$params['nominalCodePurchases']]['account2NominalId'];
								}
							}
							
							if($params['stock']['stockTracked']){
								unset($request['items'][$requestCount]['PurchaseDetails']['COGSAccountCode']);
								unset($request['items'][$requestCount]['InventoryAssetAccountCode']); 
								$request['items'][$requestCount]['IsTrackedAsInventory']				= false;
								$request['items'][$requestCount]['SalesDetails']['AccountCode']			= $InventMgt_SellNominalCode;
								$request['items'][$requestCount]['PurchaseDetails']['AccountCode']		= $InventMgt_PurchaseNominalCode;
							}
							if($this->ci->globalConfig['enableCOGSJournals']){
								if($params['stock']['stockTracked']){
									$request['items'][$requestCount]['SalesDetails']['AccountCode']		= $IncomeAccountRef;
									$request['items'][$requestCount]['PurchaseDetails']['AccountCode']	= $AssetAccountRef;
								}
								else{
									$request['items'][$requestCount]['SalesDetails']['AccountCode']		= $IncomeAccountRef;
									$request['items'][$requestCount]['PurchaseDetails']['AccountCode']	= $ExpenseAccountRef;
								}
							}
						}
						if($itmeQueryResults['ItemID']){
							$request['items'][$requestCount]['ItemID'] = $itmeQueryResults['ItemID'];
							$this->ci->db->update('products',array('createdProductId' => $itmeQueryResults['ItemID']),array('id' => $productDatas['id']));
						}
						$requestLogs[strtolower($productDatas['sku'])]['Request Data']				= $request['items'][$requestCount];
						$requestLogs[strtolower($productDatas['sku'])]['Request Data']['productId']	= $productDatas['productId'];
						$requestCount++;
					}
					if($request){
						$this->headers	= array();
						$url			= '2.0/Items';
						if($accountDetails['OAuthVersion'] == '2'){
							$url		= '2.0/Items?unitdp=4';
							$this->initializeConfig($account2Id, 'POST', $url);
						}
						else{
							$url		= '2.0/Items';
							$UrlParams	= array('unitdp' => '4');
							$this->initializeConfig($account2Id, 'POST', $url,$UrlParams);			
							$url		= '2.0/Items?unitdp='.urlencode('4');
						}
						$results	= $this->getCurl($url, 'POST', json_encode($request), 'json', $account2Id)[$account2Id];
						$ceatedParams	= array(
							'Request data	: ' => $request,
							'Response data	: ' => $results,
						);
						$productIds	= array_column($datas,'productId');
						if($productIds){
							$this->ci->db->where_in('productId',$productIds)->update('products',array('ceatedParams' => json_encode($ceatedParams)),array('account2Id' => $account2Id));
							if((strtolower($results['Status']) == 'ok') AND (isset($results['Items']['Item']))){
								$xeroItems	= $results['Items']['Item'];
								if(!isset($xeroItems['0'])){
									$xeroItems	= array($xeroItems);
								}
								foreach($xeroItems as $xeroItem){
									
									/* if(in_array($clientcode, $skuTrimmingClientCodes)){ */
									if($config['InventoryManagementEnabled'] == true){
										$updateProId	= $allSKUandID[$xeroItem['Code']];
										$this->ci->db->update('products',array('status' => '1','createdProductId' => $xeroItem['ItemID'],'isError' => 0),array('productId' => $updateProId,'account2Id' => $account2Id));
									}
									else{
										$this->ci->db->update('products',array('status' => '1','createdProductId' => $xeroItem['ItemID'],'isError' => 0),array('sku' => $xeroItem['Code'],'account2Id' => $account2Id));
									}
									$requestLogs[strtolower($xeroItem['Code'])]['Response Data']	= $xeroItem;
									
								}
							}
							else{
								$this->ci->db->where_in('productId',$productIds)->update('products',array('isError' => 1),array('account2Id' => $account2Id));
							}
						}
					}
					if($requestLogs){
						foreach($requestLogs as $requestLog){ 
							$productId	= $requestLog['Request Data']['productId'];
							$path		= FCPATH.'logs'. DIRECTORY_SEPARATOR .'account2'. DIRECTORY_SEPARATOR . $account2Id. DIRECTORY_SEPARATOR .'products'. DIRECTORY_SEPARATOR;
							if(!is_dir($path)){
								mkdir($path,0777,true);
								chmod(dirname($path), 0777);
							}
							file_put_contents($path. $productId.'.logs',"\n\n Xero Log Added On : ".date('c')." \n". json_encode($requestLog),FILE_APPEND);
						}
					}
				}
			}
		}
	}
}
?>