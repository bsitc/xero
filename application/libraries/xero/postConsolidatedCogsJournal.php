<?php
$this->reInitialize();
$getAllCurrencyWithRate	= $this->ci->brightpearl->getAllCurrencyWithRate();
$clientcode				= $this->ci->config->item('clientcode');
$dateLockSettings		= (strlen(trim($this->ci->globalConfig['dateLockSettings'])) > 0) ? (json_decode(trim($this->ci->globalConfig['dateLockSettings']), true)) : array();
foreach($this->accountDetails as $account2Id => $accountDetails){
	if(!$clientcode){continue;}
	if(!$this->ci->globalConfig['enableCOGSJournals']){continue;}
	//cogs for Keen DIST LTD. is stopped, req by Neha on 9th Fab 2023
	if($clientcode == 'keenxero'){if($account2Id == 5){continue;}}
	
	
	$config				= $this->accountConfig[$account2Id];
	$XeroBaseCurrency	= strtolower($config['defaultCurrency']);
	
	$this->ci->db->reset_query();
	$datas	= $this->ci->db->get_where('cogs_journal',array('account2Id' => $account2Id , 'status' => 0))->result_array();
	if(!$datas){continue;}
	$allCogsOrderIds	= array_column($datas,'orderId');
	$allCogsOrderIds	= array_filter($allCogsOrderIds);
	$allCogsOrderIds	= array_unique($allCogsOrderIds);
	
	$this->ci->db->reset_query();
	if($allCogsOrderIds){
		$this->ci->db->where_in('orderId',$allCogsOrderIds);
	}
	$allSaveSalesCreditRef		= array();
	$allSaveSalesCredits		= array();
	$allSaveSalesCreditsDatas	= $this->ci->db->select('account1Id,orderId,createOrderId,rowData,sendInAggregation,aggregationId,invoiceRef,isNetOff')->get_where('sales_credit_order',array('COGSPosted' => 0, 'status <>' => '0' ,'sendInAggregation' => 1))->result_array();
	if($allSaveSalesCreditsDatas){
		
		foreach($allSaveSalesCreditsDatas as $allSaveSalesCreditsData){
			if(!$allSaveSalesCreditsData['sendInAggregation']){
				continue;
			}
			if($allSaveSalesCreditsData['createOrderId']){
				$bpconfig				= $this->ci->account1Config[$allSaveSalesCreditsData['account1Id']];
				$rowData				= json_decode($allSaveSalesCreditsData['rowData'],true);
				$ConsolAPIFieldValueID	= '';
				
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowData[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				
				$CustomFieldValueIDTemps		= '';
				if((isset($rowData['customFields'])) AND (isset($rowData['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowData['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
					$CustomFieldValueIDTemps	= $rowData['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				}
				
				$allSaveSalesCredits[$allSaveSalesCreditsData['createOrderId']][]						= $allSaveSalesCreditsData['orderId'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['isNetOff']			= $allSaveSalesCreditsData['isNetOff'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['invoiceRef']			= $allSaveSalesCreditsData['invoiceRef'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['taxDate']			= $rowData['invoices'][0]['taxDate'];
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['CustomFieldValueID']	= $CustomFieldValueIDTemps;
				$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']][$allSaveSalesCreditsData['orderId']]['shippingStatus'] = $rowData['stockStatusCode'];
			
				if($ConsolAPIFieldValueID){
					$allSaveSalesCreditRef[$allSaveSalesCreditsData['createOrderId']]['CustomFieldValueID']	= $ConsolAPIFieldValueID;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	if($allCogsOrderIds){
		$this->ci->db->where_in('orderId',$allCogsOrderIds);
	}
	$allSaveSalesRef			= array();
	$allSaveSalesOrders			= array();
	$allSaveSalesOrdersDatas	= $this->ci->db->select('account1Id,orderId,createOrderId,rowData,sendInAggregation,aggregationId,invoiceRef,isNetOff')->get_where('sales_order',array('COGSPosted' => 0, 'status <>' => '0' ,'sendInAggregation' => 1))->result_array();
	if($allSaveSalesOrdersDatas){
		foreach($allSaveSalesOrdersDatas as $allSaveSalesOrdersData){
			if(!$allSaveSalesOrdersData['sendInAggregation']){
				continue;
			}
			if($allSaveSalesOrdersData['createOrderId']){
				$bpconfig				= $this->ci->account1Config[$allSaveSalesOrdersData['account1Id']];
				$rowData				= json_decode($allSaveSalesOrdersData['rowData'],true);
				$ConsolAPIFieldValueID	= '';
				
				if($this->ci->globalConfig['enableAggregationOnAPIfields']){
					$account1APIFieldIds	= explode(".",$bpconfig['apiFieldForConsol']);
					$APIfieldValueTmps		= '';
					foreach($account1APIFieldIds as $account1APIFieldIdsTemp){
						if(!$APIfieldValueTmps){
							$APIfieldValueTmps	= @$rowData[$account1APIFieldIdsTemp];
						}
						else{
							$APIfieldValueTmps	= @$APIfieldValueTmps[$account1APIFieldIdsTemp];
						}
					}
					if($APIfieldValueTmps){
						$ConsolAPIFieldValueID	= $APIfieldValueTmps;
					}
				}
				
				$CustomFieldValueIDTemps		= '';
				if((isset($rowData['customFields'])) AND (isset($rowData['customFields'][$bpconfig['CustomFieldMappingFieldName']])) AND (isset($rowData['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id']))){
					$CustomFieldValueIDTemps	= $rowData['customFields'][$bpconfig['CustomFieldMappingFieldName']]['id'];
				}
				
				$allSaveSalesOrders[$allSaveSalesOrdersData['createOrderId']][]						= $allSaveSalesOrdersData['orderId'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['isNetOff']				= $allSaveSalesOrdersData['isNetOff'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['invoiceRef']			= $allSaveSalesOrdersData['invoiceRef'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['taxDate']				= $rowData['invoices'][0]['taxDate'];
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['CustomFieldValueID']	= $CustomFieldValueIDTemps;
				$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']][$allSaveSalesOrdersData['orderId']]['shippingStatus'] = $rowData['shippingStatusCode'];
				
				if($ConsolAPIFieldValueID){
					$allSaveSalesRef[$allSaveSalesOrdersData['createOrderId']]['CustomFieldValueID']	= $ConsolAPIFieldValueID;
				}
			}
		}
	}
	
	$this->ci->db->reset_query();
	$nominalMappingTemps	= $this->ci->db->get_where('mapping_nominal',array('account1CustomFieldValue' => '','account2Id' => $account2Id))->result_array();
	$nominalMappings		= array();
	$nominalChannelMappings	= array();
	if(!empty($nominalMappingTemps)){
		foreach($nominalMappingTemps as $nominalMappingTemp){
			if((isset($nominalMappingTemp['account1ChannelId'])) AND (strlen(trim($nominalMappingTemp['account1ChannelId'])) > 0)){
				$nominalChannelMappings[strtolower($nominalMappingTemp['account1ChannelId'])][$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
			else{
				$nominalMappings[$nominalMappingTemp['account1NominalId']]	= $nominalMappingTemp;
			}
		}
	}
	
	$this->ci->db->reset_query();
	$channelMappingsTemps	= $this->ci->db->get_where('mapping_channel',array('account2Id' => $account2Id))->result_array();
	$channelMappings		= array();
	if($channelMappingsTemps){
		foreach($channelMappingsTemps as $channelMappingsTemp){
			$channelMappings[$channelMappingsTemp['account1ChannelId']]	= $channelMappingsTemp;
		}
	}

	$this->ci->db->reset_query();
	$ChannelcustomfieldMappingKey		= '';
	$ChannelcustomfieldMappings			= array();
	$ChannelcustomfieldMappingsTemps	= $this->ci->db->order_by('id','desc')->get_where('mapping_channelcustomfield',array('account2Id' => $account2Id))->result_array();
	if($ChannelcustomfieldMappingsTemps){
		foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
			if($ChannelcustomfieldMappingsTemp['account1CustomFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1CustomFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1APIFieldId']){
				$ChannelcustomfieldMappingKey	= 'account1APIFieldId';
				break;
			}
			elseif($ChannelcustomfieldMappingsTemp['account1WarehouseId']){
				$ChannelcustomfieldMappingKey	= 'account1WarehouseId';
				break;
			}
			else{
				break;
			}
		}
		if($ChannelcustomfieldMappingKey){
			foreach($ChannelcustomfieldMappingsTemps as $ChannelcustomfieldMappingsTemp){
				if($ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey]){
					$ChannelcustomfieldChannel		= $ChannelcustomfieldMappingsTemp['account1ChannelId'];
					$ChannelcustomfieldKey2Value	= $ChannelcustomfieldMappingsTemp[$ChannelcustomfieldMappingKey];
					$ChannelcustomfieldMappings[$ChannelcustomfieldChannel][$ChannelcustomfieldKey2Value]	= $ChannelcustomfieldMappingsTemp;
				}
			}
		}
	}
		
	$postDatasTmpSO	= array();
	$postDatasTmpSC	= array();
	$jounralDataSO	= array();
	$jounralDataSC	= array();
	if($datas){
		foreach($datas as $orderDatas){
			if($orderDatas['status'] == 4){continue;}
			if($orderDatas['createdJournalsId']){continue;}
			if($orderDatas['journalTypeCode'] == 'GO'){
				$jounralDataSO[$orderDatas['orderId']][] = $orderDatas;
			}
			if(($orderDatas['journalTypeCode'] == 'SG') OR ($orderDatas['journalTypeCode'] == 'PG')){
				$jounralDataSC[$orderDatas['orderId']][] = $orderDatas;
			}
		}
		foreach($allSaveSalesOrders as $createOrderId => $bpOrderIds){
			foreach($bpOrderIds as $bpOrderId){
				if(isset($jounralDataSO[$bpOrderId])){
					foreach($jounralDataSO[$bpOrderId] as $key22 => $individualJournalData){
						$newJournalTaxDate	= date('Ymd',strtotime($individualJournalData['taxDate']));
						$postDatasTmpSO[$createOrderId][$newJournalTaxDate][][0] = $jounralDataSO[$bpOrderId][$key22];
					}
				}
			}
		}
		foreach($allSaveSalesCredits as $createOrderId => $bpOrderIds){
			foreach($bpOrderIds as $bpOrderId){
				if(isset($jounralDataSC[$bpOrderId])){
					foreach($jounralDataSC[$bpOrderId] as $key22 => $individualJournalData){
						$newJournalTaxDate	= date('Ymd',strtotime($individualJournalData['taxDate']));
						$postDatasTmpSC[$createOrderId][$newJournalTaxDate][][0] = $jounralDataSC[$bpOrderId][$key22];
					}
				}
			}
		}
		if($postDatasTmpSO){
			foreach($postDatasTmpSO as $createOrderId => $journalDataTmpNew){
				foreach($journalDataTmpNew as $JournalTaxDateConsol => $journalDataTmp){
					$taxDate			    = date('Y-m-d',strtotime($JournalTaxDateConsol));
					$ItemSequence			= 0;
					$Narration			    = '';
					$currency			    = '';
					$channelId			    = '';
					$trackingDetails	    = array();
					$JournalRequest		    = array();
					$JournalLines		    = array();
					$AllDebits			    = array();
					$AllCredits				= array();
					$DebitByNominalArray	= array();
					$CreditByNominalArray	= array();
					$allProcessedJournalsId	= array();
					$allProcessedOrdersId	= array();
					
					$SalesOrderAllInfo			= $this->ci->db->order_by('orderId','desc')->get_where('sales_order',array('createOrderId' => $createOrderId))->row_array();
					if(!$SalesOrderAllInfo){
						continue;
					}
					if($SalesOrderAllInfo['consolPostOptions'] == 2){
						$this->ci->db->where_in('orderId',$allSaveSalesOrders[$createOrderId])->update('cogs_journal',array('status' => 4, 'createdJournalsId' => '', 'message' => 'Posting Is Disable', 'isConsolidated' => 1));
						$this->ci->db->where_in('orderId',$allSaveSalesOrders[$createOrderId])->update('sales_order',array('COGSPosted' => 1));
						continue;
					}
					
					
					$config1					= $this->ci->account1Config[$SalesOrderAllInfo['account1Id']];
					$SalesOrderRowData			= json_decode($SalesOrderAllInfo['rowData'],true);
					$SalesOrderCreatedRowData	= json_decode($SalesOrderAllInfo['createdRowData'],true);
					$BrightpearlBaseCurrency	= strtolower($config1['currencyCode']);
					$MainOrderCurrency			= strtolower($SalesOrderRowData['currency']['orderCurrencyCode']);
					$JournalCurrency			= '';
					foreach($journalDataTmp as $journalData){
						foreach($journalData as $journalDatas){
							if(!$JournalCurrency){
								$JournalCurrency	= strtolower($journalDatas['currencyCode']);
							}
							else{
								break;
							}
						}
						if($JournalCurrency){
							break;
						}
					}
					if(!$JournalCurrency){
						continue;
					}
					$BPReturnedExRate			= $SalesOrderRowData['currency']['exchangeRate'];
					$XeroReturnedExRate			= 1;
					if(isset($SalesOrderCreatedRowData['Response data	: ']['Invoices']['Invoice']['CurrencyRate'])){
						$XeroReturnedExRate			= $SalesOrderCreatedRowData['Response data	: ']['Invoices']['Invoice']['CurrencyRate'];						
					}
					$JournalCalculationVariable	= 1;
					$APIReturnedExRate			= 0;
					
					if($SalesOrderAllInfo['consolPostOptions'] == 0){
						if($BrightpearlBaseCurrency != $XeroBaseCurrency){
							if($MainOrderCurrency != $JournalCurrency){
								$JournalCalculationVariable	= ($JournalCalculationVariable * $BPReturnedExRate);
							}
							if($MainOrderCurrency != $XeroBaseCurrency){
								$JournalCalculationVariable	= ($JournalCalculationVariable * (1 / $XeroReturnedExRate));
							}
						}
						$JournalCalculationVariable	= sprintf("%.6f",($JournalCalculationVariable));
					}
					elseif($SalesOrderAllInfo['consolPostOptions'] == 1){
						if($BrightpearlBaseCurrency != $XeroBaseCurrency){
							if($getAllCurrencyWithRate[$SalesOrderAllInfo['account1Id']][strtolower($XeroBaseCurrency)]['latestExRate']){
								$APIReturnedExRate	= $getAllCurrencyWithRate[$SalesOrderAllInfo['account1Id']][strtolower($XeroBaseCurrency)]['latestExRate'];
							}
							if(!$APIReturnedExRate){
								$this->ci->db->where_in('orderId',$allSaveSalesOrders[$createOrderId])->update('cogs_journal',array('message' => 'exchange rate not found'));
								continue;
							}
							else{
								$JournalCalculationVariable	= ($JournalCalculationVariable * $APIReturnedExRate);
							}
						}
						$JournalCalculationVariable	= sprintf("%.6f",($JournalCalculationVariable));
					}
					
					foreach($journalDataTmp as $journalData){
						foreach($journalData as $journalDatas){
							$config1		= $this->ci->account1Config[$journalDatas['account1Id']];
							$JournalType	= $journalDatas['journalTypeCode'];
							$params			= json_decode($journalDatas['params'],true);
							$channelId		= $journalDatas['channelId'];
							
							$AllCredits	= $params['credits'];
							foreach($AllCredits as $key => $CreditsData){
								$CreditByNominalArray[$JournalType][$CreditsData['nominalCode']] += $CreditsData['transactionAmount'];
							}
							$AllDebits	= $params['debits'];
							foreach($AllDebits as $key => $DebitsData){
								$DebitByNominalArray[$JournalType][$DebitsData['nominalCode']] += $DebitsData['transactionAmount'];
							}
	
							$orderShippingStatus		= '';
							$orderShippingStatus		= $allSaveSalesRef[$createOrderId][$journalDatas['orderId']]['shippingStatus'];
							if($orderShippingStatus == 'ASS'){
								$allProcessedOrdersId[]	= $journalDatas['orderId'];
							}
							$allProcessedJournalsId[]	= $journalDatas['journalsId'];
						}
					}
					if($DebitByNominalArray AND $CreditByNominalArray){
						//sending InvoiceTaxDate ON COGS in case of NetOff Consolidation
						if($allSaveSalesRef[$createOrderId]['isNetOff']){
							$taxDate	= $allSaveSalesRef[$createOrderId]['taxDate'];
							if($clientcode == 'oskarswoodenarkxerom'){
								$BPDateOffset	= substr($taxDate,23,6);
								$BPDateOffset	= explode(":",$BPDateOffset);
								$tempHours		= (int)$BPDateOffset[0];
								$tempMinutes	= (int)$BPDateOffset[1];
								$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
								if($tempHours < 0){
									$totalMinutes = (-1) * $totalMinutes;
								}
								$date			= new DateTime($taxDate);
								$BPTimeZone		= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($totalMinutes){
									$totalMinutes	.= ' minute';
									$date->modify($totalMinutes);
								}
								$taxDate			= $date->format('Y-m-d');
							}
							else{
								$BPOffset	= (int)substr($taxDate,23,3);
								$Acc2Offset	= 0;
								$diff		= $BPOffset - $Acc2Offset;
								$date		= new DateTime($taxDate);
								$BPTimeZone	= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff){
									$diff	.= ' hour';
									$date->modify($diff);
								}
								$taxDate	= $date->format('Y-m-d');
							}
						}
						
						if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['consolcogs'])) AND (strlen($dateLockSettings['consolcogs']) > 0)){
							$checkTaxDate	= date('Ymd',strtotime($taxDate));
							$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['consolcogs'])));
							
							if($dateLockSettings['consolcogsCondition'] == '<='){
								if($checkTaxDate <= $orderDateLock){
									//
								}
								else{continue;}
							}
							elseif($dateLockSettings['consolcogsCondition'] == '>='){
								if($checkTaxDate >= $orderDateLock){
									//
								}
								else{continue;}
							}
							elseif($dateLockSettings['consolcogsCondition'] == '='){
								if($checkTaxDate == $orderDateLock){
									//
								}
								else{continue;}
							}
						}
						
						$CustomFieldValueID	= $allSaveSalesRef[$createOrderId]['CustomFieldValueID'];
						$trackingDetails1	= array();
						$trackingDetails2	= array();
						
						if(isset($channelId)){
							if(isset($channelMappings[$channelId])){
								$trackingDetails1	= $channelMappings[$channelId]['account2ChannelId'];
								$trackingDetails1	= explode("~=",$trackingDetails1);
							}
						}
						if($ChannelcustomfieldMappings AND (isset($channelId))){
							if(($ChannelcustomfieldMappingKey == 'account1CustomFieldId') OR ($ChannelcustomfieldMappingKey == 'account1APIFieldId')){
								if($ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]){
									$trackingDetails2	= $ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]['account2ChannelId'];
									$trackingDetails2	= explode("~=",$trackingDetails2);
								}
							}
							else{
								$trackingDetails2		= array();
							}
						}
						
						$Narration		= $allSaveSalesRef[$createOrderId]['invoiceRef'];
						$ItemSequence	= 0;
						
						foreach($DebitByNominalArray as $TypeOfJournal => $DebitByNominalArrayData){
							foreach($DebitByNominalArrayData as $NominalOfJournal => $DebitAmt){
								if(!$DebitAmt){
									continue;
								}
								$debitAccRef	= '';
								if($TypeOfJournal == 'GO'){
									$debitAccRef	= $config['COGSDebitNominalSO'];
									if(isset($nominalMappings[$NominalOfJournal])){
										if($nominalMappings[$NominalOfJournal]['account2NominalId']){
											$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
										}
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
										if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
											$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
										}
									}
								}
								$JournalLines[$ItemSequence]	= array(
									'Description'	=> $TypeOfJournal.' COGS Journal',
									'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ($DebitAmt))),
									'AccountCode'	=> $debitAccRef,
									'TaxType'		=> $config['salesNoTaxCode'],
								);
								if($trackingDetails1 OR $trackingDetails2){
									if($trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][0]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
										$JournalLines[$ItemSequence]['Tracking'][1]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									elseif($trackingDetails1 AND !$trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
									}
									elseif(!$trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									else{
										unset($JournalLines[$ItemSequence]['Tracking']);
									}
								}
								$ItemSequence++;
							}
						}
						foreach($CreditByNominalArray as $TypeOfJournal => $CreditByNominalArrayData){
							foreach($CreditByNominalArrayData as $NominalOfJournal => $CreditAmt){
								if(!$CreditAmt){
									continue;
								}
								$creditAccRef	= '';
								if($TypeOfJournal == 'GO'){
									$creditAccRef	= $config['COGSCreditNominalSO'];
									if(isset($nominalMappings[$NominalOfJournal])){
										if($nominalMappings[$NominalOfJournal]['account2NominalId']){
											$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
										}
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
										if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
											$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
										}
									}
								}
								$JournalLines[$ItemSequence]	= array(
									'Description'	=> $TypeOfJournal.' COGS Journal',
									'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ((-1) * ($CreditAmt)))),
									'AccountCode'	=> $creditAccRef,
									'TaxType'		=> $config['salesNoTaxCode'],
								);
								if($trackingDetails1 OR $trackingDetails2){
									if($trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][0]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
										$JournalLines[$ItemSequence]['Tracking'][1]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									elseif($trackingDetails1 AND !$trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
									}
									elseif(!$trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									else{
										unset($JournalLines[$ItemSequence]['Tracking']);
									}
								}
								$ItemSequence++;
							}
						}
						if($JournalLines){
							$JournalRequest	= array(
								'Narration'					=> $Narration,
								'JournalLines'				=> $JournalLines,
								'Date'						=> $taxDate,
								'LineAmountTypes'			=> "NoTax",
								'Status'					=> "POSTED",
								'ShowOnCashBasisReports'	=> false,
							);
							if($clientcode == 'keenxero'){
								$JournalRequest['LineAmountTypes']	= 'Exclusive';
							}
							if($clientcode == 'nlg'){
								$JournalRequest['LineAmountTypes']	= 'Exclusive';
							}
						}
						if($JournalRequest){
							$this->headers	= array();
							$url			= '2.0/ManualJournals';
							$results		= $this->getCurl($url, 'PUT', json_encode($JournalRequest), 'json', $account2Id)[$account2Id];
							$createdParams['Request data	: ']	= $JournalRequest;
							$createdParams['Response data	: ']	= $results;
							$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('createdParams' => json_encode($createdParams)));
							if((strtolower($results['Status']) == 'ok') AND (isset($results['ManualJournals']['ManualJournal']['ManualJournalID']))){
								$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('status' => 1, 'createdJournalsId' => $results['ManualJournals']['ManualJournal']['ManualJournalID'], 'message' => '', 'isConsolidated' => 1));
								
								if(!empty($allProcessedOrdersId)){
									$this->ci->db->where_in('orderId',$allProcessedOrdersId)->update('sales_order',array('COGSPosted' => 1));
								}
							}
						}
					}
				}
			}
		}
		if($postDatasTmpSC){
			foreach($postDatasTmpSC as $createOrderId => $journalDataTmpNew){
				foreach($journalDataTmpNew as $JournalTaxDateConsol => $journalDataTmp){
					$taxDate			    = date('Y-m-d',strtotime($JournalTaxDateConsol));
					$ItemSequence			= 0;
					$Narration			    = '';
					$currency			    = '';
					$channelId			    = '';
					$trackingDetails	    = array();
					$JournalRequest		    = array();
					$JournalLines		    = array();
					$AllDebits			    = array();
					$AllCredits				= array();
					$DebitByNominalArray	= array();
					$CreditByNominalArray	= array();
					$allProcessedJournalsId	= array();
					$allProcessedOrdersId	= array();
					
					$SalesCreditOrderAllInfo		= $this->ci->db->order_by('orderId','desc')->get_where('sales_credit_order',array('createOrderId' => $createOrderId))->row_array();
					if(!$SalesCreditOrderAllInfo){
						continue;
					}
					
					if($SalesCreditOrderAllInfo['consolPostOptions'] == 2){
						$this->ci->db->where_in('orderId',$allSaveSalesCredits[$createOrderId])->update('cogs_journal',array('status' => 4, 'createdJournalsId' => '', 'message' => 'Posting Is Disable', 'isConsolidated' => 1));
						$this->ci->db->where_in('orderId',$allSaveSalesCredits[$createOrderId])->update('sales_credit_order',array('COGSPosted' => 1));
						continue;
					}
					
					$config1						= $this->ci->account1Config[$SalesCreditOrderAllInfo['account1Id']];
					$SalesCreditOrderRowData		= json_decode($SalesCreditOrderAllInfo['rowData'],true);
					$SalesCreditOrderCreatedRowData	= json_decode($SalesCreditOrderAllInfo['createdRowData'],true);
					$BrightpearlBaseCurrency		= strtolower($config1['currencyCode']);
					$MainOrderCurrency				= strtolower($SalesCreditOrderRowData['currency']['orderCurrencyCode']);
					$JournalCurrency				= '';
					foreach($journalDataTmp as $journalData){
						foreach($journalData as $journalDatas){
							if(!$JournalCurrency){
								$JournalCurrency	= strtolower($journalDatas['currencyCode']);
							}
							else{
								break;
							}
						}
						if($JournalCurrency){
							break;
						}
					}
					if(!$JournalCurrency){
						continue;
					}
					$BPReturnedExRate				= $SalesCreditOrderRowData['currency']['exchangeRate'];
					$XeroReturnedExRate			= 1;
					if(isset($SalesCreditOrderCreatedRowData['Response data	: ']['CreditNotes']['CreditNote']['CurrencyRate'])){
						$XeroReturnedExRate				= $SalesCreditOrderCreatedRowData['Response data	: ']['CreditNotes']['CreditNote']['CurrencyRate'];
					}
					$JournalCalculationVariable		= 1;
					$APIReturnedExRate				= 0;
					
					if($SalesCreditOrderAllInfo['consolPostOptions'] == 0){
						if($BrightpearlBaseCurrency != $XeroBaseCurrency){
							if($MainOrderCurrency != $JournalCurrency){
								$JournalCalculationVariable	= ($JournalCalculationVariable * $BPReturnedExRate);
							}
							if($MainOrderCurrency != $XeroBaseCurrency){
								$JournalCalculationVariable	= ($JournalCalculationVariable * (1 / $XeroReturnedExRate));
							}
						}
						$JournalCalculationVariable	= sprintf("%.6f",($JournalCalculationVariable));
					}
					elseif($SalesCreditOrderAllInfo['consolPostOptions'] == 1){
						if($BrightpearlBaseCurrency != $XeroBaseCurrency){
							if($getAllCurrencyWithRate[$SalesCreditOrderAllInfo['account1Id']][strtolower($XeroBaseCurrency)]['latestExRate']){
								$APIReturnedExRate	= $getAllCurrencyWithRate[$SalesCreditOrderAllInfo['account1Id']][strtolower($XeroBaseCurrency)]['latestExRate'];
							}
							if(!$APIReturnedExRate){
								$this->ci->db->where_in('orderId',$allSaveSalesCredits[$createOrderId])->update('cogs_journal',array('message' => 'exchange rate not found'));
								continue;
							}
							else{
								$JournalCalculationVariable	= ($JournalCalculationVariable * $APIReturnedExRate);
							}
						}
						$JournalCalculationVariable	= sprintf("%.6f",($JournalCalculationVariable));
					}
					
					
					foreach($journalDataTmp as $journalData){
						foreach($journalData as $journalDatas){
							$config1		= $this->ci->account1Config[$journalDatas['account1Id']];
							$JournalType	= $journalDatas['journalTypeCode'];
							$params			= json_decode($journalDatas['params'],true);
							$channelId		= $journalDatas['channelId'];
							
							$AllCredits	= $params['credits'];
							foreach($AllCredits as $key => $CreditsData){
								$CreditByNominalArray[$JournalType][$CreditsData['nominalCode']] += $CreditsData['transactionAmount'];
							}
							$AllDebits	= $params['debits'];
							foreach($AllDebits as $key => $DebitsData){
								$DebitByNominalArray[$JournalType][$DebitsData['nominalCode']] += $DebitsData['transactionAmount'];
							}
							
							$orderShippingStatus		= '';
							$orderShippingStatus		= $allSaveSalesCreditRef[$createOrderId][$journalDatas['orderId']]['shippingStatus'];
							if($orderShippingStatus == 'SCA'){
								$allProcessedOrdersId[]	= $journalDatas['orderId'];
							}
							$allProcessedJournalsId[]	= $journalDatas['journalsId'];
						}								
					}
					if($DebitByNominalArray AND $CreditByNominalArray){
						//sending InvoiceTaxDate ON COGS in case of NetOff Consolidation
						if($allSaveSalesCreditRef[$createOrderId]['isNetOff']){
							$taxDate	= $allSaveSalesCreditRef[$createOrderId]['taxDate'];
							if($clientcode == 'oskarswoodenarkxerom'){
								$BPDateOffset	= substr($taxDate,23,6);
								$BPDateOffset	= explode(":",$BPDateOffset);
								$tempHours		= (int)$BPDateOffset[0];
								$tempMinutes	= (int)$BPDateOffset[1];
								$totalMinutes	= (abs($tempHours) * 60) + ($tempMinutes);
								if($tempHours < 0){
									$totalMinutes = (-1) * $totalMinutes;
								}
								$date			= new DateTime($taxDate);
								$BPTimeZone		= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($totalMinutes){
									$totalMinutes	.= ' minute';
									$date->modify($totalMinutes);
								}
								$taxDate			= $date->format('Y-m-d');
							}
							else{
								$BPOffset	= (int)substr($taxDate,23,3);
								$Acc2Offset	= 0;
								$diff		= $BPOffset - $Acc2Offset;
								$date		= new DateTime($taxDate);
								$BPTimeZone	= 'GMT';
								$date->setTimezone(new DateTimeZone($BPTimeZone));
								if($diff){
									$diff	.= ' hour';
									$date->modify($diff);
								}
								$taxDate	= $date->format('Y-m-d');
							}
						}
						
						if(($taxDate) AND (is_array($dateLockSettings)) AND (!empty($dateLockSettings)) AND (isset($dateLockSettings['consolcogs'])) AND (strlen($dateLockSettings['consolcogs']) > 0)){
							$checkTaxDate	= date('Ymd',strtotime($taxDate));
							$orderDateLock	= date('Ymd',strtotime(trim($dateLockSettings['consolcogs'])));
							
							if($dateLockSettings['consolcogsCondition'] == '<='){
								if($checkTaxDate <= $orderDateLock){
									//
								}
								else{continue;}
							}
							elseif($dateLockSettings['consolcogsCondition'] == '>='){
								if($checkTaxDate >= $orderDateLock){
									//
								}
								else{continue;}
							}
							elseif($dateLockSettings['consolcogsCondition'] == '='){
								if($checkTaxDate == $orderDateLock){
									//
								}
								else{continue;}
							}
						}
						
						$CustomFieldValueID	= $allSaveSalesCreditRef[$createOrderId]['CustomFieldValueID'];
						$trackingDetails1	= array();
						$trackingDetails2	= array();
						
						if(isset($channelId)){
							if(isset($channelMappings[$channelId])){
								$trackingDetails1	= $channelMappings[$channelId]['account2ChannelId'];
								$trackingDetails1	= explode("~=",$trackingDetails1);
							}
						}
						if($ChannelcustomfieldMappings AND (isset($channelId))){
							if(($ChannelcustomfieldMappingKey == 'account1CustomFieldId') OR ($ChannelcustomfieldMappingKey == 'account1APIFieldId')){
								if($ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]){
									$trackingDetails2	= $ChannelcustomfieldMappings[$channelId][$CustomFieldValueID]['account2ChannelId'];
									$trackingDetails2	= explode("~=",$trackingDetails2);
								}
							}
							else{
								$trackingDetails2		= array();
							}
						}
						
						$Narration		= $allSaveSalesCreditRef[$createOrderId]['invoiceRef'];
						$ItemSequence	= 0;
						
						foreach($DebitByNominalArray as $TypeOfJournal => $DebitByNominalArrayData){
							foreach($DebitByNominalArrayData as $NominalOfJournal => $DebitAmt){
								if(!$DebitAmt){
									continue;
								}
								$debitAccRef	= $config['COGSDebitNominalSC'];
								if(isset($nominalMappings[$NominalOfJournal])){
									if($nominalMappings[$NominalOfJournal]['account2NominalId']){
										$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
									}
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
									if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
										$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
									}
								}
								
								
								if($TypeOfJournal == 'PG'){
									$debitAccRef	= $config['COGSDebitNominalSCPG'];
									if(isset($nominalMappings[$NominalOfJournal])){
										if($nominalMappings[$NominalOfJournal]['account2NominalId']){
											$debitAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
										}
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
										if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
											$debitAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
										}
									}
									
								}
								$JournalLines[$ItemSequence]	= array(
									'Description'	=> $TypeOfJournal.' COGS Journal',
									'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ($DebitAmt))),
									'AccountCode'	=> $debitAccRef,
									'TaxType'		=> $config['salesNoTaxCode'],
								);
								if($trackingDetails1 OR $trackingDetails2){
									if($trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][0]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
										$JournalLines[$ItemSequence]['Tracking'][1]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									elseif($trackingDetails1 AND !$trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
									}
									elseif(!$trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									else{
										unset($JournalLines[$ItemSequence]['Tracking']);
									}
								}
								$ItemSequence++;
							}
						}
						foreach($CreditByNominalArray as $TypeOfJournal => $CreditByNominalArrayData){
							foreach($CreditByNominalArrayData as $NominalOfJournal => $CreditAmt){
								if(!$CreditAmt){
									continue;
								}
								$creditAccRef	= $config['COGSCreditNominalSCPG'];
								if(isset($nominalMappings[$NominalOfJournal])){
									if($nominalMappings[$NominalOfJournal]['account2NominalId']){
										$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
									}
								}
								if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
									if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
										$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
									}
								}
								if($TypeOfJournal == 'SG'){
									$creditAccRef	= $config['COGSCreditNominalSC'];
									if(isset($nominalMappings[$NominalOfJournal])){
										if($nominalMappings[$NominalOfJournal]['account2NominalId']){
											$creditAccRef	= $nominalMappings[$NominalOfJournal]['account2NominalId'];
										}
									}
									if(($channelId) AND (isset($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]))){
										if($nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId']){
											$creditAccRef	= $nominalChannelMappings[strtolower($channelId)][$NominalOfJournal]['account2NominalId'];
										}
									}
								}
								$JournalLines[$ItemSequence]	= array(
									'Description'	=> $TypeOfJournal.' COGS Journal',
									'LineAmount'	=> sprintf("%.4f",($JournalCalculationVariable * ((-1) * ($CreditAmt)))),
									'AccountCode'	=> $creditAccRef,
									'TaxType'		=> $config['salesNoTaxCode'],
								);
								if($trackingDetails1 OR $trackingDetails2){
									if($trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][0]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
										$JournalLines[$ItemSequence]['Tracking'][1]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									elseif($trackingDetails1 AND !$trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails1['0'],
											'Option'	=> $trackingDetails1['1']
										);
									}
									elseif(!$trackingDetails1 AND $trackingDetails2){
										$JournalLines[$ItemSequence]['Tracking'][]	= array(
											'Name'		=> $trackingDetails2['0'],
											'Option'	=> $trackingDetails2['1']
										);
									}
									else{
										unset($JournalLines[$ItemSequence]['Tracking']);
									}
								}
								$ItemSequence++;
							}
						}
						if($JournalLines){
							$JournalRequest	= array(
								'Narration'					=> $Narration,
								'JournalLines'				=> $JournalLines,
								'Date'						=> $taxDate,
								'LineAmountTypes'			=> "NoTax",
								'Status'					=> "POSTED",
								'ShowOnCashBasisReports'	=> false,
							);
							if($clientcode == 'keenxero'){
								$JournalRequest['LineAmountTypes']	= 'Exclusive';
							}
							if($clientcode == 'nlg'){
								$JournalRequest['LineAmountTypes']	= 'Exclusive';
							}
						}
						if($JournalRequest){
							$this->headers	= array();
							$url			= '2.0/ManualJournals';
							$results		= $this->getCurl($url, 'PUT', json_encode($JournalRequest), 'json', $account2Id)[$account2Id];
							$createdParams['Request data	: ']	= $JournalRequest;
							$createdParams['Response data	: ']	= $results;
							$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('createdParams' => json_encode($createdParams)));
							if((strtolower($results['Status']) == 'ok') AND (isset($results['ManualJournals']['ManualJournal']['ManualJournalID']))){
								$this->ci->db->where_in('journalsId',$allProcessedJournalsId)->update('cogs_journal',array('status' => 1, 'createdJournalsId' => $results['ManualJournals']['ManualJournal']['ManualJournalID'], 'message' => '', 'isConsolidated' => 1));
								
								if(!empty($allProcessedOrdersId)){
									$this->ci->db->where_in('orderId',$allProcessedOrdersId)->update('sales_credit_order',array('COGSPosted' => 1));
								}
							}
						}
					}
				}
			}
		}
	}
}