<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>COGS Journal</span><i class="fa fa-circle"></i></li>
				<li><span>Journal Info</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> COGS Journal Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($cogsjournalInfo['params'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> COGS Journal Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								if($cogsjournalInfo['createdParams']){
									echo "<pre>".json_encode(json_decode($cogsjournalInfo['createdParams'],true), JSON_PRETTY_PRINT)."</pre>";
								}
								else{
									$rowDataFilePath	= FCPATH.'createdRowData'. DIRECTORY_SEPARATOR .'cogsJournal'. DIRECTORY_SEPARATOR .$cogsjournalInfo['account2Id']. DIRECTORY_SEPARATOR .$cogsjournalInfo['journalTypeCode']. DIRECTORY_SEPARATOR;
									$rowDataFilePath	= $rowDataFilePath.$cogsjournalInfo['journalsId'].'.json';
									$createdParams		= file_get_contents($rowDataFilePath);
									
									/* echo "<pre>";
									print_r(json_decode($createdParams,true));
									echo "</pre>"; */
									
									echo "<pre>".json_encode(json_decode($createdParams,true), JSON_PRETTY_PRINT)."</pre>";
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>