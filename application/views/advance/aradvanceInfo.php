<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Advance</span><i class="fa fa-circle"></i></li>
				<li><span>AR Advance</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> AR Advance Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php
								$rowInfo	= $aradvanceInfo;
								unset($rowInfo['orderLineDetails']);
								$rowInfo	= json_encode($rowInfo);
								echo "<pre>".json_encode(json_decode($rowInfo,true), JSON_PRETTY_PRINT)."</pre>"; 
							?>
						</div>
					</div>
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['fetchSalesOrder']);?> Order Line Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($aradvanceInfo['orderLineDetails'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
						</div>
					</div>
				</div>
				<div class="portlet ">		
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['postSalesOrder']);?> AR Advance Information
						</div>		
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<?php	echo "<pre>".json_encode(json_decode($aradvanceInfo['createdRowData'],true), JSON_PRETTY_PRINT)."</pre>";	?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>