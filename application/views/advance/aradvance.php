<?php
$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postProduct'].'_account')->result_array();
$userLoginData			= $this->session->userdata('login_user_data');
$accessRoles			= array('admin', 'developer', '1', 'testing');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="index.html">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Advance</span><i class="fa fa-circle"></i></li>
				<li><span>AR Advance</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-shopping-cart"></i>AR Advance Listing</div>
						<div class="actions">
							<a href="<?php echo base_url('advance/aradvance/fetchAradvance');?>" class="btn btn-circle btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs">Fetch AR Advance</span>
							</a>
							<?php	if(in_array($userLoginData['role'], $accessRoles)){	?>
							<a href="" class="btn btn-circle btnactionsubmit" onclick="confirmAction()">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Post AR Advance</span>
							</a>
							<a href="" class="btn btn-circle btnactionsubmit" onclick="confirmAction1()">
								<i class="fa fa-upload"></i>
								<span class="hidden-xs">Apply AR Advance</span>
							</a>
							<?php	}	?>
							<a href="<?php echo base_url('advance/aradvance/exportAradvance?');?>" class="btn btn-circle btn-danger exportBtn">
								<i class="fa fa-file-excel-o"></i>
								<span class="hidden-xs">Export Report in CSV</span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper">
								<span></span>
								<?php	if(in_array($userLoginData['role'], $accessRoles)){	?>
								<select class="table-group-action-input form-control input-inline input-small input-sm">
									<option value="">Select...</option>
									<option value="0">Pending</option>
									<option value="1">Sent</option>
									<option value="2">Reversed</option>
									<option value="3">Applied</option>
									<option value="4">Archive</option>
									<option value="9">RemoveError</option>
								</select>
								<button class="btn btn-sm btn-success table-group-action-submit"><i class="fa fa-check"></i>Submit</button>
								<?php	}	?> 
							</div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="10%"><?php echo $this->globalConfig['account1Name'];?></th>
										<th width="10%"><?php echo $this->globalConfig['account2Name'];?></th>
										<th width="8%">Payment</th>
										<th width="8%">Journal</th>
										<th width="8%">Order</th>
										<th width="8%">Amount</th>
										<th width="8%">Type</th>
										<th width="8%">Method</th>
										<th width="8%">Contact</th>
										<th width="10%">Date</th>
										<th width="10%">Channel</th>
										<th width="10%">Txn</th>
										<th width="10%">Prepayment</th>
										<th width="10%">Status</th>
										<th width="10%">Message</th>
										<th width="10%">Actions</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td>
											<select name="account1Id" class="form-control form-filter input-sm account1Id">
												<option value="">Select an option</option>
												<?php
													foreach($account1MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td>
											<select name="account2Id" class="form-control form-filter input-sm account2Id">
												<option value="">Select an option</option>
												<?php
													foreach($account2MappingTemps as $account1MappingTemp){	
														echo '<option value="'.$account1MappingTemp['id'].'">'.$account1MappingTemp['name'].'</option>';
													}
												?>
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm paymentId" name="paymentId" /></td>
										<td><input type="text" class="form-control form-filter input-sm journalId" name="journalId" /></td>
										<td><input type="text" class="form-control form-filter input-sm orderId" name="orderId" /></td>
										<td><input type="text" class="form-control form-filter input-sm amountPaid" name="amountPaid" /></td>
										<td><input type="text" class="form-control form-filter input-sm paymentType" name="paymentType" /></td>
										<td><input type="text" class="form-control form-filter input-sm paymentMethodCode" name="paymentMethodCode" /></td>
										<td><input type="text" class="form-control form-filter input-sm contactId" name="contactId" /></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm paymentDate_from" readonly name="paymentDate_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm paymentDate_to" readonly name="paymentDate_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td><input type="text" class="form-control form-filter input-sm channelId" name="channelId" /></td>
										<td><input type="text" class="form-control form-filter input-sm bankTxnId" name="bankTxnId" /></td>
										<td><input type="text" class="form-control form-filter input-sm prePaymentId" name="prePaymentId" /></td>
										<td>
											<select name="status" class="form-control form-filter input-sm status">
												<option value="">Select...</option>
												<option value="0">Pending</option>
												<option value="1">Sent</option>
												<option value="2">Reversed</option>
												<option value="3">Applied</option>
												<option value="4">Archive</option>
											</select>
										</td>
										<td><input type="text" class="form-control form-filter input-sm message" name="message" /></td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button>
											</div>
											<button class="btn btn-sm btn-default filter-cancel">	<i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody> </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>		
<script type="text/javascript">
	function confirmAction(){
		var answer = confirm("Do you want to send all the Advances?");
		if(answer == true){
			$.ajax({
				url			:	"<?php echo base_url('advance/aradvance/postAradvance');?>",
				type		:	"POST",
				dataType	:	"json",
				success		:	function(){}
			})
		}
	}
	function confirmAction1(){
		var answer = confirm("Do you want to apply all the Advances?");
		if(answer == true){
			$.ajax({
				url			:	"<?php echo base_url('advance/aradvance/applyAradvance');?>",
				type		:	"POST",
				dataType	:	"json",
				success		:	function(){}
			})
		}
	}
	loadUrl = '<?php echo base_url('advance/aradvance/getAradvance');?>';
	
	jQuery(".exportBtn").on("click",function(e){
		e.preventDefault();
		url	= jQuery(this).attr('href')+'account1Id='+jQuery(".account1Id").val()+'&account2Id='+jQuery(".account2Id").val()+'&paymentId='+jQuery(".paymentId").val()+'&journalId='+jQuery(".journalId").val()+'&orderId='+jQuery(".orderId").val()+'&amountPaid='+jQuery(".amountPaid").val()+'&paymentType='+jQuery(".paymentType").val()+'&paymentMethodCode='+jQuery(".paymentMethodCode").val()+'&contactId='+jQuery(".contactId").val()+'&channelId='+jQuery(".channelId").val()+'&bankTxnId='+jQuery(".bankTxnId").val()+'&prePaymentId='+jQuery(".prePaymentId").val()+'&status='+jQuery(".status").val()+'&paymentDate_to='+jQuery(".paymentDate_to").val()+'&paymentDate_from='+jQuery(".paymentDate_from").val();
		window.location.href	= url;
	})
</script>
<style>
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
		top: 28px !important;
	}
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
		top: 28px !important;
	}
</style>