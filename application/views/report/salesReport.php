<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Sales Repport Details</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-file"></i>Sales Report Listing</div>
						<div class="actions">
							<a href="<?php echo base_url('report/salesReport/fetchSalesReport');?>" class="btn btn-circle btn-info btnactionsubmit">
								<i class="fa fa-download"></i>
								<span class="hidden-xs">Fetch Sales Report</span>
							</a>
							<a href="<?php echo base_url('report/salesReport/exportSalesReport');?>" class="btn btn-circle green-meadow btnactionsubmit">
								<i class="fa fa-file"></i>
								<span class="hidden-xs">Export CSV</span>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-container">
							<div class="table-actions-wrapper"></div>
							<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="1%"><input type="checkbox" class="group-checkable"> </th>
										<th width="10%">BP OrderID</th>
										<th width="10%">Xero OrderID</th>
										<th width="10%">BP Invoice</th>
										<th width="10%">Xero Invoice</th>
										<th width="5%">BP Order Amount</th>
										<th width="5%">Xero Order Amount</th>
										<th width="5%">BP Tax Amount</th>
										<th width="5%">Xero Tax Amount</th>
										<th width="5%">BP Paid Amount</th>
										<th width="5%">Xero Paid Amount</th>
										<th width="10%">BP Tax Date</th>
										<th width="10%">Xero Tax Date</th>
										<th width="10%">Action</th>
									</tr>
									<tr role="row" class="filter">
										<td></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderId"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="createOrderId"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="bpInvoice"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="xeroInvoice"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderAmountBP"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="orderAmountXero"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="taxAmountBP"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="taxAmountXero"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="paidAmountBP"></td>
										<td><input type="text" class="form-control form-filter input-sm" name="paidAmountXero"></td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="bpTaxDate_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="bpTaxDate_to" placeholder="To" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="xeroTaxDate_from" placeholder="From" />
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="xeroTaxDate_to" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<div class="margin-bottom-5"><button class="btn btn-sm btn-success filter-submit margin-bottom"><i class="fa fa-search"></i>Search</button></div>
											<button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>Reset</button>
										</td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	loadUrl	= '<?php echo base_url('report/salesReport/getSalesReport');?>';
</script>
<style>
.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-right.datepicker-orient-bottom {
	top: 28px !important;
}
.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
	top: 28px !important;
}
</style>