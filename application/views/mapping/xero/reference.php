<?php	
	$account1Fieldconfigso	= $data['account1CustomFieldId'];
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Mapping</span><i class="fa fa-circle"></i></li>
				<li><span>Channel + Custom Field : Reference/Notes/InvoiceLine Mapping</span></li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-tag"></i>Channel + Custom Field : Reference/Notes/InvoiceLine Mapping</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Mapping</span>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="">
						<table class="table table-hover text-centered actiontable" id="sample_4">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th width="10%"><?php echo $this->globalConfig['account1Name'];?> Channel</th>
									<th width="10%">Xero Invoice Reference</th>
									<th width="10%">Xero Invoice Payment Reference</th>
									<th width="10%">Xero Invoice Notes & History</th>
									<th width="10%">Prefix For Notes & History</th>
									<th width="10%">Xero Invoice Line</th>
									<th width="10%">Prefix For Line</th>
									<th width="10%"><?php echo $this->globalConfig['account1Name'];?> Account</th>
									<th width="10%"><?php echo $this->globalConfig['account2Name'];?> Account</th>
									<th width="5%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="account1ChannelId"></span></td>
									<td><span class="value" data-value="account1CustomFieldId"></span></td>
									<td><span class="value" data-value="account1CustomFieldIdforPayRef"></span></td>
									<td><span class="value" data-value="account1CustomFieldIdforNotes"></span></td>
									<td><span class="value" data-value="account1PrefixForNotes"></span></td>
									<td><span class="value" data-value="account1CustomFieldIdLine"></span></td>
									<td><span class="value" data-value="account1PrefixForLine"></span></td>
									<td><span class="value" data-value="account1Id"></span></td>
									<td><span class="value" data-value="account2Id"></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('mapping/reference/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	foreach($data['data'] as $key => $row){	?>
								<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
								<tr class="tr<?php echo $row['id'];?>">
									<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									
									<td><span class="value" data-value="account1ChannelId"><?php echo @($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']])?($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']]['name']):($row['account1ChannelId']);?></span></td>
									
									
									
									<td>
										<span class="value" data-value="account1CustomFieldId">
										<?php
											if($row['account1CustomFieldId']){
												$account1CustomFieldId	= explode(",",trim($row['account1CustomFieldId']));
												foreach($account1CustomFieldId as $account1CustomFieldIdTemp){
													echo @($data['account1CustomFieldId'][$account1CustomFieldIdTemp])?($data['account1CustomFieldId'][$account1CustomFieldIdTemp]['name']):($account1CustomFieldIdTemp);
													echo "<br>";
												}
											}
										?>
										</span>
									</td>
									
									<td>
										<span class="value" data-value="account1CustomFieldIdforPayRef">
										<?php
											if($row['account1CustomFieldIdforPayRef']){
												$account1CustomFieldIdforPayRef	= explode(",",trim($row['account1CustomFieldIdforPayRef']));
												foreach($account1CustomFieldIdforPayRef as $account1CustomFieldIdforPayRefTemp){
													echo @($data['account1CustomFieldId'][$account1CustomFieldIdforPayRefTemp])?($data['account1CustomFieldId'][$account1CustomFieldIdforPayRefTemp]['name']):($account1CustomFieldIdforPayRefTemp);
													echo "<br>";
												}
											}
										?>
										</span>
									</td>
									
									<td>
										<span class="value" data-value="account1CustomFieldIdforNotes">
										<?php
											if($row['account1CustomFieldIdforNotes']){
												$account1CustomFieldIdforNotes	= explode(",",trim($row['account1CustomFieldIdforNotes']));
												foreach($account1CustomFieldIdforNotes as $account1CustomFieldIdforNotesTemp){
													echo @($data['account1CustomFieldId'][$account1CustomFieldIdforNotesTemp])?($data['account1CustomFieldId'][$account1CustomFieldIdforNotesTemp]['name']):($account1CustomFieldIdforNotesTemp);
													echo "<br>";
												}
											}
										?>
										</span>
									</td>
									
									<td><span class="value" data-value="account1PrefixForNotes"><?php echo @($row['account1PrefixForNotes']);?></span></td>
									
									<td>
										<span class="value" data-value="account1CustomFieldIdLine">
										<?php
											if($row['account1CustomFieldIdLine']){
												$account1CustomFieldIdLine	= explode(",",trim($row['account1CustomFieldIdLine']));
												foreach($account1CustomFieldIdLine as $account1CustomFieldIdLine){
													echo @($data['account1CustomFieldId'][$account1CustomFieldIdLine])?($data['account1CustomFieldId'][$account1CustomFieldIdLine]['name']):($account1CustomFieldIdLine);
													echo "<br>";
												}
											}
										?>
										</span>
									</td>
									
									<td><span class="value" data-value="account1PrefixForLine"><?php echo @($row['account1PrefixForLine']);?></span></td>
									
									<td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
									
									<td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
									
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/reference/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade " id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><b>Channel + Custom Field : Reference/Notes/InvoiceLine Mapping</b></h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('mapping/reference/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide"><button class="close" data-close="alert"></button> You have some form errors. Please check below.</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
											<?php
												foreach ($data['account2Id'] as $account2Id) {
													echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Channel Name</label>
									<div class="col-md-7">
										<?php	if(@$data['account1ChannelId']){	?>
										<select name="data[account1ChannelId]" data-required="0" class="form-control acc1listoption account1ChannelId">
											<option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Channel Name</option>
												<?php
													foreach ($data['account1ChannelId'] as $accountId => $account1ChannelIds) {
														foreach ($account1ChannelIds as $account1ChannelId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1ChannelId['id'].'">'.ucwords($account1ChannelId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	
												}
												else{
										?>
										<input type="text" name="data[account1ChannelId]" data-required="0" class="form-control account1ChannelId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Xero Invoice Reference</label>
									<div class="col-md-7">
										<select name="data[account1CustomFieldId]" data-required="0" class="form-control account1CustomFieldId">
											<option value="">Select a value</option>
											<?php
												foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
													echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
												}
											?>
										</select> 
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Xero Invoice Payment Reference</label>
									<div class="col-md-7">
										<select name="data[account1CustomFieldIdforPayRef]" data-required="0" class="form-control account1CustomFieldIdforPayRef">
											<option value="">Select a value</option>
											<?php
												foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
													echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
												}
											?>
										</select> 
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Xero Invoice Notes & History</label>
									<div class="col-md-7">
										<select name="data[account1CustomFieldIdforNotes][]" data-required="0" class="form-control account1CustomFieldIdforNotes" multiple="true">
											<option value="">Select a value</option>
											<?php
												foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
													echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
												}
											?>
										</select> 
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Prefix For Notes & History</label>
									<div class="col-md-7">
										<input type="text" name="data[account1PrefixForNotes]" data-required="0" class="form-control account1PrefixForNotes" />
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Xero Invoice Line</label>
									<div class="col-md-7">
										<select name="data[account1CustomFieldIdLine][]" data-required="0" class="form-control account1CustomFieldIdLine" multiple="true">
											<option value="">Select a value</option>
											<?php
												foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
													echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
												}
											?>
										</select> 
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4">Prefix For Invoice Line</label>
									<div class="col-md-7">
										<input type="text" name="data[account1PrefixForLine]" data-required="0" class="form-control account1PrefixForLine" />
									</div>
								</div>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>