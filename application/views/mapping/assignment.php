<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Mapping</span><i class="fa fa-circle"></i></li>
				<li><span>Assignment : TrackingCategory Mapping</span></li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-bar-chart"></i>Assignment : TrackingCategory Mapping</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Mapping</span>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="">
						<table class="table table-hover text-centered actiontable" id="sample_4">
							<thead>
								<tr>
									<th width="5%">#</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Assigned To</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Project</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Channel</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> LeadSource</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Team</th>
									<th width="15%"><?php echo $this->globalConfig['account2Name'];?> Channel</th>
									<th width="15%"><?php echo $this->globalConfig['account1Name'];?> Account</th>
									<th width="15%"><?php echo $this->globalConfig['account2Name'];?> Account</th>
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="account1StaffId"></span></td>
									<td><span class="value" data-value="account1ProjectId"></span></td>
									<td><span class="value" data-value="account1ChannelId"></span></td>
									<td><span class="value" data-value="account1LeadSourceId"></span></td>
									<td><span class="value" data-value="account1TeamId"></span></td>
									<td><span class="value" data-value="account2ChannelId"></span></td>
									<td><span class="value" data-value="account1Id"></span></td>
									<td><span class="value" data-value="account2Id"></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('mapping/assignment/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	foreach ($data['data'] as $key =>  $row){	?>
								<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
								<tr class="tr<?php echo $row['id'];?>">
									<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									
									<td><span class="value" data-value="account1StaffId"><?php echo @($data['account1StaffId'][$row['account1Id']][$row['account1StaffId']])?($data['account1StaffId'][$row['account1Id']][$row['account1StaffId']]['name']):($row['account1StaffId']);?></span></td>
									
									<td><span class="value" data-value="account1ProjectId"><?php echo @($data['account1ProjectId'][$row['account1Id']][$row['account1ProjectId']])?($data['account1ProjectId'][$row['account1Id']][$row['account1ProjectId']]['name']):($row['account1ProjectId']);?></span></td>
									
									<td><span class="value" data-value="account1ChannelId"><?php echo @($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']])?($data['account1ChannelId'][$row['account1Id']][$row['account1ChannelId']]['name']):($row['account1ChannelId']);?></span></td>
									
									<td><span class="value" data-value="account1LeadSourceId"><?php echo @($data['account1LeadSourceId'][$row['account1Id']][$row['account1LeadSourceId']])?($data['account1LeadSourceId'][$row['account1Id']][$row['account1LeadSourceId']]['name']):($row['account1LeadSourceId']);?></span></td>
									
									<td><span class="value" data-value="account1TeamId"><?php echo @($data['account1TeamId'][$row['account1Id']][$row['account1TeamId']])?($data['account1TeamId'][$row['account1Id']][$row['account1TeamId']]['name']):($row['account1TeamId']);?></span></td>
									
									<td><span class="value" data-value="account2ChannelId"><?php echo @($data['account2ChannelId'][$row['account2Id']][$row['account2ChannelId']])?($data['account2ChannelId'][$row['account2Id']][$row['account2ChannelId']]['name']):($row['account2ChannelId']);?></span></td>
									
									<td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
									
									<td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
									
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/assignment/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade " id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Assignment : TrackingCategory Mapping</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('mapping/assignment/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body">
								<div class="alert alert-danger display-hide"><button class="close" data-close="alert"></button> You have some form errors. Please check below.</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Account
										<span class="required" aria-required="true"> * </span>
									</label>
									<div class="col-md-7">
										<select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
											<?php
												foreach ($data['account1Id'] as $account1Id) {
													echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Account<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
											<?php
												foreach ($data['account2Id'] as $account2Id) {
													echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Assigned To Name</label>
									<div class="col-md-7">
										<?php	if(@$data['account1StaffId']){	?>
										<select name="data[account1StaffId]" data-required="0" class="form-control acc1listoption account1StaffId">
											<option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Staff Name</option>
												<?php
													foreach ($data['account1StaffId'] as $accountId => $account1StaffIds) {
														foreach ($account1StaffIds as $account1StaffId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1StaffId['id'].'">'.ucwords($account1StaffId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	
												}
												else{
										?>
										<input type="text" name="data[account1StaffId]" data-required="0" class="form-control account1StaffId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Project Name</label>
									<div class="col-md-7">
										<?php	if(@$data['account1ProjectId']){	?>
										<select name="data[account1ProjectId]" data-required="0" class="form-control acc1listoption account1ProjectId">
											<option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Project Name</option>
												<?php
													foreach ($data['account1ProjectId'] as $accountId => $account1ProjectIds) {
														foreach ($account1ProjectIds as $account1ProjectId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1ProjectId['projectId'].'">'.ucwords($account1ProjectId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	
												}
												else{
										?>
										<input type="text" name="data[account1ProjectId]" data-required="0" class="form-control account1ProjectId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Channel Name</label>
									<div class="col-md-7">
										<?php	if(@$data['account1ChannelId']){	?>
										<select name="data[account1ChannelId]" data-required="0" class="form-control acc1listoption account1ChannelId">
											<option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Channel Name</option>
												<?php
													foreach ($data['account1ChannelId'] as $accountId => $account1ChannelIds) {
														foreach ($account1ChannelIds as $account1ChannelId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1ChannelId['id'].'">'.ucwords($account1ChannelId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	
												}
												else{
										?>
										<input type="text" name="data[account1ChannelId]" data-required="0" class="form-control account1ChannelId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> LeadSource Name</label>
									<div class="col-md-7">
										<?php	if(@$data['account1LeadSourceId']){	?>
										<select name="data[account1LeadSourceId]" data-required="0" class="form-control acc1listoption account1LeadSourceId">
											<option value="">Select a <?php echo $this->globalConfig['account1Name'];?> LeadSource Name</option>
												<?php
													foreach ($data['account1LeadSourceId'] as $accountId => $account1LeadSourceIds) {
														foreach ($account1LeadSourceIds as $account1LeadSourceId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1LeadSourceId['id'].'">'.ucwords($account1LeadSourceId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	
												}
												else{
										?>
										<input type="text" name="data[account1LeadSourceId]" data-required="0" class="form-control account1LeadSourceId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Team Name</label>
									<div class="col-md-7">
										<?php	if(@$data['account1TeamId']){	?>
										<select name="data[account1TeamId]" data-required="0" class="form-control acc1listoption account1TeamId">
											<option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Team Name</option>
												<?php
													foreach ($data['account1TeamId'] as $accountId => $account1TeamIds) {
														foreach ($account1TeamIds as $account1TeamId) {
															echo '<option class="acc1listoption'.$accountId.'" value="'.$account1TeamId['id'].'">'.ucwords($account1TeamId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	
												}
												else{
										?>
										<input type="text" name="data[account1TeamId]" data-required="0" class="form-control account1TeamId" />
										<?php	}	?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> TrackingCategory Name<span class="required" aria-required="true"> * </span></label>
									<div class="col-md-7">
										<?php	if(@$data['account2ChannelId']){	?> 
										<select name="data[account2ChannelId]" data-required="1" class="form-control acc2listoption account2ChannelId">
											<option value="">Select a <?php echo $this->globalConfig['account2Name'];?> TrackingCategory Name</option>
												<?php
													foreach ($data['account2ChannelId'] as $account2Id => $account2ChannelIds) {
														foreach ($account2ChannelIds as $account2ChannelId) {
															echo '<option class="acc2listoption'.$account2Id.'" value="'.$account2ChannelId['id'].'">'.ucwords($account2ChannelId['name']).'</option>';
														}
													}
												?>
										</select>
										<?php	
												}
												else{
										?>
										<input type="text" name="data[account2ChannelId]" data-required="1" class="form-control account2ChannelId" />
										<?php	}	?>
									</div>
								</div>
							</div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction">Save</button>
						<button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>