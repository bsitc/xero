<div class="page-content-wrapper">
<div class="page-content">
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
			<li><span>Product Info</span></li>
		</ul>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['account1Liberary']);?> Product Information
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<?php	echo "<pre>".json_encode(json_decode($productInfo['params'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
					</div>
				</div>
			</div>
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-file"></i><?php echo ucwords($this->globalConfig['postProduct']);?> Product Information
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<?php	echo "<pre>".json_encode(json_decode($productInfo['ceatedParams'],true), JSON_PRETTY_PRINT)."</pre>"; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>