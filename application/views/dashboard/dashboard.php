<?php
//xero
$librariesNames	= array('brightpearl' => 'Brightpearl','shopify' => 'Shopify', 'fastmag' => 'FASTMAG','netsuite' => 'Netsuite','ilg' => 'ILG','bluepark' => 'Bluepark');
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Dashboard</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="profile-sidebar">
					<div class="portlet light profile-sidebar-portlet ">
						<div class="profile-userpic">
							<img src="<?php echo $user_session_data['profileimage'];?>" class="img-responsive profilepicture" alt=""></img>
						</div>
						<div class="profile-usertitle">
							<div class="profile-usertitle-name"> <?php echo $data['user']['firstname'] ." " . $data['user']['lastname'];?> </div>
						</div>
					</div>
				</div>
				<div class="profile-content">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title tabbable-line">
									<div class="caption caption-md">
										<i class="icon-globe theme-font hide"></i>
										<span class="caption-subject font-blue-madison bold uppercase">Dashboard</span>
									</div>
									<ul class="nav nav-tabs"><li class="active newtabcss"><a href="#tab_1_1" data-toggle="tab">Personal Info</a></li></ul>
								</div>
								<div class="portlet-body">
									<div class="tab-content">
										<div class="tab-pane active" id="tab_1_1">
											<form role="form" action="<?php echo base_url('users/profile/saveBasic');?>">
												<div class="alert alert-success hide">
													<strong>Info!</strong> Data saved successfully.
												</div>
												<div class="form-group">
													<label class="control-label">Username</label>
													<input type="text" name="firstname" value="<?php echo $data['user']['username'] ;?>" placeholder="First Name" class="form-control" />
												</div>
												<div class="form-group">
													<label class="control-label">Email</label>
													<input type="email" placeholder="Email" name="email" value="<?php echo $data['user']['email'];?>"  class="form-control" />
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--
					<div class="row statmaindiv">
						<div class="col-md-3 statdiv">
							<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
								<h4 class="widget-thumb-heading">Remaining Xero API</h4>
								<div class="widget-thumb-wrap">
									<div class="row">
										<div class="col-md-4">
											<i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
										</div>
										<div class="col-md-8">
											<div class="row">
												<div class="widget-thumb-body">
													<div class="col-md-8">
														<span class="widget-thumb-subtitle">Account 1</span>
													</div>
													<div class="col-md-4">
														<span class="widget-thumb-body-stat" data-counter="counterup" data-value="0">0</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="widget-thumb-body">
													<div class="col-md-8">
														<span class="widget-thumb-subtitle">Account 2</span>
													</div>
													<div class="col-md-4">
														<span class="widget-thumb-body-stat" data-counter="counterup" data-value="0">0</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 statdiv">
							<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
								<h4 class="widget-thumb-heading">Order Fetched in 24 hours</h4>
								<div class="widget-thumb-wrap">
									<div class="row">
										<div class="col-md-4">
											<i class="widget-thumb-icon bg-blue icon-basket"></i>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">SO</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">PO</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">SC</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">PC</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 statdiv">
							<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
								<h4 class="widget-thumb-heading">Pending Orders</h4>
								<div class="widget-thumb-wrap">
									<div class="row">
										<div class="col-md-4">
											<i class="widget-thumb-icon bg-blue fa fa-exclamation"></i>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">SO</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">PO</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">SC</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="widget-thumb-body">
												<span class="widget-thumb-subtitle">PC</span>
												<span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071">0</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					-->
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo $this->config->item('script_url');?>assets/pages/css/profile.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->config->item('script_url');?>assets/pages/scripts/profile.js" type="text/javascript"></script>