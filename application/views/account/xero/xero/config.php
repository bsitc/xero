<?php
//xero
	$allCurrencyList		= array('AFA','ALL','DZD','AOR','ARS','AMD','AWG','AUD','AZN','BSD','BHD','BDT','BBD','BYN','BZD','BMD','BTN','BOB','BWP','BRL','GBP','BND','BGN','BIF','KHR','CAD','CVE','KYD','XOF','XAF','XPF','CLP','CNY','COP','KMF','CDF','CRC','HRK','CUP','CZK','DKK','DJF','DOP','XCD','EGP','SVC','ERN','EEK','ETB','EUR','FKP','FJD','GMD','GEL','GHS','GIP','XAU','XFO','GTQ','GNF','GYD','HTG','HNL','HKD','HUF','ISK','XDR','INR','IDR','IRR','IQD','ILS','JMD','JPY','JOD','KZT','KES','KWD','KGS','LAK','LVL','LBP','LSL','LRD','LYD','LTL','MOP','MKD','MGA','MWK','MYR','MVR','MRO','MUR','MXN','MDL','MNT','MAD','MZN','MMK','NAD','NPR','ANG','NZD','NIO','NGN','KPW','NOK','OMR','PKR','XPD','PAB','PGK','PYG','PEN','PHP','XPT','PLN','QAR','RON','RUB','RWF','SHP','WST','STD','SAR','RSD','SCR','SLL','XAG','SGD','SBD','SOS','ZAR','KRW','LKR','SDG','SRD','SZL','SEK','CHF','SYP','TWD','TJS','TZS','THB','TOP','TTD','TND','TRY','TMT','AED','UGX','XFU','UAH','UYU','USD','UZS','VUV','VEF','VND','YER','ZMK','ZWL');
	$account1Fieldconfigso	= $data['account1Fieldconfigso'];
	$account1Fieldconfigpo	= $data['account1Fieldconfigpo'];
	$userLoginData			= $this->session->userdata('login_user_data');
	$accessRoles			= array('admin', 'developer', '1');
	if(in_array($userLoginData['role'], $accessRoles)){
?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Account Settings</span><i class="fa fa-circle"></i></li>
				<li><span><?php echo $this->globalConfig['account2Name'];?> Settings</span><i class="fa fa-circle"></i></li>
				<li><span>Default Configuration</span> </li>
			</ul>
		</div>
		<div class="portlet ">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-cogs"></i><?php echo $this->globalConfig['account2Name'];?> Configuration</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
						<i class="fa fa-plus"></i>
						<span class="hidden-xs">Add New Configuration</span>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="table-responsive">          
						<table class="table table-hover text-centered actiontable">
							<thead>
								<tr>
									<th width="5%">#</th> 
									<th width="25%"><?php echo $this->globalConfig['account2Name'];?> ID</th>
									<th width="25%">Account ID</th>
									<th width="10%">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr class="clone hide">
									<td><span class="value" data-value="id"></span></td>
									<td><span class="value" data-value="xeroAccountId"></span></td>
									<td><span class="value" data-value="compte"></span></td>
									<td class="action">
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	foreach($data['data'] as $key => $row){	?>
								<tr class="tr<?php echo $row['id'];?>">
									<td><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
									<td><span class="value" data-value="xeroAccountId"><?php echo $row['xeroAccountId'];?></span></td>
									<td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
									<td class="action">
										<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
										<a class="actioneditbtn btn btn-icon-only" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
										<a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
									</td>
								</tr>
								<?php	}	?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-xl">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><b><?php echo $this->globalConfig['account2Name'];?> Configuration</b></h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
							<div class="form-body"></div>
							<input type="hidden" name="data[id]" class="id" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="pull-left btn btn-primary submitAction sbold btnExtra">Save</button>
						<button type="button" class="btn yellow btn-outline sbold btnExtra" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="confighml">
<?php
	$data['data']	= ($data['data'])?($data['data']):(array(''));
	foreach($data['data'] as $key => $row){
		$account1Id	= $data['saveAccount'][$row['xeroAccountId']]['account1Id'];
?>
	<div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button> You have some form errors. Please check below.
		</div>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#defaults" data-toggle="tab">Default Settings</a></li>
			<?php	if(($this->globalConfig['enableInventoryManagement']) OR ($this->globalConfig['enableInventoryTransfer'])){	?>
			<li><a href="#inventorySettings" data-toggle="tab">Inventory Settings</a></li>
			<?php	}	?>
			<?php	if($this->globalConfig['enableCOGSJournals'] || $this->globalConfig['enableCOGSWOInvoice'] ){	?>
			<li><a href="#defaultCOGSAccounts" data-toggle="tab">COGS Settings</a></li>
			<?php	}	?>
			<li><a href="#defaultsItemsNominals" data-toggle="tab">Items/Nominal Settings</a></li>
			<?php	if($this->globalConfig['enableAmazonFees']){	?>
			<li><a href="#amazonFees" data-toggle="tab">Amazon Settings</a></li>
			<?php	}	?>
			<li><a href="#otherSettings" data-toggle="tab">Other Settings</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="defaults">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle"><?php echo $this->globalConfig['account2Name'];?> ID<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[xeroAccountId]" data-required="1" class="form-control xeroAccountId">
										<option value="">Select a <?php echo $this->globalConfig['account2Name'];?> account</option>
										<?php
											foreach ($data['saveAccount'] as $saveAccount) {
												echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Currency</label>
								<div class="marginStyle">
									<input type="text" name="data[defaultCurrency]" value="<?php echo $data['accountInfo'][$row['xeroAccountId']]['BaseCurrency']; ?>" class="form-control" readonly="readonly" />
									
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Timezone</label>
								<div class="marginStyle">
									<input type="text" name="data[XeroTimeZone]" value="<?php echo $data['accountInfo'][$row['xeroAccountId']]['Timezone']; ?>" class="form-control" readonly="readonly" />
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Accounts</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">COGS Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[COGSAccountRef]" data-required="1" class="form-control COGSAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Income Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[IncomeAccountRef]" data-required="1" class="form-control IncomeAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Expense Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle">
									<select name="data[ExpenseAccountRef]" data-required="1" class="form-control ExpenseAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Asset Account<span class="required" aria-required="true"> * </span></label>
								<div class="marginStyle marginStyle2">
									<select name="data[AssetAccountRef]" data-required="1" class="form-control AssetAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Stock Adjustment Account</label>
								<div class="marginStyle">
									<select name="data[stockAdjustmentAccountRef]" class="form-control stockAdjustmentAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<?php	if($this->globalConfig['enablePrepayments']){	?>
						<div class="col-md-3">
							<div class="form-group">
									<label class="control-label marginStyle">PrePayment Account</label>
									<div class="marginStyle">
										<select name="data[prePaymentNominal]" class="form-control prePaymentNominal">
											<option value="">Select Account</option>
											<?php
												foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
													echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
												}
											?>
										</select>
									</div>
								</div>
						</div>
						<?php	}	?>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Tax Codes</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order Tax Code</label>
								<div class="marginStyle">
									<select name="data[TaxCode]" class="form-control TaxCode">
										<option value="">Select Tax</option>
										<?php
											foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
												echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order No Tax Code</label>
								<div class="marginStyle">
									<select name="data[salesNoTaxCode]" class="form-control salesNoTaxCode">
										<option value="">Select Tax</option>
										<?php
											foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
												echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Purchase Order Tax Code</label>
								<div class="marginStyle">
									<select name="data[PurchaseTaxCode]" class="form-control PurchaseTaxCode">
										<option value="">Select Tax</option>
										<?php
											foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
												echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Purchase Order No Tax Code</label>
								<div class="marginStyle marginStyle2">
									<select name="data[PurchaseNoTaxCode]" class="form-control PurchaseNoTaxCode">
										<option value="">Select Tax</option>
										<?php
											foreach($data['getAllTax'][$row['xeroAccountId']] as $getAllTax){
												echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Send Total Tax as Line Item</label>
								<div class="marginStyle">
									<select name="data[SendTaxAsLineItem]" class="form-control SendTaxAsLineItem">
										<option value="">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Tax Item Line Nominal Code</label>
								<div class="marginStyle">
									<select name="data[TaxItemLineNominal]" class="form-control TaxItemLineNominal">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default Price Lists</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Product Purchase Price List</label>
								<div class="marginStyle">
									<select name="data[productCostPriceList]"  class="form-control productCostPriceList">
										<option value="">Select Price List</option>
										<?php
											foreach ($data['pricelist'][$account1Id] as $pricelist) {
												echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Product Sell Price List</label>
								<div class="marginStyle">
									<select name="data[productRetailPriceList]"  class="form-control productRetailPriceList">
										<option value="">Select Price List</option>
										<?php
											foreach($data['pricelist'][$account1Id] as $pricelist){
												echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
			</div>
		
			<div class="tab-pane" id="defaultsItemsNominals">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-4">Default Items Codes/Identify Nominal Codes</legend>
					</div>
					<div class="row">
						<div class="col-md-1 itemHeading"><label class="control-label">Item</label></div>
						<div class="col-md-2 itemHeading"><label class="control-label">Xero Item Code</label></div>
						<div class="col-md-3 itemHeading"><label class="control-label">Xero Sales Account</label></div>
						<div class="col-md-3 itemHeading"><label class="control-label">Xero Purchase Account</label></div>
						<div class="col-md-2 itemHeading"><label class="control-label">Brightpearl Nominal</label></div>
						<div class="col-md-1 itemHeading"><label class="control-label">Created</label></div>
					</div>
					<hr />
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Shipping</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[shippingItem]" class="form-control shippingItem" type="text" placeholder="Enter Shipping Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[shippingSalesNominalXero]" class="form-control shippingSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[shippingPurchaseNominalXero]" class="form-control shippingPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForShipping]" class="form-control nominalCodeForShipping" type="text" placeholder="Enter Shipping Identify Nominal Code" /></div>
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['shippingItemCreated']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Gift Card</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[giftCardItem]" class="form-control giftCardItem" type="text" placeholder="Enter GiftCard Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[giftcardSalesNominalXero]" class="form-control giftcardSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[giftcardPurchaseNominalXero]" class="form-control giftcardPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForGiftCard]" class="form-control nominalCodeForGiftCard" type="text" placeholder="Enter GiftCard Identify Nominal Code" /></div>
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['giftcardItemCreated']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Coupon</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[couponItem]" class="form-control couponItem" type="text" placeholder="Enter Coupon Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[couponSalesNominalXero]" class="form-control couponSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[couponPurchaseNominalXero]" class="form-control couponPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForDiscount]" class="form-control nominalCodeForDiscount" type="text" placeholder="Enter Coupon Identify Nominal Code" /></div>
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['couponItemCreated']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Landed Cost</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[landedCostItem]" class="form-control landedCostItem" type="text" placeholder="Enter LandedCost Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[landedCostSalesNominalXero]" disabled class="form-control landedCostSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[landedCostPurchaseNominalXero]" class="form-control landedCostPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[nominalCodeForLandedCost]" class="form-control nominalCodeForLandedCost" type="text" placeholder="Enter LandedCOst Identify Nominal Code" /></div>
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['landedcostItemCreated']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Generic SKU</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[genericSku]" class="form-control genericSku" type="text" placeholder="Enter Generic SKU Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[genericSalesNominalXero]" class="form-control genericSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[genericPurchaseNominalXero]" class="form-control genericPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['GenericItemCreated']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Discount</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[discountItem]" class="form-control discountItem" type="text" placeholder="Enter Discount Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[discountSalesNominalXero]" class="form-control discountSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[discountPurchaseNominalXero]" class="form-control discountPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['discountItemCreaed']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">Round Off</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[roundOffItem]" class="form-control roundOffItem" type="text" placeholder="Enter RoundOff Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[roundoffSalesNominalXero]" class="form-control roundoffSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[roundoffPurchaseNominalXero]" class="form-control roundoffPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['roundoffItemCreated']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group itemHeading1">
								<label class="control-label marginStyle itemLabel">PurchaseCredit</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="marginStyle"><input name="data[PurchaseCreditItem]" class="form-control PurchaseCreditItem" type="text" placeholder="Enter Purchase Credit Item Code" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[purcreditSalesNominalXero]" disabled class="form-control purcreditSalesNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="marginStyle">
									<select name="data[purcreditPurchaseNominalXero]" class="form-control purcreditPurchaseNominalXero">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-1">
							<div class="form-group">
								<div class="marginStyle">
									<?php
										if($row['purcredititemCreated']){
									?>
										<span style="margin-left:20px;color:green"><i class="fa fa-check" aria-hidden="true"></i></span>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			
			<div class="tab-pane" id="amazonFees">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Amazon Fee</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fee Account</label>
								<div class="marginStyle">
									<select name="data[amazonfeeAccountRef]"  class="form-control amazonfeeAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<?php	if($this->globalConfig['enableAmazonFees']){	?>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fee (Other) Conatct ID</label>
								<div class="marginStyle"><input name="data[otherAmazonFeeContactId]" class="form-control otherAmazonFeeContactId" type="text" placeholder="Enter Amazon Fee (Other) Conatct ID" /></div>
							</div>
						</div>
						<?php	}	?>
						<div class="col-md-6"></div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Amazon Fee Channel</label>
								<div class="marginStyle">
									<select name="data[AmazonFeesChannels][]"  class="form-control AmazonFeesChannels"  multiple="true">
										<option value="">Select Channel</option>
										<?php
											foreach($data['channel'][$account1Id] as $channel){
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>				
						</div>
						<div class="col-md-9"></div>
					</div>
				</fieldset>
			</div>
		
			<div class="tab-pane" id="inventorySettings">
				<?php	if($this->globalConfig['enableInventoryManagement']){	?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Inventory Management</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Inventory Management Enabled</label>
								<div class="marginStyle">
									<select name="data[InventoryManagementEnabled]" class="form-control InventoryManagementEnabled">
										<option value="">Select</option>
										<option value="0">Yes</option>
										<option value="1">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">IM Product Purchase Nominal Code</label>
								<div class="marginStyle">
									<select name="data[InventoryManagementProductPurchaseNominalCode]" class="form-control InventoryManagementProductPurchaseNominalCode">
										<option value="">Select Nominal</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">IM Product Sell Nominal Code</label>
								<div class="marginStyle">
									<select name="data[InventoryManagementProductSellNominalCode]" class="form-control InventoryManagementProductSellNominalCode">
										<option value="">Select Nominal</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Disable SKU Details On Documents</label>
								<div class="marginStyle">
									<select name="data[disableSkuDetails]" class="form-control disableSkuDetails">
										<option value="">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Bundle Component Suppression</label>
								<div class="marginStyle">
									<select name="data[BundleSuppression]" data-required="0" class="form-control BundleSuppression">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select> 
								</div> 
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
				<?php	}	?>
				<?php	if($this->globalConfig['enableInventoryTransfer']){		?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Inventory Transfer</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Account</label>
								<div class="marginStyle">
									<select name="data[InventoryTransferSalesAccountRef]" class="form-control InventoryTransferSalesAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Purchase Account</label>
								<div class="marginStyle">
									<select name="data[InventoryTransferPurchaseAccountRef]" class="form-control InventoryTransferPurchaseAccountRef">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Inter Company Supplier</label>
								<div class="marginStyle"><input name="data[InterCoSupplier]" class="form-control InterCoSupplier" type="text" placeholder="Enter Xero Inventory Transfer Supplier ID" /></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Inter Company Customer</label>
								<div class="marginStyle marginStyle2"><input name="data[InterCoCustomer]" class="form-control InterCoCustomer" type="text" placeholder="Enter Xero Inventory Transfer Customer ID" /></div>
							</div>
						</div>
					</div>
				</fieldset>
				<?php	}	?>
				
				<?php	if($this->globalConfig['enableSAConsol']){		?>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Stock Adjustment Consolidation</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Nominal For Positive Stock</label>
								<div class="marginStyle">
									<select name="data[positiveStockAdjustment]" data-required="0" class="form-control positiveStockAdjustment">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Nominal For Negative Stock</label>
								<div class="marginStyle">
									<select name="data[negativeStockAdjustment]" data-required="0" class="form-control negativeStockAdjustment">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Nominal For Inventory Line</label>
								<div class="marginStyle">
									<select name="data[inventoryLineStockAdjustment]" data-required="0" class="form-control inventoryLineStockAdjustment">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						
					</div>
				</fieldset>
				<?php	}	?>
			</div>
		
			<div class="tab-pane" id="defaultCOGSAccounts">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Default COGS Accounts</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order (GO) - Credit Account</label>
								<div class="marginStyle">
									<select name="data[COGSCreditNominalSO]"  class="form-control COGSCreditNominalSO">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Order (GO) - Debit Account</label>
								<div class="marginStyle">
									<select name="data[COGSDebitNominalSO]"  class="form-control COGSDebitNominalSO">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>	
					</div>	
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Credit (SG) - Credit Account</label>
								<div class="marginStyle">
									<select name="data[COGSCreditNominalSC]"  class="form-control COGSCreditNominalSC">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Credit (SG) - Debit Account</label>
								<div class="marginStyle">
									<select name="data[COGSDebitNominalSC]"  class="form-control COGSDebitNominalSC">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Sales Credit (PG) - Credit Account</label>
								<div class="marginStyle">
									<select name="data[COGSCreditNominalSCPG]"  class="form-control COGSCreditNominalSCPG">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2">Sales Credit (PG) - Debit Account</label>
								<div class="marginStyle marginStyle2">
									<select name="data[COGSDebitNominalSCPG]"  class="form-control COGSDebitNominalSCPG">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>				
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Purchase Order (GO) - Default Account</label>
								<div class="marginStyle">
									<select name="data[DefaultCOGSforPurchase]"  class="form-control DefaultCOGSforPurchase">
										<option value="">Select Account</option>
										<?php
											foreach($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef){
												echo '<option value="'.$IncomeAccountRef['id'].'">'.$IncomeAccountRef['id'].' - '.ucwords($IncomeAccountRef['name']).'</option>';
											}
										?>
									</select> 
								</div>
							</div>
						</div>
						<div class="col-md-9"></div>
					</div>
				</fieldset>
			</div>
			
			<div class="tab-pane" id="otherSettings">
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Reference & Notes Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Reference On Invoice/Credit Note</label>
								<div class="marginStyle">
									<select name="data[TakeAsRefrence]" class="form-control TakeAsRefrence">
										<option value="">Select Any Option</option>
										<option value="BrightpearlCustomerReference">Brightpearl Customer Ref. No.</option>
										<option value="BrightpearlOrderID">Brightpearl Order ID</option>
										<?php
											foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
												echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle"><span aria-required="true"></span>Reference On SO/SC Payments</label>
								<div class="marginStyle">
									<select name="data[useRefOnPayments]" data-required="0" class="form-control useRefOnPayments">
										<option value="">Select</option>
										<option value="Reference">Reference (On Payments)</option>
										<option value="Transactionref">Transaction Ref (On Payments)</option>
										<?php
											foreach($account1Fieldconfigso as $fields => $account1Fieldconfigsos){
												echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigsos['name']).'</option>';
											}
										?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle"><span aria-required="true"></span>Notes On Bill/Vendor Credit</label>
								<div class="marginStyle">
									<select name="data[notesOnPurchaseInvoice][]" class="form-control notesOnPurchaseInvoice" multiple="true">
										<option value="">Select</option>
										<?php
											foreach($account1Fieldconfigpo as $fields => $account1Fieldconfigpos){
												echo '<option value="'.$fields.'">'.ucwords($account1Fieldconfigpos['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle marginStyle2"><span aria-required="true"></span>Prefix On Notes & History</label>
								<div class="marginStyle marginStyle2"><input name="data[PrefixForNotes]" class="form-control PrefixForNotes" type="text" placeholder="Enter Notes & History Prefix" /></div>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Discount Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Override Discount Item Nominal Code</label>
								<div class="marginStyle">
									<select name="data[OverrideDiscountItemAccRef]" class="form-control OverrideDiscountItemAccRef">
										<option value="">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<!--<label class="control-label marginStyle">Discount Format (If SKU Details ON)(Only Applicable on Invoice)</label> -->
								<label class="control-label marginStyle">Invoice Discount Format (If SKU Details ON)</label>
								<div class="marginStyle">
									<select name="data[discountPercentFormat]" class="form-control discountPercentFormat">
										<option value="0">Select</option>
										<option value="1">As a Separate Line Item</option>
										<option value="2">As a Percentage within the Line Item</option>
										<option value="3">Send Net Amount on Discount Line Item</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Channel & Warehouse</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Channel</label>
								<div class="marginStyle">
									<select name="data[channelIds][]"  class="form-control channelIds"  multiple="true">
										<option value="">Select Channel</option>
										<option value="NullChannel">Blank Channel</option>
										<?php
											foreach($data['channel'][$account1Id] as $channel){
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Default Warehouse</label>
								<div class="marginStyle">
									<select name="data[warehouses][]" class="form-control warehouses" multiple="true">
										<option value="">Select Warehouse</option>
										<?php
											foreach($data['warehouse'][$account1Id] as $warehouse){
												echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<?php	if($this->globalConfig['enablePrepayments']){	?>
							<div class="form-group">
								<label class="control-label marginStyle">Channels for Pre-Payments</label>
								<div class="marginStyle">
									<select name="data[channelForAdvance][]"  class="form-control channelForAdvance"  multiple="true">
										<option value="">Select Channel</option>
										<option value="NullChannel">Blank Channel</option>
										<?php
											foreach($data['channel'][$account1Id] as $channel){
												echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
											}
										?>
									</select>
								</div>
							</div>
							<?php	}	?>
						</div>
						<div class="col-md-3"></div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Additional Fetch Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Custom Field Name (Sales)</label>
								<div class="marginStyle">
									<input name="data[customFieldForSalesFetch]" class="form-control customFieldForSalesFetch" type="text" placeholder="Enter Custom Field Name" />
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Custom Field Values (Sales)</label>
								<div class="marginStyle">
									<input name="data[customFieldForSalesFetchValue]" class="form-control customFieldForSalesFetchValue" type="text" placeholder="Enter Custom Field Values" />
									<span class="help-block"> (Use || For Multiple Values)</span>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<legend class="legendstyle col-md-3">Purchase Settings</legend>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Send Purchase As :</label>
								<div class="marginStyle">
									<select name="data[sendPurchaseAs]"  class="form-control sendPurchaseAs">
										<option value="1">AUTHORISED (Approved)</option>
										<option value="2">DRAFT</option>
										<option value="3">SUBMITTED (Waiting For Approval)</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Enable Purchase Consol (Batch Invoice)</label>
								<div class="marginStyle">
									<select name="data[enablePurchaseConsol]"  class="form-control enablePurchaseConsol">
										<option value="0">Select</option>
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label marginStyle">Purchase Consol On</label>
								<div class="marginStyle">
									<select name="data[purchaseConsolBasedON]" class="form-control purchaseConsolBasedON">
										<option value="">Select</option>
										<option value="supplierID">Supplier ID</option>
										<option value="suppplierAccountCode">Supplier Account Code</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
<?php
	}
?>
</div>
<?php 
	}
	else{
?>
<div class="page-content-wrapper">
	<div class="page-content">
	<div class="portlet ">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-exclamation-triangle"></i>You Do not have permission to access this page.</div>
		</div>
	</div>
</div>
<?php
	}
?>