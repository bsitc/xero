<?php
//xero
$clientcode			= $this->config->item('clientcode');
$librariesNames		= $this->config->item('librariesNames');
$mappings			= $this->config->item('mapping');
$fieldmappings		= $this->config->item('fieldmapping');
$userLoginData		= $this->session->userdata('login_user_data');
$accessRoles		= array('admin', 'developer', '1');
$accessRoles1		= array('developer');

$tooltipTitle		= array(
	"OrderID"			=> array('Sales', 'SalesCredit','Purchase', 'PurchaseCredit'),
	"JournalID"			=> array('COGS', 'AmazonFee','StockJournal'),
	"GoodsMovementID"	=> array('StockAdjustment'),
	"CustomerID"		=> array('Customer'),
	"ProductID"			=> array('ProductID'),
);
$allAccount2NameArr	= array();
$allAccount2Names	= $this->db->get_where('account_'.$data['globalConfig']['account2Liberary'].'_account')->result_array();
foreach($allAccount2Names as $allAccount2NamesTemp){			
	$allAccount2NameArr[$allAccount2NamesTemp['id']]	= $allAccount2NamesTemp['name'];
}
?>
<head>
	<link href="<?php echo base_url(); ?>/extracss/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>/extracss/bootstrap-toggle.min.js"></script>
</head>
<style>
	.vl {
		border-left: 4px solid #26284A;
		height: 500px;
		position: absolute;
		left: 50%;
		margin-left: -3px;
		top: 0;
	}
	.form-horizontal .control-label{
		text-align:left !important;
	}
	.bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-default, .bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-default{
		font-size:12px !important;
	}
	.headerpin{
		background-color: #26284a!important;
		color: white!important;
		line-height: 1.1!important;
		width: 70%!important;
		border-top-right-radius:50px!important;
		border-bottom-right-radius:50px!important;
		padding:2px 25%  2px 10px!important;
		margin-left:-10px!important;
		cursor: pointer;
		font-size: 18px!important;
		margin-top:10px!important;
	}
	.submitAction{
		min-width:120px!important;
		line-height: 2.4rem !important;
	}
	.headerpin:hover{
		background-color: #FC664A !important;
	}
	.headerpin .fa{
		font-size: normal!important;
		font-weight: lighter!important;
	}
	.password-policies {
        list-style-type: none;
        padding-left: 0px;
    }
    .password-policies li {
        padding: 5px 25px;
        color: #ccc;
        background-image: url('<?php echo base_url(); ?>/extracss/inactive.svg');
        background-position: 0 center;
        background-size: 20px 20px;
        background-repeat: no-repeat;
    }
    .password-policies li.active {
        color: #23b08d;
        background-image: url('<?php echo base_url(); ?>/extracss/active.svg');
        background-position: 0 center;
        background-size: 20px 20px;
        background-repeat: no-repeat;
    }
	.strength {
        position: relative;
        height: 8px;
        margin: 10px 0 5px;
        border-radius: 4px;
        background-color: #e1e5ec;
        transition: .5s;
    }
    .percentage {
        position: absolute;
        height: 8px;
        border-radius: 4px;
        background-color: #23b08d;
        content: '';
    }
	.strengthmsg {
        padding-right: 5px;
        color: #3d3935;
        font-size: 1.2rem;
        float: right;
    }
	
</style>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li><a href="<?php echo base_url();?>dashboard">Home</a><i class="fa fa-circle"></i></li>
				<li><span>Profile</span></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="profile-content">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light ">
								<div class="portlet-title tabbable-line">
									<div class="caption caption-md">
										<i class="icon-globe theme-font hide"></i>
									</div>
									<ul class="nav nav-tabs">
										<li class="hide"><a href="#tab_1_1" data-toggle="tab"><b>PERSONAL INFO</b></a></li>
										<li class="hide"><a href="#tab_1_2" data-toggle="tab"><b>CHANGE AVATAR</b></a></li>
										<li class="active"><a href="#tab_1_3" data-toggle="tab"><b>CHANGE PASSWORD</b></a></li>
										<?php	if(in_array($userLoginData['role'], $accessRoles)){	?>
										<li><a href="#tab_1_4" data-toggle="tab"><b>CONNECTOR CONFIGURATION</b></a></li>
										<li><a href="#tab_1_6" data-toggle="tab"><b>TOOLS</b></a></li>
										<li><a href="#tab_1_7" data-toggle="tab"><b>SET POSTING DATES</b></a></li>
										<?php	}	?>
										
										<?php	if((!in_array($userLoginData['role'], $accessRoles)) AND (($clientcode == 'hawkeopticsxero') OR ($clientcode == 'hawkeusxero'))){	?>
										<li><a href="#tab_1_6" data-toggle="tab">Tools</a></li>
										<?php	}	?>
										<?php	if(in_array($userLoginData['role'], $accessRoles1)){		?>
										<li><a href="#tab_1_5" data-toggle="tab"><b>AUTOMATION SETTINGS</b></a></li>
										<?php	}	?>
									</ul>
								</div>
								<div class="portlet-body">
									<div class="tab-content">
										<div class="tab-pane hide" id="tab_1_5">
											<form role="form" action="<?php echo base_url('users/profile/saveAutomationsetup');?>">
												<div class="alert alert-success hide">      
													<strong>Info!</strong> Data saved successfully.
												</div>
												<div class="row">
													<div class="form-group col-md-3">
														<label class="control-label">Product Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="productSync" value="1" <?php echo ($data['automationConfig']['productSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">Customer Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="customerSync" value="1" <?php echo ($data['automationConfig']['customerSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">SalesOrder Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="salesOrderSync" value="1" <?php echo ($data['automationConfig']['salesOrderSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">SalesCredit Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="salesCreditSync" value="1" <?php echo ($data['automationConfig']['salesCreditSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
												</div>
												<div class="row">
													<div class="form-group col-md-3">
														<label class="control-label">PurchaseOrder Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="purchaseOrderSync" value="1" <?php echo ($data['automationConfig']['purchaseOrderSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">PurchaseCredit Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="purchaseCreditSync" value="1" <?php echo ($data['automationConfig']['purchaseCreditSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">StockAdjustment Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="stockAdjustmentSync" value="1" <?php echo ($data['automationConfig']['stockAdjustmentSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">AmazonFees Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="amazonFeesSync" value="1" <?php echo ($data['automationConfig']['amazonFeesSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
												</div>
												<div class="row">
													<div class="form-group col-md-3">
														<label class="control-label">CogsJournal Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="cogsJournalSync" value="1" <?php echo ($data['automationConfig']['cogsJournalSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">SalesOrder_Consol Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="salesOrderConsolSync" value="1" <?php echo ($data['automationConfig']['salesOrderConsolSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">SalesCredit_Consol Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="salesCreditConsolSync" value="1" <?php echo ($data['automationConfig']['salesCreditConsolSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-3">
														<label class="control-label">CogsJournal_Consol Sync</label>
														<div class="class=form-control">
															<input type="checkbox" name="cogsJournalConsolSync" value="1" <?php echo ($data['automationConfig']['cogsJournalConsolSync'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
												</div>
												<div class="row">
													<div class="form-group col-md-3">
														<label class="control-label">Disable Automation</label>
														<div class="class=form-control">
															<input type="checkbox" name="disableAutomation" value="1" <?php echo ($data['automationConfig']['disableAutomation'])?('checked="checked"'):('');?> class="switchbtn" />
														</div> 
													</div>
													<div class="form-group col-md-9"></div>
												</div>
												<div class="margin-top-10">
													<a href="javascript:;" class="btn savebtnCss savefrombtn">Save Changes<img style="display:none;width: 30px" src="<?php echo base_url('assets/layouts/layout/img/ajax-loader-1.gif');?>"> </a>
													<a href="javascript:;" class="btn default">Cancel</a>
												</div>
											</form>
										</div>
										<div class="tab-pane hide" id="tab_1_1">
											<form role="form" action="<?php echo base_url('users/profile/saveBasic');?>">
												<div class="alert alert-success hide">
													<strong>Info!</strong> Data saved successfully.
												</div>
												<div class="form-group">
													<label class="control-label">First Name</label>
													<input type="text" name="firstname" value="<?php echo $data['user']['firstname'];?>" placeholder="First Name" class="form-control" />
												</div>
												<div class="form-group">
													<label class="control-label">Last Name</label>
													<input type="text" placeholder="Last Name" name="lastname" value="<?php echo $data['user']['lastname'];?>"  class="form-control" />
												</div>
												<div class="form-group">
													<label class="control-label">Mobile Number</label>
													<input type="text" placeholder="Mobile Number" name="phone" value="<?php echo $data['user']['phone'];?>"  class="form-control" />
												</div>
												<div class="form-group">
													<label class="control-label">Email</label>
													<input type="email" placeholder="Email" name="email" value="<?php echo $data['user']['email'];?>"  class="form-control" /> 
												</div>
												<div class="margiv-top-10">
													<a href="javascript:;" class="btn savefrombtn savebtnCss"> <span class="text">Save Changes</span> <img style="display:none;width: 30px" src="<?php echo $this->config->item('script_url').'assets/layouts/layout/img/ajax-loader-1.gif';?>"> </a>
													<a href="javascript:;" class="btn default">Cancel</a>
												</div>
											</form>
										</div>
										<div class="tab-pane" id="tab_1_7">
											<form role="form" action="<?php echo base_url('users/profile/saveDatesDatas');?>">
												<div class="alert alert-success hide"><strong>Info!</strong> Data saved successfully.</div>
												<div class="row">
													<div class="col-md-2"><label class="control-label"><b>Type</b></label></div>
													<div class="col-md-2"><label class="control-label"><b>Date</b></label></div>
													<div class="col-md-2"><label class="control-label"><b>Condition</b></label></div>
												</div>
												</br>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Sales</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="sales" value="<?php echo $data['postingDates']['sales'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="salesCondition" class="form-control" value="<?php echo $data['postingDates']['salesCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Sales Credit</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="salescredit" value="<?php echo $data['postingDates']['salescredit'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="salescreditCondition" class="form-control" value="<?php echo $data['postingDates']['salescreditCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Consol Sales</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="consolsales" value="<?php echo $data['postingDates']['consolsales'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="consolsalesCondition" class="form-control" value="<?php echo $data['postingDates']['consolsalesCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Consol Sales Credit</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="consolsalescredit" value="<?php echo $data['postingDates']['salescredit'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="consolsalescreditCondition" class="form-control" value="<?php echo $data['postingDates']['salescreditCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Purchase</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="purchase" value="<?php echo $data['postingDates']['purchase'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="purchaseCondition" class="form-control" value="<?php echo $data['postingDates']['purchaseCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Purchase Credit</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="purchaseCredit" value="<?php echo $data['postingDates']['purchaseCredit'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="purchaseCreditCondition" class="form-control" value="<?php echo $data['postingDates']['purchaseCreditCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">COGS</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="cogs" value="<?php echo $data['postingDates']['cogs'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="cogsCondition" class="form-control" value="<?php echo $data['postingDates']['cogsCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Consol COGS</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="consolcogs" value="<?php echo $data['postingDates']['consolcogs'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="consolcogsCondition" class="form-control" value="<?php echo $data['postingDates']['consolcogsCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-2">
														<div class="form-group">
															<label class="control-label">Stocks</label>
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<input type="date" name="stocks" value="<?php echo $data['postingDates']['stocks'];?>" placeholder="" class="form-control" />
														</div>
													</div>
													<div class="col-md-2">
														<div class="form-group">
															<select name="stocksCondition" class="form-control" value="<?php echo $data['postingDates']['stocksCondition'];?>">
																<option value="<="><=</option>
																<option value=">=">>=</option>
																<option value="=">=</option>
															</select>
														</div>
													</div>
												</div>
												<div class="margiv-top-10">
													<a href="javascript:;" class="btn savefrombtn savebtnCss"> <span class="text">Save Changes</span> <img style="display:none;width: 30px" src="<?php echo $this->config->item('script_url').'assets/layouts/layout/img/ajax-loader-1.gif';?>"> </a>
													<a href="javascript:;" class="btn default">Cancel</a>
												</div>
											</form>
										</div>
										<div class="tab-pane hide" id="tab_1_2">
											<p>Update profile picture</p>
											<form action="<?php echo base_url('users/profile/updateProfilePic');?>" role="form" enctype="multipart/form-data">
												<div class="alert alert-success hide">
													<strong>Info!</strong> Data saved successfully.
												</div>
												<div class="form-group">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
															<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
														</div>
														<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
														<div>
															<span class="btn default btn-file">
																<span class="fileinput-new"> Select image</span>
																<span class="fileinput-exists">Change</span>
																<input type="file" name="..." />
															</span>
															<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">Remove</a>
														</div>
													</div>
												</div>
												<div class="margin-top-10">
													<a href="javascript:;" class="btn savebtnCss savefrombtn">Submit<img style="display:none;width: 30px" src="<?php echo $this->config->item('script_url') .'assets/layouts/layout/img/ajax-loader-1.gif';?>" /></a>
													<a href="javascript:;" class="btn default">Cancel</a>
												</div>
											</form>
										</div>
										<div class="tab-pane active" id="tab_1_3">
											<div class="row">
												<div class="col-md-6">
													<form role="form" action="<?php echo base_url('users/profile/updatePassword');?>">
														<div class="alert alert-success hide">      
															<strong>Info!</strong> Data saved successfully.
														</div>
														<div class="row">
															<div class="col-md-8">
																<div class="form-group">
																	<label class="control-label">Username</label>
																	<input type="text" disabled name="username" value="<?php echo $user_session_data['username'];?>" class="form-control" />
																</div>
																<div class="form-group password-container">
																	<label class="control-label">Current Password</label>
																	<input type="password" id="currentpassword" name="password" class="form-control" />
																	<i class="toggle-password fa fa-eye" id="togglePassword1"></i>
																</div>
																<div class="form-group password-container">
																	<label class="control-label">New Password</label>
																	<input type="password" name="newpassword" onkeyup="validatePassword()" id="newpassword" class="form-control" />
																	<i class="toggle-password fa fa-eye" id="togglePassword2"></i>
																	<div class="strength"><span class="percentage"></span></div>
																	<span id="strengthmsg" class="strengthmsg" style="display: none;">Poor</span>
																</div>
																<div class="form-group password-container">
																	<label class="control-label">Confirm New Password</label>
																	<input type="password" id="newpassword2" name="newpassword2" class="form-control" />
																	<i class="toggle-password fa fa-eye" id="togglePassword3"></i>
																</div>
																<div class="margin-top-10">
																	<a href="javascript:;" class="btn savebtnCss savefrombtn">Change Password<img style="display:none;width: 30px" src="<?php echo base_url('assets/layouts/layout/img/ajax-loader-1.gif');?>"> </a>
																</div>
															</div>
															<div class="col-md-4">
																<h4 style="margin-top: 100px;">Password Policy</h4>
																<ul class="password-policies">
																	<li class="policy-lowercase">
																		Lowercase
																	</li>
																	<li class="policy-uppercase">Uppercase</li>
																	<li class="policy-numbers">Numbers</li>
																	<li class="policy-symbols">Symbols</li>
																	<li class="policy-length">At Least 16 Characters</li>
																</ul>
															</div>
														</div>
													</form>
												</div>
											</div>				
										</div>
										<div class="tab-pane" id="tab_1_4">
											<form role="form" action="<?php echo base_url('users/profile/saveGlobalConfig');?>">
												<div class="alert alert-success hide"><strong>Info!</strong> Data saved successfully.</div>
												<div class="row">
													<div class="col-md-6" style="border-left: 4px solid #26284A;">
														<div class="row"><legend class="legendstyle col-md-3">General</legend></div>
														<div class="form-group">
															<label class="control-label">Application Name</label>
															<input type="text" name="app_name" value="<?php echo $data['globalConfig']['app_name'];?>" class="form-control" />
														</div>
														<div class="row enableAccountSettings">
															<div class="form-group col-md-6">
																<label for="account1Name">Account 1 Name</label>
																<input readonly type="text" name="account1Name" value="<?php echo $data['globalConfig']['account1Name'];?>" class="form-control" />
															</div>
															<div class="form-group col-md-6">
																<label for="account1Liberary">Account 1 Library</label>
																<input readonly type="text" name="account1Liberary" value="<?php echo $data['globalConfig']['account1Liberary'];?>" class="form-control" />
															</div>
														</div>
														<div class="row enableAccountSettings">
															<div class="form-group col-md-6">
																<label for="account2Name">Account 2 Name</label>
																<input readonly type="text" name="account2Name" value="<?php echo $data['globalConfig']['account2Name'];?>" class="form-control" />
															</div>
															<div class="form-group col-md-6">
																<label for="account2Liberary">Account 2 Library</label>
																<input readonly type="text" name="account2Liberary" value="<?php echo $data['globalConfig']['account2Liberary'];?>" class="form-control" />
															</div>
														</div>
													</div>
													<div class="col-md-6" style="border-left: 4px solid #26284A;">
														<div class="row"><legend class="legendstyle col-md-3">Mappings</legend></div>
														<div class="row">
														<?php	foreach($mappings as $mapKey => $mapping){	?>
															<div class="form-group col-md-3">
																<label for="enable<?php echo ucwords($mapKey);?>Mapping"><?php echo $mapping;?></label>
																<input type="checkbox" data-size="mini" data-toggle="toggle" name="enable<?php echo ucwords($mapKey);?>Mapping" value="1" <?php echo ($data['globalConfig']['enable'.ucwords($mapKey).'Mapping'])?('checked="checked"'):('');?> class="switchbtn"  />
															</div>
														<?php	}	?>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6" style="border-left: 4px solid #26284A;">
														<div class="row"><legend class="legendstyle col-md-3">Documents</legend></div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Inventory Transfer</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableInventoryTransfer" value="1" <?php echo ($data['globalConfig']['enableInventoryTransfer'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Inventory Management</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableInventoryManagement" value="1" <?php echo ($data['globalConfig']['enableInventoryManagement'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">COGS Journal</label>
																<div class="class="form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableCOGSJournals"  id="enableCOGSJournals" value="1" <?php echo ($data['globalConfig']['enableCOGSJournals'])?('checked="checked"'):('');?> class="switchbtn enableCOGSJournals"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">COGS w/o Invoicing</label>
																<div class="class="form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableCOGSWOInvoice" value="1" <?php echo ($data['globalConfig']['enableCOGSWOInvoice'])?('checked="checked"'):('');?> class="switchbtn enableCOGSWOInvoice"  />
																</div>
															</div>
															
														</div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Amazon Fee</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableAmazonFees" value="1" <?php echo ($data['globalConfig']['enableAmazonFees'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Amazon Fee (Other)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini"  data-toggle="toggle" name="enableAmazonFeeOther" value="1" <?php echo ($data['globalConfig']['enableAmazonFeeOther'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Customer Advances</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini"  data-toggle="toggle" name="enablePrepayments" value="1" <?php echo ($data['globalConfig']['enablePrepayments'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Product</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableProduct" value="1" <?php echo ($data['globalConfig']['enableProduct'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Customer</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableCustomer" value="1" <?php echo ($data['globalConfig']['enableCustomer'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Sales Order</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableSalesOrder" value="1" <?php echo ($data['globalConfig']['enableSalesOrder'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Sales Credit</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableSalesCredit" value="1" <?php echo ($data['globalConfig']['enableSalesCredit'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Purchase Order</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enablePurchaseOrder" value="1" <?php echo ($data['globalConfig']['enablePurchaseOrder'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Purchase Credit</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enablePurchaseCredit" value="1" <?php echo ($data['globalConfig']['enablePurchaseCredit'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Stock Adjustment</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableStockAdjustment" value="1" <?php echo ($data['globalConfig']['enableStockAdjustment'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Consol Stock Journal</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableConsolStockAdjustment" value="1" <?php echo ($data['globalConfig']['enableConsolStockAdjustment'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">GRNI Journal</label>
																<div class="class="form-control">
																	<input type="checkbox"  data-size="mini" data-toggle="toggle" name="enableGRNIjournal" value="1" <?php echo ($data['globalConfig']['enableGRNIjournal'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-6" style="border-left: 4px solid #26284A;">
														<div class="row"><legend class="legendstyle col-md-3">Advance Features</legend></div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Generic Customer Mapping Cust.</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableGenericCustomerMappingCustomazation" value="1" <?php echo ($data['globalConfig']['enableGenericCustomerMappingCustomazation'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Canadian Tax Mapping</label>
																<div class="class="form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableAdvanceTaxMapping" value="1" <?php echo ($data['globalConfig']['enableAdvanceTaxMapping'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3 enableAggregation">
																<label for="enableNetOffConsol">Netoff Consol SO/SC</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableNetOffConsol" value="1" <?php echo ($data['globalConfig']['enableNetOffConsol'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label for="enableSAConsol">Consol Stock Adjustment</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableSAConsol" value="1" <?php echo ($data['globalConfig']['enableSAConsol'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label"><b>Consolidation</b></label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableAggregation" value="1" <?php echo ($data['globalConfig']['enableAggregation'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3 enableAggregation">
																<label for="enableAggregationAdvance">Consol on Custom Field</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableAggregationAdvance" value="1" <?php echo ($data['globalConfig']['enableAggregationAdvance'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3 enableAggregation">
																<label for="enableAggregationOnAPIfields">Consol on API Field</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableAggregationOnAPIfields" value="1" <?php echo ($data['globalConfig']['enableAggregationOnAPIfields'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3 enableAggregation">
																<label for="enableConsolidationMappingCustomazation">Consol Cust. on Custom Field (Exclude)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="enableConsolidationMappingCustomazation" value="1" <?php echo ($data['globalConfig']['enableConsolidationMappingCustomazation'])?(	'checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6" style="border-left: 4px solid #26284A;">
														<div class="row"><legend class="legendstyle col-md-3">Disable Payments</legend></div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Brightpearl => Xero (SO)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="disableSOpaymentbptoqbo" value="1" <?php echo ($data['globalConfig']['disableSOpaymentbptoqbo'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Xero => Brightpearl (SO)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="disableSOpaymentqbotobp" value="1" <?php echo ($data['globalConfig']['disableSOpaymentqbotobp'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Brightpearl => Xero (SC)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="disableSCpaymentbptoqbo" value="1" <?php echo ($data['globalConfig']['disableSCpaymentbptoqbo'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Xero => Brightpearl (SC)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="disableSCpaymentqbotobp" value="1" <?php echo ($data['globalConfig']['disableSCpaymentqbotobp'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
														</div>
														<div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Consol Payment SO</label>
																<div class="class=form-control">
																	<input type="checkbox"  data-size="mini" data-toggle="toggle" name="disableConsolSOpayments" value="1" <?php echo ($data['globalConfig']['disableConsolSOpayments'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Consol Payment SC</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="disableConsolSCpayments" value="1" <?php echo ($data['globalConfig']['disableConsolSCpayments'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Xero => Brightpearl (PO)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="disablePOpaymentqbotobp" value="1" <?php echo ($data['globalConfig']['disablePOpaymentqbotobp'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Xero => Brightpearl (PC)</label>
																<div class="class=form-control">
																	<input type="checkbox" data-size="mini" data-toggle="toggle" name="disablePCpaymentqbotobp" value="1" <?php echo ($data['globalConfig']['disablePCpaymentqbotobp'])?('checked="checked"'):('');?> class="switchbtn"  />
																</div> 
															</div>
														</div>
													</div>
													<div class="col-md-6" style="border-left: 4px solid #26284A;">
														<div class="row"><legend class="legendstyle col-md-3">Archive Settings</legend></div>
														<div class="row">
															<div class="form-group col-md-12">
																<label class="control-label">Email Alerts Receipents</label>
																<div class="class=form-control">
																	<textarea rows="4" placeholder="Enter Email Address" name="emailReceipents" class="form-control"  ><?php echo $data['globalConfig']['emailReceipents'];?></textarea>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-md-6">
																<label for="autoArchivePeriod">Select Document Archive Duration</label><span>(default value : 90 days)</span>
																<select name="autoArchivePeriod" class="form-control" value="<?php echo $data['globalConfig']['autoArchivePeriod']; ?>">
																	<option value="0">Select</option>
																	<option value="30">30 Days</option>
																	<option value="45">45 Days</option>
																	<option value="60">60 Days</option>
																	<option value="75">75 Days</option>
																	<option value="90">90 Days</option>
																	<option value="105">105 Days</option>
																	<option value="120">120 Days</option>
																	<option value="135">135 Days</option>
																	<option value="150">150 Days</option>
																	<option value="165">165 Days</option>
																	<option value="180">180 Days</option>
																</select>
															</div>
															<div class="form-group col-md-6">
																<label for="archiveType">Select Archive Document Type</label>
																<select name="archiveType[]"  multiple="multiple"   class="form-control">
																<?php
																$archiveType = array(
																	''					=>'Select',
																	'so'				=>'Sales Order',
																	'po'				=>'Purchase Order',
																	'sc'				=>'Sales Credit',
																	'pc'				=>'Purchase Credit',
																	'cogs'				=>'Cogs Journal', 
																	'amazon'			=>'Amazon Fee',
																	'amazonfeeother'	=>'Amazon Fee Other',
																	'stock'				=>'Stock Journal/Adjustment',
																);
																$savearchiveType = explode(",",$data['globalConfig']['archiveType']);
																foreach ($archiveType as $id => $archiveTypes) {
																	$selected = (in_array($id, $savearchiveType)) ? ('selected="selected"') :('');
																	echo '<option value="'.$id.'" '.$selected.'>'.$archiveTypes.'</option>';
																}
																?>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="margin-top-10">
													<a href="javascript:;" class="btn savebtnCss savefrombtn">Save Changes
													<img style="display:none;width: 30px" src="<?php echo $this->config->item('script_url').'assets/layouts/layout/img/ajax-loader-1.gif';?>">
													</a>
												</div>
											</form>
										</div>
										<?php require_once('toolsview.php');	?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="<?php echo $this->config->item('script_url');?>assets/pages/css/profile.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->config->item('script_url');?>assets/pages/scripts/profile.js" type="text/javascript"></script>
<script>
	document.getElementById('togglePassword1').addEventListener('click', function() {
		var passwordField1	= document.getElementById('currentpassword');
		var type			= passwordField1.getAttribute('type') === 'password' ? 'text' : 'password';
		passwordField1.setAttribute('type', type);
	});
	document.getElementById('togglePassword2').addEventListener('click', function() {
		var passwordField2	= document.getElementById('newpassword');
		var type			= passwordField2.getAttribute('type') === 'password' ? 'text' : 'password';
		passwordField2.setAttribute('type', type);
	});
	document.getElementById('togglePassword3').addEventListener('click', function() {
		var passwordField3	= document.getElementById('newpassword2');
		var type			= passwordField3.getAttribute('type') === 'password' ? 'text' : 'password';
		passwordField3.setAttribute('type', type);
	});
	 $(".enableCOGSWOInvoice").change(function() {
		if(this.checked) {
			var enableCogs = 	$('#enableCOGSJournals').is(':checked'); 
			if(enableCogs==true){
				 if (confirm("COGS with invoicing is already enabled. Please disable that to continue.") == true) {
					// $('#enableCOGSJournals').trigger('click'); 
					 $('#enableCOGSJournals').prop('checked', false).change()
					 $( ".enableCOGSWOInvoice" ).prop( "checked", true );
					   //$("input[name=enableCOGSJournals]").attr('checked', false);
				 }else{
					  $( ".enableCOGSWOInvoice" ).prop( "checked", false ).change()
				 }
			}
		}
	});
	function validatePassword(){
		var status				= false;
		var newpassword			= document.getElementById("newpassword").value;
		var strengthmsg			= document.getElementById("strengthmsg");
		
		var patternLowercase	= /[a-z]/;
		var patternUppercase	= /[A-Z]/;
		var patternNumbers		= /[\d]/;
		var patternSymbols		= /[!-/:-@[-`{-~]/;
		
		var length				= document.getElementsByClassName("policy-length");
		var lowercase			= document.getElementsByClassName("policy-lowercase");
		var uppercase			= document.getElementsByClassName("policy-uppercase");
		var numbers				= document.getElementsByClassName("policy-numbers");
		var symbols				= document.getElementsByClassName("policy-symbols");

		if(newpassword.length >= 16){
			length[0].classList.add("active");
		}else{
			length[0].classList.remove("active");
		}
		if(patternLowercase.test(newpassword)){
			lowercase[0].classList.add("active");
		}else{
			lowercase[0].classList.remove("active");
		}
		if(patternUppercase.test(newpassword)){
			uppercase[0].classList.add("active");
		}else{
			uppercase[0].classList.remove("active");
		}
		if(patternNumbers.test(newpassword)){
			numbers[0].classList.add("active");
		}else {
			numbers[0].classList.remove("active");
		}
		if(patternSymbols.test(newpassword)){
			symbols[0].classList.add("active");
		}else{
			symbols[0].classList.remove("active");
		}
		
		var active		= $(".password-policies .active");
		var percentage	= $(".strength .percentage");
		var width		= 0;
		var bgcolor		= "#e1e5ec";
		if(active.length > 0){
			width	= active.length / 5 * 100;
		}
		
		if(active.length <= 1) {
			strengthmsg.innerHTML	= "Poor";
			bgcolor					= "#d36a83";
		}else if(active.length == 2) {
			strengthmsg.innerHTML	= "Weak";
			bgcolor					= "#e9bec8";
		}else if(active.length == 3) {
			strengthmsg.innerHTML	= "Midium";
			bgcolor					= "#9be99d";
		} else if(active.length == 4) {
			strengthmsg.innerHTML	= "Good";
			bgcolor					= "#36b139";
		}else{
			strengthmsg.innerHTML	= "Strong";
			bgcolor					= "#086a0b";
			status					= true;
		}
		
		if(newpassword.length == 0) {
			strengthmsg.setAttribute("style", "display: none;");
		}else{
			strengthmsg.setAttribute("style", "display: block;");
		}
		
		percentage[0].setAttribute("style", "width: " + width + "%; background-color: "+bgcolor);
		return status;
	}
</script>