<?php
	$global_config	= $this->session->userdata('global_config');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Your b.solutions Brightpearl | Xero integration is here.">
		<meta property="og:title" content="Brightpearl | Xero Integration by b.solutions" />
		<meta property="og:description" content="Your b.solutions Brightpearl | Xero integration is here." />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo $this->config->item('base_url').'users/login/' ?>" />
		<meta property="og:image" content="<?php echo $this->config->item('base_url').'extracss/favicon.png' ?>" />
		<link rel="shortcut icon" href="<?php echo base_url();?>/extracss/favicon.png" />
		<link rel="stylesheet" href="<?php echo base_url();?>/extracss/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url();?>/extracss/font-4-6-awesome.min.css" type="text/css" >
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@300;400;500;600;700&display=swap" />
		<link rel="stylesheet" href="<?php echo base_url();?>/extracss/styles.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url();?>/extracss/sideBar.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url();?>/extracss/rightBar.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url();?>/extracss/login.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url();?>/extracss/2extracss.css" type="text/css">
		<script src="<?php echo base_url();?>/extracss/ag-grid-community.min.js"></script>
		<title><?php echo $global_config['app_name'];?></title>
		
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		
		<!--
		<link href="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		-->
		
		<link href="<?php echo $this->config->item('script_url');?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="<?php echo $this->config->item('script_url');?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('script_url');?>assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
		
		<style>
			.ag-column-drop-wrapper{
				display: none;
			}
			.neW_cls {
				width: 100%;
				float: left;
			}
			.loginCopyRight.new{
				right: 3% !important;
				text-align: right;
				left: auto;
			}
			.loginCopyRight{
				position: fixed;
				width: auto;
			}
			.loginCopyRight{
				left: 2%;text-align: left;
				right: auto;
			}
			@media only screen and (max-width: 1025px){
				.loginCopyRight.new{
					left: 41% !important;
				}
				.loginCopyRight.new{
					right: 4% !important;
				}
			}
			@media only screen and (max-width: 993px){
				.loginForm{
					margin-bottom: 10% !important;
				}
				.row.no-gutters {
					margin-right: 0px;
				}
				.loginCopyRight{
					width: auto;
				}
				.loginCopyRight.new{
					right: 5% !important;
				}
			}
			@media only screen and (max-width: 768px){
				form.login-form.loginForm {
					width: 100% !important;
					margin-bottom: 12% !important;
				}
				#debug-icon{
					right: 15px;
				}
				.loginCopyRight.new {
					right: 8% !important;
				}
			}
			@media only screen and (max-width: 640px) {
				.loginCopyRight.new {
					right: 10% !important;
				}
			}
			@media only screen and (max-width: 510px){
				form.login-form.loginForm {
					width: 100% !important;
					min-width: fit-content !important;
				}
				.loginLBox{
					height: 100vh !important;
				}
				.loginCopyRight.new {
					right: 12% !important;
				}
			}
			@media only screen and (max-width: 500px) {
				.loginCopyRight.new {
					right: 14% !important;
				}
				.loginCopyRight{
					bottom: 50px;
				}
				#debug-icon{
					bottom: 35px;
				}
			}
			@media only screen and (max-width: 450px) {
				.loginCopyRight.new {
					right: 16% !important;
				}
			}
			@media only screen and (max-width: 375px) {
				.loginForm h2{
					font-size: 28px;
				}
				.loginCopyRight.new {
					right: 18% !important;
				}
				.loginLBox{
					padding: 27px 10px 27px 10px !important;
				}
				.loginCopyRight{
					bottom: 20px;
				}
			}
		</style>
	</head>
	<body>
		<section class="loginPage" style="background:#212854;">
			<div class="row no-gutters" style="background-repeat: no-repeat;background-size: cover;background-attachment: fixed;background-position: center;">
				<div class="col-md-12 col-lg-7 col-xs-12 logn">
					<div class="logo-default">
						<a href="https://b-solutions.io/" target="_blank"><img src="<?php echo base_url();?>/extracss/bsitc-logo-left.jpg" style="width: 15em!important;margin-top: 20px;"></a>
					</div>
					<div class="loginLBox" style="background-color: transparent!important;"> 
						<form  action="javascript:;" class="login-form loginForm" method="post" style="min-width:510px;min-height:23em;">
							<h2>Brightpearl | Xero Integration</h2>
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								<span>Enter any username and password.</span>
							</div>
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" name="username" class="form-control" autocomplete="off" id="inputEmail" required placeholder="Enter username" /> 
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" name="password" class="form-control" autocomplete="off" required placeholder="Enter password"  id="inputPassword" />
							</div>
							<a href="https://b-solutions.io/helpdesk/" target="blank" id="forget-password" class="forget-password">Forgot Password ? Please raise a ticket</a>
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<button type="submit" id="btnSubmit" class="btn btn-primary" style="text-transform:none!important;">Sign in</button>
							
						</form>
					</div>
				</div>
				<div class="col-md-12 col-lg-5 col-xs-12 logn" style="background: #fff;">
					<div class="loginLBox" style="background: url('<?php echo base_url();?>/extracss/bpxero.gif');background-repeat: no-repeat;background-size: 70%;background-attachment: local;background-position: center;"></div>
					
				</div>
				<div class="neW_cls">
					<div class="loginCopyRight">Copyright &copy; b.solutions <?= date('Y'); ?></div>
					<div class="loginCopyRight new" >
						<a href="https://b-solutions.io/"  target="_blank" style="color: #fc664a;z-index: revert;">b-solutions.io</a>
					</div>
				</div>
			</div>
		</section>
		
		<script src="<?php echo base_url();?>/extracss/js/jquery.slim.min.js"></script>
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		
		<!--
		<script src="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
		<script src="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
		<script src="<?php /* echo $this->config->item('script_url'); */?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		-->
		
		<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('script_url');?>assets/global/scripts/app.min.js" type="text/javascript"></script>
		<script>
			var baseurl	= "<?php echo base_url();?>";
			var url		= baseurl + "users/login/checkLogin";
			jQuery("body").addClass("login_body");
			jQuery(".login-form").on("submit",function(e){
				e.preventDefault();
				jQuery(".alert-danger").hide();	
				var username = jQuery("#inputEmail").val();
				var password = jQuery("#inputPassword").val();
				jQuery("#btnSubmit").attr("disabled","1").html("Please wait...");
				jQuery.post(url,jQuery( ".login-form" ).serialize(),function(res){
					console.log(res)
					if(res == "1"){
						jQuery("#btnSubmit").html("Success!");
						jQuery(".alert-success").show();
						window.location= baseurl + "sales/sales";
					}
					else{
						jQuery("#btnSubmit").removeAttr("disabled","1").html("Sign in");
						jQuery(".alert-danger").show();
					}
					
				})
			});
			$(document).ready(function(){
				$('.login-bg').backstretch([
					"<?php echo $this->config->item('script_url');?>/assets/pages/img/login/bg1.jpg",
					"<?php echo $this->config->item('script_url');?>/assets/pages/img/login/bg2.jpg",
					"<?php echo $this->config->item('script_url');?>/assets/pages/img/login/bg3.jpg"
				],{
					fade: 1000,
					duration: 8000
				});
				$('#clickmewow').click(function(){
					$('#radio1003').attr('checked', 'checked');
				});
				
				$('form').each(function(){
					$(this).find('input').keypress(function(e) {
						if(e.which == 10 || e.which == 13) {
							console.log("test");
							$(this).submit();
						}
					});
				});
			})
		</script>
	</body>
</html>