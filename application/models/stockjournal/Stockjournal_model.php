<?php
#[\AllowDynamicProperties]
class Stockjournal_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}	
	public function fetchStockjournal($journalId = ''){
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$StockjournalDatas	= $this->{$this->globalConfig['fetchStockJournal']}->fetchStockjournal($journalId);
		foreach($StockjournalDatas as $fetchAccount1Id => $StockjournalData){
			if($StockjournalData['saveTime']){
				$saveTime	= $StockjournalData['saveTime'] - (60*10);
			}
			$StockjournalDatas1	= $StockjournalData['return'];
			$AlljournalIds		= array();
			$batchInsert		= array();
			foreach($StockjournalDatas1 as $account1Id => $StockjournalDataTemp){
				$AlljournalIds		= array_keys($StockjournalDataTemp);
				$savedJournalIds	= array();
				$savedJournalData	= $this->db->select('id,journalId')->get_where('stock_journals')->result_array();
				foreach($savedJournalData as $savedJournalDataTemp){
					$savedJournalIds[$savedJournalDataTemp['journalId']]		= $savedJournalDataTemp;
				}
				
				$savedJournalIdsArch	= array();
				$savedJournalDataArch	= $this->db->select('id,journalId')->get_where('stock_journals_archived')->result_array();
				foreach($savedJournalDataArch as $savedJournalDataArchTemp){
					$savedJournalIdsArch[$savedJournalDataArchTemp['journalId']]	= $savedJournalDataArchTemp;
				}
				
				foreach($StockjournalDataTemp as $journalId => $row){
					if(!$journalId){continue;}
					if($savedJournalIds[$journalId]){continue;}
					if($savedJournalIdsArch[$journalId]){continue;}
					$batchInsert[]	= $row; 
				}
			}
			$inserted		= 0;
			$insertChunk	= 100;
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$insertChunk,true); 
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('stock_journals', $batchInsert); 
				}
			}
			if($inserted){
				$this->db->insert('cron_management', array('type' => 'stockjournals'.$fetchAccount1Id, 'saveTime' => $saveTime)); 
			}
		}
	}
	public function postConsolStockjournal($journalId = ''){
		$this->{$this->globalConfig['postStockJournal']}->postConsolStockjournal($journalId);
	}
	public function getStockjournal(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array("order"=> $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status == 9){
					$dbTable	= 'stock_journals';
					$this->{$this->globalConfig['fetchStockJournal']}->removeErrorFlag($ids,$dbTable);
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Error Removed Successfully.";
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('stock_journals', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']			= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']			= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('journalId'))){
				$where['journalId']				= trim($this->input->post('journalId'));
			}
			if(trim($this->input->post('created_journalId'))){
				$where['created_journalId']		= trim($this->input->post('created_journalId'));
			}
			if(trim($this->input->post('InvoiceNumber'))){
				$where['InvoiceNumber']			= trim($this->input->post('InvoiceNumber'));
			}
			if(trim($this->input->post('credit_nominalCode'))){
				$where['credit_nominalCode']	= trim($this->input->post('credit_nominalCode'));
			}
			if(trim($this->input->post('debit_nominalCode'))){
				$where['debit_nominalCode']		= trim($this->input->post('debit_nominalCode'));
			}
			if(trim($this->input->post('total_amt'))){
				$where['total_amt']				= strtoupper(trim($this->input->post('total_amt')));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']				= trim($this->input->post('status'));
			}
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('createdOn_from'))){
			$query->where('date(createdOn) >= ', "date('" . $this->input->post('createdOn_from') . "')", false);
		}
		if(trim($this->input->post('createdOn_to'))){
			$query->where('date(createdOn) <= ', "date('" . $this->input->post('createdOn_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countsales')->get('stock_journals')->row_array()['countsales'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('createdOn_from'))){
			$query->where('date(createdOn) >= ', "date('" . $this->input->post('createdOn_from') . "')", false);
		}
		if(trim($this->input->post('createdOn_to'))){
			$query->where('date(createdOn) <= ', "date('" . $this->input->post('createdOn_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0'	=> 'Pending','1' => 'Sent','4' => 'Archive');
		$statusColor			= array('0'	=> 'default','1' => 'success','4' => 'danger');
		$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalId', 'created_journalId', 'InvoiceNumber', 'credit_nominalCode', 'debit_nominalCode', 'total_amt', 'taxDate','createdOn','message', 'status');
		if($this->session->userdata('order')){
			foreach($this->session->userdata('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas					= $query->limit($limit, $start)->select('id,account1Id,account2Id,journalId,created_journalId,InvoiceNumber,journalTypeCode,taxDate,credit_nominalCode,debit_nominalCode,credit_transactionAmount,debit_transactionAmount,total_amt,credit_taxCode,debit_taxCode,status,message,channelId,currencyId,currencyCode,exchangeRate,createdOn')->get('stock_journals')->result_array();
		
		$account1Mappings	= array();
		$account2Mappings	= array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchStockJournal'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postStockJournal'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalId', 'created_journalId', 'total_amt', 'taxDate','createdOn','message', 'status');
		foreach($datas as $data){
			$viewOrder	= '';
			if($data['createdJournalsId']){
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="https://go.xero.com/AccountsPayable/View.aspx?InvoiceID='.$data['created_journalId'].'">View Bill in Xero</a>
								</li>';
			}
			$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalsId', 'createdJournalsId', 'orderId','taxDate','journalTypeCode','created','status');
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				$data['journalId'],
				@$data['created_journalId'],
				@$data['InvoiceNumber'],
				@$data['credit_nominalCode'],
				@$data['debit_nominalCode'],
				@sprintf("%.2f",$data['total_amt']),
				$data['taxDate'],
				$data['createdOn'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				@$data['message'], 
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs">Tools</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('/stockjournal/stockjournal/stockjournalinfo/'.$data['journalId']).'">Journal Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=accounts-correction&journal_id='.$data['journalId'].'">View Journal in Brightpearl</a>
						</li> 
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}