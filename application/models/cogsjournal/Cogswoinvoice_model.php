<?php
#[\AllowDynamicProperties]
class Cogswoinvoice_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}	
	public function fetchCogswoinvoice($journalsId = '', $accountId = ''){
		$fetchby			= $journalsId;
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$cogsJournalDatass	= $this->{$this->globalConfig['account1Liberary']}->fetchCogswoinvoice($journalsId, $accountId );
		$fatchedJournalIds	= array();
		foreach($cogsJournalDatass as $fetchAccount1Id => $cogsJournalDatassTemps){
			if($cogsJournalDatassTemps['saveTime']){
				$saveTime	= $cogsJournalDatassTemps['saveTime'] - (60*10);
			}
			$cogsJournalDatas	= $cogsJournalDatassTemps['return'];
			$batchInsert		= array();
			foreach($cogsJournalDatas as $account1Id => $cogsJournalData){
				$journalInfo			= array();
				$tempJournalDatas		= $this->db->select('id,journalsId')->get_where('cogs_woinvoice_journal')->result_array();
				foreach($tempJournalDatas as $tempJournalData){
					$journalInfo[$tempJournalData['journalsId']]	= $tempJournalData;
				}
				
				$journalInfoArc			= array();
				$tempJournalDatasArc	= $this->db->select('id,journalsId')->get_where('cogs_woinvoice_journal_archived')->result_array();
				foreach($tempJournalDatasArc as $tempJournalDataArc){
					$journalInfoArc[$tempJournalDataArc['journalsId']]	= $tempJournalDataArc;
				}
				
				foreach($cogsJournalData as $journalsId => $row){
					if(!$journalsId){
						continue;
					}
					if($journalInfo[$journalsId]){
						continue;
					}
					elseif($journalInfoArc[$journalsId]){
						continue;
					}
					else{
						$batchInsert[]	= $row; 
					}
				}
			}
			$inserted		= 0;
			$insertChunk	= 100;
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$insertChunk,true); 
				foreach($batchInserts as $batchInsert){
					$this->db->insert_batch('cogs_woinvoice_journal', $batchInsert); 
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'cogswoinvoicejournal'.$fetchAccount1Id, 'saveTime' => $saveTime)); 
				}
			}
		}
	}
	public function postCogswoInvoice($orderId = ''){
		$this->{$this->globalConfig['account2Liberary']}->postCogswoInvoice($orderId);
	}
	public function postConsolidatedCogswoInvoice($orderId = ''){
		$this->{$this->globalConfig['account2Liberary']}->postConsolidatedCogswoInvoice($orderId);
	}
	public function getCogswoInvoicejournal(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array("order"=> $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status == 9){
					$dbTable	= 'cogs_woinvoice_journal';
					$this->{$this->globalConfig['account1Liberary']}->removeErrorFlag($ids,$dbTable);
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Error Removed Successfully.";
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('cogs_woinvoice_journal', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']		= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']		= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('journalsId'))){
				$where['journalsId']		= trim($this->input->post('journalsId'));
			}
			if(trim($this->input->post('createdJournalsId'))){
				$where['createdJournalsId']	= trim($this->input->post('createdJournalsId'));
			}
			if(trim($this->input->post('orderId'))){
				$where['orderId']			= trim($this->input->post('orderId'));                
			}
			if(trim($this->input->post('journalTypeCode'))){
				$where['journalTypeCode']	= strtoupper(trim($this->input->post('journalTypeCode')));
			}
			if(trim($this->input->post('channelName'))){
				$where['channelName']		= trim($this->input->post('channelName'));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']			= trim($this->input->post('status'));
			}
			if(trim($this->input->post('isConsolidated')) >= '0'){
				$where['isConsolidated']	= trim($this->input->post('isConsolidated'));
			}
		}
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('created_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if(trim($this->input->post('created_to'))){
			$query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countsales')->get('cogs_woinvoice_journal')->row_array()['countsales'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('taxDate_from'))){
			$query->where('date(taxDate) >= ', "date('" . $this->input->post('taxDate_from') . "')", false);
		}
		if(trim($this->input->post('taxDate_to'))){
			$query->where('date(taxDate) <= ', "date('" . $this->input->post('taxDate_to') . "')", false);
		}
		if(trim($this->input->post('created_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('created_from') . "')", false);
		}
		if(trim($this->input->post('created_to'))){
			$query->where('date(created) <= ', "date('" . $this->input->post('created_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0'	=> 'Pending','1' => 'Sent','4' => 'Archive');
		$statusColor			= array('0'	=> 'default','1' => 'success','4' => 'danger');
		$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalsId', 'createdJournalsId', 'invoiceReference', 'orderId','journalTypeCode','channelName','taxDate', 'created','isConsolidated','status');
		if($this->session->userdata('order')){
			foreach($this->session->userdata('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		
		$account1Mappings		= array();
		$account2Mappings		= array();
		$datas					= $query->limit($limit, $start)->get('cogs_woinvoice_journal')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['account2Liberary'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$viewOrder	= '';
			$message	= $data['message'];
			if($data['createdJournalsId']){
				$viewOrder	=	'<li>
									<a class="newInfoBtn" target="_blank" href="https://go.xero.com/Journal/View.aspx?InvoiceID='.$data['createdJournalsId'].'">View Journal in Xero</a>
								</li>';
			}
			$displayProRowHeader	= array('id', 'account1Id', 'account2Id', 'journalsId', 'createdJournalsId', 'orderId','taxDate','journalTypeCode','channelName','created','status');
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				$data['journalsId'],
				@$data['createdJournalsId'],
				@$data['invoiceReference'],
				@$data['orderId'], 
				$data['journalTypeCode'],
				$data['channelName'],
				$data['taxDate'],
				$data['created'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['isConsolidated'],
				$message,
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs">Tools</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('/cogsjournal/cogswoinvoice/postCogswoInvoice/'.$data['journalsId']).'">Post Journal</a>
						</li>
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('/cogsjournal/cogswoinvoice/cogswoInvoicejournalInfo/'.$data['journalsId']).'">Journal Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'">View SO in Brightpearl</a>
						</li>
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}