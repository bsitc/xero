<?php
#[\AllowDynamicProperties]
class SalesReport_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchSalesReport(){
		$BrightPearlSalesReportData	= $this->brightpearl->fetchSalesReport();
		$XeroSalesReportData		= $this->xero->fetchSalesReport();
		$allXeroSavedData			= array();
		foreach($XeroSalesReportData as $account2Id	=> $XeroSalesReportDatas){
			foreach($XeroSalesReportDatas as $CreatedOrderID	=> $XeroSalesReportDatasTemp){
				$allXeroSavedData[$XeroSalesReportDatasTemp['orderId']]	= $XeroSalesReportDatasTemp;
			}
		}
		
		foreach($BrightPearlSalesReportData as $account1Id	=> $BrightPearlSalesReportDatas){
			$savedDatasTemp		= $this->db->get_where('daily_report',array('orderId <>' => ''))->result_array();
			$DBsavedDatasTemp	= array();
			$batchInsert		= array();
			$batchUpdate		= array();
			foreach($savedDatasTemp as $savedDatasTempDatas){
				$DBsavedDatasTemp[$savedDatasTempDatas['orderId']]	= $savedDatasTempDatas;
			}
			foreach($BrightPearlSalesReportDatas as $orderId => $BrightPearlSalesDatas){
				if($DBsavedDatasTemp[$orderId]){
					if($allXeroSavedData[$orderId]){
						$BrightPearlSalesDatas	= array_merge($allXeroSavedData[$orderId], $BrightPearlSalesDatas);
					}
					$batchUpdate[]	= $BrightPearlSalesDatas;
				}
				else{
					if($allXeroSavedData[$orderId]){
						$BrightPearlSalesDatas	= array_merge($allXeroSavedData[$orderId], $BrightPearlSalesDatas);
					}
					$batchInsert[]	= $BrightPearlSalesDatas;
				}
				
			}
			$updateOrder	= 100;
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					if($batchInsert){
						$this->db->insert_batch('daily_report', $batchInsert); 
					}
				}
			}
			if($batchUpdate){
				$inserted		= '1';
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$this->db->update_batch('daily_report', $batchUpdate,'orderId');   
					}
				}
			}
		}
	}
	public function getSalesReport(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array("order"=> $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status != ''){
					$this->db->where_in('id', $ids)->update('sales_order', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"]	= "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('orderId'))){
				$where['orderId']			= trim($this->input->post('orderId'));
			}
			if(trim($this->input->post('createOrderId'))){
				$where['createOrderId']		= trim($this->input->post('createOrderId'));
			}
			if(trim($this->input->post('bpInvoice'))){
				$where['bpInvoice']			= trim($this->input->post('bpInvoice'));
			}
			if(trim($this->input->post('xeroInvoice'))){
				$where['xeroInvoice']		= trim($this->input->post('xeroInvoice'));
			}
			if(trim($this->input->post('orderAmountBP'))){
				$where['orderAmountBP']		= trim($this->input->post('orderAmountBP'));
			}
			if(trim($this->input->post('orderAmountXero'))){
				$where['orderAmountXero']	= trim($this->input->post('orderAmountXero'));
			}
			if(trim($this->input->post('taxAmountBP'))){
				$where['taxAmountBP']		= trim($this->input->post('taxAmountBP'));
			}
			if(trim($this->input->post('taxAmountXero'))){
				$where['taxAmountXero']		= trim($this->input->post('taxAmountXero'));
			}
			if(trim($this->input->post('paidAmountBP'))){
				$where['paidAmountBP']		= trim($this->input->post('paidAmountBP'));
			}
			if(trim($this->input->post('paidAmountXero'))){
				$where['paidAmountXero']	= trim($this->input->post('paidAmountXero'));
			}
		}
		if(trim($this->input->post('bpTaxDate_from'))){
			$query->where('date(bpTaxDate) >= ', "date('" . $this->input->post('bpTaxDate_from') . "')", false);
		}
		if(trim($this->input->post('bpTaxDate_to'))){
			$query->where('date(bpTaxDate) <= ', "date('" . $this->input->post('bpTaxDate_to') . "')", false);
		}
		if(trim($this->input->post('xeroTaxDate_from'))){
			$query->where('date(xeroTaxDate) >= ', "date('" . $this->input->post('xeroTaxDate_from') . "')", false);
		}
		if(trim($this->input->post('xeroTaxDate_to'))){
			$query->where('date(xeroTaxDate) <= ', "date('" . $this->input->post('xeroTaxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countsales')->get('daily_report')->row_array()['countsales'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('bpTaxDate_from'))){
			$query->where('date(bpTaxDate) >= ', "date('" . $this->input->post('bpTaxDate_from') . "')", false);
		}
		if(trim($this->input->post('bpTaxDate_to'))){
			$query->where('date(bpTaxDate) <= ', "date('" . $this->input->post('bpTaxDate_to') . "')", false);
		}
		if(trim($this->input->post('xeroTaxDate_from'))){
			$query->where('date(xeroTaxDate) >= ', "date('" . $this->input->post('xeroTaxDate_from') . "')", false);
		}
		if(trim($this->input->post('xeroTaxDate_to'))){
			$query->where('date(xeroTaxDate) <= ', "date('" . $this->input->post('xeroTaxDate_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$displayProRowHeader	= array('id', 'orderId', 'createOrderId','orderAmountBP', 'orderAmountXero', 'taxAmountBP', 'taxAmountXero', 'paidAmountBP', 'paidAmountXero','bpCreateDate', 'bpTaxDate','bpPaymentDate','XeroCreateDate', 'updateDate');
		if($this->session->userdata($this->router->directory.$this->router->class)){
			foreach ($this->session->userdata($this->router->directory.$this->router->class) as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		$datas					= $query->limit($limit, $start)->get('daily_report')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		foreach($datas as $data){
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$data['orderId'],
				@$data['createOrderId'],
				@$data['bpInvoice'],
				@$data['xeroInvoice'],
				@$data['orderAmountBP'],
				@$data['orderAmountXero'],
				@$data['taxAmountBP'],
				@$data['taxAmountXero'],
				@$data['paidAmountBP'],
				@$data['paidAmountXero'],
				@$data['bpTaxDate'],
				@$data['xeroTaxDate'],
				'',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}