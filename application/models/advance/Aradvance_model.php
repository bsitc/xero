<?php
#[\AllowDynamicProperties]
class Aradvance_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}	
	public function fetchAradvance($id = ''){
		$cogsJournalDatass	= $this->{$this->globalConfig['fetchSalesOrder']}->fetchAradvance($id);
	}
	public function postAradvance($id = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postAradvance($id);
	}
	public function applyAradvance($id = ''){
		$this->{$this->globalConfig['postSalesOrder']}->applyAradvance($id);
	}
	public function getAradvance(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
			$orderData	= array("order"=> $this->input->post('order'));
			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status == 9){
					$dbTable	= 'arAdvance';
					$this->{$this->globalConfig['fetchSalesOrder']}->removeErrorFlag($ids,$dbTable);
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Error Removed Successfully.";
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('arAdvance', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){$where['account1Id'] = trim($this->input->post('account1Id'));}
			if(trim($this->input->post('account2Id'))){$where['account2Id'] = trim($this->input->post('account2Id'));}
			if(trim($this->input->post('paymentId'))){$where['paymentId'] = trim($this->input->post('paymentId'));}
			if(trim($this->input->post('transactionRef'))){$where['transactionRef'] = trim($this->input->post('transactionRef'));}
			if(trim($this->input->post('transactionCode'))){$where['transactionCode'] = trim($this->input->post('transactionCode'));}
			if(trim($this->input->post('paymentMethodCode'))){$where['paymentMethodCode'] = trim($this->input->post('paymentMethodCode'));}
			if(trim($this->input->post('paymentType'))){$where['paymentType'] = trim($this->input->post('paymentType'));}
			if(trim($this->input->post('orderId'))){$where['orderId'] = trim($this->input->post('orderId'));}
			if(trim($this->input->post('currencyId'))){$where['currencyId'] = trim($this->input->post('currencyId'));}
			if(trim($this->input->post('currencyCode'))){$where['currencyCode'] = trim($this->input->post('currencyCode'));}
			if(trim($this->input->post('amountAuthorized'))){$where['amountAuthorized'] = trim($this->input->post('amountAuthorized'));}
			if(trim($this->input->post('amountPaid'))){$where['amountPaid'] = trim($this->input->post('amountPaid'));}
			if(trim($this->input->post('journalId'))){$where['journalId'] = trim($this->input->post('journalId'));}
			if(trim($this->input->post('contactId'))){$where['contactId'] = trim($this->input->post('contactId'));}
			if(trim($this->input->post('journalTypeCode'))){$where['journalTypeCode'] = trim($this->input->post('journalTypeCode'));}
			if(trim($this->input->post('exchangeRate'))){$where['exchangeRate'] = trim($this->input->post('exchangeRate'));}
			if(trim($this->input->post('description'))){$where['description'] = trim($this->input->post('description'));}
			if(trim($this->input->post('orderTotal'))){$where['orderTotal'] = trim($this->input->post('orderTotal'));}
			if(trim($this->input->post('status')) >= '0'){$where['status'] = trim($this->input->post('status'));}
			if(trim($this->input->post('message'))){$where['message'] = trim($this->input->post('message'));}
			if(trim($this->input->post('channelId'))){$where['channelId'] = trim($this->input->post('channelId'));}
			if(trim($this->input->post('bankTxnId'))){$where['bankTxnId'] = trim($this->input->post('bankTxnId'));}
			if(trim($this->input->post('prePaymentId'))){$where['prePaymentId'] = trim($this->input->post('prePaymentId'));}
			if(trim($this->input->post('applicationId'))){$where['applicationId'] = trim($this->input->post('applicationId'));}
		}
		if(trim($this->input->post('paymentDate_from'))){
			$query->where('date(paymentDate) >= ', "date('" . $this->input->post('paymentDate_from') . "')", false);
		}
		if(trim($this->input->post('paymentDate_to'))){
			$query->where('date(paymentDate) <= ', "date('" . $this->input->post('paymentDate_to') . "')", false);
		}
		if(trim($this->input->post('expires_from'))){
			$query->where('date(expires) >= ', "date('" . $this->input->post('expires_from') . "')", false);
		}
		if(trim($this->input->post('expires_to'))){
			$query->where('date(expires) <= ', "date('" . $this->input->post('expires_to') . "')", false);
		}
		if(trim($this->input->post('createdOn_from'))){
			$query->where('date(createdOn) >= ', "date('" . $this->input->post('createdOn_from') . "')", false);
		}
		if(trim($this->input->post('createdOn_to'))){
			$query->where('date(createdOn) <= ', "date('" . $this->input->post('createdOn_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as totalRecords')->get('arAdvance')->row_array()['totalRecords'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('paymentDate_from'))){
			$query->where('date(paymentDate) >= ', "date('" . $this->input->post('paymentDate_from') . "')", false);
		}
		if(trim($this->input->post('paymentDate_to'))){
			$query->where('date(paymentDate) <= ', "date('" . $this->input->post('paymentDate_to') . "')", false);
		}
		if(trim($this->input->post('expires_from'))){
			$query->where('date(expires) >= ', "date('" . $this->input->post('expires_from') . "')", false);
		}
		if(trim($this->input->post('expires_to'))){
			$query->where('date(expires) <= ', "date('" . $this->input->post('expires_to') . "')", false);
		}
		if(trim($this->input->post('createdOn_from'))){
			$query->where('date(createdOn) >= ', "date('" . $this->input->post('createdOn_from') . "')", false);
		}
		if(trim($this->input->post('createdOn_to'))){
			$query->where('date(createdOn) <= ', "date('" . $this->input->post('createdOn_to') . "')", false);
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0'	=> 'Pending','1' => 'Sent','2' => 'Reversed','3' => 'Applied','4' => 'Archive');
		$statusColor			= array('0'	=> 'default','1' => 'success','2' => 'success','3' => 'success','4' => 'danger');
		$displayProRowHeader	= array('id', 'account1Id','account2Id','paymentId','journalId','orderId','amountPaid','paymentType','paymentMethodCode','contactId','paymentDate','channelId','bankTxnId','prePaymentId','status','message','',);
		
		if($this->session->userdata('order')){
			foreach($this->session->userdata('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		
		$account1Mappings		= array();
		$account2Mappings		= array();
		$datas					= $query->limit($limit, $start)->get('arAdvance')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		foreach($datas as $data){
			$viewOrder				= '';
			$viewbankTxn			= '';
			$viewPrePayment			= '';
			$viewReversalPrePayment	= '';
			if($data['prePaymentId']){
				$viewPrePayment	=	'<li>
									<a class="newInfoBtn" target="_blank" href="https://go.xero.com/AccountsReceivable/ViewCreditNote.aspx?creditNoteID='.$data['prePaymentId'].'">View Prepayment in Xero</a>
								</li>';
			}
			if($data['bankTxnId']){
				$viewbankTxn	=	'<li>
									<a class="newInfoBtn" target="_blank" href="https://go.xero.com/Bank/ViewTransaction.aspx?bankTransactionID='.$data['bankTxnId'].'">View Bank Txn in Xero</a>
								</li>';
			}
			if($data['reversalBankTxnId']){
				$viewReversalPrePayment	=	'<li>
									<a class="newInfoBtn" target="_blank" href="https://go.xero.com/Bank/ViewTransaction.aspx?bankTransactionID='.$data['reversalBankTxnId'].'">View Reverse Payment in Xero</a>
								</li>';
			}
			if($data['invoiceId']){
				$viewOrder		=	'<li>
										<a class="newInfoBtn" target="_blank" href="https://go.xero.com/AccountsReceivable/View.aspx?InvoiceID='.$data['invoiceId'].'">View Invoice in Xero</a>
									</li>';
			}
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				$data['paymentId'],
				$data['journalId'],
				$data['orderId'],
				@sprintf("%.2f",$data['amountPaid']),
				$data['paymentType'],
				$data['paymentMethodCode'],
				$data['contactId'],
				date('Y-m-d',strtotime($data['paymentDate'])),
				$data['channelId'],
				$data['bankTxnId'],
				$data['prePaymentId'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				$data['message'],
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs">Tools</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('/advance/aradvance/postAradvance/'.$data['orderId']).'">Post AR Advance</a>
						</li>
						<li>
							<a class="btnactionsubmit" href="'.base_url('/advance/aradvance/applyAradvance/'.$data['orderId']).'">Apply AR Advance</a>
						</li>
						<li>
							<a class="newInfoBtn" target = "_blank" href="'.base_url('/advance/aradvance/aradvanceInfo/'.$data['paymentId']).'">AR Advance Info</a>
						</li>
						<li>
							<a class="newInfoBtn" target="_blank" href="'.$account1Mappings[$data['account1Id']]['viewUrl'].'/patt-op.php?scode=invoice&oID='.$data['orderId'].'">View Sales in Brightpearl</a>
						</li>
						'.$viewPrePayment.'
						'.$viewbankTxn.'
						'.$viewReversalPrePayment.'
						'.$viewOrder.'
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}