<?php
#[\AllowDynamicProperties]
class Tax_model extends CI_Model{
	public function get(){
		$data				= array();
		$data['data']		= $this->db->get('mapping_tax')->result_array();
		$account1IdTemps	= $this->db->get('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		$account1Id			= array();
		foreach ($account1IdTemps as $account1IdTemp) {
			$account1Id[$account1IdTemp['id']]	= $account1IdTemp;
		}
		$account2IdTemps	= $this->db->get('account_'.$this->globalConfig['account2Liberary'].'_account')->result_array();
		$account2Id			= array();
		foreach ($account2IdTemps as $account2IdTemp) {
			$account2Id[$account2IdTemp['id']]	= $account2IdTemp;
		}
		$data['account1Id'] = $account1Id;
		$data['account2Id'] = $account2Id;
		return $data;
	}
	public function delete($id){
		if($id){
			$saveDataInfo	= $this->db->get_where('mapping_tax',array('id' => $id))->row_array();
			$storeDataLogs	= array(
				'type'			=> 'mapping_tax_deleted',
				'logs'			=> json_encode($saveDataInfo),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'ipAddress'		=> getenv('REMOTE_ADDR'),
			);
			$this->db->where(array('id' => $id))->delete('mapping_tax');
			$this->db->insert('configlogs', $storeDataLogs);
		}
	}
	public function save($data){
		if(!empty($data)){
			foreach($data as $dataKey => $dataValue){
				if(is_array($data[$dataKey])){
					$dataValue		= array_filter($dataValue);
					$dataValue		= array_unique($dataValue);
					$data[$dataKey]	= implode(",",$dataValue);
				}
			}
		}
		if($data['id']){
			$saveDataInfo			= array();
			$saveDataInfo['old']	= $this->db->get_where('mapping_tax',array('id' => $data['id']))->row_array();
			$saveDataInfo['new']	= $data;
			$storeDataLogs	= array(
				'ipAddress'		=> getenv('REMOTE_ADDR'),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'type'			=> 'mapping_tax_updated',
				'logs'			=> json_encode($saveDataInfo),
			);
			
			$data['status'] = $this->db->where(array('id' => $data['id']))->update('mapping_tax',$data);
			
			if($data['status']){$this->db->insert('configlogs', $storeDataLogs);}
		}
		else{
			$data['status'] = $this->db->insert('mapping_tax',$data);
			$data['id']		= $this->db->insert_id();
			if($data['id']){
				$storeDataLogs	= array(
					'ipAddress'		=> getenv('REMOTE_ADDR'),
					'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
					'type'			=> 'mapping_tax_added',
					'logs'			=> json_encode($data),
				);
				$this->db->insert('configlogs', $storeDataLogs);
			}
		}
		return $data;
	}
}
?>