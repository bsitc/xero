<?php
#[\AllowDynamicProperties]
class Allowance_model extends CI_Model{
	public function get(){
		$data				= array();
		$data['data']		= $this->db->get('mapping_allowance')->result_array();
		$account1IdTemps	= $this->db->get('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		$account1Id			= array();
		foreach ($account1IdTemps as $account1IdTemp) {
			$account1Id[$account1IdTemp['id']]	= $account1IdTemp;
		}
		$account2IdTemps	= $this->db->get('account_'.$this->globalConfig['account2Liberary'].'_account')->result_array();
		$account2Id			= array();
		foreach ($account2IdTemps as $account2IdTemp) {
			$account2Id[$account2IdTemp['id']]	= $account2IdTemp;
		}
		$data['account1Id']	= $account1Id;
		$data['account2Id']	= $account2Id;
		return $data;
	}
	public function delete($id){
		$saveDataInfo	= $this->db->get_where('mapping_allowance',array('id' => $id))->row_array();
		$storeDataLogs	= array(
			'ipAddress'		=> getenv('REMOTE_ADDR'),
			'type'			=> 'mapping_allowance_deleted',
			'logs'			=> json_encode($saveDataInfo),
		);
		$this->db->insert('configlogs', $storeDataLogs);
		$this->db->where(array('id' => $id))->delete('mapping_allowance');
	}
	public function save($data){
		if(!empty($data)){
			foreach($data as $dataKey => $dataValue){
				if(is_array($data[$dataKey])){
					$dataValue		= array_filter($dataValue);
					$dataValue		= array_unique($dataValue);
					$data[$dataKey]	= implode(",",$dataValue);
				}
			}
		}
		if($data['id']){
			$saveDataInfo			= array();
			$saveDataInfo['old']	= $this->db->get_where('mapping_allowance',array('id' => $data['id']))->row_array();
			$saveDataInfo['new']	= $data;
			$storeDataLogs	= array(
				'ipAddress'		=> getenv('REMOTE_ADDR'),
				'type'			=> 'mapping_allowance_updated',
				'logs'			=> json_encode($saveDataInfo),
			);
			
			$data['status']	= $this->db->where(array('id' => $data['id']))->update('mapping_allowance',$data);
			
			if($data['status']){$this->db->insert('configlogs', $storeDataLogs);}
		}
		else{
			$data['status']	= $this->db->insert('mapping_allowance',$data);
			$data['id']		= $this->db->insert_id();
			if($data['id']){
				$storeDataLogs	= array(
					'ipAddress'		=> getenv('REMOTE_ADDR'),
					'type'			=> 'mapping_allowance_added',
					'logs'			=> json_encode($data),
				);
				$this->db->insert('configlogs', $storeDataLogs);
			}
		}
		return $data;
	}
}
?>