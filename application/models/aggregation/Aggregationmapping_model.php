<?php
#[\AllowDynamicProperties]
class Aggregationmapping_model extends CI_Model{
	public function get(){
		$data					= array();
		$data['data']			= $this->db->get('mapping_aggregation')->result_array();
		
		$account1Id				= array();
		$account1Config			= array();
		$account1IdTemps		= $this->db->get('account_'.$this->globalConfig['account1Liberary'].'_account')->result_array();
		$account1ConfigTemps	= $this->db->get('account_'.$this->globalConfig['account1Liberary'].'_config')->result_array();
		foreach($account1IdTemps as $account1IdTemp){
			$account1Id[$account1IdTemp['id']]	= $account1IdTemp;
		}
		foreach($account1ConfigTemps as $account1ConfigTemp){
			$account1Config[$account1ConfigTemp['id']]	= $account1ConfigTemp;
		}
		
		$account2Id				= array();
		$account2Config			= array();
		$account2IdTemps		= $this->db->get('account_'.$this->globalConfig['account2Liberary'].'_account')->result_array();
		$account2ConfigTemps	= $this->db->get('account_'.$this->globalConfig['account2Liberary'].'_config')->result_array();
		foreach($account2IdTemps as $account2IdTemp){
			$account2Id[$account2IdTemp['id']]			= $account2IdTemp;
		}
		foreach($account2ConfigTemps as $account2ConfigTemp){
			$account2Config[$account2ConfigTemp['id']]	= $account2ConfigTemp;
		}
		
		$data['account1Id']		= $account1Id;
		$data['account1Config']	= $account1Config;
		$data['account2Id']		= $account2Id;
		$data['account2Config']	= $account2Config;
		return $data;
	}
	public function delete($id){
		if($id){
			$saveDataInfo	= $this->db->get_where('mapping_aggregation',array('id' => $id))->row_array();
			$storeDataLogs	= array(
				'type'			=> 'mapping_aggregation_deleted',
				'logs'			=> json_encode($saveDataInfo),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'ipAddress'		=> getenv('REMOTE_ADDR'),
			);
			$this->db->where(array('id' => $id))->delete('mapping_aggregation');
			$this->db->insert('configlogs', $storeDataLogs);
		}
	}
	public function save($data){
		if($data['id']){
			$saveDataInfo			= array();
			$saveDataInfo['old']	= $this->db->get_where('mapping_aggregation',array('id' => $data['id']))->row_array();
			$saveDataInfo['new']	= $data;
			$storeDataLogs	= array(
				'ipAddress'		=> getenv('REMOTE_ADDR'),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'type'			=> 'mapping_aggregation_updated',
				'logs'			=> json_encode($saveDataInfo),
			);
			
			$data['status']	= $this->db->where(array('id' => $data['id']))->update('mapping_aggregation',$data);
			
			if($data['status']){$this->db->insert('configlogs', $storeDataLogs);}
		}
		else{
			$data['status']	= $this->db->insert('mapping_aggregation',$data);
			$data['id']		= $this->db->insert_id();
			if($data['id']){
				$storeDataLogs	= array(
					'ipAddress'		=> getenv('REMOTE_ADDR'),
					'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
					'type'			=> 'mapping_aggregation_added',
					'logs'			=> json_encode($data),
				);
				$this->db->insert('configlogs', $storeDataLogs);
			}
		}
		return $data;
	}
}
?>