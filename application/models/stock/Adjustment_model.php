<?php
//xero
#[\AllowDynamicProperties]
class Adjustment_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchStockAdjustment($orderId = '', $accountId = '', $fetchType = ''){
		$fetchby		= $orderId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$salesDatass	= $this->{$this->globalConfig['fetchStockAdjustment']}->fetchStockAdjustment($orderId, $accountId, '' , $fetchType);
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(@$salesDatassTemps['saveTime']){
				$saveTime	= $salesDatassTemps['saveTime'] - (60*10);
			}
			$salesDatas			= $salesDatassTemps['return'];
			$batchInsert		= array();
			$batchGoodsInserts	= array();
			$batchGoodsInsert	= array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$orderIds		= array_keys($salesData);
				$orderInfo		= array();
				$orderInfo2		= array();
				$tempItemDatas	= $this->db->select('id,orderId,ActivityId')->get_where('stock_adjustment')->result_array();
				foreach($tempItemDatas as $tempItemData){
					$orderInfo[$tempItemData['orderId']]			= $tempItemData;
					if($tempItemData['ActivityId']){
						$orderInfo2[$tempItemData['ActivityId']]	= $tempItemData;
					}
				}
				
				$orderInfoArch		= array();
				$orderInfo2Arch		= array();
				$tempItemDatasArch	= $this->db->select('id,orderId,ActivityId')->get_where('stock_adjustment_archived')->result_array();
				foreach($tempItemDatasArch as $tempItemDatasArchtemp){
					$orderInfoArch[$tempItemDatasArchtemp['orderId']]			= $tempItemDatasArchtemp;
					if($tempItemDatasArchtemp['ActivityId']){
						$orderInfo2Arch[$tempItemDatasArchtemp['ActivityId']]	= $tempItemDatasArchtemp;
					}
				}
				
				foreach($salesData as $orderId => $row){
					$duplicateEntry	= 0;
					if(!$orderId){
						continue;
					}
					foreach($row as $UniqueID => $TransferData){
						if($TransferData['ActivityId']){
							if($orderInfo2[$TransferData['ActivityId']]){
								$duplicateEntry	= 1;
								break;
							}
							if($orderInfo2Arch[$TransferData['ActivityId']]){
								$duplicateEntry	= 1;
								break;
							}
						}
					}
					$isAllreadyInserted = 0;
					if($orderInfo[$orderId]){
						continue;
					}
					if($orderInfoArch[$orderId]){
						continue;
					}
					if($duplicateEntry){
						continue;
					}
					foreach ($row as $productId => $gitems){
						$batchGoodsInsert[]	= $gitems;
					}
				}
			}
			$inserted		= 0;
			$updateOrder	= 500;
			if($batchGoodsInsert){ 
				$inserted			= '1';
				$batchGoodsInserts	= array_chunk($batchGoodsInsert,$updateOrder,true);
				foreach($batchGoodsInserts as $batchGoodsInsert){
					$this->db->insert_batch('stock_adjustment', $batchGoodsInsert);
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'stockadjustment'.$fetchAccount1Id,'saveTime' => $saveTime)); 
				}
			}
		}
	}
	public function postStockAdjustment($id = '',$fetchType = ''){
		$inventoryDatas	= $this->{$this->globalConfig['postStockAdjustment']}->postStockAdjustment($id,$fetchType);
	}
	public function postConsolStockAdjustment($id = ''){
		$inventoryDatas	= $this->{$this->globalConfig['postStockAdjustment']}->postConsolStockAdjustment($id);
	}
	
	public function getAdjustment(){
		$groupAction		= $this->input->post('customActionType');
		$records			= array();
		$records["data"]	= array();
		if($this->input->post('order')){
 			$orderData		= array($this->router->directory.$this->router->class => $this->input->post('order'));
 			$this->session->set_userdata($orderData);
		}
		if($groupAction == 'group_action'){
			$ids	= $this->input->post('id');
			if($ids){
				$status	= $this->input->post('customActionName');
				if($status == 9){
					$dbTable	= 'stock_adjustment';
					$this->{$this->globalConfig['fetchSalesOrder']}->removeErrorFlag($ids,$dbTable);
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Error Removed Successfully.";
				}
				elseif($status != ''){
					$this->db->where_in('id', $ids)->update('stock_adjustment', array('status' => $status));
					$records["customActionStatus"]	= "OK";
					$records["customActionMessage"] = "Group action successfully has been completed. Well done!";
				}
			}
		}
		
		$this->db->reset_query();
		$productMappings		= array();
		$productMappingsSKU		= array();
		$productMappingsTemps	= $this->db->select('productId,sku')->get_where('products',array('productId <>' => ''))->result_array();
		foreach($productMappingsTemps as $productMappingsTemp){
			$productMappings[$productMappingsTemp['productId']]	= $productMappingsTemp;
			$productMappingsSKU[$productMappingsTemp['sku']]	= $productMappingsTemp;
		}
		
		$where	= array();
		$query	= $this->db;
		if($this->input->post('action') == 'filter'){
			if(trim($this->input->post('account1Id'))){
				$where['account1Id']			= trim($this->input->post('account1Id'));
			}
			if(trim($this->input->post('account2Id'))){
				$where['account2Id']			= trim($this->input->post('account2Id'));
			}
			if(trim($this->input->post('orderId'))){
				$where['orderId']				= trim($this->input->post('orderId'));
			}
			if(trim($this->input->post('createdOrderId'))){
				$where['createdOrderId']		= trim($this->input->post('createdOrderId'));
			}
			if(trim($this->input->post('ActivityId'))){
				$where['ActivityId']			= trim($this->input->post('ActivityId'));
			}
			if(trim($this->input->post('GoodNotetype'))){
				$where['GoodNotetype']			= trim($this->input->post('GoodNotetype'));
			}
			if(trim($this->input->post('qty'))){
				$where['qty']					= trim($this->input->post('qty'));
			}
			if(trim($this->input->post('productId'))){
				$where['productId']				= trim($this->input->post('productId'));
			}
			if(trim($this->input->post('goodsNoteId'))){
				$where['goodsNoteId']			= trim($this->input->post('goodsNoteId'));
			}
			if(trim($this->input->post('price'))){
				$where['price']					= trim($this->input->post('price'));
			}
			if(trim($this->input->post('status')) >= '0'){
				$where['status']				= trim($this->input->post('status'));
			}
			if(trim($this->input->post('sku'))){
				if($productMappingsSKU[trim($this->input->post('sku'))]){
					$checkProductID		= $productMappingsSKU[trim($this->input->post('sku'))]['productId'];
					$where['productId']	= $checkProductID;
					
				}
			}
		}
		if(trim($this->input->post('updated_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if(trim($this->input->post('warehouseId'))){
			$query->where('warehouseId', $this->input->post('warehouseId'));
		}
		if($where){
			$query->like($where);
		}
		$totalRecord	= @$query->select('count("id") as countpro')->get('stock_adjustment')->row_array()['countpro'];
		$limit			= intval($this->input->post('length'));
		$limit			= $limit < 0 ? $totalRecord : $limit;
		$start			= intval($this->input->post('start'));
		$query			= $this->db;
		if(trim($this->input->post('updated_from'))){
			$query->where('date(created) >= ', "date('" . $this->input->post('updated_from') . "')", false);
		}
		if(trim($this->input->post('updated_to'))){
			$query->where('date(created) < ', "date('" . $this->input->post('updated_to') . "')", false);
		}
		if(trim($this->input->post('warehouseId'))){
			$query->where('warehouseId', $this->input->post('warehouseId'));
		}
		if($where){
			$query->like($where);
		}
		$status					= array('0' => 'Pending', '1' => 'Sent', '2' => 'Updated', '3' => 'Error', '4' => 'Archive');
		$statusColor			= array('0' => 'default', '1' => 'success', '2' => 'info', '3' => 'warning', '4' => 'danger');
		$displayProRowHeader	= array('id','account1Id','account2Id', 'orderId','goodsNoteId','ActivityId','createdOrderId','warehouseId', 'productId', '', 'qty', 'price', '', 'GoodNotetype','created', 'status');
		if($this->input->post('order')){
			foreach($this->input->post('order') as $ordering){
				if(@$displayProRowHeader[$ordering['column']]){
					$query->order_by($displayProRowHeader[$ordering['column']], $ordering['dir']);
				}
			}
		}
		
		$account1Mappings	= array();
		$account2Mappings	= array();
		$datas	= $query->limit($limit, $start)->select('id, account1Id, account2Id, orderId, goodsNoteId, ActivityId, createdOrderId, warehouseId, productId, qty, price, GoodNotetype, created, status')->get('stock_adjustment')->result_array();
		$account1MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['fetchSalesOrder'].'_account')->result_array();
		foreach($account1MappingTemps as $account1MappingTemp){
			$account1Mappings[$account1MappingTemp['id']]	= $account1MappingTemp;
		}
		$account2MappingTemps	= $this->db->get_where('account_'.$this->globalConfig['postSalesOrder'].'_account')->result_array();
		foreach($account2MappingTemps as $account2MappingTemp){
			$account2Mappings[$account2MappingTemp['id']]	= $account2MappingTemp;
		}
		
		$allMappedWarehouseName	= array();
		$allBPWarehouse			= $this->brightpearl->getAllLocation();
		if($allBPWarehouse){
			foreach($allBPWarehouse[1] as $allBPWarehouses){
				$allMappedWarehouseName[$allBPWarehouses['id']]	= $allBPWarehouses['name'];
			}
		}
		
		foreach($datas as $data){
			$transfertype	= 'Inventory Adjustment';
			$postID			= $data['orderId'];
			$InfoButton		= 'Adjustment';
			if($data['GoodNotetype'] == 'GO'){
				$transfertype	= 'Inventory Transfer';
				$postID			= $data['ActivityId'];
				$InfoButton		= 'Transfer';
			}
			if($data['GoodNotetype'] == 'SC'){
				$transfertype	= 'Inventory Adjustment';
				$postID			= $data['orderId'];
				$InfoButton		= $data['orderId'];
				$InfoButton		= 'Adjustment';
			}
			$ViewOrder	= '';
			if($data['createdOrderId']){
				if(($transfertype == 'Inventory Adjustment') AND ($data['qty'] > 0)){
					$ViewOrder	=	'<li>
										<a class="newInfoBtn" target="_blank" href="https://go.xero.com/AccountsPayable/View.aspx?InvoiceID='.$data['createdOrderId'].'">View Document in Xero</a>
									</li>';
				}
				if(($transfertype == 'Inventory Adjustment') AND ($data['qty'] < 0)){
					if($this->globalConfig['enableSAConsol']){
						$ViewOrder	=	'<li>
											<a class="newInfoBtn" target="_blank" href="https://go.xero.com/AccountsPayable/View.aspx?InvoiceID='.$data['createdOrderId'].'">View Document in Xero</a>
										</li>';
					}
					else{
						$ViewOrder	=	'<li>
											<a class="newInfoBtn" target="_blank" href="https://go.xero.com/AccountsPayable/ViewCreditNote.aspx?creditNoteID='.$data['createdOrderId'].'">View Document in Xero</a>
										</li>';
					}
				}
				if($transfertype == 'Inventory Transfer'){
					$ViewOrder	=	'<li>
										<a class="newInfoBtn" target="_blank" href="https://go.xero.com/AccountsReceivable/View.aspx?InvoiceID='.$data['createdOrderId'].'">View Document in Xero</a>
									</li>';
				}
			}
			$records["data"][]	= array(
				'<input type="checkbox" name="id[]" value="' . $data['id'] . '">',
				@$account1Mappings[$data['account1Id']]['name'],
				@$account2Mappings[$data['account2Id']]['name'],
				$data['orderId'],
				$data['goodsNoteId'],
				$data['ActivityId'],
				$data['createdOrderId'],
				$allMappedWarehouseName[$data['warehouseId']],
				$data['productId'],
				$productMappings[$data['productId']]['sku'],
				$data['qty'],
				@sprintf("%.2f",$data['price']),
				@sprintf("%.2f",($data['qty'] * $data['price'])),
				$transfertype,
				$data['created'],
				'<span class="label label-sm label-' . $statusColor[$data['status']] . '">' . $status[$data['status']] . '</span>',
				'<div class="btn-group">
					<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						<i class="fa fa-share"></i>
						<span class="hidden-xs"> Tools </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<div class="dropdown-menu pull-right">
						<li>
							<a class="btnactionsubmit" href="'.base_url('stock/adjustment/postStockAdjustment/'.$postID).'">Post Stock Adjustment</a>
						</li>
						'.$ViewOrder.'
						<li>
							<a class="newInfoBtn" class="" target="_blnak" href="' . base_url('stock/adjustment/adjustmentinfo/'.$data['id']) . '">Stock '.$InfoButton.' Info</a>
						</li>
					</div>
				</div>',
			);
		}
		$draw						= intval($this->input->post('draw'));
		$records["draw"]			= $draw;
		$records["recordsTotal"]	= $totalRecord;
		$records["recordsFiltered"]	= $totalRecord;
		return $records;
	}
}