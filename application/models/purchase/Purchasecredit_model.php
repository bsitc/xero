<?php
#[\AllowDynamicProperties]
class Purchasecredit_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchPurchaseCredit($orderId = '', $accountId = ''){
		$fetchby		= $orderId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$purchaseDatass	= $this->{$this->globalConfig['fetchPurchaseCredit']}->fetchPurchaseCredit($orderId, $accountId);
		foreach($purchaseDatass as $fetchAccount1Id => $purchaseDatassTemps){
			if(isset($purchaseDatassTemps['saveTime'])){
				$saveTime	= $purchaseDatassTemps['saveTime'] - (60*10);
			}
			$purchaseDatas	= (isset($purchaseDatassTemps['return'])) ? ($purchaseDatassTemps['return']) : array();
			$batchInsert	= array();
			$batchUpdate	= array();
			$inserted		= 0;
			$updateOrder	= 100;
			foreach($purchaseDatas as $account1Id => $purchaseData){
				$savedOrderData		= array();
				$archivedOrderData1	= array();
				$archivedOrderData2	= array();
				
				$ordersInDatabase	= $this->db->select('id,orderId,status')->get_where('purchase_credit_order')->result_array();
				if(!empty($ordersInDatabase)){
					foreach($ordersInDatabase as $ordersInDatabases){
						$savedOrderData[$ordersInDatabases['orderId']]		= $ordersInDatabases;
					}
				}
				
				$ordersInDatabase	= $this->db->select('orderId')->get_where('purchase_credit_order_archived')->result_array();
				if(!empty($ordersInDatabase)){
					foreach($ordersInDatabase as $ordersInDatabases){
						$archivedOrderData1[$ordersInDatabases['orderId']]	= $ordersInDatabases;
					}
				}
				
				$ordersInDatabase	= $this->db->select('orderId')->get_where('z_old_purchaseCreditIds')->result_array();
				if(!empty($ordersInDatabase)){
					foreach($ordersInDatabase as $ordersInDatabases){
						$archivedOrderData2[$ordersInDatabases['orderId']]	= $ordersInDatabases;
					}
				}
				
				foreach($purchaseData as $orderId => $row){
					if(!$orderId){continue;}
					if(isset($archivedOrderData1[$orderId])){continue;}
					if(isset($archivedOrderData2[$orderId])){continue;}
					
					if(isset($savedOrderData[$orderId])){
						if(isset($row['orders'])){
							if($savedOrderData[$orderId]['status'] == 4){continue;}
							$orderRow			= $row['orders'];
							$orderRow['id']		= $savedOrderData[$orderId]['id'];
							$orderRow['status']	= $savedOrderData[$orderId]['status'];
							$batchUpdate[]		= $orderRow;
						}
					}
					else{
						if(isset($row['orders'])){
							$orderRow		= $row['orders'];
							$batchInsert[]	= $orderRow;
						}
					}
				}
			}
			if($batchUpdate){
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$inserted	= $this->db->update_batch('purchase_credit_order', $batchUpdate,'id');
					}
				}
			}
			if($batchInsert){
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					if($batchInsert){
						$inserted	= $this->db->insert_batch('purchase_credit_order', $batchInsert); 
					}
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'purchaseCredit'.$fetchAccount1Id,'saveTime' => $saveTime));
				}
			}
		}
	}
	public function postPurchaseCredit($orderId = ''){
		$this->{$this->globalConfig['postPurchaseCredit']}->postPurchaseCredit($orderId);
	}
}