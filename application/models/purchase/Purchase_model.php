<?php
#[\AllowDynamicProperties]
class Purchase_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchPurchase($orderId = '', $accountId = ''){
		$fetchby		= $orderId;
		$saveTime		= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$purchaseDatass	= $this->{$this->globalConfig['fetchPurchaseOrder']}->fetchPurchase($orderId, $accountId);
		foreach($purchaseDatass as $fetchAccount1Id => $purchaseDatassTemps){
			if(isset($purchaseDatassTemps['saveTime'])){
				$saveTime	= $purchaseDatassTemps['saveTime'] - (60*10);
			}
			
			//changesAddedAfterGRNIJournal
			$grniJournalsOrderIDs	= array();
			
			$purchaseDatas	= (isset($purchaseDatassTemps['return'])) ? ($purchaseDatassTemps['return']) : array();
			$batchInsert	= array();
			$batchUpdate	= array();
			$inserted		= 0;
			$updateOrder	= 100;
			foreach($purchaseDatas as $account1Id => $purchaseData){
				$savedOrderData		= array();
				$archivedOrderData1	= array();
				$archivedOrderData2	= array();
				
				$ordersInDatabase	= $this->db->select('id,orderId,status')->get_where('purchase_order')->result_array();
				if(!empty($ordersInDatabase)){
					foreach($ordersInDatabase as $ordersInDatabases){
						$savedOrderData[$ordersInDatabases['orderId']]		= $ordersInDatabases;
					}
				}
				
				$ordersInDatabase	= $this->db->select('orderId')->get_where('purchase_order_archived')->result_array();
				if(!empty($ordersInDatabase)){
					foreach($ordersInDatabase as $ordersInDatabases){
						$archivedOrderData1[$ordersInDatabases['orderId']]	= $ordersInDatabases;
					}
				}
				
				$ordersInDatabase	= $this->db->select('orderId')->get_where('z_old_purchaseIds')->result_array();
				if(!empty($ordersInDatabase)){
					foreach($ordersInDatabase as $ordersInDatabases){
						$archivedOrderData2[$ordersInDatabases['orderId']]	= $ordersInDatabases;
					}
				}
				
				foreach($purchaseData as $orderId => $row){
					if(!$orderId){continue;}
					if(isset($archivedOrderData1[$orderId])){continue;}
					if(isset($archivedOrderData2[$orderId])){continue;}
					
					if(isset($savedOrderData[$orderId])){
						if(isset($row['orders'])){
							if($savedOrderData[$orderId]['status'] == 4){continue;}
							$orderRow			= $row['orders'];
							$orderRow['id']		= $savedOrderData[$orderId]['id'];
							$orderRow['status']	= $savedOrderData[$orderId]['status'];
							$batchUpdate[]		= $orderRow;
							
							//changesAddedAfterGRNIJournal
							if(strlen($orderRow['bpInvoiceNumber']) > 0){
								$grniJournalsOrderIDs[$orderId]	= array(
									'orderId'			=> $orderId,
									'orderInvoiceRef'	=> $orderRow['bpInvoiceNumber'],
									'orderTaxdate'		=> $orderRow['taxDate'],
									'isOrderInvoiced'	=> 1,
								);
							}
						}
					}
					else{
						if(isset($row['orders'])){
							$orderRow		= $row['orders'];
							$batchInsert[]	= $orderRow;
							
							//changesAddedAfterGRNIJournal
							if(strlen($orderRow['bpInvoiceNumber']) > 0){
								$grniJournalsOrderIDs[$orderId]	= array(
									'orderId'			=> $orderId,
									'orderInvoiceRef'	=> $orderRow['bpInvoiceNumber'],
									'orderTaxdate'		=> $orderRow['taxDate'],
									'isOrderInvoiced'	=> 1,
								);
							}
						}
					}
				}
			}
			if($batchUpdate){
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$inserted	= $this->db->update_batch('purchase_order', $batchUpdate,'id');
					}
				}
			}
			if($batchInsert){
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					if($batchInsert){
						$inserted	= $this->db->insert_batch('purchase_order', $batchInsert); 
					}
				}
			}
			//changesAddedAfterGRNIJournal
			if(!empty($grniJournalsOrderIDs)){
				$grniJournalsOrderIDss	= array_chunk($grniJournalsOrderIDs,$updateOrder,true);
				foreach($grniJournalsOrderIDss as $grniJournalsOrderIDsss){
					if($grniJournalsOrderIDsss){
						$this->db->update_batch('grni_journal', $grniJournalsOrderIDsss,'orderId');
					}
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'purchase'.$fetchAccount1Id,'saveTime' => $saveTime));
				}
			}
		}
	}
	public function postPurchase($orderId = ''){
		$this->{$this->globalConfig['postPurchaseOrder']}->postPurchase($orderId);
	}
}