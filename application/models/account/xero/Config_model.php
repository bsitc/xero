<?php
#[\AllowDynamicProperties]
class Config_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci = get_instance();
	}
	public function get($type = ''){
		$data					= array();
		$data['data']			= $this->db->get('account_xero_config')->result_array();
		$saveAccountTemps		= $this->db->get('account_xero_account')->result_array();
		$saveAccount			= array();
		foreach($saveAccountTemps as $saveAccountTemp){
			$saveAccount[$saveAccountTemp['id']]	= $saveAccountTemp;
		}
		$data['saveAccount']	= $saveAccount;
		if($type == 'account1'){
		}
		else{
			$data['channel']				= $this->{$this->globalConfig['account1Liberary']}->getAllChannel();
			$data['getAllTax']				= $this->{$this->globalConfig['account2Liberary']}->getAllTax(); 
			$data['warehouse']				= $this->{$this->globalConfig['account1Liberary']}->getAllLocation();
			$data['pricelist']				= $this->{$this->globalConfig['account1Liberary']}->getAllPriceList();
			$data['accountInfo']			= $this->{$this->globalConfig['account2Liberary']}->getOrganizationInfo();
			$data['IncomeAccountRef']		= $this->{$this->globalConfig['account2Liberary']}->getAccountDetails();
			$data['PaymentMethodRef']		= $this->{$this->globalConfig['account2Liberary']}->getAllPaymentMethod(); 
			$data['prepaymentAccount']		= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
			$data['account1Fieldconfigso']	= $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();
			$data['account1Fieldconfigpo']	= $this->{$this->globalConfig['account1Liberary']}->purchaseOrderFieldConfig();
		}
		return $data;
	}
	public function delete($id){
		if($id){
			$saveDataInfo	= $this->db->get_where('account_xero_config',array('id' => $id))->row_array();
			$storeDataLogs	= array(
				'type'			=> 'xero_config_deleted',
				'logs'			=> json_encode($saveDataInfo),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'ipAddress'		=> getenv('REMOTE_ADDR'),
			);
			$this->db->where(array('id' => $id))->delete('account_xero_config');
			$this->db->insert('configlogs', $storeDataLogs);
		}
	}
	public function save($data){
		$accountsData	= $this->db->get_where('account_xero_account', array('id' => $data['xeroAccountId']))->row_array();
		$data['name']	= $accountsData['name'];
		
		if(!empty($data)){
			foreach($data as $dataKey => $dataValue){
				if(is_array($data[$dataKey])){
					$dataValue		= array_filter($dataValue);
					$dataValue		= array_unique($dataValue);
					$data[$dataKey]	= implode(",",$dataValue);
				}
			}
		}
		
		if($data['id']){
			$saveDataInfo			= array();
			$saveDataInfo['old']	= $this->db->get_where('account_xero_config',array('id' => $data['id']))->row_array();
			$saveDataInfo['new']	= $data;
			$storeDataLogs	= array(
				'ipAddress'		=> getenv('REMOTE_ADDR'),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'type'			=> 'xero_config_updated',
				'logs'			=> json_encode($saveDataInfo),
			);
			
			$status	= $this->db->where(array('id' => $data['id']))->update('account_xero_config', $data);
			
			if($status){$this->db->insert('configlogs', $storeDataLogs);}
		}
		else{
			$saveConfig		= $this->db->get_where('account_xero_config', array('xeroAccountId' => $data['xeroAccountId']))->row_array();
			if($saveConfig){
				$saveDataInfo			= array();
				$saveDataInfo['old']	= $saveConfig;
				$saveDataInfo['new']	= $data;
				$storeDataLogs	= array(
					'ipAddress'		=> getenv('REMOTE_ADDR'),
					'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
					'type'			=> 'xero_config_updated',
					'logs'			=> json_encode($saveDataInfo),
				);
				$data['id']	= $saveConfig['id'];
				$status		= $this->db->where(array('id' => $data['id']))->update('account_xero_config', $data);
				$this->db->insert('configlogs', $storeDataLogs);
			}
			else{
				$status		= $this->db->insert('account_xero_config', $data);
				$data['id']	= $this->db->insert_id();
				if($data['id']){
					$storeDataLogs	= array(
						'ipAddress'		=> getenv('REMOTE_ADDR'),
						'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
						'type'			=> 'xero_config_added',
						'logs'			=> json_encode($data),
					);
					$this->db->insert('configlogs', $storeDataLogs);
				}
			}
		}
		
		$data		= $this->db->get_where('account_xero_config', array('id' => $data['id']))->row_array();
		$this->{$this->globalConfig['account2Liberary']}->createDefaultItems($data);
		$data		= $this->db->get_where('account_xero_config', array('id' => $data['id']))->row_array();
		if($data['id']){
			$data['status']	= '1';
		}
		return $data;
	}
}