<?php
#[\AllowDynamicProperties]
class Config_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->ci = get_instance();
	}
	public function get($type = ''){
		$data					= array();
		$data['data']			= $this->db->get('account_brightpearl_config')->result_array();
		$data['saveAccount']	= $this->db->get('account_brightpearl_account')->result_array();
		if($type == 'account1'){
			$data['accountinfo']			= $this->{$this->globalConfig['account1Liberary']}->getAccountInfo();
			$data['defaultPaymentMethod']	= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
			$data['orderstatus']			= $this->{$this->globalConfig['account1Liberary']}->getAllOrderStatus();
			$data['account1APIField']		= $this->{$this->globalConfig['account1Liberary']}->salesOrderAPIField();
			$data['account1Fieldconfigso']	= $this->{$this->globalConfig['account1Liberary']}->salesOrderFieldConfig();
		}
		else{
			$data['accountinfo']			= $this->{$this->globalConfig['account2Liberary']}->getAccountInfo();
			$data['pricelist']				= $this->{$this->globalConfig['account2Liberary']}->getAllPriceList();
			$data['warehouse']				= $this->{$this->globalConfig['account2Liberary']}->getAllLocation();
			$data['channel']				= $this->{$this->globalConfig['account2Liberary']}->getAllChannel();
			$data['orderstatus']			= $this->{$this->globalConfig['account2Liberary']}->getAllOrderStatus();
			$data['shippingmethods']		= $this->{$this->globalConfig['account2Liberary']}->getAllShippingMethod();
			$data['tax']					= $this->{$this->globalConfig['account2Liberary']}->getAllTax();
			$data['tag']					= $this->{$this->globalConfig['account2Liberary']}->getAllTag();
			$data['nominalCode']			= $this->{$this->globalConfig['account2Liberary']}->nominalCode(); 
			$data['defaultPaymentMethod']	= $this->{$this->globalConfig['account1Liberary']}->getAllPaymentMethod();
		}
		return $data;
	}
	public function delete($id){
		if($id){
			$saveDataInfo	= $this->db->get_where('account_brightpearl_config',array('id' => $id))->row_array();
			$storeDataLogs	= array(
				'type'			=> 'brightpearl_config_deleted',
				'logs'			=> json_encode($saveDataInfo),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'ipAddress'		=> getenv('REMOTE_ADDR'),
			);
			$this->db->where(array('id' => $id))->delete('account_brightpearl_config');
			$this->db->insert('configlogs', $storeDataLogs);
		}
	}
	public function save($data){
		$accountsData	= $this->db->get_where('account_brightpearl_account', array('id' => $data['brightpearlAccountId']))->row_array();
		$data['name']	= $accountsData['name'];
		
		if(!empty($data)){
			foreach($data as $dataKey => $dataValue){
				if(is_array($data[$dataKey])){
					$dataValue		= array_filter($dataValue);
					$dataValue		= array_unique($dataValue);
					$data[$dataKey]	= implode(",",$dataValue);
				}
			}
		}
		
		if($data['id']){
			$saveDataInfo			= array();
			$saveDataInfo['old']	= $this->db->get_where('account_brightpearl_config',array('id' => $data['id']))->row_array();
			$saveDataInfo['new']	= $data;
			$storeDataLogs	= array(
				'ipAddress'		=> getenv('REMOTE_ADDR'),
				'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
				'type'			=> 'brightpearl_config_updated',
				'logs'			=> json_encode($saveDataInfo),
			);
			
			$status	= $this->db->where(array('id' => $data['id']))->update('account_brightpearl_config', $data);
			
			if($status){$this->db->insert('configlogs', $storeDataLogs);}
		}
		else{
			$saveConfig		= $this->db->get_where('account_brightpearl_config', array('brightpearlAccountId' => $data['brightpearlAccountId']))->row_array();
			if($saveConfig){
				$saveDataInfo			= array();
				$saveDataInfo['old']	= $saveConfig;
				$saveDataInfo['new']	= $data;
				$storeDataLogs	= array(
					'ipAddress'		=> getenv('REMOTE_ADDR'),
					'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
					'type'			=> 'brightpearl_config_updated',
					'logs'			=> json_encode($saveDataInfo),
				);
				$data['id']	= $saveConfig['id'];
				$status		= $this->db->where(array('id' => $data['id']))->update('account_brightpearl_config', $data);
				$this->db->insert('configlogs', $storeDataLogs);
			}
			else{
				$status		= $this->db->insert('account_brightpearl_config', $data);
				$data['id']	= $this->db->insert_id();
				if($data['id']){
					$storeDataLogs	= array(
						'ipAddress'		=> getenv('REMOTE_ADDR'),
						'userId'		=> ($this->session->userdata('login_user_data')['username']) ? ($this->session->userdata('login_user_data')['username']) : '',
						'type'			=> 'brightpearl_config_added',
						'logs'			=> json_encode($data),
					);
					$this->db->insert('configlogs', $storeDataLogs);
				}
			}
		}
		$data		= $this->db->get_where('account_brightpearl_config', array('id' => $data['id']))->row_array();
		if($data['id']){
			$data['status']	= '1';
		}
		return $data;
	}
}