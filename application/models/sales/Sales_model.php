<?php
#[\AllowDynamicProperties]
class Sales_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function fetchSales($orderId = '', $accountId = ''){
		$fetchby			= $orderId;
		$saveTime			= date('Y-m-d\TH:i:s',strtotime('-250 min'));
		$salesDatass		= $this->{$this->globalConfig['fetchSalesOrder']}->fetchSales($orderId, $accountId);
		$fatchedOrderIds	= array();
		foreach($salesDatass as $fetchAccount1Id => $salesDatassTemps){
			if(isset($salesDatassTemps['saveTime'])){
				$saveTime	= $salesDatassTemps['saveTime'] - (60*10);
			}
			$salesDatas		= (isset($salesDatassTemps['return'])) ? ($salesDatassTemps['return']) : array();
			$batchInsert	= array();
			$batchUpdate	= array(); 
			foreach($salesDatas as $account1Id => $salesData){
				$savedOrdersInDB	= array();
				$savedArchivedIds1	= array();
				$savedArchivedIds2	= array();
				
				$savedDatasTemp	= $this->db->select('id,orderId,createOrderId,status,isPaymentCreated')->get_where('sales_order')->result_array();
				if(!empty($savedDatasTemp)){
					foreach($savedDatasTemp as $savedDatasTemps){
						$savedOrdersInDB[$savedDatasTemps['orderId']]	= $savedDatasTemps;
					}
				}
				
				$savedDatasTemp	= $this->db->select('orderId')->get_where('sales_order_archived')->result_array();
				if(!empty($savedDatasTemp)){
					foreach($savedDatasTemp as $savedDatasTemps){
						$savedArchivedIds1[$savedDatasTemps['orderId']]	= $savedDatasTemps;
					}
				}
				
				$savedDatasTemp	= $this->db->select('orderId')->get_where('z_old_salesIds')->result_array();
				if(!empty($savedDatasTemp)){
					foreach($savedDatasTemp as $savedDatasTemps){
						$savedArchivedIds2[$savedDatasTemps['orderId']]	= $savedDatasTemps;
					}
				}
				
				foreach($salesData as $orderId => $row){
					if(!$orderId){continue;}
					if(isset($savedArchivedIds1[$orderId])){continue;}
					if(isset($savedArchivedIds2[$orderId])){continue;}
					if(isset($savedOrdersInDB[$orderId])){
						if($savedOrdersInDB[$orderId]['status'] == 4){continue;}
						$row['orders']['id']		= $savedOrdersInDB[$orderId]['id'];
						$row['orders']['status']	= $savedOrdersInDB[$orderId]['status'];
						$batchUpdate[]	= $row['orders'];
					}
					else{
						$batchInsert[]	= $row['orders'];
					}
					$fatchedOrderIds[]	= $orderId;
				}
			}
			$inserted		= 0;
			$updateOrder	= 100;
			if($batchUpdate){
				$inserted		= '1';
				$batchUpdates	= array_chunk($batchUpdate,$updateOrder,true);
				foreach($batchUpdates as $batchUpdate){
					if($batchUpdate){
						$this->db->update_batch('sales_order', $batchUpdate,'id');
					}
				}
			}
			if($batchInsert){
				$inserted		= '1';
				$batchInserts	= array_chunk($batchInsert,$updateOrder,true); 
				foreach($batchInserts as $batchInsert){
					if($batchInsert){
						echo $this->db->insert_batch('sales_order', $batchInsert); 
					}
				}
			}
			if($inserted){
				if(!$fetchby){
					$this->db->insert('cron_management', array('type' => 'sales'.$fetchAccount1Id,'saveTime' => $saveTime)); 
				}
			}
		}
		$this->{$this->globalConfig['fetchSalesOrder']}->fetchSalesPayment($fatchedOrderIds);
	}
	public function postSales($orderId = ''){
		$this->{$this->globalConfig['postSalesOrder']}->postSales($orderId);
		$this->{$this->globalConfig['fetchSalesOrder']}->postSalesPayment($orderId);
	}
}