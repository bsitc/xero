# Changelog, Xero
All notable changes to this project will be documented in this file.


## [2.0.0] - 2023-04-17


### ClientSpecifChanges
	-	skip cogs fetching for a comapny in Keen
	-	skip cogs posting for a comapny in Keen

###	Added
	-	sku trimming is applicable for all clients now
	-	added a option in consolidation mapping to manage the use of nominal mapping
	-	checking the stockStatusCondition for updating the status of orderId in consolCogsPosting
	-	Email Alert added when Xero App Disconnects
	-	GRNI Journal
	-	option to disable nominal mapping in consolidation
###	Change
	-	PHP 8.2 version update
	-	added the suffix in the reference of individual stockAdjustments
	-	removed the nominal mapping in the posting of individual stockAdjustments
	-	description in postAmazonFeeOther
	-	removed the storing of address and orderrows in PO/PC/SO/SC
	-	optimized the logStoring functionality
	-	optimized the fetching of otherAmazonFees
	-	added SMTP password and username fields for email alerts
	- 	Archive functionality.
	-	SKU limit 30 is removed for all NON-IM connectors
	-	description limit is set 4000 for all connectors
###	Fixed
	-	added the roundOff in the SO/SC individual payments in case of base currency consolidation
	-	fixed the duplicate lineitem issue after formatting the lineItem sequence
	-	syntaxIssue in postSalesCreditPayment to brightpearl
	-	NULL account reference issue in customer fetching











## [1.0.4] - 2022-11-09

###	Added
	-	COGS, Amazon Fee archiving
	-	Payment due date customization (client specific)

###	Change
	-	Line itme in refrence mapping (Removed for SC in Hawke US UK AND REPO)



## [1.0.3] - 2022-08-29

###	Fixed
	-	issue in individual payment for consolidated orders, (triggred on LGR)
	
###	Change
	-	Amazon Fee other, added BR type journal handling


## [1.0.2] - 2022-08-18

###	Change
	-	Line itme in refrence mapping (moved to first Line)


## [1.0.1] - 2022-08-18

###	Added
	-	Other Amazon Fee Consolidation
	-	Line itme in refrence mapping
	-	PO Consol Option based on Supplier Account Codes
	-	Pending COGS Journal IDs in the email Alerts
	-	Contact Update customization for specific client (Hawke Optics USA)
###	Fixed
	-	default nominal in consol stock adustment based of nominal number range(asset account when 1000-1999, else stock account)
	-	Missing Reference issue in SO/SC individual payments in case of NULL journalID
###	Change
	-	coupon nominal change in individual sales (neha)
	-	coupon amount logic in Buldleproduct case



## [1.0.0] - 2022-06-07

###	Added
	-	Export function in COGS journal page
	-	Purchase consolidation for batch invoices
	-	LineType(tax inc. or tax exc.) condition in cogsJournal (client specific, currently keenxero)
	-	added brightpearl api field to xero category mapping on individual so/sc
	-	added line item sorting in consolidation so/sc
###	Changed
	-	contact linking logic
	-	stop sending nominal on taxItem in case of `Send tax as line 'Yes'`, in consolidation SO/SC/NETOFF
	-	added client code for trimming the SKU upto 30 (client specific, currently keenxero,bfxero,arten,logicofenglishxero,tinyexplorerxero)
	-	added exclude string condition in consolidation even if it is a API field consolidation
###	Fixed
	-	issue fixed in individual sales/credits in consolidation mapping, exclude string condition missing a else condition, (wild-cosmetics)
	-	fixed product mapping issue in case of DisableSKUDetails in PO/PC/POConsol
	-	skip the journal line in individual COGS posting if line amount is zero
	-	issue in archiving code
	-	pending SC payment not getting triggered in email alerts
	-	in case case of partial shipment in cogs consolidation, status not getting update in DB, throwing a DB error and terminate rest of the script
	-	4 decimal issue in amazon consolidation payments
	-	issue in so/sc individual payment code, if consolidation is enable but there isn't any consolidation mapping payment code was not working
	-	added 6 hours delay in some condition for so/sc individual payment in case of error