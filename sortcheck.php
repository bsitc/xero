<?php
function getDirContents($dir, $filter = '', &$results = array()) {
    $files = scandir($dir);
    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value); 

        if(!is_dir($path)) {
			$filename = basename($path);
			if($filename == 'sortcheck.php'){
				continue;
			}
            if(empty($filter) || preg_match($filter, $path)){
				$fileContents = file_get_contents($path);
				if(substr_count(strtolower($fileContents),'sort=')){
					$matches = preg_match_all("/sort=(.*?)\|+/i", $fileContents, $testarray); 
					if($testarray['0']){
						$results[] = array('path' => $path,'testarray' => $testarray['0']);
						foreach($testarray['0'] as $findText){
							$replaceText = explode("|",$findText)['0'].".";
							$fileContents = str_replace($findText,$replaceText,$fileContents);
							file_put_contents($path,$fileContents);
						}
					}
				}
			}
        } elseif($value != "." && $value != "..") {
            getDirContents($path, $filter, $results);
        }
    }
    return $results;
}
$initialDir = dirname(__FILE__) ;
$results = getDirContents($initialDir, '/\.php$/');  
echo "<pre>";print_r($results); echo "</pre>";die(__FILE__.' : Line No :'.__LINE__);
?>