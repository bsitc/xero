<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ERROR); 
define('BASEPATH','');

define("DS", DIRECTORY_SEPARATOR);
$dirname = dirname(__FILE__);
/* $command = "cd ".$dirname;
exec($command);
$command = "git reset --hard";
exec($command);
$command = "git pull origin master";
exec($command); */
$sqltext = file_get_contents($dirname.DS.'dbchange.sql');
if($sqltext){
	include($dirname.DS."application".DS."config".DS."database.php");	
	$SqlConn = new SqlConn($db);
	$SqlConn->processQuery($sqltext);
	/* $createTableQuery = "CREATE TABLE IF NOT EXISTS `git_db_deploy` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `sqlquery` longtext,
	  PRIMARY KEY (id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$SqlConn->processQuery($createTableQuery);
	$sql = 'SELECT * from git_db_deploy where sqlquery="'.$SqlConn->connection->real_escape_string($sqltext).'"';
	$getRow = $SqlConn->getRow($sql);
	if(!$getRow){
		$insertArray = array(
			'id' => '',
			'sqlquery'	=> $sqltext,
		);	
		$SqlConn->insertArray('git_db_deploy',array($insertArray));
		$SqlConn->processQuery($sqltext);
	} */
}

class SqlConn{
	public $connection,$user,$pass,$bridge5db,$host;
	function __construct($dbconfig){
		if(!$dbconfig['default']['database']){return false;}
		$this->user = $dbconfig['default']['username'];
		$this->pass = $dbconfig['default']['password'];
		$this->bridge5db = $dbconfig['default']['database'];
		$this->host = 'localhost';
		$this->connection = new mysqli($this->host, $this->user, $this->pass,$this->bridge5db); 
		if (!$this->connection) {
			die("Connection failed: " . mysqli_connect_error());
		}
	}
	public function __destruct(){
		$this->connection->close();
	}
	public function processQuery($sql = ''){
		if($sql){
			$result = $this->connection->query($sql);
			return $result;
		}
	}
	public function getRowList($sql){
		if($sql){
			$result  =  $this->connection->query($sql);
			$rows = array();
			if(@$result->num_rows){
				while($row = $result->fetch_assoc()){
						$rows[] = $row;
				}
			}
			return $rows;
		}
	}
	public function getRow($sql){
		if($sql){
			$result  =  $this->connection->query($sql);
			$rows = array();
			if(@$result->num_rows){
				$rows = $result->fetch_assoc();
			}
			return $rows;
		}		
	}
	public function insertArray($tablename,$datas){

		if(($tablename)&&($datas)){
			$datas = ($datas['0'])?$datas:array($datas);
			$queryval = ' VALUES ';
			$querykey = ' (';
			$countkey = 1;
			foreach($datas as $data){
				if($countkey){
					$keysVals = array_keys($data);
					foreach($keysVals as $keysVal){
						$querykey .= '`'.$this->connection->real_escape_string($keysVal)."`,";
					}
					$countkey = 0;
				}
				$queryval .= '(';
				foreach($data as $value){
					$queryval .= "'".$this->connection->real_escape_string($value)."',";
				}
				$queryval = rtrim($queryval,',');
				$queryval .= '),';
			}
			$queryval = rtrim($queryval,",");
			$querykey = rtrim($querykey,",").") ";
			$sql = "INSERT INTO ".$tablename. $querykey. $queryval;
			$res = $this->connection->query($sql);  
		}
	}
	public function updateArray($tablename,$data,$where = 'id'){
		if(($tablename)&&($data)){
			$subquery = ' SET ';			
			foreach($data as $key => $value){
				if($value)
					$subquery .= $key ." = '".$this->connection->real_escape_string($value)."',";
			}
			$subquery = rtrim($subquery,",");
			if($data[$where]){
				$sql = "UPDATE ".$tablename. $subquery . " WHERE ".$where. " = '" . $this->connection->real_escape_string($data[$where]) ."'";				
				return $this->connection->query($sql); 
			}
			else {
				return false;
			}
		}
	}
}


?>