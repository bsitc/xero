<?php declare(strict_types = 1);

// odsl-/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries
return \PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => 'v1',
   'data' => 
  array (
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/Template.php' => 
    array (
      0 => '410985a0434e15e51be9433ffe8beae48cfee97e',
      1 => 
      array (
        0 => 'template',
      ),
      2 => 
      array (
        0 => 'load_template',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/Xero.php' => 
    array (
      0 => '4a425e8af0db562bc9f388e92f4b51dc3f52d301',
      1 => 
      array (
        0 => 'xero',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'reinitialize',
        2 => 'initializeconfig',
        3 => 'getcurl',
        4 => 'refreshtoken',
        5 => 'flattenassocarray',
        6 => 'getnonce',
        7 => 'generatesignature',
        8 => 'getconnections',
        9 => 'getproducts',
        10 => 'postproducts',
        11 => 'postcustomers',
        12 => 'postsales',
        13 => 'postaggregationsales',
        14 => 'postaggregationsalescredit',
        15 => 'postsalescredit',
        16 => 'postpurchase',
        17 => 'postpurchasebatchinvoice',
        18 => 'postpurchasecredit',
        19 => 'poststockadjustment',
        20 => 'fetchsalespayment',
        21 => 'postaggregationsalespayment',
        22 => 'postaggregationsalescreditpayment',
        23 => 'postsalespayment',
        24 => 'fetchsalescreditpayment',
        25 => 'postsalescreditpayment',
        26 => 'fetchpurchasepayment',
        27 => 'fetchpurchaseconsolpayment',
        28 => 'fetchpurchasecreditpayment',
        29 => 'postjournal',
        30 => 'postconsolidatedjournal',
        31 => 'postcogsjournal',
        32 => 'postcogswoinvoice',
        33 => 'postconsolcogsjournal',
        34 => 'postconsolidatedcogswoinvoice',
        35 => 'postconsolstockjournal',
        36 => 'postnetoffconsolorder',
        37 => 'postnetoffconsolpayment',
        38 => 'postamazonfeeother',
        39 => 'postgrnijournal',
        40 => 'postaradvance',
        41 => 'applyaradvance',
        42 => 'reversearaadvance',
        43 => 'getallchannelmethod',
        44 => 'getallpaymentmethod',
        45 => 'getalltax',
        46 => 'getaccountdetails',
        47 => 'getorganizationinfo',
        48 => 'getalllocation',
        49 => 'getallshippingmethod',
        50 => 'getallregisters',
        51 => 'getallsalesrep',
        52 => 'getallallowance',
        53 => 'gettradingpartnerid',
        54 => 'getallbrand',
        55 => 'getallsupplier',
        56 => 'getaccountinfo',
        57 => 'getallpricelist',
        58 => 'callfunction',
        59 => 'array_to_xml',
        60 => 'fetchavalarataxrate',
        61 => 'fetchsalesreport',
        62 => 'sendtokenalert',
        63 => 'createdefaultitems',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/Common.php' => 
    array (
      0 => '84c6c0afb566e649b740c50e591345fdda2bf05b',
      1 => 
      array (
        0 => 'common',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getcurrencyrate',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/Brightpearl.php' => 
    array (
      0 => '12ce07dd5aabdfeaf05725831ea73d4661d9185f',
      1 => 
      array (
        0 => 'brightpearl',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'reinitialize',
        2 => 'generatetoken',
        3 => 'getcurl',
        4 => 'range_string',
        5 => 'getresultbyid',
        6 => 'fetchproducts',
        7 => 'fetchcustomers',
        8 => 'fetchsales',
        9 => 'fetchsalespayment',
        10 => 'postsalespayment',
        11 => 'fetchpurchase',
        12 => 'postpurchasepayment',
        13 => 'postpurchaseconsolpayment',
        14 => 'postsalescreditpayment',
        15 => 'fetchsalescreditpayment',
        16 => 'postpurchasecreditpayment',
        17 => 'postgoodsdispatch',
        18 => 'postsalescreditconfimation',
        19 => 'postacknowledgement',
        20 => 'updateproductcustomfileds',
        21 => 'fetchsalescredit',
        22 => 'fetchpurchasecredit',
        23 => 'fetchstockadjustment',
        24 => 'fetchjournal',
        25 => 'fetchcogsjournal',
        26 => 'fetchcogswoinvoice',
        27 => 'fetchstockjournal',
        28 => 'fetchamazonfeeother',
        29 => 'fetchgrnijournal',
        30 => 'fetcharadvance',
        31 => 'callfunction',
        32 => 'fetchjournalbyids',
        33 => 'getproductstock',
        34 => 'getproductpricelist',
        35 => 'getallshippingmethod',
        36 => 'getexchangerate',
        37 => 'getallbpexchangerate',
        38 => 'getallcurrency',
        39 => 'getallcurrencywithrate',
        40 => 'getalllocation',
        41 => 'getallchannel',
        42 => 'getallorderstatus',
        43 => 'getallcategorymethod',
        44 => 'getalltax',
        45 => 'getseason',
        46 => 'getallpricelist',
        47 => 'nominalcode',
        48 => 'getalltag',
        49 => 'getallsalesrep',
        50 => 'getallprojects',
        51 => 'getallbrand',
        52 => 'getallchannelmethod',
        53 => 'getallteam',
        54 => 'getallleadsource',
        55 => 'getallpaymentmethod',
        56 => 'getaccountinfo',
        57 => 'getallallowance',
        58 => 'getallsalescustomfield',
        59 => 'getallsalescustomfieldfortax',
        60 => 'salesorderapifield',
        61 => 'salesorderfieldconfig',
        62 => 'purchaseorderfieldconfig',
        63 => 'fetchsalesreport',
        64 => 'closeallpayments',
        65 => 'removeerrorflag',
        66 => 'sendapialert',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/Mailer.php' => 
    array (
      0 => '50ab157801f44f7237df860914352adfdb200a22',
      1 => 
      array (
        0 => 'mailer',
      ),
      2 => 
      array (
        0 => 'send',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-lv.php' => 
    array (
      0 => '04858197d206b0abaa31a68baaa911cdee93dc3d',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-fr.php' => 
    array (
      0 => '7adbbee2a253cfd19d3fca6eb5bf198dbabfc240',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-eo.php' => 
    array (
      0 => '9b26b5fbe0586fbe3eb4cb91cb166156c7adf6ed',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-pt_br.php' => 
    array (
      0 => 'bf4cd1d7a6354de700bdca67ec66abdc6da8fd09',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ru.php' => 
    array (
      0 => '28674479b876af9f65972b69d4f73458276a3bdf',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-bg.php' => 
    array (
      0 => '133636eb771cf677159a863694daeb3dbeacc984',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ja.php' => 
    array (
      0 => 'd3a3192ff2a670a69303f478a8e28d94bb1accad',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-sk.php' => 
    array (
      0 => 'fa528d1286be3ad68e58911f9fdd070f7a730fd7',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ro.php' => 
    array (
      0 => '858ab70c8de66c3cc58d9581e7bf46b7578ca37d',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-gl.php' => 
    array (
      0 => 'e426d75bffb904979c68254a4c18766ce94dd5ee',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-fo.php' => 
    array (
      0 => 'a65922ed3fa3c5a9c3229687a5e05db17c3006eb',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-de.php' => 
    array (
      0 => '8a8359e445cf4a816783ae7cd850aaf19b3ef4e0',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-hr.php' => 
    array (
      0 => '525f0cf2019f54be155bc8e848489b5b3ae40fd5',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-nb.php' => 
    array (
      0 => '2beb966c31b1227a370195ec7861666d86132446',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-da.php' => 
    array (
      0 => '8cca4cbaf5c80b314357b02b6b44c7d139b42cd2',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ms.php' => 
    array (
      0 => '73842c4ab191110f704751aa984cd3bd81196736',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ar.php' => 
    array (
      0 => 'f919d19590bf936442e6513acdfdb9cf290b1bae',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-fa.php' => 
    array (
      0 => 'd5f2690133cb6a4c4352fd7565140574fd5b1387',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-fi.php' => 
    array (
      0 => '698dc052874dbf53aa966171d2ff35d8d5df319c',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-it.php' => 
    array (
      0 => '30d76c6996cda024bfbc8f9718b6fe9794013bb9',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ka.php' => 
    array (
      0 => '99fd3d8c086ae35adac0119bed64aebe8d9d6fe7',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-hu.php' => 
    array (
      0 => 'd845ff86e68bd9814b8c667b4e2ef417c0abba77',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-he.php' => 
    array (
      0 => 'd059005712412c6039e1b92529fa0bde5227947a',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-zh_cn.php' => 
    array (
      0 => '1133ba4bac2f69195119fd41ff01eb709fe9d911',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-nl.php' => 
    array (
      0 => '97b933e05c434f27ccd29acf390f3621cd621fcf',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-id.php' => 
    array (
      0 => '59f012fb8b287fed71de97edffe6543d216c228b',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-es.php' => 
    array (
      0 => '785c65c501bab0add0a2d940c6c69c418760abd0',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-el.php' => 
    array (
      0 => 'e35b3583490fd27979d6e99dd483c55576a51c95',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-uk.php' => 
    array (
      0 => '7edb988aada327250c64df0299d6a7856b69b038',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ca.php' => 
    array (
      0 => 'ce23cd7e81d573639114bf8de4d6275f9dec71b6',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-tr.php' => 
    array (
      0 => '9a1a10b1476bca5afd399878a0644908b980558d',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-az.php' => 
    array (
      0 => '0516cb32f176b27388f3fb50d3cb7410b1fcd4cc',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ch.php' => 
    array (
      0 => '81b11f5666d3766ddff01eb6bd9cd6ad4c9c88aa',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-pt.php' => 
    array (
      0 => 'e864c821c3287404fbbd198cc1af744a441c9f5d',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-cs.php' => 
    array (
      0 => '60e94f73f4c73d696ec8a895e743a52117badeb8',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-zh.php' => 
    array (
      0 => 'b1721b5f41009b95474ce8154a1b08517e233d94',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-ko.php' => 
    array (
      0 => 'b0bc1f9509102c2271d3ffafbd2cdd5cc39c1bbf',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-rs.php' => 
    array (
      0 => '3269510260b63524924332d25f00ae48d2549b4f',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-am.php' => 
    array (
      0 => '65d00b88bab0cf3102134cb91321e8c5aab9cec7',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-sl.php' => 
    array (
      0 => '7cb757bf7e6f265ec0f6d123a9faaeb3b0102aba',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-lt.php' => 
    array (
      0 => '07d9e89d8cbb2dc3aa1e08957fb95146e932bf20',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-be.php' => 
    array (
      0 => 'a270e0990ac36ae74641e2b8232b7fa56468c9ed',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-sv.php' => 
    array (
      0 => '74e7f60171454e216c25d510d23488f1b0633ab9',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-et.php' => 
    array (
      0 => 'a3929a2af48aff35af949a4412aecb6ae0516662',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-pl.php' => 
    array (
      0 => '23b3484b94a40b0882d96ad123c3806e850ca16a',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/language/phpmailer.lang-vi.php' => 
    array (
      0 => '81d1244ba4b0e1c26039390dbd4ca5d1baa4ecc3',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/class.phpmailer.php' => 
    array (
      0 => '7456736f5186a99a2ee4c47856678e63fb3e0464',
      1 => 
      array (
        0 => 'phpmailer',
        1 => 'phpmailerexception',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => '__destruct',
        2 => 'mailpassthru',
        3 => 'edebug',
        4 => 'ishtml',
        5 => 'issmtp',
        6 => 'ismail',
        7 => 'issendmail',
        8 => 'isqmail',
        9 => 'addaddress',
        10 => 'addcc',
        11 => 'addbcc',
        12 => 'addreplyto',
        13 => 'addorenqueueanaddress',
        14 => 'addanaddress',
        15 => 'parseaddresses',
        16 => 'setfrom',
        17 => 'getlastmessageid',
        18 => 'validateaddress',
        19 => 'idnsupported',
        20 => 'punyencodeaddress',
        21 => 'send',
        22 => 'presend',
        23 => 'postsend',
        24 => 'sendmailsend',
        25 => 'isshellsafe',
        26 => 'mailsend',
        27 => 'getsmtpinstance',
        28 => 'smtpsend',
        29 => 'smtpconnect',
        30 => 'smtpclose',
        31 => 'setlanguage',
        32 => 'gettranslations',
        33 => 'addrappend',
        34 => 'addrformat',
        35 => 'wraptext',
        36 => 'utf8charboundary',
        37 => 'setwordwrap',
        38 => 'createheader',
        39 => 'getmailmime',
        40 => 'getsentmimemessage',
        41 => 'generateid',
        42 => 'createbody',
        43 => 'getboundary',
        44 => 'endboundary',
        45 => 'setmessagetype',
        46 => 'headerline',
        47 => 'textline',
        48 => 'addattachment',
        49 => 'getattachments',
        50 => 'attachall',
        51 => 'encodefile',
        52 => 'encodestring',
        53 => 'encodeheader',
        54 => 'hasmultibytes',
        55 => 'has8bitchars',
        56 => 'base64encodewrapmb',
        57 => 'encodeqp',
        58 => 'encodeqpphp',
        59 => 'encodeq',
        60 => 'addstringattachment',
        61 => 'addembeddedimage',
        62 => 'addstringembeddedimage',
        63 => 'inlineimageexists',
        64 => 'attachmentexists',
        65 => 'alternativeexists',
        66 => 'clearqueuedaddresses',
        67 => 'clearaddresses',
        68 => 'clearccs',
        69 => 'clearbccs',
        70 => 'clearreplytos',
        71 => 'clearallrecipients',
        72 => 'clearattachments',
        73 => 'clearcustomheaders',
        74 => 'seterror',
        75 => 'rfcdate',
        76 => 'serverhostname',
        77 => 'lang',
        78 => 'iserror',
        79 => 'fixeol',
        80 => 'addcustomheader',
        81 => 'getcustomheaders',
        82 => 'msghtml',
        83 => 'html2text',
        84 => '_mime_types',
        85 => 'filenametotype',
        86 => 'mb_pathinfo',
        87 => 'set',
        88 => 'secureheader',
        89 => 'normalizebreaks',
        90 => 'sign',
        91 => 'dkim_qp',
        92 => 'dkim_sign',
        93 => 'dkim_headerc',
        94 => 'dkim_bodyc',
        95 => 'dkim_add',
        96 => 'haslinelongerthanmax',
        97 => 'gettoaddresses',
        98 => 'getccaddresses',
        99 => 'getbccaddresses',
        100 => 'getreplytoaddresses',
        101 => 'getallrecipientaddresses',
        102 => 'docallback',
        103 => 'errormessage',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/class.phpmaileroauthgoogle.php' => 
    array (
      0 => '31a2ab0c8902c1b719ab335b44a3327d5c0bd159',
      1 => 
      array (
        0 => 'phpmaileroauthgoogle',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getprovider',
        2 => 'getgrant',
        3 => 'gettoken',
        4 => 'getoauth64',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/extras/ntlm_sasl_client.php' => 
    array (
      0 => '9f263944220ea7c280fe5c400287ae28688ccc83',
      1 => 
      array (
        0 => 'ntlm_sasl_client_class',
      ),
      2 => 
      array (
        0 => 'initialize',
        1 => 'asciitounicode',
        2 => 'typemsg1',
        3 => 'ntlmresponse',
        4 => 'typemsg3',
        5 => 'start',
        6 => 'step',
      ),
      3 => 
      array (
        0 => 'SASL_NTLM_STATE_START',
        1 => 'SASL_NTLM_STATE_IDENTIFY_DOMAIN',
        2 => 'SASL_NTLM_STATE_RESPOND_CHALLENGE',
        3 => 'SASL_NTLM_STATE_DONE',
        4 => 'SASL_FAIL',
        5 => 'SASL_CONTINUE',
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/extras/htmlfilter.php' => 
    array (
      0 => '2bf219baec0fcb73e8a263a2c2b9b36d5c02f1fe',
      1 => 
      array (
      ),
      2 => 
      array (
        0 => 'tln_tagprint',
        1 => 'tln_casenormalize',
        2 => 'tln_skipspace',
        3 => 'tln_findnxstr',
        4 => 'tln_findnxreg',
        5 => 'tln_getnxtag',
        6 => 'tln_deent',
        7 => 'tln_defang',
        8 => 'tln_unspace',
        9 => 'tln_fixatts',
        10 => 'tln_fixurl',
        11 => 'tln_fixstyle',
        12 => 'tln_body2div',
        13 => 'tln_sanitize',
        14 => 'htmlfilter',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/extras/EasyPeasyICS.php' => 
    array (
      0 => '5916fca597652a5195ec3e69fd14079a28efd7ee',
      1 => 
      array (
        0 => 'easypeasyics',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'addevent',
        2 => 'getevents',
        3 => 'clearevents',
        4 => 'getname',
        5 => 'setname',
        6 => 'render',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/class.pop3.php' => 
    array (
      0 => '5dad690b9e6ae5875b7633f05939c76d9d4a3eb9',
      1 => 
      array (
        0 => 'pop3',
      ),
      2 => 
      array (
        0 => 'popbeforesmtp',
        1 => 'authorise',
        2 => 'connect',
        3 => 'login',
        4 => 'disconnect',
        5 => 'getresponse',
        6 => 'sendstring',
        7 => 'checkresponse',
        8 => 'seterror',
        9 => 'geterrors',
        10 => 'catchwarning',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/class.smtp.php' => 
    array (
      0 => '89bc6aa6fefecb56e6939c58155870c66495063e',
      1 => 
      array (
        0 => 'smtp',
      ),
      2 => 
      array (
        0 => 'edebug',
        1 => 'connect',
        2 => 'starttls',
        3 => 'authenticate',
        4 => 'hmac',
        5 => 'connected',
        6 => 'close',
        7 => 'data',
        8 => 'hello',
        9 => 'sendhello',
        10 => 'parsehellofields',
        11 => 'mail',
        12 => 'quit',
        13 => 'recipient',
        14 => 'reset',
        15 => 'sendcommand',
        16 => 'sendandmail',
        17 => 'verify',
        18 => 'noop',
        19 => 'turn',
        20 => 'client_send',
        21 => 'geterror',
        22 => 'getserverextlist',
        23 => 'getserverext',
        24 => 'getlastreply',
        25 => 'get_lines',
        26 => 'setverp',
        27 => 'getverp',
        28 => 'seterror',
        29 => 'setdebugoutput',
        30 => 'getdebugoutput',
        31 => 'setdebuglevel',
        32 => 'getdebuglevel',
        33 => 'settimeout',
        34 => 'gettimeout',
        35 => 'errorhandler',
        36 => 'recordlasttransactionid',
        37 => 'getlasttransactionid',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/class.phpmaileroauth.php' => 
    array (
      0 => '5b9c014d2c433e00c0c2c2a646e282dd92dc01f1',
      1 => 
      array (
        0 => 'phpmaileroauth',
      ),
      2 => 
      array (
        0 => 'getoauthinstance',
        1 => 'smtpconnect',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/phpmailer/PHPMailerAutoload.php' => 
    array (
      0 => '6705b087680747bc472dc1822f375a1404bc6eb5',
      1 => 
      array (
      ),
      2 => 
      array (
        0 => 'phpmailerautoload',
        1 => '__autoload',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postNetOffConsolPayment.php' => 
    array (
      0 => 'da39a3ee5e6b4b0d3255bfef95601890afd80709',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/bkup_postaggregationSalesPayment.php' => 
    array (
      0 => 'da39a3ee5e6b4b0d3255bfef95601890afd80709',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postNetOffConsolOrder.php' => 
    array (
      0 => '4fd6f9fe991ee5e981ece87438dba93ecdac5272',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postCogsjournal.php' => 
    array (
      0 => '854bb4e8f9585fa012c86d8a7437f40385f2dc3f',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postStockAdjustment.php' => 
    array (
      0 => '603950b03930367c497930c317f9a16d2a0c24a1',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postConsolidatedCogswoInvoice.php' => 
    array (
      0 => '54003b665123d4520a9208a317f3871d7e9ced15',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/timezonesData.php' => 
    array (
      0 => 'b1b34b2193eb8109567495d858fcfa8f4fdffc20',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postPurchaseBatchInvoice.php' => 
    array (
      0 => '150dbcc4b47810f56f156cb1858bbce9b7c62491',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postConsolidatedJournal.php' => 
    array (
      0 => '92a330447086f1394d4924d6aab44a9b492e5e06',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postaggregationSalesCredit.php' => 
    array (
      0 => '28f7ac8805b3e829f207746d53c33f1b2bbf056e',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postaggregationSales.php' => 
    array (
      0 => 'beae4612114e39672970aad28a033939a96a9dd6',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postSalesCredit.php' => 
    array (
      0 => '0164e01c328b48fdbe19027c7f78ce5397d372c1',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postConsolStockjournal.php' => 
    array (
      0 => '17f422a3a4418461d9b994a189e3e7fcc64d6dec',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postCustomers.php' => 
    array (
      0 => '0ca89c53509da1c5d9a423809a0b304ca86fad6c',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/reverseAraadvance.php' => 
    array (
      0 => '2ff9011241b73dcf4c0eddb94a4b440788387cd5',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postJournal.php' => 
    array (
      0 => '352ae6cbf5cb806ee004e8ecf8794d9adab576ea',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/fetchSalesPayment.php' => 
    array (
      0 => 'd729231051a3db596caf71faf4004ce9c58efdfc',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postConsolidatedCogsJournal.php' => 
    array (
      0 => '0a657671913c3b914ff39b75b9a1ba44462da310',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postaggregationSalescreditPayment.php' => 
    array (
      0 => 'd1dcf30daefaece59fc58b5f648053ea38a4b561',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/fetchPurchasePayment.php' => 
    array (
      0 => '2b58598e0b290b20784522250b255964f2dbfe39',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postCogswoInvoice.php' => 
    array (
      0 => 'c90e2955ad1f1dfdf9406404986c5010299ca9fd',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postAmazonFeeOther.php' => 
    array (
      0 => 'a53c500e42f8b2d5fc439eb04126e526ede398ab',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postaggregationSalesPayment.php' => 
    array (
      0 => 'c04f002ad1d36e10257ad392c30abedc26648199',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/fetchSalesCreditPayment.php' => 
    array (
      0 => 'eaad702166a37bf91a78611c7a9dd58a789bd6e7',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postSalesPayment.php' => 
    array (
      0 => '689e457c118a598a1d92ef6c84e6f1379342a2ac',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postPurchaseCredit.php' => 
    array (
      0 => '3a237a97d68b31182d6d63021da61138938bb8a1',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postSalesCreditPayment.php' => 
    array (
      0 => 'ff9be8281b53eb2331df96d7c4cd460799fc0e70',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postAradvance.php' => 
    array (
      0 => '94c033e07f91f43c5dcd3b5552efdfd0dea91cce',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postSales.php' => 
    array (
      0 => '7a37c6750e448518de4aba436241c23fe5622630',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postPurchase.php' => 
    array (
      0 => '16145a4a4986f7daaddcd788b1384ed44082a329',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postGrnijournal.php' => 
    array (
      0 => 'd00c58433bfb747174f6769d4c259fe881f634d7',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/fetchPurchaseCreditPayment.php' => 
    array (
      0 => '881541777113fc72c395cbcc08151d7873f8a5d9',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/fetchPurchaseConsolPayment.php' => 
    array (
      0 => '57f61c0d6388bedabd69a19db2f661d0dd4a4ed5',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/applyAradvance.php' => 
    array (
      0 => 'f261993798f0b4b8aac3b10ffd79a702f38f3a30',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/postProducts.php' => 
    array (
      0 => 'c9eb8fc07513e9461d4b9ebcff6debaac8d222e0',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/xero/netoffBackup.php' => 
    array (
      0 => 'da39a3ee5e6b4b0d3255bfef95601890afd80709',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchCustomers.php' => 
    array (
      0 => '29ef2c7615d76cfc6b621c0c205fe0f7d9bb7c8e',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchJournal.php' => 
    array (
      0 => '0e5a75ca74a6f379f34d6527570ef11b576de71e',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchCogswoinvoice.php' => 
    array (
      0 => 'ddd985127867668949e870cbe530569249c2f986',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchPurchaseCredit.php' => 
    array (
      0 => '4c7a38beeedc4c2258d6c3316ee2d99383290010',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchAmazonFeeOther.php' => 
    array (
      0 => 'ccc53b55e332581e83b0ee3fad5980a49eff4238',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchSales.php' => 
    array (
      0 => '62b53942e8419d0d2d4908bbe9959a7dfbef62e0',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchStockjournal.php' => 
    array (
      0 => '404a6e2417549b2db999ca04dcebe5e3798e2ba5',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchSalesPayment.php' => 
    array (
      0 => '17893951f56eb9aabd3a1efe5c07df3d1f13760a',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchProducts.php' => 
    array (
      0 => '5afb46b5cae0b2e7fc39214c288e2e30789dba84',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchStockAdjustment.php' => 
    array (
      0 => '39a35f7c367ca33a4276af92c944f17d3ed84516',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchAradvance.php' => 
    array (
      0 => '7921c9ffb0c7298c78915b451d1429a215499640',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/postPurchaseCreditPayment.php' => 
    array (
      0 => '7cc4cc4dfcd6444fb8cde60b7448ecee50a94b6e',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchSalesCreditPayment.php' => 
    array (
      0 => 'c2e6be9e1308c6ab460efa5be3316c0cd4c0f942',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/postSalesPayment.php' => 
    array (
      0 => 'd6ca817bbba4deb3d3763a0b76a1fcc53f5761c1',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchPurchase.php' => 
    array (
      0 => '76691051ff2a5a2b38fe13d4bc396293d05497e3',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchSalesCredit.php' => 
    array (
      0 => '819aa9195f1d1897dd0c156bfeb53484101a8041',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/postSalesCreditPayment.php' => 
    array (
      0 => '8d85b4acba79ebc12e9ea45d48349a98a6d223ac',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/postPurchasePayment.php' => 
    array (
      0 => 'f88d0e5ce041e0d45d9a5724e11fee1fc908817d',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/postPurchaseConsolPayment.php' => 
    array (
      0 => '4bcad64d59313ec5b01364e7b381a85900bb67e9',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchGrnijournal.php' => 
    array (
      0 => 'd4c8d717dde08e828541bfbb9000c0f835be88a5',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/brightpearl/xero/fetchCogsjournal.php' => 
    array (
      0 => '6322ef974791a1acf04f775fba93617fe2f67756',
      1 => 
      array (
      ),
      2 => 
      array (
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/libraries/Mypagination.php' => 
    array (
      0 => 'e25e75cf2fb00a3eb9bec34995622eef1d85a7cd',
      1 => 
      array (
        0 => 'mypagination',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'initialize',
        2 => 'create_links',
        3 => '_parse_attributes',
        4 => '_attr_rel',
        5 => 'getpaginationconfig',
        6 => 'getlistorder',
      ),
      3 => 
      array (
      ),
    ),
  ),
));