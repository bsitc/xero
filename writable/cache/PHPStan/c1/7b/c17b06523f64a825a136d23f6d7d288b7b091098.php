<?php declare(strict_types = 1);

// odsl-/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models
return \PHPStan\Cache\CacheItem::__set_state(array(
   'variableKey' => 'v1',
   'data' => 
  array (
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Trading_model.php' => 
    array (
      0 => 'c2163410b78a1a83a45cfecbd3d474dd990bb9a9',
      1 => 
      array (
        0 => 'trading_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Brand_model.php' => 
    array (
      0 => 'fe300ea67e4dd4be1d8d724dc2e70604596611bb',
      1 => 
      array (
        0 => 'brand_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Genericcustomer_model.php' => 
    array (
      0 => 'ebf9a6a9300b0df6c62157f74bbe034c98c2ae79',
      1 => 
      array (
        0 => 'genericcustomer_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Packedmapping_model.php' => 
    array (
      0 => '70c4ca1852060375a021c82a7021f588a772f745',
      1 => 
      array (
        0 => 'packedmapping_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Warehousestock_model.php' => 
    array (
      0 => 'dc40ffb285ddd87dd97f1f2ea7f5f22879e5ea8a',
      1 => 
      array (
        0 => 'warehousestock_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Pricelist_model.php' => 
    array (
      0 => 'bc419c4c9493417b76eb1857b135427445cd7af0',
      1 => 
      array (
        0 => 'pricelist_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Nominal_model.php' => 
    array (
      0 => 'a75114c6839f0aff87497f80efa96fc3ee2a2b00',
      1 => 
      array (
        0 => 'nominal_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Allowance_model.php' => 
    array (
      0 => '5e8f1c07501b840359a80b7d3e538cf38212a8cf',
      1 => 
      array (
        0 => 'allowance_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Payment_model.php' => 
    array (
      0 => 'faa66d9c2f9153db01cfa6427db225d59d0f8cb9',
      1 => 
      array (
        0 => 'payment_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Productcategory_model.php' => 
    array (
      0 => '4637ce691bdd90899f209c3b1cf4206b6a5df4b4',
      1 => 
      array (
        0 => 'productcategory_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Salesrep_model.php' => 
    array (
      0 => '7d9d2987f1cffeb7af65f3a18142409527c03d51',
      1 => 
      array (
        0 => 'salesrep_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Channelcustomfield_model.php' => 
    array (
      0 => 'e5263b17ea0f2a9b524ac55ce7d35ac3d60d66b5',
      1 => 
      array (
        0 => 'channelcustomfield_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Paymentpurchase_model.php' => 
    array (
      0 => '5498aaaa4ecdb085c9276fdddcb0f68f663faaab',
      1 => 
      array (
        0 => 'paymentpurchase_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Taxasline_model.php' => 
    array (
      0 => '931a8e33dfd9610d21d7d2dc55d605bae80ca7a1',
      1 => 
      array (
        0 => 'taxasline_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Taximacro_model.php' => 
    array (
      0 => '52538ec1d75fb4b9d6240128376842595c9faa12',
      1 => 
      array (
        0 => 'taximacro_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Category_model.php' => 
    array (
      0 => '60c4510b6e717e27640afca4e86123e35a92a410',
      1 => 
      array (
        0 => 'category_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
        3 => 'autosave',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Warehousetransfer_model.php' => 
    array (
      0 => '09fb0dcd3e2c495b445c8807cfc3c141c9022ee1',
      1 => 
      array (
        0 => 'warehousetransfer_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Inventoryadvice_model.php' => 
    array (
      0 => 'eb4861c837bbad88aae7557d78d75310904c2b12',
      1 => 
      array (
        0 => 'inventoryadvice_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Reference_model.php' => 
    array (
      0 => '4dd185ba3eae01c89cd751a9d08814a7136d59d1',
      1 => 
      array (
        0 => 'reference_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Box_model.php' => 
    array (
      0 => '0d0c86928e7b034789685eba86e6069551491e6b',
      1 => 
      array (
        0 => 'box_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Leadsource_model.php' => 
    array (
      0 => 'cf520c45e441a7656e969e8a145b55e027d6e513',
      1 => 
      array (
        0 => 'leadsource_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Assignment_model.php' => 
    array (
      0 => '49eef965451f637c38748e9053ecf29eca290d87',
      1 => 
      array (
        0 => 'assignment_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Warehouse_model.php' => 
    array (
      0 => 'f313154f307bd2266935c87dc5ecdeca59512b12',
      1 => 
      array (
        0 => 'warehouse_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Shipping_model.php' => 
    array (
      0 => '74af1bccbcf6309b329be23e557a51ee9e231c42',
      1 => 
      array (
        0 => 'shipping_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Contactlinking_model.php' => 
    array (
      0 => 'c2f885b866c8eca49d9a63064b949d3e5a8f29ae',
      1 => 
      array (
        0 => 'contactlinking_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Channel_model.php' => 
    array (
      0 => '301679df8bdb3f6a5bf51ddf66957985c7c91541',
      1 => 
      array (
        0 => 'channel_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/mapping/Tax_model.php' => 
    array (
      0 => '595222599434be9798d4137ff15155f40edf8a8e',
      1 => 
      array (
        0 => 'tax_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/purchase/Purchasecredit_model.php' => 
    array (
      0 => '25adfeaf5f13a7c601ebe8ab0dbfac9dde5d400c',
      1 => 
      array (
        0 => 'purchasecredit_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchpurchasecredit',
        2 => 'postpurchasecredit',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/purchase/xero/Purchase_overridemodel.php' => 
    array (
      0 => 'af4343038f5c71272f9bbce266e2b8e6e6d562e4',
      1 => 
      array (
        0 => 'purchase_overridemodel',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'postpurchase',
        2 => 'getpurchase',
        3 => 'getpurchaseitem',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/purchase/xero/Purchasecredit_overridemodel.php' => 
    array (
      0 => '35ff57d10bb8734e108c0ea6aa0923be37fb2019',
      1 => 
      array (
        0 => 'purchasecredit_overridemodel',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getcredit',
        2 => 'postpurchasecredit',
        3 => 'getcredititem',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/purchase/Purchase_model.php' => 
    array (
      0 => 'fc438d5180cc7e8bab6c84495b86b8df66f3d932',
      1 => 
      array (
        0 => 'purchase_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchpurchase',
        2 => 'postpurchase',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/stockjournal/Stockjournal_model.php' => 
    array (
      0 => '445fd0cc99d4fef29ebc0f9744c0dd5df491711f',
      1 => 
      array (
        0 => 'stockjournal_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchstockjournal',
        2 => 'postconsolstockjournal',
        3 => 'getstockjournal',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/sales/Sales_model.php' => 
    array (
      0 => '5c0a5681d309eb97571762c32001c36ecd185503',
      1 => 
      array (
        0 => 'sales_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchsales',
        2 => 'postsales',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/sales/xero/Credit_overridemodel.php' => 
    array (
      0 => 'a461852e9c857d0bf3782be49e61bb924e84c83e',
      1 => 
      array (
        0 => 'credit_overridemodel',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getcredit',
        2 => 'postsalescredit',
        3 => 'postaggregationsalescredit',
        4 => 'postnetoffconsolorder',
        5 => 'getcredititem',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/sales/xero/Sales_overridemodel.php' => 
    array (
      0 => 'b8f9a835754d994a7eba28247a2cd2cd603c5ad6',
      1 => 
      array (
        0 => 'sales_overridemodel',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getsales',
        2 => 'postsales',
        3 => 'postaggregationsales',
        4 => 'postnetoffconsolorder',
        5 => 'getsalesitem',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/sales/Credit_model.php' => 
    array (
      0 => 'cdd546b4eb4e46bf257f820bff88e1df01761199',
      1 => 
      array (
        0 => 'credit_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchsalescredit',
        2 => 'postsalescredit',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/journal/Journal_model.php' => 
    array (
      0 => '29f02e0cce648fb2331edf153ba2ba391c8d27d6',
      1 => 
      array (
        0 => 'journal_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchjournal',
        2 => 'postjournal',
        3 => 'getjournal',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/journal/Otheramazonfee_model.php' => 
    array (
      0 => '4c27ef75287fa0260ee3d2ef2b035e1ff019d995',
      1 => 
      array (
        0 => 'otheramazonfee_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchamazonfeeother',
        2 => 'postamazonfeeother',
        3 => 'getamazonfeeother',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/Dashboard_model.php' => 
    array (
      0 => 'e766ab4a5e3d6a1edb7d0eb07ed2188d7dc85748',
      1 => 
      array (
        0 => 'dashboard_model',
      ),
      2 => 
      array (
        0 => '__construct',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/cogsjournal/Grnijournal_model.php' => 
    array (
      0 => 'f6f86373dddf4bd505a8c7ad8413c67dd0e39d37',
      1 => 
      array (
        0 => 'grnijournal_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchgrnijournal',
        2 => 'postgrnijournal',
        3 => 'getgrnijournal',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/cogsjournal/Cogsjournal_model.php' => 
    array (
      0 => '81cbbed6fbd35dd37344cb8ca4358f3b1a1178b2',
      1 => 
      array (
        0 => 'cogsjournal_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchcogsjournal',
        2 => 'postcogsjournal',
        3 => 'postconsolcogsjournal',
        4 => 'getcogsjournal',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/cogsjournal/Cogswoinvoice_model.php' => 
    array (
      0 => '5d1026c8f167ca840b9bf086de06364c13da760b',
      1 => 
      array (
        0 => 'cogswoinvoice_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchcogswoinvoice',
        2 => 'postcogswoinvoice',
        3 => 'postconsolidatedcogswoinvoice',
        4 => 'getcogswoinvoicejournal',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/report/SalesReport_model.php' => 
    array (
      0 => 'e6383b190bd0eeb1c83681607d8831944b57e874',
      1 => 
      array (
        0 => 'salesreport_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchsalesreport',
        2 => 'getsalesreport',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/users/Profile_model.php' => 
    array (
      0 => '761ffc115202d9c3761ece3cd5e107cce33ea9c6',
      1 => 
      array (
        0 => 'profile_model',
      ),
      2 => 
      array (
        0 => 'getuserdetails',
        1 => 'getglobalconfig',
        2 => 'savedatesdatas',
        3 => 'savebasic',
        4 => 'updatepassword',
        5 => 'update',
        6 => 'uploadedprofiepic',
        7 => 'executemultiplesql',
        8 => 'saveglobalconfig',
        9 => 'updatemetadata',
        10 => 'updatedataids',
        11 => 'getautomationconfig',
        12 => 'saveautomationsetup',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/users/Login_model.php' => 
    array (
      0 => '3640cba26ab3366906b54875ec8489704b4a6b84',
      1 => 
      array (
        0 => 'login_model',
      ),
      2 => 
      array (
        0 => 'login',
        1 => 'update',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/aggregation/Aggregationtax_model.php' => 
    array (
      0 => '42c97639a8fbf7de161e1e12f41efbf299062821',
      1 => 
      array (
        0 => 'aggregationtax_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/aggregation/Salesreport_model.php' => 
    array (
      0 => 'f34633e324f8410b2c42c5b5dd7e021d9fd875f3',
      1 => 
      array (
        0 => 'salesreport_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getsales',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/aggregation/Config_model.php' => 
    array (
      0 => '517bf3d56fe5a546aa0a5061449e122c02cdd287',
      1 => 
      array (
        0 => 'config_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'get',
        2 => 'delete',
        3 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/aggregation/Aggregationmapping_model.php' => 
    array (
      0 => '7ac620e0df9994694bbb3e959944b571aad7bd46',
      1 => 
      array (
        0 => 'aggregationmapping_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/aggregation/Salescreditreport_model.php' => 
    array (
      0 => '5b8fe2f316e08f1846952bd58e74b1a8b6be29f4',
      1 => 
      array (
        0 => 'salescreditreport_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'getcredit',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/advance/Aradvance_model.php' => 
    array (
      0 => '46f52877f1f82b2e4b2965bc4e1ff23a86ba5f9d',
      1 => 
      array (
        0 => 'aradvance_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetcharadvance',
        2 => 'postaradvance',
        3 => 'applyaradvance',
        4 => 'getaradvance',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/qbd/Config_model.php' => 
    array (
      0 => 'ac391fd35464b33dede6cd827a34f2fe32b0f0e1',
      1 => 
      array (
        0 => 'config_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/qbd/Account_model.php' => 
    array (
      0 => 'c78b7f8f6c6ef3edf95b3bacc11a4baf4d24c005',
      1 => 
      array (
        0 => 'account_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/qbo/Config_model.php' => 
    array (
      0 => '29970f085573d4c840e7f27f2af95cafc88dc420',
      1 => 
      array (
        0 => 'config_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/qbo/Account_model.php' => 
    array (
      0 => 'e345aae140d95c5600c95d56c7c7f82a501a1f48',
      1 => 
      array (
        0 => 'account_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/xero/Config_model.php' => 
    array (
      0 => 'ea958faebcf8999023a4b7aaa472135d2c2c54bb',
      1 => 
      array (
        0 => 'config_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'get',
        2 => 'delete',
        3 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/xero/Account_model.php' => 
    array (
      0 => '29b2e37520d8b19a3ed3d6099ea3ebb977f52679',
      1 => 
      array (
        0 => 'account_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'removetoken',
        3 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/brightpearl/Config_model.php' => 
    array (
      0 => '296932360b21a3f8ce3a0c1827f767160ddb4868',
      1 => 
      array (
        0 => 'config_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'get',
        2 => 'delete',
        3 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/account/brightpearl/Account_model.php' => 
    array (
      0 => '0fa6ee6919c81b6919a32d635734c2ec7b78a6c9',
      1 => 
      array (
        0 => 'account_model',
      ),
      2 => 
      array (
        0 => 'get',
        1 => 'delete',
        2 => 'save',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/customers/Customers_model.php' => 
    array (
      0 => '16f2552b2b0150b340d0840b89bcc4c91c93132c',
      1 => 
      array (
        0 => 'customers_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchcustomers',
        2 => 'postcustomers',
        3 => 'getcustomers',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/logs/Globallog_model.php' => 
    array (
      0 => '1cf2748738e9796a7e5e0edb725974b4c5a8d849',
      1 => 
      array (
        0 => 'globallog_model',
      ),
      2 => 
      array (
        0 => 'insertlog',
        1 => 'record_count',
        2 => 'updatelog',
        3 => 'autoupdatelog',
        4 => 'getlogs',
        5 => 'getbplogdetails',
        6 => 'getlogdetails',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/stock/Adjustment_model.php' => 
    array (
      0 => '542d24dd7be0c15cfd206a0267cc3ad8f21d6edb',
      1 => 
      array (
        0 => 'adjustment_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchstockadjustment',
        2 => 'poststockadjustment',
        3 => 'getadjustment',
      ),
      3 => 
      array (
      ),
    ),
    '/var/www/vhosts/bsitc-repo.com/xerodemo.bsitc-repo.com/xerouk/application/models/products/Products_model.php' => 
    array (
      0 => '832cf9730c075810b142ecf1c1466046094f4ba1',
      1 => 
      array (
        0 => 'products_model',
      ),
      2 => 
      array (
        0 => '__construct',
        1 => 'fetchproducts',
        2 => 'postproducts',
        3 => 'postloadedproduct',
        4 => 'getproduct',
        5 => 'getloadproduct',
      ),
      3 => 
      array (
      ),
    ),
  ),
));