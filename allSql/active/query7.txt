{
	"addTable": [
		{
			"tableName": "mapping_contactlinking",
			"query": "CREATE TABLE IF NOT EXISTS `mapping_contactlinking` (`id` int(11) NOT NULL AUTO_INCREMENT,`account1FieldValue` text,`account2ContactId` varchar(255) DEFAULT NULL,`account1Id` int(11) NOT NULL,`account2Id` int(11) NOT NULL,`created` datetime NOT NULL,`updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
		}
	],
	"addColumn": [
		{
			"tableName": "global_config",
			"columnName": "enableContactlinkingMapping",
			"columnType": "INT",
			"isNull": "",
			"DEFAULT": "0"
		}
	],
	"changeColumn": []
}