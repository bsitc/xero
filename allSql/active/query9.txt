{
	"addTable": [],
	"addColumn": [
		{
			"tableName": "global_config",
			"columnName": "dateLockSettings",
			"columnType": "TEXT",
			"isNull": "NULL",
			"DEFAULT": ""
		},
		{
			"tableName": "configlogs",
			"columnName": "userId",
			"columnType": "VARCHAR (255)",
			"isNull": "NULL",
			"DEFAULT": ""
		}
	],
	"changeColumn": []
}