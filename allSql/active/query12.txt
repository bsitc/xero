{
	"addTable": [],
	"addColumn": [
		{
			"tableName": "account_xero_config",
			"columnName": "customFieldForSalesFetch",
			"columnType": "VARCHAR (255)",
			"isNull": "NULL",
			"DEFAULT": ""
		},
		{
			"tableName": "account_xero_config",
			"columnName": "customFieldForSalesFetchValue",
			"columnType": "TEXT",
			"isNull": "NULL",
			"DEFAULT": ""
		}
	],
	"changeColumn": []
}