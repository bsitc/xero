CREATE TABLE IF NOT EXISTS `mapping_warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1WarehouseId` varchar(255) NOT NULL,
  `account2WarehouseId` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  `AddressName` varchar(255) DEFAULT NULL,
  `tradingIdPartnerId` varchar(255) DEFAULT NULL,
  `Address1` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `PostalCode` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;