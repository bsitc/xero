<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Pricelist
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Pricelist Mapping </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Mapping </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="15%">Currency Code</th>
                                    <th width="20%"><?php echo $this->globalConfig['account1Name'];?> Pricelist Method</th>
                                    <th width="20%"><?php echo $this->globalConfig['account2Name'];?> Pricelist Method</th>
                                    <th width="15%"><?php echo $this->globalConfig['account1Name'];?> Save Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Save Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="currencyCode"></span></td>
                                    <td><span class="value" data-value="account1PricelistId"></span></td>
                                    <td><span class="value" data-value="account2PricelistId"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/pricelist/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="currencyCode"><?php echo @($data['currencyCode'][$row['currencyCode']])?($data['currencyCode'][$row['currencyCode']]['name']):($row['currencyCode']);?></span></td>
                                    <td><span class="value" data-value="account1PricelistId"><?php echo @($data['account1PricelistId'][$row['account1PricelistId']])?($data['account1PricelistId'][$row['account1PricelistId']]['name']):($row['account1PricelistId']);?></span></td>
                                    <td><span class="value" data-value="account2PricelistId"><?php echo @($data['account2PricelistId'][$row['account2PricelistId']])?($data['account2PricelistId'][$row['account2PricelistId']]['name']):($row['account2PricelistId']);?></span></td>
                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(<?php echo json_encode($row);?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/pricelist/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Pricelist Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/pricelist/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
                                        <?php
                                        foreach ($data['account1Id'] as $account1Id) {
                                            echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account2Id]" data-required="1" class="form-control account2Id acc2list">
                                        <?php
                                        foreach ($data['account2Id'] as $account2Id) {
                                            echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Currency Code
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['currencyCode']){ ?> 
                                    <select name="data[currencyCode]" class="form-control currencyCode">
                                        <option value="">Select Currency Code</option>
                                        <?php
                                        foreach ($data['currencyCode'] as $currencyCode) {
                                            echo '<option value="'.$currencyCode['id'].'">'.ucwords($currencyCode['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[currencyCode]" data-required="1" class="form-control currencyCode">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Pricelist Method
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account1PricelistId']){ ?> 
                                    <select name="data[account1PricelistId]" data-required="1" class="form-control account1PricelistId acc1listoption">
                                        <option value="">Select a <?php echo $this->globalConfig['account1Name'];?> Pricelist Method</option>
                                        <?php
                                        foreach ($data['account1PricelistId'] as $accountId => $account1PricelistIds) {
                                            foreach ($account1PricelistIds as $account1PricelistId) {
                                                echo '<option class="acc1listoption'.$accountId.'" value="'.$account1PricelistId['id'].'">'.ucwords($account1PricelistId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account1PricelistId]" data-required="1" class="form-control account1PricelistId">
                                    <?php } ?>
                                </div>
                            </div>   

                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Pricelist Method
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account2PricelistId']){ ?> 
                                    <select name="data[account2PricelistId]" data-required="1" class="form-control account2PricelistId acc2listoption">
                                        <option value="">Select a <?php echo $this->globalConfig['account2Name'];?> Pricelist Method</option>
                                        <?php
                                        foreach ($data['account2PricelistId'] as $accountId => $account2PricelistIds) {
                                            foreach ($account2PricelistIds as $account2PricelistId) {
                                                echo '<option class="acc2listoption'.$accountId.'" value="'.$account2PricelistId['id'].'">'.ucwords($account2PricelistId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account2PricelistId]" data-required="1" class="form-control account2PricelistId">
                                    <?php } ?>
                                </div>
                            </div>     
                              
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>