<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Mappings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> Warehouse
            <small>Mapping</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Warehouse Mapping </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Mapping </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="">          
                        <table class="table table-hover text-centered actiontable" id="sample_4">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="15%"><?php echo $this->globalConfig['account1Name'];?> Warehouse Name</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Location Name</th>
                                    <th width="10%"><?php echo $this->globalConfig['account2Name'];?> Trading Partner Id</th>
                                    <th width="15%"><?php echo $this->globalConfig['account2Name'];?> Trading Partner Name</th>
                                    <th width="10%"><?php echo $this->globalConfig['account1Name'];?> Save Id</th>
                                    <th width="10%"><?php echo $this->globalConfig['account2Name'];?> Save Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="account1WarehouseId"></span></td>
                                    <td><span class="value" data-value="account2WarehouseId"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"></span></td>
                                    <td><span class="value" data-value="account1Id"></span></td>
                                    <td><span class="value" data-value="account2Id"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('mapping/warehouse/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="account1WarehouseId"><?php echo @($data['account1WarehouseId'][$row['account1Id']][$row['account1WarehouseId']])?($data['account1WarehouseId'][$row['account1Id']][$row['account1WarehouseId']]['name']):($row['account1WarehouseId']);?></span></td>
                                    <td><span class="value" data-value="account2WarehouseId"><?php echo @($data['account2WarehouseId'][$row['account2WarehouseId']])?($data['account2WarehouseId'][$row['account2WarehouseId']]['name']):($row['account2WarehouseId']);?></span></td>
                                    <td><span class="value" data-value="tradingIdPartnerId"><?php echo @($data['tradingIdPartnerId'][$row['tradingIdPartnerId']])?($data['tradingIdPartnerId'][$row['tradingIdPartnerId']]['name']):($row['tradingIdPartnerId']);?></span></td>
									<td><span class="value" data-value="tradingIdPartnerId"><?php echo @($this->tradingPartnerDatas[strtolower($row['tradingIdPartnerId'])])?($this->tradingPartnerDatas[strtolower($row['tradingIdPartnerId'])]['tradingIdPartnerName']):($row['tradingIdPartnerId']);?></span></td>

                                    <td><span class="value" data-value="account1Id"><?php echo @($data['account1Id'][$row['account1Id']])?($data['account1Id'][$row['account1Id']]['name']):($row['account1Id']);?></span></td>
                                    <td><span class="value" data-value="account2Id"><?php echo @($data['account2Id'][$row['account2Id']])?($data['account2Id'][$row['account2Id']]['name']):($row['account2Id']);?></span></td>
                                    <td class="action">
										<script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('mapping/warehouse/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Warehouse Mapping</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('mapping/warehouse/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account1Id]" data-required="1" class="form-control account1Id acc1list">
                                        <?php
                                        foreach ($data['account1Id'] as $account1Id) {
                                            echo '<option value = "'.$account1Id['id'].'">'.ucwords($account1Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Save Id
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <select name="data[account2Id]"  data-required="1" class="form-control account2Id acc2list">
                                        <?php
                                        foreach ($data['account2Id'] as $account2Id) {
                                            echo '<option value = "'.$account2Id['id'].'">'.ucwords($account2Id['name']).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Warehouse Name
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account1WarehouseId']){ ?> 
                                    <select name="data[account1WarehouseId]" data-required="1" class="form-control account1WarehouseId acc1listoption">
                                        <option value="" class="show">Select a <?php echo $this->globalConfig['account1Name'];?> Location Name</option>
                                        <?php
                                        foreach ($data['account1WarehouseId'] as $accountId => $account1WarehouseIds) {
                                            foreach ($account1WarehouseIds as $account1WarehouseId) {
                                                echo '<option class="acc1listoption'.$accountId.'" value="'.$account1WarehouseId['id'].'">'.ucwords($account1WarehouseId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account1WarehouseId]" data-required="1" class="form-control account1WarehouseId">
                                    <?php } ?>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Warehouse Method
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-7">
                                    <?php if(@$data['account2WarehouseId']){ ?> 
                                    <select name="data[account2WarehouseId]" data-required="1" class="form-control account2WarehouseId acc2listoption">
                                        <option value="" class="show">Select a <?php echo $this->globalConfig['account2Name'];?> Warehouse Method</option>
                                        <?php
                                        foreach ($data['account2WarehouseId'] as $accountId => $account2WarehouseIds) {
                                            foreach ($account2WarehouseIds as $account2WarehouseId) {
                                                echo '<option class="acc2listoption'.$accountId.'" value="'.$account2WarehouseId['id'].'">'.ucwords($account2WarehouseId['name']).'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <?php } else { ?>
                                        <input type="text" name="data[account2WarehouseId]"  data-required="1" class="form-control account2WarehouseId">
                                    <?php } ?>
                                </div>
                            </div> 
							<div class="form-group">
								<label class="control-label col-md-4"> AddressName
									<span class="required"  > * </span>
								</label>
								<div class="col-md-7">
									<input type="text" name="data[AddressName]"  class="form-control AddressName">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-4"> <?php echo $this->globalConfig['account2Name'];?> Trading Partner Id
									<span class="required"  > * </span>
								</label>
								<div class="col-md-7">
									<input type="text" name="data[tradingIdPartnerId]"  class="form-control tradingIdPartnerId">
								</div>
							</div>								
							<div class="form-group">
								<label class="control-label col-md-4"> Address1
									<span class="required"  > * </span>
								</label>
								<div class="col-md-7">
									<input type="text" name="data[Address1]"  class="form-control Address1">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-4"> City
									<span class="required"  > * </span>
								</label>
								<div class="col-md-7">
									<input type="text" name="data[City]"  class="form-control City">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-4"> State
									<span class="required"  > * </span>
								</label>
								<div class="col-md-7">
									<input type="text" name="data[State]"  class="form-control State">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-4"> PostalCode
									<span class="required"  > * </span>
								</label>
								<div class="col-md-7">
									<input type="text" name="data[PostalCode]"  class="form-control PostalCode">
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label col-md-4"> Country
									<span class="required"  > * </span>
								</label>
								<div class="col-md-7">
									<input type="text" name="data[Country]"  class="form-control Country">
								</div>
							</div>
								
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>