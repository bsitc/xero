<?php
$defaultBox = $this->db->get('mapping_box')->row_array();
$boxName = '( '.$defaultBox['id'].' ) ' .$defaultBox['boxName'];
$boxId = $defaultBox['id'];
?>
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Packing Details</span>
				</li>
			</ul>			
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title"> Packing
			<small>Details</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row" id="main-content" ng-app="demo" ng-controller="NestedListsDemoController">
			<div class="col-md-12">
				<!-- Begin: life time stats -->
				<div class="portlet light portlet-fit portlet-datatable bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-settings font-dark"></i>
							<span class="caption-subject font-dark sbold uppercase"> Order #<?php echo $orderId;?>
								<span class="hidden-xs">| <?php echo ($orderData['created'])?(date('M d, Y H:i:s',strtotime($orderData['created']))):('');?></span>
							</span>
						</div>
						<?php if($data['picked']) { ?>
						<div class="actions">
							<a href="<?php echo base_url('packed/packed/generatePackedFile/'.$orderId);?>" class="btn btn-circle btn-info">
								<i class="fa fa-download"></i>
								<span class="hidden-xs"> Generate Packing File </span>
							</a>
							<a href="javascript:;" class="btn btn-circle btn-danger printactionbtn">
								<i class="fa fa-print"></i>
								<span class="hidden-xs"> Print Label of Selected Box </span>
							</a>
							
						</div>	
						<?php } ?>
					</div>
					<div class="portlet-body">
						<?php
						if(@$this->session->flashdata('success')){ ?>
							<div class="alert alert-success">
							  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>.
							</div>
						<?php } ?>
						<div class="tabbable-line">		
							<form action="<?php echo base_url('packed/packed/printLabel/'.$orderId);?>" id="printLabelForm" method="post">
							<?php 
							if($data['picked']) { ?>
							<?php foreach($data['picked'] as $shipmentIdentification => $packedDetails):
							$isPackedFileGenerated = 0;
							foreach($packedDetails as $goodsId => $pickedItems){
								foreach($pickedItems as $pickedItem){
									if($pickedItem['filename']){
										$isPackedFileGenerated = '1';
										break;
									}
								}
							}
							$isBoxPrinted  = ($shipmentIdentification == 'shipmentIdentification')?('0'):'1';
							$shipmentIdentification = ($shipmentIdentification == 'shipmentIdentification')?($config['ShipmentIdentification']):($shipmentIdentification);
							$CarrierPackageID = $config['companyExtension'] . $config['companyPrefix'] . $config['serialNumber'];
							?> 
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="portlet grey-cascade box">
										<div class="portlet-title">
											<div class="caption">
												<?php if($isBoxPrinted) { ?>
												<input type="checkbox"  name="shipmentIdentification[]" value="<?php echo $shipmentIdentification;?>" checked="true" />
												<?php } ?>
												<div style="width: 90%;display: initial;padding-left: 22px;"><i class="fa fa-cogs"></i> Shipment Identification # : <?php echo $shipmentIdentification;?> </div></div>
											<div class="actions">
												<?php if($isBoxPrinted) { ?>
													<?php if($isPackedFileGenerated) { ?>
													<a href="<?php echo base_url($data['shipmentIdentification'][$shipmentIdentification]);?>" class="btn btn-circle btn-success" download="<?php echo basename($data['shipmentIdentification'][$shipmentIdentification]);?>"><i class="fa fa-download"></i> Download Packing File</a>
													<?php } ?>
													<a href="<?php echo base_url('packed/packed/printLabel/'.$orderId.'/'.$shipmentIdentification);?>" class="btn btn-circle btn-primary" target="_blank"><i class="fa fa-print"></i> Print Labels</a>
												<?php }  ?>
											</div>
										</div>
								
										<div class="portlet-body">
											
										<?php 
										foreach($packedDetails as $goodsId => $pickedItems): ?> 
											<div class="portlet light bordered" id="blockui_sample_1_portlet_body">
												<div class="portlet-title">
													<div class="caption">
														<span class="caption-subject font-green-sharp sbold">Goods-out # <?php echo $goodsId;?></span>
													</div>
													<div class="actions">
														<a href="<?php echo base_url('packed/packed/generateCustomSlip/'.$pickedItems['0']['goodsId']);?>" class="btn btn-circle btn-success" target="_blank"><i class="fa fa-print"></i> Download packing slip</a>

														<?php if($isBoxPrinted) { ?>
														<a href="<?php echo base_url('packed/packed/printLabelByGoodsId/'.$pickedItems['0']['goodsId']);?>" class="btn btn-circle btn-info" target="_blank"><i class="fa fa-print"></i> Print Label</a>
														<?php } ?>
													</div>
												</div>
												
												<div class="portlet-body">
													<div class="table-responsive">
														<table class="table table-hover table-bordered table-striped">
															<thead>
																<tr>
																	<th >S.No</th>
																	<th >BP Order #</th>
																	<th >Shipment Identification</th>
																	<th >SSCC Code</th>
																	<th >Product Id</th> 
																	<th >Product SKU</th> 
																	<th >Total Qty</th> 
																	<th >File Uploaded</th> 
																</tr>
															</thead>
															<tbody>
															<?php
															foreach($pickedItems as $key => $value): ?>
															<tr>
																<td ><?php echo ++$key; ?></td> 
																<td ><?php echo $orderId;?></td>
																<td ><?php echo $shipmentIdentification;?></td>
																<td ><?php echo ($value['CarrierPackageID'])?($value['CarrierPackageID']):($CarrierPackageID); ?></td>					
																<td ><?php echo $value['productId']; ?></td>					
																<td ><?php echo @$value['sku']; ?></td>					
																<td><?php echo (int)$value['qtyMessage'];?></td>			
																<td><?php echo ($value['status'])?'Yes':'No';?></td>			
															</tr>
															<?php endforeach; ?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<?php endforeach; ?>
											
										</div>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
							<?php } else { echo "No item packed.";}?>
							</form>
						</div>
					</div>
				</div>
				<!-- End: life time stats -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<script>
jQuery(".printactionbtn").on("click",function(e){
	e.preventDefault();
	jQuery("#printLabelForm").submit();
});
</script>
<style>
.table-responsive {
    padding-bottom: 0px;
}
.checker {
    float: left;
}
</style>

