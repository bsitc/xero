<?php
$librariesNames = array('brightpearl' => 'Brightpearl','shopify' => 'Shopify', 'fastmag' => 'FASTMAG','netsuite' => 'Netsuite','ilg' => 'ILG','bluepark' => 'Bluepark');
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->>
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="<?php echo $user_session_data['profileimage'];?>" class="img-responsive" alt=""> </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> <?php echo $data['user']['firstname'] ." " . $data['user']['lastname']  ;?> </div>
                    </div>                  
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Dashboard</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                    </li>                                    
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->
                                    <div class="tab-pane active" id="tab_1_1">
                                        <form role="form" action="<?php echo base_url('users/profile/saveBasic');?>">
                                            <div class="alert alert-success hide">      
                                                 <strong>Info!</strong> Data saved successfully.                                     
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Username</label>
                                                <input type="text" name="firstname" value="<?php echo $data['user']['username'] ;?>" placeholder="First Name" class="form-control" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="email" placeholder="Email" name="email" value="<?php echo $data['user']['email'];?>"  class="form-control" /> </div>                                         
                                        </form>
                                    </div>
                                   

                                    <!-- END CHANGE PASSWORD TAB -->                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<link href="<?php echo $this->config->item('script_url');?>assets/pages/css/profile.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $this->config->item('script_url');?>assets/pages/scripts/profile.js" type="text/javascript"></script>