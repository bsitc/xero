<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">               
	<div class="page-sidebar navbar-collapse collapse">                   
		<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li class="sidebar-toggler-wrapper hide">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler"> </div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
		  	<li class="nav-item dashboard">
				<a href="<?php echo base_url();?>dashboard" class="nav-link ">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item account">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-cogs"></i>
					<span class="title">Account Settings</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">		
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-sun-o"></i>
							<span class="title"><?php echo $this->globalConfig['account1Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">		
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account1/account');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account1/config');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>

						</ul>
					</li>
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-sun-o"></i>
							<span class="title"><?php echo $this->globalConfig['account2Name'];?> Settings</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">		
							<li class="nav-item  ">
								<a href="<?php echo base_url('account/account2/account');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Accounts</span>
								</a>
							</li>
							<li class="nav-item hide ">
								<a href="<?php echo base_url('account/account2/config');?>" class="nav-link ">
									<i class="glyphicon glyphicon-chevron-right "></i>
									<span class="title">Default Configuration</span>
								</a>
							</li>

						</ul>
					</li>	
					<li class="nav-item hide">
						<a href="<?php echo base_url('account/sscc');?>" class="nav-link ">
							<i class="glyphicon glyphicon-chevron-right "></i>
							<span class="title">SSCC Settings</span>
						</a>						
					</li>	
					
				</ul>
			</li>
			

			<?php if($this->globalConfig['enableMapping']){ ?>
			<li class="nav-item sales">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="icon-handbag"></span>
					<span class="title">Mapping</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu mapping">
					<?php if($this->globalConfig['enableShippingMapping']){ ?> 					
					<li class="nav-item  sales">
						<a href="<?php echo base_url();?>mapping/shipping" class="nav-link ">
							<span aria-hidden="true" class="icon-bag"></span>
							<span class="title">Shipping Mapping</span>
						</a>
					</li>  
					<?php } ?>         
					<?php if($this->globalConfig['enableWarehouseMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/warehouse" class="nav-link ">
							<span aria-hidden="true" class="icon-map"></span>
							<span class="title">Warehouse Mapping</span>
						</a>
					</li>
					<?php } ?> 
					<?php if($this->globalConfig['enablePricelistMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/pricelist" class="nav-link ">
							<span aria-hidden="true" class="icon-calculator"></span>
							<span class="title">Pricelist Mapping</span>
						</a>
					</li>
					<?php } ?> 
					<?php if($this->globalConfig['enablePaymentMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/payment" class="nav-link ">
							<span aria-hidden="true" class="icon-credit-card"></span>
							<span class="title">Payment Mapping</span>
						</a>
					</li>
					<?php } ?>
					<?php if($this->globalConfig['enableChannelMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/channel" class="nav-link ">
							<span aria-hidden="true" class="icon-bar-chart"></span>
							<span class="title">Channel Mapping</span>
						</a>
					</li>
					<?php } ?> 
					<?php if($this->globalConfig['enableCategoryMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/category" class="nav-link ">
							<span aria-hidden="true" class="icon-map"></span>
							<span class="title">Category Department Mapping</span>
						</a>
					</li>
					<?php } ?> 
					<?php if($this->globalConfig['enableSalesrepMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/salesrep" class="nav-link ">
							<span aria-hidden="true" class="icon-map"></span>
							<span class="title">Sales Rep Mapping Mapping</span>
						</a>
					</li>
					<?php } ?> 
					<?php if($this->globalConfig['enableTradingMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/trading" class="nav-link ">
							<span aria-hidden="true" class="icon-users"></span>
							<span class="title">Trading Partner Mapping</span>
						</a>
					</li>
					<?php } ?> 
					<?php if($this->globalConfig['enableAllowanceMapping']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/allowance" class="nav-link ">
							<span aria-hidden="true" class="icon-map"></span>
							<span class="title">Allowance Mapping</span>
						</a>
					</li>
					<?php } ?> 
					
					<li class="nav-item inventoryadvice ">
						<a href="<?php echo base_url();?>mapping/inventoryadvice" class="nav-link ">
							<i class="fa fa-cubes"></i>
							<span class="title">Inventory Advice Mapping</span>
						</a>
					</li>
					<li class="nav-item packedmapping ">
						<a href="<?php echo base_url();?>mapping/packedmapping" class="nav-link ">
							<i class="fa fa-cubes"></i>
							<span class="title">Packing Mapping</span>
						</a>
					</li>
					
					
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>mapping/box" class="nav-link ">
							<span aria-hidden="true" class="icon-map"></span>
							<span class="title">Box Mapping</span>
						</a>
					</li>
					
					
				</ul>
			</li>   
			<?php } ?> 
			
			<li class="nav-item fieldconfig">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-handbag"></i>
					<span class="title">Field Configuration</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>

				<ul class="sub-menu">					
					<li class="nav-item ack">
						<a href="<?php echo base_url();?>fieldconfig/ack" class="nav-link ">
							<i class="icon-graph"></i>
							<span class="title">Acknowledgement</span>
						</a>
					</li> 
					<li class="nav-item packing">
						<a href="<?php echo base_url();?>fieldconfig/packing" class="nav-link ">
							<i class="icon-graph"></i>
							<span class="title">Packing</span>
						</a>
					</li> 
					
					<li class="nav-item invoice">
						<a href="<?php echo base_url();?>fieldconfig/invoice" class="nav-link ">
							<i class="icon-graph"></i>
							<span class="title">Invoice</span>
						</a>
					</li> 
					
				</ul>
			</li>   
			
			<?php if($this->globalConfig['enableProduct']){ ?>
			<li class="nav-item products">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-graph"></i>
					<span class="title">Products</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>

				<ul class="sub-menu">					
					<li class="nav-item products">
						<a href="<?php echo base_url();?>products/products" class="nav-link ">
							<i class="icon-graph"></i>
							<span class="title">Products</span>
						</a>
					</li>         
					<?php if($this->globalConfig['enablePrebook']){ ?>                    
					<li class="nav-item preproducts hide ">
						<a href="<?php echo base_url();?>products/products/preproducts" class="nav-link ">
							<i class="fa fa-graph"></i>
							<span class="title">Pre-Order</span>
						</a>
					</li>
					<?php } ?>  					
				</ul>
			</li>   

			
			<?php } ?>
			<?php if($this->globalConfig['enableCustomer']){ ?>
			<li class="nav-item customers">
				<a href="<?php echo base_url();?>customers/customers" class="nav-link ">
					<i class="icon-user"></i>
					<span class="title">Customers</span>
					<span class="selected"></span>
				</a>
			</li>
			<?php } ?>

			<?php if($this->globalConfig['enableSalesOrder']){ ?>
			<li class="nav-item sales">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-basket"></i>
					<span class="title">Orders</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>

				<ul class="sub-menu">					
					<li class="nav-item  sales">
						<a href="<?php echo base_url();?>sales/sales" class="nav-link ">
							<i class="icon-basket"></i>
							<span class="title">Orders</span>
						</a>
					</li>					
					<?php if($this->globalConfig['enableDispatchConfirmation']){ ?>                    
					<li class="nav-item dispatch ">
						<a href="<?php echo base_url();?>sales/dispatch" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Dispatch Confirmation</span>
						</a>
					</li>
					<?php } ?>  					
				</ul>
			</li>   
			<?php } ?> 
			
			<li class="nav-item packed">
				<a href="<?php echo base_url();?>packed/packed" class="nav-link ">
					<i class="icon-drawer"></i>
					<span class="title">Packing</span>
					<span class="selected"></span>
				</a>
			</li>			
			<li class="nav-item inventoryadvice">
				<a href="<?php echo base_url();?>stock/inventoryadvice" class="nav-link ">
					<i class="fa fa-cubes"></i>
					<span class="title">Inventory Advice</span>
					<span class="selected"></span>
				</a>
			</li>			
			
			<?php if($this->globalConfig['enablePurchaseOrder']){ ?>
			<li class="nav-item purchase">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<span aria-hidden="true" class="icon-handbag"></span>
					<span class="title">Orders</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>

				<ul class="sub-menu">					
					<li class="nav-item  purchase">
						<a href="<?php echo base_url();?>purchase/purchase" class="nav-link ">
							<i class="icon-basket"></i>
							<span class="title">Orders</span>
						</a>
					</li>           
					<?php if($this->globalConfig['enableReceipt']){ ?>                    
					<li class="nav-item  receipt">
						<a href="<?php echo base_url();?>purchase/receipt" class="nav-link ">
							<i class="fa fa-truck"></i>
							<span class="title">Dispatch Confirmation</span>
						</a>
					</li>
					<?php } ?>  					
				</ul>
			</li>   
			<?php } ?>  

			<?php if(($this->globalConfig['enableStockSync']) ||($this->globalConfig['enableStockAdjustment'])){ ?>
			<li class="nav-item stock">				
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-recycle"></i>
					<span class="title">Stock Details</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
				</a>
				<ul class="sub-menu">					
					<?php if($this->globalConfig['enableStockAdjustment']){ ?>                    
					<li class="nav-item  adjustment">
						<a href="<?php echo base_url();?>stock/adjustment" class="nav-link ">
							<i class="fa fa-cubes"></i>
							<span class="title">Stock Adjustment</span>
						</a>
					</li>           
					<?php } ?>  					
					<li class="nav-item  sync">
						<a href="<?php echo base_url();?>stock/sync" class="nav-link ">
							<i class="fa fa-recycle"></i>
							<span class="title">Stock Sync</span>
						</a>
					</li>
					<li class="nav-item  synclog">
						<a href="<?php echo base_url();?>stock/synclog" class="nav-link ">
							<i class="fa fa-trash-o"></i>
							<span class="title">Stock Sync Log</span>
						</a>
					</li>

				</ul>
			</li>   
			<?php } ?>  
		  	<li class="nav-item logs">
				<a href="<?php echo base_url('logs/globallog');?>" class="nav-link ">
					<i class="icon-trash"></i>
					<span class="title">Global Logs</span>
					<span class="selected"></span>
				</a>
			</li>

			


	   </ul>
		<!-- END SIDEBAR MENU -->
		<!-- END SIDEBAR MENU -->
	</div>
	<!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
<?php	
$controllerName = @$this->router->class;
$allDirectory = @json_encode(array_filter(explode("/",$this->router->directory)));
$method = @$this->router->method;
?>
<script>	
	var directory =<?php echo $allDirectory;?>;
	var controllerName = '<?php echo $controllerName;?>';
	var method = '<?php echo $method;?>';
	var activeClass = '';
	for (index in directory) {
		value = directory[index];
		if(jQuery(activeClass + " ."+value).length){
			activeClass += " ." + value;
		}
	}
	var activeClass1 = '';	
	if(controllerName !=""){
		if(jQuery(activeClass + " ."+controllerName).length){
			activeClass += " ." + controllerName;
		}		
	}
	if( (method !="") && (method != 'index')){
		if(jQuery(activeClass + " ."+method).length){
			activeClass += " ." + method;
		}		
	}
	if(activeClass != ""){
		jQuery(activeClass).addClass('active open');
	}
</script>