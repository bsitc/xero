CREATE TABLE IF NOT EXISTS `stock_inventoryadvice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  `inventoryAdviceId` int(10) NOT NULL,
  `tradingIdPartnerId` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `noOfProduct` int(11) NOT NULL,
  `warehouseId` varchar(50) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;