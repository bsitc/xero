CREATE TABLE IF NOT EXISTS `mapping_allowance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  `account1AllowanceId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account1AllowanceName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account2AllowanceId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account2AllowanceName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradingPartnerId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherproductname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;