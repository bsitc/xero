CREATE TABLE IF NOT EXISTS `field_ack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1PackedId` varchar(255) NOT NULL,
  `account2PackedId` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  `tradingIdPartnerId` varchar(255) DEFAULT NULL,
  `IndustryCode` int(11) NOT NULL DEFAULT '0',
  `InternalOrderNumber` int(11) NOT NULL DEFAULT '0',
  `AcknowledgmentTypeCode` int(11) DEFAULT '0',
  `AcknowledgementNumber` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;