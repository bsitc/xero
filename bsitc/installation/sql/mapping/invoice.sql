CREATE TABLE IF NOT EXISTS `field_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1PackedId` varchar(255) NOT NULL,
  `account2PackedId` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `account2Id` int(11) NOT NULL,
  `tradingIdPartnerId` varchar(255) DEFAULT NULL,
  `itemLevelReference` int(11) NOT NULL DEFAULT '0',
  `paymentTerms` int(11) NOT NULL DEFAULT '1',
  `chargesAllowances` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;