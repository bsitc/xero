CREATE TABLE IF NOT EXISTS `account_vend_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `clientId` varchar(255) NOT NULL,
  `clientSecret` varchar(255) NOT NULL,
  `redirectUrl` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  `tokenType` varchar(255) NOT NULL,
  `expires` varchar(255) NOT NULL,
  `expiresIn` int(11) NOT NULL,
  `refreshToken` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_vend_account` (`id`, `account1Id`, `name`, `clientId`, `clientSecret`, `redirectUrl`, `accessToken`, `tokenType`, `expires`, `expiresIn`, `refreshToken`, `domain`, `created`, `status`, `params`) VALUES
(1, 1, 'bsitctestdemo1', 'Rkagsmq35QnrGU0r6DeayFngt979vVEQ', 'E8Z5Pce46nplTGejyEeV6JjHjgkWgEDZ', 'http://bsitc-bridge16.com/a3merchandise/webhooks/refreshToken/deantest1', '5OtjwgBqfI0uRnXNsVr4t_85DrhjXn7NzJZY2kSu', 'Bearer', '1530271565', 86400, '2BFOPTKDJPzXT8ODoyytcvcaj4Dgw4k13JLQanSv', 'https://bsitctestdemo1.vendhq.com/', '2018-05-14 13:32:41', 0, NULL);
CREATE TABLE IF NOT EXISTS `account_vend_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendAccountId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `anonymousVendCustId` varchar(255) NOT NULL,
  `vendDefaultCurrrency` varchar(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `emailId` varchar(255) NOT NULL,
  `customerId` varchar(50) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `taxCode` varchar(255) NOT NULL,
  `priceTier` varchar(255) NOT NULL,
  `currency` varchar(155) NOT NULL,
  `paymentTerm` varchar(255) NOT NULL,
  `recievables` varchar(255) NOT NULL,
  `revenueAccount` varchar(255) NOT NULL,
  `salesRepresentative` varchar(255) NOT NULL,
  `line1` varchar(255) NOT NULL,
  `line2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `postal` varchar(255) NOT NULL,
  `bpProductCustomFieldName` varchar(255) DEFAULT NULL,
  `bpProductCustomFieldValue` varchar(255) DEFAULT NULL,
  `bpCustomerPricelistId` varchar(255) DEFAULT NULL,
  `bpSalePricelistId` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO `account_vend_config` (`id`, `vendAccountId`, `name`, `anonymousVendCustId`, `vendDefaultCurrrency`, `companyName`, `emailId`, `customerId`, `location`, `taxCode`, `priceTier`, `currency`, `paymentTerm`, `recievables`, `revenueAccount`, `salesRepresentative`, `line1`, `line2`, `city`, `state`, `country`, `postal`, `bpProductCustomFieldName`, `bpProductCustomFieldValue`, `bpCustomerPricelistId`, `bpSalePricelistId`, `created`) VALUES
(1, 1, 'bsitctestdemo1', '', '', 'Anonymous BSITC DEMO 1', 'bsitcdemo1@mailinator.com', '397', '', '2', '', '', '', '', '', '', 'Address 1', '', 'City', 'State', 'GB', 'W129LF', 'PCF_VENDACC1,PCF_VENDACC2', 'bsitcdemo1', '3', '3', '2018-06-18 16:06:04'),
(2, 2, 'bsitctestdemo2', '', '', 'Anonymous BSITC DEMO 2', 'bsitcdemo2@mailinator.com', '398', '', '2', '', '', '', '', '', '', 'address 1', '', 'city', 'state', 'GB', 'W129LF', 'PCF_VENDACC1,PCF_VENDACC2', 'bsitcdemo2', '3', '3', '2018-06-18 16:08:31');