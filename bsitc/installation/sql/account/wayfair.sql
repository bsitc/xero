CREATE TABLE IF NOT EXISTS `account_wayfair_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `account1Id` int(11) NOT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passive` varchar(255) DEFAULT NULL,
  `debug` varchar(255) DEFAULT NULL,
  `sales` varchar(255) DEFAULT NULL,
  `acknowledge` varchar(255) DEFAULT NULL,
  `dispatch` varchar(255) DEFAULT NULL,
  `purchase` varchar(255) DEFAULT NULL,
  `poacknowledge` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `invoice` varchar(255) DEFAULT NULL,
  `stock` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO `account_wayfair_account` (`id`, `name`, `account1Id`, `hostname`, `username`, `password`, `passive`, `debug`, `sales`, `acknowledge`, `dispatch`, `purchase`, `poacknowledge`, `receipt`, `invoice`, `stock`, `status`, `params`) VALUES
(1, 'test', 1, 'test', 'test', 'test', 'true', 'true', 'sales', NULL, NULL, '', '', '', '', 'stock', 0, '');
CREATE TABLE IF NOT EXISTS `account_wayfair_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `wayfairAccountId` int(11) NOT NULL,
  `damageCharge` varchar(100) DEFAULT NULL,
  `BPCustomNotWayfair` varchar(255) DEFAULT NULL,
  `SupplierID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_wayfair_config` (`id`, `name`, `wayfairAccountId`, `damageCharge`, `BPCustomNotWayfair`, `SupplierID`) VALUES
(1, '77.68.95.130', 1, '5', 'PCF_NOTWAYFA', '37802');