CREATE TABLE IF NOT EXISTS `account_rakuten_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `userId` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `params` text,
  `mode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_rakuten_account` (`id`, `account1Id`, `name`, `userId`, `password`, `created`, `status`, `params`, `mode`) VALUES
(1, 1, '129', '129', 'Webgistix', '2018-09-12 05:55:38', 0, NULL, NULL);
CREATE TABLE IF NOT EXISTS `account_rakuten_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rakutenAccountId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `WarehouseID` varchar(255) DEFAULT NULL,
  `Carrier` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `account_rakuten_config` (`id`, `rakutenAccountId`, `name`, `WarehouseID`, `Carrier`, `created`) VALUES
(1, 1, '129', '8', 'DHL', '2018-09-12 20:18:11');