CREATE TABLE IF NOT EXISTS `account_nuorder_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account1Id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `appKey` varchar(255) NOT NULL,
  `appSecret` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `tokenSecret` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO `account_nuorder_account` (`id`, `account1Id`, `name`, `appKey`, `appSecret`, `token`, `tokenSecret`, `url`, `status`, `params`) VALUES
(1, 1, 'MFPeople', 'kwGmD8h3QQAK7Mxhf85kzmrz', 'npGKxfBgK6dkHfqx5UYc86VCMr2tUUmwvPAvU3x8BKukvaR6bBV2yjb7b7eXEwss', 'HcvpVXFQuN22FPygQpu88aZq', 'VyyySejRtRrA5ZvMNTPH3NX42MRzjrhV6JyZHGWpe5DrX2pXCsm88e9MjMNv4Sxb', 'https://wholesale.sandbox1.nuorder.com/api/', 1, '');
CREATE TABLE IF NOT EXISTS `account_nuorder_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `nuorderAccountId` int(11) NOT NULL,
  `productSize` varchar(255) NOT NULL,
  `baseCurrency` varchar(255) NOT NULL,
  `productColor` varchar(255) NOT NULL,
  `productSeason` varchar(255) NOT NULL,
  `productDepartment` varchar(255) NOT NULL,
  `productCategory` varchar(255) NOT NULL,
  `defaultCompany` varchar(255) NOT NULL,
  `defaultCompanyCode` varchar(255) NOT NULL,
  `salesRepresentative` varchar(255) NOT NULL,
  `fetchOrderStatus` varchar(255) NOT NULL,
  `orderCompleteStatus` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO `account_nuorder_config` (`id`, `name`, `nuorderAccountId`, `productSize`, `baseCurrency`, `productColor`, `productSeason`, `productDepartment`, `productCategory`, `defaultCompany`, `defaultCompanyCode`, `salesRepresentative`, `fetchOrderStatus`, `orderCompleteStatus`) VALUES
(1, 'MFPeople', 1, 'One Size', 'USD', 'No Color', 'No Season', 'No Department', 'No Category', 'No Company', 'NOCOMPANY', 'kristina.land+reptest@nuorder.com', 'approved', 'shipped');