<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span>
                </li>

            </ul>
        </div>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['account2Name']);?> 3PL Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%"><?php echo ucwords($this->globalConfig['account2Name']);?> Id</th>
                                    <th width="25%">Store Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="radialAccountId"></span></td>
                                    <td><span class="value" data-value="name"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="radialAccountId"><?php echo $row['radialAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><?php echo ucwords($this->globalConfig['account2Name']);?> Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form id="saveActionForm" action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>                 
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  ?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
            <div class="form-group">
                <label class="control-label col-md-5"><?php echo ucwords($this->globalConfig['account2Name']);?> Id
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[radialAccountId]" data-required="1" class="form-control radialAccountId">
                        <option value="">Select a save <?php echo strtolower($this->globalConfig['account2Name']);?> account</option>
                        <?php
                        foreach ($data['saveAccount'] as $saveAccount) {
                            echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Account Code
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[accountCode]"data-required="1" class="form-control accountCode" type="text"> </div>
            </div> 
			<div class="form-group">
                <label class="control-label col-md-5">BP Custom Field Not for radial
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[BPCustomNotradial]"data-required="1" class="form-control BPCustomNotradial" type="text"> </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-5">Default Product Type
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[defaultProductType]"data-required="1" class="form-control defaultProductType" type="text"> </div>
            </div>
 
			<div class="form-group">
                <label class="control-label col-md-5">BP custom field for Country of origin
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[customCountryOfOrigin]"data-required="1" class="form-control customCountryOfOrigin" type="text">
					<span class=""><b>Note:</b> Value get from Brightpearl Custom Field.<span>
				</div>
					
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Default Country of origin
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[defaultCountryOfOrigin]"data-required="1" class="form-control defaultCountryOfOrigin" type="text">
					<span class=""><b>Note:</b> It's used if Brightpearl Country of Origin value is blank.<span>
				</div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">BP custom field for fabric
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[customFabric]"data-required="1" class="form-control customFabric" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">BP custom field for customs number
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[customCustomsNumber]"data-required="1" class="form-control customCustomsNumber" type="text"> </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-5">Default Order Type
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[orderType]"data-required="1" class="form-control orderType" type="text"> </div>
            </div> 

            <div class="form-group">
                <label class="control-label col-md-5">Default Language
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[defaultLanguage]"data-required="1" class="form-control defaultLanguage" type="text"></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Catalog Id
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[catalogId]"data-required="1" class="form-control catalogId" type="text"></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Do Not Ship
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[doNotShip]"data-required="1" class="form-control doNotShip" type="text"></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Distribution Center
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[distributionCentre]"data-required="1" class="form-control distributionCentre" type="text"></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Order Tax Type
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[orderTaxType]"data-required="1" class="form-control orderTaxType" type="text"></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Default Payment Method
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[defaultPaymentMethod]"data-required="1" class="form-control defaultPaymentMethod" type="text"></div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Sales Order Origin
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[orderOrigin]"data-required="1" class="form-control orderOrigin" type="text"></div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Order Type Inventory Transfer (sale)
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[orderTypeInventorySales]"data-required="1" class="form-control orderTypeInventorySales" type="text"> </div>
            </div> 
			<div class="form-group">
                <label class="control-label col-md-5">Order Type Inventory Transfer (purchase)
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[orderTypeInventoryPurchase]"data-required="1" class="form-control orderTypeInventoryPurchase" type="text"> </div>
            </div> 
			<div class="form-group">
                <label class="control-label col-md-5">RMA Number Prefix
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[rmaNumberPrefix]"data-required="1" class="form-control rmaNumberPrefix" type="text"> </div>
            </div>  
			<div class="form-group">
                <label class="control-label col-md-5">Language Identifier(langId)
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[languageIdentifier]"data-required="1" class="form-control languageIdentifier" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Shipping product identify nominal code
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[defaultProductNominalCode]"data-required="1" class="form-control defaultProductNominalCode" type="text"> </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Sales Credit qtyUom
                    <span class="required"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[qtyUom]"data-required="1" class="form-control qtyUom" type="text"> </div>
            </div>
			
        </div> 
    <?php } ?>
</div>