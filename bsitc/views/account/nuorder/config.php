<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span>
                </li>

            </ul>
        </div>
        <h3 class="page-title"> NuOrder
            <small>NuOrder</small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>NuOrder Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">NuOrder Id</th>
                                    <th width="25%">Store Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="nuorderAccountId"></span></td>
                                    <td><span class="value" data-value="name"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="nuorderAccountId"><?php echo $row['nuorderAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog">
            <div class="modal-dialog">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">NuOrder Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form id="saveActionForm" action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>                 
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  ?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
            <div class="form-group">
                <label class="control-label col-md-5">NuOrder Id
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[nuorderAccountId]" data-required="1" class="form-control nuorderAccountId">
                        <option value="">Select a save NuOrder account</option>
                        <?php
                        foreach ($data['saveAccount'] as $saveAccount) {
                            echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Default product color
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[productColor]"data-required="1" class="form-control productColor" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Default product size
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[productSize]"data-required="1" class="form-control productSize" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Default product season
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[productSeason]"data-required="1" class="form-control productSeason" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Default product department
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[productDepartment]"data-required="1" class="form-control productDepartment" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Default product category 
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[productCategory]"data-required="1" class="form-control productCategory" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Default Company
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[defaultCompany]"data-required="1" class="form-control defaultCompany" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Default Company Code
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[defaultCompanyCode]"data-required="1" class="form-control defaultCompanyCode" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Sales Representative
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[salesRepresentative]"data-required="1" class="form-control salesRepresentative" type="text"> </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-md-5">Fetch NuOrder sales order status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[fetchOrderStatus]" data-required="1" class="form-control fetchOrderStatus">
                        <option value="review">Review</option>
                        <option value="pending">Pending</option>
                        <option value="edited">Edited</option>
                        <option value="approved">Approved</option>
                        <option value="processed">Processed</option>
                        <option value="shipped">Shipped</option>
                        <option value="cancelled">Cancelled</option>
                    </select>
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-5">Sent to Brightpearl order status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[orderSentStatus]" data-required="1" class="form-control orderSentStatus">
                        <option value="review">Review</option>
                        <option value="pending">Pending</option>
                        <option value="edited">Edited</option>
                        <option value="approved">Approved</option>
                        <option value="processed">Processed</option>
                        <option value="shipped">Shipped</option>
                        <option value="cancelled">Cancelled</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-5">Shipped sales order status
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <select name="data[orderCompleteStatus]" data-required="1" class="form-control orderCompleteStatus">
                        <option value="review">Review</option>
                        <option value="pending">Pending</option>
                        <option value="edited">Edited</option>
                        <option value="approved">Approved</option>
                        <option value="processed">Processed</option>
                        <option value="shipped">Shipped</option>
                        <option value="cancelled">Cancelled</option>
                    </select>
                </div>  
            </div> 				
            <div class="form-group">
                <label class="control-label col-md-5">Default currency
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-6">
                    <input name="data[baseCurrency]"data-required="1" class="form-control baseCurrency" type="text"> </div>
            </div> 
        </div> 
    <?php } ?>
</div>