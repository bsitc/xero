<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->>
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Account Settings</span> 
                </li>

            </ul>
        </div>
        <h3 class="page-title"> <?php echo $this->globalConfig['account2Name'];?>
            <small><?php echo $this->globalConfig['account2Name'];?></small>
        </h3>
        <div class="portlet ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i> <?php echo $this->globalConfig['account2Name'];?> Configuration </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-info actionaddbtn">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> Add New Configuration </span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-responsive">          
                        <table class="table table-hover text-centered actiontable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th> 
                                    <th width="25%"> <?php echo $this->globalConfig['account2Name'];?> Id</th>
                                    <th width="25%">Account Id</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="clone hide">
                                    <td ><span class="value" data-value="id"></span></td>
                                    <td><span class="value" data-value="xeroAccountId"></span></td>
                                    <td><span class="value" data-value="compte"></span></td>
                                    <td class="action">
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" delurl="<?php echo base_url('account/'.$data['type'].'/config/delete/');?>" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php   foreach ($data['data'] as $key =>  $row) { ?>                               
                                <tr class="tr<?php echo $row['id'];?>">
                                    <td ><span class="value" data-value="id"><?php echo $key + 1;?></span></td>
                                    <td><span class="value" data-value="xeroAccountId"><?php echo $row['xeroAccountId'];?></span></td>
                                    <td><span class="value" data-value="name"><?php echo $row['name'];?></span></td>
                                    <td class="action">
                                        <script> var data<?php echo $row['id'];?> = <?php echo json_encode($row);?>;</script>
                                        <a class="actioneditbtn btn btn-icon-only blue" href="javascript:;" onclick=editAction(data<?php echo $row['id'];?>) title="View"><i class="fa fa-edit" title="Edit settings" ></i></a>
                                        <a href="javascript:;" onclick="deleteAction('<?php echo base_url('account/'.$data['type'].'/config/delete/'.$row['id']);?>',this)" class="actiondelbtn btn btn-icon-only red" title="View"><i class="fa fa-trash danger" title="Delete settings" ></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actionmodal" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">        
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"> <?php echo $this->globalConfig['account2Name'];?> Account Settings</h4>
                </div>
                <div class="modal-body">
                   <form action="<?php echo base_url('account/'.$data['type'].'/config/save');?>" method="post" id="saveActionForm" class="form-horizontal saveActionForm" novalidate="novalidate">
                        <div class="form-body">
                                                                                  
                        </div>
                        <input type="hidden" name="data[id]" class="id" />
                    </form>                         
                </div>
                <div class="modal-footer">
                  <button type="button" class="pull-left btn btn-primary submitAction">Save</button>
                  <button type="button" class="btn yellow btn-outline sbold" data-dismiss="modal">Close</button>
                </div>
              </div>                  
            </div>
        </div>
    </div>
</div>


<div class="confighml">
    <?php   
    $data['data'] = ($data['data'])?($data['data']):(array(''));
    foreach ($data['data'] as $key =>  $row) {  
		$account1Id = $data['saveAccount'][$row['xeroAccountId']]['account1Id'];
		?>
        <div class="htmlaccount<?php echo @$row['id'];?>" style="display: none;">
            <div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
			<div class="form-group">
				<label class="control-label col-md-4"> <?php echo $this->globalConfig['account2Name'];?> Id
					<span class="required" aria-required="true"> * </span>
				</label>
				<div class="col-md-7">
					<select name="data[xeroAccountId]" data-required="1" class="form-control xeroAccountId">
						<option value="">Select a save <?php echo $this->globalConfig['account2Name'];?> account</option>
						<?php
						foreach ($data['saveAccount'] as $saveAccount) {
							echo '<option value="'.$saveAccount['id'].'">'.ucwords($saveAccount['name']).'</option>';
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> COGSAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[COGSAccountRef]" data-required="1" class="form-control COGSAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div> 
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> IncomeAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[IncomeAccountRef]"  class="form-control IncomeAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div> 
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> AssetAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[AssetAccountRef]" data-required="1" class="form-control AssetAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								//if($IncomeAccountRef['Classification'] == 'Revenue')
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> ExpenseAccountRef
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[ExpenseAccountRef]" data-required="1" class="form-control ExpenseAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								//if($IncomeAccountRef['Classification'] == 'Revenue')
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Stock Adjustment AccountRef 
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[stockAdjustmentAccountRef]" data-required="1" class="form-control stockAdjustmentAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								//if($IncomeAccountRef['Classification'] == 'Revenue')
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>	
			<div class="form-group hide">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account1Name'];?> Prepayment AccountRef 
                    <span class="required" aria-required="false"> * </span> 
                </label>
                <div class="col-md-7">
                    <select name="data[prepaymentAccount]" data-required="0" class="form-control prepaymentAccount">
                        <?php
                        foreach ($data['prepaymentAccount'][$account1Id] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								//if($IncomeAccountRef['Classification'] == 'Revenue')
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>	
			
			<div class="form-group hide"> 
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> PayType Account Ref
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[PayTypeAccRef]" class="form-control PayTypeAccRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Discount Account Ref
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[DiscountAccountRef]" data-required="1" class="form-control DiscountAccountRef">
                        <?php
                        foreach ($data['IncomeAccountRef'][$row['xeroAccountId']] as $IncomeAccountRef) {
							/* foreach ($IncomeAccountRefs as $IncomeAccountRef) { */
								echo '<option value="'.$IncomeAccountRef['id'].'">'.ucwords($IncomeAccountRef['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Order TaxCode
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[TaxCode]" data-required="1" class="form-control TaxCode">
                        <?php
                        foreach ($data['getAllTax'][$row['xeroAccountId']] as $getAllTax) {
							/* foreach ($getAllTaxs as $getAllTax) { */
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Order No  TaxCode
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[salesNoTaxCode]" data-required="1" class="form-control salesNoTaxCode">
                        <?php
                        foreach ($data['getAllTax'][$row['xeroAccountId']] as $getAllTax) {
							/* foreach ($getAllTaxs as $getAllTax) { */
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group">
                <label class="control-label col-md-4"><?php echo $this->globalConfig['account2Name'];?> Default Sales Line Item TaxCode
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[orderLineTaxCode]" data-required="1" class="form-control orderLineTaxCode">
                        <?php
                        foreach ($data['getAllTax'][$row['xeroAccountId']] as $getAllTax) {
							/* foreach ($getAllTaxs as $getAllTax) { */
								echo '<option value="'.$getAllTax['id'].'">'.ucwords($getAllTax['name']).'</option>';
							/* } */
                        }
                        ?>
                    </select> 
                </div>
            </div>
			
			<div class="form-group">
				<label class="control-label col-md-4">Generic Sku<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[genericSku]" data-required="1" class="form-control genericSku" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Shipping Item Id<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[shippingItem]" data-required="1" class="form-control shippingItem" type="text"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4">Discount Item Id<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[discountItem]" data-required="1" class="form-control discountItem" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Shipping product identify nominal code<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[nominalCodeForDiscount]" data-required="1" class="form-control nominalCodeForDiscount" type="text"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Default Currency<span class="required" aria-required="true"> * </span></label>
				<div class="col-md-7"><input name="data[defaultCurrency]" data-required="1" class="form-control defaultCurrency" type="text"></div>
			</div>
			<div class="form-group">
                <label class="control-label col-md-4">Product purchase price list
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[productCostPriceList]"  class="form-control productCostPriceList">
                        <?php
                        foreach ($data['pricelist'][$account1Id] as $pricelist) {
                            echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">Product sell price list
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[productRetailPriceList]"  class="form-control productRetailPriceList">
                        <?php
                        foreach ($data['pricelist'][$account1Id] as $pricelist) {
                            echo '<option value="'.$pricelist['id'].'">'.ucwords($pricelist['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">Default channel
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[channelIds][]"  class="form-control channelIds"  multiple="true">
						<option value=""> Select Channel </option>
                        <?php
                        foreach ($data['channel'][$account1Id] as $channel) {
                            echo '<option value="'.$channel['id'].'">'.ucwords($channel['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div>
			<div class="form-group">
                <label class="control-label col-md-4">Default warehouse
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-7">
                    <select name="data[warehouses][]" class="form-control warehouses" multiple="true">
						<option value="">Select warehouse</option>
                        <?php
                        foreach ($data['warehouse'][$account1Id] as $warehouse) {
                            echo '<option value="'.$warehouse['id'].'">'.ucwords($warehouse['name']).'</option>';
                        }
                        ?>
                    </select> 
                </div>
            </div> 

        </div> 
    <?php } ?>
</div>