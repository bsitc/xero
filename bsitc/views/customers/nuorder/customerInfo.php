<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Customer Details</span>
			</li>
		</ul>
	</div>
	<!-- END PAGE BAR -->
	<!-- BEGIN PAGE TITLE-->
	<h3 class="page-title"> Customers
		<small>Pre-info</small>
	</h3>
	<!-- END PAGE TITLE-->
	<!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet ">		
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['fetchCustomer']);?> Customer Data</div
					</div>		
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<?php
						echo "<pre>";
						print_r(json_decode($customerInfo['params'],true));
						echo "</pre>";
						?>
					</div>
				</div>
			</div>
			<div class="portlet ">		
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo ucwords($this->globalConfig['postCustomer']);?> Customer Data</div
					</div>		
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<?php
						echo "<pre>";
						print_r(json_decode($customerInfo['ceatedParams'],true));
						echo "</pre>";
						?>
					</div>
				</div>
			</div>
			
			<!-- End: life time stats -->
		</div>
	</div>
</div>
<script src="<?php echo $this->config->item('script_url');?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>		
