<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Import Products</span>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<form class="form-horizontal saveActionForm" id="submitProductForm" action="<?php echo base_url('products/products/importBulkProducts') ?>" method="post" enctype="multipart/form-data" >
				<div class="portlet ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart"></i>Upload Product Category File 
						</div>
						<div class="actions">
							 
							<input id="submitDownloadSample" type="submit" name="downloadSampleWithAllCsvFile" value="Download Sample with all fields" class="btn btn-circle btn-info">
							
							<input id="downloadSampleCsvFile" type="submit" name="downloadSampleCsvFile" value="Download Required fields Sample" class="btn btn-circle btn-info">
							<input id="downloadSampleSchemaFile" type="submit" name="downloadSampleSchemaFile" value="Download Product Category Schema" class="btn btn-circle btn-info">
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<?php if($this->session->flashdata('uploadmessage')){ ?>
								<div class="alert alert-success">
								   <?php echo $this->session->flashdata('uploadmessage');?>
								</div>
							<?php } ?>
							<?php if($this->session->flashdata('errormessage')){ ?>
								<div class="alert alert-danger">
								   <?php echo $this->session->flashdata('errormessage');?>
								</div>
							<?php } ?>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Category
								<span class="required" aria-required="true"> * </span> 
							</label>
							<div class="col-md-4">
								<select name="productCategory"   class="form-control">
									<option value="">Select Category</option>
									<?php 
										foreach($categoryDatas as $categoryData){?>
											<option value="<?php echo trim($categoryData['subCategortName']); ?>"><?php echo $categoryData['bpCategoryName']; ?></option>
									<?php } ?>
								</select> 
							</div>
						</div>						
						<div class="form-group">
							<label class="control-label col-md-2">Select File
								<span class="required" aria-required="true"> * </span> 
							</label>
							<div class="col-md-4">
								<input type="file" class="form-control file" name="productFile" id="productFile" style="padding-bottom:40px"/>
							</div>
						</div>
						<div class="form-group">	
							<div class="col-md-3">
							</div>
							<div class="col-md-8">
								<button id="submitProductFile" type="submit" class="btn btn-success">Upload product</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
jQuery(document).on('click', '#submitProductFile', function(){
	var productCategory = jQuery('#productCategory').val();
	if(productCategory == ""){
		alert("Please choose category first.");
		return false;
	}
	jQuery("#submitProductForm").submit();
});
</script>
