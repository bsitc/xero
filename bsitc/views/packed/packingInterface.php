<?php
$defaultBox = $this->db->get('mapping_box')->row_array();
$boxName = '( '.$defaultBox['id'].' ) ' .$defaultBox['boxName'];
$boxId = $defaultBox['id'];
?>
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Packing Details</span>
				</li>
			</ul>			
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title"> Packing
			<small>Details</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row" id="main-content" ng-app="demo" ng-controller="NestedListsDemoController">
			<div class="col-md-12">
				<!-- Begin: life time stats -->
				<div class="portlet light portlet-fit portlet-datatable bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-settings font-dark"></i>
							<span class="caption-subject font-dark sbold uppercase"> Order #<?php echo $orderId;?>
								<span class="hidden-xs">| <?php echo ($orderData['created'])?(date('M d, Y H:i:s',strtotime($orderData['created']))):('');?></span>
							</span>
						</div>	
						
					</div>
					<div class="portlet-body">
						<?php
						if(@$this->session->flashdata('success')){ ?>
							<div class="alert alert-success">
							  <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>.
							</div>
						<?php } if(@$this->session->flashdata('error')){ ?>
							<div class="alert alert-danger">
							  <strong>Error!</strong> <?php echo $this->session->flashdata('error');?>.
							</div>
						<?php } ?>
						<div class="tabbable-line">
							
							<div class="row">										
								<div class="col-md-12 col-sm-12">
									<div class="portlet blue-hoki box">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-cogs"></i>Order Item Details </div>
										</div>
										<div class="portlet-body">
											<div class="table-responsive">
												<table class="table table-hover table-bordered table-striped">
													<thead>
														<tr>
															<th >S.No</th>
															<th >BP Order #</th>
															<th >Product Id</th> 
															<th >Product SKU</th> 
															<th >Total Qty</th> 
															<th >Remaining Qty</th> 
															<th >Packed Qty</th> 
															<th >Shipped Qty</th> 
														</tr>
													</thead>
													<tbody>
													<?php
													$count = 1;  $itemCount = 1;  
													$itemDetails = array();
													foreach($data['items'] as $key=>$value): ?>
													<tr>
														<td ><?php echo $count++; ?></td>
														<td ><?php echo $orderId;?></td>
														<td ><?php echo $value['productId']; ?></td>					
														<td ><?php echo @$value['productSku']; ?></td>					
														<td><?php echo (int)$value['quantity'];?></td>
														<td><?php echo (int)$value['quantity'] - ((int)$value['packedQty'] + (int)$value['shippedQty']);?></td>
														<td><?php echo (int)$value['packedQty'];?></td>
														<td><?php echo (int)$value['shippedQty'];?></td>
														<?php
															@$itemDetails[$value['productId']] = 0;
														?>
													</tr>									
													<?php 
													$remainingQty = (int)$value['quantity'] - ((int)$value['packedQty'] + (int)$value['shippedQty']);
													if($remainingQty > 0){
														$items[] = array(
															'effectAllowed' => 'copy',
															'productId' 	=> $value['productId'],
															'id' 			=> $itemCount,
															'productSku' 	=> $value['productSku'],
															'quantity' 		=> $remainingQty,
															'readonly' 		=> 1,
														);
														$itemCount ++;
													}
													endforeach; ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<form method="post" action="<?php echo base_url('packed/packed/createGoodsOutNote/'.$orderId);?>" >
							<?php 
							if($data['picked']) { ?>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="portlet grey-cascade box">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-cogs"></i>Picked Item Details <span class="notes" style="font-size: 15px;color: yellow;padding-left: 25px;" >(All checked goods out notes will be deleted) </span></div>
										</div>
								
										<div class="portlet-body">
											
										<?php foreach($data['picked'] as $goodsId => $pickedItems): ?> 
											<div class="portlet light bordered" id="blockui_sample_1_portlet_body">
												<div class="portlet-title">
													<div class="caption">
														<input type="checkbox"  name="delGoodsId[]" value="<?php echo $goodsId;?>" checked="true" />
														<span class="caption-subject font-green-sharp sbold">Goods-out # <?php echo $goodsId;?></span>
													</div>
												</div>
												<div class="portlet-body">
													<div class="table-responsive">
														<table class="table table-hover table-bordered table-striped">
															<thead>
																<tr>
																	<th >S.No</th>
																	<th >BP Order #</th>
																	<th >Product Id</th> 
																	<th >Product SKU</th> 
																	<th >Total Qty</th> 
																</tr>
															</thead>
															<tbody>
															<?php
															foreach($pickedItems as $key => $value): ?>
															<tr>
																<td ><?php echo $count++; ?></td>
																<td ><?php echo $orderId;?></td>
																<td ><?php echo $value['productId']; ?></td>					
																<td ><?php echo @$value['productSku']; ?></td>					
																<td><?php echo (int)$value['quantity'];?></td>			
															</tr>
															<?php endforeach; ?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<?php endforeach; ?>
											
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							
							
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="portlet green-meadow box">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-cogs"></i>Action Box </div>
										</div>
										<div class="portlet-body">
											<div class="row static-info">
												<div class="form-body col-md-12 col-sm-12">								
													<div class="form-group">
														<label class="col-md-1 control-label">Select Box &nbsp</label>
														<div class="col-md-3">
															<select class="form-control selectboxlist" style="">
																<?php 
																foreach($data['boxDetails'] as $boxDetails){
																	echo '<option value="'.$boxDetails['id'].'">('.$boxDetails['id'].') '.$boxDetails['boxName'].'</option>';
																}
																?>
														   </select>  
														</div>
														<div class="col-md-3">
															<a href="javascript:;" class="btn btn-info clonerowbtn" ng-click="cloneRow()">Add New Box</a>
														</div>
													</div>													
												</div>
												
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="portlet grey-cascade box">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-cogs"></i>Packed Item Details </div>
										</div>
										<div class="portlet-body">
											<div class="table-responsive">
												<div class="row-fluid" style="padding-left: 40px;padding-right: 36px;">
													<div id="astroid-layout-builder" class="ui-sortable">
														<div >
															<div ng-repeat="(zone, containers) in models.dropzones" >	
																<div class="astroid-layout-section"
																dnd-list="containers"
																dnd-allowed-types="['container']"
																dnd-external-sources="true"
																dnd-drop="dropCallback(index, item, external, type)">
																	<div class="container-element box box-blue"
																	ng-repeat="container in containers track by $index"
																	 >						
																		<div class="section-actions clearfix">
																			<div class="float-left">
																				<span class="astroid-move-row move show-on-hover left" href="#"><i class="fa fa-arrows"></i></span>
																				<h4 class="section-title">{{container.box}}</h4> 
																			</div>
																			<div class="float-right">
																				<ul class="astroid-layout-row-option-list">
																					<li class="show-on-hover remove-section right">
																						<a class="astroid-layout-row-remove" href="javascript:void()" ng-click="deleteRow($index,container)" ><i  style="width:36px;"  class="fa fa-trash"></i></a>
																					</li>
																				</ul>
																			</div>
																		</div>
																		<!-- Section Name -->
																		<div class="astroid-layout-row-container ui-sortable">
																			<div class="astroid-layout-column col-md-12 clear">
																				<div class="column-inner"> 
																					
																
																				</div>
																			</div>
																			
																			<div class="col-md-12 ui-sortable columnsortable clear"
																			dnd-list="container.items"
																			dnd-allowed-types="['item']" 
																			dnd-horizontal-list="true"
																			dnd-external-sources="true"
																			dnd-effect-allowed="{{container.effectAllowed}}"
																			dnd-dragover="dragoverCallback(index, external, type)"
																			dnd-drop="dropCallback(index, item, external, type)">								
																				<div class="astroid-layout-column col-md-10 row-fluid"
																						ng-repeat="item in container.items"
																						dnd-draggable="item"
																						dnd-type="'item'"
																						dnd-effect-allowed="{{item.effectAllowed}}"
																						dnd-moved="container.items.splice($index, 1)">
																					<div class="col-md-6">
																					{{item.productSku}}
																					</div>
																					<div class="col-md-3">
																					<input  type="text" itemIndex = {{$index}} parentIndex = {{$parent.$index}} itemId={{item.parentId}} class="itemvalues form-control inputtext itemqty{{$parent.$index}}{{$index}} data-parent{{item.id}}" value="{{item.quantity}}"  ng-change="updateQty(item)" ng-model="item.newquantity" ng-readonly="item.readonly" />
																					</div>
																					<div class="col-md-1">
																					<a  href="javascript:void('0')" ng-click="deleteColomItem($parent.$index,$index)" > <i class="fa fa-trash"></i> </a>
																					</div>
																					
																				</div>
																			</div>
																		</div>
																	</div>					
																</div>
															</div>	
															<div class="hide"> 
															<h2>Generated Model</h2>		
															<pre>{{modelAsJson}}</pre> 
															</div>
															<div class="col-md-12" style="text-align: center;">	
																<textarea name="boxdetails" class="hide">{{modelAsJson}}</textarea>
																<button type="submit" class="btn btn-danger">Submit boxes</button>	
															</div>
														</div>
													</div>
												</div>
							
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
							
							
							
							
						</div>
					</div>
				</div>
				<!-- End: life time stats -->
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
<script src="//code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>
<script src="<?php echo $this->config->item('script_url');?>assets/draganddrop/angular-drag-and-drop-lists.js"></script>
<!-- Stuff that is only required in this demo (no need to copy) -->
<link rel="stylesheet" href="<?php echo $this->config->item('script_url');?>assets/draganddrop/framework/vendor/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $this->config->item('script_url');?>assets/draganddrop/framework/vendor/bootstrap-theme.min.css">
<link href="<?php echo $this->config->item('script_url');?>assets/draganddrop/admin-style.css" rel="stylesheet" />
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-route.min.js"></script>
	
<script>
jQuery("select").removeClass("tbl_length");
orderId = '<?php echo $orderId;?>';
jQuery(".creategoodsremaining").on("click", function (e){
	e.preventDefault();
	var boxId = jQuery(".selectboxlist").val();
	if(boxId == ''){
		alert("Please select Box Type");
	}
	else{
		if(confirm("Are you sure want to pack all remaing item in one Box?")){
			window.location.href  = '<?php echo base_url('sales/orderpack/createGoodsOutNote/');?>/' + orderId + '/1/' + boxId;
		}
	}
});

angular.module("demo", ["ngRoute", "dndLists"])
.controller('NestedListsDemoController', function($scope) {
    $scope.models = {
        selected: null,
        templates: [
            {type: "item", id: 2},
            {type: "container", id: 1, columns: [[], []]}
        ],
        dropzones: {
            /* "0": [
				{
				  "items": [				
					{
					  "productSku": "copy 26",
					  "quantity": "20",
					  "effectAllowed": "copy"
					},
					{
					  "productSku": "copy 26",
					  "quantity": "20",
					  "effectAllowed": "copy"
					},
				  ],
				  "effectAllowed": "copy",
				  "box": "Box 1",
				}               
			], */
			
			
			"0": [
				{
				  "items": <?php echo json_encode($items);?>,
				  "box": '<?php echo $boxName;?>',
				  "boxId": '<?php echo $boxId;?>',
				}               
			],
			
			
			
            
        },
		'itemDetails' : <?php echo json_encode($itemDetails);?>,
		'itemCount' : <?php echo $itemCount;?>,
    };
	
	
	$scope.updateQty = function(item) {		
		console.log(item);
		itemQty =  parseInt(item.quantity); 
		itemNewQty =  parseInt(item.newquantity); 
		parentId = item.parentId;
		parentQty = angular.element(jQuery('.data-parent'+parentId)).val();
		parentIndex = angular.element(jQuery('.data-parent'+parentId)).attr('parentindex');
		index = angular.element(jQuery('.data-parent'+parentId)).attr('itemindex');
		if (isNaN(itemNewQty)) itemNewQty = 0;
		if (isNaN(itemQty)) itemQty = 0;
		if (isNaN(parentQty)) parentQty = 0;		
		parentQty = parseInt(parentQty) + parseInt(itemQty);
		if(itemNewQty > parentQty){
			alert("You can not add more than : " + parentQty);
			item.newquantity = 0;
		}
		else{
			assignQty = parseInt(parentQty) - parseInt(itemNewQty);
			$scope.models.dropzones['0'][parentIndex]['items'][index]['quantity'] = assignQty;
			item.quantity = itemNewQty;
			item.newquantity = itemNewQty;
		}
	};
	
	
	$scope.dragoverCallback = function(index, external, type, callback) {
		$scope.logListEvent('dragged over', index, external, type);
        // Invoke callback to origin for container types.
        if (type == 'container' && !external) {
            //console.log('Container being dragged contains ' + callback() + ' items');
        }
		//console.log("DragePostion : " + index);
        return index < 10; // Disallow dropping in the third row.
	};
	
     $scope.dropCallback = function(index, item, external, type) {
		item.parentId = item.id;
		item.readonly = 0;
		item.quantity = parseInt($scope.models.itemDetails[item.productId]);
		item.id = $scope.models.itemCount;
        $scope.logListEvent('dropped at', index, external, type);
		$scope.models.itemCount = $scope.models.itemCount + 1;
		$scope.models.itemDetails[item.productId] = 0;
        // Return false here to cancel drop. Return true if you insert the item yourself.
        return item;
    };
	$scope.logEvent = function(message) {
        //console.log(message);
    };
	$scope.logListEvent = function(action, index, external, type) {
        var message = external ? 'External ' : '';
        message += type + ' element was ' + action + ' position ' + index;
        //console.log(message);
    };
    $scope.$watch('models.dropzones', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);
	
	$scope.cloneRow = function(){	
		var boxId = jQuery(".selectboxlist").val();
		if(boxId == ''){
			alert("Please select Box Type");
		}
		else{
			listnew = {};
			listnew['items'] = [];
			listnew['box'] = jQuery(".selectboxlist option:selected").text();
			listnew['boxId'] = jQuery(".selectboxlist").val();
			listnew['effectAllowed'] = 'copy';
			listnew['items'] = listnew.items;
			$scope.models.dropzones['0'].unshift(listnew);		
		}
	};
	$scope.deleteRow = function(zone,list){
		itemDetails = $scope.models.dropzones['0'][zone]['items'];		
		for (var key in itemDetails) {
			 var itemDetail = itemDetails[key];
			 $scope.models.itemDetails[itemDetail.productId] = parseInt($scope.models.itemDetails[itemDetail.productId]) + parseInt(itemDetail.quantity);
		}
		$scope.models.dropzones['0'].splice(zone, 1);
	};
	$scope.deleteColomItem = function(parentIndex,index){	
		colDetails = $scope.models.dropzones['0'][parentIndex]['items'][index];
		productId = colDetails.productId;
		qty = parseInt(colDetails.quantity);
		$scope.models.itemDetails[productId] += qty;
		$scope.models.dropzones['0'][parentIndex]['items'].splice(index, 1);
	};
	
	$scope.addColumn = function(index){	
		var productId = jQuery(".productselect"+index).val();
		var productSku = jQuery(".productselect"+index + " option:selected").text();
		var qty = jQuery(".sizeselect"+index).val();
		if(productId == ''){
			alert("Please select product");
		}
		else{
			if(qty == ''){
				alert("Please enter the product qty");
			}
			else{
				var itemQty = $scope.models.itemDetails[productId];
				if(itemQty < qty){
					alert("You can add maximum qty of this item is : " + itemQty);
				}
				else{
					$scope.models.itemDetails[productId] -= qty;
					listnew = $scope.models.dropzones['0'][index]['items'];
					obj = { "effectAllowed": "copy", "productId": productId, "productSku": productSku, "quantity": qty };
					listnew.push(obj);
					$scope.models.dropzones['0'][index]['items'] = listnew;
				}
			}
		}
	};	
}); 
</script>
<style>
.tbl_length { width: auto; }
.astroid-layout-row-container.ui-sortable {
    padding-bottom: 51px;
}
.column-inner {
    display: inline-block;
    width: 100%;
}
.astroid-layout-row-container  .inputtext{
    height: auto;
}
.astroid-layout-column.col-md-10.row-fluid.ng-scope {
    border-bottom: 1px solid #ccc;
    margin-left: 50px;
	line-height: 39px;
}
.ui-sortable.columnsortable {
    padding-bottom: 20px;
}
.columnsortable  .astroid-layout-column {
	cursor: all-scroll;
}
.table-responsive {
    padding-bottom: 5px;
}
.left {
    float: left;
}
</style>
