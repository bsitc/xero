<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="index.html">Home</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Currency Details</span>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet ">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i>Currencies Listing </div>
					<div class="actions">
						<a href="<?php echo base_url('purchase/purchase/fetchRate');?>" class="btn btn-circle btn-info btnactionsubmit">
							<i class="fa fa-download"></i>
							<span class="hidden-xs"> Fetch Rate </span>
						</a>
						<a href="<?php echo base_url('purchase/purchase/postRate');?>" class="btn btn-circle green-meadow btnactionsubmit">
							<i class="fa fa-upload"></i>
							<span class="hidden-xs"> Post Rate </span>
						</a>
						<div class="btn-group">
							<a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown">
								<i class="fa fa-share"></i>
								<span class="hidden-xs"> Tools </span>
								<i class="fa fa-angle-down"></i>
							</a>
							<div class="dropdown-menu pull-right">
								<?php
									foreach($this->account1Account as $account1Account)
									echo '<li><a class="btnactionsubmit" href="'.base_url('purchase/purchase/postRate/'.$account1Account['id']).'"> Post rate to '.$account1Account['name'].' </a></li>';								
								?>											
							</div>
						</div>
					</div>					
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<div class="table-actions-wrapper hide">
							
						</div>
						<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									<th width="1%"><input type="checkbox" class="group-checkable"> </th>
									<th width="15%"> From Currency</th>
									<th width="15%"> To Currency </th>
									<th width="10%"> Rate </th>
									<th width="10%"> Update </th>
									<th width="10%"> Action </th>
								</tr>
								<tr role="row" class="filter">
									<td> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="fromCurrency"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="toCurrency"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="exchangeRate"> </td>
									<td><input type="text" class="form-control form-filter input-sm" name="updated"> </td>
									<td><div class="margin-bottom-5">	<button class="btn btn-sm btn-success filter-submit margin-bottom">		<i class="fa fa-search"></i> Search</button></div><button class="btn btn-sm btn-default filter-cancel">	<i class="fa fa-times"></i> Reset</button>
									</td>
								</tr>
							</thead>
							<tbody> </tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
</div>
<!-- END CONTENT BODY -->
</div>		
<?php if(@!$this->session->userdata($this->router->directory.$this->router->class)[0]){	echo '<script type="text/javascript"> jsOrder =  [ 1, "desc" ];</script>'; } ?>
<script type="text/javascript">
	loadUrl = '<?php echo base_url('purchase/purchase/getPurchase');?>';
</script>