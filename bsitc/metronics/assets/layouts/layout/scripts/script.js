function editAction(data){
    if( typeof data.id !== 'undefined' || data.id === null ){
        if(jQuery(".confighml .htmlaccount"+data.id).length){
            jQuery("#actionmodal .form-body").html(jQuery(".confighml .htmlaccount"+data.id).html());
        }
    }
    jQuery("#actionmodal").modal("show");
    for (var key in data){
        if (data.hasOwnProperty(key)) {
            jQuery("#actionmodal ."+key).val(data[key]);
            if(jQuery("#actionmodal ."+key).is("select")) {
				getMultiple = jQuery("#actionmodal ."+key).attr('multiple');
				if(getMultiple){
					keyvaluesTemps = data[key];
					if( typeof data.id !== 'undefined' ){
						keyvalues = keyvaluesTemps.split(",");
						jQuery('#actionmodal .' + key).val(keyvalues);
						keyvalues.forEach(function(element) {
						  jQuery('#actionmodal .' + key +  ' option[value="'+ element +'"]').prop('selected', true); 
						});
					}
				}
				else{
					jQuery('#actionmodal .' + key +  ' option[value="'+ key +'"]').attr('selected','1');   
				}				
            }
        }
    }
    jQuery(".acc1list").trigger("change");
    jQuery(".acc2list").trigger("change");
    for (var key in data){
        if (data.hasOwnProperty(key)) {
            jQuery("#actionmodal ."+key).val(data[key]);
            if(jQuery("#actionmodal ."+key).is("select")) {
				getMultiple = jQuery("#actionmodal ."+key).attr('multiple');
				if(getMultiple){
					keyvaluesTemps = data[key];
					if( typeof data.id !== 'undefined' ){
						keyvalues = keyvaluesTemps.split(",");
						jQuery('#actionmodal .' + key).val(keyvalues);
						keyvalues.forEach(function(element) {
							selecteoptionelement = jQuery('#actionmodal .' + key +  ' option[value="'+ element +'"]');
							selecteoptionelement.each(function() {
								if (jQuery(this).css("display") == "block") {
									jQuery(this).prop('selected', true); 
								} else {
									//jQuery(this).prop('selected', true); 
								}
							});
					
							//jQuery('#actionmodal .' + key +  ' option[value="'+ element +'"]').prop('selected', true); 
						});
					}
				}
				else{
					keyvaluesTemps = data[key];
					selecteoptionelement = jQuery('#actionmodal .' + key +  ' option[value="'+ keyvaluesTemps +'"]');
					selecteoptionelement.each(function() {
						if (jQuery(this).css("display") == "block") {
							jQuery(this).prop('selected', true); 
						} else {
							//jQuery(this).prop('selected', true); 
						}
					});
					//jQuery('#actionmodal .' + key +  ' option[value="'+ key +'"]').attr('selected','1');
				}
            }
        }
    }
}
function deleteAction(url,thisobj){
	if(jQuery(thisobj).attr('disabled') == 'disabled'){ return false; }
 	if (confirm("Are you sure want to delete?")) {
 		jQuery(thisobj).attr('disabled','disabled');
        jQuery.get(url,function(res){
        	jQuery(thisobj).closest('tr').remove();
        })
    } else {
        return false;
    }
}
jQuery(document).ready(function(){
    jQuery(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    jQuery(document).on("click",".showbtn",function(){
    	jQuery(this).prev(".showpassword").removeClass("hide").end().prev().prev().addClass("hide");
    	jQuery(this).hide();
    })
    jQuery(".actionaddbtn").on("click", function(e){
    	e.preventDefault();
        if(jQuery(".confighml div:first-child").length){
            jQuery("#actionmodal .form-body").html(jQuery(".confighml div:first-child").html());
        }
        jQuery(".submitAction").removeAttr('disabled');
    	jQuery("#actionmodal input,#actionmodal select").val('');
		jQuery("#actionmodal textarea,#actionmodal select").val('');
		jQuery("#actionmodal select option").removeAttr('selected');
    	jQuery("#actionmodal").modal("show");
    });
	
	jQuery(".useractionaddbtn").on("click", function(e){
    	e.preventDefault();
        if(jQuery(".confighml div:first-child").length){
            jQuery("#actionmodal .form-body").html(jQuery(".confighml div:first-child").html());
        }
        jQuery(".submitAction").removeAttr('disabled');
    	jQuery("#actionmodal input,#actionmodal select").val('');
		jQuery("#actionmodal textarea,#actionmodal select").val('');
		jQuery("#actionmodal select option").removeAttr('selected');
    	jQuery("#actionmodal").modal("show");
    });
    jQuery(".acc1list").on("change",function(){
        acc1listval = jQuery(this).val();
        if(acc1listval.length > 0){
            jQuery(".acc1listoption option").hide();
            jQuery(".acc1listoption option").removeAttr('selected');
            jQuery(".acc1listoption"+acc1listval).show();
        }
    })
    jQuery(".acc2list").on("change",function(){
        acc2listval = jQuery(this).val();
        if(acc2listval.length > 0){
            jQuery(".acc2listoption option").hide();
            jQuery(".acc2listoption option").removeAttr('selected');
            jQuery(".acc2listoption"+acc2listval).show();
        }
    })

    jQuery(".submitAction").on("click",function(e){
        e.preventDefault();
        submitAction = jQuery(this);
        jQuery("#saveActionForm .alert-danger").hide();
        jQuery("#saveActionForm .form-group").removeClass("has-error");
        jQuery(this).attr('disabled','disabled');
        var error = false;
        jQuery("#saveActionForm .form-group").each(function(){
            $this = jQuery(this);
            var input = jQuery(this).find("input");    
            if(input.length == '0') {
              input = jQuery(this).find("select");   
            }   
            if(input.attr('data-required') == '1'){
                if( (input.val() == '') || (input.val() == null) ){
                    error = true;
                    $this.addClass("has-error");
                }
            }
        })
        if(error == false){
            var id = jQuery("#saveActionForm .id").val();
            form = jQuery("#saveActionForm");
            formdata = form.serialize();
            jQuery.ajax({
                type:"POST",
                url:form.attr("action"),
                data:formdata,
                success: function(response){
                    submitAction.removeAttr('disabled');
					console.log(response);
                    resobj = JSON.parse(response); 
                    form.find(".alert").html(resobj.message);
                    if(resobj.status == true){
                        jQuery("#actionmodal").modal("hide");
                        if(id == ''){
                            id = resobj['id'];
                            if((jQuery(".tr"+id).length == 0)||(jQuery(".tr"+id).length == '0')){
                                appendHtml = '<tr class="tr'+id+'">'+ jQuery(".actiontable .clone").html() +'</tr>'
                                jQuery(".actiontable tbody").append(appendHtml);
                                delurl = jQuery(".tr"+id+ " .actiondelbtn").attr('delurl')+id;
                                jQuery(".tr"+id+ " .actiondelbtn").attr("onclick","deleteAction('"+delurl+"',this)");
                                jQuery(".tr"+id+ " .actioneditbtn").attr("onclick","editAction("+response+")");
                            }
                        }
                        if(id > 0){
                            jQuery(".tr"+id + " .value").each(function(){
                                jQuery(".tr"+id+ " .actioneditbtn").attr("onclick","editAction("+response+")");
                                keyval = jQuery(this).attr('data-value');
                                jQuery(this).html(resobj[keyval]);
                            })
                        }
                    }
					jQuery(".usermodel").modal("hide");
					jQuery(".userfiltertrigger").trigger('click');
                }
            });           
        }
        else{
            submitAction.removeAttr('disabled');
            jQuery("#saveActionForm .alert-danger").show();
            jQuery("#saveActionForm .alert-danger").html('<button class="close" data-close="alert"></button> You have some form errors. Please check below.');
        }
    })
	jQuery('#datatable_products input').keypress(function (e) { 
		var key = e.which;
		if(key == 13){
			jQuery('.filter-submit').click();
		}
	}); 
})
