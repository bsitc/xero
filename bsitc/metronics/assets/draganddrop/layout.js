angular.module("demo", ["ngRoute", "dndLists"])
.controller('NestedListsDemoController', function($scope) {
    $scope.models = {
        selected: null,
        templates: [
            {type: "item", id: 2},
            {type: "container", id: 1, columns: [[], []]}
        ],
        dropzones: {
            "A": [
                {
                    "type": "container",
                    "id": 1,
                    "columns": [
						[
                            {
                                "type": "item",
								 "colsize": "6",
                                "id": "3"
                            }
                        ],
                        [
                            {
								"type": "container",
								"id": 2,
								"columns": [
									[
										{
											"type": "item",
											"colsize": "6",
											"id": "1"
										},                            
									],
									[
										{
											"type": "container",
											"id": 3,
											"columns": [
												[
													{
														"type": "item",
														"colsize": "6",
														"id": "1"
													},                            
												],
												[
													{
														"type": "item",
														 "colsize": "6",
														"id": "3"
													}
												]
											]
										},
									]
								]
							},                           
                        ],
                       
                    ]
                },
				{
                    "type": "container",
                    "id": 4,
                     "columns": [
                        [
                            {
                                "type": "item",
                                "colsize": "9",
                                "id": "1"
                            },                            
                        ],
                        [
                            {
                                "type": "item",
								 "colsize": "3",
                                "id": "3"
                            }
                        ]
                    ]
                },
				
            ]            
        }
		
    };
	$scope.dragoverCallback = function(index, external, type, callback) {
		// Invoke callback to origin for container types.
		if (type == 'container' && !external) {
			console.log('Container being dragged contains ' + callback() + ' items');
		}
		return index < 10; // Disallow dropping in the third row.
	};

    $scope.dropCallback = function(index, item, external, type) {
        // Return false here to cancel drop. Return true if you insert the item yourself.
        return item;
    };
	
    $scope.$watch('models.dropzones', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);
	
	$scope.cloneRow = function(index,list){	
		listnew = {};
		listnew['items'] = [        
			{
				  "effectAllowed": "move",
				  "colsize": "12",
				  "params" : {"name":"none"}
			},        
		];
		listnew['effectAllowed'] = "copyMove";
		listnew['params'] = {"sectionTitle":"New Section"};
		$scope.models.dropzones.splice((index + 1), 0, listnew);
	};
	$scope.deleteRow = function(event){	
		 var element = event.currentTarget;		 
		var removeText = [];
		jQuery(element).parentsUntil("#astroid-layout-builder").each(function(){
			dataindex = jQuery(this).attr('data-index');
			console.log("dataindex", dataindex);
				if (typeof dataindex !== "undefined") {
				datatype = jQuery(this).attr('data-type');
				if (typeof datatype !== "undefined") {
					if(dataindex.length){
						if(datatype == 'container'){
							//removeText = "["+ dataindex +"]" + removeText;
							removeText.push(dataindex);
						}
						else{
							//removeText = ".columns[" + dataindex + "]" + removeText;
							removeText.push('columns');
							removeText.push(dataindex);
						}					
					}
				}
			}
		});
		
		console.log(removeText);
		
		if(removeText.length > 0){
			length = removeText.length - 1;
			index = removeText.pop();
			if(removeText.length == 0){
				$scope.models.dropzones['A'].splice(index,'1')
			}
			else if(removeText.length == '1'){
				$scope.models.dropzones['A'][removeText[0]].splice(index,'1')
			}
			else if(removeText.length == '2'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]].splice(index,'1')
			}
			else if(removeText.length == '3'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]].splice(index,'1')
			}
			else if(removeText.length == '4'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]][removeText[3]].splice(index,'1')
			}
			else if(removeText.length == '5'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]][removeText[3]][removeText[4]].splice(index,'1')
			}
			else if(removeText.length == '6'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]][removeText[3]][removeText[4]][removeText[5]].splice(index,'1')
			}
			else if(removeText.length == '7'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]][removeText[3]][removeText[4]][removeText[5]][removeText[6]].splice(index,'1')
			}
			else if(removeText.length == '8'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]][removeText[3]][removeText[4]][removeText[5]][removeText[6]][removeText[7]].splice(index,'1')
			}
			else if(removeText.length == '9'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]][removeText[3]][removeText[4]][removeText[5]][removeText[6]][removeText[7]][removeText[8]].splice(index,'1')
			}
			else if(removeText.length == '10'){
				$scope.models.dropzones['A'][removeText[0]][removeText[1]][removeText[2]][removeText[3]][removeText[4]][removeText[5]][removeText[6]][removeText[7]][removeText[8]][removeText[9]].splice(index,'1')
			}
			
			
			for (i = 0; i < removeText.length; i++) {
				key = removeText[i];
			} 

			//console.log($scope.models.dropzones['A'][key]);
			
			console.log(index);
			/* for(var removeTex in removeText){
				$scope.models.dropzones['A'][removeText]
			} */

			
			console.log(removeText);
			//$scope.models.dropzones['A'].splice([0].columns[1],1)
			//$scope.models.dropzones['A'].splice(removeText,'1')
			//console.log($scope.models.dropzones['A'][0]['columns'][1][0]['columns'][1][0]);
		}


		
	};
	$scope.addColumn = function(index, column, ispopup = '0'){		
		columnArray = JSON.parse("[" + column + "]");	
		listnew = [];maxvaluesize = 12;
		angular.forEach(columnArray, function(value, key) {
			tmp = maxvaluesize - value;
			if(tmp >= 0 ){
				obj = { "effectAllowed": "move", "colsize": value, "params" : {"name":"none"} };
			}
			else{
				obj = { "effectAllowed": "move", "colsize": maxvaluesize, "params" : {"name":"none"} };
			}
			maxvaluesize = tmp;
			listnew.push(obj);
		});		
		if(ispopup == '1'){
			containerCoumn = $scope.models.dropzones[$scope.selectedColIndex];	
			$scope.models.dropzones[$scope.selectedColIndex]['items'] = listnew;
		}
		else{
			containerCoumn = $scope.models.dropzones[index];	
			$scope.models.dropzones[index]['items'] = listnew;
		}
		$scope.selectedColSize = column;
	};
	
	$scope.openRowpopup = function(index, container){	
		$scope.rowparamas = container.params;
		$scope.rowparamas.index = container.index;
		angular.element(jQuery('#astroup-rowpopup')).modal('show');
	};
	
	$scope.openColpopup = function(rowindex, index,column){		
		$scope.columnparams = column.params;
		angular.element(jQuery('#astroup-colpopup')).modal('show');
	};	
	$scope.openCustomLayoutpopup = function(index){
		rowItems = $scope.models.dropzones[index]['items'];
		selectedColSize = '';
		angular.forEach(rowItems, function(value, key) {
			selectedColSize += value.colsize + ',';
		});
		selectedColSize = selectedColSize.substring(0, selectedColSize.length - 1);
		$scope.selectedColSize = selectedColSize;	
		$scope.selectedColIndex = index;		
		angular.element(jQuery('#astroup-customlayoutpop')).modal('show');
	};
	$scope.addCustomColumn = function(){
		index = $scope.selectedColIndex;
		$scope.addColumn(index,$scope.selectedColSize);	
		angular.element(jQuery('#astroup-customlayoutpop')).modal('hide');
	};
	
});