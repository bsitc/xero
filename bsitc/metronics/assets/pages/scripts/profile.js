var Profile = function() {
    var dashboardMainChart = null;
    return {
        //main function
        init: function() {
            Profile.initMiniCharts();
        },
        initMiniCharts: function() {
            // IE8 Fix: function.bind polyfill
            if (App.isIE8() && !Function.prototype.bind) {
                Function.prototype.bind = function(oThis) {
                    if (typeof this !== "function") {
                        // closest thing possible to the ECMAScript 5 internal IsCallable function
                        throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
                    }
                    var aArgs = Array.prototype.slice.call(arguments, 1),
                        fToBind = this,
                        fNOP = function() {},
                        fBound = function() {
                            return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
                                aArgs.concat(Array.prototype.slice.call(arguments)));
                        };
                    fNOP.prototype = this.prototype;
                    fBound.prototype = new fNOP();
                    return fBound;
                };
            }
            $("#sparkline_bar").sparkline([8, 9, 10, 11, 10, 10, 12, 10, 10, 11, 9, 12, 11], {
                type: 'bar',
                width: '100',
                barWidth: 6,
                height: '45',
                barColor: '#F36A5B',
                negBarColor: '#e02222'
            });
            $("#sparkline_bar2").sparkline([9, 11, 12, 13, 12, 13, 10, 14, 13, 11, 11, 12, 11], {
                type: 'bar',
                width: '100',
                barWidth: 6,
                height: '45',
                barColor: '#5C9BD1',
                negBarColor: '#e02222'
            });
        }
    };
}();
if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        Profile.init();
    });
}
jQuery(document).ready(function() {
    jQuery(".savefrombtn").on("click",function(e){
        $this = jQuery(this);
        $this.attr('disabled','disabled');
        $this.find('img').show();
        $this.find('span').hide();
        form = $this.closest("form");
        e.preventDefault();
        formdata = form.serialize();
        if(form.find('.fileinput-preview.thumbnail img').length){
            formdata = {'file' : form.find('.fileinput-preview.thumbnail img').attr('src')};
        }
        jQuery.ajax({
            type:"POST",
            url:form.attr("action"),
            data:formdata,
            success: function(response){
                $this.find('img').hide();
                $this.find('span').show();
                $this.removeAttr('disabled');
                resobj = JSON.parse(response); 
                form.find(".alert").html(resobj.message);
                if(resobj.status != true){
                    form.find(".alert").addClass('alert-danger');
                    form.find(".alert").removeClass('alert-success');
                }
                else{
                    form.find(".alert").removeClass('alert-danger');
                    form.find(".alert").addClass('alert-success');
                }
                form.find(".alert").removeClass("hide");
            }
        });
    });
    jQuery(".switchbtn").change(function(){
        nameClass = jQuery(this).attr('name');
        if(jQuery(this).prop('checked') == true){
            jQuery("." + nameClass).show();
        } 
        else{
             jQuery("." + nameClass).hide();
        }
    });
    jQuery(".switchbtn").each(function(){
         nameClass = jQuery(this).attr('name');
        if(jQuery(this).prop('checked')){
            jQuery("." + nameClass).show();
        } 
        else{
             jQuery("." + nameClass).hide();
        }
    });
    jQuery("select").each(function(){
        var value = jQuery(this).attr('value');
        jQuery(this).find('option[value="'+value+'"]').attr('selected','1')
    });
});